package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class Email_Signup_page extends LoadableComponent <Email_Signup_page> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	
	private static final String BREAD_CRUMB = "div[class='breadcrumb']";
	
	
	@FindBy(css = BREAD_CRUMB + " a[title='Go to Home']")
	WebElement breadcrumbHome;
	
	@FindBy(css = BREAD_CRUMB + " a[title='Email']")
	WebElement breadcrumbEmail;
	
	@FindBy(css = "div[id='main'] h2")
	WebElement emailSignup;
	
	@FindBy(css = "div[id='main'] h3")
	static WebElement emailTxt;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public Email_Signup_page(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, emailSignup))) {
			Log.fail("Stay connected page is not display", driver);
		
		}
		
	}
	
	
	/**
	 * To get a text from stay Connected Page 
	 * 
	 * @param breadCrumbStayConnect
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from stayConnected Page 
	 * @throws Exception 
	 */
	final public String getTextFromemailSignupPage(WebDriver driver) throws Exception {
		String textEmailAddress = BrowserActions.getText(driver, emailTxt, "Email signup Page");
		return textEmailAddress;

	}// getTextFromEmailSignuppage

}
