package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;



public class TermsAndConditionsPage extends LoadableComponent <TermsAndConditionsPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of Terms & Conditions Page **************************
	 **********************************************************************************************/
	public static final String TERMS_CONDITIONS_CONTENT_HEADER ="Terms & Conditions of Sale";
	
	@FindBy(css = "a.breadcrumb-element")
	WebElement breadcrumb;

	@FindBy(css = "div#primary")
	WebElement primaryContent;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public TermsAndConditionsPage(WebDriver driver) {		
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {   
		
		if (!isPageLoaded) {
			Assert.fail();
		}		

		if (isPageLoaded && !(Utils.waitForElement(driver, primaryContent))) {
			Log.fail("Terms & Conditions page did not open up.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {		

		isPageLoaded = true;	
		Utils.waitForPageLoad(driver);
	}// load	
	
	public String getContentHeader() throws Exception{
		String contentHeaderText= null;
		contentHeaderText=BrowserActions.getText(driver, breadcrumb, "Terms & Condition content header");
		return contentHeaderText;		
	}
	

}// GiftRegistryPage
