package com.belk.pages.footers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.SearchResultPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class ListOfBrandPage extends LoadableComponent <ListOfBrandPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer; 
	String runPltfrm = Utils.getRunPlatForm();
	
	public static final String LIST_OF_BRAND_CONTENT_STRING ="Shop by Brand";
	
	@FindBy(css = ".pt_brand-search-result")
	static WebElement bcBrand;
	
	@FindBy(css = "div#primary")
	WebElement brandPrimaryContent;
	
	@FindBy(css = ".breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;
	
	@FindBy(css= ".breadcrumb")
	List<WebElement> shopByBreadCrumb;
	
	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".filterby-refinement.hide-desktop>a")
	WebElement btnMobileFilterBy;

	@FindBy(css = ".refinement-link")
	List<WebElement> fldCatagoryRefinement;

	@FindBy(css = "div[id='primary']>div[class='html-slot-container']")
	WebElement txtShopByBrandBanner;
	
	@FindBy(css=".exclusive-brands-asset")
	WebElement lblExclusiveBrandAssetBanner;
	
	@FindBy(css = ".header-search #q")
	public WebElement txtSearch;

	
	@FindBy(css = "input[id='page-search-text']")
	WebElement txtSearchBrand;

	@FindBy(css = "div[class='error error-msg hide']")
	WebElement alertMsg;

	@FindBy(css = ".brand_head")
	List<WebElement> lstBrand;

	@FindBy(css = ".brand_head.active+div ul:nth-child(1) li a")
	List<WebElement> lstSubBrand;
	
	@FindBy(css = ".backtotop>a")
    WebElement lblBackToTop;        
    
    @FindBy(css = ".filterby-refinement.hide-desktop>a")
    WebElement btnFilterByMobile;
    
    @FindBy(css = "input[id='page-search-text']")
    WebElement txtEnterBrand;
    
    @FindBy(css = "#secondary")
    List<WebElement> lstLeftNavRefinement;

	@FindBy(css = "ul[id='category-level-1']>li")
	List<WebElement> lstCategory;
	
	@FindBy(xpath="//div[@class='brand_head active'][text() = 'A']")	
    WebElement lblActiveBrandHead; 
		
	@FindBy(xpath="//div[@class='brand_head'][text() = 'A']")	
    WebElement lblFirstBrandHeadMobile; 
	
	@FindBy(css = "div[class='brand_head'] :nth-child(1)")
    List<WebElement> lblBrandHeading;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public ListOfBrandPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, bcBrand))) {
			Log.fail("List Of Brands page is not displayed", driver);
		
		}
		elementLayer = new ElementLayer(driver);
		
	}
	
	/**
	 * To get a text from Store Location Page 
	 * 
	 * @param bcBrand
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Store Location Page 
	 * @throws Exception 
	 */
	public String getTextFromListBrandPage() throws Exception {
		String textBCListBrand = BrowserActions.getText(driver, bcBrand, "List Brand Page");
		return textBCListBrand;

	}//getTextFromListBrandPage
	
	/**
	 * To click and navigate to a category in breadcrumb
	 * 
	 * @param categoryName
	 * @throws Exception
	 */
	public boolean VerifyCategoryNameInBreadcrumb() throws Exception {
		boolean status = false;
		String[] categoryName1 = { "Home", "Shop by Brand" };
		String categoryName2 = "Back to Home";
		if (runPltfrm == "desktop") {
			for (int i = 0; i < categoryName1.length; i++) {
				if (categoryName1[i].equals(BrowserActions.getText(driver, lstTxtProductInBreadcrumb.get(i), "Breadcrumb in desktop"))) {
					status = true;
				}
			}
		} else if (runPltfrm == "mobile") {
			if (categoryName2.equals(BrowserActions.getText(driver, txtProductInBreadcrumbMobile, "Breadcrumb in mobile"))) {
				status = true;
			}
		}
		return status;
	}

	/**
	 * To click the Filter By button in Mobile
	 * 
	 * @throws Exception
	 */
	public void clickFilterByInMobile() throws Exception {
		BrowserActions.clickOnElement(btnMobileFilterBy, driver, "Filter By option in mobile");
	}

	public boolean verifyRefinementCount() throws Exception {
		boolean status = false;
		for (WebElement element : fldCatagoryRefinement) {
			WebElement element1 = element.findElement(By.cssSelector("span"));
			if (element1.getAttribute("class").contains("refine-count")) {
				element = element1;
				status = true;
			}

		}
		return status;
	}
	
	
	
	/**
	 * 
	 * @param brandToSearch
	 * @return SearchResultPAge
	 * @throws Exception
	 */
	public SearchResultPage searchBrand(String brandToSearch) throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.typeOnTextField(txtSearchBrand, brandToSearch, driver, "Search Box");
		Log.trace("Text entered in Search Box.");
		if ((!Utils.waitForElement(driver, alertMsg))) {
			driver.findElement(By.id("page-search-text")).sendKeys(Keys.ENTER);
			Log.event("Searched the provided product!", StopWatch.elapsedTime(startTime));

			return new SearchResultPage(driver).get();
		}
		return null;
	}

	/**
	 * To navigating to Brand Toggle
	 * 
	 * @throws Exception
	 */
	public boolean VerifyBrandToggle() throws Exception {
		boolean returnValue = false;
		for (int i = 0; i < lstBrand.size(); i++) {
			BrowserActions.clickOnElement(lstBrand.get(i), driver,
					"Brand Names Expand Toggle");
			WebElement toggle = lstBrand.get(i);
			if (!toggle.getAttribute("class").contains("active")) {
				returnValue = true;
				BrowserActions.clickOnElement(lstBrand.get(i), driver,
						"Brand Names Expand Toggle");
			}
		}
		return returnValue;
	}

	/**
	 * To Select Brand
	 * 
	 * @throws Exception
	 */
	public String selectBrand() throws Exception {
		String selected_brand = null;
		for (int i = 0; i < lstBrand.size(); i++) {
			
				BrowserActions.javascriptClick(lstBrand.get(i), driver, "Brand Names");
				BrowserActions.javascriptClick(lstBrand.get(++i), driver, "Brand Name");
				for (int j = 0; j < lstSubBrand.size(); j++) {
						WebElement ele = lstSubBrand.get(j);					
						
						selected_brand = ele.getText();
						BrowserActions.javascriptClick(ele, driver, "");
						break;
				}
			break;
		
		}

		return selected_brand;
	}
	/**
	 * To Check Brand columns lists
	 * return count
	 * @throws Exception
	 */
	
	public int checkBrandColumnsCount() throws Exception{
		int count = 0;
	
		List<WebElement> lists = driver.findElements(By.cssSelector(".brand_head.active+div ul"));
	   
		if(Utils.getRunPlatForm()=="desktop")
				count = lists.size();
		if(Utils.getRunPlatForm()=="mobile"){
			   BrowserActions.clickOnElement(lstBrand.get(0), driver, "Brand Names");
				count = lists.size();
		}
		return count;
		
	}
	
	public void clickBackToToplink() throws Exception{
		BrowserActions.scrollToViewElement(lblBackToTop, driver);
		BrowserActions.clickOnElement(lblBackToTop, driver, "Back To Top");		
		BrowserActions.nap(2);
	}
	
	public void clickFilterBy() throws Exception{
		BrowserActions.clickOnElement(btnFilterByMobile, driver, "Filter By");		
	}
	
	public boolean verifyTxtEnterBrand() throws Exception{
		boolean status =false;		
		String txtBrandName = BrowserActions.getTextFromAttribute(driver, txtEnterBrand, "placeholder", "Placeholder text");
		if(txtBrandName.equals("Enter brand name")){
			status = true;
		}
		else{
			status = false;
		}
		return status;				
	}	
	
	/**
	 * To Select Category
	 * 
	 * @return selected_category
	 */
	public String selectCategoryByIndex(int index) throws Exception {
		String Selected_category = null;
		WebElement e = lstCategory.get(index - 1);
		Selected_category = e.getText();
		if (Selected_category != "Clearance")
			BrowserActions.clickOnElement(e, driver, " Selecting Category");
		return Selected_category;
	}

	public boolean verifyColorOfAlertMsg(){
		boolean status = false;
		String colorOfAlertMsgText = "rgba(151, 29, 34, 1)";
		status = Utils.verifyCssPropertyForElement(alertMsg, "color",
				colorOfAlertMsgText);
		return status;
	}

	public String getTextFromAlertMsg() throws Exception{
		return BrowserActions
				.getText(driver, alertMsg, "Text in alert message");
	}

	public boolean verifyPageSearchTxtBoxDisplayedAboveBrands() {
		boolean status = false;
		return status;
	}
	
	/**
	 * To Verify if the List of Brand Headings Ordered and Formatted
	 * @return
	 * @throws Exception
	 */
	public boolean verfiyBrandHeadingsOrderedAndFormatted() throws Exception {
		
		char x= 'A';
		int result =2;
		String cssActual =null;
		String cssExpected ="HelveticaNeue-Bold,Arial";
		
		if (runPltfrm != "mobile") 	
		{	
			x='B';	
			BrowserActions.scrollToViewElement(lblActiveBrandHead, driver);	
		}		
		else if (runPltfrm == "mobile") 
		{
			x='A';
			BrowserActions.scrollToViewElement(lblActiveBrandHead, driver);	
		}
		for (int i=0; i<lblBrandHeading.size(); i++)
		{
			
			cssActual=lblBrandHeading.get(i).getCssValue("font-family");
			if(!BrowserActions.getText(driver, lblBrandHeading.get(i), "List Brand Page").equals(x))
			{
				result=0;
			}
			else if(!Utils.verifyCssPropertyForElement(lblBrandHeading.get(i), cssActual,cssExpected))
			{
				result=0;
			}
			
			x = (char) (x+1);			
		}		
		
		if(result==2)
			return true;
		else
			return false;

		
				
	}//verfiyBrandHeadingsOrderedAndFormatted


	/**
	 * To verify if the Search Brand Text Box appears above the List of Brands
	 * @return
	 * @throws Exception
	 */
	public boolean verifyTxtSearchBrandAppearsAboveListofBrands() throws Exception {
		BrowserActions.scrollToViewElement(txtEnterBrand, driver);
		WebElement element = txtEnterBrand.findElement(By.xpath("../../..")).findElement(By.xpath("//following-sibling::div[@class='brand-list-cont']"));
		return Utils.waitForElement(driver, element); 
	}
	
}
