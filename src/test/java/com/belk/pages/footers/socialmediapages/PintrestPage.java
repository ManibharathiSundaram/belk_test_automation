package com.belk.pages.footers.socialmediapages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.Log;
import com.belk.support.Utils;

public class PintrestPage extends LoadableComponent <PintrestPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public static final String title="pinterest";
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of Pinterest Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "div[class='appContent']")
	WebElement PinterestContent;
	
	/**********************************************************************************************
	 ********************************* WebElements of Pinterest Page - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public PintrestPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, PinterestContent))) {
			Log.fail("Pinterest did not open up. Site might be down.", driver);
		}

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,PinterestContent );
		
	}

}

