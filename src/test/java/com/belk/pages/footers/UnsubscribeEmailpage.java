package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class UnsubscribeEmailpage extends LoadableComponent <UnsubscribeEmailpage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	@FindBy(css = "div#primary")
	WebElement unSubscribeMailContent;
	
	
	@FindBy(css = "div#primary>h1")
	WebElement lblErrorText;
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public UnsubscribeEmailpage(WebDriver driver) {		
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {   
		
		if (!isPageLoaded) {
			Assert.fail();
		}		

		if (isPageLoaded && !(Utils.waitForElement(driver, unSubscribeMailContent))) {
			Log.fail("UnSubscribe email page did not open up.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {		

		isPageLoaded = true;	
		Utils.waitForPageLoad(driver);
	}// load	

	public String getTextFromUnScribeEmail() throws Exception {
		String textError= BrowserActions.getText(driver, lblErrorText, "UnSubscribeEmail Page");
		return textError;
	}//getTextFromAboutPage

}
