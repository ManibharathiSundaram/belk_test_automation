package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.Log;
import com.belk.support.Utils;



public class BelkRewardCardPage extends LoadableComponent <BelkRewardCardPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(css = "img[src*='welcome_header_en.jpg']")
	WebElement belkCreditCard;
	
	@FindBy(css = "input[name='userId']")
	WebElement txtUserID;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public BelkRewardCardPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	

	@Override
	protected void isLoaded() throws Error {
		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver,  txtUserID))) {
			Log.fail("Belk credit card page not open up. Site might be down.", driver);
		}
	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

		try {
			Utils.switchWindows(driver, "Belk Rewards Card", "title", "false");
		}
		catch (Exception e) {
		}

		Utils.waitForElement(driver, belkCreditCard);		
	}
}
