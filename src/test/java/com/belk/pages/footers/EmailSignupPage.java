package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.reusablecomponents.ShippingPageUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class EmailSignupPage extends LoadableComponent<EmailSignupPage> {
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public String txtFirstname = "input[id='dwfrm_emailsignup_firstName']";
	public String txtLastname = "input[id='dwfrm_emailsignup_lastName']";
	public String txtAddressOne = "input[id='dwfrm_emailsignup_address1']";
	public String txtAddressTwo = "input[id='dwfrm_emailsignup_address2']";
	public String txtCityField = "input[id='dwfrm_emailsignup_city']";
	public String chkState = "select[id='dwfrm_emailsignup_states_state'] + div";
	public String txtZipcode = "input[id='dwfrm_emailsignup_postal']";
	public String txtPhoneNo = "input[id='dwfrm_emailsignup_phone']";
	public String drpGender = "select[id='dwfrm_emailsignup_gender'] + div";
	public String drpBdayMonth = "select[id='dwfrm_emailsignup_calendar_month'] + div";
	public String drpBdayDay = "select[id='dwfrm_emailsignup_calendar_day'] + div";
	public String drpBdayYear = "select[id='dwfrm_emailsignup_calendar_year'] + div";
	public String btnSavePreferences = "button[name='dwfrm_emailsignup_confirm']";
	
	public static final String EMAIL_SIGNUP_CONTENT_MSG = "Email Signup";
	public static final String EMAIL_SIGNUP_SAVE_PREF_CONTENT_MSG = "Save Preferences";
	

	private static final String BREAD_CRUMB = "div[class='breadcrumb']";	
	public static final String EMAIL_SIGNUP = "div.email-signup-bottom";

	@FindBy(css = BREAD_CRUMB + " a[title='Go to Home']")
	WebElement breadcrumbHome;

	@FindBy(css = BREAD_CRUMB + " a[title='Email']")
	WebElement breadcrumbEmail;
	
	@FindBy(css = "div#main")
	WebElement emailContent;

	@FindBy(css = "div[id='main'] h2")
	WebElement emailSignup;

	@FindBy(css = "div[id='main'] h3")
	WebElement emailTxt;	

	@FindBy(id = "dwfrm_emailsignup_email")
	WebElement txtEmail;

	@FindBy(id = "dwfrm_emailsignup_firstName")
	WebElement txtFirstName;

	@FindBy(id = "dwfrm_emailsignup_lastName")
	WebElement txtLastName;

	@FindBy(id = "dwfrm_emailsignup_address1")
	WebElement txtAddress1;
	
	@FindBy(id = "dwfrm_emailsignup_address2")
	WebElement txtAddress2;

	@FindBy(id = "dwfrm_emailsignup_city")
	WebElement txtCity;

	@FindBy(css = "label[for='dwfrm_emailsignup_states_state']")
	WebElement lblState;

	@FindBy(id = "dwfrm_emailsignup_postal")
	WebElement txtZipCode;

	@FindBy(css = "label[for='dwfrm_emailsignup_gender']")
	WebElement lblGender;
	
	@FindBy(css = ".email-date-row")
	WebElement divBirthDate;	

	@FindBy(css = "dwfrm_emailsignup_calendar_month")
	WebElement drpMonth;
	
	@FindBy(id = "dwfrm_emailsignup_calendar_day")
	WebElement drpDay;
	
	@FindBy(id = "dwfrm_emailsignup_calendar_year")
	WebElement drpYear;

	@FindBy(id = "dwfrm_emailsignup_phone")
	WebElement txtPhone;
	
	@FindBy(css = "div[class='email-city'] div[class='form-field-tooltip']>a.tooltip")
	WebElement lnkApoFpo;
	
	@FindBy(css = "div[class='email-city'] div[class='form-field-tooltip']>a[aria-describedby*=ui-id-]")
	WebElement toolTipApoFpoOverlay;
	
	@FindBy(name = "dwfrm_emailsignup_cancel")
	WebElement btnCancel;	

	@FindBy(name = "dwfrm_emailsignup_confirm")
	WebElement btnSavePreference;
	
	@FindBy(css = "div[class='email-phone-row'] div[class='form-field-tooltip']>a.tooltip")			
	 WebElement lnkWhyIsReq;

	@FindBy(css = EMAIL_SIGNUP +" p:nth-child(2)>a")
	WebElement btnPrivacyPolicy;
	
	@FindBy(css = ".mobile-messages p a")
	WebElement lnkMobileMessage;
	
	@FindBy(css = "li[class='stop-email-messages']>p>a")
	WebElement btnEmailUnsubscribe;
	
	@FindBy(css = "div.email-signup-bottom-end>a:nth-child(2)")
	WebElement btnTermsAndUse;
	
	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	List<WebElement> txtProductInBreadcrumbMobile;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException
	 */
	public EmailSignupPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}
	
	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, emailSignup))) {
			Log.fail("Email SignUp page didn't open up", driver);
		}	
		elementLayer = new ElementLayer(driver);
	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get a text from stay Connected Page
	 * 
	 * @param breadCrumbStayConnect
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from stayConnected Page
	 * @throws Exception
	 */
	public String getTextFromemailSignupPage() throws Exception {
		String textEmailAddress = BrowserActions.getText(driver, emailTxt, "Email signup Page");
		return textEmailAddress;

	}// getTextFromEmailSignuppage

	/**
	 * To verify the the text present
	 */
	public String verifyTextFromEmailsignup() throws Exception {
		String textEmail = BrowserActions.getText(driver, emailSignup, "Email Signup");
		return textEmail;
	}

	public String getTextEmailSignup() throws Exception {
		String txtMessage = BrowserActions.getText(driver, emailTxt,
				"Qatest123@Gmail.Com Is Now Subscribed To Receive Emails From Belk");
		return txtMessage;
	}

	public String getTextFromemail_up() throws Exception {
		String textEmailsignup = BrowserActions.getText(driver, emailSignup, "Email Signup");
		return textEmailsignup;
	}

	public String verifysavebutton() throws Exception {
		String textButtonemailpage = BrowserActions.getText(driver, btnSavePreference, "Save Preferences");
		return textButtonemailpage;
	}
	
	public void mouseOverAPOFPO() throws InterruptedException {		
		BrowserActions.scrollToView(txtZipCode, driver);
		BrowserActions.mouseHover(driver, lnkApoFpo);
		BrowserActions.nap(3);
	}	
	
	public ApoFpoPage navigateToApoFpoPage() throws Exception{
		
		//BrowserActions.clickOnElement(lnkApoFpo, driver, "clicking ApoFpo tooltip");
		String runPltfrm = Utils.getRunPlatForm();
		if(runPltfrm == "mobile")
		{
		BrowserActions.javascriptClick(lnkApoFpo, driver, "clicking ApoFpo tooltip");
		BrowserActions.nap(1);
		BrowserActions.javascriptClick(lnkApoFpo, driver, "clicking ApoFpo tooltip");
		BrowserActions.nap(1);
	    }else{
	    	mouseOverAPOFPO();
	    	BrowserActions.javascriptClick(lnkApoFpo, driver, "clicking ApoFpo tooltip");
			BrowserActions.nap(1);
	    }
		//Utils.waitForPageLoad(driver);
		//BrowserActions.clickOnElement(lnkApoFpo, driver, "clicking ApoFpo tooltip");
		return new ApoFpoPage(driver).get();
	}
	
	public void mouseOverWHYISREQ() {                			
      BrowserActions.mouseHover(driver, lnkWhyIsReq);     
      BrowserActions.nap(3);
       } 
	
	 public WhyIsRequiredPage navigateToWhyIsRequiredPage() throws Exception{			
		mouseOverWHYISREQ();
		String runPltfrm = Utils.getRunPlatForm();
		if(runPltfrm == "mobile")
		{
		BrowserActions.scrollToView(lnkWhyIsReq, driver);
		BrowserActions.javascriptClick(lnkWhyIsReq, driver, "clicking Why Is Required tooltip");
		BrowserActions.nap(1);
		BrowserActions.javascriptClick(lnkWhyIsReq, driver, "clicking Why Is Required tooltip");
		BrowserActions.nap(1);
	    }else{
	    	BrowserActions.javascriptClick(lnkWhyIsReq, driver, "clicking Why Is Required tooltip");
			BrowserActions.nap(1);
	    }
		//Utils.waitForPageLoad(driver);		
		//lnkWhyIsReq.click();
		//BrowserActions.clickOnElement(lnkWhyIsReq, driver, "clicking Why Is Required tooltip");			
		return new WhyIsRequiredPage(driver).get();			
	 }			
        			
     public void clickCancelButton() throws Exception{			
    	  BrowserActions.moveToElementJS(driver, btnCancel);			
    	  BrowserActions.clickOnElement(btnCancel, driver, "Clicking Cancel Button");			             			
    }
     
     public void clickSaveButton() throws Exception{			
   	  BrowserActions.moveToElementJS(driver, btnSavePreference);			
   	  BrowserActions.clickOnElement(btnSavePreference, driver, "Clicking Save Preference Button");			             			
   }     

     public PrivacyAndPolicyPage NavigateToPrivacyPolicy() throws Exception{
    	 BrowserActions.clickOnElement(btnPrivacyPolicy, driver, "Clicking privacy policy Button");	
    	 return new PrivacyAndPolicyPage(driver).get();
     }
     
     public void clickMobileMessage() throws Exception{
    	 BrowserActions.scrollToViewElement(lnkMobileMessage, driver);
    	 BrowserActions.clickOnElement(lnkMobileMessage, driver, "Mobile message Link");
     }
     
     public boolean verifyMobileMessageLink() throws Exception{
    	 boolean status = false;
    	 String expectedHref = "sms:+23551;?&body=JOIN%20to%20BELK1";
    	 String actualHref = BrowserActions.getTextFromAttribute(driver, lnkMobileMessage, "href", "Mobile message link");
    	 if(actualHref.equals(expectedHref)) {
    		 status = true;
    	 }
    	 return status;
     }
     
     public UnsubscribeEmailpage navigateToUnSubscribeLink() throws Exception{
    	 BrowserActions.scrollToViewElement(btnEmailUnsubscribe, driver);
    	 BrowserActions.clickOnElement(btnEmailUnsubscribe, driver, "Clicking UnSubscribe Button");	
    	 return new UnsubscribeEmailpage(driver).get();
     }
     
     public TermsAndConditionsPage NavigateToTermsAndCondition() throws Exception{
    	 BrowserActions.clickOnElement(btnTermsAndUse, driver, "Clicking terms and condition Button");	
    	 return new TermsAndConditionsPage(driver).get();
     }   
     
     /**
 	 * To search any product with clicking 'Enter' Key in the keyboard
 	 * The parameterized product name
 	 * 
 	 * @param String txtTo Search
 	 * @throws Exception
 	 */	
 	public void toolTipApoFpoOverlayClickESC() throws Exception {		
 		lnkApoFpo.sendKeys(Keys.ESCAPE);
// 		BrowserActions.mouseHover(driver, lblGender);
// 		BrowserActions.nap(1);
// 		mouseOverAPOFPO(); 	
// 		BrowserActions.nap(1);
 	}
 	
 	/**
 	 * To search any product with clicking 'Enter' Key in the keyboard
 	 * The parameterized product name
 	 * 
 	 * @param String txtTo Search
 	 * @throws Exception
 	 */	
 	public void mouseOutFromApoFpo() throws Exception {		
 		BrowserActions.mouseHover(driver, txtCity);
 	}
 	
 	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = txtProductInBreadcrumbMobile;
		} else {
			lstElement = lstTxtProductInBreadcrumb;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}
 	
	public LinkedHashMap<String, String> fillingPreferencesDetails(String useAddressForShipping, String genderType,
			String birthday) throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String month = birthday.split("-")[0];
		String date = birthday.split("-")[1];
		String year = birthday.split("-")[2];
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaFirst" + randomFirstName, txtFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_lastname_QaLast" + randomLastName, txtLastname);

		String address = checkoutProperty.getProperty(useAddressForShipping);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if (useAddressForShipping.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}

		shippingDetails.put("type_address_" + address1, txtAddressOne);
		if (useAddressForShipping.contains("_1"))
			shippingDetails.put("type_address1_" + address2, txtAddressTwo);
		shippingDetails.put("type_city_" + city, txtCityField);
		shippingDetails.put("select_state_" + state, chkState);
		shippingDetails.put("type_zipcode_" + zipcode, txtZipcode);
		shippingDetails.put("select_gender_" + genderType, drpGender);
		shippingDetails.put("select_month_" + month, drpBdayMonth);
		shippingDetails.put("select_day_" + date, drpBdayDay);
		shippingDetails.put("select_year_" + year, drpBdayYear);
		shippingDetails.put("type_phoneno_" + phoneNo, txtPhoneNo);
		shippingDetails.put("click_savepreferencesbutton_", btnSavePreferences);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		Utils.waitForPageLoad(driver);
		return shippingDetails;

	}
}
