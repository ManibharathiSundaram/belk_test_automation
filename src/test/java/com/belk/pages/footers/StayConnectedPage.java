package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class StayConnectedPage extends LoadableComponent <StayConnectedPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer; 
	
	@FindBy(css = ".hide-mobile .breadcrumb-element")
	List<WebElement> lstTxtInBreadCrumbInDesktop;
	
	@FindBy(css = ".breadcrum-device.hide-desktop a")
	List<WebElement> lstTxtInBreadCrumbInMobile;
	
	@FindBy(css = "div#primary>div>div")
	WebElement primaryContent;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public StayConnectedPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, lstTxtInBreadCrumbInDesktop.get(0)) || Utils.waitForElement(driver, lstTxtInBreadCrumbInMobile.get(0)))) {
			Log.fail("Stay connected page is not displayed", driver);
		
		}
		elementLayer = new ElementLayer(driver);
	}
	
	/**
	 * To get a text from stay Connected Page 
	 * 
	 * @return: String - text from stayConnected Page 
	 * @throws Exception 
	 */
	public ArrayList<String> getTextFromstayConnectedPage() throws Exception {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = lstTxtInBreadCrumbInMobile;
		} else {
			lstElement = lstTxtInBreadCrumbInDesktop;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;

	}// getTextFromstayConnectedPage

}
