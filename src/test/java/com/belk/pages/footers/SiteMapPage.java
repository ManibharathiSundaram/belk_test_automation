package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class SiteMapPage extends LoadableComponent<SiteMapPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	@FindBy(css = "div.breadcrumb a[class='breadcrumb-element last-element")
	WebElement bcSiteMap;
	
	@FindBy(css = "div#primary")
	WebElement siteContent;
	
	@FindBy(css ="div.secondary-navigation>span")
	WebElement btnCustomerService;
	
	@FindBy(css ="div.account-nav-asset div[class='need-data'] h3")
	WebElement btnNeedHelp;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException
	 */
	public SiteMapPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}
	
	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, siteContent))) {
			Log.fail("SiteMape page didn't open up", driver);
		}	
		elementLayer = new ElementLayer(driver);
	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public List<String> getL2Category(String level1) throws Exception{
		List<String> dataToBeReturned = new ArrayList<String>();
		List<WebElement> l1category = BrowserActions.checkLocators(driver, ".site-heading:not([class*='clearance'])>span>a");
		WebElement l1element=BrowserActions.getMachingTextElementFromList(l1category, level1, "contains");
		List<WebElement> l2category=l1element.findElements(By.xpath("..//following-sibling::ul/li/a"));
		
		for(WebElement l2 : l2category){
			dataToBeReturned.add(BrowserActions.getText(driver, l2, "L2 Category"));
		}
		Log.event(level1 + "and its l2 category "+dataToBeReturned);
		return dataToBeReturned;		
	}
	

}
