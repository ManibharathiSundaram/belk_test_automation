package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class ApoFpoPage extends LoadableComponent <ApoFpoPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public static final String title="apo-fpo";
	
	/**********************************************************************************************
	 ********************************* WebElements of PDP Page ***********************************
	 **********************************************************************************************/
	
	@FindBy(css = "div#primary")
	WebElement ApoFpoContent;
	
	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;
	
	@FindBy(css = ".breadcrum-device a")
	List<WebElement> txtProductInBreadcrumbMobile;

	/**********************************************************************************************
	 ********************************* WebElements of PDP Page - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public ApoFpoPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, ApoFpoContent))) {
			Log.fail("ApoFpo did not open up. Site might be down.", driver);
		}
		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,ApoFpoContent );
		
	}
	
	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = txtProductInBreadcrumbMobile;
		} else {
			lstElement = lstTxtProductInBreadcrumb;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}
}
