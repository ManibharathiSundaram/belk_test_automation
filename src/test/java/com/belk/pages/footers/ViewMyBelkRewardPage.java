package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class ViewMyBelkRewardPage extends LoadableComponent <ViewMyBelkRewardPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	
	public static final String BELK_REWARDS_BENEFITS_CONTENT_STRING="Belk Rewards Benefits";
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Go to Belk Rewards Benefits']")
	static WebElement bcBelkRewardsBenefits;
	
	@FindBy(css = ".breadcrum-device a[title='Back to customer service']")
	WebElement bcBelkRewardsBenefitsInMobile;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public ViewMyBelkRewardPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	

	@Override
	protected void isLoaded() throws Error {
		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, bcBelkRewardsBenefits)||Utils.waitForElement(driver, bcBelkRewardsBenefitsInMobile))) {
			Log.fail("Belk credit card page not open up. Site might be down.", driver);
		}

	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

		try {
			Utils.switchWindows(driver, "Home Page", "title", "false");
		}
		catch (Exception e) {
		}

		Utils.waitForElement(driver, bcBelkRewardsBenefits);
		
	}
	
	/**
	 * To get a text from Belk Reward Page	
	 * 
	 * @return: String - text from Belk Reward Page 
	 * @throws Exception 
	 */
	public String getTextFromBelkRewardsPage() throws Exception {
		String textBCBelkRewards = BrowserActions.getText(driver, bcBelkRewardsBenefits, "Belk reward Page");
		return textBCBelkRewards;
	}//getTextFromBelkRewardsPage

}
