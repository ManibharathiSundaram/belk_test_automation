package com.belk.pages.footers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.footers.socialmediapages.FacebookPage;
import com.belk.pages.footers.socialmediapages.GooglePlusPage;
import com.belk.pages.footers.socialmediapages.InstagramPage;
import com.belk.pages.footers.socialmediapages.PintrestPage;
import com.belk.pages.footers.socialmediapages.TwitterPage;
import com.belk.pages.footers.socialmediapages.YoutubePage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;
import com.gargoylesoftware.htmlunit.javascript.host.media.webkitAudioContext;

public class Footers extends LoadableComponent<Footers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of Footers ***********************************
	 **********************************************************************************************/

	private static final String CUSTOMER_SERVICE = "div.footer-item1.footer-item-links";
	private static final String REWARD_CARD = "div.footer-item2.footer-item-links";
	private static final String ABOUT_BELK = "div.footer-item3.footer-item-links";
	private static final String App = "div.footer-item4.footer-app-links";
	private static final String SOCIAL_MEDIA = "div.footer-social:not([class*='hide'])";
	private static final String FOOTER_EMAIL = "div.footer-email";

	private static final String FOOTER_BOTTOM_LINKS = ".footer-bottom-links";
	String runPltfrm = Utils.getRunPlatForm();

	@FindBy(id = "footer")
	WebElement footerPanel;

	@FindBy(css = CUSTOMER_SERVICE
			+ " a[title='Customer Service']:not([href^='#'])")
	WebElement lnkCustomerService;

	@FindBy(css = CUSTOMER_SERVICE + " h3[class='active']")
	public WebElement expandCustomerService;

	@FindBy(css = REWARD_CARD + " h3[class='active']")
	public WebElement expandBelkRewardCard;

	@FindBy(css = ABOUT_BELK + " h3[class='active']")
	public WebElement expandAboutBelk;

	@FindBy(css = CUSTOMER_SERVICE + " h3")
	public WebElement closedCustomerService;

	@FindBy(css = REWARD_CARD + " h3")
	public WebElement closedBelkRewardCard;

	@FindBy(css = ABOUT_BELK + " h3")
	public WebElement closedAboutBelk;

	@FindBy(css = CUSTOMER_SERVICE + " a[title='Policy & Guidelines']")
	WebElement lnkPolicyGuideLines;

	@FindBy(css = CUSTOMER_SERVICE
			+ " div.footer-item1.footer-item-links div>ul>li:nth-child(1)>a")
	WebElement lnkTrackYourOrder;

	@FindBy(css = CUSTOMER_SERVICE + " div>ul>li:nth-child(1)>a")
	WebElement lnkTrackYourOrderOriginal;

	@FindBy(css = CUSTOMER_SERVICE
			+ " div>ul>li:nth-child(2)>a:not([href^='#'])")
	WebElement lnkShippingInFormation;

	@FindBy(css = CUSTOMER_SERVICE + " div>ul>li:nth-child(2)>a")
	WebElement lnkShippingInFormationOriginal;

	@FindBy(css = CUSTOMER_SERVICE + " a[title='FAQs']:not([href^='#'])")
	WebElement lnkFQAS;

	@FindBy(css = CUSTOMER_SERVICE
			+ " div>ul>li:nth-child(6)>a:not([href^='#'])")
	WebElement lnkContact;

	@FindBy(css = CUSTOMER_SERVICE + " div>ul>li:nth-child(6)>a")
	WebElement lnkContactOriginal;

	@FindBy(css = CUSTOMER_SERVICE
			+ " a[title='Store Locations']:not([href^='#'])")
	WebElement lnkStoreLocation;

	@FindBy(css = REWARD_CARD
			+ " a[title='Check Available Balance']:not([href^='#'])")
	WebElement lnkBlekRewardCard;

	@FindBy(css = REWARD_CARD
			+ " a[title='Apply for a Belk Credit Card']:not([href^='#'])")
	WebElement lnkApplyCreditCard;

	@FindBy(css = REWARD_CARD
			+ " a[title='Pay Your Bill Online']:not([href^='#'])")
	WebElement lnkPayYourBill;

	@FindBy(css = REWARD_CARD
			+ " a[title='Check Available Balance']:not([href^='#'])")
	WebElement lnkCheckAvailableBalance;

	@FindBy(css = REWARD_CARD
			+ " a[title='Visit My Belk Rewards']:not([href^='#'])")
	WebElement lnkVisitBelkRewards;

	@FindBy(css = ABOUT_BELK
			+ " .content-asset a[href*='aboutbelk']:not([href^='#'])")
	WebElement lnkAboutBelk;

	@FindBy(css = ABOUT_BELK + " li:nth-child(1)>a:not([href^='#'])")
	WebElement lnkViewOurAds;

	@FindBy(css = ABOUT_BELK + " li:nth-child(1)>a")
	WebElement lnkViewOurAdsOriginal;

	@FindBy(css = ABOUT_BELK + " li:nth-child(2)>a:not([href^='#'])")
	WebElement lnkListOfBrands;

	@FindBy(css = ABOUT_BELK + " li:nth-child(3)>a:not([href^='#'])")
	WebElement lnkCareersAtBelk;

	@FindBy(css = ABOUT_BELK + " li:nth-child(3)>a")
	WebElement lnkCareersAtBelkOriginal;

	@FindBy(css = ABOUT_BELK + " li:nth-child(4)>a:not([href^='#'])")
	WebElement lnkVendorInformation;

	@FindBy(css = ABOUT_BELK + " li:nth-child(4)>a")
	WebElement lnkVendorInformationOriginal;

	@FindBy(css = ABOUT_BELK + " li:nth-child(5)>a:not([href^='#'])")
	WebElement lnkCorporateInformation;

	@FindBy(css = ABOUT_BELK + " li:nth-child(5)>a")
	WebElement lnkCorporateInformationOriginal;

	@FindBy(css = App + " a[title='Download Our App']:not([href^='#'])")
	WebElement lnkdownloadApp;

	@FindBy(css = App + " a[title=iOS]:not([href^='#'])")
	WebElement lnkIOs;

	@FindBy(css = App + " a[title='Android']:not([href^='#'])")
	WebElement lnkAndriod;

	@FindBy(css = SOCIAL_MEDIA)
	WebElement lnkSocialMedia;

	@FindBy(css = SOCIAL_MEDIA + " a[title='Stay Connected']:not([href^='#'])")
	WebElement lnkStayConnect;

	@FindBy(css = SOCIAL_MEDIA + " a[class='footer-facebook']:not([href^='#'])")
	WebElement lnkfacebook;

	@FindBy(css = SOCIAL_MEDIA + " a[class='footer-twitter']:not([href^='#'])")
	WebElement lnkTwitter;

	@FindBy(css = SOCIAL_MEDIA + " a[class='footer-pintrest']:not([href^='#'])")
	WebElement lnkPrintrest;

	@FindBy(css = SOCIAL_MEDIA
			+ " a[class='footer-instagram']:not([href^='#'])")
	WebElement lnkInstagram;

	@FindBy(css = SOCIAL_MEDIA + " a[class='footer-youtube']:not([href^='#'])")
	WebElement lnkYouTube;

	@FindBy(css = SOCIAL_MEDIA + " a[class='footer-google ']")
	WebElement lnkGooglePlus;

	@FindBy(css = FOOTER_EMAIL
			+ " label[for='email-alert-address']:not([href^='#'])")
	WebElement signUP;

	@FindBy(css = FOOTER_EMAIL + " input[name='email-address']")
	WebElement txtEmail;

	@FindBy(css = FOOTER_EMAIL + " button[name='home-email']:not([href^='#'])")
	WebElement btnJoin;

	@FindBy(css = "span[class*='error-text error']")
	WebElement txtEmailError;

	@FindBy(css = "div[class='footer-legal-msg'] div[class='html-slot-container']")
	WebElement legalMessage;

	@FindBy(css = "div[class='footer-bottom-call']")
	WebElement phoneNo;

	@FindBy(css = FOOTER_BOTTOM_LINKS
			+ " a[href$='/terms.html']:not([href^='#'])")
	WebElement lnkTerms;

	@FindBy(css = FOOTER_BOTTOM_LINKS + " a[href$='=terms']")
	WebElement lnkTermsOriginal;

	@FindBy(css = FOOTER_BOTTOM_LINKS
			+ " a[href$='privacy-policy']:not([href^='#'])")
	WebElement lnkPrivacy;

	@FindBy(css = FOOTER_BOTTOM_LINKS
			+ " a[href$='privacy-policy.html']:not([href^='#'])")
	WebElement lnkPrivacyOriginal;

	@FindBy(css = "div[class='footer-bottom-links']>span:nth-child(2)>a:nth-child(2)")
	WebElement btnPageFeedBack;

	@FindBy(css = "div[class='footer-bottom-links']>span:nth-child(2)>a:nth-child(1)")
	WebElement btnSiteMap;

	@FindBy(css = "div.footer-bottom-call>a")
	WebElement btnOrderByCall;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 */
	public Footers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, lnkCustomerService))) {
			Log.fail("Footer Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public void scrollToFooter() throws InterruptedException {
		BrowserActions.scrollToViewElement(lnkCustomerService, driver);
	}

	public void clickCustomerServiceLink() throws Exception {
		BrowserActions.clickOnElement(lnkCustomerService, driver,
				"Customer service");
	}

	public void clickBelkRewardCards() throws Exception {
		BrowserActions.scrollToViewElement(lnkBlekRewardCard, driver);
		BrowserActions.clickOnElement(lnkBlekRewardCard, driver,
				"Belk reward card");
	}

	public void clickBelkRewardCardBalance() throws Exception {
		BrowserActions.scrollToViewElement(lnkCheckAvailableBalance, driver);
		BrowserActions.clickOnElement(lnkCheckAvailableBalance, driver,
				"Check Available Balance");
	}

	public void clickAboutBelk() throws Exception {
		BrowserActions.scrollToViewElement(lnkAboutBelk, driver);
		BrowserActions.clickOnElement(lnkAboutBelk, driver, "About Belk");
	}

	public CustomerServicePage navigateToCustomerService() throws Exception {
		BrowserActions.clickOnElement(lnkCustomerService, driver,
				"Customer service");
		return new CustomerServicePage(driver).get();
	}

	public TermsAndConditionsPage navigateToPolicyGuideLine() throws Exception {
		BrowserActions.scrollToViewElement(lnkPolicyGuideLines, driver);
		BrowserActions.clickOnElement(lnkPolicyGuideLines, driver,
				"Policy & guideline");
		return new TermsAndConditionsPage(driver).get();
	}

	public TermsAndConditionsPage navigateToTermsAndConditions()
			throws Exception {
		BrowserActions.scrollToViewElement(lnkTermsOriginal, driver);
		BrowserActions.clickOnElement(lnkTermsOriginal, driver, "Terms link");
		return new TermsAndConditionsPage(driver).get();
	}

	public PrivacyAndPolicyPage navigateToPrivacyAndPolicy() throws Exception {
		BrowserActions.clickOnElement(lnkPrivacyOriginal, driver, "Privay & Policy link");
		return new PrivacyAndPolicyPage(driver).get();
	}

	public FaqPage navigateToFAQ() throws Exception {
		BrowserActions.clickOnElement(lnkFQAS, driver, "FAQ");
		return new FaqPage(driver).get();
	}

	public void clickContactUsLink() throws Exception {
		BrowserActions.clickOnElement(lnkContactOriginal, driver, "FAQ");
	}

	public StoreLocationPage navigateToStore() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			scrollToFooter();
			BrowserActions.clickOnElement(driver.findElement(By
					.cssSelector("div.footer-item1.footer-item-links")),
					driver, "CustomerService");
			BrowserActions.javascriptClick(lnkStoreLocation, driver,
					"Store Location");

		} else {
			BrowserActions.javascriptClick(lnkStoreLocation, driver,
					"Store Location");
		}

		return new StoreLocationPage(driver).get();
	}

	public ListOfBrandPage navigateToListBrand() throws Exception {
		if (runPltfrm == "desktop") {
			scrollToFooter();
			BrowserActions.clickOnElement(lnkListOfBrands, driver,
					"List Of Brand");
		} else if (runPltfrm == "mobile") {
			scrollToFooter();
			BrowserActions.clickOnElement(lnkAboutBelk, driver, "About Belk");
			BrowserActions.clickOnElement(lnkListOfBrands, driver,
					"List of Brands");
		}
		return new ListOfBrandPage(driver).get();
	}

	public BelkRewardCardPage navigateToBelkRewardCard() throws Exception {
		BrowserActions.clickOnElement(lnkBlekRewardCard, driver,
				"Belk Reward Card");
		return new BelkRewardCardPage(driver).get();
	}

	public BelkRewardCardPage navigateToBelkRewardCardBalance()
			throws Exception {
		BrowserActions.clickOnElement(lnkCheckAvailableBalance, driver,
				"Check Available Balance");
		return new BelkRewardCardPage(driver).get();
	}

	public BelkRewardCardPage navigateToBelkPayBillOnline() throws Exception {
		BrowserActions.clickOnElement(lnkPayYourBill, driver,
				"Belk Reward Card");
		return new BelkRewardCardPage(driver).get();
	}

	public ApplyCreditCard navigateToApplyCreditCard() throws Exception {
		BrowserActions.clickOnElement(lnkApplyCreditCard, driver,
				"Belk Apply Credit Card");
		return new ApplyCreditCard(driver).get();
	}

	public ViewMyBelkRewardPage navigateToViewMyRewardCard() throws Exception {
		BrowserActions.clickOnElement(lnkVisitBelkRewards, driver,
				"Belk Reward Card");
		Utils.switchToNewWindow(driver);
		return new ViewMyBelkRewardPage(driver).get();
	}

	public AboutBelkPage navigateToAboutBelk() throws Exception {
		BrowserActions.clickOnElement(lnkAboutBelk, driver, "About Belk");
		return new AboutBelkPage(driver).get();
	}

	public GoogleStorePage navigateToGoogleStore() throws Exception {
		BrowserActions.clickOnElement(lnkAndriod, driver, "Andriod");
		return new GoogleStorePage(driver).get();
	}

	public AppleStorePage navigateToAppleStore() throws Exception {
		BrowserActions.scrollToViewElement(lnkIOs, driver);
		BrowserActions.javascriptClick(lnkIOs, driver, "Footer iOS link");
		return new AppleStorePage(driver).get();
	}

	public String getIOSLinkURL() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, lnkIOs, "href",
				"IOS link");
	}

	public StayConnectedPage navigateToStayConnected() throws Exception {
		BrowserActions.clickOnElement(lnkStayConnect, driver, "Stay connected");
		return new StayConnectedPage(driver).get();
	}

	public EmailSignupPage fillingEmailAddress(String emailid) throws Exception {
		BrowserActions.scrollToViewElement(txtEmail, driver);
		BrowserActions.typeOnTextField(txtEmail, emailid, driver,
				"Entering the vaild email address");
		BrowserActions.clickOnElement(btnJoin, driver, "Click on join button'");
		return new EmailSignupPage(driver).get();
	}

	/**
	 * To get a text from Footer page
	 * 
	 * @param txtEmailError
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Footer Page
	 * @throws Exception
	 */

	public String getTextFromEmailMessage(String emailid) throws Exception {
		BrowserActions.scrollToViewElement(txtEmail, driver);
		BrowserActions.typeOnTextField(txtEmail, emailid, driver,
				"Entering the vaild email address");
		BrowserActions.clickOnElement(btnJoin, driver, "Click on join button'");
		String txtErrorMessage = BrowserActions.getText(driver, txtEmailError,
				"Fetching the error message in the footer page");
		return txtErrorMessage;
	}

	/**
	 * To get a text from Footer page
	 * 
	 * @param legalMessage
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Footer Page
	 * @throws Exception
	 */
	public String getTextFromLegalMessage() throws Exception {
		String txtLegalMessage = BrowserActions.getText(driver, legalMessage,
				"Fetching the Legal message in the footer page");
		return txtLegalMessage;
	}

	// Click on Page Feedback link
	public void clickOnPageFeedBack() throws Exception {
		BrowserActions.scrollToViewElement(btnPageFeedBack, driver);
		BrowserActions.clickOnElement(btnPageFeedBack, driver,
				"Clicking on Page Feed Back");
	}

	// Click on Page Feedback link
	public void clickOnOrderByCall() throws Exception {
		BrowserActions.scrollToViewElement(btnOrderByCall, driver);
		BrowserActions.clickOnElement(btnOrderByCall, driver,
				"Clicking on Order By call");
	}

	public SiteMapPage navigateToSiteMap() throws Exception {
		BrowserActions.scrollToViewElement(btnSiteMap, driver);
		BrowserActions.clickOnElement(btnSiteMap, driver, "site Map");
		return new SiteMapPage(driver).get();

	}

	public FacebookPage navigateToFacebookPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkfacebook, driver);
		BrowserActions.clickOnElement(lnkfacebook, driver,
				"clicking Social Media Facebook Icon");
		Utils.waitForPageLoad(driver);
		return new FacebookPage(driver);
	}

	public TwitterPage navigateToTwitterPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkTwitter, driver);
		BrowserActions.clickOnElement(lnkTwitter, driver,
				"clicking Social Media Twitter Icon");
		return new TwitterPage(driver);
	}

	public InstagramPage navigateToInstagramPage() throws Exception {

		BrowserActions.clickOnElement(lnkInstagram, driver,
				"clicking Social Media Instagram Icon");
		return new InstagramPage(driver);
	}

	public YoutubePage navigateToYoutubePage() throws Exception {

		BrowserActions.clickOnElement(lnkYouTube, driver,
				"clicking Social Media Youtube Icon");
		return new YoutubePage(driver);
	}

	public GooglePlusPage navigateToGooglePlusPage() throws Exception {

		BrowserActions.clickOnElement(lnkGooglePlus, driver,
				"clicking Social Media GooglePlus Icon");
		return new GooglePlusPage(driver);
	}

	public PintrestPage navigateToPinterestPage() throws Exception {

		BrowserActions.clickOnElement(lnkPrintrest, driver,
				"clicking Social Media Pinterest Icon");
		return new PintrestPage(driver);
	}

}
