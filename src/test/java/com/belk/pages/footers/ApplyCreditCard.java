package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.Log;
import com.belk.support.Utils;



public class ApplyCreditCard extends LoadableComponent <ApplyCreditCard> {
	
	
	private WebDriver driver;
	private boolean isPageLoaded;

	//final static String UrlCredit ="https://www.onlinecreditcenter2.com/eapplygen2/load.do?cHash=1315641838&subActionId=1000&langId=en";
	@FindBy(css = "td[class='app_abtyou_left_col'] span[class='app_textbox_comment']")
	WebElement Emailindicate;
	
	@FindBy(css ="#valPropHomeDetails > u")
    WebElement lnkTapforDetails;

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public ApplyCreditCard(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}	

	protected void isLoaded() throws Error {
        Utils.waitForPageLoad(driver);
        String runPltfrm = Utils.getRunPlatForm(); 
        if (!isPageLoaded) {
               Assert.fail();
        }      
        if(runPltfrm == "desktop"){
               if (isPageLoaded && !(Utils.waitForElement(driver, Emailindicate))){
                     Log.fail("Apply Belk credit card page did not open up.", driver);          
               }
        }
        else{
               if (isPageLoaded && !(Utils.waitForElement(driver, lnkTapforDetails))){
                     Log.fail("Apply Belk credit card page did not open up.", driver);          
               }
        }
	}

	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		try {
			Utils.switchWindows(driver, "Home Page", "title", "false");
		}
		catch (Exception e) {
		}
		Utils.waitForElement(driver, Emailindicate);
		
	}
}
