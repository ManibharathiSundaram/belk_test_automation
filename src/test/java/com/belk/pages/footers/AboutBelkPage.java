package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class AboutBelkPage extends LoadableComponent <AboutBelkPage>{
	

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	@FindBy(css = "div#primary")
	WebElement bcAboutBelk;
	
	@FindBy(css = "div#primary h1")
	WebElement lblErrorText;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public AboutBelkPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}


	protected void isLoaded() throws Error {
		
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, bcAboutBelk)))
			Log.fail("About Belk Page is not open", driver);
		elementLayer = new ElementLayer(driver);
	}
		

	protected void load() {
			isPageLoaded = true;
			Utils.waitForPageLoad(driver);	

	}
	
	/**
	 * To get a text from Belk Reward Page	
	 * 
	 * @return: String - text from Belk Reward Page 
	 * @throws Exception 
	 */
	public String getTextFromAboutPage() throws Exception {
		String textBCaboutBelk= BrowserActions.getText(driver, lblErrorText, "About Belk Page");
		return textBCaboutBelk;
	}//getTextFromAboutPage

}
