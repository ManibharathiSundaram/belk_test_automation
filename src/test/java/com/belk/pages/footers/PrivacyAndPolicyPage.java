package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;



public class PrivacyAndPolicyPage extends LoadableComponent <PrivacyAndPolicyPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of Privacy & Policy Page **************************
	 **********************************************************************************************/
	public static final String PRIVACY_POLICY_CONTENT_HEADER ="Privacy Policy";
	
	@FindBy(css = "div#primary")
	WebElement privacyContent;
	
	@FindBy(css = ".content-asset>h1")
	WebElement contentHeader;
	
	@FindBy(css = ".hide-mobile .breadcrumb-element")
	List<WebElement> lstTxtInBreadCrumbInDesktop;

	@FindBy(css = ".breadcrum-device a")
	List<WebElement> lstTxtInBreadCrumbInMobile;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public PrivacyAndPolicyPage(WebDriver driver) {		
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {   
		
		if (!isPageLoaded) {
			Assert.fail();
		}		

		if (isPageLoaded && !(Utils.waitForElement(driver, lstTxtInBreadCrumbInDesktop.get(0)) || Utils.waitForElement(driver, lstTxtInBreadCrumbInMobile.get(0)))) {
			Log.fail("Privacy & Policy page did not open up.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {		

		isPageLoaded = true;	
		Utils.waitForPageLoad(driver);
	}// load	
	
	public String getContentHeader() throws Exception{
		String contentHeaderText= null;
		contentHeaderText=BrowserActions.getText(driver, contentHeader, "Terms & Condition content header");
		return contentHeaderText;		
	}
	
	/**
	 * To get a text from stay Connected Page 
	 * 
	 * @return: String - text from stayConnected Page 
	 * @throws Exception 
	 */
	public ArrayList<String> getTextFromBreadcrumb() throws Exception {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = lstTxtInBreadCrumbInMobile;
		} else {
			lstElement = lstTxtInBreadCrumbInDesktop;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;

	}

}// GiftRegistryPage
