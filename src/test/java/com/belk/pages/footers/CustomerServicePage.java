package com.belk.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class CustomerServicePage extends LoadableComponent <CustomerServicePage>{
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Customer Service']")
	WebElement btnCustomerservices;
	
	@FindBy(css = "div#primary")
	WebElement custmerServicesPrimaryContent;
	
	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtInBreadCrumb;
	
	@FindBy(css = "div[class='refinement Folder folder-refinement']>ul>li:nth-child(1)")
	WebElement lnkPolicyGuidelines;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public CustomerServicePage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}


	protected void isLoaded() {
		
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, lstTxtInBreadCrumb.get(1)))) {
			Log.fail("Customer Services page didn't open up", driver);
		}	
		elementLayer = new ElementLayer(driver);
	}


	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	

	}
	
	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		for (WebElement element : lstTxtInBreadCrumb) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}
	
	/**
	 * to navigate to policy and guidelines link
	 * @return
	 * @throws Exception
	 */
	public PolicyGuideLinePage navigateToPolicyGuidelinePage() throws Exception {
		BrowserActions.clickOnElement(lnkPolicyGuidelines, driver,
				"Customer service");
		return new PolicyGuideLinePage(driver).get();
	}
}
