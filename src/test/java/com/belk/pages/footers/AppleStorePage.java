package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.Log;
import com.belk.support.Utils;

public class AppleStorePage extends LoadableComponent <AppleStorePage>{
	private WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(css =  "div[class='center-stack'] div[metrics-loc='Titledbox_iPhone Apps']")
	WebElement imgiPhoneBelk;
	
	public AppleStorePage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	} 

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

		try {
			Utils.switchWindows(driver, "Belk, Inc. Apps on the App Store", "title", "false");
		}
		catch (Exception e) {
		}

		Utils.waitForElement(driver, imgiPhoneBelk);
		
	}

	@Override
	protected void isLoaded() {
		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, imgiPhoneBelk))) {
			Log.fail("Apple store page not open up. Site might be down.", driver);
		}
		
	}

}
