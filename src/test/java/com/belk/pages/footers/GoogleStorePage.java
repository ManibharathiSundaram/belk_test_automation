package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.Log;
import com.belk.support.Utils;

public class GoogleStorePage extends LoadableComponent <GoogleStorePage>{
	private WebDriver driver;
	private boolean isPageLoaded;
	public static final String URL = "https://play.google.com/store/apps/details?id=com.belk.android.belk&hl=en";
	
	@FindBy(css ="div[class='details-info'] div[class='cover-container']")
	WebElement imgBelk;
	
	public GoogleStorePage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	
	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
		Utils.waitForPageLoad(driver);
		
		try {
			Utils.switchWindows(driver, URL, "url", "false");
		}
		catch (Exception e) {
		}
		
		if (isPageLoaded && !(Utils.waitForElement(driver, imgBelk))) {
			Log.fail("Google Play Store page not open up. Site might be down.", driver);
		}		
	}
	
	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;			
	}
}
