package com.belk.pages.footers;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class PolicyGuideLinePage extends LoadableComponent <PolicyGuideLinePage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Go to Terms & Conditions of Sale']")
	static WebElement bcPoliy;
	

    @FindBy(css = ".breadcrumb")
    WebElement lstTxtInBreadCrumb;
    
    @FindBy(css = "ul[class='folder-content-list']>li a:not([class='content-title'])")
    List<WebElement> lnkReadMore; 
    
    @FindBy(css = "div[class='back'] a")
	WebElement lnkBackToResult;
    
    @FindBy(css="div[class='content-asset']>h1")
	WebElement lblHeading;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public PolicyGuideLinePage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	
	public String getTextOfHeading() throws Exception{
		return BrowserActions.getText(driver, lblHeading, "Get Text of the Heading");
		
	}


	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, bcPoliy))) {
			Log.fail("Stay connected page is not display", driver);
		
		}
		
	}
	
	/**
	 * To get a text from Policy Guideline Page 
	 * 
	 * @param bcPoliy
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Policy Guideline Page 
	 * @throws Exception 
	 */
	final public static String getTextFromPolicyPage(WebDriver driver) throws Exception {
		String textbreadCrumbPolicy = BrowserActions.getText(driver, bcPoliy, "Policy Page");
		return textbreadCrumbPolicy;

	}// getTextFromPolicyPage
	
	public String getTextFromBreadCrumbInDesktop() throws Exception {
		String breadcrumb = lstTxtInBreadCrumb.getText().trim();
	    return breadcrumb;
	}
	
	/**
	 * to click on readmore link
	 * @param index
	 * @throws Exception
	 */
	public void clickOnReadMoreLink(int index) throws Exception{
		BrowserActions.scrollToView(lnkReadMore.get(index), driver);
		BrowserActions.clickOnElement(lnkReadMore.get(index), driver, "Clicked On ReadMore link");
	}
	
	public void clickOnBackToResults() throws Exception{
		BrowserActions.clickOnElement(lnkBackToResult, driver, "Clicked on Back To Results Link");
	}

}
