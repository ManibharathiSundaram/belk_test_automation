package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class FaqPage extends LoadableComponent <FaqPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	public static final String FAQS_CONTENT_NAME="FAQs";
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Go to FAQs']")
	WebElement bcFAQS;
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Back to customer service']")
	WebElement bcFAQSMobile;
	
	@FindBy(css = "div#primary")
	WebElement fqaPrimaryContent;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public FaqPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}	
		if (isPageLoaded && !((Utils.waitForElement(driver, bcFAQS) || Utils.waitForElement(driver, bcFAQSMobile)))) {
			Log.fail("FAQ page is not display", driver);		
		}		
		elementLayer = new ElementLayer(driver);
	}	

	/**
	 * To get a text from FAQ Page 
	 * 
	 * @param bcPoliy
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from FAQ Page 
	 * @throws Exception 
	 */
	public String getTextFromFAQSPage() throws Exception {
		String textbreadFAQ = BrowserActions.getText(driver, bcFAQS, "FAQS Page");
		return textbreadFAQ;

	}// getTextFromFAQSPage


}
