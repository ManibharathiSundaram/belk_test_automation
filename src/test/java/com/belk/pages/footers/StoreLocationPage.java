package com.belk.pages.footers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class StoreLocationPage extends LoadableComponent <StoreLocationPage> {
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	public static final String STORES_CONTENT_STRING="Store Locator";
	
	@FindBy(css = "div[class='breadcrumb'] a[title='Go to Store Locator']")
	WebElement bcStore;
	
	@FindBy(css = ".pt_store-locator")
	WebElement readyElement;
	
	@FindBy(css = ".breadcrum-device.hide-desktop a")
	WebElement txtBreadCrumb;
	
	@FindBy(css = "#dwfrm_storelocator_postalCode")
	WebElement txtZipCode;
	
	@FindBy(css = ".searchstore-button")
	WebElement searchButton;
	
	@FindBy(css = ".store-results.full-width")
	WebElement storeContentPage;
	

	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException 
	 */
	public StoreLocationPage(WebDriver driver){
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
	
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Store Location page is not displayed", driver);
		
		}
		elementLayer = new ElementLayer(driver);
		
	}
	

	/**
	 * To get a text from Store Location Page 
	 * 
	 * @return: boolean 
	 * @throws Exception 
	 */
	public boolean verifyBreadcrumb() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		String textbreadFAQ = null;
		
		boolean status = false;
		if (runPltfrm == "desktop") {
			textbreadFAQ = BrowserActions
					.getText(driver, bcStore, "Store Page");
			if(textbreadFAQ.equals(STORES_CONTENT_STRING)) {
				status = true;
			}
		} else if (runPltfrm == "mobile") {
			if (txtBreadCrumb.getText().equals("Back to Home")) {
				status = true;
			}
		}
		return status;

	}//getTextFromStorePage
	
	public void enterZipCode(String txtZipSearch) throws Exception {
		BrowserActions.typeOnTextField(txtZipCode, txtZipSearch, driver, "Enter Zipcode");
	}

	public void clickOnSearchButton() throws Exception{
		Utils.waitForElement(driver, searchButton);
		BrowserActions.javascriptClick(searchButton, driver, "searchButton");
	}
}
