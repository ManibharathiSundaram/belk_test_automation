package com.belk.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class CategoryLandingPage extends LoadableComponent<CategoryLandingPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of CategoryLandingPage - Ends ****************************
	 **********************************************************************************************/

	// --------- LeftNav Section ---------- //
	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lnkBreadcrumbInmobile;

	@FindBy(css = ".toggle-grid>i")
	WebElement fldToggleView;
	
	@FindBy(css = ".breadcrumb.hide-mobile>h2>a")
	List<WebElement> breadcrumbLnk;

	@FindBy(css = "#category-level-1>li>h4>a")
	List<WebElement> l4Category;

	@FindBy(css = "div[class='search-result-options last']>div[class='backtotop']>a")
	WebElement txtBacktoTop;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleListBottom;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleGridBottom;

	@FindBy(css = "ul[id='search-result-items']>li")
	public List<WebElement> totalProducts;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(css = ".breadcrumb-refinement-value")
	List<WebElement> txtBreadCrumbValue;

	@FindBy(css = "span[data-divider='and']")
	WebElement txtRefinementBreadCrumb;

	@FindBy(css = ".breadcrumb span[class*='breadcrumb']")
	List<WebElement> txtSortByRefinement;

	@FindBy(css = "span[class='breadcrumb-element breadcrumb-result-text']")
	WebElement txtSearchResultInBreadcrumb;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement bcLastCategoryName;

	@FindBy(css = ".backtorefine>a")
	WebElement btnBackarrowLeftNav;

	@FindBy(className = "pt_categorylanding")
	WebElement readyElement;

	@FindBy(css = "div[id='secondary']")
	WebElement leftNavPanel;

	@FindBy(css = "#secondary .refinement,div[class='filterby-refinement hide-desktop']")
	List<WebElement> lstLeftNavRefinement;

	@FindBy(css = "div[class='refinement   category-refinement'] >ul>li>h4>a")
	public List<WebElement> lstCategoryRefinement;
	// --------- BreadCrumb Section ---------- //

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	List<WebElement> txtProductInBreadcrumbMobile;

	// -------------- Filter By section ---------- //
	@FindBy(css = "div[class='filterby-refinement hide-desktop'] a")
	WebElement btnFilterByMobile;

	@FindBy(css = ".refinement")
	List<WebElement> lstRefinementFilterOptions;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement iconCloseMenu;

	@FindBy(css = "div[id='secondary'] div[class*='refinement refinementSize'] h3")
	WebElement txtSize;

	@FindBy(css = "div.size>h3>i")
	WebElement btnSizetoggle;

	@FindBy(css = ".loader[style*='block']")
	WebElement spinner;

	@FindBy(css = ".refinement-header")
	WebElement textCatagoryrefinement;

	@FindBy(css = "div:not([class*='last'])>.sort-by .custom-select")
	WebElement drpSortBy;

	@FindBy(css = ".last > div.sort-by")
	WebElement drpSortbyBottom;

	@FindBy(css = "div:not([class*='last'])>.sort-by .selection-list li")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "div[class*='last']>.sort-by .selection-list li")
	List<WebElement> lstSortByOptionsBottom;

	@FindBy(css = ".breadcrumb-refinement")
	WebElement txtRefinementValueInBreadcrumb;

	@FindBy(css = "div[class='breadcrumb hide-mobile'] a[href*='home']")
	List<WebElement> txtBreadCrumbHome;

	@FindBy(css = "div[class*='filterby-refinement']>a")
	WebElement btnMobileFilterBy;

	@FindBy(css = ".breadcrumb-refinement")
	WebElement lblBreadcrumbRefinement;

	@FindBy(css = ".header-search #q")
	WebElement txtSearch;

	@FindBy(css = "form[name='simpleSearch'] button")
	WebElement btnSearch;

	@FindBy(css = "ul[id='category-level-1']>li>a[data-layout='categoryproducthits']")
	public List<WebElement> l2refinementheaders;

	@FindBy(css = "a.thumb-link")
	List<WebElement> lstProductImages;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	@FindBy(css = "div[class*='quick-view']")
	WebElement qvStyle;

	@FindBy(css = ".loader[style*='block']")
	WebElement searchResultsSpinner;

	@FindBy(css = "ul[id='search-result-items'] li .product-name>a")
	List<WebElement> lstProducts;

	@FindBy(css = "#category-level-1 > li:nth-child(1) > h4 > a")
	WebElement category1InL2OfWomen;

	@FindBy(css = "#category-level-1 > li:nth-child(2) > h4 > a")
	WebElement category2InL2OfWomen;

	@FindBy(css = "#category-level-1 > li:nth-child(3) > h4 > a")
	WebElement category3InL2OfWomen;

	@FindBy(css = "#category-level-1 > li:nth-child(4) > h4 > a")
	WebElement category4InL2OfWomen;

	@FindBy(css = "#category-level-1 > li:nth-child(3) > h4 > a > i")
	WebElement btnToggle;

	@FindBy(css = "#secondary > div.refinement.refinementSize > ul > li:nth-child(1) > a > i")
	WebElement firstSize;

	@FindBy(css = "div[class='refinement   category-refinement'] #category-level-1 li[class='expandable toggle-down'] a")
	List<WebElement> lstL2Sub_Refinements;

	@FindBy(css = "a.thumb-link>picture>img")
	List<WebElement> lstImgPrimary;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement IconCloseMenu;

	@FindBy(css = "div ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> chkRefinMentActive;

	@FindBy(css = "#main")
	WebElement pageSource;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public CategoryLandingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();

	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Category Landing Page did not open up", driver);
		}
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = txtProductInBreadcrumbMobile;
		} else {
			lstElement = lstTxtProductInBreadcrumb;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To click the Filter By button in Mobile
	 * 
	 * @throws Exception
	 */
	public void clickFilterByInMobile() throws Exception {
		BrowserActions.scrollToViewElement(btnFilterByMobile, driver);
		BrowserActions.clickOnElement(btnFilterByMobile, driver,
				"Filter By option in mobile");
	}

	/**
	 * To verify search refinement size,price brand,color is displayed properly
	 * 
	 * @param filterType
	 *            - Ex. color, size
	 * @return
	 * @throws Exception
	 */
	public boolean verifyRefinement(String filterType) throws Exception {
		boolean status = false;
		for (WebElement lstElement : lstRefinementFilterOptions) {
			List<WebElement> lstEle = lstElement.findElements(By
					.cssSelector(".toggle"));
			for (WebElement element : lstEle) {
				if (BrowserActions
						.getText(driver, element, "Refinement option")
						.equalsIgnoreCase(filterType)) {
					status = true;
					break;
				}
			}
		}

		return status;
	}

	/**
	 * To click on close icon in refinement section
	 */
	public void clickCloseIconInRefinementSection() throws Exception {
		BrowserActions.scrollToViewElement(iconCloseMenu, driver);
		BrowserActions.clickOnElement(iconCloseMenu, driver, "Refinement Page");
	}

	/**
	 * To verify the color of the Size text displayed as blue
	 * 
	 * @param cssAttribute
	 * @param cssValue
	 * @return status
	 * @throws Exception
	 */
	public boolean verifyColorOfSizeRefinementText() throws Exception {
		boolean status = false;
		String cssAttr = "color";
		String cssValue = "rgba(16, 114, 181, 1)";
		status = Utils.verifyCssPropertyForElement(txtSize, cssAttr, cssValue);
		return status;
	}

	/**
	 * To verify expand toggle is displayed for 'filterType'
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public boolean verifyExpandToggle(String filterType) throws Exception {
		boolean status = false;
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");
			BrowserActions.scrollToView(element1, driver);
			if (filterType.equals("price")) {
				filterType = filterType.replace("price", " ");
			}

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (toggle.getAttribute("class").contains("expanded")) {
			status = true;
		}
		return status;
	}

	/**
	 * To click the collapse/expand button of 'filterType' in the filter panel
	 * 
	 * @param filterType
	 *            - Eg: color, size
	 * @throws Exception
	 */
	public void clickCollapseExpandToggle(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			if (element1.getAttribute("class").contains(filterType)) {
				element1 = element1.findElement(By.cssSelector("h3 i"));
				element = element1;
				break;
			}
		}
		BrowserActions
				.clickOnElement(element, driver, "Collapse/Expand toggle");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilClpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, spinner);
	}

	/**
	 * To get refinement list options
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getRefinementList() throws Exception {
		ArrayList<String> refinement = new ArrayList<>();
		int refinementsize = lstRefinementFilterOptions.size() - 1;
		// Getting the web element that matches the 'filterType'
		for (int i = 0; i < refinementsize; i++) {
			if (lstRefinementFilterOptions.get(i + 1).findElement(
					By.cssSelector("h3")) != null) {
				WebElement toggle = lstRefinementFilterOptions.get(i + 1)
						.findElement(By.cssSelector("h3"));
				refinement.add(BrowserActions.getText(driver, toggle,
						"refinement list"));

			}
		}
		return refinement;
	}

	/**
	 * To verify Catagory refinement is expanded properly
	 * 
	 * @param txtcatagory
	 * @return status
	 * @throws Exception
	 */
	public boolean verifyExpandToggleOfCatagoryRefineMent(String txtcatagory)
			throws Exception {
		boolean status = false;
		if (BrowserActions.getText(driver, textCatagoryrefinement,
				"Catagory refinement").equals(txtcatagory)) {
			// check expand toggle in the catagory refinement
			if (textCatagoryrefinement.getAttribute("class").contains(
					"expanded")) {
				status = true;
			}
		}
		return status;
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String selectSortByDropDownByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");
		BrowserActions.clickOnElement(lstSortByOptions.get(index - 1), driver,
				"SortBy dropdown list");
		waitUntilPlpSpinnerDisappear();
		return lstSortByOptions.get(index - 1).getText();
	}

	// ---------- Spinner ------------ //

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, spinner);
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedSortByValue() throws Exception {
		return drpSortBy.findElement(By.cssSelector("div")).getText().trim();
	}

	/**
	 * To get selected option from sort by dropdown in breadcrumb
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedSortByValueInBreadcrumb() throws Exception {
		return BrowserActions.getText(driver, txtRefinementValueInBreadcrumb,
				"selected option from sort by dropdown in breadcrumb");
	}

	public HomePage clickOnHome() throws Exception {
		for (int i = 0; i < txtBreadCrumbHome.size(); i++) {
			BrowserActions.clickOnElement(txtBreadCrumbHome.get(0), driver,
					"Click on home");
		}
		return new HomePage(driver);
	}

	public void clickOnFilterByOption() throws Exception {
		BrowserActions.clickOnElement(btnMobileFilterBy, driver,
				"Click on filter by");
	}

	/**
	 * To verify the color of refinement headings. return status-boolean
	 */
	public boolean verifyColorOfRefinementHeadings(String filterType)
			throws Exception {
		boolean status = false;

		for (int i = 1; i < lstRefinementFilterOptions.size() - 1; i++) {
			WebElement toggle = lstRefinementFilterOptions.get(i).findElement(
					By.cssSelector("h3"));
			if (BrowserActions.getText(driver, toggle, "refinement options")
					.trim().equals(filterType)) {
				// Clicking expand toggle if filter list is not expanded
				String cssAttrColor = "color";
				String cssValueOfColor = "rgba(16, 114, 181, 1)";
				status = Utils.verifyCssPropertyForElement(toggle,
						cssAttrColor, cssValueOfColor);
				break;

			}

		}
		return status;
	}

	/**
	 * To search for a product
	 * 
	 * @param textToSearch
	 * @return SearchResultPage
	 * @throws Exception
	 */
	public SearchResultPage searchProductKeyword(String textToSearch)
			throws Exception {
		final long startTime = StopWatch.startTime();
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));
		return new SearchResultPage(driver).get();
	}

	public String getL2HeadersColorByRow(int index) throws Exception {
		WebElement element = driver.findElement(By
				.cssSelector("ul[id='category-level-1']>li:nth-child(" + index
						+ ")>a[data-layout='categoryproducthits']"));
		return element.getCssValue("color");
	}

	public String getL2HeaderByRow(int index) throws Exception {
		String l2Header = driver.findElement(
				By.cssSelector("ul[id='category-level-1']>li:nth-child("
						+ index + ")>a[data-layout='categoryproducthits']"))
				.getAttribute("title");
		if (l2Header.contains("Go to Category: ")) {
			l2Header = l2Header.replace("Go to Category: ", "");
		}
		return l2Header;
	}

	public boolean checkL2HeaderAccordionStatusByRow(int index) {
		return driver.findElement(
				By.cssSelector("ul[id='category-level-1']>li:nth-child("
						+ index + ")>a[data-layout='categoryproducthits']>i"))
				.isDisplayed();
	}

	public String getFontStyleByRow(int index) {
		return driver.findElement(
				By.cssSelector("ul[id='category-level-1']>li:nth-child("
						+ index + ")>a[data-layout='categoryproducthits']"))
				.getCssValue("font-weight");
	}

	// --------------------------Left Navigation - Category
	// Refinment----------------------//
	/**
	 * To get the List of Items' text in Left Navigation - Category Refinement
	 * 
	 * @return List<String> - List of Items' Text
	 * @throws Exception
	 */
	public List<String> getLeftNavigationListText() throws Exception {
		List<String> l3CategoryText = new ArrayList<String>();

		for (int i = 0; i < lstCategoryRefinement.size(); i++) {
			String temp = lstCategoryRefinement.get(i)
					.getAttribute("innerHTML").trim().replace("amp;", "")
					.split("<")[0];
			l3CategoryText.add(temp);
			System.out.println(temp);
		}
		return l3CategoryText;
	}

	/**
	 * To get the List of Items' Count in Left Navigation - Category Refinement
	 * 
	 * @return List<String> - List of Items' Count
	 * @throws Exception
	 */
	public List<String> getCategoryRefinementItemsCount() throws Exception {
		List<String> l3CategoryText = new ArrayList<String>();

		for (int i = 0; i < lstCategoryRefinement.size(); i++) {
			String temp = lstCategoryRefinement.get(i)
					.findElement(By.cssSelector(".refine-count"))
					.getAttribute("innerHTML").replace("(", "")
					.replace(")", "").trim();
			l3CategoryText.add(temp);
		}
		return l3CategoryText;
	}

	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickViewWithProductId() throws Exception {
		if (lstProductImages.size() > 0) {
			BrowserActions.mouseHover(driver, lstProductImages.get(0));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	// ------------QuickView-----------------------//

	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickView() throws Exception {
		if (lstProductImages.size() > 0) {
			int rand = Utils.getRandom(0, lstProductImages.size());
			BrowserActions.scrollToViewElement(lstProductImages.get(rand),
					driver);
			BrowserActions.mouseHover(driver, lstProductImages.get(rand));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"quickview button");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	/**
	 * To verify quick view state
	 * 
	 * @return boolean
	 */
	public boolean QVStateNone() {
		return Utils.verifyCssPropertyForElement(qvStyle, "display", "none");
	}

	// ---------- Spinner ------------ //

	/**
	 * To wait for Search results spinner
	 */
	public void waitUntilSearchSpinnerDisappear() {
		BrowserActions.nap(3);
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
	}

	/**
	 * To navigate to PDPPage with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDPWithProductId() throws Exception {
		if (lstProducts.size() > 0) {
			BrowserActions.clickOnElement(lstProducts.get(0), driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * Click on the back button in the Left nav.Desktop
	 * 
	 * @throws Exception
	 */
	public void clickOnBackButton() throws Exception {
		BrowserActions.mouseHover(driver, btnBackarrowLeftNav);
		BrowserActions.clickOnElement(btnBackarrowLeftNav, driver,
				"Click on the back button in the Left Nav.");

	}

	/**
	 * Verify the back to category option is displayed in the Breadcrumb.
	 * 
	 * @param breadcrumbCategoryy
	 * @return
	 * @throws Exception
	 */

	public boolean verifyBackToCategoyInBreadCrumb(String breadcrumbCategory)
			throws Exception {
		boolean status = false;
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (element.getText().trim()
						.equalsIgnoreCase("Back to " + breadcrumbCategory)) {
					status = true;
				}
			}
		} else if (runPltfrm == "mobile") {
			if (BrowserActions
					.getText(driver, lnkBreadcrumbInmobile,
							"Back to Home Breadcrumb").trim()
					.equalsIgnoreCase("Back to " + breadcrumbCategory)) {
				status = true;
			}
		}
		return status;
	}

	/**
	 * Click on the back to category in breadcrumb and navigate to L1 category
	 * and Home option is displayed.
	 * 
	 * @param breadcrumCategory
	 * @throws Exception
	 */
	public void backToCategoyInBreadCrumbCategory(String breadcrumCategory)
			throws Exception {
		if (BrowserActions
				.getText(driver, lnkBreadcrumbInmobile,
						"Back to Home Breadcrumb").trim()
				.equalsIgnoreCase("Back to " + breadcrumCategory)) {
			BrowserActions.javascriptClick(lnkBreadcrumbInmobile, driver,
					"Click on Category" + breadcrumCategory);

		}
	}

	public boolean verifyColorIn1L2Category() throws Exception {
		String colorString = category1InL2OfWomen.getCssValue("color");
		if (colorString.equals("rgba(16, 114, 181, 1)")) {
			return true;

		} else {
			return false;
		}
	}

	public boolean verifyColorIn2L2Category() throws Exception {
		String colorString = category2InL2OfWomen.getCssValue("color");
		if (colorString.equals("rgba(16, 114, 181, 1)")) {
			return true;

		} else {
			return false;
		}
	}

	public boolean verifyColorIn3L2Category() throws Exception {
		String colorString = category3InL2OfWomen.getCssValue("color");
		if (colorString.equals("rgba(16, 114, 181, 1)")) {
			return true;

		} else {
			return false;
		}
	}

	public boolean verifyColorIn4L2Category() throws Exception {
		String colorString = category4InL2OfWomen.getCssValue("color");
		if (colorString.equals("rgba(16, 114, 181, 1)")) {
			return true;

		} else {
			return false;
		}
	}

	/**
	 * Click on the Category Refinement Toggle to navigate to next Level
	 * category
	 *
	 * @throws Exception
	 */
	public String clickCategoryRefinementToggle() throws Exception {
		String CategoryName = null;
		int rand = ThreadLocalRandom.current().nextInt(1,
				lstL2Sub_Refinements.size());
		CategoryName = BrowserActions.getText(driver,
				lstL2Sub_Refinements.get(rand - 1),
				"Category Refinement Name to be clicked");
		BrowserActions
				.clickOnElement(lstL2Sub_Refinements.get(rand - 1), driver,
						"Clicking Category refinement toggle To naviagate to next Category");
		return CategoryName;

	}

	/**
	 * get text on the back button in the Left nav
	 * 
	 * return txtpreviousCategory
	 * 
	 * @throws Exception
	 */
	public String getTxtOnBackButton() throws Exception {
		return BrowserActions.getText(driver, btnBackarrowLeftNav,
				"Previous Category Name");

	}

	public String getImageUrlInListPage(int index) throws Exception {
		return (BrowserActions.getTextFromAttribute(driver,
				lstImgPrimary.get(index - 1), "src", "image src"));

	}

	/**
	 * To click on close icon in search refinement section
	 */
	public void clickOnCloseIcon() throws Exception {

		BrowserActions.clickOnElement(IconCloseMenu, driver, "Refinement Page");
	}

	/**
	 * To get breadcrumb last value
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getBreadCrumbLastValue() throws Exception {
		// BrowserActions.scrollToViewElement(txtRefinementBreadCrumb, driver);
		ArrayList<String> values = new ArrayList<>();
		for (WebElement element : txtBreadCrumbValue) {
			values.add(BrowserActions
					.getText(driver, element, "breadcrumb value")
					.replace("x", "").trim());
		}
		return values;
	}

	public void clickCollapseExpandToggle(String filterType, String... collapse)
			throws Exception {
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (collapse.length > 0
				&& toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		} else if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To select/unselect the refinements in the search refinement panel
	 * 
	 * @param filterType
	 * @param optionValue
	 * @return String
	 * @throws Exception
	 */
	public String selectUnselectFilterOptions(String filterType,
			String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		// Getting the web element that matches the 'filterType'

		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul>li:not([class*='hide'])"));
			if (!(optionValue.length > 0)) {
				if (lstElement.size() == 1) {
					WebElement e = lstElement.get(0);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				} else {
					int rand = ThreadLocalRandom.current().nextInt(1,
							lstElement.size());
					WebElement e = lstElement.get(rand - 1);
					// Clicking random filter values if not mentioned
					BrowserActions.scrollToViewElement(e, driver);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				}

			} else {
				WebElement e = BrowserActions.getMachingTextElementFromList(
						lstElement, optionValue[0], "contains");
				// Clicking filter option based on 'optionValue'
				// BrowserActions.clickOnElement(e.findElement(By.cssSelector("a
				// i")),
				// driver, "Filter values");
				WebElement selectoption = e.findElement(By.cssSelector("a i"));
				if (selectoption.getAttribute("class").contains(
						"toggle-check-active")) {
					BrowserActions.javascriptClick(e.findElement(By
							.cssSelector("a i[class*='toggle-check-active']")),
							driver, "Un selecting the Filter values");
				} else {
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Selectin the Filter values");
				}
				waitUntilSearchSpinnerDisappear();
				filterValue = optionValue[0].toString();

			}
		}

		Utils.waitForPageLoad(driver);

		return filterValue;
	}


	/**
	 * To get breadcrumb value
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getBreadCrumbValue() throws Exception {
		ArrayList<String> values = new ArrayList<>();
		for (WebElement element : breadcrumbLnk) {
			values.add(BrowserActions.getText(driver, element,
					"breadcrumb value"));// .replace("x", "").trim());
		}
		return values;
	}

	/**
	 * To get click on l4 category by index value
	 * 
	 * @throws Exception
	 */
	public void clickL4CategoryByIndex(int index) throws Exception {
		Utils.waitForElement(driver, l4Category.get(index));
		BrowserActions.clickOnElement(l4Category.get(index - 1), driver,
				"L4 category");
		Utils.waitForPageLoad(driver);
	}

	public String getL4CategoryNameByIndex(int index) throws Exception {
		Utils.waitForElement(driver, l4Category.get(index));
		String l4Name = BrowserActions.getText(driver,
				l4Category.get(index - 1), "L4 category");
		return l4Name;
	}
}
