package com.belk.pages;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.lang.*;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.reusablecomponents.ShoppingBagUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class ShoppingBagPage extends LoadableComponent<ShoppingBagPage> {
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	private static final String SELECTED_COLOR = "ul[class='swatches color']>li.selected>a>img";
	private static final String LIST_OF_COLOR = "div[class='product-col-2 product-detail'] div[class='product-variations'] ul.swatches.color>li:not([class*='selected'])";
	private static final String SELECTED_QTY = ".quantity div[class*='selected-option']";
	public String GLOBAL_MESSAGE = "Items from your other shopping bag have been added to your current shopping bag.";
	public String LINE_MESSAGE = "This item in your bag has been changed.";

	@FindBy(css = "div[id='mini-cart']")
	WebElement divCart;

	@FindBy(css = "div[class='cart-order-totals'] button[class='button-fancy-large']")
	WebElement btnCheckoutInOrderSummary;

	@FindBy(css = ".empty-cart")
	WebElement CouponContainsInCart;

	@FindBy(css = "div [class='login-box login-account']")
	WebElement divLoginBox;

	@FindBy(css = ".cart-row")
	List<WebElement> productList;

	@FindBy(css = ".coupons-input+.coupon-submit>a+button#add-coupon")
	WebElement btnApplyCouponNxtToViewAll;

	@FindBy(css = ".textbutton.simple")
	WebElement btnRemoveCoupon;

	@FindBy(css = "div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front bonusproduct-modal ui-draggable']")
	WebElement selectFreeGiftModal;

	@FindBy(css = "div[class='addto-wishlist'] span[class='in-wishlist']")
	WebElement txtItemAddedWishList;

	@FindBy(css = ".content-asset>h1")
	WebElement txtYourShoppingBagEmpty;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrumb")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(xpath = ".//*[@id='cart-table']")
	WebElement tblCartTable;

	@FindBy(css = ".need-a-coupon.hide-mobile.simple.button")
	WebElement btnViewAllCoupons;

	@FindBy(css = "#cart-items-form > fieldset > div > div > div > div.coupon-submit > span > a")
	WebElement btnMobileViewAllCoupons;

	@FindBy(css = "div[class='cart-actions hide-mobile']>a>button")
	WebElement btnContinueShopping;

	@FindBy(css = "div[class='cart-actions cart-actions-top hide-desktop']>a>button")
	WebElement btnContinueShoppingMobile;

	@FindBy(css = ".button.primarybutton")
	WebElement btnContinueShoppingWithoutProducts;

	@FindBy(css = "div>#dwfrm_cart_couponCode")
	WebElement txtCouponCode;

	@FindBy(css = "#add-coupon")
	WebElement btnApplyCoupon;

	@FindBy(css = ".cartcoupon.clearfix>div>span[class='value']")
	WebElement txtAppliedCoupon;

	@FindBy(css = ".need-a-coupon")
	WebElement btnNeedCoupon;

	@FindBy(css = "div[class='hide-mobile'] div[class='cart-promo cart-promo-approaching']")
	WebElement promoFreeShipDsk;

	@FindBy(css = "div[class='hide-desktop'] div[class='cart-promo cart-promo-approaching']")
	WebElement promoFreeShipMob;

	@FindBy(css = "#QuickViewDialog")
	WebElement QuickViewDialog;

	@FindBy(css = "#user-zip")
	WebElement txtZipCode;

	@FindBy(css = "[type='button']:not([title*='Close'])>span")
	WebElement btnContinue;

	@FindBy(css = "button[class='select-store-button']")
	WebElement btnSelectStore;

	@FindBy(css = ".no-results.error")
	WebElement errorMessage;

	@FindBy(css = "#checkout-form > div > div > div.coupon-btn > a.hide-mobile.need-a-coupon")
	WebElement btnNeedCouponMobile;

	@FindBy(css = "div.ordertotals div.cart-actions form#checkout-form.cart-action-checkout fieldset button.button-fancy-large")
	WebElement btnCheckOut;

	@FindBy(css = ".button-text")
	List<WebElement> lstBtnRemove;

	@FindBy(css = ".user-logout>a")
	WebElement btnLogout;

	@FindBy(css = ".order-total>td")
	List<WebElement> fldEstimatedTotal;

	@FindBy(css = ".order-subtotal>td")
	List<WebElement> fldMerchandiseTotal;

	@FindBy(css = ".order-shipping>td:nth-child(2)")
	WebElement txtShippingValue;

	@FindBy(css = ".order-giftbox>td")
	List<WebElement> giftBoxTotal;

	@FindBy(css = ".order-sales-tax>td")
	List<WebElement> fldSalesTax;

	@FindBy(css = ".product-brand>a")
	WebElement ProductBrandlnk;

	@FindBy(css = ".item-edit-details:nth-child(2)>a")
	WebElement ProductNamelnk;

	@FindBy(css = ".cart-cancel.close-dialog")
	WebElement cancelLnkUpdatePopup;

	@FindBy(css = "div [id='main']>div>div[class='hide-mobile']>div>span")
	WebElement shippingMsg;

	@FindBy(css = ".mini-cart-total>a")
	WebElement iconMinicart;

	@FindBy(css = "div.empty_title> h1")
	public WebElement txtemptycarttitle;

	@FindBy(css = ".minicart-quantity")
	WebElement txtMinicartQuantityText;

	@FindBy(css = ".order-subtotal > td:nth-child(2)")
	WebElement lblOrderMerchandiseTotal;

	@FindBy(css = ".order-shipsurcharge>td:nth-child(2)")
	WebElement lblOrderSurcharge;

	@FindBy(css = ".order-shipping > td:nth-child(2)")
	WebElement lblOrderShippingAmount;

	@FindBy(css = ".order-discount td:nth-child(2)")
	WebElement lblOrderShippingAmountDiscount;

	@FindBy(css = ".order-sales-tax>td:nth-child(2)")
	WebElement lblOrdereSalestax;

	@FindBy(css = ".order-giftbox>td:nth-child(2)")
	WebElement lblOrdereGiftBox;

	@FindBy(css = ".order-total>td:nth-child(2)")
	WebElement lblOrdertotal;

	@FindBy(css = ".order-totals-table-cont ")
	WebElement lstTxtodrerdatails;

	@FindBy(css = ".cart-promo.cart-promo-approaching")
	WebElement txtfreeshoppingmsg;

	@FindBy(css = "div[class='error']")
	WebElement txtInvalidCouponError;

	@FindBy(css = ".cart-action-continue-shopping")
	WebElement btnContinueShoppingInEmptyBag;

	@FindBy(css = "div[class='sku']")
	List<WebElement> UPCinBagPage;

	@FindBy(css = ".gift-message-checkbox")
	List<WebElement> lstCheckBoxAddFreeGiftMessage;

	@FindBy(css = ".gift-message-checkbox")
	WebElement chckboxAddfreemsg;

	@FindBy(css = ".product-list-item>.item-edit-details")
	List<WebElement> lstProductNames;

	@FindBy(css = "button[id='add-to-cart']")
	WebElement btnUpdateShoppingBag;

	@FindBy(css = "h1.product-name")
	WebElement productName;

	@FindBy(css = "button[class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']")
	WebElement btnCloseUpdateShoppingBAgPage;

	@FindBy(css = "button[id='add-to-cart']")
	WebElement btnAddtoBag;

	@FindBy(css = ".loader[style*='block']")
	WebElement ShoppingBagspinner;

	@FindBy(css = "div[class='item-price']>div[class='standard-price']")
	WebElement txtStandardPrice;

	@FindBy(css = ".quantity .selected-option")
	List<WebElement> lstupdatedQty;

	@FindBy(css = "div[data-attribute='color'] span[class='value']")
	List<WebElement> lstUpdatedColor;

	@FindBy(css = "div[class='gift-message-checkbox-text']")
	List<WebElement> divGiftMessageBox;

	@FindBy(css = "textarea[name*='giftmessagetext']")
	List<WebElement> txtAreaGiftMessageBox;

	@FindBy(css = ".char-count")
	WebElement charCountMsg;

	@FindBy(css = "ul[class='menu-utility-user'] li.myaccount>a")
	WebElement btnMyAccount;

	@FindBy(css = "a[class='cart-cancel close-dialog']")
	WebElement cancelEditDetails;

	@FindBy(css = "div[id='QuickViewDialog']")
	WebElement editDetailsPopup;

	@FindBy(css = "a[title='Add this product to gift registry']")
	List<WebElement> lstAddToRegistryLink;

	@FindBy(css = ".step-1")
	WebElement tabShipping;

	@FindBy(css = ".order-value")
	WebElement orderSummaryTotal;

	@FindBy(css = "#cart-table tr.cart-row button[value='Remove']")
	List<WebElement> removeProductByIndex;

	@FindBy(css = ".item-image>img")
	WebElement productImg;

	@FindBy(css = ".product-brand")
	WebElement txtProductBrand;

	@FindBy(css = ".item-edit-details>a")
	WebElement txtProductName;

	@FindBy(css = "div[class='product-list-item']>div[data-attribute='color']>span[class='value']")
	List<WebElement> lblColor;

	@FindBy(css = "div[class='item-price']>div[class='now-price']")
	WebElement txtNowPrice;

	@FindBy(css = "div[class='product-list-item']>div[data-attribute='size']>span[class='value']")
	List<WebElement> lblSize;

	@FindBy(css = "div[class='item-price'] div.standard-price")
	List<WebElement> lblStandardPrice;

	@FindBy(css = "div[class='item-price'] div.now-price")
	List<WebElement> lblNowPrice;

	@FindBy(css = "div[class='item-price']")
	List<WebElement> lblPriceSectionInProduct;

	@FindBy(css = "div[class='item-quantity']")
	WebElement drpQuantity;

	@FindBy(css = ".price-total")
	List<WebElement> lblSubtotal;

	@FindBy(css = ".add-to-wishlist.wishlist-button")
	WebElement btnAddToWishlist;

	@FindBy(css = ".add-to-registry.gift-registry-unregistered")
	WebElement btnAddToRegistry;

	@FindBy(css = ".find-a-store-pdp")
	WebElement btnFindStore;

	@FindBy(css = ".gift-box-checkbox div[class*='checkbox-wrapper']")
	WebElement chkAddGiftBox;

	@FindBy(css = ".cart-detail-table>p")
	WebElement promoFinalTxt;

	@FindBy(css = "div[class='gift-message-checkbox']")
	WebElement txtGiftMessage;

	@FindBy(css = "#add-to-cart")
	WebElement btnUpdateShoppingCart;

	@FindBy(css = "#dialog-container")
	WebElement RegistryPopUp;

	@FindBy(css = "#secondary div[class='mini-payment-instrument order-component-block']")
	WebElement txtPaymentDetails;

	@FindBy(css = "#add-to-cart")
	WebElement btnUpdateShoppingcart;

	@FindBy(css = "#dialog-container")
	WebElement findInStorePopUp;

	@FindBy(css = "textarea[id='dwfrm_cart_shipments_i0_items_i0_giftmessagetext']")
	WebElement txtGiftMessageTextArea;

	@FindBy(css = ".myaccount.medium>a")
	WebElement lnkMyAccount;

	@FindBy(css = "div[ class='selected-option selected']")
	WebElement getNoOfQtyInShoppingBag;

	@FindBy(css = "div [class='cart-top-alerts']>span")
	WebElement GloabalMessage;

	@FindBy(css = "div [id='left-col']>form>fieldset>table>tbody>tr>td>div")
	WebElement LineMessage;

	@FindBy(css = ".order-totals-table-cont")
	WebElement txtOrderSummaryDetails;

	@FindBy(css = "span[class='info']")
	WebElement msgCartMerging;

	@FindBy(css = "table[id='cart-table'] > tbody > tr.cart-row")
	List<WebElement> lstItemInCart;

	@FindBy(css = "div[class='item-price'] div[class='standard-price']")
	WebElement productPricing;

	@FindBy(xpath = ".//*[@id='primary']/div[2]/div/span")
	WebElement freeShippingMessageMyBag;

	@FindBy(css = ".free-gift.bold")
	WebElement lblFreeGift;

	@FindBy(css = "[id='cart-table']>tbody>tr:nth-child(1)")
	WebElement txtSurchargeOnCartPage;

	@FindBy(css = ".price-total>span")
	WebElement txtSubtotalspan;

	@FindBy(css = ".user-logout.medium>a")
	WebElement lnkSignOut;

	@FindBy(css = "#primary > div.cart_top_content > div.cart-top-alerts > span")
	WebElement lblGlobalitemalertmessage;

	@FindBy(css = "#cart-table > tbody > tr.cart-lineitem-alert")
	WebElement lblLineitemalertmessage;

	@FindBy(css = ".callout-message")
	WebElement lblFreeGiftMessage;

	@FindBy(css = "table[id='cart-table']>tbody>tr:not([class*='cart-row'])>td[class='cart-details']>table>tbody>tr>td[class='cart-detail-table-data']>div[class='item-total']>span")
	WebElement giftProductPrice;

	@FindBy(css = ".promotion .callout-message")
	WebElement lblPromotionMessage;

	@FindBy(css = "a.thumb-cartPageLink")
	WebElement cartPageLink;

	@FindBy(css = "[id='cart-table']>tbody>tr:nth-child(2)>td>div")
	WebElement txtLineItemRestriction;

	@FindBy(css = "[id='cart-table']>tbody>tr:nth-child(1)>td>div")
	WebElement txtLineItemStateRestriction;

	@FindBy(css = "tr.cart-lineitem-alert>td>div")
	WebElement txtSurchargeMsg;

	@FindBy(css = "div[class='product-list-item']>div[class='free-gift bold']")
	List<WebElement> lblPromotionProductName;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details']")
	List<WebElement> lblPromotionProduct;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details'] div[class='sku']")
	List<WebElement> lblPromotionProductUpc;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details'] div[data-attribute='color']>span[class='value']")
	List<WebElement> lblPromotionProductColor;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details'] div[data-attribute='size']>span[class='value']")
	List<WebElement> lblPromotionProductSize;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details'] .price-total")
	List<WebElement> lblPromotionProductSubTotal;

	@FindBy(css = ".product-UPC")
	List<WebElement> lblUpcofProduct;

	@FindBy(css = ".quantity ul[class='selection-list']>li:not([class='selected'])")
	public List<WebElement> lstQty;

	@FindBy(css = ".loader[style*='block']")
	WebElement spinner;

	@FindBy(css = ".cart-detail-table>p")
	WebElement finalPromoMessage;

	@FindBy(css = ".quantity div[class='custom-select']")
	// li[class='attribute variant-dropdown
	// size']>div>div>div[class='selected-option']
	WebElement drpQty;

	@FindBy(css = ".coupon-success>span")
	public WebElement txtCouponSuccess;

	@FindBy(css = ".discount.first .value")
	WebElement txtCouponDiscountValue;

	@FindBy(css = ".order-discount.discount>td:nth-child(2)")
	WebElement lblOrderCouponSavings;

	@FindBy(css = ".price-coupon")
	List<WebElement> lblPriceCoupon;

	@FindBy(css = ".thumb-cartPageLink")
	WebElement lnkAddFreeGiftToBag;

	@FindBy(xpath = ".//*[@id='cart-table']/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/div[2]/span")
	WebElement txtSubtotalspanOfFreeGift;

	@FindBy(css = "#cart-items-form > fieldset > div > div > div > div.coupon-success > span")
	WebElement couponMessage;

	@FindBy(css = "#cart-items-form > fieldset > div > div > div > div.coupon-success > i")
	WebElement couponTickMark;

	@FindBy(css = "#cart-items-form > fieldset > div > div > div > div.rowcoupons > div.item-details > div.cartcoupon.clearfix > div > span.label")
	WebElement txtCoupon;

	@FindBy(css = ".price-total>span")
	WebElement productPriceInShopping;

	@FindBy(css = "tr[class='cart-lineitem-alert'] div[class='info']")
	WebElement txtSurchargeOnShoppingCartPage;

	@FindBy(css = "span[class='price-total']")
	WebElement txtSubtotalPriceLineItem;

	@FindBy(css = "div[class='shipping-surcharge']")
	WebElement txtShippingSurcharge;

	@FindBy(css = ".toggle-grid>i:nth-child(2)")
	WebElement toggleListMobile;

	@FindBy(css = "button.menu-toggle.sprite")
	WebElement lnkHamburger;

	@FindBy(css = ".menu-active")
	WebElement islnkHamburgerOpened;
	@FindBy(css = ".header-top.hide-mobile.full-width>div>ul>li[class='myaccount medium']>a")
	WebElement myAccountPage;

	@FindBy(css = "div [class='wrapper-content']>ul[class='menu-utility-user hide-desktop']>li:nth-child(1)>a")
	WebElement myAccountPageMobile;

	@FindBy(css = ".cart_top_content>h1")
	WebElement myBagHeader;

	@FindBy(css = ".gift-box-checkbox .form-row.label-inline.form-indent")
	WebElement chckboxAddagiftboxText;

	@FindBy(css = "#dwfrm_cart_shipments_i0_items_i0_giftbox")
	WebElement chckgiftboxStatus;

	@FindBy(css = ".gift-box-text-col :nth-child(1)")
	WebElement txtboxandribbon1stline;

	@FindBy(css = ".gift-box-text-col :nth-child(2)")
	WebElement txtboxandribbon2ndline;

	@FindBy(css = ".order-giftbox>td:nth-child(2)")
	WebElement ttlgiftboxpriceinestimatedshipping;

	@FindBy(css = "table[id='cart-table']>tbody>tr:not([class*='cart-row'])")
	WebElement GwpModalInCartPage;

	@FindBy(css = ".callout-message:nth-child(1)")
	WebElement gpwProductMessage;

	@FindBy(css = "div[class='cart-rebates']")
	WebElement txtcartRebate;

	@FindBy(css = "tr:not([class='cart-row']) td[class='cart-details'] div[class='sku'] span[class='product-UPC']")
	WebElement txtPromoProdUPC;

	@FindBy(css = "p[style='color:red']:nth-child(3)")
	WebElement txtPromoMessage;

	@FindBy(css = ".order-discount.discount")
	public WebElement trowTotalCouponSavings;

	@FindBy(css = "tr[class='cart-lineitem-alert'] div[class='info']")
	List<WebElement> txtLineItems;

	@FindBy(css = ".callout-message:nth-child(1)>a")
	WebElement lnkViewDetails;

	@FindBy(css = "div[id='dialog-container']")
	WebElement popUpFreeGiftDetails;

	@FindBy(css = "div[class='callout-message']:nth-child(1)>a")
	WebElement viewDetails;

	@FindBy(css = ".choice-bonus-desc>p")
	WebElement description;

	@FindBy(css = "button>span[class='ui-button-icon-primary ui-icon ui-icon-closethick']")
	WebElement closePopUp;

	@FindBy(css = "#main>div>div[id='left-col']>form>fieldset>table>tbody>tr:not([class='cart-row'])>td[class='item-image']")
	WebElement giftImage;

	@FindBy(css = "div[class='sku']:nth-child(2)")
	WebElement UpcOfFreeProduct;

	@FindBy(css = "div[class*='bonusproduct-modal'][style*='block']")
	WebElement bonusproductmodalOpened;

	@FindBy(css = "div[class*='bonusproduct-modal'][style*='none']")
	WebElement bonusproductmodalClosed;

	@FindBy(css = ".coupons-data .item-details")
	WebElement CouponDetailsFromCouponSection;

	@FindBy(css = ".coupons-copy")
	WebElement CouponResourceMessage;

	@FindBy(css = ".item-total .bonus-item")
	WebElement txtCouponApplied;

	@FindBy(css = ".store-name")
	WebElement storeName;

	@FindBy(css = ".store-address")
	WebElement storeAddress;

	@FindBy(css = ".store-phone>a")
	WebElement storeNumber;

	@FindBy(css = ".store-miles>a")
	WebElement storeDistance;

	@FindBy(css = ".store-working")
	WebElement storeWorkingHour;

	@FindBy(css = ".store-status.store-in-stock")
	WebElement storeProductInStock;

	@FindBy(css = ".store-heading")
	WebElement storeHeading;

	@FindBy(css = "input[name='q']")
	WebElement txtSearch;

	@FindBy(css = ".header-search button")
	WebElement btnSearch;

	@FindBy(css = "div[class='hide-mobile'] div[class='cart-promo cart-promo-approaching']")
	WebElement MsgNeededAmountForFreeShip;

	@FindBy(css = ".coupon-success>span")
	WebElement CouponAppliedSuccessMsg;

	@FindBy(css = ".empty-cart .item-details")
	WebElement CouponDetailsFromEmptyCart;

	@FindBy(css = ".empty-cart .info")
	WebElement txtFromCouponWarningMsgEmptyBag;
	
	@FindBy(css = ".message-qv")
	WebElement txtMsgOnlineOnly;
	
	@FindBy(css = ".cart-footer .content-asset p:nth-child(2)")
	WebElement txtCartFooterMsg;
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public ShoppingBagPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// GiftRegistryPage

	public ShoppingBagPage(WebDriver driver, String url) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divCart))) {
			Log.fail("Shopping Bag page did not open up.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	/**
	 * To click on checkout in order summary for guest user
	 * 
	 * @param shoppingBagPage
	 * @return
	 * @throws Exception
	 */
	public Object clickOnCheckoutInOrderSummary(ShoppingBagPage shoppingBagPage) throws Exception {
		BrowserActions.clickOnElement(btnCheckoutInOrderSummary, driver, "Checkout button in Order Summary");
		Utils.waitForPageLoad(driver);

		if (Utils.waitForElement(driver, divLoginBox))
			return new SignIn(driver).get();

		return new CheckoutPage(driver).get();
	}

	/**
	 * To get the product count
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getProductCount() throws Exception {
		return productList.size();
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb(String... runMode) throws InterruptedException {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
		if (runPltfrm == "desktop" || (runMode.length > 0)) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	/**
	 * To click the breadcrumb link by index
	 * 
	 * @param index
	 * @throws Exception
	 */
	public void clickBreadcrumbByIndex(int index) throws Exception {
		if (runPltfrm == "mobile") {
			BrowserActions.scrollToViewElement(txtProductInBreadcrumbMobile, driver);
			BrowserActions.clickOnElement(txtProductInBreadcrumbMobile, driver, "Breadcrumb");
		} else {
			BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(index), driver);
			BrowserActions.clickOnElement(lstTxtProductInBreadcrumb.get(index), driver, "Breadcrumb");
		}
	}

	/**
	 * To verify the coupon pane is displayed below the line items
	 * 
	 * @return
	 */
	public boolean verifyCouponPaneBelowProductList() {
		boolean status = false;
		if (Utils.waitForElement(driver, tblCartTable.findElement(
				By.xpath("following-sibling::div[@class='cart-footer']/div[@class='cart-coupon-code']")))) {
			status = true;
		}
		return status;
	}

	/**
	 * To click the view all coupon button
	 * 
	 * @return coupon page
	 * @throws Exception
	 */
	public CouponsPage clickViewAllcoupon() throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(btnMobileViewAllCoupons, driver, "View All Coupons");
		} else {
			BrowserActions.clickOnElement(btnViewAllCoupons, driver, "View All Coupons");
		}
		return new CouponsPage(driver).get();
	}

	/**
	 * To click on to Continue Shopping button with product returns to Home
	 * Page/PDP
	 * 
	 * @throws Exception
	 */
	public void clickContinueShopping() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(btnContinueShoppingMobile, driver, "Continue shopping");
		} else {
			BrowserActions.clickOnElement(btnContinueShopping, driver, "Continue shopping");
		}
	}

	/**
	 * To click continue shopping button without products
	 * 
	 * @throws Exception
	 */
	public void clickContinueShoppingWithoutProducts() throws Exception {
		BrowserActions.clickOnElement(btnContinueShoppingWithoutProducts, driver, "Continue shopping");
	}

	/**
	 * To enter the coupon code
	 * 
	 * @param couponCode
	 * @throws Exception
	 */
	public void enterCouponCode(String couponCode) throws Exception {
		ShoppingBagUtils.enterCouponCode(txtCouponCode, btnApplyCoupon, couponCode, driver);
	}

	/**
	 * To get the applied coupon code
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getAppliedCouponCode() throws Exception {
		return (BrowserActions.getText(driver, txtAppliedCoupon, "Coupon Code"));
	}

	/**
	 * To click the need coupon link
	 * 
	 * @return Coupon page
	 * @throws Exception
	 */
	public CouponsPage clickNeedCoupon() throws Exception {
		BrowserActions.clickOnElement(btnNeedCoupon, driver, "Click on Need a Coupon link");
		return new CouponsPage(driver).get();
	}

	/**
	 * To verify the need coupon link
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyNeedCouponlink() throws Exception {
		boolean status = false;
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			if (!btnNeedCouponMobile.isDisplayed()) {
				status = true;
			} else {
				status = false;
			}
		} else {
			if (!btnNeedCoupon.isDisplayed()) {
				status = false;
			} else {
				status = true;
			}
		}

		return status;
	}

	/**
	 * To remove all added cart items for registered user
	 *
	 * @throws Exception
	 */
	public void removeAddedCartItems() throws Exception {

		if (Integer.parseInt(headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
			BrowserActions.clickOnElement(iconMinicart, driver, "View bag button");

			for (int i = 0; i < lstBtnRemove.size(); i++) {
				BrowserActions.javascriptClick(lstBtnRemove.get(i), driver, "Remove button");
			}
		} else {
			Log.event("Minicart is empty.");
		}

	}

	public void removeAddedCartItemsin(int value) throws Exception {
		Utils.waitForPageLoad(driver);
		Log.event(">>>>>>>>>Expected");
		if (getProductCountInBag() > 0) {
			for (int i = 0; i < getProductCountInBag(); i++) {
				BrowserActions.clickOnElement(
						driver.findElement(By.cssSelector(
								"#cart-table tr.cart-row button[value='Remove']" + i + "_deleteProduct']")),
						driver, "Remove button");
				Log.event("Item-" + (i - 1) + "deleted");
				Utils.waitForPageLoad(driver);
			}
		} else {
			Log.event("Bag is empty.");
		}
	}

	/**
	 * To get the count in minicart icon
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getMinicartCount() throws Exception {
		String minicartQryText = BrowserActions.getText(driver, txtMinicartQuantityText, "Minicart quantity text");
		String[] minicartQtytextSplit = minicartQryText.split(" ");
		return Integer.parseInt(minicartQtytextSplit[0]);
	}

	/**
	 * To click apply coupon in shopping bag
	 * 
	 * @param txtCouponCodeToEnter
	 * @throws Exception
	 */
	// public void applyCouponInShoppingBag(String txtCouponCodeToEnter) throws
	// Exception {
	//
	// BrowserActions.typeOnTextField(txtCouponCode, txtCouponCodeToEnter,
	// driver, "coupon code Box");
	// BrowserActions.clickOnElement(btnApplyCoupon, driver, "coupon code Box");
	// Utils.waitForPageLoad(driver);
	// }// applyCouponInShoppingBag

	/**
	 * To verify the need coupon displayed below the continue shopping
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyNeedCouponDisplayedBelowContinueShopping() throws Exception {
		boolean status = false;
		if (Utils.waitForElement(driver,
				btnContinueShoppingInEmptyBag.findElement(By.xpath("following-sibling::a[@class='need-a-coupon']")))) {
			status = true;

		}
		return status;
	}

	/**
	 * To get the text from empty bag
	 * 
	 * @return order text
	 * @throws Exception
	 */
	public String getTextFromEmptyBag() throws Exception {
		return BrowserActions.getText(driver, txtYourShoppingBagEmpty, "Empty Bag");
	}

	/*
	 * public ArrayList<String> getTextfromodrerdetails() { ArrayList<String>
	 * orderText = new ArrayList<>(); for (WebElement element :
	 * lstTxtodrerdatails) { if (!element.getText().equals(""))
	 * orderText.add(element.getText()); } return orderText; }
	 */

	/**
	 * To get the text from free shipping
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextfromFreeshipping() throws Exception {
		return BrowserActions.getText(driver, txtfreeshoppingmsg, "added message");
	}

	/**
	 * To get item index in product list by UPC
	 *
	 * @param UPC
	 * @return
	 * @throws Exception
	 *             Last Modified By - Gowri & Last Modified Date - 03/01/2017
	 */
	public String getIndexByUPC(String UPC) throws Exception {
		for (int i = 0; i < UPCinBagPage.size(); i++) {
			if (UPCinBagPage.get(i).getText().replace("UPC: ", "").equalsIgnoreCase(UPC))
				return Integer.toString(i);
		}

		return "no";
	}

	/**
	 * To Click on Add Free Gift Message CheckBox
	 * 
	 * @param index
	 * @throws Exception
	 */

	public void checkAddFreeGiftMessage(int index) throws Exception {
		BrowserActions.scrollToView(productList.get(index), driver);
		BrowserActions.selectRadioOrCheckbox(
				productList.get(index).findElement(By.cssSelector(".gift-message-checkbox")), "YES");// (lstCheckBoxAddFreeGiftMessage.get(index),
																										// driver,
																										// "Add
																										// Free
																										// Gift
																										// Message
																										// Checkbox");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify gift meassage box
	 * 
	 * @param index
	 * @return
	 * @throws Exception
	 */

	public boolean verifyGiftMessage(int index) throws Exception {
		if (divGiftMessageBox.get(index).getCssValue("display").contains("block"))
			return true;
		return false;

		/*
		 * WebElement giftMessageBox = BrowserActions.checkLocator(driver,
		 * "#dwfrm_cart_shipments_i0_items_i" + index + "_giftmessagetext");
		 * return Utils.waitForElement(driver, giftMessageBox);
		 */
	}

	/**
	 * TO fill gift message box
	 * 
	 * @param index
	 * @throws Exception
	 */
	public void putTextInGiftMessageBox(int index) throws Exception {
		WebElement giftMessageBox = driver
				.findElement(By.cssSelector("#dwfrm_cart_shipments_i0_items_i" + index + "_giftmessagetext"));
		BrowserActions.clickOnElement(giftMessageBox, driver, "Clicking on gift message");
		String Message = RandomStringUtils.randomAlphanumeric(250).toLowerCase();
		BrowserActions.typeOnTextField(giftMessageBox, Message, driver, "gift message");
		Utils.waitForPageLoad(driver);
	}

	public CheckoutPage clickCheckoutInOrderSummaryForRegUser() throws Exception {
		BrowserActions.clickOnElement(btnCheckoutInOrderSummary, driver, "Checkout button in Order Summary");
		Utils.waitForPageLoad(driver);
		if (!Utils.waitForElement(driver, tabShipping)) {
			BrowserActions.clickOnElement(btnCheckoutInOrderSummary, driver, "Checkout button in Order Summary");
			Utils.waitForPageLoad(driver);
		}
		return new CheckoutPage(driver).get();
	}

	/**
	 * To check bag is empty
	 * 
	 * @return
	 */
	public boolean isShoppingBagEmpty() {

		return productList.size() == 0;

	}

	public void removeItemsFromBag() throws Exception {
		do {
			WebElement lnkRemove = driver.findElement(By.cssSelector("button[name*='deleteProduct']"));
			BrowserActions.clickOnElement(lnkRemove, driver, "Remove Button");
			Utils.waitForPageLoad(driver);
		} while (productList.size() != 0);
	}

	public void removeCouponsFromBag() throws Exception {
		List<WebElement> lnkRemoveCoupon = driver
				.findElements(By.cssSelector(".rowcoupons button[name*='deleteCoupon']"));
		for (int i = 0; i < lnkRemoveCoupon.size(); i++) {
			BrowserActions.clickOnElement(lnkRemoveCoupon.get(i), driver, "Remove Button");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To click the EditDetails Link
	 * 
	 * @return UpdateShoppingBag Page
	 * @throws Exception
	 */
	public void clickEditDetails(String productName) throws Exception {
		String ShoppingBagProductName = null;
		for (WebElement element : lstProductNames) {
			ShoppingBagProductName = BrowserActions.getText(driver, element, "Product Name");
			BrowserActions.scrollToViewElement(element, driver);
			if (productName.contains(ShoppingBagProductName)) {
				WebElement element1 = element.findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector(".item-user-actions>.item-edit-details>a"));
				BrowserActions.scrollToViewElement(element1, driver);
				BrowserActions.clickOnElement(element1, driver, "Clicking on Edit Details");
			}
		}
		Utils.waitForElement(driver, editDetailsPopup);
		BrowserActions.scrollToViewElement(editDetailsPopup, driver);
	}

	public String getElementType() throws Exception {
		String tagOfElement = productName.getTagName();
		return tagOfElement;
	}

	public void closeEditDetailsModal() throws Exception {
		BrowserActions.clickOnElement(btnCloseUpdateShoppingBAgPage, driver, "Update Shopping Bag Close Button");
		Utils.waitForPageLoad(driver);
	}

	public void clickUpdateShoppingBag() throws Exception {
		BrowserActions.scrollToViewElement(btnAddtoBag, driver);
		BrowserActions.clickOnElement(btnAddtoBag, driver, "Update Shopping Bag Close Button");
		Utils.waitForPageLoad(driver);
	}

	public String selectQuantity(String... qty) throws Exception {
		// checking size drop down have one value or more that one
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver, SELECTED_QTY);
		BrowserActions.scrollToViewElement(qtyDefaultValue, driver);
		if (qty.length > 0) {
			BrowserActions.clickOnElement(qtyDefaultValue, driver, "Clicked Size Button");
			BrowserActions.nap(1);
			try {
				List<WebElement> listOfqty = driver.findElements(By.cssSelector("div[class='quantity'] ul>li"));
				if (qty[0].equalsIgnoreCase("random")) {
					int rand = Utils.getRandom(0, listOfqty.size());
					BrowserActions.clickOnElement(listOfqty.get(rand), driver, "Select size option");

				} else if (listOfqty.size() > 0) {
					String optionValue = qty[0].toString();
					WebElement qtyoption = BrowserActions.getMachingTextElementFromList(listOfqty, optionValue,
							"equals");
					BrowserActions.clickOnElement(qtyoption, driver, "Size drop down options");

				}
			} catch (Exception e) {
				Log.event("Quantity is selected as default");
			}
		}
		waitForSpinner();
		return BrowserActions.checkLocator(driver, SELECTED_QTY).getText();
	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(2);
		Utils.waitUntilElementDisappear(driver, ShoppingBagspinner);
		BrowserActions.nap(2);
	}

	/**
	 * To select the color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public String selectColor() throws Exception {
		String selectedColorName = null;
		List<WebElement> listOfColor = driver.findElements(By.cssSelector(LIST_OF_COLOR));

		if ((listOfColor.size() == 0)) {
			WebElement defaultColor = BrowserActions.checkLocator(driver, SELECTED_COLOR);
			BrowserActions.scrollToViewElement(defaultColor, driver);
			return selectedColorName = BrowserActions.getTextFromAttribute(driver, defaultColor, "alt",
					"Alt attribute value");
		}

		if (listOfColor.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0, listOfColor.size());
			BrowserActions.scrollToViewElement(listOfColor.get(rand), driver);
			BrowserActions.clickOnElement(listOfColor.get(rand), driver, "select color");
			waitForSpinner();
			WebElement selectedColor = BrowserActions.checkLocator(driver, SELECTED_COLOR);
			selectedColorName = BrowserActions.getTextFromAttribute(driver, selectedColor, "alt",
					"Alt attribute value");
		} else {
			throw new Exception("Color swatched not available (or) not selectable in the UI");
		}
		return selectedColorName;
	}

	public String getTextFromRemainCharInGiftMessage(int index) throws Exception {
		return BrowserActions.getText(driver, productList.get(index).findElement(By.cssSelector(".char-count")),
				"Water Mark Text");
	}

	public boolean verifyWaterMarkByIndex(int index) throws Exception {
		WebElement element = productList.get(index).findElement(By.cssSelector(".char-count"));
		if (Utils.waitForElement(driver, element))
			return true;
		return false;
	}

	public void removeAddedCartItemsInShoppingBag() throws Exception {
		int removeButtonsCount = lstBtnRemove.size();
		BrowserActions.scrollToViewElement(btnMyAccount, driver);
		String signStatus = BrowserActions.getText(driver, btnMyAccount, "MyAccount text");
		if (signStatus.contains("My Account")) {
			if (getMinicartCount() > 0) {
				// BrowserActions.clickOnElement(iconMinicart, driver,
				// "View bag button");

				for (int i = 0; i < removeButtonsCount; i++) {
					BrowserActions.clickOnElement(lstBtnRemove.get(0), driver, "Remove button");

				}
			} else {
				Log.event("Minicart is empty.");
			}

		} else
			Log.message("Please login with valid credential");
	}

	public void clickOnEditDetailsCancel() throws Exception {
		BrowserActions.clickOnElement(cancelEditDetails, driver, " Clicking On 'Cancel' In the EditDetails Pop up");
		Utils.waitForPageLoad(driver);
	}

	public void closeEditDetailsByEsc() throws Exception {
		Utils.waitForElement(driver, editDetailsPopup);
		Actions action = new Actions(driver);
		action.click(editDetailsPopup).build().perform();
		action.sendKeys(Keys.ESCAPE).build().perform();

	}

	public void closeEditDetilsByClickingOtherElement() throws Exception {
		Utils.waitForElement(driver, btnCheckoutInOrderSummary);
		BrowserActions.scrollToViewElement(btnCheckoutInOrderSummary, driver);
		BrowserActions.javascriptClick(btnCheckoutInOrderSummary, driver, "Closing the EditDetails PopUp");
	}

	public void clickOnAddToWishList(String productName) throws Exception {
		String ShoppingBagProductName = null;
		for (WebElement element : lstProductNames) {
			ShoppingBagProductName = BrowserActions.getText(driver, element, "Product Name");
			BrowserActions.scrollToViewElement(element, driver);
			if (productName.contains(ShoppingBagProductName)) {
				WebElement element1 = element.findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector("a[title='Add this item to wishlist']"));
				BrowserActions.clickOnElement(element1, driver, "Adding Item to WishList");
			}
		}
	}

	public String checkItemAddedToWishList(String productName) throws Exception {
		String txtwishlist = null;
		txtwishlist = BrowserActions.getText(driver, txtItemAddedWishList, "text WishList");
		return txtwishlist;
	}

	public RegistrySignedUserPage clickOnRegistry(String productName) throws Exception {
		String ShoppingBagProductName = null;
		for (WebElement element : lstProductNames) {
			ShoppingBagProductName = BrowserActions.getText(driver, element, "Product Name");
			BrowserActions.scrollToViewElement(element, driver);
			if (productName.contains(ShoppingBagProductName)) {
				WebElement element1 = element.findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector("a[title='Add this product to gift registry']"));
				BrowserActions.clickOnElement(element1, driver, "Adding Item to Registry");
				return new RegistrySignedUserPage(driver);
			}
		}
		return null;

	}

	public boolean checkAddToRegistry(int index) throws Exception {
		String txtregistry = "Registry";
		String txtRegistryLink = BrowserActions.getText(driver, lstAddToRegistryLink.get(index),
				"Text from Registry Link");
		if (txtRegistryLink.contains(txtregistry)) {
			return true;
		}
		return false;
	}

	/**
	 * To get index of product
	 *
	 * @param ProductName
	 * @return index
	 * @throws Exception
	 */
	public int getIndexByProductName(String productName) throws Exception {
		for (int i = 0; i < lstProductNames.size(); i++) {
			if (productName.contains(lstProductNames.get(i).getText()))
				return i;
		}
		return (-1);

	}

	/**
	 * 
	 * To get the estimated value of the order summary
	 * 
	 * @return
	 * @throws Exception
	 */
	public float getEstimatedValue() throws Exception {
		String estimatedValue = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, orderSummaryTotal, "Total order summary")).trim()
				.replace("$", "");
		return Float.parseFloat(estimatedValue);

	}

	/**
	 * @throws Exception
	 *             To remove cart items by index index starts from 0
	 */

	public ShoppingBagPage removeItemsByIntex(int index) throws Exception {
		Utils.waitForPageLoad(driver);
		if (getProductCountInBag() > index) {
			int productIndex = 0;
			for (WebElement ele : removeProductByIndex) {
				if (productIndex == index) {
					BrowserActions.clickOnElement(ele, driver, "select any product");
					break;
				}
				productIndex++;
			}
		} else {
			throw new Exception("Product is not show ");
		}
		return new ShoppingBagPage(driver).get();
	}

	public int getProductCountInBag() throws Exception {
		return driver.findElements(By.cssSelector("#cart-table >tbody > tr")).size();
	}

	/**
	 * To get Product Name
	 * 
	 * @Return Name
	 * @throws Exception
	 */

	public String PrdName() throws Exception {

		String Productname = BrowserActions.getText(driver, txtProductName, "Product name in shopping bag");
		return Productname;
	}

	/**
	 * To get Product Size
	 * 
	 * @Return Size
	 * @throws Exception
	 */

	public String PrdSize() throws Exception {

		String ProductSize = BrowserActions.getText(driver, lblSize.get(0), "Size in shopping bag");
		return ProductSize;
	}

	/**
	 * To get Product Color
	 * 
	 * @Return Color
	 * @throws Exception
	 */
	public String PrdColor() throws Exception {
		String ProductColor = BrowserActions.getText(driver, lblColor.get(0), "color in shopping bag");
		return ProductColor;
	}

	/**
	 * To get Product Subtotal
	 * 
	 * @Return Subtotal
	 * @throws Exception
	 */
	public String PrdSubtotal() throws Exception {

		String ProductSubtotal = BrowserActions.getText(driver, lblSubtotal.get(0), "Subtotal name in shopping bag");
		return ProductSubtotal;
	}

	public void clickOnProductName() throws Exception {
		BrowserActions.clickOnElement(txtProductName, driver, "Product Name");
	}

	public PdpPage clickProductName() throws Exception {
		BrowserActions.clickOnElement(lstProductNames.get(Utils.getRandom(0, lstProductNames.size())), driver,
				"Product Name");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
		// return new PDPPopUpPage(driver).get();
	}

	public String getQuantityValue(int index) throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(5);
		return BrowserActions.getText(driver, lstupdatedQty.get(index), " Upadated Quantity from ShoppingBag Page");
	}

	public String getColorValue(int index) throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(5);
		return BrowserActions.getText(driver, lstUpdatedColor.get(index), " Upadated Quantity from ShoppingBag Page");
	}

	/**
	 * To get Merchandise total
	 * 
	 * @throws Exception
	 * @Return Merchandise total
	 */
	public float getMerchandiseTotal() throws Exception {
		String merchandideValue = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, lblOrderMerchandiseTotal, "Merchandise Total"))
				.trim().replace("$", "");
		return Float.parseFloat(merchandideValue);

	}

	/**
	 * To get Shipping Amount
	 * 
	 * @throws Exception
	 * @Return shippingAmount
	 */
	public float getShippingAmount() throws Exception {
		String shippingValue = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, lblOrderShippingAmount, "Shipping Amount")).trim()
				.replace("$", "");
		return Float.parseFloat(shippingValue);
	}

	/**
	 * To get Shipping Amount
	 * 
	 * @throws Exception
	 * @Return shippingAmount
	 */
	public float getShippingAmountDiscount() throws Exception {
		String shippingValue = (BrowserActions.getText(driver, lblOrderShippingAmountDiscount,
				"Shipping Amount Discount")).trim().replace("-$", "");
		return Float.parseFloat(shippingValue);
	}

	/**
	 * To get Sales tax
	 * 
	 * @throws Exception
	 * @Return Sales tax
	 */
	public float getSalesTax() throws Exception {
		String saleTax = null;
		try {
			saleTax = Utils.checkPriceWithDollar(BrowserActions.getText(driver, lblOrdereSalestax, "Saled Tax")).trim();
			if (saleTax.contains("$")) {
				saleTax = saleTax.trim().replace("$", "");
			} else {
				saleTax = "0";
			}
		} catch (Exception e) {
			NumberFormatException x = new NumberFormatException();
			if (e.equals(x) || e.getMessage().contains("$")) {
				saleTax.replace("$", "");
				return Float.parseFloat(saleTax);
			}
			// TODO: handle exception
		}
		return Float.parseFloat(saleTax);
	}

	/**
	 * To get Estimated Order Total
	 * 
	 * @throws Exception
	 * @Return Estimated Order Total
	 */
	public String getEstimatedOrderTotal() throws Exception {
		return BrowserActions.getText(driver, lblOrdertotal, "Estimated Order Total");
	}

	/**
	 * To get product Details
	 * 
	 * @return
	 * @return
	 * @Return product Details
	 * 
	 *         Last Modified By : Dhanapal
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productSet = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < productList.size(); i++) {
			LinkedHashMap<String, String> productDetail = new LinkedHashMap<String, String>();
			productDetail.put("ProductName", lstProductNames.get(i).getText());
			productDetail.put("Upc", UPCinBagPage.get(i).getText().trim().split("\\:")[1].trim());
			productDetail.put("Color", lblColor.get(i).getText());
			productDetail.put("Size", lblSize.get(i).getText());

			String price = new String();
			if (lblPriceSectionInProduct.get(i).getText().contains("Clearance")) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".now-price")).getText().trim()
						.split("\\$")[1].trim();
			} else if (lblPriceSectionInProduct.get(i).getText().contains("Now")) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".now-price")).getText().trim()
						.split("\\$")[1].trim();
			} else if (Utils.waitForElement(driver,
					lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".standard-price")))) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".standard-price")).getText().trim()
						.replace("$", "");
			}

			productDetail.put("Price", price);
			productDetail.put("Quantity", lstupdatedQty.get(i).getText());
			productSet.add(productDetail);
		}
		return productSet;
	}

	/**
	 * To get SubTotal Amount After Updated Quantity Calculating sub total value
	 * after updating Quantity Quantity =2; price = 10; subTotal = Quantity *
	 * price Last modified By - Nandhini.Bala & Date modified - 1/24/2017
	 * 
	 * @Return subTotal
	 */
	public float getSubTotalAfterUpdatedQuantity() throws Exception {
		float subTotal = 0;
		float couponPrice = 0;
		String price = new String();
		String quantity;
		float surCharges = 0;
		for (int i = 0; i < productList.size(); i++) {
			if (lblPriceSectionInProduct.get(i).getText().contains("Clearance")) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".now-price")).getText().trim()
						.split("\\$")[1].trim();
			} else if (lblPriceSectionInProduct.get(i).getText().contains("Now")) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".now-price")).getText().trim()
						.split("\\$")[1].trim();
			} else if (Utils.waitForElement(driver,
					lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".standard-price")))) {
				price = lblPriceSectionInProduct.get(i).findElement(By.cssSelector(".standard-price")).getText().trim()
						.replace("$", "");
			}
			quantity = BrowserActions.getText(driver, lstupdatedQty.get(i), "Updated Quantity").replace("$", "");

			if (Utils.waitForElement(driver, txtShippingSurcharge)) {
				surCharges = Float
						.parseFloat(BrowserActions.getText(driver, txtShippingSurcharge, "surcharges").split("\\$")[1]);
			}
			try {
				couponPrice = Float.parseFloat(
						(BrowserActions.getText(driver, lblPriceCoupon.get(i), "Coupon Price").split("\\$")[1]));
			} catch (Exception e) {
				// TODO: handle exception
			}

			subTotal = (Float.parseFloat(price) * Float.parseFloat(quantity)) + surCharges - couponPrice;
		}
		return subTotal;
	}

	/**
	 * To get Calculate Estimate dOrder Total estimattedOrderTotal =
	 * estimattedOrderTotal = (merchandiseTotal + shippingTotal + salesTax +
	 * Gift Box + Surcharge ) - couponAmount
	 * 
	 * @Return estimattedOrderTotal Last modified By - Nandhini.Bala & Date
	 *         modified - 1/24/2017
	 */
	public float getCalculateEstimatedOrderTotal() throws Exception {
		float estimattedOrderTotal = 0;
		float salesTax = 0;
		float giftAmount = 0;
		float merchandiseTotal = getMerchandiseTotal();
		float shippingTotal = getShippingAmount();
		float surchargeAmount = 0;
		float couponAmount = 0;
		float shippingDiscount = 0;

		if (lstTxtodrerdatails.getText().contains("Sales Tax")) {
			salesTax = getSalesTax();
		}

		if (lstTxtodrerdatails.getText().contains("Gift Box")) {
			giftAmount = getGiftBoxAmount();
		}

		if (lstTxtodrerdatails.getText().contains("Surcharge")) {
			surchargeAmount = getSurchargeAmount();
		}

		if (lstTxtodrerdatails.getText().contains("Total Coupon Savings")) {
			couponAmount = getCouponAmount();
		}

		if (Utils.waitForElement(driver, lblOrderShippingAmountDiscount)) {
			shippingDiscount = getShippingAmountDiscount();
		}
		estimattedOrderTotal = (merchandiseTotal + shippingTotal + salesTax + giftAmount + surchargeAmount)
				- couponAmount - shippingDiscount;

		return estimattedOrderTotal;
	}

	public void clickMyAccountLink() throws Exception {
		BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
	}

	/**
	 * To check add gift box
	 * 
	 * @throws Exception
	 */
	public void checkAddGiftBox() throws Exception {
		BrowserActions.clickOnElement(chkAddGiftBox, driver, "Click add gift box");
	}

	/**
	 * To get GiftBox Amount
	 * 
	 * @return
	 * @throws Exception
	 */
	public float getGiftBoxAmount() throws Exception {
		String giftAmount = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, lblOrdereGiftBox, "Get Gift box amount")).trim()
				.replace("$", "");
		return Float.parseFloat(giftAmount);
	}

	/**
	 * To check add gift box
	 * 
	 * @throws Exception
	 */
	public void checkAddGiftBoxByIndex(int index) throws Exception {
		WebElement chkGiftBox = driver
				.findElement(By.cssSelector("#dwfrm_cart_shipments_i0_items_i" + (index - 1) + "_giftbox"));
		BrowserActions.selectRadioOrCheckbox(chkGiftBox, "YES");// (chkAddGiftBox,
																// driver,
																// "Click add
																// gift box");
		Utils.waitForPageLoad(driver);
	}

	public String getNoOfQtyInShoppingBagPage() throws Exception {
		Utils.waitForElement(driver, getNoOfQtyInShoppingBag);
		String Qty = BrowserActions.getText(driver, getNoOfQtyInShoppingBag, "No of Qty");
		return Qty;
	}

	public String getGlobalMessage() throws Exception {
		Utils.waitForElement(driver, GloabalMessage);
		String str = BrowserActions.getText(driver, GloabalMessage, "Gloabal Message");
		System.out.println(str);
		return str;
	}

	public String getLineMessage() throws Exception {
		Utils.waitForElement(driver, LineMessage);
		String str1 = BrowserActions.getText(driver, LineMessage, "Line Message");
		System.out.println(str1);
		return str1;
	}

	/**
	 * To check add gift box
	 * 
	 * @throws Exception
	 */
	public void checkAddGiftBoxByProductName(String prdName) throws Exception {

		// WebElement chkGiftBox =
		// driver.findElement(By.cssSelector("#dwfrm_cart_shipments_i0_items_i"+
		// (index-1) +"_giftbox"));
		for (int i = 0; i < lstItemInCart.size(); i++) {
			String productName = lstItemInCart.get(i)
					.findElement(By.cssSelector(".product-list-item .item-edit-details > a")).getText().trim();
			if (productName.contains(prdName)) {
				WebElement chkGiftBox = lstItemInCart.get(i).findElement(By.cssSelector("input[id*='giftbox']"));
				BrowserActions.selectRadioOrCheckbox(chkGiftBox, "YES");// (chkAddGiftBox,
																		// driver,
																		// "Click
																		// add
																		// gift
																		// box");
				Utils.waitForPageLoad(driver);
				break;
			}
		}
	}

	public String gettextfromOrderSummaryinShoppingPage() throws Exception {
		String OrderDetailsInShoppingPage = BrowserActions.getText(driver, txtOrderSummaryDetails,
				" Order summary details from ShoppingBag Page");
		return OrderDetailsInShoppingPage;
	}

	/**
	 * return cart merging message
	 * 
	 * @throws Exception
	 */
	public String getmsgCartMerging() throws Exception {
		return BrowserActions.getText(driver, msgCartMerging,
				" MSG : Items from your other shopping bag have been added to your current shopping bag");
	}

	public String productPricingShoopingCart() throws Exception {
		String redColouredProductPricingValue = BrowserActions.getText(driver, productPricing,
				"To get the Product Pricing value on the Mini Cart");

		return redColouredProductPricingValue;

	}

	/**
	 * To get the Product price styling in the shopping cart page
	 * 
	 * @return
	 * @throws Exception
	 */

	public boolean verifyProductPrice() throws Exception {
		String color = SearchUtils.getLabelColor(productPricing);
		if ((color.equals("#c00)")))
			return true;

		return false;
	}

	public String surchargeTxtDisplay() throws Exception {
		String surchargeGetTxt = BrowserActions.getText(driver, txtSurchargeOnCartPage,
				"Surcahrge Text Should be displayed in Shopping Cart page");
		return surchargeGetTxt;
	}

	public String verifyFreeShippingMessage() throws Exception {
		String freeShippingOnMyBag = BrowserActions.getText(driver, freeShippingMessageMyBag,
				"To Verify the Free Shipping Message on the My Bag Page");
		return freeShippingOnMyBag;
	}

	public String getStandardSubtotalPrice() {
		String standardprice = txtSubtotalspan.getText().toString();
		if (standardprice.length() > 6 && standardprice.contains(" ")) {
			standardprice = standardprice.split(" ", 2)[0];
		}
		return standardprice;
	}

	public void clickSignOut() throws Exception {

		BrowserActions.clickOnElement(lnkSignOut, driver, "Click on the SIgnout button");
	}

	public void ChangeQuantity(int index) throws Exception {
		clickqtyDropown();
		BrowserActions.clickOnElement(lstQty.get(index - 1), driver, "Size");
		Utils.waitForPageLoad(driver);
	}

	public void clickqtyDropown() throws Exception {
		BrowserActions.clickOnElement(drpQty, driver, "Quantity Dropdown");
	}

	public String getProductImageSrc() throws Exception {

		String imageUrl = BrowserActions.getTextFromAttribute(driver, productImg, "src", "href of indexed image");
		return imageUrl;
	}

	public boolean getBagmergeGloballevelalert() throws Exception {
		String cartMergeStatus = null;
		boolean status = false;

		cartMergeStatus = BrowserActions.getText(driver, lblGlobalitemalertmessage, "Shopping Cart merge information");
		if (cartMergeStatus
				.equalsIgnoreCase("Items from your other shopping bag have been added to your current shopping bag.")) {
			status = true;
		}

		return status;
	}

	/**
	 * To Get merge cart alert message at item level
	 * 
	 * @throws Exception
	 */
	public boolean getBagmergeLineitemlevelalert() throws Exception {
		String cartMergeStatus = null;
		boolean status = false;

		cartMergeStatus = BrowserActions.getText(driver, lblLineitemalertmessage, "Shopping Cart merge information");
		if (cartMergeStatus.equalsIgnoreCase("This item in your bag has been changed.")) {
			status = true;
		}

		return status;
	}

	public String getSelectedGiftProductPrice() throws Exception {
		String giftPdctPrice = BrowserActions.getText(driver, giftProductPrice, "Gift Product Price");
		giftPdctPrice = giftPdctPrice.split("Subtotal: ")[1];
		return giftPdctPrice;
	}

	/**
	 * getPromotionMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getPromotionMessage() throws Exception {
		return BrowserActions.getText(driver, lblPromotionMessage, "Promotion message").trim();
	}

	/**
	 * getPromotionMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getPromotionPageLinl() throws Exception {
		return BrowserActions.getText(driver, cartPageLink, "Promotion message").trim();
	}

	/**
	 * To getPromotionProductDetails
	 * 
	 * @return
	 * @return
	 * @Return product Details
	 */
	public LinkedList<LinkedHashMap<String, String>> getPromotionProductDetails() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productSet = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < lblPromotionProduct.size(); i++) {
			LinkedHashMap<String, String> productDetail = new LinkedHashMap<String, String>();
			productDetail.put("ProductName", lblPromotionProductName.get(i).getText());
			productDetail.put("Upc", lblPromotionProductUpc.get(i).getText());
			productDetail.put("Color", lblPromotionProductColor.get(i).getText());
			productDetail.put("Size", lblPromotionProductSize.get(i).getText());
			productDetail.put("Subtotal", lblPromotionProductSubTotal.get(i).getText());
			productSet.add(productDetail);
		}
		return productSet;
	}

	/**
	 * to get the cose details from shopping cart page
	 * 
	 * @return
	 * @throws Exception
	 * 
	 *             Last Modified By : Dhanapal.K Last Modified Date : 3/1/2017
	 *             Last Modified By Nandhini B 24/1/2016
	 */
	public LinkedHashMap<String, String> getOrderSummaryDetails() throws Exception {
		LinkedHashMap<String, String> orderSummaryDetails = new LinkedHashMap<String, String>();
		String merchandiseTotal = new String();
		String couponSavings = new String();
		String giftbox = new String();
		String surcharge = new String();
		String estimatedShipping = new String();
		String salesTax = new String();
		String estimatedOrderTotal = new String();

		if (Utils.waitForElement(driver, lblOrderMerchandiseTotal)) {
			merchandiseTotal = lblOrderMerchandiseTotal.getText().trim().trim().replace("$", "");
			orderSummaryDetails.put("MerchandiseTotal", merchandiseTotal);
		}

		// if(Utils.waitForElement(driver, BrowserActions.checkLocator(driver,
		// ".order-discount.discount"))){

		if (Utils.waitForElement(driver, lblOrderCouponSavings)) {
			couponSavings = lblOrderCouponSavings.getText().trim().replace("$", "").trim();
			orderSummaryDetails.put("CouponSavings", couponSavings);
		}

		if (Utils.waitForElement(driver, lblOrdereGiftBox)) {
			giftbox = lblOrdereGiftBox.getText().trim().replace("$", "").trim();
			orderSummaryDetails.put("Giftbox", giftbox);
		}
		if (Utils.waitForElement(driver, lblOrderSurcharge)) {
			surcharge = lblOrderSurcharge.getText().trim().trim().replace("$", "");
			orderSummaryDetails.put("Surcharge", surcharge);
		}

		if (Utils.waitForElement(driver, lblOrderShippingAmount)) {
			estimatedShipping = lblOrderShippingAmount.getText().trim().replace("$", "").trim();
			orderSummaryDetails.put("EstimatedShipping", estimatedShipping);
		}

		if (Utils.waitForElement(driver, lblOrdereSalestax)) {
			salesTax = lblOrdereSalestax.getText().trim().replace("$", "").trim();
			orderSummaryDetails.put("SalesTax", salesTax);
		}

		if (Utils.waitForElement(driver, lblOrdertotal)) {
			estimatedOrderTotal = lblOrdertotal.getText().trim().replace("$", "").trim();
			orderSummaryDetails.put("EstimatedOrderTotal", estimatedOrderTotal);
		}

		return orderSummaryDetails;

	}

	/**
	 * To get the upc in mini cart
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getUPCInMinicartPage() throws Exception {
		ArrayList<String> UPCinMinicart = new ArrayList<String>();
		for (WebElement element : lblUpcofProduct) {
			UPCinMinicart.add(BrowserActions.getText(driver, element, "Upc"));
		}
		return UPCinMinicart;

	}

	/**
	 * To select quantiy using index
	 * 
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String selectQtyByIndex(int index) throws Exception {
		clickQtyDropown();
		BrowserActions.clickOnElement(lstQty.get(index - 1), driver, "Quantity");
		String quantity = BrowserActions.getText(driver, lstQty.get(index - 1), "quantity");
		Utils.waitUntilElementDisappear(driver, spinner);
		return quantity;
	}

	/**
	 * To click Size dropdown
	 * 
	 * @throws Exception
	 */
	public void clickQtyDropown() throws Exception {
		BrowserActions.clickOnElement(drpQty, driver, "Size dropdown");
	}

	public String getCouponDiscountValue() throws Exception {
		BrowserActions.scrollToView(txtCouponDiscountValue, driver);
		return BrowserActions.getText(driver, txtCouponDiscountValue, "Coupon Success Message").replace("$", "");
	}

	/**
	 * To get total coupon savings
	 * 
	 * @throws Exception
	 * @Return couponSavings
	 */
	public String getCouponSavings() throws Exception {
		String couponSavings = BrowserActions.getText(driver, lblOrderCouponSavings, "Order Coupon Savings Total")
				.trim().replace("$", "");
		return couponSavings;

	}

	public String getCouponSuccessMsg() throws Exception {
		return BrowserActions.getText(driver, txtCouponSuccess, "Coupon Success Message");
	}

	public boolean verifyAddFreeGift() {
		boolean status = false;
		if (lnkAddFreeGiftToBag.isDisplayed() && lnkAddFreeGiftToBag.isEnabled()) {
			status = true;
		}
		return status;
	}

	/**
	 * To get GiftBox Amount
	 * 
	 * @return
	 * @throws Exception
	 */
	public float getSurchargeAmount() throws Exception {
		String surchargeAmount = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, lblOrderSurcharge, "Get surcharge amount")).trim()
				.replace("$", "");
		return Float.parseFloat(surchargeAmount);
	}

	public boolean verifyGlobalErrorMessageColour() throws Exception {
		boolean status3 = false;
		String cssValueOfColor = "rgba(76, 122, 57, 1)";
		status3 = Utils.verifyCssPropertyForElement(couponTickMark, cssValueOfColor, "");
		return status3;
	}

	public String getCouponText() throws Exception {
		return BrowserActions.getText(driver, txtCoupon, "Coupon Applied Text");
	}

	/**
	 * getCouponAmount
	 * 
	 * @return
	 * @throws Exception
	 */
	public float getCouponAmount() throws Exception {
		String couponAmount = BrowserActions.getText(driver, lblOrderCouponSavings, "Coupon Amount").replace("-$", "")
				.trim();
		return Float.parseFloat(couponAmount);
	}

	/**
	 * @throws Exception
	 *             Get the text of the free gift added product
	 * 
	 */
	public String freeGiftAddedMessage() throws Exception {

		String freegiftAdded = BrowserActions.getText(driver, lblFreeGiftMessage,
				"Get the text message of the free gift Added product");
		return freegiftAdded;
	}

	/**
	 * To get the standard price i.e $0 for free gift added product
	 * 
	 * @return
	 */
	public String getStandardSubtotalPriceFreeAddedGift() {
		String standardprice = txtSubtotalspanOfFreeGift.getText().toString();
		return standardprice;
	}

	public String getProductPriceFromshoppingBagpage() throws Exception {
		String price = BrowserActions.getText(driver, productPriceInShopping, "Product Price in Shopping bag Page ")
				.replace("$", "");
		return price;
	}

	public String getCouponErrMsg() throws Exception {
		return txtInvalidCouponError.getText().trim();
	}

	public String getStandardPriceMsg() throws Exception {
		String price = txtStandardPrice.getText();
		return price;
	}

	public double getNowPriceMsg() throws Exception {

		double NowPriceValue = Double
				.parseDouble(BrowserActions.getText(driver, txtNowPrice, "Now Price Value").replace("$", ""));
		return NowPriceValue;
	}

	/**
	 * To get the text message of the product which is restricted for shipping.
	 * 
	 * @return txtRestrictionMessage
	 * @throws Exception
	 * 
	 */

	public String getTextRestrictionShippingMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		String txtRestrictionMessage = BrowserActions.getText(driver, txtLineItemRestriction,
				"The Product Restricted for shipping");
		return txtRestrictionMessage;
	}

	/**
	 * To get the text message of the product which is restricted for state
	 * shipping.
	 * 
	 * @return txtRestrictionMessage
	 * @throws Exception
	 * 
	 */

	public String getTextRestrictionStateShippingMessage() throws Exception {
		String txtRestrictionStatetMessage = BrowserActions.getText(driver, txtLineItemStateRestriction,
				"The Product Restricted for shipping state");
		return txtRestrictionStatetMessage;
	}

	/**
	 * To get the surcharge text on the shopping cart page
	 * 
	 * @throws Exception
	 */
	public String getTextSurchargeMessage() throws Exception {

		String txtSurchargeMessage = BrowserActions.getText(driver, txtSurchargeOnShoppingCartPage,
				"Text of the Shipping Surcharge on the shopping cart page");
		return txtSurchargeMessage;
	}

	/**
	 * To get the subtotal line item price
	 * 
	 * @throws Exception
	 */
	public String txtSubtotalLineItemPrice() throws Exception {
		String txtSubtotalPrice = BrowserActions.getText(driver, txtSubtotalPriceLineItem,
				"To get the text price of the Subtotal");
		return txtSubtotalPrice;
	}

	/**
	 * To get the subtotal line item price
	 * 
	 * @throws Exception
	 */
	public String txtShippingSurchargeAmmount() throws Exception {
		String txtSurcharge = BrowserActions.getText(driver, txtShippingSurcharge,
				"To get the text price of the Shipping Surcharge");
		return txtSurcharge;
	}

	public void clickMobileHamburgerMenu() throws Exception {

		BrowserActions.scrollToViewElement(lnkHamburger, driver);
		BrowserActions.javascriptClick(lnkHamburger, driver, "Mobile Hamburger Link");
		BrowserActions.nap(1);
		if (Utils.waitForElement(driver, islnkHamburgerOpened)) {
			Log.event("clicked Hamburger Menu");
		} else {
			throw new Exception("Hamburger Menu didn't open up");
		}
	}

	public void clickOnMyAccountLink() throws Exception {
		if (runPltfrm == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.scrollToViewElement(myAccountPageMobile, driver);
			BrowserActions.clickOnElement(myAccountPageMobile, driver, "My Account Link");
		} else {
			BrowserActions.scrollToViewElement(myAccountPage, driver);
			BrowserActions.clickOnElement(myAccountPage, driver, "My Account Link");
		}
	}

	/**
	 * To get add GiftBox price
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getAddGiftBoxPrice() throws Exception {
		String txtAddagiftbox = BrowserActions.getText(driver, chckboxAddagiftboxText, "gift box price");
		String[] txtsplit = txtAddagiftbox.split("Box");
		String[] pricesplit = txtsplit[1].split(" each");
		String price = pricesplit[0];
		price = price.replace("($", "");
		return price;
	}

	/**
	 * To get add GiftBox price from Order Summary
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getpriceofgiftboxfromordersummary() throws Exception {
		String giftboxpricefromodersummary = BrowserActions.getText(driver, ttlgiftboxpriceinestimatedshipping,
				"Gift box price from order summary");
		String[] giftboxpricefromordersummarysplit = giftboxpricefromodersummary.split("\\$");
		String pricefromorder = giftboxpricefromordersummarysplit[1];
		return pricefromorder;
	}

	/**
	 * To get Box and Ribbon Included Text
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getTextfromAddGiftBoxandRibbon() throws Exception {
		String txtAddagiftboxandribbon = BrowserActions.getText(driver, txtboxandribbon1stline,
				"Box and Ribbon message");
		return txtAddagiftboxandribbon;
	}

	/**
	 * To get Items not Wrapped Text
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextfromAddGiftItemsNotWrapped() throws Exception {
		String txtAddagiftboxitemswrapped = BrowserActions.getText(driver, txtboxandribbon2ndline,
				"Items not wrapped  message");
		return txtAddagiftboxitemswrapped;
	}

	/**
	 * to Verify add Gift Checkbox is Selected or not
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean isAddGiftCheckBoxSelected() {
		boolean status = false;
		WebElement element = chckgiftboxStatus;
		status = BrowserActions.isRadioOrCheckBoxSelected(element);
		return status;
	}

	/**
	 * getTextSubTotal
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextSubTotal() throws Exception {
		String txtSubtTotal = BrowserActions
				.getText(driver, txtSubtotalPriceLineItem, "To get the text price of the SubTotal").split("\\$")[1];
		return txtSubtTotal;
	}

	/**
	 * To get the Now Price
	 */
	public String getTextNowPrice() throws Exception {
		String txtOfNowPrice = BrowserActions.getText(driver, txtNowPrice, "Get the best now price");
		return txtOfNowPrice;
	}

	/**
	 * To get the Promotional message on the shopping cart page
	 * 
	 * @throws Exception
	 */

	public String getTextPrmotionalMessage() throws Exception {
		String txtBuyOneText = BrowserActions.getText(driver, promoFinalTxt, "To get the Promotional Text Message");
		return txtBuyOneText;

	}

	/**
	 * To check the rebate Text is present on the Shopping Cart page
	 * 
	 * @throws Exception
	 */

	public String getTextRebateMessage() throws Exception {
		String rebateText = BrowserActions.getText(driver, txtcartRebate,
				"To check Rebate message is displayed on the Cart page");
		return rebateText;

	}

	/**
	 * getUPC value of free gift item
	 */
	public String getUPCofFreeGift() throws Exception {
		BrowserActions.scrollToView(txtPromoProdUPC, driver);
		String txtUPC = BrowserActions.getText(driver, txtPromoProdUPC, "To get the UPC value of the Free Gift Item");
		return txtUPC;
	}

	public String getCostInShoppingBag() throws Exception {
		return BrowserActions.getText(driver, shippingMsg, "Payment Cost");
	}

	public String getEmptycarttitle() throws Exception {
		return BrowserActions.getText(driver, txtemptycarttitle, "Empty cart title").toString();
	}

	public String getPromoMessageInShoppingBagPage() throws Exception {
		BrowserActions.scrollToView(finalPromoMessage, driver);
		return BrowserActions.getText(driver, finalPromoMessage, "Fetching the Promotion Message On ShoppingBag Page");
	}

	public boolean getProductName(String productName) throws Exception {
		boolean status = false;
		for (int i = 0; i < lstProductNames.size(); i++) {
			if (lstProductNames.get(i).getText().trim().equals(productName.trim())) {
				status = true;
				break;
			}
		}
		return status;
	}

	/**
	 * To check the Free Shipping Text is present on the Shopping Cart page for
	 * desktp
	 * 
	 * @throws Exception
	 */

	public String getTextOfFreeShippingMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		String freeShipText = BrowserActions.getText(driver, promoFreeShipDsk,
				"Get text Free Shipping message is displayed on the Cart page");
		return freeShipText;

	}

	/**
	 * To check the Free Shipping Text is present on the Shopping Cart page for
	 * mobile
	 * 
	 * @throws Exception
	 */

	public String getTextOfFreeShippingMessageMob() throws Exception {

		String freeShipTextmob = BrowserActions.getText(driver, promoFreeShipMob,
				"Get text Free Shipping message is displayed on the Cart page");
		return freeShipTextmob;
	}

	public String getgpwMessage() throws Exception {
		String promoMessage = BrowserActions.getText(driver, gpwProductMessage, "Get GPW Promo Message");
		return promoMessage;
	}

	public void clickOnFindStorelnk() throws Exception {
		Utils.waitForElement(driver, btnFindStore);
		BrowserActions.javascriptClick(btnFindStore, driver, "btn Find Store");
	}

	public void applyZipCodeInShoppingBag(String txtZipCodeToEnter) throws Exception {
		Utils.waitForElement(driver, txtZipCode);
		BrowserActions.typeOnTextField(txtZipCode, txtZipCodeToEnter, driver, "Zip code Box");
		BrowserActions.clickOnElement(btnContinue, driver, "Zip code Box");
		Utils.waitForPageLoad(driver);
		BrowserActions.clickOnElement(btnSelectStore, driver, "Click on the Select Store button");
	}

	/**
	 * To get the text from order total
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromOrderTotal() throws Exception {
		return BrowserActions.getText(driver, fldEstimatedTotal.get(1), "Sales Tax");
	}

	/**
	 * To get the text from giftBox total
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromGiftBoxTotal() throws Exception {
		return BrowserActions.getText(driver, giftBoxTotal.get(1), "Gift Box");
	}

	/**
	 * To get the text from mrechandise total
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromOrderSummary() throws Exception {
		return BrowserActions.getText(driver, fldMerchandiseTotal.get(1), "Merchantise total value");
	}

	/**
	 * To get the text from shipping value
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromShipping() throws Exception {
		return BrowserActions.getText(driver, txtShippingValue, "Shipping value").replace("$", "");
	}

	/**
	 * To get the text from sales tax
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromSalesTax() throws Exception {
		return BrowserActions.getText(driver, fldSalesTax.get(1), "Sales Tax");
	}

	public void clcikOnBrandNameInShoppingBag() throws Exception {
		Utils.waitForElement(driver, ProductBrandlnk);
		BrowserActions.javascriptClick(ProductBrandlnk, driver, "Product Brandlnk");
	}

	public void clcikOnProductNameInShoppingBag() throws Exception {
		Utils.waitForElement(driver, ProductNamelnk);
		BrowserActions.javascriptClick(ProductNamelnk, driver, "Product Namelnk");
		Utils.waitForPageLoad(driver);
	}

	public void clcikOncancelLnkUpdatePopup() throws Exception {
		Utils.waitForElement(driver, cancelLnkUpdatePopup);
		BrowserActions.javascriptClick(cancelLnkUpdatePopup, driver, "Product cancellnk popup");
	}

	/**
	 * To get couponprice in ShoppingBagPage return couponprice
	 * 
	 * @throws Exception
	 */

	public float getAppliedCouponPrice() throws Exception {
		float couponPrice = 0;
		float coupontotal = 0;
		for (int i = 0; i < productList.size(); i++) {
			couponPrice = Float.parseFloat(
					(BrowserActions.getText(driver, lblPriceCoupon.get(i), "Coupon Price").split("\\$")[1]));
			coupontotal += couponPrice;
		}
		return coupontotal;
	}

	public ArrayList<String> getTextFromLineItem() throws Exception {
		ArrayList<String> toBeReturned = new ArrayList<String>();
		for (WebElement element : txtLineItems) {
			toBeReturned.add(BrowserActions.getText(driver, element, "Sales Tax"));
		}
		return toBeReturned;
	}

	public void clickOnRemoveCouponLnk() throws Exception {
		Utils.waitForElement(driver, btnRemoveCoupon);
		BrowserActions.javascriptClick(btnRemoveCoupon, driver, "btnR emove Coupon");
		Utils.waitForPageLoad(driver);
	}

	public String getDescriptionOfGift() throws Exception {
		String promoMessage = BrowserActions.getText(driver, description,
				"Descrption of the GPW Free gift From Pop Up");
		return promoMessage;
	}

	public void clickOnViewDetails() throws Exception {
		Utils.waitForElement(driver, viewDetails);
		BrowserActions.scrollToView(viewDetails, driver);
		BrowserActions.javascriptClick(viewDetails, driver, "View Details");
	}

	public void clickOnclosePopUp() throws Exception {
		Utils.waitForElement(driver, closePopUp);
		BrowserActions.scrollToView(closePopUp, driver);
		BrowserActions.javascriptClick(closePopUp, driver, "Close GPW Free gift Pop Up");
	}

	public String getUPCFreeProduct() throws Exception {
		String UPCText = BrowserActions.getText(driver, UpcOfFreeProduct, "Get Upc Of Free Product");
		String UPCText1 = UPCText.trim().replace("UPC: ", "");
		return UPCText1;

	}

	/*
	 * To click apply coupon in shopping bag
	 * 
	 * @param txtCouponCodeToEnter
	 * 
	 * @throws Exception
	 */
	public void applyCouponInShoppingBag(String txtCouponCodeToEnter) throws Exception {
		BrowserActions.scrollToView(txtCouponCode, driver);
		BrowserActions.typeOnTextField(txtCouponCode, txtCouponCodeToEnter, driver, "coupon code Box");
		BrowserActions.clickOnElement(btnApplyCoupon, driver, "coupon code Box");
		BrowserActions.scrollToView(txtCouponCode, driver);
		Utils.waitForPageLoad(driver);

	}// applyCouponInShoppingBag

	/**
	 * To get the text from shipping value
	 * 
	 * @return CouponResourceMessage
	 * @throws Exception
	 */
	public String getTextFromCouponResourceMsg() throws Exception {
		return BrowserActions.getText(driver, CouponResourceMessage, "Coupon Resource Message");
	}

	/**
	 * To get the Coupon details from Coupon Section
	 * 
	 * @return :CouponDetails
	 * @throws Exception
	 */
	public String getCouponDetailsFromCouponSection() throws Exception {
		return BrowserActions.getText(driver, CouponDetailsFromCouponSection, "CouponDetails");
	}

	public String getCouponTxt() throws Exception {
		String str = BrowserActions.getTextFromAttribute(driver, txtCouponCode, "style", "txtCouponCode");

		return str;

	}

	public void enterCoupon(String couponCode) throws Exception {
		BrowserActions.typeOnTextField(txtCouponCode, couponCode, driver, "Entering category in the search field");
	}

	public String getTextCouponApplied() throws Exception {
		return (BrowserActions.getText(driver, txtCouponApplied, "Coupon Code"));
	}

	public LinkedHashMap<String, String> getStoreDetailsInStoreLocationPage() throws Exception {

		LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();

		String StoreName = BrowserActions.getText(driver, storeName, "Store name");
		String StoreAddress = BrowserActions.getText(driver, storeAddress, "Store name");

		String StoreNumber = BrowserActions.getText(driver, storeNumber, "Store name");

		String StoreDistance = BrowserActions.getText(driver, storeDistance, "Store name");
		String StoreWorkingHour = BrowserActions.getText(driver, storeWorkingHour, "Store name");
		String StoreProductInStock = BrowserActions.getText(driver, storeProductInStock, "Store name");

		dataToBeReturned.put("StoreName", StoreName);
		dataToBeReturned.put("StoreAddress", StoreAddress);
		dataToBeReturned.put("StoreNumber", StoreNumber);
		dataToBeReturned.put("StoreDistance", StoreDistance);
		dataToBeReturned.put("StoreWorkingHour", StoreWorkingHour);
		dataToBeReturned.put("StoreProductInStock", StoreProductInStock);

		return dataToBeReturned;

	}

	/**
	 * To Search and navigate to pdp page
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public PdpPage searchAndNavigateToPDP(String textToSearch) throws Exception {
		final long startTime = StopWatch.startTime();
		if (textToSearch.startsWith("S_")) {
			textToSearch = textToSearch.split("S_")[1];
		}
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!", StopWatch.elapsedTime(startTime));

		return new PdpPage(driver).get();
	}

	/**
	 * To get the text from Needed amount for free ship message
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromNeededAmountForFreeShip() throws Exception {
		return BrowserActions.getText(driver, MsgNeededAmountForFreeShip, "Amount Needed For Free ship");

	}

	/**
	 * To get the Coupon applied Success Msg from Coupon Section
	 * 
	 * @return :CouponAppliedSuccessMsg
	 * @throws Exception
	 */
	public String getCouponSuccessfullyAppliedMsg() throws Exception {
		return BrowserActions.getText(driver, CouponAppliedSuccessMsg, "Coupon applied Success Msg");
	}

	/**
	 * To get the Coupon details from Coupon Section
	 * 
	 * @return :CouponDetails
	 * @throws Exception
	 */
	public String getCouponDetailsFromEmptyCart() throws Exception {
		return BrowserActions.getText(driver, CouponDetailsFromEmptyCart, "CouponDetails");
	}

	/**
	 * To get the txtWarningMsgEmptyBag from Coupon Section
	 * 
	 * @return :WarningMsgEmptyBag
	 * @throws Exception
	 */
	public String getTextFromCouponWarningMsgInEmptyBag() throws Exception {
		return BrowserActions.getText(driver, txtFromCouponWarningMsgEmptyBag, "Warning Message");
		
	}
	
	

public String getTxtOnlineOnly() throws Exception {
		return BrowserActions.getText(driver, txtMsgOnlineOnly ,
				"text Onlinre only");
	}
/**
	 * To Get This item will incur a surcharge alert message
	 * 
	 * @throws Exception
	 */
	public boolean getMsgItemIncldeSurchargealert() throws Exception {
		String cartMergeStatus = null;
		boolean status = false;

		cartMergeStatus = BrowserActions.getText(driver,
				lblLineitemalertmessage, "Shipping Surcharge information");
		if (cartMergeStatus
				.contains("This item will incur a surcharge")) {
			status = true;
		}

		return status;
	}

/**
	 * To get the Cart Footer Message
	 * 
	 * @return :cartfootermessage
	 * @throws Exception
	 */
	public String getCartFooterMsg() throws Exception {
		return BrowserActions.getText(driver, txtCartFooterMsg,
				"CouponDetails");
	}



}// Shopping Bag Page
