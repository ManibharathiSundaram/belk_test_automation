package com.belk.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class CustomerServiceLoginPage extends LoadableComponent<CustomerServiceLoginPage> {

	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	
	@FindBy(css="#dwfrm_csslogin")
	WebElement readyElement;
	
	@FindBy(css="input[id='dwfrm_csslogin_username']")
	WebElement txtLoginName;
	
	@FindBy(css="input[name='dwfrm_csslogin_password']")
	WebElement txtPassword;
	
	@FindBy(css="input[name='dwfrm_csslogin_login']")
	WebElement btnLogon;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public CustomerServiceLoginPage(WebDriver driver, String url) {
		appURL = url;
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// HomePage

	/**
	 * 
	 * @param driver
	 *            : webdriver
	 */
	public CustomerServiceLoginPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		elementLayer = new ElementLayer(driver);
	}// HomePage

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("CSS Login Page did not open up. Site might be down.", driver);
		}
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		driver.get(appURL);
		Utils.waitForPageLoad(driver);
	}// load
	
	/**
	 * To login to Customer Service Home Page with username and password
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception 
	 */
	public CustomerServiceHomePage logInToCSHomePage(String username, String password) throws Exception {
		enterEmailID(username);
		enterPassword(password);
		clickBtnSignIn();
		return new CustomerServiceHomePage(driver).get();
	}
	
	/**
	 * Enter email id
	 * 
	 * @param emailid
	 *            as string
	 * @throws Exception
	 */
	public void enterEmailID(String username) throws Exception {
		Utils.waitForElement(driver, txtLoginName);
		BrowserActions
				.typeOnTextField(txtLoginName, username, driver, "Email id");
		Log.event("Entered the Username : " + username);
	}

	/**
	 * Enter password
	 * 
	 * @param pwd
	 *            as string
	 * @throws Exception
	 */
	public void enterPassword(String pwd) throws Exception {
		Utils.waitForElement(driver, txtPassword);
		BrowserActions.typeOnTextField(txtPassword, pwd, driver, "Password");
		Log.event("Entered the Password: " + pwd);
	}

	/**
	 * To click LogIn button on signin page
	 * 
	 * @throws Exception
	 */
	public void clickBtnSignIn() throws Exception {
		final long startTime = StopWatch.startTime();
		Utils.waitForElement(driver, btnLogon);
		BrowserActions.clickOnElement(btnLogon, driver, "Login");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Login' button on SignIn page",
				StopWatch.elapsedTime(startTime));
	}
}
