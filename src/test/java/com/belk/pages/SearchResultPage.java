package com.belk.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.bcel.generic.GOTO;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class SearchResultPage extends LoadableComponent<SearchResultPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Search ResultPage - Ends ****************************
	 **********************************************************************************************/

	private static final String SIZE = "div.refinementSize";
	private static final String BRAND = "div[class='refinement brand ']";
	// private static final String LST_REFINEMENT_OPTIONS = ".view-more-less";
	private static final String SELECTED_COLOR = "ul[class='swatches color']>li.selected>a>img";
	// private static final String DEFAULT_SELECTED_VALUE_SORTBY =
	// "div[class='search-result-options'] .sort-by .custom-select
	// .selected-option";
	private static final String LIST_PRODUCT = ".search-result-items >li>div";
	private static final String LNK_VIEW_MORE = ".view-more-less span:not([class*='hide'])";
	// private static final String LNK_VIEW_MORE_LESS =
	// ".view-more-less span:not([class*='hide'])";

	private static final String LNK_VIEW_LESS = "li[class='view-more-less active'] span[class*='hide']";

	// --------------- Header Section ---------------------- //

	@FindBy(css = "label[for='q']")
	WebElement lblSearchText;

	@FindBy(css = "label[for='q']")
	WebElement lblSearchTextMobile;

	@FindBy(css = "input[name='q']")
	WebElement txtSearch;

	// --------------- Breadcrumb Section ---------------------- //

	@FindBy(css = "div[class='breadcrumb hide-mobile'] a[href*='home']")
	List<WebElement> txtBreadCrumbHome;

	@FindBy(css = ".breadcrumb-element.breadcrumb-result-text")
	WebElement BreadCrumbEndValue;

	@FindBy(css = ".breadcrumb-refinement-value")
	List<WebElement> txtBreadCrumbValue;

	@FindBy(css = "span[data-divider='and']")
	WebElement txtRefinementBreadCrumb;

	@FindBy(css = ".breadcrumb span[class*='breadcrumb']")
	List<WebElement> txtSortByRefinement;

	@FindBy(css = "span[class='breadcrumb-element breadcrumb-result-text']")
	WebElement txtSearchResultInBreadcrumb;

	@FindBy(css = "div:not([id*='primary'])>.breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = "div:not([id*='primary'])>.breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".section-header .error-text")
	WebElement txtErrorMessage;

	@FindBy(css = ".breadcrumb-relax")
	WebElement btnbreadCrumbRefinementClose;

	@FindBy(css = ".breadcrumb-refined-by")
	WebElement txtRefinedBy;

	@FindBy(css = "#main div[class='breadcrumb hide-mobile']")
	List<WebElement> txtBreadCrumb;

	@FindBy(css = ".breadcrumb-relax")
	List<WebElement> bcX;

	@FindBy(css = "div[class*='slot-grid-header'] .html-slot-container")
	WebElement lblSearchresultBanner;

	// --------------- Left Navigation Section ----------------- //

	@FindBy(css = "div[class='refineby-attribute']")
	WebElement sectionFilterBy;

	@FindBy(css = "div[class*='filterby-refinement']>a")
	WebElement btnFilterByMobile;

	@FindBy(css = ".filter-active")
	WebElement btnFilterByMobileActive;

	@FindBy(css = ".device-clear-all")
	WebElement lnkClearAll;

	@FindBy(css = "div#secondary div.close_menu i")
	WebElement btnMobileRefinementClose;

	// ----------- Refinement - Refinement Size ------------ //

	@FindBy(css = SIZE)
	WebElement btnsize;

	@FindBy(css = SIZE + " ul[style*='none']")
	public WebElement btnSizeExpand;

	@FindBy(css = SIZE + " i[class='toggle-plus-minus']")
	WebElement btnSizeCollapse;

	@FindBy(css = SIZE + " ul li a")
	List<WebElement> listSelectSize;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilter;

	// ----------- Refinement - Brand ------------ //

	@FindBy(css = BRAND)
	WebElement lblBrand;

	@FindBy(css = BRAND + " ul li")
	List<WebElement> lstBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> lstSubBrand;

	@FindBy(css = "div[class^='refinement']")
	List<WebElement> lstRefinementFilterOptions;

	@FindBy(css = "div[class*='category-refinement']")
	List<WebElement> lstCategoryRefinement;

	@FindBy(css = BRAND + " span[class='viewmore-refinement']")
	WebElement btnBrandViewMore;

	@FindBy(css = BRAND + " ul li")
	List<WebElement> listBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> listSubBrand;

	// -------------- Result product section ------------------ //

	@FindBy(css = "#results-products")
	WebElement txtProductFound;

	@FindBy(css = "a[href*='content']")
	WebElement lnkArticlesFound;

	@FindBy(css = "#results-content")
	WebElement lblArticel;

	@FindBy(css = ".folder-content-list li")
	List<WebElement> lstArticles;

	// ---------------- Search Result Filter Section ------------- //

	@FindBy(css = "div[class='search-result-options']")
	WebElement sectionSearchResult;

	@FindBy(css = "div:not([class*='last'])>.sort-by .custom-select div[class*='selected']")
	WebElement drpSortBy;

	@FindBy(css = ".last > div.sort-by")
	WebElement drpSortbyBottom;

	@FindBy(css = "div.product-refine-filter")
	WebElement txtSortBy;

	@FindBy(css = "div.product-refine")
	WebElement txtSortByMobile;

	@FindBy(css = "div:not([class*='last'])>.sort-by .selection-list li:not(.selected)")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "div[class*='last']>.sort-by .selection-list li")
	List<WebElement> lstSortByOptionsBottom;

	@FindBy(css = ".toggle-grid.hide-mobile>i:nth-child(1)")
	WebElement toggleGrid;

	@FindBy(css = ".toggle-grid.hide-mobile>i:nth-child(2)")
	WebElement toggleList;

	@FindBy(css = ".filterby-refinement.hide-desktop>span>i:nth-child(1)")
	WebElement toggleGridForMobile;

	@FindBy(css = ".filterby-refinement.hide-desktop>span>i:nth-child(2)")
	WebElement toggleListForMobile;

	@FindBy(css = ".toggle-grid")
	WebElement toggleView;

	@FindBy(css = ".wide-tiles")
	WebElement listSearchResult;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .custom-select")
	WebElement drpItemsPerPage;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .custom-select")
	WebElement drpItemsPerPageBottom;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptions;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptionsBottom;

	// ---------------- Result Hit Section ------------- //

	@FindBy(css = "div[class='search-result-options'] .results-hits")
	WebElement txtResultHits;

	@FindBy(css = "div[class*='last'] .results-hits")
	WebElement txtFooterResultHits;

	// --------------- Pagination Section ------------- //

	@FindBy(css = ".pagination li")
	List<WebElement> lstPageNos;

	@FindBy(css = ".page-next i")
	WebElement iconNextPage;

	@FindBy(css = ".page-last i")
	WebElement iconLastPage;

	@FindBy(css = ".page-previous i")
	WebElement iconPreviousPage;

	@FindBy(css = ".page-first i")
	WebElement iconFirstPage;

	@FindBy(css = ".pagination>ul>li")
	WebElement txtPagination;

	@FindBy(css = "div[class='search-result-options'] .current-page")
	WebElement lblCurrentPage;

	// -------------- Product List Section ----------- //

	@FindBy(css = "ul[id='search-result-items'] li .product-name>a")
	List<WebElement> lstProducts;

	@FindBy(css = "a.name-link")
	List<WebElement> lstProductNames;

	@FindBy(css = "a.thumb-link")
	List<WebElement> lstProductImages;

	@FindBy(css = "a.thumb-link>picture>img")
	List<WebElement> lstImgPrimary;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	@FindBy(css = "ul[id='search-result-items']>li")
	public List<WebElement> totalProducts;

	@FindBy(css = SELECTED_COLOR)
	List<WebElement> totalSelectedColor;

	@FindBy(css = "div[class*='quick-view']")
	WebElement qvStyle;

	@FindBy(css = "div ul li[class='brand-alphabet  expanded']")
	List<WebElement> lstBrandExpand;

	/*
	 * @FindBy(css = "ul.swatches.color>li.selectable") List<WebElement>
	 * lstColor;
	 */

	@FindBy(css = "div.product-swatches>ul>li:not([class*='hide'])>a:not([class*='un'])")
	List<WebElement> lstColor;

	@FindBy(xpath = ".//*[@id='secondary']/div[4]/h3")
	WebElement fldColorHeading;

	@FindBy(xpath = "//div[@id='secondary']//*[contains(.,'Color')]/i")
	WebElement lnkColorfilterToggle;

	@FindBy(css = "div.product-swatches>ul>li")
	List<WebElement> listColor;

	@FindBy(css = "#primary .wide-tiles")
	WebElement verifyListView;

	@FindBy(css = "div[class='search-result-options'] div[class='custom-select'] select[id='grid-sort-header']")
	WebElement defaultSelection;

	@FindBy(css = "div:not([class*='last'])>.items-per-page .custom-select .selected-option")
	WebElement dropItemPerPage;

	// ---------- Spinner ------------ //

	@FindBy(css = ".loader[style]")
	WebElement searchResultsSpinner;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement iconCloseMenu;

	@FindBy(css = ".refinement.category-refinement>ul>li")
	List<WebElement> textCatagoryrefinement;

	@FindBy(css = ".section-header>h1")
	WebElement lblErrorText;

	@FindBy(css = ".error-header")
	WebElement lblCorrectedSearchtxt;

	@FindBy(css = "div[class='breadcrumb hide-mobile']>span[class='breadcrumb-refined-by']")
	WebElement lblSortBy;

	@FindBy(css = "div[class='breadcrumb hide-mobile']>span[class='breadcrumb-refinement']")
	WebElement lblSorted;

	@FindBy(css = "div[class='breadcrumb hide-mobile']>span[class='breadcrumb-refined-by-prod']")
	WebElement lblRefinedBy;

	@FindBy(css = ".breadcrumb-element.breadcrumb-result-text>a")
	WebElement lnkSearchKeyword;

	@FindBy(css = "div[class='search-result-options last']>div[class='backtotop']>a")
	WebElement txtBacktoTop;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleListBottom;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleGridBottom;

	@FindBy(css = "div[class='product-badge']")
	WebElement txtProductBadge;

	@FindBy(css = ".header-nav .close_menu.hide-desktop>i")
	public WebElement btnHamburgurClose;

	@FindBy(css = ".refinement:nth-child(6) .refinement-link>span")
	List<WebElement> lblPriceRange;

	@FindBy(css = "div[id='secondary'] div:nth-child(6)>h3")
	public WebElement refinementPriceLabel;

	@FindBy(css = ".refinements>div:nth-child(6) h3[class='toggle'] .toggle-plus-minus")
	public WebElement pricetoggleClosed;

	@FindBy(css = ".refinements>div:nth-child(6) h3[class='toggle expanded'] .toggle-plus-minus")
	public WebElement pricetoggleExpanded;

	@FindBy(css = ".brand>h3>i[class='toggle-plus-minus']")
	public WebElement brandToggleClosed;

	@FindBy(css = ".brand>.expanded")
	public WebElement brandToggleExpanded;

	@FindBy(xpath = "//*[@id='secondary']/div[7]/ul/li/i")
	List<WebElement> brandAlphabetToggleExpand;

	@FindBy(css = "div[id='secondary'] div[class='refinement  ']")
	public WebElement refinementPrice;

	@FindBy(css = LIST_PRODUCT)
	List<WebElement> lstProduct;

	@FindBy(css = ".thumb-link>picture>img")
	WebElement imgPrimary;

	@FindBy(css = ".refinement-link >span [class='refine-count']")
	List<WebElement> lstPriceCountByRefinement;

	@FindBy(css = ".brand>ul>li:nth-child(1)")
	WebElement btnBrandAlphabetToggle;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li > ul > li > a > span > span")
	WebElement lstBrandCountByRefinement;

	@FindBy(css = "button[value='View All Articles']")
	WebElement btnViewAllArticles;

	@FindBy(css = "ul[id='search-result-items']")
	WebElement searchPage;

	@FindBy(css = "div[class='search-result-content '] ul[id='search-result-items'] li:nth-child(1)")
	WebElement searchResult;

	@FindBy(css = ".section-header p a")
	WebElement lnkSearchData;

	@FindBy(css = ".breadcrumb-refined-by-prod")
	WebElement txtRefinedByInBreadcrumb;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(css = ".menu-active")
	WebElement islnkHamburgerOpened;

	@FindBy(css = "#primary h1")
	WebElement txtContentHeader;

	@FindBy(css = "a[class='button mini-cart-link-cart']")
	WebElement btnViewBag;

	@FindBy(css = "#main")
	WebElement pageSource;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = ".breadcrumb-element.resultstext")
	WebElement txtArticlesBreacrumb;

	@FindBy(css = ".breadcrumb-element.resultstext>a")
	WebElement txtSearchTerm;

	@FindBy(css = ".swatch-list")
	List<WebElement> lstImgColorSwatches;

	@FindBy(css = ".content-title")
	List<WebElement> lstArticlesFound;

	@FindBy(css = ".article-page")
	WebElement pageArticle;

	@FindBy(css = ".article-page>a")
	WebElement lnkBackToSearchResults;

	@FindBy(css = "div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-price'] div[class='product-sales-price']")
	List<WebElement> lstsrchSuggtnProductName;

	@FindBy(css = "div[class='refinement Folder folder-refinement']")
	WebElement contentRefinement;

	@FindBy(css = "div[class*='refinement brand']")
	WebElement brandRefinement;

	@FindBy(css = "a[class='breadcrumb-element last-element search-page']")
	WebElement lnkSelectedCatagoryInBreadcrumb;

	@FindBy(css = "div[class='custom-select current_item'] select option")
	List<WebElement> optionsInItemsPerPge;

	@FindBy(css = ".folder-content-list li p")
	List<WebElement> txtDescriptionArticle;

	@FindBy(css = "div[class*='category-refinement'] ul li h4 a")
	List<WebElement> refinementCatagory;

	@FindBy(css = ".viewmore-refinement")
	List<WebElement> ViewMorelnkInRefinement;

	@FindBy(css = ".toggle-check.toggle-check-active")
	List<WebElement> toogleActivelnk;

	// />>>>>>>>>>>>>>>>>sprint 2B >>>>>>>>>>>>>>>>>>

	@FindBy(css = "[class='product-tile']>div>ul>li>a[class='swatch selected']>img")
	List<WebElement> lstOfColorSwatchProduct;

	@FindBy(css = ".loader[style*='block']")
	WebElement PDPspinner;

	@FindBy(css = ".article-page>a")
	List<WebElement> ArticlePge;

	@FindBy(css = "#primary>div.html-slot-container")
	WebElement SearchBanner;

	@FindBy(css = ".breadcrumb-element.breadcrumb-result-text>a")
	WebElement breadcrumbStyle;

	@FindBy(css = ".toggle-check.toggle-check-active")
	WebElement toogleActive;

	@FindBy(css = ".refinement.refinementColor>ul>li:not([class*='view-more']):not([class*='hide']):not([class*='clear'])>a")
	List<WebElement> lstColorSwatch;

	@FindBy(css = ".brand-alphabet:not([class*='hide'])")
	List<WebElement> brandName;

	@FindBy(css = "[class='refinement brand ']>ul>li[class='view-more-less']>span:not([class*='hide'])")
	WebElement brandViewMore;

	@FindBy(css = ".brand-alphabet")
	List<WebElement> brandAfterExpandProductCount;

	@FindBy(css = "div[class='refinement refinementColor ']>h3")
	WebElement viewColorExpand;

	@FindBy(css = "div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-price'] div[class='product-sales-price']")
	List<WebElement> lstsrchSuggtnProductPrice;

	@FindBy(css = "div[class='refinement refinementColor '] i[class='toggle-plus-minus']")
	WebElement btncolor;

	@FindBy(css = "#primary > div.search-result-options > div.refinement.category-refinement > span:nth-child(2) > a")
	WebElement btnWomen;

	@FindBy(css = ".content-slot.slot-grid-header")
	WebElement contentBanner;

	@FindBy(css = "#quickviewbutton")
	WebElement QuickViewIcon;

	@FindBy(css = "div:not([class*='last'])>.sort-by .selection-list li")
	List<WebElement> listSortByOptions;

	@FindBy(css = "div[class='custom-select current_item']>ul[class='selection-list']>li")
	List<WebElement> listItemsPerPageOptions;

	@FindBy(css = ".readmore")
	WebElement lnkReadmore;

	@FindBy(css = "select[id='grid-paging-header'] + div[class*='selected-option']")
	WebElement drpItemPerPage;

	@FindBy(css = "div[class*='category-refinement']>ul>li[class='expandable toggle-down'] ")
	List<WebElement> lstCategoryRefinementToggle;

	@FindBy(css = "div ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> chkRefinMentActive;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement IconCloseMenu;

	@FindBy(css = "div[class='refinement refinementColor ']")
	WebElement fldColorRefinement;

	@FindBy(css = ".product-standard-price")
	List<WebElement> lstOriginalPrice_Standard_Price;

	@FindBy(css = ".product-sales-price")
	List<WebElement> lstNowPrice_Sale_Price;

	@FindBy(css = ".toggle-grid.hide-mobile>i[data-option='wide']")
	WebElement listToggle;

	@FindBy(css = ".toggle-grid>i[data-option='wide']")
	WebElement listToggleMobile;

	@FindBy(css = "#secondary")
	WebElement lstRefinement;

	@FindBy(css = ".content-slot .html-slot-container")
	WebElement globalSlotBanner;

	// //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public SearchResultPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded
				&& !(Utils.waitForElement(driver, sectionSearchResult))) {
			Log.fail(
					"Search result Page did not open up. Search Results might not be found.",
					driver);
		}
	}

	// --------------- Header Section ---------------------- //

	/**
	 * To enter product in search text box
	 * 
	 * @param txtToSearch
	 * @throws Exception
	 */
	public void enterProduct(String txtToSearch) throws Exception {
		BrowserActions.typeOnTextField(txtSearch, txtToSearch, driver,
				"Enter product in the search text box");
	}

	/**
	 * To get a text from search result page
	 * 
	 * @return: String - text from search result page
	 * @throws Exception
	 */
	public String getTextFromSearchResult() throws Exception {
		String txtSearchTextBox = BrowserActions.getText(driver,
				txtSearchResultInBreadcrumb,
				"Fetching the search text value  in the search result page");
		return txtSearchTextBox;
	}

	/**
	 * To get a text from search result page
	 * 
	 * @param lblSearchText
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from search result page
	 * @throws Exception
	 */
	public String getTextFromSearchTextBox() throws Exception {
		String txtSearchTextBox = null;
		if (Utils.getRunPlatForm() == "mobile")
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchTextMobile,
							"Fetching the search text value  in the search result page");
		else
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchText,
							"Fetching the search text value  in the search result page");
		return txtSearchTextBox;
	}

	// --------------- Breadcrumb Section ---------------------- //

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	public List<String> getTextFromEntrieBreadCrumb() throws Exception {
		List<String> breadcrumbText = new ArrayList<>();
		for (WebElement element : txtSortByRefinement) {
			System.out.print(element.getText());
			breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To get breadcrumb last value
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getBreadCrumbLastValue() throws Exception {
		// BrowserActions.scrollToViewElement(txtRefinementBreadCrumb, driver);
		ArrayList<String> values = new ArrayList<>();
		for (WebElement element : txtBreadCrumbValue) {
			values.add(BrowserActions
					.getText(driver, element, "breadcrumb value")
					.replace("x", "").trim());
		}
		return values;
	}

	/**
	 * To get breadcrumb value & and text
	 * 
	 * @throws Exception
	 */
	public String getTextFromBread() throws Exception {
		String selectedBreadCrumbValues = BrowserActions.getText(driver,
				txtRefinementBreadCrumb, "").replace("x", "");
		String[] slectedBreadCrumb = selectedBreadCrumbValues.split(" ");
		String txtAnd = txtRefinementBreadCrumb.getAttribute("data-divider");
		String slectedBreadCrumbText = (slectedBreadCrumb[1] + " " + txtAnd);
		return slectedBreadCrumbText.trim();
	}

	/**
	 * To get breadcrumb value & and text
	 * 
	 * @throws Exception
	 */
	public String getBreadCrumbRefinement() throws Exception {
		String selectedBreadCrumbValues = BrowserActions.getText(driver,
				txtRefinementBreadCrumb, "").replace("x", "");
		String[] slectedBreadCrumb = selectedBreadCrumbValues.split(" ");
		String txtAnd = txtRefinementBreadCrumb.getAttribute("data-divider");
		String slectedBreadCrumbText = (slectedBreadCrumb[1] + " " + txtAnd);
		return slectedBreadCrumbText.trim();
	}

	/**
	 * To click and navigate to a category in breadcrumb
	 * 
	 * @param categoryName
	 * @throws Exception
	 */
	public void clickCategoryByNameInBreadcrumb(String categoryName)
			throws Exception {
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (element.getText().equalsIgnoreCase(categoryName)) {
					BrowserActions.clickOnElement(element, driver,
							"Category in breadcrumb");
					break;
				}
			}
		} else {
			BrowserActions.clickOnElement(txtProductInBreadcrumbMobile, driver,
					"Category in breadcrumb");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * @return lblBanner
	 * @throws Exception
	 */
	public String getTextFromBanner() throws Exception {
		String lblBanner = BrowserActions.getText(driver,
				lblSearchresultBanner, "Banner text");
		return lblBanner;
	}

	// --------------- Left Navigation Section ----------------- //

	/**
	 * To select Catgeory Refinement
	 * 
	 * @return txtsubcategory
	 * @throws Exception
	 */
	public String selectCatgeoryRefinement() throws Exception {
		List<WebElement> lstElement = null;
		String txtsubcategory = null;
		WebElement element = null;

		for (WebElement element1 : lstCategoryRefinement) {
			element = element1;
			break;
		}
		lstElement = element.findElement(By.xpath("..")).findElements(
				By.cssSelector("div[class*='category-refinement']>ul>li"));
		for (int category = 0; category < lstElement.size();) {
			WebElement e = lstElement.get(category);
			String txtcategory = e.getText();
			if (txtcategory != null) {
				e.click();
			}
			lstElement = element
					.findElement(By.xpath(".."))
					.findElements(
							By.cssSelector("div[class*='category-refinement']>ul>li[class*='toggle-up'] .subcategories li a"));
			for (int subcat = 0; subcat < lstElement.size(); subcat++) {
				WebElement subcategory = lstElement.get(subcat);
				txtsubcategory = txtcategory + "-" + subcategory.getText();
				if (txtsubcategory != null) {
					BrowserActions.javascriptClick(subcategory, driver,
							"clicking sub category");
					break;
				}
			}
			break;
		}

		return txtsubcategory;

	}

	/**
	 * To get breadcrumb category value
	 * 
	 * @throws Exception
	 */
	public String getBreadCrumbProductValue() throws Exception {
		String value = BrowserActions.getText(driver,
				txtProductInBreadcrumbMobile, "breadcrumb value").replace(" x",
				"");
		return value;
	}

	/**
	 * To select/unselect the refinements in the search refinement panel
	 * 
	 * @param filterType
	 * @param optionValue
	 *            - optional
	 * @return String
	 * @throws Exception
	 */
	public boolean verifySpinner(String filterType, String... optionValue)
			throws Exception {
		boolean returnValue = false;
		WebElement element = null;
		List<WebElement> lstElement = null;
		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			// Getting the web element that matches the 'filterType'
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded
			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}
			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul>li:not([class*='hide'])"));
			if (!(optionValue.length > 0)) {
				if (lstElement.size() == 1) {
					WebElement e = lstElement.get(0);
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
					returnValue = Utils.waitForElement(driver,
							searchResultsSpinner);
				} else {
					int rand = ThreadLocalRandom.current().nextInt(1,
							lstElement.size());
					WebElement e = lstElement.get(rand - 1);
					// Clicking random filter values if not mentioned
					BrowserActions.scrollToViewElement(e, driver);
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
					returnValue = Utils.waitForElement(driver,
							searchResultsSpinner);
				}

			} else {
				WebElement e = BrowserActions.getMachingTextElementFromList(
						lstElement, optionValue[0], "contains");
				// Clicking filter option based on 'optionValue'
				BrowserActions.clickOnElement(
						e.findElement(By.cssSelector("a i")), driver,
						"Filter values");
				returnValue = Utils
						.waitForElement(driver, searchResultsSpinner);

			}
			Utils.waitForPageLoad(driver);
		}
		return returnValue;
	}

	public String selectUnselectFilterOptionsWithGreaterThanSixty(
			String filterType, String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String[] filterValue = null;
		// Getting the web element that matches the 'filterType'
		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}
		// Clicking expand toggle if filter list is not expanded
		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}
		// Getting the list of web elements below the 'filterType'
		lstElement = element.findElement(By.xpath("..")).findElements(
				By.cssSelector("div[class='refinement " + filterType.trim()
						+ " ']>ul>li:not([class*='hide'])"));

		for (int i = 0; i < lstElement.size(); i++) {
			WebElement e = lstElement.get(i);
			BrowserActions.scrollToViewElement(e, driver);
			filterValue = e.getText().split("\\(");
			String a = filterValue[1];
			int count = Integer.valueOf(a);
			if (count < 60) {

			}

		}

		/*
		 * if (!(optionValue.length > 0)) { if (lstElement.size() == 1) {
		 * WebElement e = lstElement.get(0); filterValue = e.getText();
		 * BrowserActions.javascriptClick(e.findElement(By.cssSelector("a i")),
		 * driver, "Filter values"); }else { int rand =
		 * ThreadLocalRandom.current().nextInt(1, lstElement.size()); WebElement
		 * e = lstElement.get(rand - 1); // Clicking random filter values if not
		 * mentioned BrowserActions.scrollToViewElement(e, driver); filterValue
		 * = e.getText();
		 * BrowserActions.javascriptClick(e.findElement(By.cssSelector("a i")),
		 * driver, "Filter values"); }
		 * 
		 * } else { WebElement e =
		 * BrowserActions.getMachingTextElementFromList(lstElement,
		 * optionValue[0], "contains"); // Clicking filter option based on
		 * 'optionValue' //
		 * BrowserActions.clickOnElement(e.findElement(By.cssSelector("a // i
		 * ")), // driver, "Filter values"); WebElement selectoption =
		 * e.findElement(By.cssSelector("a i")); if
		 * (selectoption.getAttribute("class").contains("toggle-check-active"))
		 * { BrowserActions.javascriptClick(e.findElement(By.cssSelector(
		 * "a i[class*='toggle-check-active']")), driver,
		 * "Un selecting the Filter values"); } else {
		 * BrowserActions.javascriptClick(e.findElement(By.cssSelector("a i")),
		 * driver, "Selectin the Filter values"); }
		 * waitUntilSearchSpinnerDisappear(); filterValue =
		 * optionValue[0].toString();
		 * 
		 * }
		 */
		Utils.waitForPageLoad(driver);
		return filterValue[0];
	}

	/**
	 * Clicking on 'X'
	 * 
	 * @throws Exception
	 */
	public void ClickXToRemoveRefinement() throws Exception {
		BrowserActions.clickOnElement(bcX.get(0), driver, "Clicking On 'X'");
		waitUntilSearchSpinnerDisappear();
	}

	public String selectedRefinementcount(String filterType) throws Exception {

		WebElement element = null;
		String filterValue = null;
		// Getting the web element that matches the 'filterType'

		if (Utils.getRunPlatForm().equals("mobile")) {
			BrowserActions.clickOnElement(btnFilterByMobile, driver,
					"Click on filter by");
		}

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");
			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}

		WebElement selectedrefinement = BrowserActions.checkLocator(driver,
				"div[class='refinement " + filterType.trim()
						+ " ']>ul>li.selected a span.refine-count");
		filterValue = selectedrefinement.getText().replace("(", "")
				.replace(")", "");

		if (Utils.getRunPlatForm().equals("mobile")) {
			WebElement toggleexpand = element.findElement(By.cssSelector("h3"));
			if (toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.clickOnElement(toggleexpand, driver,
						"closed toggle");
			}
		}
		return filterValue;

	}

	public void clickOnFilterByOption() throws Exception {
		BrowserActions.javascriptClick(btnFilterByMobile, driver,
				"Click on filter by");
	}

	/**
	 * To click the view more/less link
	 * 
	 * @param removeRefinement
	 * @throws Exception
	 */

	public ArrayList<String> removeRefinement() throws Exception {
		BrowserActions
				.scrollToViewElement(btnbreadCrumbRefinementClose, driver);
		ArrayList<String> removeRefinement = getBreadCrumbLastValue();
		BrowserActions.clickOnElement(btnbreadCrumbRefinementClose, driver,
				"Clicked refinement close(x) button");
		waitUntilSearchSpinnerDisappear();
		Utils.waitForPageLoad(driver);
		return removeRefinement;
	}

	/**
	 * To click the view more/less link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickViewMoreOrLessLnk(String filterType) throws Exception {
		WebElement element = BrowserActions.getMachingTextElementFromList(
				lstRefinementFilterOptions, filterType, "contains");
		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_MORE)), driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To click 'Clear All' link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickClearAllLnk() throws Exception {
		BrowserActions.clickOnElement(lnkClearAll, driver, "Clear all link");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To verify expand toggle is displayed for 'filterType'
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public boolean verifyExpandToggle(String filterType) throws Exception {
		boolean status = false;
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");
			BrowserActions.scrollToView(element1, driver);
			if (filterType.equals("price")) {
				filterType = filterType.replace("price", " ");
			}

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (toggle.getAttribute("class").contains("expanded")) {
			status = true;
		}
		return status;
	}

	public void closeMobileRefinement() throws Exception {
		// WebElement close = BrowserActions.checkLocator(driver,
		// btnMobileRefinementClose);
		BrowserActions.javascriptClick(btnMobileRefinementClose, driver,
				"Close Mobile refinement");
		Utils.waitForPageLoad(driver);
	}

	// ----------- Refinement - Brand ------------ //

	/**
	 * To verify the brand group
	 * 
	 * @throws Exception
	 */
	public void verifyBrandGroup() throws Exception {
		String pattern = ("[A-Z]+");
		Pattern p = Pattern.compile(pattern);
		for (int brand = 0; brand < lstBrand.size(); brand++) {
			String brandname = lstBrand.get(brand).getText();
			Matcher m = p.matcher(brandname);
			if (m.find()) {
				lstBrand.get(brand).click();
				if (lstSubBrand.size() > 5) {
					((JavascriptExecutor) driver)
							.executeScript("scroll(0,250);");
					break;
				}
			}

		}
	}

	/**
	 * To select first brand
	 * 
	 * @throws Exception
	 */
	public void selectFirstBrand() throws Exception {
		for (int brand = 0; brand < listBrand.size();) {
			listBrand.get(brand).click();
			SearchUtils.selectRefinementFirstValue(listSubBrand, driver);
			break;
		}
	}

	/**
	 * To unselect first brand
	 * 
	 * @throws Exception
	 */
	public void UnSelectSelectedFirstBrand() throws Exception {
		for (int brand = 0; brand < listBrand.size();) {
			listBrand.get(brand).click();
			SearchUtils.selectRefinementFirstValue(listSubBrand, driver);
			Utils.waitForPageLoad(driver);
			SearchUtils.unSelectRefinementFirstValue(listSubBrand, driver);
			Utils.waitForPageLoad(driver);
			break;
		}
	}

	/**
	 * To verify the multiple selected brand
	 * 
	 * @throws Exception
	 */
	public void verifySelectMultipleBrand() throws Exception {
		String pattern = ("[A-Z]+");
		Pattern p = Pattern.compile(pattern);
		for (int brand = 0; brand < lstBrand.size(); brand++) {
			String brandname = lstBrand.get(brand).getText();
			Matcher m = p.matcher(brandname);
			if (m.find()) {
				lstBrand.get(brand).click();
				for (int subBrand = 0; subBrand < lstSubBrand.size(); subBrand++) {
					lstSubBrand.get(subBrand).click();
					if (subBrand == '2') {
						break;
					}
				}
			}
		}

	}

	/**
	 * To get refinement list options
	 * 
	 * @throws Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();
		// List<WebElement> refinementcount =
		// BrowserActions.checkLocators(driver, lstRefinementFilterOptions +
		// " h3");

		// Getting the web element that matches the 'filterType'
		for (int i = 0; i < lstRefinementFilterOptionsInMobile.size(); i++) {

			WebElement element = lstRefinementFilterOptionsInMobile.get(i)
					.findElement(By.xpath(".."));
			System.out.print("Locator" + element);
			String[] refinementText = element.getAttribute("class").split(" ");
			// List<String> refinementText =
			// Arrays.asList(element.getAttribute("class").trim().split(" "));
			if (refinementText.length == 1) {
				refinement.add("price");
			} else
				refinement.add(refinementText[1]);
		}
		return refinement;
	}

	// -------------- Result product section ------------------ //

	/**
	 * To verify the multiple selected brand
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getProductsFound() throws Exception {
		return BrowserActions
				.getText(driver, txtProductFound, "Products Found");

	}

	// ---------------- Search Result Filter Section ------------- //

	/**
	 * To select option from sort by dropdown randomly
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String selectSortByRandom() throws Exception {
		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");
		int rand = ThreadLocalRandom.current().nextInt(1,
				lstSortByOptions.size());
		String selectedDropdownText = lstSortByOptions.get(rand - 1).getText();
		BrowserActions.clickOnElement(lstSortByOptions.get(rand - 1), driver,
				"SortBy dropdown list");
		waitUntilSearchSpinnerDisappear();
		return selectedDropdownText;
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String selectSortByDropDownByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");
		String sortByValue = lstSortByOptions.get(index - 1).getText();
		BrowserActions.clickOnElement(lstSortByOptions.get(index - 1), driver,
				"SortBy dropdown list");
		waitUntilSearchSpinnerDisappear();
		return sortByValue;
	}

	/**
	 * To select option from Items Per Page by dropdown ByText
	 * 
	 * @param optionValue
	 * @param bottomType
	 * @return String
	 * @throws Exception
	 */
	public String selectSortByDropDownByText(String optionValue)
			throws Exception {
		// WebElement element = null;
		WebElement element = null;
		// List<WebElement> lstElements = null;
		if (BrowserActions
				.getText(driver, drpSortBy, "sort by drop down selected option")
				.trim().toLowerCase().contains(optionValue)) {
			return BrowserActions.getText(driver, drpSortBy,
					"sort by drop down selected option");
		}
		BrowserActions.clickOnElement(drpSortBy, driver, "sort by");

		for (WebElement e : lstSortByOptions) {
			if (e.getAttribute("innerText").contains(optionValue)) {
				element = e;
				break;
			}
		}

		String selectedValue = element.getText();
		BrowserActions.clickOnElement(element, driver, "sort by dropdown");
		waitUntilSearchSpinnerDisappear();
		return selectedValue;
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public ArrayList<String> getSortByOptionsList() throws Exception {
		ArrayList<String> lstValues = new ArrayList<>();
		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");
		for (WebElement element : lstSortByOptions) {
			if (!element.getText().equals(""))
				lstValues.add(element.getText().trim());
		}
		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");
		return lstValues;
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedSortByValue() throws Exception {
		return drpSortBy.findElement(By.cssSelector("div")).getText().trim();
	}

	/**
	 * To click Grid/List toggle
	 * 
	 * @param toggleType
	 *            - 'Grid' or 'List'
	 * @throws Exception
	 */
	public void clickGridListToggle(String toggleType) throws Exception {
		WebElement element = null;

		if (runPltfrm == "mobile") {
			if (toggleType.equals("Grid")) {
				element = toggleGridForMobile;
			} else if (toggleType.equals("List")) {
				element = toggleListForMobile;
			}
		} else {
			if (toggleType.equals("Grid")) {
				element = toggleGrid;
			} else if (toggleType.equals("List")) {
				element = toggleList;
			}
		}

		BrowserActions.clickOnElement(element, driver, "" + toggleType
				+ " toggle");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To get the current view type ("wide" or "grid")
	 * 
	 * @return String - "grid" or "wide"
	 * @throws Exception
	 */
	public String getCurrentViewType() throws Exception {
		String className = BrowserActions.getTextFromAttribute(driver,
				toggleView, "class", "Class Name of Toggle view");
		if (className.contains("wide"))
			return "wide";
		else
			return "grid";
	}

	/**
	 * To select option from Items Per Page dropdown randomly
	 * 
	 * @param bottomType
	 *            - 'bottom'
	 * @return String
	 * @throws Exception
	 */
	public String selectItemsPerPageDropDownRandom(String... bottomType)
			throws Exception {
		WebElement element = null;
		List<WebElement> lstElements = null;
		if (bottomType.length != 0) {
			element = drpItemsPerPageBottom;
			lstElements = lstItemsPerPageOptionsBottom;
		} else {
			element = drpItemsPerPage;
			lstElements = lstItemsPerPageOptions;
		}
		BrowserActions.clickOnElement(element, driver,
				"Items per page dropdown");
		int rand = ThreadLocalRandom.current().nextInt(1, lstElements.size());
		String selectedValue = lstElements.get(rand - 1).getText();
		BrowserActions.clickOnElement(lstElements.get(rand - 1), driver,
				"Items per page dropdown");
		BrowserActions.nap(5);
		waitUntilSearchSpinnerDisappear();
		return selectedValue;
	}

	/**
	 * To select option from Items Per Page by dropdown randomly
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String selectItemsPerPageDropDownByIndex(int index,
			String... bottomType) throws Exception {
		WebElement element = null;
		List<WebElement> lstElements = null;
		if (bottomType.length != 0) {
			element = drpItemsPerPageBottom;
			lstElements = lstItemsPerPageOptionsBottom;
		} else {
			element = drpItemsPerPage;
			lstElements = lstItemsPerPageOptions;
		}
		BrowserActions.clickOnElement(element, driver,
				"Items per page dropdown");
		String selectedValue = lstElements.get(index - 1).getText();
		BrowserActions.clickOnElement(lstElements.get(index - 1), driver,
				"Items per page dropdown");
		waitUntilSearchSpinnerDisappear();
		try {
			// Adding this wait till all the products get loaded in Search
			// result page
			Utils.waitUntilElementDisappear(driver, element, 40);
		} catch (Exception e) {
			Log.event("Waited for the page load");
		}
		return selectedValue;
	}

	/**
	 * To select option from Items Per Page by dropdown randomly
	 * 
	 * @param optionValue
	 * @param bottomType
	 * @return String
	 * @throws Exception
	 */
	public String selectItemsPerPageDropDownByText(String optionValue,
			String... bottomType) throws Exception {
		WebElement element = null;
		WebElement element1 = null;
		List<WebElement> lstElements = null;
		if (bottomType.length != 0) {
			element = drpItemsPerPageBottom;
			lstElements = lstItemsPerPageOptionsBottom;
		} else {
			element = drpItemsPerPage;
			lstElements = lstItemsPerPageOptions;
		}
		BrowserActions.clickOnElement(element, driver,
				"Items per page dropdown");
		for (WebElement e : lstElements) {
			if (e.getText().contains(optionValue)) {
				element1 = e;
				break;
			}
		}
		String selectedValue = element1.getText();
		BrowserActions.clickOnElement(element1, driver,
				"Items per page dropdown");
		waitUntilSearchSpinnerDisappear();
		Utils.waitForPageLoad(driver);
		// try {
		// // Adding this wait till all the products get loaded in Search
		// // result page
		// Utils.waitUntilElementDisappear(driver, element);
		// } catch (Exception e) {
		// Log.event("Waited for the page load");
		// }

		return selectedValue;
	}

	// ---------------- Result Hit Section ------------- //

	/**
	 * To get the count of the viewing page (Example: "Viewing 1-30 of 30")
	 * 
	 * @return String - (Example: "Viewing 1-30 of 30")
	 * @throws Exception
	 */
	public String getResultHitsText() throws Exception {

		return BrowserActions.getText(driver, txtResultHits, "Item count text");
	}

	// --------------- Pagination Section ------------- //

	/**
	 * To navigate to page in the pagination
	 * 
	 * @param pageNo
	 * @throws Exception
	 */
	public void navigateToPagination(String pageNo) throws Exception {
		if (pageNo.equals("Last")) {
			BrowserActions.clickOnElement(iconLastPage, driver, "Last Page");
		} else if (pageNo.equals("First")) {
			BrowserActions.clickOnElement(iconFirstPage, driver, "First Page");
		} else if (pageNo.equals("Previous")) {
			BrowserActions
					.clickOnElement(iconPreviousPage, driver, "Last Page");
		} else if (pageNo.equals("Next")) {
			BrowserActions.clickOnElement(iconNextPage, driver, "Last Page");
		} else {
			WebElement element = BrowserActions.getMachingTextElementFromList(
					lstPageNos, pageNo, "equals");
			BrowserActions.scrollToView(element, driver);
			BrowserActions.clickOnElement(element, driver, "Page No " + pageNo
					+ "");
		}
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To get visible page numbers
	 * 
	 * @return pageNos
	 */
	public ArrayList<String> getVisiblePageNos() {
		ArrayList<String> pageNos = new ArrayList<>();
		List<WebElement> lstElement = driver
				.findElements(By
						.cssSelector("div:not([class*='last'])>.pagination ul li:not([class*='-']) a"));
		for (WebElement ele : lstElement) {
			pageNos.add(ele.getAttribute("class"));
		}
		return pageNos;
	}

	public int getCurrentPageNo() {
		int currentPageNo = Integer.parseInt(lblCurrentPage.getText());
		return currentPageNo;
	}

	// -------------- Product List Section ----------- //

	/**
	 * To select the product by index
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage selectProductByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(lstProduct.get(index - 1), driver,
				"Select " + index + "th product");
		return new PdpPage(driver).get();
	}

	/**
	 * To click on Articles found
	 * 
	 * @throws Exception
	 */
	public void clickOnArticlesFound() throws Exception {
		BrowserActions.clickOnElement(lnkArticlesFound, driver,
				"Articel found link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * 
	 * @return lblArticleFound
	 * @throws Exception
	 */
	public String getTextFromArticles() throws Exception {
		BrowserActions.scrollToViewElement(lblArticel, driver);
		String lblArticleFound = BrowserActions.getText(driver, lblArticel,
				"Articel found details");
		return lblArticleFound;
	}

	/*
	 * To check List of Articles Count return count
	 * 
	 * @throws Exception
	 */

	public int checkArticlesCount() throws Exception {
		int count = 0;
		BrowserActions.scrollToViewElement(lstArticles.get(0), driver);
		if (Utils.waitForElement(driver, btnViewAllArticles)) {
			BrowserActions.clickOnElement(btnViewAllArticles, driver,
					"View All Articles button");
			Utils.waitForPageLoad(driver);
		}
		count = lstArticles.size();
		return count;

	}

	/**
	 * To navigate to PDPPage by selecting random product
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDP() throws Exception {
		if (lstProducts.size() > 0) {
			int rand = Utils.getRandom(0, lstProducts.size());
			BrowserActions.javascriptClick(lstProducts.get(rand), driver,
					"Select random product");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to PDPPage with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDPWithProductId() throws Exception {
		if (lstProducts.size() > 0) {
			BrowserActions.scrollToView(lstProducts.get(0), driver);
			BrowserActions.clickOnElement(lstProducts.get(0), driver,
					"select any product");
			Utils.waitForPageLoad(driver);
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to PDPPage by index value of product
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDPByRow(int index) throws Exception {
		if (totalProducts.size() > 0) {
			BrowserActions.clickOnElement(totalProducts.get(index - 1), driver,
					"Select quantity option");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To hover on color swatch in product tile
	 * 
	 * @param productIndex
	 * @param colorIndex
	 * @throws Exception
	 */
	public String hoverColorOnProductTile(int productIndex, int colorIndex)
			throws Exception {
		String src = null;
		WebElement element = lstProducts.get(productIndex - 1).findElement(
				By.xpath("..//.."));
		List<WebElement> lstElement = element.findElements(By
				.cssSelector(".product-swatches>ul[class='swatch-list'] img"));
		src = lstElement.get(colorIndex - 1).getAttribute("src");
		BrowserActions.mouseHover(driver, lstElement.get(colorIndex - 1));
		return src;

	}

	/**
	 * To get selected color value by index value of color
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedColorByRow(int index) throws Exception {
		String selectedcolor = null;
		if (totalProducts.size() > 0) {
			selectedcolor = totalSelectedColor.get((index - 1) * 2)
					.getAttribute("src");
		}
		Utils.waitForPageLoad(driver);
		return selectedcolor;
	}

	/**
	 * To get the current X coordinate value
	 * 
	 * @index index
	 * @return int - X coordinate value
	 */
	public int getCurrentLocationOfPdtByIndex(int index) {
		WebElement image = lstProductImages.get(index - 1);
		Point point = image.getLocation();
		return point.getX();

	}

	/**
	 * To click on color swatch in product tile
	 * 
	 * @param productIndex
	 * @param colorIndex
	 * @throws Exception
	 */
	public void clickColorOnProductTile(int productIndex, int colorIndex)
			throws Exception {
		WebElement element = lstProducts.get(productIndex - 1).findElement(
				By.xpath("..//.."));
		List<WebElement> lstElement = element.findElements(By
				.cssSelector(".product-swatches>ul[class='swatch-list'] img"));
		BrowserActions.clickOnElement(lstElement.get(colorIndex - 1), driver,
				"color swatch in product tile");
	}

	/**
	 * To get color src value of primary image
	 * 
	 * @return String
	 */
	public String getPrimaryImageColor(int index, String... swatches)
			throws Exception {
		List<WebElement> viewAllColor = null;
		String dataToBeReturned = null;
		boolean status = false;
		if (swatches.length > 0) {
			if (swatches[0].equals("more than 5")) {
				int i = 0;
				while (status == false || i == 0)
					for (i = 0; i < driver.findElements(
							By.cssSelector("a.name-link")).size(); i++) {
						try {
							WebElement e = lstProductNames
									.get(i)
									.findElement(By.xpath("../.."))
									.findElement(By.cssSelector(".swatch-list"));
							List<WebElement> lstElements = e.findElements(By
									.cssSelector("ul li"));
							if (lstElements.size() >= 5) {
								viewAllColor = lstElements
										.get(0)
										.findElement(By.xpath("../.."))
										.findElements(
												By.cssSelector(".product-swatches-cont"));
								try {
									BrowserActions.scrollToViewElement(
											viewAllColor.get(0), driver);
									Actions actions = new Actions(driver);
									actions.moveToElement(viewAllColor.get(0))
											.build().perform();
									;
									lstElements = viewAllColor
											.get(0)
											.findElements(
													By.cssSelector(".swatch-list-modal ul li"));
									if (lstElements.size() > 5) {
										status = true;
										dataToBeReturned = viewAllColor
												.get(index - 1)
												.findElement(By.xpath("../.."))
												.findElement(
														By.cssSelector("a.thumb-link>picture>img"))
												.getAttribute("src")
												.split("&layer")[0];
										actions.release(viewAllColor.get(0))
												.build().perform();
										break;
									}
								} catch (Exception e2) {
									// TODO: handle exception
								}
							}
						} catch (Exception e) {
							Log.event("No color is found");
							if (status == true)
								break;
						}
						if (status == false && i == lstProductNames.size() - 1) {
							navigateToPagination("Next");
						}
					}
			} else if (swatches[0].equals("swatches")) {
				dataToBeReturned = lstImgColorSwatches
						.get(index - 1)
						.findElement(By.xpath("../.."))
						.findElement(By.cssSelector("a.thumb-link>picture>img"))
						.getAttribute("src").split("&layer")[0];
			}
		} else
			dataToBeReturned = lstImgPrimary.get(index - 1).getAttribute("src")
					.split("&layer")[0];
		return dataToBeReturned;
	}

	/**
	 * To select the each color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public int getColorSwatchCount(int productIndex) throws Exception {
		WebElement element = lstProducts.get(productIndex - 1).findElement(
				By.xpath("../.."));
		List<WebElement> lstElement = element.findElements(By
				.cssSelector(".product-swatches>ul[class='swatch-list'] img"));
		return lstElement.size();
	}

	/**
	 * To verify selected color swatch is highlighted with orange background
	 * 
	 * @param productIndex
	 * @param colorIndex
	 * @return
	 * @throws Exception
	 */
	public boolean verifyColorSwatchIsHighlighted(int productIndex,
			int colorIndex) throws Exception {
		boolean status = false;
		clickColorOnProductTile(productIndex, colorIndex);
		BrowserActions.mouseHover(driver, lstProducts.get(productIndex - 1));
		WebElement element = lstProducts.get(productIndex - 1).findElement(
				By.xpath("..//.."));
		List<WebElement> lstElement = element.findElements(By
				.cssSelector(".product-swatches>ul[class='swatch-list'] a"));
		if (lstElement.get(colorIndex - 1).getCssValue("border-color")
				.equals("rgb(237, 103, 0)")) {
			status = true;
		}
		return status;
	}

	// ------ QuickView ------ //
	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickView() throws Exception {
		if (lstProductImages.size() > 0) {
			// int rand = ThreadLocalRandom.current().nextInt(1,
			// lstProductImages.size());
			int rand = Utils.getRandom(0, lstProductImages.size());
			BrowserActions.scrollToViewElement(lstProductImages.get(rand),
					driver);
			BrowserActions.mouseHover(driver, lstProductImages.get(rand));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"quickview button");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		Utils.waitForPageLoad(driver);
		return new QuickViewPage(driver).get();
	}

	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickViewWithProductId() throws Exception {
		if (lstProductImages.size() > 0) {
			BrowserActions.scrollToViewElement(lstProductImages.get(0), driver);
			BrowserActions.mouseHover(driver, lstProductImages.get(0));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	/**
	 * To verify quick view state
	 * 
	 * @return boolean
	 */
	public boolean QVStateNone() {
		return Utils.verifyCssPropertyForElement(qvStyle, "display", "none");
	}

	// ---------- Spinner ------------ //

	/**
	 * To wait for Search results spinner
	 */
	public void waitUntilSearchSpinnerDisappear() {
		BrowserActions.nap(3);
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
	}

	/**
	 * To get the count of displayed products
	 * 
	 * @return int
	 */
	public int getProductCount() {
		return lstProductNames.size();
	}

	/**
	 * To select the product by name random
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage selectProductByNameRandom() throws Exception {

		Log.message("lstProductNames.size() ----" + lstProductNames.size());
		if (lstProductNames.size() > 1) {
			int rand = ThreadLocalRandom.current().nextInt(1,
					lstProductNames.size());

			BrowserActions.clickOnElement(lstProductNames.get(rand - 1),
					driver, "Select quantity option");
		}
		if (lstProductNames.size() == 1) {
			BrowserActions.clickOnElement(lstProductNames.get(0), driver,
					"Select quantity option");
		}
		return new PdpPage(driver).get();
	}

	public boolean verifyFilterByColor() {
		boolean returnValue = false;
		returnValue = Utils.verifyCssPropertyForElement(btnFilterByMobile,
				"color", "rgba(16, 114, 181, 1)");
		return returnValue;
	}

	/**
	 * To click the Filter By button in Mobile
	 * 
	 * @throws Exception
	 */
	public void clickFilterByInMobile() throws Exception {
		BrowserActions.javascriptClick(btnFilterByMobile, driver,
				"Filter By option in mobile");
	}

	/**
	 * To verify Catagory refinement is expanded properly
	 * 
	 * @return status
	 * @throws Exception
	 */
	public boolean verifyExpandToggleOfCatagoryRefinement() throws Exception {
		boolean status = false;
		for (WebElement lstElement : textCatagoryrefinement) {
			// check expand toggle in the catagory refinement
			if (((WebElement) lstElement).getAttribute("class").contains(
					"toggle-up")) {
				status = true;
			} else {
				status = false;
				break;
			}
		}
		return status;
	}

	/**
	 * To verify search refinement size,price brand,color is displayed properly
	 * 
	 * @param filterType
	 *            - Ex. color, size
	 * @return
	 * @throws Exception
	 */
	public boolean verifyRefinement(String filterType) throws Exception {
		boolean status = false;
		for (WebElement lstElement : lstRefinementFilterOptions) {
			List<WebElement> lstEle = lstElement.findElements(By
					.cssSelector(".toggle"));
			for (WebElement element : lstEle) {
				if (BrowserActions
						.getText(driver, element, "Refinement option")
						.equalsIgnoreCase(filterType)) {
					status = true;
					break;
				}
			}
		}

		return status;
	}

	/**
	 * To verify the color of the Size text displayed as blue
	 * 
	 * @return boolean - status
	 * @throws Exception
	 */
	public boolean verifyColorOfSizeRefinementText() throws Exception {
		boolean status = false;
		String cssAttr = "color";
		String cssValue = "rgba(16, 114, 181, 1)";
		WebElement ele = null;
		for (WebElement element : lstRefinementFilterOptions) {
			if (element.getAttribute("class").trim()
					.equals("refinement refinementSize")) {
				ele = element.findElement(By.cssSelector("h3"));
			}
		}
		status = Utils.verifyCssPropertyForElement(ele, cssAttr, cssValue);
		return status;
	}

	public boolean verifyClickHomeInBreadCrumb() throws Exception {
		boolean status = false;
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				Log.message("element.getText() ===>" + element.getText());
				if (element.getText().equals("Home")) {
					BrowserActions.clickOnElement(element, driver,
							"Click on home in Breadcrumb");
					status = true;
					break;
				}

				if (element.getText().equals("Your search results for")) {

					String searchTerm = BrowserActions.getTextFromAttribute(
							driver, lnkSearchKeyword, "title", "Search Term");
					Log.message("searchTerm ===>" + searchTerm);
					status = true;
					break;
				}

			}
		} else if (runPltfrm == "mobile") {
			if (txtProductInBreadcrumbMobile.getText().equals("Back to Home")) {
				BrowserActions.clickOnElement(txtProductInBreadcrumbMobile,
						driver, "Click on home in Breadcrumb");
				status = true;
			}
		}
		return status;
	}

	public boolean verifyErrorMessage(String txtToCompare) throws Exception {
		boolean compare = false;
		String errorMessage = BrowserActions.getText(driver, lblErrorText,
				"Error Label");

		// String strCompare = "We're Sorry But No Results Where Found
		// For:".concat(" " + '"' + txtToCompare + '"' + ".");
		String strCompare = "0 items found for:".concat(" " + '"'
				+ txtToCompare + '"' + ".");
		if (errorMessage.equals(strCompare)) {
			compare = true;
		} else {
			compare = false;
		}
		return compare;
	}

	public boolean verifyCorrectedSearchMessage() throws Exception {
		boolean correctedText = false;
		String correctedMessage = BrowserActions.getText(driver,
				lblCorrectedSearchtxt, "Corrected Message");
		if (correctedMessage.contains("Did you mean")) {
			correctedText = true;
		} else {
			correctedText = false;
		}
		return correctedText;
	}

	public void clickSearchTermInBreadCrumb() throws Exception {
		BrowserActions.clickOnElement(lnkSearchKeyword, driver,
				"Click on Search Term in Breadcrumb");
		waitUntilSearchSpinnerDisappear();
	}

	public PdpPage verifyClickProductName(int index) throws Exception {
		BrowserActions.clickOnElement(lstProductNames.get(index - 1), driver,
				"Select quantity option");
		return new PdpPage(driver).get();
	}

	public void clickBackToTop() throws Exception {
		BrowserActions.scrollToViewElement(txtBacktoTop, driver);
		BrowserActions.clickOnElement(txtBacktoTop, driver,
				"Click on Back to Top link");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To Click Hamburger Link for Mobile
	 * 
	 * @throws Exception
	 */
	public void closeMobileHamburgerMenu() throws Exception {
		BrowserActions.javascriptClick(btnHamburgurClose, driver,
				"Close Mobile HamburgerMenu");
		BrowserActions.nap(1);
		if (!Utils.waitForElement(driver, btnHamburgurClose)) {
			Log.event("clicked Hamburger Menu Close");
		} else {
			throw new Exception("Hamburger Menu didn't Close");
		}
	}

	/**
	 * to get the category ranges count
	 * 
	 * @return int - count
	 */
	public int getPriceRangeValueCount() {
		return driver
				.findElements(
						By.cssSelector("div[class='refinement']>ul>li:not([class*='clear-refinement'])"))
				.size();
	}

	public String getPriceRangeByRow(int index) {
		return lblPriceRange.get(index).getText();
	}

	/**
	 * Click on Expand/Collapse for price refinement
	 * 
	 * @param value
	 * @throws Exception
	 */
	public void priceToggle(boolean value) throws Exception {
		if (value == true && Utils.waitForElement(driver, pricetoggleClosed))
			BrowserActions.clickOnElement(pricetoggleClosed, driver,
					"Price toggle expanded");
		else if (value == false
				&& Utils.waitForElement(driver, pricetoggleExpanded))
			BrowserActions.clickOnElement(pricetoggleExpanded, driver,
					"Price toggle closed");
	}

	public void brandToggle(boolean value) throws Exception {
		if (value == true && Utils.waitForElement(driver, brandToggleClosed))
			BrowserActions.clickOnElement(brandToggleClosed, driver,
					"Brand toggle expanded");
		else if (value == false
				&& Utils.waitForElement(driver, brandToggleExpanded))
			BrowserActions.clickOnElement(brandToggleExpanded, driver,
					"Brand toggle closed");
	}

	public boolean getRefinementExpandedStatus(int index) {
		return driver
				.findElement(
						By.cssSelector(".refinements>div:nth-child(" + index
								+ ")>ul")).getCssValue("display")
				.contains("block");
	}

	/**
	 * to verify that the given web element have check box for each item in list
	 * 
	 * @param element
	 *            refinement web elements with checkbox
	 * @return
	 */
	public boolean refinementCheckBoxStatus(WebElement element) {
		boolean status = true;
		List<WebElement> lstItems = element.findElements(By
				.cssSelector("ul>li:not([class*='clear'])"));
		for (int i = 0; i < lstItems.size(); i++) {
			if (!lstItems.get(i).findElement(By.tagName("i"))
					.getAttribute("class").equals("toggle-check"))
				status = false;
		}
		return status;
	}

	/**
	 * to get string array of the brand name and product name for i-th product
	 * 
	 * @param index
	 *            of product in search result page
	 * @return String Array
	 */
	public String getBrandName(int index) {
		String brandName = new String();
		String productName = new String();
		WebElement productNameLink = totalProducts.get(index - 1).findElement(
				By.className("name-link"));
		productName = productNameLink.getText().trim();

		if (productNameLink.getText().trim().replace(productName, "").trim() != " ") {
			// Log.message("------------>>>>>>>>>>" +
			// productNameLink.getText().trim().replace(brandProductName[1],
			// ""));
			brandName = productNameLink.getText().replace(productName, "");
		}

		return brandName;
	}

	/**
	 * to get string array of the brand name and product name for i-th product
	 * 
	 * @param index
	 *            in search result page
	 * @return String Array
	 */
	public String getProductName(int index) {
		WebElement productNameLink = totalProducts.get(index - 1).findElement(
				By.className("name-link"));
		return productNameLink.getText();
	}

	/**
	 * To verify the product have review stars
	 * 
	 * @param index
	 *            of product
	 * @return 'true' or 'false'
	 * @throws Exception
	 */
	public boolean verifyReviewStarsByRow(int index) throws Exception {
		WebElement element = BrowserActions.checkLocator(driver,
				".search-result-items >li>div .product-review .rating");
		// lstProduct.get(index - 1).findElement(By.cssSelector(".product-review
		// .rating"))
		return Utils.waitForElement(driver, element);
	}

	public int getProductCountInPage() {
		return totalProducts.size();
	}

	/**
	 * To wait for Search results spinner
	 */

	/**
	 * 
	 * To verify option from sort by drop down By Index.(It will only click on
	 * the sort button and it will return the size and name of that particular
	 * element name )
	 * 
	 * @param index
	 * 
	 * @return String
	 * 
	 * @throws Exception
	 * 
	 */

	public ArrayList<String> getSortByList() throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();

		BrowserActions.clickOnElement(drpSortBy, driver, "SortBy dropdown");

		for (WebElement element : lstSortByOptions) {

			if (!element.getText().equals(""))

				lstValues.add(element.getText().trim());

		}

		return lstValues;

	}

	/**
	 * To select the each color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public int getColorSwatchCount() throws Exception {
		// Log.message("swatch count is >> " + lstColor.size());
		return lstColor.size();
	}

	public int getMouseHoveredOnSwatchCount() throws Exception {
		WebElement viewAllColor = driver.findElement(By
				.cssSelector(".product-swatches-cont"));
		Actions actions = new Actions(driver);
		actions.moveToElement(viewAllColor).build().perform();
		List<WebElement> swatchColor = viewAllColor.findElements(By
				.cssSelector(".swatch-list-modal a img"));
		return swatchColor.size();

	}

	/**
	 * 
	 * To get mousehovered on swatch in searchResultPage !
	 *
	 *
	 */

	public String getMouseHoveredOnSwatch(int index) throws Exception {
		WebElement viewAllColor = driver.findElement(By
				.cssSelector(".product-swatches-cont"));
		Actions actions = new Actions(driver);
		actions.moveToElement(viewAllColor).build().perform();
		Log.message("Hovering on the view all color !");
		List<WebElement> swatchColor = viewAllColor.findElements(By
				.cssSelector(".swatch-list-modal a img"));

		actions.clickAndHold(swatchColor.get(index - 1)).build().perform();

		String src = swatchColor.get(index - 1).getAttribute("src")
				.split("&crop=")[0];
		Log.message("Getting src of swatch image !");

		return src;

	}

	/**
	 * To get color src value of primary image
	 * 
	 * @return String
	 */
	public String getPrimaryImageColor() throws Exception {
		return imgPrimary.getAttribute("src").split("&layer")[0];
	}

	public int verifyBreadCrumbHomeCount() {
		int homeCount = txtBreadCrumbHome.size();
		return homeCount;
	}

	/**
	 * To get color src value of swatches by index
	 * 
	 * @param index
	 * @return String
	 */
	public String getSelectedSwatchColors(int index) throws Exception {
		return lstColor.get(index - 1)
				.findElement(By.cssSelector(".swatch-image"))
				.getAttribute("src").split("&crop")[0];

	}

	public void selectColorByIndex(int index) throws Exception {

		BrowserActions.clickOnElement(lstColor.get(index - 1), driver, "Color");
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);

	}

	public String getSortByValueInBreadcrumb() throws Exception {
		String Breabcrumb = BrowserActions.getText(driver, lblSorted,
				"Sorted Value in Breadcrumb");
		return Breabcrumb;
	}

	/**
	 * To get the text from sort by option
	 * 
	 * @return text
	 * @throws Exception
	 */

	public String getTextFromSortBy() throws Exception {
		String dataToBeReturned = null;
		String platform = Utils.getRunPlatForm();
		if (platform == "mobile")
			dataToBeReturned = BrowserActions.getText(driver, txtSortByMobile,
					"SortBy");
		else
			dataToBeReturned = BrowserActions.getText(driver, txtSortBy,
					"SortBy");

		return dataToBeReturned;
	}

	/**
	 * To verify the color of refinement headings.
	 */
	public boolean verifyColorOfRefinementHeadings(String filterType)
			throws Exception {
		boolean status = false;

		for (int i = 1; i < lstRefinementFilterOptions.size() - 1; i++) {
			WebElement toggle = lstRefinementFilterOptions.get(i).findElement(
					By.cssSelector("h3"));
			if (BrowserActions.getText(driver, toggle, "refinement options")
					.trim().equals(filterType)) {
				// Clicking expand toggle if filter list is not expanded
				String cssAttrColor = "color";
				String cssValueOfColor = "rgba(16, 114, 181, 1)";
				status = Utils.verifyCssPropertyForElement(toggle,
						cssAttrColor, cssValueOfColor);
				break;

			}

		}
		return status;
	}

	/*
	 * 
	 * get Count of List in Price Ranges..
	 */

	public int lstPriceCountByRefinement() {
		return lstPriceCountByRefinement.size();
	}

	/*
	 * 
	 * get priceCheckBox Count of Product.
	 */
	public int clickPriceRefinement(int index) throws Exception {
		int dataToBeReturned = 0;
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			clickFilterByInMobile();
			clickCollapseExpandToggle(" ");
		}
		List<WebElement> lstPricechkBox = driver
				.findElements(By
						.xpath("//h3[contains(text(),'Price')]/following-sibling::ul/li"));
		WebElement pricechkBox = lstPricechkBox.get(index);
		String dataToBeReturned1 = pricechkBox
				.findElement(
						By.cssSelector(".refinement-link >span [class='refine-count']"))
				.getText();
		dataToBeReturned1 = dataToBeReturned1.replace("(", "").replace(")", "")
				.trim();
		dataToBeReturned = Integer.parseInt(dataToBeReturned1);
		// BrowserActions.clickOnElement(pricechkBox, driver, "PriceBoxToogle");
		BrowserActions.actionClick(pricechkBox, driver, "PriceBoxToogle");
		Utils.waitForElement(driver, btnFilterByMobile, 10);
		return dataToBeReturned;
	}

	public void verifyCountBrandRefinement() throws Exception {
		BrowserActions.clickOnElement(brandAlphabetToggleExpand.get(0), driver,
				"BrandAlphabetBoxToogle");

	}

	public String getMouseHovered(int index) throws Exception {
		List<WebElement> viewAllColor = null;
		String dataToBeReturned = null;
		boolean status = false;
		int i = 0;
		while (status == false || i == 0)
			for (i = 0; i < driver.findElements(By.cssSelector("a.name-link"))
					.size(); i++) {
				try {
					WebElement e = lstProductNames.get(i)
							.findElement(By.xpath("../.."))
							.findElement(By.cssSelector(".swatch-list"));
					List<WebElement> lstElements = e.findElements(By
							.cssSelector("ul li"));
					if (lstElements.size() >= 5) {
						viewAllColor = lstElements
								.get(0)
								.findElement(By.xpath("../.."))
								.findElements(
										By.cssSelector(".product-swatches-cont"));
						try {
							BrowserActions.scrollToViewElement(
									viewAllColor.get(0), driver);
							BrowserActions.mouseHover(driver,
									viewAllColor.get(0));
							lstElements = viewAllColor.get(0).findElements(
									By.cssSelector(".swatch-list-modal ul li"));
							if (lstElements.size() > 5) {
								List<WebElement> swatchColor = viewAllColor
										.get(0).findElements(
												By.cssSelector("img"));
								String src = swatchColor.get(index - 1)
										.getAttribute("src");
								BrowserActions.scrollToViewElement(
										swatchColor.get(index - 1), driver);
								BrowserActions.javascriptClick(
										swatchColor.get(index - 1), driver,
										"color swatch");
								dataToBeReturned = src.split("&crop=")[0];
								status = true;
								break;
							}
						} catch (Exception e2) {
							// TODO: handle exception
						}
					}
				} catch (Exception e) {
					Log.event("No color is found");
					if (status == true)
						break;
				}
				if (status == false && i == lstProductNames.size() - 1) {
					navigateToPagination("Next");
				}
				GOTO LABEL;
			}
		return dataToBeReturned;
		// WebElement viewAllColor = null;
		// label:
		// for(WebElement element :lstProductNames) {
		// try {
		// WebElement e =
		// element.findElement(By.xpath("../..")).findElement(By.cssSelector(".swatch-list"));
		// List<WebElement> lstElements=
		// e.findElements(By.cssSelector("ul li"));
		// if(lstElements.size()>5) {
		// viewAllColor =
		// lstElements.get(0).findElement(By.xpath("..")).findElement(By.cssSelector(".product-swatches-cont"));
		// break;
		// } else {
		// navigateToPagination("Next");
		// GOTO label;
		// }
		// } catch (Exception e) {
		// Log.event("No color is found");
		// }
		// }
		// // WebElement viewAllColor =
		// driver.findElement(By.cssSelector(".product-swatches-cont"));
		// BrowserActions.scrollToViewElement(viewAllColor, driver);
		// BrowserActions.mouseHover(driver, viewAllColor);
	}

	/**
	 * To get the default option in Item per page drop down
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getDefaultSelection() throws Exception {
		Select mySelect = new Select(defaultSelection);
		WebElement option = mySelect.getFirstSelectedOption();
		String defaultSelection = option.getText();
		return defaultSelection;
	}

	public QuickViewPage navigateToQuickViewByIndex(int index) throws Exception {
		if (lstProductImages.size() > 0) {
			BrowserActions.mouseHover(driver, lstProductImages.get(index - 1));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	public PdpPage selectProduct() throws Exception {
		BrowserActions.clickOnElement(searchResult, driver, "Search result");
		return new PdpPage(driver).get();

	}

	/**
	 * To click on the search data in the "Did you mean" sentence
	 * 
	 * @throws Exception
	 */
	public void clickOnSearchData() throws Exception {
		BrowserActions.clickOnElement(lnkSearchData, driver, "Search Data ");
	}

	/**
	 * To navigate to CategoryLandingPage
	 * 
	 * @return CategoryLandingPage
	 * @throws Exception
	 */
	public CategoryLandingPage clickCatgeoryRefinement() throws Exception {
		List<WebElement> lstElement = null;
		String txtsubcategory = null;
		WebElement element = null;
		for (WebElement element1 : lstCategoryRefinement) {
			element = element1;
			break;
		}

		lstElement = element.findElement(By.xpath("..")).findElements(
				By.cssSelector("div[class*='category-refinement']>ul>li>a"));
		for (int i = 0; i < lstElement.size(); i++) {
			WebElement e = lstElement.get(i);
			String txtcategory = e.getText();
			if (txtcategory.contains("(")) {
				e.click();
				break;
			}
			lstElement = element
					.findElement(By.xpath(".."))
					.findElements(
							By.cssSelector("div[class*='category-refinement']>ul>li[class*='toggle-up'] .subcategories li a"));
			for (int j = 0; j < lstElement.size(); j++) {
				WebElement sube = lstElement.get(j);
				txtsubcategory = sube.getText();
				if (txtsubcategory.contains("(")) {
					sube.click();
					break;
				}
			}
		}
		return new CategoryLandingPage(driver).get();
	}

	/**
	 * To get the Text - Refined By from breacrumb
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextRefinedByfromBreadcrumb() throws Exception {

		return BrowserActions.getText(driver, txtRefinedByInBreadcrumb,
				"Refined by");

	}

	/**
	 * To get the list of brand names
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getBrandNameInProductlist() throws Exception {

		ArrayList<String> arrlist = new ArrayList<String>();
		Utils.waitForElement(driver, lstBrandName.get(1));
		for (WebElement element : lstBrandName) {
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from breadcrumb"));
		}
		return arrlist;

	}

	public boolean clickCollapseToggle(String filterType) throws Exception {
		boolean status = false;
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			if (element1.getAttribute("class").trim()
					.equals("refinement " + filterType)) {
				element1 = element1.findElement(By.cssSelector("h3 i"));
				element = element1;
				BrowserActions.clickOnElement(element, driver,
						"Collapse/Expand toggle");
				status = true;
				break;
			}
		}

		waitUntilSearchSpinnerDisappear();
		return status;
	}

	/**
	 * To select the each color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public int getColorSwatchCount(String... allColorSwatches) throws Exception {
		List<WebElement> lstElements = null;
		if (allColorSwatches.length > 0) {
			lstElements = listColor;
		} else {
			lstElements = lstColor;
		}
		return lstElements.size();
	}

	public String getTextFromContentHeader() throws Exception {
		return (BrowserActions.getText(driver, txtContentHeader,
				"Content Header text"));
	}

	public HomePage clickOnHome() throws Exception {
		for (int i = 0; i < txtBreadCrumbHome.size(); i++) {
			BrowserActions.clickOnElement(txtBreadCrumbHome.get(0), driver,
					"Click on home");
		}
		return new HomePage(driver);
	}

	public void clickViewAllArticlesButton() throws Exception {
		BrowserActions.clickOnElement(btnViewAllArticles, driver,
				"Click on View All Articles button");
	}

	public String getSearchTerm() throws Exception {
		String searchTerm = BrowserActions.getTextFromAttribute(driver,
				txtSearchTerm, "title", "Search Term");
		return searchTerm;
	}

	public String getArticleBreadcrumbText() throws Exception {
		String breadCrumb = BrowserActions.getText(driver,
				txtArticlesBreacrumb, "Article breadcrumb");
		return breadCrumb;
	}

	public boolean verifyCountOfArticlesDisplayed() throws Exception {
		if (lstArticlesFound.size() > 6) {
			Log.event("Article displayed is more than 6");
			return false;

		}
		Log.event("Article displayed is less than or equal to 6");
		return true;
	}

	public void clickOnViewAllArticles() throws Exception {
		BrowserActions.javascriptClick(btnViewAllArticles, driver,
				"Clicked on View Articles");
		BrowserActions.nap(2);
	}

	/**
	 * To Click on 'Back To Product' link in View All Articles Page
	 * 
	 * @return
	 * @throws Exception
	 */
	public void clickOnBackToProductResults() throws Exception {
		BrowserActions.clickOnElement(lnkBackToSearchResults, driver,
				"BackToSearchResults");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * @param ProductName
	 * @return index
	 * @throws Exception
	 */
	public int getIndexByProductName(String productName) throws Exception {
		boolean status = false;
		int i = 0;
		while (status == false || i == 0) {
			for (i = 0; i < lstProductNames.size(); i++) {
				if (productName.contains(lstProductNames.get(i).getText())) {
					status = true;
					return i;
				}
			}
			if (status == false) {
				int pageNo = getCurrentPageNo() + 1;
				navigateToPagination(String.valueOf(pageNo));
			}
		}
		return (-1);

	}

	public ArrayList<String> getProductDetails(int index) throws Exception {
		ArrayList<String> ProductDetails = new ArrayList<>();
		ProductDetails.add(0, BrowserActions.getText(driver,
				lstProductNames.get(index), "Product Name"));
		// ProductDetails.add(1,BrowserActions.getText(driver,
		// lstProductStandardPrice.get(index), "Product Standard Price"));
		// ProductDetails.add(2,BrowserActions.getText(driver,
		// lstProductSalesPrice.get(index), "Product Sales Price"));
		return ProductDetails;

	}

	public int refinementValuesCountViewMore(String filterType,
			String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		int refinementCount = 0;
		// Getting the web element that matches the 'filterType'
		Utils.waitForPageLoad(driver);
		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul li:not([class='view-more-less'])"));
			refinementCount = lstElement.size();
			Utils.waitForPageLoad(driver);

		}

		return refinementCount;
	}

	public int refinementValuesCountViewLess(String filterType,
			String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		int refinementCount = 0;
		// Getting the web element that matches the 'filterType'
		Utils.waitForPageLoad(driver);
		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul li:not([class='view-more-less'])"));
			refinementCount = lstElement.size();
			Utils.waitForPageLoad(driver);

		}

		return refinementCount;
	}

	/**
	 * To click the view less link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickViewLessLnk(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}
		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_LESS)), driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To click the view more link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickViewMoreLink(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}
		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_MORE)), driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To click on catagory name
	 * 
	 * @throws Exception
	 */
	public void clickOnCatagoryTextInBreadrcrumb() throws Exception {
		BrowserActions.clickOnElement(lnkSelectedCatagoryInBreadcrumb, driver,
				"Selected Catagory");

	}

	/**
	 * To click on first selected Catagory in breadcrumb
	 * 
	 * @throws Exception
	 */
	public void clickOnFirstSelectedCatagory() throws Exception {

		BrowserActions.clickOnElement(
				txtSearchResultInBreadcrumb.findElement(By.cssSelector("a")),
				driver, "First selected catagory");
	}

	/**
	 * To click On Items per page drop down
	 * 
	 * @throws Exception
	 */
	public void clickOnItemsPerPageDropDown() throws Exception {

		BrowserActions
				.clickOnElement(drpItemsPerPage, driver, "Items per page");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the option value in items per page drop down
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getOptionValueInItemsPerPage() throws Exception {

		ArrayList<String> arrlist = new ArrayList<String>();
		Utils.waitForElement(driver, optionsInItemsPerPge.get(1));
		for (WebElement element : optionsInItemsPerPge) {
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from breadcrumb"));
		}
		return arrlist;

	}

	/**
	 * To scroll to article
	 * 
	 * @return
	 * @throws Exception
	 */
	public void ScrollToArticle() throws Exception {
		BrowserActions.scrollToViewElement(lstArticles.get(0), driver);

	}

	public boolean verifyArticlesTitleBold() throws Exception {

		boolean status = false;
		for (WebElement element : lstArticles) {
			status = Utils.verifyCssPropertyForElement(
					element.findElement(By.cssSelector("a.content-title")),
					"font-family", "HelveticaNeue-Bold, Arial");

		}
		return status;
	}

	public boolean verifyLThreeCatagoryTitleBold() throws Exception {

		boolean status = false;
		for (WebElement element : lstArticles) {
			status = Utils.verifyCssPropertyForElement(
					element.findElement(By.cssSelector("a.content-title")),
					"font-family", "HelveticaNeue-Bold, Arial");

		}
		return status;
	}

	public int refinementValuesCount(String filterType, String... optionValue)
			throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		int refinementCount = 0;
		// Getting the web element that matches the 'filterType'
		Utils.waitForPageLoad(driver);
		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element
					.findElement(By.xpath(".."))
					.findElements(
							By.cssSelector("div[class='refinement "
									+ filterType.trim()
									+ " ']>ul>li:not([class*='view-more']):not([class*='hide'])"));
			refinementCount = lstElement.size();
			Utils.waitForPageLoad(driver);

		}

		return refinementCount;
	}

	/*
	 * To click the view more/less link
	 * 
	 * @param filterType
	 * 
	 * @throws Exception
	 */

	public void clickViewMore(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}
		BrowserActions.scrollToViewElement(
				element.findElement(By.cssSelector(LNK_VIEW_MORE)), driver);

		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_MORE)), driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To click the view less link
	 * 
	 * @param filterType
	 * @throws Exception
	 */

	public void clickViewLess(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement refinement" + filterType)) {
				element = element1;
				break;
			}
		}
		BrowserActions.scrollToViewElement(
				element.findElement(By.cssSelector(LNK_VIEW_LESS)), driver);

		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_LESS)), driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyArticlesDecendingOrder() throws Exception {
		boolean status = false;
		ArrayList<String> arrlistBeforeSort = new ArrayList<String>();
		ArrayList<String> arrlistAfterSort = new ArrayList<String>();

		for (WebElement element : lstArticles) {
			arrlistBeforeSort.add(BrowserActions.getText(driver,
					element.findElement(By.cssSelector("a.content-title")),
					"Article title"));

		}
		arrlistAfterSort.addAll(arrlistBeforeSort);
		Collections.sort(arrlistAfterSort);
		Collections.reverse(arrlistAfterSort);
		if (arrlistBeforeSort.equals(arrlistAfterSort)) {
			status = true;

		}
		// after sorting

		return status;
	}

	public ArrayList<String> getTextFromCatagories() throws Exception {

		ArrayList<String> arrlistCatagory = new ArrayList<String>();

		for (WebElement element : refinementCatagory) {
			arrlistCatagory.add(BrowserActions.getText(driver, element,
					"Catagory refinement"));

		}
		return arrlistCatagory;
	}

	/**
	 * Is refinement present or not
	 * 
	 * @param filterType
	 * @return
	 * @throws Exception
	 */
	public boolean isRefinementPrsent(String filterType) throws Exception {
		boolean status = false;

		// Getting the web element that matches the 'filterType'
		Utils.waitForPageLoad(driver);
		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {

					status = true;
					break;
				}
			}

		}
		return status;

	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(2);
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
		BrowserActions.nap(2);
	}

	/**
	 * To select the product which have swatch and any swatch is by default
	 * selected
	 * 
	 * @throws Exception
	 */
	public PdpPage selectRandomColorSwatchProduct() throws Exception {
		if (lstOfColorSwatchProduct.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0,
					lstOfColorSwatchProduct.size());
			String productName = BrowserActions.getTextFromAttribute(driver,
					lstOfColorSwatchProduct.get(rand), "alt", "Product Name");
			BrowserActions.scrollToViewElement(
					lstOfColorSwatchProduct.get(rand), driver);
			WebElement ele = lstOfColorSwatchProduct.get(rand).findElement(
					By.xpath("../../../.."));
			ele.getAttribute("innerHTML");
			WebElement ele1 = ele.findElement(By
					.xpath("preceding-sibling::div[@class='product-name']/a"));
			BrowserActions
					.javascriptClick(ele1, driver, "click swatch product");
			waitForSpinner();
		} else {
			throw new Exception("coclor Swatch product is not shown in the UI");
		}
		return new PdpPage(driver).get();
	}

	public void clickOnViewAllArticleButton() throws Exception {
		Utils.waitForElement(driver, btnViewAllArticles);
		BrowserActions.scrollToViewElement(btnViewAllArticles, driver);
		BrowserActions.javascriptClick(btnViewAllArticles, driver,
				"View All Articles");

	}

	/**
	 * Verify the Saved default Address displayed in Gray color
	 * 
	 * @param index
	 * @throws Exception
	 */
	public String verifyLabelStyleBold() {
		String Actualcolor = breadcrumbStyle.getCssValue("font-family").split(
				",")[0];
		return Actualcolor;

	}

	/*
	 * get the random color from swatch from refinment
	 */

	public String selectSwatchColorFromRefinment() throws Exception {
		int rand = ThreadLocalRandom.current()
				.nextInt(0, lstColorSwatch.size());
		String colorName = BrowserActions.getTextFromAttribute(driver,
				lstColorSwatch.get(rand), "title", "color Name").split(":")[1]
				.trim();
		BrowserActions.scrollToViewElement(lstColorSwatch.get(rand), driver);
		BrowserActions.javascriptClick(lstColorSwatch.get(rand), driver,
				"click swatch color");
		waitForSpinner();
		return colorName;

	}

	/*
	 * get the brand name from refinement
	 */

	public ArrayList<String> getBrandNameInRefinement() throws Exception {
		ArrayList<String> arrlist = new ArrayList<String>();
		for (WebElement element : brandName) {
			BrowserActions.scrollToView(element, driver);
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from brand"));
		}
		return arrlist;

	}

	/**
	 * To click the view more link// btnBrandViewMore
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickViewMoreLinkInBrandRefinement() throws Exception {
		Utils.waitForElement(driver, brandViewMore);
		BrowserActions.scrollToViewElement(brandViewMore, driver);
		BrowserActions.javascriptClick(brandViewMore, driver,
				"click on the Tab");
		waitUntilSearchSpinnerDisappear();
	}

	public int brandAfterClickViewMoreProductCount() {
		int size = brandAfterExpandProductCount.size();
		return size;
	}

	public void clickOnViewColorExpandForMobileView() throws Exception {
		Utils.waitForElement(driver, viewColorExpand);
		BrowserActions.scrollToViewElement(viewColorExpand, driver);
		BrowserActions.javascriptClick(viewColorExpand, driver,
				"view Color Expand");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the current value of the "Item per Page" dropdown
	 * 
	 * @return String - Current value Ex. View 30
	 * @throws Exception
	 */
	public String getCurrentValueInItemPerPageDropdown() throws Exception {

		return BrowserActions
				.getText(driver, drpItemsPerPage.findElement(By
						.cssSelector(".selected-option")),
						"Item per page dropdown value");
	}

	public boolean VerifySelectedFilterOptions(String filterType,
			String optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		boolean status = false;

		// Getting the web element that matches the 'filterType'

		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul>li:not([class*='hide'])"));

			for (int i = 0; i < lstElement.size(); i++) {
				WebElement e = lstElement.get(i);
				filterValue = e.getText();
				if (filterValue.contains(optionValue)) {
					status = BrowserActions.isRadioOrCheckBoxSelected(e);
				}

				waitUntilSearchSpinnerDisappear();

			}
		}
		return status;
	}

	/**
	 * To get the current Y coordinate value
	 * 
	 * @index index
	 * @return int -Y coordinate value
	 */
	public int getCurrentYLocationOfArticleByIndex(int index) {
		WebElement image = lstArticles.get(index - 1);
		Point point = image.getLocation();
		return point.getY();
	}

	public void clickCollapseExpandToggleMobile() throws Exception {
		BrowserActions.clickOnElement(btncolor, driver,
				"Collapse/Expand toggle");
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To get the current X coordinate value
	 * 
	 * @index index
	 * @return int - X coordinate value
	 */
	public int getCurrentXLocationOfArticleByIndex(int index) {
		WebElement image = lstArticles.get(index - 1);
		Point point = image.getLocation();
		return point.getX();

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getImageUrlInListPage(int index) throws Exception {
		return (BrowserActions.getTextFromAttribute(driver,
				lstImgPrimary.get(index - 1), "src", "image src"));
	}

	public void clickCollapseExpandToggleofSubSection(String filterType,
			String... collapse) throws Exception {
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (collapse.length > 0
				&& toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		} else if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}
		waitUntilSearchSpinnerDisappear();

		BrowserActions.clickOnElement(
				element.findElements(By.cssSelector("ul li.brand-alphabet"))
						.get(0), driver, "sub section");

	}

	public void clickOnBtnWomen() throws Exception {
		BrowserActions.javascriptClick(btnWomen, driver, "Clicked on Women");
	}

	public boolean verifyColorForFilterByMobile() throws Exception {
		boolean status = false;
		String cssAttr = "color";
		String color = btnFilterByMobile.getCssValue("color");
		status = Utils.verifyCssPropertyForElement(btnFilterByMobile, cssAttr,
				color);
		return status;
	}

	/**
	 * To hover on quickView
	 * 
	 * @throws Exception
	 */
	public void hoverOnQuickView() throws Exception {
		if (lstProductImages.size() > 0) {
			Actions actions = new Actions(driver);
			int rand = Utils.getRandom(0, lstProductImages.size());
			BrowserActions.scrollToViewElement(lstProductImages.get(rand),
					driver);
			actions.moveToElement(lstProductImages.get(rand)).build().perform();

			// BrowserActions.mouseHover(driver, lstProductImages.get(rand));
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		Utils.waitForPageLoad(driver);
	}

	public String getSelectedOptionFromItemsPerPageafterchangingValue()
			throws Exception {
		return BrowserActions.getText(driver, dropItemPerPage,
				"Items per Page Dropdown");

	}

	public boolean verifyTextColorFromItemsPerPageDropDown() throws Exception {
		String actualClassProperty = dropItemPerPage.getCssValue("color");

		if (actualClassProperty.equals("rgba(0, 0, 0, 1)")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean verifyTextColorOptionSortByDropdown() throws Exception {
		boolean dataToReturned = true;
		String normalRGBValue = "rgba(0, 0, 0, 1)";
		for (int i = 0; i < listSortByOptions.size(); i++) {

			dataToReturned &= Utils.verifyCssPropertyForElement(
					listSortByOptions.get(i), "color", normalRGBValue);

		}
		return dataToReturned;
	}

	public String getSelectedOptionFromItemsPerPageDropDown() throws Exception {
		return BrowserActions.getText(driver, drpItemPerPage,
				"Items per Page Dropdown");

	}

	public ArrayList<String> getItemsPerPageOptionList() throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();
		lstValues.add(drpItemPerPage.getText());
		BrowserActions.clickOnElement(drpItemPerPage, driver,
				"Items Per Page dropdown");

		for (int i = 0; i < listItemsPerPageOptions.size(); i++) {

			if (!listItemsPerPageOptions.get(i).getText().equals(""))

				lstValues.add(listItemsPerPageOptions.get(i).getText().trim());
		}

		// "Items Per Page dropdown");
		return lstValues;
	}

	public void deleteAllCookies() {
		driver.manage().deleteAllCookies();
		driver.navigate().refresh();
	}

	public PrivacyAndPolicyPage navigateToPrivacyPolicyPage() throws Exception {
		BrowserActions.clickOnElement(lnkReadmore, driver,
				"Read more link in Article");
		return new PrivacyAndPolicyPage(driver).get();

	}

	public boolean VerifyCategoryToggle() throws Exception {
		boolean returnValue = false;
		for (int i = 0; i < lstCategoryRefinementToggle.size(); i++) {
			BrowserActions.clickOnElement(lstCategoryRefinementToggle.get(i),
					driver, "Category Refinement Expand Toggle");
			WebElement toggle = lstCategoryRefinementToggle.get(i);
			if (!toggle.getAttribute("class").contains("toggle-up")) {
				returnValue = true;
				BrowserActions.clickOnElement(
						lstCategoryRefinementToggle.get(i), driver,
						"Category Refinement Expand Toggle");
			}
		}
		return returnValue;
	}

	/**
	 * To click on close icon in search refinement section
	 */
	public void clickOnCloseIcon() throws Exception {

		BrowserActions.clickOnElement(IconCloseMenu, driver, "Refinement Page");
	}

	/**
	 * To click the collapse/expand button of 'filterType' in the filter panel
	 * 
	 * @param filterType
	 *            - Eg: color, size
	 * @throws Exception
	 */
	public void clickCollapseExpandToggle(String filterType, String... collapse)
			throws Exception {
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (collapse.length > 0
				&& toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		} else if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}
		waitUntilSearchSpinnerDisappear();
	}

	/**
	 * To select/unselect the refinements in the search refinement panel
	 * 
	 * @param filterType
	 * @param optionValue
	 * @return String
	 * @throws Exception
	 */
	public String selectUnselectFilterOptions(String filterType,
			String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		// Getting the web element that matches the 'filterType'

		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul>li:not([class*='hide'])"));
			if (!(optionValue.length > 0)) {
				if (lstElement.size() == 1) {
					WebElement e = lstElement.get(0);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				} else {
					int rand = ThreadLocalRandom.current().nextInt(1,
							lstElement.size());
					WebElement e = lstElement.get(rand - 1);
					// Clicking random filter values if not mentioned
					BrowserActions.scrollToViewElement(e, driver);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				}

			} else {
				WebElement e = BrowserActions.getMachingTextElementFromList(
						lstElement, optionValue[0], "contains");
				// Clicking filter option based on 'optionValue'
				// BrowserActions.clickOnElement(e.findElement(By.cssSelector("a
				// i")),
				// driver, "Filter values");
				WebElement selectoption = e.findElement(By.cssSelector("a i"));
				if (selectoption.getAttribute("class").contains(
						"toggle-check-active")) {
					BrowserActions.javascriptClick(e.findElement(By
							.cssSelector("a i[class*='toggle-check-active']")),
							driver, "Un selecting the Filter values");
				} else {
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Selectin the Filter values");
				}
				waitUntilSearchSpinnerDisappear();
				filterValue = optionValue[0].toString();

			}
		}

		Utils.waitForPageLoad(driver);

		return filterValue;
	}

	public String getBreadCrumbEndValue() throws Exception {
		String BreadCrumbValue = BrowserActions.getText(driver,
				BreadCrumbEndValue, "BreadCrumb End Value").split("Your")[1]
				.trim();
		return BreadCrumbValue;
	}

	/*
	 * Click on Expand/Collapse for Color refinement
	 * 
	 * @param value
	 * 
	 * @throws Exception
	 */
	public void colorToggle(boolean value) throws Exception {
		if (value == true && Utils.waitForElement(driver, btncolor))
			BrowserActions.clickOnElement(btncolor, driver,
					"Color toggle expanded");
		else if (value == false
				&& Utils.waitForElement(driver, viewColorExpand))
			BrowserActions.clickOnElement(viewColorExpand, driver,
					"Color toggle closed");
	}

	public String getTextFromProductBadge() throws Exception {
		return BrowserActions.getText(driver, txtProductBadge, "Product batch");
	}

	public HashMap<String, String> getPriceDetailsByIndex(int index)
			throws Exception {
		HashMap<String, String> priceDetails = new HashMap<String, String>();
		String className = BrowserActions.getTextFromAttribute(driver,
				lstNowPrice_Sale_Price.get(index - 1), "class", "Class Name");

		if (className.contains("no-standard-price")) {
			priceDetails
					.put("NowPrice", BrowserActions.getText(driver,
							lstNowPrice_Sale_Price.get(index - 1), "Now Price"));
		} else {
			priceDetails.put("OriginalPrice", BrowserActions.getText(driver,
					lstOriginalPrice_Standard_Price.get(index - 1),
					"Original Price"));
			priceDetails
					.put("NowPrice", BrowserActions.getText(driver,
							lstNowPrice_Sale_Price.get(index - 1), "Now Price"));
		}
		return priceDetails;
	}

	/*
	 * get the random color from swatch from refinment by index
	 */

	public String selectSwatchColorFromRefinmentByIndex(int index)
			throws Exception {
		String colorName = BrowserActions.getTextFromAttribute(driver,
				lstColorSwatch.get(index - 1), "title", "color Name")
				.split(":")[1].trim();
		BrowserActions.scrollToViewElement(lstColorSwatch.get(index - 1),
				driver);
		BrowserActions.javascriptClick(lstColorSwatch.get(index - 1), driver,
				"click swatch color");
		Utils.waitForPageLoad(driver);
		return colorName;

	}

	public void clickOnListToggleIcon() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			BrowserActions.javascriptClick(listToggleMobile, driver,
					"List Toggle");
		} else {
			BrowserActions.javascriptClick(listToggle, driver, "List Toggle");
		}
	}
}
