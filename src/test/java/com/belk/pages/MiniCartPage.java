package com.belk.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class MiniCartPage extends LoadableComponent<MiniCartPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	/**********************************************************************************************
	 ********************************* WebElements of PDP Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".mini-cart-content[style*='block']")
	WebElement miniCartContent;

	@FindBy(css = "a[id='mini-cart-link-checkout']")
	WebElement btncheckout;

	@FindBy(css = ".mini-cart-products .success")
	WebElement miniCartSuccessMsg;

	@FindBy(id = "mini-cart")
	WebElement miniCart;

	@FindBy(css = "a.mini-cart-link")
	WebElement lnkMiniCart;

	@FindBy(css = "span[class='minicart-quantity']")
	WebElement itemCountInMiniCart;

	@FindBy(css = "div[class='mini-cart-name']>a")
	WebElement miniCartProductName;

	

	////////////////////////////////////////////////

	@FindBy(css=".mini-cart-image>img")
	WebElement imgMinicartImage;

	@FindBy(css="div[data-attribute='color']")
	List<WebElement> colorInMinicart;

	@FindBy(css="div[data-attribute='size']")
	List<WebElement> sizeInMinicart;

	@FindBy(css="div[class='mini-cart-item-total']")
	List<WebElement> quantityInMinicart;

	@FindBy(css="div[class='mini-cart-pricing']")
	List<WebElement> pricesInMinicart;

	@FindBy(css = "div[class='mini-cart-name']>a")
	List<WebElement> productNameInMinicart;

	//	@FindBy(css= "button[class='button-fancy-large']")
	//	WebElement btnCheckout;

	@FindBy(css="div[class='selected-option selected']")
	List<WebElement> lstQuantityValue;


	@FindBy(css=".minicart-quantity")
	WebElement txtMinicartQuantityText;

	@FindBy(css=".user-logout>a")
	WebElement btnLogout;

	@FindBy(css=".mini-cart-total>a")
	WebElement iconMinicart;

	@FindBy(css=".button-text")
	List<WebElement> lstBtnRemove;

	@FindBy(css = "div[class='mini-cart-content'] .ps-scrollbar-y")
	WebElement scrollbar;

	@FindBy(xpath=".//div[contains (@class,'mini-cart-products')]")
	WebElement panalMinicartProducts;
	
	@FindBy(css = ".mini-cart-content")
	WebElement minicartContent;
	
	@FindBy(xpath = ".//*[@id='scrollbar-0']/div[2]/span")
	WebElement clicktoggle;
	
	@FindBy(css=".mini-cart-subtotals")
	WebElement ordertotal;
	
	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;
	
	@FindBy(css=".mini-cart-slot .mini-cart-saveamount")
	WebElement txtSaveProductDiscountSavingsMsg;
	//////////////////////////////////////////////

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public MiniCartPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, miniCart))) {
			Log.fail("Mini cart did not displayed.", driver);
		}

		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To mouse hover to mini cart
	 * @throws Exception 
	 */
	public void mouseOverMiniCart() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();	

		if(!Utils.waitForElement(driver, miniCartContent,2)){
			if(runPltfrm=="desktop")
				BrowserActions.mouseHover(driver, miniCart);
		}

		if(Utils.waitForElement(driver, miniCartContent,2)){
			Log.event("Mini cart overlay already opened");
		}else{					
			if(runPltfrm=="desktop")
				BrowserActions.mouseHover(driver, miniCart);
			else if(runPltfrm=="mobile")	
				BrowserActions.javascriptClick(lnkMiniCart, driver, "mini cart ");			
		}
	}

	/**
	 * To get the success message from mini cart
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getSuccessMsgFromMiniCard() throws Exception {
		//		mouseOverMiniCart();
		String txtShippingMessage = BrowserActions.getText(driver, miniCartSuccessMsg,
				"Fetching the  bag overly text value in the minicart");
		return txtShippingMessage;
	}

	/**
	 * To get the product count from mini cart
	 * 
	 * @return txtProductCount
	 * @throws Exception
	 */
	public String getCartProductCount() throws Exception {
		String txtProductCount = BrowserActions.getText(driver, itemCountInMiniCart, "Getting item count in the cart");
		return txtProductCount;
	}

	/**
	 * To navigating to shopping bag page by clicking 'View Bag' in mini cart
	 * 
	 * @return ShoppingBagPage
	 * @throws Exception
	 */
	public ShoppingBagPage navigateToBag() throws Exception {
		if(Utils.waitForElement(driver, minicartContent))
			if(minicartContent.getCssValue("display").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			int productCount = Integer.parseInt(getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if(productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
		BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click the product name in the minicart
	 * @return
	 * @throws Exception
	 */
	public PdpPage clickProductNameOnMiniCart() throws Exception {

		mouseOverMiniCart();
		Utils.waitForElement(driver, miniCartContent);
		BrowserActions.javascriptClick(miniCartProductName, driver, "clicking producy name in the overly bag");

		return new PdpPage(driver).get();
	}

	/**
	 * To get the count in minicart icon
	 * @return
	 * @throws Exception
	 * Last Modified By - Gowri & Last Modified Date - 03/01/2017
	 */
	public int getMinicartCount() throws Exception{
		String minicartQryText=BrowserActions.getText(driver, txtMinicartQuantityText, "Minicart quantity text");
		return Integer.parseInt(minicartQryText.trim());
	}

//	/**
//	 * To remove all added cart items for registered user
//	 * @throws Exception
//	 */
//	public void removeAddedCartItems() throws Exception{
//		String signStatus=BrowserActions.getText(driver, btnLogout, "Logout text");
//		if(signStatus.equals("Sign Out")){
//			if(getMinicartCount()>0){
//				BrowserActions.clickOnElement(iconMinicart, driver, "View bag button");
//
//				for(int i=0;i<lstBtnRemove.size();i++){
//					BrowserActions.clickOnElement(lstBtnRemove.get(i), driver, "Remove button");
//				}
//			}else{
//				Log.event("Minicart is empty.");
//			}
//
//		}else
//			Log.message("Please login with valid credential");
//	}

	/**
	 * To get the product quantity in the minicart popup by index
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getProductQuantityByIndex(int index) throws Exception{
		return(BrowserActions.getText(driver, quantityInMinicart.get(index-1), "Product quantity"));
	}

	/**
	 * To get the product name in the minicart popup by index
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getProductNameByIndex(int index) throws Exception{
		return(BrowserActions.getText(driver, productNameInMinicart.get(index-1), "Product name"));
	}

	/**
	 * To get the product color in the minicart popup by index
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getProductColorByIndex(int index) throws Exception{
		return(BrowserActions.getText(driver, colorInMinicart.get(index-1), "Product color"));
	}

	/**
	 * To get the product size in the minicart popup by index
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getProductSizeByIndex(int index) throws Exception{
		return(BrowserActions.getText(driver, sizeInMinicart.get(index-1), "Product size"));
	}

	/**
	 * To get the product price in the minicart popup by index
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getProductPriceByIndex(int index) throws Exception{
		return(BrowserActions.getText(driver, pricesInMinicart.get(index-1), "Product prices"));
	}

	/**
	 * TO get the product total quantity in the Shopping bag
	 * @return
	 * @throws Exception
	 */
	public int getProductTotalQuantity() throws Exception{
		int value=0;
		for(int i=0;i<lstQuantityValue.size();i++){
			String temp=null;
			temp=BrowserActions.getText(driver, lstQuantityValue.get(i), "Quantity in cartpage").replace("\"", "");
			value+=Integer.parseInt(temp);
		}
		return value;
	}

	/**
	 * TO get the quantity of all products in the minicart
	 * @return
	 * @throws Exception
	 */
	public int getQuantityOfAllProductInMinicart() throws Exception{
		int qty = 0;
		Utils.waitForElement(driver, miniCartContent,2);
		for (int i = 1; i <= quantityInMinicart.size(); i++) {
			String txtQuantity = getProductQuantityByIndex(i);
			String[] txtQty = txtQuantity.split(" ");
			qty += Integer.parseInt(txtQty[1]);
		}
		return qty;
	}
	
	/**
	 * To get the total item count in the minicart
	 * @return
	 * @throws Exception
	 */
	public int getTotalItemInMinicart() throws Exception{
		int qtycount=0;
		qtycount=quantityInMinicart.size();
		return qtycount;
	}

	/**
	 * To verify the subtotal is displayed below the list of products
	 * @return
	 */
	public boolean verifySubTotalBelowListOfProducts(){
		return Utils.waitForElement(driver, panalMinicartProducts.findElement(By.xpath("following-sibling::div[@class='mini-cart-totals']/div[@class='mini-cart-subtotals']")));
	}
	
	public boolean verify5thProductIsNotVisibleInTheViewBag() throws Exception {
		boolean status = false;
		WebElement firstproductimage = driver.findElement(By.xpath(".//*[@id='scrollbar-0']/div[5]"));
		if (!Utils.waitForElement(driver, firstproductimage))
			{
			status = true;
		    }
      return status;
	}
	public boolean verify4thProductIsVisibleIntheViewBag() throws Exception {
		boolean status = false;
		WebElement firstproductimage = driver.findElement(By.xpath(".//*[@id='scrollbar-0']/div[4]"));
		if (firstproductimage.isDisplayed())
			{
			status = true;
		    }
      return status;
	}

	public boolean verify1stProductImageIsVisible() throws Exception {
		boolean status = false;
		WebElement firstproductimage = driver.findElement(By.xpath(".//*[@id='scrollbar-0']/div[1]/div[1]/img"));
		if (firstproductimage.isDisplayed())
			{
			status = true;
		    }
      return status;
	}

	public boolean verifyImageIsInvisibleForMorethanOneProduct() throws Exception {
		boolean status = false;
		WebElement secondproductimage = driver.findElement(By.xpath(".//*[@id='scrollbar-0']/div[2]/div[1]/img"));
		if (!secondproductimage.isDisplayed())
			{
			status = true;
		    }
      return status;
	}

	public void clickon2ndProducttoggle() throws Exception {
		BrowserActions.clickOnElement(clicktoggle, driver, "Mini Cart Button");
	} 

	/**
	 * To remove all added cart items for registered user
	 * @throws Exception
	 */
	public void removeAddedCartItems() throws Exception{
	       String signStatus=BrowserActions.getText(driver, btnLogout, "Logout text");
	       if(signStatus.equals("Sign Out")){
	              if(getMinicartCount()>0){
	                     if (Utils.waitForElement(driver, minicartContent))
	                           if (minicartContent.getCssValue("display").contains("block"))
	                                  BrowserActions.nap(10);
	                     String runPltfrm = Utils.getRunPlatForm();
	                     if (runPltfrm == "mobile") {
	                           MiniCartPage minicart = new MiniCartPage(driver).get();
	                           int productCount = Integer.parseInt(minicart.getCartProductCount());
	                           BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
	                           if (productCount != 0)
	                                  BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
	                     } else {
	                           BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
	                     }
	                     Utils.waitForPageLoad(driver);
	            
	                     for(int i=0;i<lstBtnRemove.size();i++){
	                           BrowserActions.clickOnElement(lstBtnRemove.get(i), driver, "Remove button");
	                     }
	              }else{
	                     Log.event("Minicart is empty.");
	              }

	       }else
	              Log.message("Please login with valid credential");
	}

	/**
	 * To get the SaveProductDiscountSavingsMsg  in the minicart
	 * @return: Save Product Discount SavingsMsg
	 * @throws Exception
	 */
	public String getSaleProductDiscountSavingsMsg() throws Exception{
	String datatobeReturned=BrowserActions.getText(driver, txtSaveProductDiscountSavingsMsg, "SaveProductDiscountSavingsMsg");
		return datatobeReturned;
	}

}// MiniCartPage
