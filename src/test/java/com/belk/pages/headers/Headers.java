package com.belk.pages.headers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ClearancePage;
import com.belk.pages.CouponsPage;
import com.belk.pages.ElementLayer;
import com.belk.pages.FindInStorePage;
import com.belk.pages.GiftCardsPage;
import com.belk.pages.HomePage;
import com.belk.pages.MiniCartPage;
import com.belk.pages.PdpPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class Headers extends LoadableComponent<Headers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of Headers ***********************************
	 **********************************************************************************************/
	private static final String HEADER_TOP = ".header-top ";
	private static final String HEADER_LEFT_CONTENT = HEADER_TOP
			+ ".menu-utility-cards .content-asset ";
	private static final String HEADER_RIGHT_CONTENT = HEADER_TOP
			+ ".menu-utility-user:not([class*='hide']) .content-asset ";
	private static final String HEADER_RIGHT_USER_CONTENT = HEADER_TOP
			+ ".menu-utility-user:not([class*='hide']) ";
	private static final String HEADER_NAV_LEVEL1 = ".header-nav .menu-category.level-1>li>";
	private static final String HEADER_SUB_CATEGORY_l2 = ".hover~.level-2 .hide-mobile>ul>li>ul>li>a";
	private static final String HEADER_SUB_CATEGORY_l2_HAS_L3 = ".hover~.level-2 .hide-mobile ul>li>a~.level-3";
	private static final String HEADER_SUB_CATEGORY_l3 = "//following-sibling::ul[@class='level-3']/li/a";

	private static final String MOBILE_HEADER = "ul.menu-utility-user.hide-desktop ";
	private static final String MOBILE_HEADER_NAV = "div.header-nav ";
	private static final String MOBILE_UNDER_HAMBURGER_EXPANDED_MENU = MOBILE_HEADER_NAV
			+ MOBILE_HEADER;

	@FindBy(id = "header")
	WebElement headerPanel;

	@FindBy(css = "button.menu-toggle.sprite")
	WebElement lnkHamburger;

	@FindBy(css = ".menu-active")
	WebElement islnkHamburgerOpened;

	@FindBy(css = ".header-banner .header-promotion")
	WebElement headerPromotion;

	@FindBy(css = HEADER_TOP)
	WebElement headerTop;

	@FindBy(css = HEADER_LEFT_CONTENT)
	WebElement headerLeftContentAsset;

	@FindBy(css = HEADER_LEFT_CONTENT + "a[title='Gift Cards']")
	WebElement lnkGiftCards;

	@FindBy(css = HEADER_LEFT_CONTENT
			+ "a[title='Gift Cards']:not([href^='#'])")
	WebElement lnkGiftCardsNotWorking;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU
			+ "a[title='Gift Cards']")
	WebElement lnkGiftCardsMobileHambrg;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU
			+ "a[title='Gift Cards']:not([href^='#'])")
	WebElement lnkGiftCardsMobileHambrgNotWorking;

	@FindBy(css = HEADER_LEFT_CONTENT + "a[title='Belk Rewards Card']")
	WebElement lnkBelkRewardsCard;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU
			+ "a[title='Belk Rewards Card']")
	WebElement lnkBelkRewardsCardMobileHambrg;

	@FindBy(css = HEADER_RIGHT_CONTENT)
	WebElement headerRightContentAsset;

	@FindBy(css = HEADER_LEFT_CONTENT + "a[title='Coupons']")
	WebElement lnkCoupons;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU + "a[title='Coupons']")
	WebElement lnkCouponsMobileHambrg;

	@FindBy(css = HEADER_RIGHT_CONTENT + "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = ".header-nav a[href*='Wishlist-Show']")
	WebElement lnkWishListMobileSignInUser;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU + "a[title='Wish List']")
	WebElement lnkWishListMobileHambrg;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU + "a[title='Registry']")
	WebElement lnkRegistryMobileHambrg;

	@FindBy(css = HEADER_RIGHT_CONTENT + "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "ul[class='account-options'] a[href*='GiftRegistry-Start']")
	WebElement lnkRegistryMobileSignInUser;

	@FindBy(css = HEADER_RIGHT_USER_CONTENT + ".myaccount a")
	WebElement lnkMyAccount;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU
			+ "a[title*='My Account']")
	WebElement lnkMyAccountMobileHambrg;

	@FindBy(css = HEADER_RIGHT_USER_CONTENT + ".user-logout a")
	WebElement lnkLoginLogout;

	@FindBy(css = ".account-logout>a")
	public WebElement lnkLoginLogoutMobileSignInUser;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU + ".user-logout a")
	WebElement lnkLoginLogoutMobileHambrg;

	@FindBy(css = ".header-top-main")
	WebElement headerTopMain;

	@FindBy(css = ".primary-logo>a")
	public WebElement belkLogo;

	@FindBy(id = "mini-cart")
	WebElement miniCart;

	@FindBy(css = "#mini-cart i")
	WebElement iconMiniCart;

	@FindBy(css = "#mini-cart a.mini-cart-link")
	WebElement lnkViewBag;

	@FindBy(css = "#mini-cart a.mini-cart-link.mini-cart-empty")
	WebElement emptyCart;

	@FindBy(css = "#mini-cart .minicart-quantity")
	WebElement cartQuantity;

	@FindBy(css = ".header-search-and-store a")
	WebElement lnkFindInStore;

	@FindBy(css = ".header-search-and-store")
	WebElement fldSearchBox;

	@FindBy(css = MOBILE_UNDER_HAMBURGER_EXPANDED_MENU + "a.findstore")
	WebElement lnkFindInStoreMobileHambrg;

	@FindBy(css = ".header-search input#q")
	WebElement txtSearchBox;

	@FindBy(css = ".header-search button")
	WebElement btnSearch;

	@FindBy(css = ".header-top .header-promotion-msg")
	WebElement headerTopPromotionMsg;

	@FindBy(css = ".header-top-main .header-promotion-msg")
	WebElement headerTopPromotionMsgMobile;

	@FindBy(css = ".header-top-main button.menu-toggle")
	WebElement headerMenuToggle;

	@FindBy(css = ".header-nav")
	WebElement headerNav;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Women']")
	WebElement lnkWomens;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Men']")
	WebElement lnkMens;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Juniors']")
	WebElement lnkJuniors;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Kids & Baby']")
	WebElement lnkKidsAndBaby;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Beauty']")
	WebElement lnkBeauty;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Shoes']")
	WebElement lnkShoes;

	@FindBy(css = HEADER_NAV_LEVEL1
			+ "a[data-displaytext='Handbags & Accessories']")
	WebElement lnkHandbagsAndAccessories;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Jewelry']")
	WebElement lnkJewelry;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Bed & Bath']")
	WebElement lnkBedAndBath;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='For the Home']")
	WebElement lnkForTheHome;

	@FindBy(css = HEADER_NAV_LEVEL1 + "a[data-displaytext='Clearance']")
	WebElement lnkClearance;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Women']")
	WebElement lnkWomensMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Men']")
	WebElement lnkMensMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Juniors']")
	WebElement lnkJuniorsMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Kids & Baby']")
	WebElement lnkKidsAndBabyMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Beauty']")
	WebElement lnkBeautyMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Shoes']")
	WebElement lnkShoesMobile;

	@FindBy(css = MOBILE_HEADER_NAV
			+ "a[data-displaytext='Handbags & Accessories']")
	WebElement lnkHandbagsAndAccessoriesMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Jewelry']")
	WebElement lnkJewelryMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Bed & Bath']")
	WebElement lnkBedAndBathMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='For the Home']")
	WebElement lnkForTheHomeMobile;

	@FindBy(css = MOBILE_HEADER_NAV + "a[data-displaytext='Clearance']")
	WebElement lnkClearanceMobile;

	@FindBy(css = ".hover~.level-2 .html-slot-container img")
	WebElement fldContentSlot;

	@FindBy(css = ".header-banner-bottom .header-banner-promotion-bottom")
	WebElement headerBannerBottomPromotion;

	@FindBy(css = ".header-banner-bottom .hide-desktop .header-banner-promotion-bottom")
	WebElement headerBannerBottomPromotionMobile;

	@FindBy(css = ".header-banner-sitewidecopy")
	WebElement headerBannerSiteWideCopy;

	// will be removed
	@FindBy(css = "p.error-text")
	WebElement searchErrorMsg;

	@FindBy(css = "input[name='q']")
	WebElement txtSearch;

	@FindBy(css = ".mini-cart-content")
	WebElement minicartContent;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;

	@FindBy(css = "div [class='header-top hide-mobile full-width']>div>div>div>ul>li:not([class*='coupons'])")
	WebElement btnGiftCardforDesktop;

	@FindBy(css = "li:nth-child(4)>div>ul>li:nth-child(1)>a")
	WebElement btnGiftCardforMobile;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public Headers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, headerPanel))) {
			Log.fail("Header Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to level1 category item Ex: Mens The parameterized level1
	 * item will be clicked
	 * 
	 * @param level1
	 *            - L1 category item
	 * @throws Exception
	 */
	public void navigateTo(String level1) throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.clickOnElement(HEADER_NAV_LEVEL1
					+ "a[data-displaytext='" + level1 + "']", driver, "");
		} else if ((Utils.getRunPlatForm() == "mobile")) {
			clickMobileHamburgerMenu();
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1 + "']");
			// BrowserActions.clickOnElement(element, driver,
			// "Level 1 Header Menu");
			BrowserActions.javascriptClick(element, driver,
					"Level 1 Header Menu Toogle(+)");
		}
		Log.message(" -----> Clicked '" + level1 + "'");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to level2 category item Ex: Mens >> Men's Clothing
	 * Parameterized level1's level2 category will be clicked
	 * 
	 * @param level1
	 *            - L1 category item
	 * @param level2
	 *            - L2 category item
	 * @throws Exception
	 */
	public void navigateTo(String level1, String level2) throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			WebElement element = BrowserActions.checkLocator(driver,
					HEADER_NAV_LEVEL1 + "a[data-displaytext='" + level1 + "']");
			try {
				BrowserActions.mouseHover(driver, element);
				if (level2.equals("random")) {
					List<WebElement> level2Parent = element
							.findElement(By.xpath(".."))
							.findElements(
									By.cssSelector("div[class='level-2'] .hide-mobile ul>li>a"));
					int rand = ThreadLocalRandom.current().nextInt(1,
							level2Parent.size());
					WebElement randomLevel2 = level2Parent.get(rand - 1);
					Utils.waitForElement(driver, randomLevel2);
					level2 = BrowserActions.getText(driver, randomLevel2,
							"L2 category name");
					BrowserActions.clickOnElement(randomLevel2, driver,
							"Level 2 category");
					Log.message(" -----> Clicked random L2 category: '"
							+ level2 + "'");
				} else {
					WebElement level2Parent = BrowserActions
							.getMachingTextElementFromList(BrowserActions
									.checkLocators(driver,
											HEADER_SUB_CATEGORY_l2), level2,
									"equals");
					BrowserActions.clickOnElement(level2Parent, driver,
							"sub category(L2): " + level2);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		} else if ((Utils.getRunPlatForm() == "mobile")) {
			clickMobileHamburgerMenu();
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1
							+ "']/following-sibling::i");
			BrowserActions.javascriptClick(element, driver,
					"Level 1 Header Menu Toogle(+)");
			if (level2.equals("random")) {
				List<WebElement> level2Parent = driver
						.findElements(By
								.cssSelector("li[class='active'] div[class='level-2'] .hide-desktop>ul>li>a"));
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement randomLevel2 = level2Parent.get(rand - 1);
				Utils.waitForElement(driver, randomLevel2);
				level2 = BrowserActions.getText(driver, randomLevel2,
						"L2 category name");
				BrowserActions.javascriptClick(randomLevel2, driver,
						"Level 2 category");
				Log.message(" -----> Clicked random L2 category: '" + level2
						+ "'");
			} else {
				WebElement level2Parent = BrowserActions
						.getMachingTextElementFromList(
								BrowserActions
										.checkLocators(driver,
												"li[class='active'] div[class='level-2'] .hide-desktop>ul>li>a"),
								level2, "equals");
				// WebElement level2ParentExpand =
				// level2Parent.findElement(By.xpath("following-sibling"));
				BrowserActions.javascriptClick(level2Parent, driver,
						"Level 2 category");
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to level3 category item Ex: Men >> Men's Clothing >> Jeans
	 * 
	 * Parameterized level1 and level2 will be navigated then level3 will be
	 * clicked
	 * 
	 * @param level1
	 *            - L1 category item
	 * @param level2
	 *            - L2 category item
	 * @param level3
	 *            - L3 category item
	 * @throws Exception
	 */
	public void navigateTo(String level1, String level2, String level3)
			throws Exception {

		if (Utils.getRunPlatForm() == "desktop") {
			WebElement element = BrowserActions.checkLocator(driver,
					HEADER_NAV_LEVEL1 + "a[data-displaytext='" + level1 + "']");
			BrowserActions.mouseHover(driver, element);
			if (level3.equals("random")) {

				List<WebElement> level2Parent = BrowserActions.checkLocators(
						driver, HEADER_SUB_CATEGORY_l2_HAS_L3);
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement level2Text = level2Parent.get(rand - 1).findElement(
						By.xpath("../a"));
				level2 = BrowserActions.getText(driver, level2Text,
						"L2 category text");
				Log.message(" Clicked random L2 category: '" + level2 + "'");
				List<WebElement> level3Category = level2Text.findElement(
						By.xpath("..")).findElements(
						By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1,
							level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					// Utils.waitForElement(driver, randomLevel3);
					level3 = BrowserActions.getText(driver, randomLevel3,
							"L3 category text");
					BrowserActions.clickOnElement(randomLevel3, driver,
							"Level 3 category");

				} else {
					throw new Exception("There is no L3 category for L1 - "
							+ level1);
				}
				Log.message(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions
						.getMachingTextElementFromList(BrowserActions
								.checkLocators(driver, HEADER_SUB_CATEGORY_l2),
								level2, "equals");
				WebElement level3catagory = BrowserActions
						.getMachingTextElementFromList(
								level2Parent.findElements(By
										.xpath(HEADER_SUB_CATEGORY_l3)),
								level3, "equals");
				BrowserActions.clickOnElement(level3catagory, driver,
						"sub category(L3): " + level3);
			}
		} else if ((Utils.getRunPlatForm() == "mobile")) {
			clickMobileHamburgerMenu();
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1
							+ "']/following-sibling::i");
			// BrowserActions.clickOnElement(element, driver, "Level 1 Header
			// Menu");
			BrowserActions.javascriptClick(element, driver,
					"Level 1 Header Menu Toogle(+)");
			if (level3.equals("random")) {
				List<WebElement> level2Parent = element
						.findElement(By.xpath(".."))
						.findElements(
								By.cssSelector("div[class='level-2'] .hide-desktop ul>li>i"));
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());

				WebElement level2Text = level2Parent.get(rand - 1);
				level2 = BrowserActions.getText(driver, level2Text,
						"L2 category text");
				BrowserActions.javascriptClick(level2Text, driver,
						"Level 2 category");

				List<WebElement> level3Category = level2Text.findElement(
						By.xpath("..")).findElements(
						By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1,
							level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3,
							"L3 category text");
					BrowserActions.javascriptClick(randomLevel3, driver,
							"Level 3 category");
					// Log.message(" Clicked random L3 category: '" + level3 +
					// "'");
				} else {
					throw new Exception("There is no L3 category for L1 - "
							+ level1);
				}
				Log.message(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions
						.getMachingTextElementFromList(
								BrowserActions
										.checkLocators(driver,
												"li[class='active'] div[class='level-2'] .hide-desktop>ul>li"),
								level2, "equals");
				WebElement level2ParentExpand = level2Parent.findElement(By
						.cssSelector("i"));
				BrowserActions.javascriptClick(level2ParentExpand, driver,
						"Level 1 Header Menu");
				WebElement level3catagory = BrowserActions
						.getMachingTextElementFromList(
								level2Parent.findElements(By
										.xpath(HEADER_SUB_CATEGORY_l3)),
								level3, "equals");
				BrowserActions.javascriptClick(level3catagory, driver,
						"sub category(L3): " + level3);
			}
		}
		Utils.waitForPageLoad(driver);
	}

	public boolean verifyCategory1ContentSlot(String level1) {
		boolean dataToReturned = false;
		String category1 = HEADER_NAV_LEVEL1 + "a[data-displaytext='" + level1
				+ "']";

		WebElement element = BrowserActions.checkLocator(driver, category1);
		BrowserActions.mouseHover(driver, element);
		dataToReturned = Utils.waitForElement(driver, fldContentSlot);
		return dataToReturned;
	}

	public GiftCardsPage navigateToGiftCards() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkGiftCardsMobileHambrg, driver,
					"Header Gift Cards");
		} else
			BrowserActions.clickOnElement(lnkGiftCards, driver,
					"Header Gift Cards");

		return new GiftCardsPage(driver).get();
	}

	public BelkRewardCardPage navigateToBelkRewardsCard() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkBelkRewardsCardMobileHambrg,
					driver, "Belk Rewards Card");
		} else
			BrowserActions.clickOnElement(lnkBelkRewardsCard, driver,
					"Belk Rewards Card");

		return new BelkRewardCardPage(driver).get();
	}

	public CouponsPage navigateToCoupons() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkCouponsMobileHambrg, driver,
					"Coupons");
		} else
			BrowserActions.clickOnElement(lnkCoupons, driver, "Coupons");

		return new CouponsPage(driver).get();
	}

	public SignIn navigateToWishListAsGuest() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkWishListMobileHambrg, driver,
					"Wish List");
		} else
			BrowserActions.clickOnElement(lnkWishList, driver, "Wish List");

		return new SignIn(driver).get();
	}

	public WishListPage navigateToWishList() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkWishListMobileSignInUser, driver,
					"Wish List");
		} else {
			BrowserActions.clickOnElement(lnkWishList, driver, "Wish List");
		}

		return new WishListPage(driver).get();
	}

	public SignIn navigateToGiftRegistryAsGuest() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkRegistryMobileHambrg, driver,
					"Registry");
		} else
			BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");

		return new SignIn(driver).get();
	}

	public GiftRegistryPage navigateToGiftRegistry() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			// BrowserActions.clickOnElement(lnkRegistryMobileSignInUser,
			// driver, "Registry");
			BrowserActions.javascriptClick(lnkRegistryMobileHambrg, driver,
					"Mobile menu lamber 'Registry' Link");
		} else {
			BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
		}

		return new GiftRegistryPage(driver).get();
	}

	public SignIn navigateToSignIn() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkLoginLogoutMobileHambrg, driver,
					"SignIn");
		} else
			BrowserActions.clickOnElement(lnkLoginLogout, driver, "SignIn");

		return new SignIn(driver).get();
	}

	public void clickSignOut() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			navigateToMyAccount();
			BrowserActions.javascriptClick(lnkLoginLogoutMobileSignInUser,
					driver, "btn Sign Out");

		} else {
			navigateToMyAccount();
			BrowserActions.javascriptClick(lnkLoginLogoutMobileSignInUser,
					driver, "btn Sign Out");

		}
		Utils.waitForPageLoad(driver);

	}

	public MyAccountPage navigateToMyAccount() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkMyAccountMobileHambrg, driver,
					"SignIn");
		} else {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "SignIn");
		}

		return new MyAccountPage(driver).get();
	}

	public FindInStorePage navigateToFindInStore() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.clickOnElement(lnkFindInStoreMobileHambrg, driver,
					"Find In Store link");
		} else
			BrowserActions.clickOnElement(lnkFindInStore, driver,
					"Find In Store link");
		return new FindInStorePage(driver).get();
	}

	public HomePage navigateToHome() throws Exception {
		BrowserActions.javascriptClick(belkLogo, driver, "Belk's logo");
		Utils.waitForPageLoad(driver);
		// Dont add .get() while returning to HomePage
		return new HomePage(driver);
	}

	public boolean verifyGlobalNavigationL1Color() throws Exception {
		String normalRGBValue = null;
		boolean dataToReturned = true;
		List<WebElement> headers = null;

		if (Utils.getRunPlatForm() == "mobile") {
			normalRGBValue = "rgba(16, 114, 181, 1)";
			clickMobileHamburgerMenu();
			headers = Arrays.asList(lnkWomensMobile, lnkMensMobile,
					lnkJuniorsMobile, lnkKidsAndBabyMobile, lnkBeautyMobile,
					lnkShoesMobile, lnkHandbagsAndAccessoriesMobile,
					lnkJewelryMobile, lnkBedAndBathMobile, lnkForTheHomeMobile,
					lnkClearanceMobile);
		} else {
			normalRGBValue = "rgba(0, 0, 0, 1)";
			headers = Arrays.asList(lnkWomens, lnkMens, lnkJuniors,
					lnkKidsAndBaby, lnkBeauty, lnkShoes,
					lnkHandbagsAndAccessories, lnkJewelry, lnkBedAndBath,
					lnkForTheHome, lnkClearance);
		}

		for (int i = 0; i < headers.size(); i++) {
			if (i != (headers.size() - 1)) {
				dataToReturned &= Utils.verifyCssPropertyForElement(
						headers.get(i), "color", normalRGBValue);
			} else {
				dataToReturned &= Utils.verifyCssPropertyForElement(
						headers.get(i), "color", "rgba(204, 0, 0, 1)");
			}
		}
		return dataToReturned;
	}

	public boolean verifyL2CategoryColor(String level1) throws Exception {
		boolean dataToReturned = true;

		if (Utils.getRunPlatForm() == "desktop") {

			WebElement element = BrowserActions.checkLocator(driver,
					HEADER_NAV_LEVEL1 + "a[data-displaytext='" + level1 + "']");
			BrowserActions.mouseHover(driver, element);
			List<WebElement> subCategory = BrowserActions.checkLocators(driver,
					HEADER_SUB_CATEGORY_l2);
			for (WebElement e : subCategory) {
				dataToReturned &= Utils.verifyCssPropertyForElement(e, "color",
						"rgba(16, 114, 181, 1)");
			}
		} else if ((Utils.getRunPlatForm() == "mobile")) {
			clickMobileHamburgerMenu();
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1
							+ "']/following-sibling::i");
			// BrowserActions.clickOnElement(element, driver, "Level 1 Header
			// Menu");
			BrowserActions.javascriptClick(element, driver,
					"Level 1 Header Menu");
			BrowserActions.nap(1);
			dataToReturned &= Utils.verifyCssPropertyForElement(element,
					"color", "rgba(16, 114, 181, 1)");

			List<WebElement> subCategory = BrowserActions
					.checkLocators(driver,
							"li[class='active'] div[class='level-2'] .hide-desktop>ul>li>a");

			for (WebElement e : subCategory) {
				dataToReturned &= Utils.verifyCssPropertyForElement(e, "color",
						"rgba(0, 0, 0, 1)");
			}

			// WebElement level2Parent =
			// BrowserActions.getMachingTextElementFromList(BrowserActions.checkLocators(driver,
			// "li[class='active'] div[class='level-2']>ul>li>a"), level2,
			// "equals");
			// WebElement level2ParentExpand =
			// level2Parent.findElement(By.xpath("following-sibling::i"));
			// BrowserActions.clickOnElement(level2ParentExpand, driver, "Level
			// 1 Header Menu");
			//
			// WebElement level3catagory =
			// BrowserActions.getMachingTextElementFromList(level2Parent.findElements(By.xpath(HEADER_SUB_CATEGORY_l3)),
			// level3, "equals");
			// BrowserActions.clickOnElement(level3catagory, driver, "sub
			// category(L3): " + level3);

		}

		return dataToReturned;
	}

	/**
	 * To Click Hamburger Link for Mobile
	 * 
	 * @throws Exception
	 */

	public void clickMobileHamburgerMenu() throws Exception {

		BrowserActions.scrollToViewElement(lnkHamburger, driver);
		BrowserActions.javascriptClick(lnkHamburger, driver,
				"Mobile Hamburger Link");
		BrowserActions.nap(1);
		// BrowserActions.clickOnElement(lnkHamburger, driver, "Mobile Hamburger
		// Link");
		if (Utils.waitForElement(driver, islnkHamburgerOpened)) {
			Log.event("clicked Hamburger Menu");
		} else {
			throw new Exception("Hamburger Menu didn't open up");
		}
	}

	/**
	 * To get the L2 category name using L1
	 * 
	 * @param level1
	 * @throws Exception
	 */
	public List<String> getL2Category(String level1) throws Exception {
		List<String> dataToBeReturned = new ArrayList<String>();
		List<WebElement> l2category = null;
		if (Utils.getRunPlatForm() == "desktop") {
			WebElement element = BrowserActions.checkLocator(driver,
					HEADER_NAV_LEVEL1 + "a[data-displaytext='" + level1 + "']");
			BrowserActions.mouseHover(driver, element);
			l2category = BrowserActions.checkLocators(driver,
					HEADER_SUB_CATEGORY_l2);
			for (WebElement l2 : l2category) {
				dataToBeReturned.add(BrowserActions.getText(driver, l2,
						"L2 Category"));
			}
		} else if ((Utils.getRunPlatForm() == "mobile")) {
			clickMobileHamburgerMenu();
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1
							+ "']/following-sibling::i");
			BrowserActions.clickOnElement(element, driver,
					"Level 1 Header Menu");
			l2category = BrowserActions
					.checkLocators(driver,
							"li[class='active'] div[class='level-2'] .hide-desktop>ul>li>a");
			for (WebElement l2 : l2category) {
				dataToBeReturned.add(BrowserActions.getText(driver, l2,
						"L2 Category"));
			}
			closeMobileHamburgerMenu();
		}
		Log.event(level1 + "and its l2 category " + dataToBeReturned);
		return dataToBeReturned;

	}

	public void closeMobileHamburgerMenu() throws Exception {
		WebElement close = BrowserActions.checkLocator(driver,
				".header-nav .close_menu.hide-desktop>i");
		BrowserActions.javascriptClick(close, driver,
				"Close Mobile HamburgerMenu");
		Utils.waitForPageLoad(driver);
	}

	// public void clickMobileHamburgerMenu() throws Exception{
	// BrowserActions.scrollToViewElement(lnkHamburger, driver);
	// BrowserActions.javascriptClick(lnkHamburger, driver, "Mobile Hamburger
	// Link");
	// //BrowserActions.clickOnElement(lnkHamburger, driver, "Mobile Hamburger
	// Link");
	// Utils.waitForPageLoad(driver);
	// }

	/**
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 * 
	 *             Last Modified By : Dhanapal. K
	 * 
	 *             Last Modified Date : 11/1/2017
	 */
	public SearchResultPage searchProductKeyword(String textToSearch)
			throws Exception {
		final long startTime = StopWatch.startTime();
		if (textToSearch.startsWith("S_")) {
			textToSearch = textToSearch.split("S_")[1];
		}

		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Entering category in the search field");
		txtSearch.sendKeys(Keys.ENTER);
		// SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));

		return new SearchResultPage(driver).get();
	}

	/**
	 * 
	 * @param Upc
	 *            To Search
	 * @return
	 * @throws Exception
	 * 
	 *             Last Modified By : Dhanapal. K
	 * 
	 *             Last Modified Date : 11/1/2017
	 */
	public PdpPage searchAndNavigateToPDP(String textToSearch) throws Exception {
		final long startTime = StopWatch.startTime();
		if (textToSearch.startsWith("S_")) {
			textToSearch = textToSearch.split("S_")[1];
		}
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));

		return new PdpPage(driver).get();
	}

	// --------------------Header Menu-------------------------//
	/**
	 * To get the Level-3 Category Items as List of WebElements for particular
	 * Category
	 * 
	 * @param l1
	 *            - Index of Level-1 Category
	 * @param l2
	 *            - Index of Level-2 Category
	 * @param l3
	 *            - Index of Level-3 Category
	 * @return List of WebElements of Level-3 Category
	 * @throws Exception
	 */
	public List<WebElement> getL3CategoryListByIndex(int l1, int l2) {
		List<WebElement> lstl3Category = null;
		if(Utils.getRunPlatForm().equals("desktop")){
			lstl3Category = driver.findElements(By
					.cssSelector(".level-1>li:nth-child("+ l1 +")>.level-2>div.hide-mobile.desktopCategory>ul>li[style*='display: list-item']:nth-child("+ l2 +")>ul>li>ul>li>a"));
		}else{
			lstl3Category = driver.findElements(By
					.cssSelector(".level-1>li:nth-child("+ l1 +")>.level-2>div.hide-desktop>ul>li:nth-child("+ l2 +")>ul>li>a"));
		}
		System.out.println("------->>>> No.of L3 : " + lstl3Category.size());
		Log.message("     L3 Categories In Menu");
		for(int i = 1; i <= lstl3Category.size(); i++)
			Log.message("        "+ i + ". " + lstl3Category.get(i-1).getAttribute("innerHTML").trim());
		return lstl3Category;
	}

	/**
	 * To get the List of Menus' Text in Level-3 Category
	 * 
	 * @param l1
	 *            - Level-1 Category Index
	 * @param l2
	 *            - Level-2 Category Index
	 * @return List<String> - List of Menus' Text
	 * @throws Exception
	 */
	public List<String> getL3ListText(int l1, int l2) throws Exception {
		List<WebElement> lstl3Category = driver.findElements(By
				.cssSelector(".level-1>li:nth-child(" + l1
						+ ")>.level-2>ul>li:nth-child(" + l2 + ")>ul>li>a"));
		List<String> l3CategoryText = new ArrayList<String>();

		for (int i = 0; i < lstl3Category.size(); i++) {
			l3CategoryText.add(lstl3Category.get(i).getAttribute("innerHTML")
					.trim().replace("amp;", "").trim().toString());
		}
		return l3CategoryText;
	}

	/**
	 * To navigate to any category randomly based on level
	 * 
	 * @param level
	 *            - 'level-1' or 'level-2' or 'level-3'
	 * @return String Array of Categories and Sub Categories Navigated with
	 *         category index id String[0] - Name of Level-1 Category String[1]
	 *         - Name of Level-2 Category String[2] - Name of Level-3 Category
	 * 
	 *         String[3] - Index of Level-1 Category String[4] - Index of
	 *         Level-2 Category String[5] - Index of Level-3 Category
	 * @throws Exception
	 */
	public String[] navigateToRandomCategory(String level) throws Exception{
		String[] category = new String[6];
		if(level.equalsIgnoreCase("level-1")){
			if (Utils.getRunPlatForm() == "desktop") {
				int l1Random = (ThreadLocalRandom.current().nextInt(10 ) + 1);
				WebElement element = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+ l1Random +")>a");
				category[0] = element.getAttribute("innerHTML").trim().replace("amp;", "").split("<")[0];
				category[3] = Integer.toString(l1Random);
				Log.message("     Selected Level-1 Category : " + category[0]);
				BrowserActions.clickOnElement(element, driver, "level-1 Category");
			} else if ((Utils.getRunPlatForm() == "mobile")) {
				clickMobileHamburgerMenu();
				int l1Random = (ThreadLocalRandom.current().nextInt(10 ) + 1);
				WebElement element = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+ l1Random +")>a");
				category[0] = element.getAttribute("innerHTML").trim().replace("amp;", "").split("<")[0];
				category[3] = Integer.toString(l1Random);
				Log.message("     Selected Level-1 Category : " + category[0]);
				BrowserActions.clickOnElement(element, driver, "level-1 Category");
			}

		}else if(level.equalsIgnoreCase("level-2")){
			if (Utils.getRunPlatForm() == "desktop") {
				int l1Random = ThreadLocalRandom.current().nextInt(10) + 1;
				WebElement level1 = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a");
				category[0] = level1.getText();
				category[3] = Integer.toString(l1Random);
				BrowserActions.mouseHover(driver, level1);
				int l2Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+ l1Random +") div.level-2>div>ul>li>a")).size();
				int l2Random = ThreadLocalRandom.current().nextInt(l2Count) + 1;

				WebElement level2Parent = BrowserActions.checkLocator(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.desktopCategory>ul>li:nth-child("+l2Random+")>ul>li>a");				
				category[1] = level2Parent.getAttribute("innerHTML").trim().replace("amp;", "").split("<")[0];
				Log.message("     Selected Level-1 Category : " + category[0], driver);
				Log.message("     Selected Level-2 Category : " + category[1]);
				category[4] = Integer.toString(l2Random);
				System.out.println("------->>>> L1 cat.no :: " + category[3]);
				System.out.println("------->>>> L2 cat.no :: " + category[4]);
				BrowserActions.clickOnElement(level2Parent, driver, "level-2 Category");
			} else if ((Utils.getRunPlatForm() == "mobile")) {
				clickMobileHamburgerMenu();
				int l1Random = ThreadLocalRandom.current().nextInt(10) + 1;
				WebElement level1 = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a+i");
				category[0] = BrowserActions.getText(driver, BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a"), "Category Name");
				category[3] = Integer.toString(l1Random);
				BrowserActions.clickOnElement(level1, driver, "level 1");
				int l2Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+ l1Random +") div.level-2>div>ul>li>a")).size();
				int l2Random = ThreadLocalRandom.current().nextInt(l2Count) + 1;

				WebElement level2Parent = BrowserActions.checkLocator(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.hide-desktop>ul>li:nth-child("+l2Random+")>a");				
				category[1] = level2Parent.getAttribute("innerHTML").trim().replace("amp;", "").split("<")[0];
				Log.message("     Selected Level-1 Category : " + category[0], driver);
				Log.message("     Selected Level-2 Category : " + category[1]);
				category[4] = Integer.toString(l2Random);
				System.out.println("------->>>> L1 cat.no :: " + category[3]);
				System.out.println("------->>>> L2 cat.no :: " + category[4]);
				BrowserActions.clickOnElement(level2Parent, driver, "level-2 Category");
			}
		}else if(level.equalsIgnoreCase("level-3")){
			if (Utils.getRunPlatForm() == "desktop") {
				int l1Random = ThreadLocalRandom.current().nextInt(10) + 1;
				WebElement level1 = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a");
				category[0] = level1.getText();
				category[3] = Integer.toString(l1Random);
				BrowserActions.mouseHover(driver, level1);
				int l3Count = 0; 
				int l3Random = 0;
				int l2Random = 0;
				int l2Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+ l1Random +") div.level-2>div>ul>li>a")).size();
				do {
					l2Random = ThreadLocalRandom.current().nextInt(l2Count) + 1;

					WebElement level2Parent = BrowserActions.checkLocator(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.desktopCategory>ul>li:nth-child("+l2Random+")>ul>li>a");				
					category[1] = level2Parent.getAttribute("innerHTML").trim().replace("amp;", "").split("<")[0];
					try {
						l3Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+l1Random+") div.level-2>div.desktopCategory>ul>li:nth-child("+l2Random+")>ul>li>ul>li")).size();
						l3Random = ThreadLocalRandom.current().nextInt(l3Count) + 1;
					}catch (Exception e) {
						// TODO: handle exception
					}
				}while(l3Count==0);

				List<WebElement> level3 = BrowserActions.checkLocators(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.desktopCategory>ul>li:nth-child("+l2Random+")>ul>li>ul>li>a");
				category[2] = level3.get(l3Random-1).getAttribute("innerHTML").trim().replace("amp;", "");
				category[5] = Integer.toString(l2Random);
				BrowserActions.clickOnElement(level3.get(l3Random-1), driver, "level-3 Category");
				Log.message("     Selected Level-1 Category : " + category[0], driver);
				Log.message("     Selected Level-2 Category : " + category[1]);
				Log.message("     Selected Level-3 Category : " + category[2]);
			} else if ((Utils.getRunPlatForm() == "mobile")) {
				clickMobileHamburgerMenu();
				int l1Random = ThreadLocalRandom.current().nextInt(10) + 1;
				WebElement level1 = BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a+i");
				category[0] = BrowserActions.getText(driver, BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+")>a"), "Category Name");
				category[3] = Integer.toString(l1Random);
				BrowserActions.clickOnElement(level1, driver, "level 1");
				int l3Count = 0; 
				int l3Random = 0;
				int l2Random = 0;
				int l2Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+ l1Random +") div.level-2>div>ul>li>a")).size();
				do {
					l2Random = ThreadLocalRandom.current().nextInt(l2Count) + 1;
					try {
						WebElement level2Parent = BrowserActions.checkLocator(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.hide-desktop>ul>li:nth-child("+l2Random+")>a+i");				
						category[1] = BrowserActions.getText(driver, BrowserActions.checkLocator(driver,".level-1>li:nth-child("+l1Random+") div.level-2>div.hide-desktop>ul>li:nth-child("+l2Random+")>a"), "Category Name");
						l3Count = driver.findElements(By.cssSelector(".level-1>li:nth-child("+l1Random+") div.level-2>div.hide-desktop>ul>li>ul.level-3")).size();
						l3Random = ThreadLocalRandom.current().nextInt(l3Count) + 1;
						BrowserActions.clickOnElement(level2Parent, driver, "level-3 Category");
					}catch (Exception e) {
						// TODO: handle exception
					}
				}while(l3Count==0);
				List<WebElement> level3 = BrowserActions.checkLocators(driver, ".level-1>li:nth-child("+l1Random+") div.level-2>div.hide-desktop>ul>li:nth-child("+l2Random+")>ul.level-3>li>a");
				category[2] = level3.get(l3Random-1).getAttribute("innerHTML").trim().replace("amp;", "");
				category[5] = Integer.toString(l2Random);
				BrowserActions.clickOnElement(level3.get(l3Random-1), driver, "level-3 Category");
				Log.message("     Selected Level-1 Category : " + category[0], driver);
				Log.message("     Selected Level-2 Category : " + category[1]);
				Log.message("     Selected Level-3 Category : " + category[2]);
			}
		}
		Utils.waitForPageLoad(driver);
		return category;
	}

	/**
	 * To get the quantity from the bag
	 * 
	 * @return String -cartQuantity
	 */
	public String getQuantityFromBag() throws Exception {
		return BrowserActions.getText(driver, cartQuantity, "Bag quantity");
	}

	public ShoppingBagPage NavigateToBagPage() throws Exception {
		if (Utils.waitForElement(driver, minicartContent))
			if (minicartContent.getAttribute("style").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0) {
				BrowserActions.scrollToView(btnViewBag, driver);
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
			}
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To mouse hover to mini cart
	 */
	public void mouseOverMiniCart() {
		BrowserActions.mouseHover(driver, miniCart);
	}

	public PdpPage clikOnGiftCardButton() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			Utils.waitForElement(driver, btnGiftCardforMobile);
			BrowserActions.scrollToViewElement(btnGiftCardforMobile, driver);
			BrowserActions.clickOnElement(btnGiftCardforMobile, driver,
					"SignIn");
		} else {
			BrowserActions.clickOnElement(btnGiftCardforDesktop, driver,
					" Gift Card link");
		}

		return new PdpPage(driver).get();
	}

	public RegistrySignedUserPage navigateToASingedUserRegistry()
			throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			// BrowserActions.clickOnElement(lnkRegistryMobileSignInUser,
			// driver, "Registry");
			BrowserActions.javascriptClick(lnkRegistryMobileHambrg, driver,
					"Mobile menu lamber 'Registry' Link");
		} else {
			BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
		}

		return new RegistrySignedUserPage(driver).get();
	}

	/**
	 * To Click on Signout link From Registry page
	 * 
	 * @return RegistryGuestUserPage
	 * @throws Exception
	 */

	public RegistryGuestUserPage clickSignOutFromRegistry() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();

			BrowserActions.clickOnElement(lnkLoginLogoutMobileSignInUser,
					driver, "Sign Out");

		} else {
			BrowserActions.clickOnElement(lnkLoginLogout, driver, "Sign Out");

		}
		Utils.waitForPageLoad(driver);
		return new RegistryGuestUserPage(driver).get();
	}

	public ClearancePage navigateToClearancePage() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			BrowserActions.javascriptClick(lnkClearanceMobile, driver,
					"Clearance Link in Mobile");
		} else {
			BrowserActions.clickOnElement(lnkClearance, driver,
					"Clearance Link in Desktop");
		}
		return new ClearancePage(driver).get();
	}

}
