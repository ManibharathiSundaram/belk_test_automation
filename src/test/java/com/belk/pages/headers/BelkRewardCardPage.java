package com.belk.pages.headers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.Log;
import com.belk.support.Utils;

public class BelkRewardCardPage extends LoadableComponent<BelkRewardCardPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public static final String title = "Belk Rewards Card";

	//.gr__belkcredit_com,.gr__belk_mycreditcard_mobi
	@FindBy(css = "input[name='userId']")
	WebElement userID;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException
	 */
	public BelkRewardCardPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		// Utils.waitForPageLoad(driver);
		if (isPageLoaded && !(Utils.waitForElement(driver, userID))) {
			Log.fail("Belk Rewards Card page not open up.", driver);
		}

	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
		try {
			driver = Utils.switchWindows(driver, title, "title", "false");
		} catch (Exception e) {
			Log.event("Exception in switch to windows");
		}
	}

}
