package com.belk.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.sf.ehcache.util.FindBugsSuppressWarnings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class ClearancePage extends LoadableComponent<ClearancePage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Clearance Page **************************
	 **********************************************************************************************/

	@FindBy(css = ".clearance-value")
	WebElement lnkrefinement;
	@FindBy(css = ".breadcrumb.hide-mobile")
	WebElement lnkbreadcrumb;
	@FindBy(css = ".breadcrumb-element")
	List<WebElement> lstTxtBreadcrumb;
	@FindBy(css = "a>span[class='hide-desktop']")
	WebElement txtProductInBreadcrumbMobile;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public ClearancePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, lnkrefinement))) {
			Log.fail("Clearance page did not open up.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	public String getBreadcrumbtxt() throws Exception {
		String txtbreadcrumb = BrowserActions.getText(driver, lnkbreadcrumb,
				"breadcrumb Text");
		return txtbreadcrumb;

	}

	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtBreadcrumb.get(0), driver);
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}

		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

}// ClearancePage

