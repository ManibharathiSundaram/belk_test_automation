package com.belk.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.footers.socialmediapages.FacebookPage;
import com.belk.pages.footers.socialmediapages.GooglePlusPage;
import com.belk.pages.footers.socialmediapages.PintrestPage;
import com.belk.pages.footers.socialmediapages.TwitterPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

import io.netty.util.internal.ThreadLocalRandom;

public class QuickViewPage extends LoadableComponent<QuickViewPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public MiniCartPage minicart;

	/**********************************************************************************************
	 ********************************* WebElements of Quick view Page ***********************************
	 **********************************************************************************************/

	private static final String SOCIAL_MEDIA = "div[class='socialsharing']";
	private static final String DEFAULT_SELECTED_VALUE_DROPDOWN = "div[class='value'] div[class*='selected-option']";
	private static final String LIST_OF_OPTIONS_IN_DROPDOWN = "div[class='custom-select current_item'] ul li:not([class*='hide'])";
	private static final String SELECTED_QTY = ".quantity .selected-option";
	private static final String LIST_OF_COLOR = "ul.swatches.color>li:not([class*='selected'])";
	private static final String SELECTED_COLOR = "ul[class='swatches color']>li.selected>a>img";

	@FindBy(css = ".product-brand a")
	WebElement lnkBrandName;

	@FindBy(css = "div[id='pdpMain']")
	WebElement quickViewContent;

	@FindBy(css = ".swatches.color li")
	List<WebElement> lstColor;

	@FindBy(css = ".color .label span")
	WebElement lblColorSelect;

	@FindBy(css = "a[class='jcarousel-nav jcarousel-next']")
	WebElement btnNextImage;

	@FindBy(css = "a[class='jcarousel-nav jcarousel-prev']")
	WebElement btnPreviousImage;

	@FindBy(css = ".thumbnail-link")
	List<WebElement> lstThumbnailImages;

	@FindBy(css = ".button.simple.gift-registry-unregistered")
	WebElement btnRegistryAsGuest;

	@FindBy(css = "a[data-action='gift-registry']")
	WebElement btnRegistrySigneduser;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-pinternet']")
	WebElement lnkPintrest;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-facebook']")
	WebElement lnkFacebook;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-twitter']")
	WebElement lnkTwitter;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-googleplus']")
	WebElement lnkGooglePlus;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-email']")
	WebElement lnkEmail;

	@FindBy(css = ".product-brand>a:not([class^='#']) ")
	WebElement productBrand;

	@FindBy(css = "button[id='add-to-cart']")
	public WebElement btnAddtoBag;

	@FindBy(css = ".loader[style*='block']")
	WebElement PDPspinner;

	@FindBy(css = "div.success")
	WebElement lblsuccessMessage;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQuantity;

	@FindBy(css = "button[class*='ui-dialog-titlebar-close']")
	WebElement btnCloseQV;

	@FindBy(css = "a[data-action='wishlist']")
	WebElement btnAddToWishList;

	@FindBy(css = ".size ul[class='selection-list'] li:not([class*='hide'])")
	List<WebElement> lstSize;

	

	@FindBy(css = ".loader[style*='block']")
	WebElement spinner;

	@FindBy(css = ".product-UPC")
	WebElement lblUPC;

	@FindBy(css = ".stock-product")
	WebElement msgLimitedAvalilability;

	@FindBy(css = ".max_buy_qty_message")
	WebElement qtyLimitedAvalilability;

	@FindBy(css = ".primary-image")
	WebElement primaryImage;

	@FindBy(css = ".jcarousel>ul")
	WebElement productThumbnails;

	@FindBy(css = ".jcarousel>ul>li:not([class *= 'selected'])")
	List<WebElement> productThumbnailsSelectable;

	@FindBy(css = "h1.product-name")
	WebElement fldProductName;

	@FindBy(css = "ul[class='swatches color']>li.selected>a>img")
	WebElement defaultColor;

	@FindBy(css = "ul[class='swatches color']>li>a>img")
	List<WebElement> lstColorSwatches;

	@FindBy(css = "div[class='login-box-content returning-customers clearfix']>p")
	WebElement txtWishlistLogin;

	@FindBy(css = ".primary-image")
	WebElement productPrimaryImage;

	@FindBy(css = "a[class='button productset-btn']")
	WebElement btnShopEntireProductSet;

	@FindBy(css = ".standardprice")
	WebElement lblStdPrice;

	@FindBy(css = ".price-standard")
	WebElement lblOrgPrice;

	@FindBy(css = ".price-sales")
	WebElement lblNowPrice;

	@FindBy(css = ".primary-image")
	WebElement imgPrimaryImage;

	@FindBy(css = "div[class='inventory-error-msg success']")
	WebElement txtAddToWishlistSuccessMessage;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(css = "button[title='Close']>span:nth-of-type(1)")
	WebElement lnkClose;

	@FindBy(css = "div[class*='filterby-refinement']>a")
	WebElement btnFilterByMobile;

	@FindBy(css = ".item-name")
	List<WebElement> lstItemName;

	@FindBy(css = "div[class='product-badge']")
	WebElement btnProductBadge;
	
	@FindBy(css = "#product-content>h3>span")
	WebElement UPCvalue;

	@FindBy(css = ".brand-name")
	WebElement productName;

	@FindBy(css = "div[class='label']>span")
	WebElement productcolor;

	@FindBy(css = "div[class='value'] div[class*='selected-option']")
	WebElement productSize;

	@FindBy(css = "[name='Quantity']>[selected='selected']")
	WebElement productQty;

	@FindBy(css = "div[class='value'] div[class*='selected-option']")
	WebElement drpSize;

	@FindBy(css = "#add-to-cart")
	WebElement btnUpdateShoppingCart;

	@FindBy(css = SELECTED_QTY)
	WebElement txtSelectedQuantity;

	@FindBy(css = ".max_buy_qty_message+div[class='stock-product']")
	WebElement txtLimitedStock;

	@FindBy(css = ".product-actions > a:nth-child(2)")
	WebElement lnkAddToWishList;

	@FindBy(css = ".product-brand")
	WebElement lnkBrandNameProductSet;

	@FindBy(css = "div[class='product-primary-image']")
	WebElement imageMainQuickviewWindow;

	@FindBy(css = "div[class='product-badge']")
	WebElement txtProductBadgeQuickView;

	@FindBy(css = "div[class*='product-set'] .product-badge")
	WebElement txtProductBadgeQuickViewInProductSet;

	@FindBy(css = ".productthumbnail")
	WebElement imgThumbnailImageQV;

	@FindBy(css = "div[id='thumbnails'] li[class='thumb selected'] a[class='thumbnail-link']")
	WebElement imgClickThumbnailQV;

	@FindBy(css = "div[id='check-gc-balance']>a:not([href^='#'])")
	WebElement giftBalance;

	@FindBy(css = "[class='product-name'][itemprop='name']")
	WebElement lblProductSetProductName;

	@FindBy(css = "#product-set-list .product-set-item")
	List<WebElement> lstProductSet;

	@FindBy(css = ".product-UPC")
	WebElement UPC;

	@FindBy(css = "div[class='value'] div[class='selected-option']")
	WebElement ValueDropDown;

	@FindBy(css = ".quantity .selected-option")
	WebElement SelectQty;

	@FindBy(css = ".shipping-promotion")
	WebElement shippingThresholdMessage;

	@FindBy(css = DEFAULT_SELECTED_VALUE_DROPDOWN)
	List<WebElement> sizeDefaultValue;

	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart")
	List<WebElement> btnAddToShoppingBag;

	@FindBy(css = "#QuickViewDialog")
	WebElement quickViewPopUp;

	@FindBy(css = ".mini-cart-diffPay.success")
	WebElement SucessMsgForAddToBag;

	@FindBy(css = "h1.product-name")
	WebElement lblProductName;

	@FindBy(css = "div.inventory > div.message-qv")
	WebElement lblOnlineonlyMessage;

	@FindBy(css = "div.shipping-promotion")
	WebElement txtFreeshippingmessage;

	@FindBy(css = "div[class='product-description']")
	WebElement lblProductDescription;

	@FindBy(css = "[class*='inventory-error-msg']")
	WebElement lblsuccessMessageWishList;

	@FindBy(css = "[class*='pdpgftmessage']")
	WebElement lblsuccessMessageGiftcard;
	
	@FindBy(css=".product-price")
	WebElement lblProductPrice;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public QuickViewPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, quickViewContent))) {
			Log.fail("QuickView Page did not open up. Site might be down.",
					driver);
		}

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(1);
		Utils.waitUntilElementDisappear(driver, PDPspinner);
	}

	/**
	 * To click Brand link
	 * 
	 * @return SearchResultPage
	 */
	public SearchResultPage clickBrandLink() throws Exception {
		BrowserActions.clickOnElement(lnkBrandName, driver, "Brand link");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To get Brand name
	 * 
	 * @return String
	 */
	public String getBrandName() throws Exception {
		return BrowserActions.getText(driver, lnkBrandName, "Brand name");
	}

	/**
	 * To mouse hover and verify the color is highlighted
	 * 
	 */
	public boolean verifyColorMouseHover() throws Exception {
		boolean status = false;
		Actions action = new Actions(driver);
		action.moveToElement(lstColor.get(0).findElement(By.cssSelector("a")))
				.build().perform();
		if (lstColor.get(0).getCssValue("color").equals("rgba(0, 0, 0, 1)")) {
			status = true;
		}
		return status;
	}

	/**
	 * To verify the color selected and the text above the color swatches are
	 * matched
	 * 
	 * @throws Exception
	 */
	public boolean verifySelectedColorIsDisplayed() throws Exception {
		boolean status = false;
		for (WebElement element : lstColor) {
			if (element.getAttribute("class").contains("selected")) {
				if (lblColorSelect.getText().equals(
						element.findElement(By.cssSelector("img"))
								.getAttribute("alt"))) {
					status = true;
					break;
				}
			}
		}
		return status;
	}

	/**
	 * To select color based on index
	 * 
	 * @param index
	 * @throws Exception
	 */
	public void selectColorByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(lstColor.get(index - 1), driver, "Color");
	}

	/**
	 * To click on image Next and Previous button in Quick view popup
	 * 
	 * @param navType
	 *            - 'Next' or 'Previous'
	 */
	public void clickImageNavArrowButton(String navType) throws Exception {
		WebElement element = null;
		if (navType.equals("Next")) {
			element = btnNextImage;
		} else if (navType.equals("Previous")) {
			element = btnPreviousImage;
		}
		BrowserActions.clickOnElement(element, driver, "Image nav button");
		try {
			Utils.waitUntilElementDisappear(driver, lstThumbnailImages.get(0),
					5);
		} catch (Exception e) {
			Log.event("Waiting after clicking");
		}
	}

	/**
	 * To get the count of thumbnail images
	 * 
	 * @return int
	 */
	public int getCountOfThumbnailImages() throws Exception {
		return lstThumbnailImages.size();
	}

	/**
	 * To select the size from size drop down
	 * 
	 * @param
	 * @return selectedSize
	 * @throws Exception
	 */
	public String selectSize(String... size) throws Exception {
		String selectedSize = null;

		// checking size drop down have one value or more that one
		WebElement sizeDefaultValue = BrowserActions.checkLocator(driver,
				DEFAULT_SELECTED_VALUE_DROPDOWN);
		if (!BrowserActions
				.getText(driver, sizeDefaultValue,
						"Size drop down selected option").trim().toLowerCase()
				.contains("select")) {
			return BrowserActions.getText(driver, sizeDefaultValue,
					"Size drop down selected option");
		}
		// clicking size drop down
		BrowserActions.clickOnElement(sizeDefaultValue, driver,
				"Clicked Size Button");
		BrowserActions.nap(1);

		List<WebElement> listOfSize = BrowserActions.checkLocators(driver,
				LIST_OF_OPTIONS_IN_DROPDOWN);

		if (size.length != 0) {
			WebElement sizeoption = BrowserActions
					.getMachingTextElementFromList(listOfSize, size.toString(),
							"equals");
			BrowserActions.clickOnElement(sizeoption, driver,
					"Size drop down options");
			waitForSpinner();
			selectedSize = BrowserActions.getText(driver, sizeDefaultValue,
					"Quantity drop down selected option");
		} else if (listOfSize.size() >= 0) {
			int rand = ThreadLocalRandom.current()
					.nextInt(0, listOfSize.size());
			BrowserActions.clickOnElement(listOfSize.get(rand), driver,
					"Select size option");
			waitForSpinner();

			selectedSize = driver
					.findElement(
							By.cssSelector("div[class='value'] div[class*='selected-option']"))
					.getText(); // In future i ill update the POM
		} else {
			throw new Exception("Size drop down didn't have no values");
		}
		Utils.waitForPageLoad(driver);
		return selectedSize;
	}

	/**
	 * Select Quantity from quantity drop down
	 * 
	 * @param quantity
	 *            - optional parameter value, will click parameterized value in
	 *            the drop down else random value
	 * @return selectedQuantity
	 * @throws Exception
	 */
	public String selectQuantity(String... quantity) throws Exception {
		String selectedQuantity = null;

		// checking size drop down have one value or more that one
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver,
				SELECTED_QTY);
		if (!BrowserActions
				.getText(driver, qtyDefaultValue,
						"Quantity drop down selected option").trim()
				.toLowerCase().contains("1")) {
			return BrowserActions.getText(driver, qtyDefaultValue,
					"Quantity drop down selected option");
		}

		BrowserActions.clickOnElement(qtyDefaultValue, driver,
				"Quantity drop down");
		BrowserActions.nap(2);
		// List<WebElement> listOfQuantity =
		// BrowserActions.checkLocators(driver, LIST_OF_OPTIONS_IN_DROPDOWN);
		List<WebElement> listOfQuantity = driver
				.findElements(By
						.cssSelector("div[class='custom-select current_item'] ul li:not([class*='hide'])")); // In
																												// Future
																												// i
																												// will
																												// update
																												// the
																												// code

		if (quantity.length > 0) {
			String optionValue = quantity[0].toString();
			WebElement qtyoption = BrowserActions
					.getMachingTextElementFromList(listOfQuantity, optionValue,
							"equals");
			BrowserActions.clickOnElement(qtyoption, driver, "");
			waitForSpinner();
			selectedQuantity = driver.findElement(By.cssSelector(SELECTED_QTY))
					.getText();
			// selectedQuantity = BrowserActions.getText(driver,
			// qtyDefaultValue, "Quantity drop down selected option");
		} else if (listOfQuantity.size() > 0) {
			int rand = Utils.getRandom(0, listOfQuantity.size());
			BrowserActions.clickOnElement(listOfQuantity.get(rand), driver,
					"Select quantity option");
			waitForSpinner();
			selectedQuantity = driver.findElement(By.cssSelector(SELECTED_QTY))
					.getText();
			// selectedQuantity = BrowserActions.getText(driver,
			// qtyDefaultValue,"Quantity drop down selected option");
		}

		return selectedQuantity;
	}

	/**
	 * To select the color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public String selectColor() throws Exception {
		String selectedColorName = null;

		List<WebElement> listOfColor = driver.findElements(By
				.cssSelector(LIST_OF_COLOR));

		if (listOfColor.size() == 0) {
			WebElement defaultColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			selectedColorName = BrowserActions.getTextFromAttribute(driver,
					defaultColor, "alt", "Alt attribute value");

		} else if (listOfColor.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0,
					listOfColor.size());
			BrowserActions.clickOnElement(listOfColor.get(rand), driver,
					"select color");
			BrowserActions.nap(10);
			WebElement selectedColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			selectedColorName = BrowserActions.getTextFromAttribute(driver,
					selectedColor, "alt", "Alt attribute value");
		} else {
			throw new Exception("Color swatch didn't have no values");
		}
		Utils.waitForPageLoad(driver);
		return selectedColorName;
	}

	/**
	 * To navigating to pintrest page after clicking pintrest
	 * 
	 * @return PintrestPage
	 * @throws Exception
	 */
	public PintrestPage naviagteToPintrest() throws Exception {
		BrowserActions.clickOnElement(lnkPintrest, driver, "Link to pintrest");
		Utils.waitForPageLoad(driver);
		return new PintrestPage(driver).get();
	}

	/**
	 * To navigating to sign in page after clicking add to registry button
	 * 
	 * @return RegistryGuestUserPage
	 * @throws Exception
	 */
	public RegistryGuestUserPage navigateToRegistryAsGuest() throws Exception {
		Utils.waitForElement(driver, btnRegistryAsGuest);
		BrowserActions.clickOnElement(btnRegistryAsGuest, driver,
				"click on registry button as a guest");
		return new RegistryGuestUserPage(driver).get();
	}

	public void addProductToBag() throws Exception {
		selectSize();
		selectColor();
		clickAddToBag();
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click the add to bag button
	 * 
	 * @throws Exception
	 */
	public void clickAddToBag() throws Exception {
		BrowserActions.clickOnElement(btnAddtoBag, driver, "Add to bag button");
	}

	/**
	 * To navigating to registry signed user page after clicking add to registry
	 * button
	 * 
	 * @return RegistrySignedUserPage
	 * @throws Exception
	 */
	public RegistrySignedUserPage navigateToRegistryAsSignedUser()
			throws Exception {
		Utils.waitForElement(driver, btnRegistrySigneduser);
		clickOnRegistrySign();
		return new RegistrySignedUserPage(driver).get();
	}

	public void clickOnRegistrySign() throws Exception {
		Utils.waitForElement(driver, btnRegistrySigneduser);
		BrowserActions.clickOnElement(btnRegistrySigneduser, driver,
				"click on registry button as a signed user");

	}

	public String getTextFromAddedRegistry() throws Exception {
		String txtAddedRegistry = BrowserActions.getText(driver,
				lblsuccessMessage, "added message");
		return txtAddedRegistry;
	}

	public boolean verifyElementType(String actualElement) {
		Utils.waitForPageLoad(driver);
		return actualElement.contains(drpQuantity.getTagName());
	}

	public SearchResultPage closeQuickViewPageByEsc() throws Exception {
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ESCAPE);
		r.keyRelease(KeyEvent.VK_ESCAPE);
		Utils.waitForPageLoad(driver);
		return new SearchResultPage(driver);
	}

	public SearchResultPage closeQuickViewPage() throws Exception {
		BrowserActions.clickOnElement(btnCloseQV, driver,
				"Quick View Close Button");
		Utils.waitForPageLoad(driver);
		return new SearchResultPage(driver);
	}

	public void clickAddToWishList() throws Exception {
		BrowserActions.clickOnElement(btnAddToWishList, driver,
				"Clickcing on Add to Wish List Button");
		Utils.waitForPageLoad(driver);
	}

	public SignIn addToWishlist() throws Exception {
		BrowserActions.clickOnElement(btnAddToWishList, driver,
				"Clickcing on Add to Wish List Button");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}

	/**
	 * To click Size dropdown
	 * 
	 * @throws Exception
	 */
	public void clickSizeDropown() throws Exception {
		BrowserActions.clickOnElement(drpSize, driver, "Size dropdown");
	}


	public String selectRandomThumbnail() throws Exception {
		int rand = Utils.getRandom(0, productThumbnailsSelectable.size());
		WebElement thumbnail = productThumbnailsSelectable.get(rand)
				.findElement(By.cssSelector(".productthumbnail"));
		while(thumbnail.isDisplayed() == false)
			BrowserActions.clickOnElement(btnNextImage, driver, "J scroll");
		BrowserActions.javascriptClick(thumbnail, driver, "Thumbnail");
		Utils.waitForPageLoad(driver);
		return thumbnail.getAttribute("src").trim().split("&layer")[0];
	}

	public String getPrimaryImageSrc() {
		return primaryImage.getAttribute("src").trim().split("&layer")[0];
	}

	public String getLimitedAvailabilityMessage() throws Exception {
		String txtToreturn = "";

		if (Utils.waitForElement(driver, msgLimitedAvalilability)) {
			if (msgLimitedAvalilability.getText().contains("Only")) {
				txtToreturn = BrowserActions.getText(driver,
						msgLimitedAvalilability,
						"Err Message of Limited Availability").trim();
			}
		}
		if (Utils.waitForElement(driver, qtyLimitedAvalilability)) {
			txtToreturn = BrowserActions.getText(driver,
					qtyLimitedAvalilability,
					"Err Message of Limited Availability").trim();
		}

		return txtToreturn;
	}

	/**
	 * To select the color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public String selectColor1() throws Exception {
		QuickViewPage quickViewPage = new QuickViewPage(driver);
		String selectedColorName = null;
		List<String> defaultColor = Arrays.asList("defaultColor");
		if (elementLayer.verifyPageElements(defaultColor, quickViewPage)) {
			WebElement colorDefaultValue = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			if (!BrowserActions
					.getText(driver, colorDefaultValue,
							"Quantity drop down selected option").trim()
					.toLowerCase().contains("selected")) {
				selectedColorName = BrowserActions.getTextFromAttribute(driver,
						colorDefaultValue, "alt", "Alt attribute value");
				return BrowserActions.getText(driver, colorDefaultValue,
						"Quantity drop down selected option");
			}
		}
		List<WebElement> listOfColor = driver.findElements(By
				.cssSelector(LIST_OF_COLOR));
		if (listOfColor.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0,
					listOfColor.size());
			BrowserActions.clickOnElement(listOfColor.get(rand), driver,
					"select color");
			WebElement selectedColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			selectedColorName = BrowserActions.getTextFromAttribute(driver,
					selectedColor, "alt", "Alt attribute value");
		}
		return selectedColorName;
	}

	public int getColorSwatchesCount() throws Exception {
		return lstColorSwatches.size();
	}

	public String getSelectedSwatchColors(int index) throws Exception {
		BrowserActions.clickOnElement(lstColorSwatches.get(index - 1), driver,
				"Select Color swatches");
		waitForSpinner();
		return lstColorSwatches.get(index - 1).getAttribute("src")
				.split("&crop")[0];
	}

	public String getFaceoutImageColors() throws Exception {
		return productPrimaryImage.getAttribute("src").split("&layer")[0];
	}

	public boolean verifyStdPriceDetails() throws Exception {
		boolean status = false;
		String stdPrice = BrowserActions.getText(driver, lblStdPrice,
				"Price Details");
		if (stdPrice.contains("$")) {
			status = true;
		}
		return status;
	}

	public boolean verifyOrgPriceDetails() throws Exception {
		boolean status = false;
		String orgPrice = BrowserActions.getText(driver, lblOrgPrice,
				"Price Details");
		if (orgPrice.contains("$")) {
			status = true;
		}
		return status;
	}

	public boolean verifyNowPriceDetails() throws Exception {
		boolean status = false;
		String nowPrice = BrowserActions.getText(driver, lblNowPrice,
				"Price Details");
		if (nowPrice.contains("$")) {
			status = true;
		}
		return status;
	}

	public String getTextFromAttribute() throws Exception {
		return (BrowserActions.getTextFromAttribute(driver, btnAddtoBag,
				"disabled", "Add coupon to bag disablity"));
	}

	/**
	 * To get the image name along with the URL
	 * 
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getImageName(int index) throws Exception {
		String imageUrl = BrowserActions.getTextFromAttribute(driver,
				imgPrimaryImage, "src", "href of indexed image");
		String[] imageUrlSplit = imageUrl.split("&layer=");
		imageUrl = imageUrlSplit[0];
		return imageUrl;
	}

	public void clickProductName() throws Exception {
		BrowserActions.clickOnElement(fldProductName, driver, "Product name");
	}

	/**
	 * To get the list of brand names
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getBrandNameInProductlist() throws Exception {

		ArrayList<String> arrlist = new ArrayList<String>();
		for (WebElement element : lstBrandName) {
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from breadcrumb"));
		}
		return arrlist;

	}

	/**
	 * To Click the brand name in pdp page
	 * 
	 * @throws Exception
	 */
	public void clickBrandname() throws Exception {
		BrowserActions.clickOnElement(lnkBrandName, driver, "Brand link");
	}

	/**
	 * To get the text from brandlink
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getTextFromBrand() throws Exception {
		return BrowserActions.getText(driver, lnkBrandName,
				"Text from Brandname");

	}

	public void clickOnFilterByOption() throws Exception {
		BrowserActions.clickOnElement(btnFilterByMobile, driver,
				"Click on filter by");
	}

	public void clickPrimaryImage() throws Exception {
		BrowserActions.clickOnElement(
				imgPrimaryImage.findElement(By.xpath("../..")), driver,
				"Click on Primary Image");
	}

	public FacebookPage navigateToFacebookPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkFacebook, driver);
		BrowserActions.clickOnElement(lnkFacebook, driver,
				"clicking Social Media Facebook Icon");
		Utils.waitForPageLoad(driver);
		return new FacebookPage(driver);
	}

	public TwitterPage navigateToTwitterPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkTwitter, driver);
		BrowserActions.clickOnElement(lnkTwitter, driver,
				"clicking Social Media Twitter Icon");
		return new TwitterPage(driver);
	}

	public GooglePlusPage navigateToGooglePlusPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkGooglePlus, driver);
		BrowserActions.clickOnElement(lnkGooglePlus, driver,
				"clicking Social Media GooglePlus Icon");
		return new GooglePlusPage(driver);
	}

	public PintrestPage navigateToPinterestPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkPintrest, driver);
		BrowserActions.clickOnElement(lnkPintrest, driver,
				"clicking Social Media Pinterest Icon");
		return new PintrestPage(driver);
	}

	/**
	 * To get selected option text from quantity dropdown
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedQunatityValue() throws Exception {
		return BrowserActions.getText(driver, txtSelectedQuantity,
				"Selected value");
	}

	/**
	 * To select the quantity from drop down and return the text of the selected
	 * quantity
	 */
	public String selectQunatityDropDown() throws Exception {

		Select dropdown = new Select(drpQuantity);
		WebElement option = dropdown.getFirstSelectedOption();
		return BrowserActions.getText(driver, option,
				"Selected option using index");

	}

	/**
	 * To getText of LimitedStockAvailability
	 *
	 * @return txtLimitedStock
	 * @throws Exception
	 */
	public String getTextofLimitedStockMessage() throws Exception {
		String textLimitedStock = null;
		textLimitedStock = BrowserActions.getText(driver, txtLimitedStock,
				"getting Text of LimitedStockAvaiable");
		return textLimitedStock;
	}

	/**
	 * To select the maximum quantity
	 *
	 * @return selectedQuantity
	 * @throws Exception
	 */

	public String selectMaxQuantity() throws Exception {
		String selectedQuantity = null;
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver,
				SELECTED_QTY);
		BrowserActions.clickOnElement(qtyDefaultValue, driver,
				"Quantity drop down");
		List<WebElement> listOfQuantity = driver
				.findElements(By
						.cssSelector("div[class='custom-select current_item'] ul li:not([class*='hide'])"));
		BrowserActions.clickOnElement(
				listOfQuantity.get(listOfQuantity.size() - 1), driver,
				"Select quantity option");
		waitForSpinner();
		selectedQuantity = driver.findElement(By.cssSelector(SELECTED_QTY))
				.getText();
		return selectedQuantity;
	}

	/**
	 * Click "Add to wishlist" link in the PDP page
	 * 
	 */
	public void clickAddToWishListLink() throws Exception {
		BrowserActions.scrollToViewElement(lnkAddToWishList, driver);
		Utils.waitForElement(driver, lnkAddToWishList);
		if (lnkAddToWishList.findElement(By.xpath("..")).getAttribute("class")
				.contains("disabled")) {
			selectColor();
			selectSize();
		}
		BrowserActions.clickOnElement(lnkAddToWishList, driver,
				"Add to wishList link");
	}

	/**
	 * To click Brand link for product set
	 * 
	 * @return SearchResultPage
	 */
	public SearchResultPage clickBrandLinkForProductSet() throws Exception {
		BrowserActions.clickOnElement(lnkBrandNameProductSet, driver,
				"Brand link of Product Set");
		return new SearchResultPage(driver).get();
	}

	public void clickOnThumbnailImage() throws Exception {
		BrowserActions.mouseHover(driver, imgClickThumbnailQV);
		BrowserActions.clickOnElement(imgClickThumbnailQV, driver, "Image");
	}

	public boolean getSelectedSwatchColors() throws Exception {
		String ColorSwatch = null;
		boolean status = false;
		// / Log.message("lstColor.size()==" + (lstColor.size() - 1));
		for (int j = 0; j < lstColor.size() - 1; j++) {
			WebElement ele = lstColor.get(j);
			Thread.sleep(1000);
			BrowserActions.clickOnElement(ele, driver, "Select Color swatches");
			Thread.sleep(1000);
			// ColorSwatch = ele.findElement(By.cssSelector("
			// img")).getAttribute("src");
			WebElement selectedColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			ColorSwatch = BrowserActions.getTextFromAttribute(driver,
					selectedColor, "src", "Alt attribute value");
			ColorSwatch = ColorSwatch.split("&crop")[0];
			String ProductColor = productPrimaryImage.getAttribute("src")
					.split("&layer")[0];
			// Log.message("ProductColor"+ProductColor);
			// Log.message("ColorSwatch"+ColorSwatch);

			if (ProductColor.equals(ColorSwatch)) {
				status = true;
			} else {
				status = false;
			}
		}
		return status;
	}

	/**
	 * To set the random Value for all products in product set
	 * 
	 * @return String Array - List of selected and set Value in product sets
	 * @throws Exception
	 */
	public String selectValueRandomInProductSet(int index) throws Exception {
		String selectedSize = new String();
		Random random = new Random();

		List<WebElement> sizeDefaultValue = BrowserActions.checkLocators(
				driver, DEFAULT_SELECTED_VALUE_DROPDOWN);
		BrowserActions.clickOnElement(sizeDefaultValue.get(index), driver,
				"Clicked Value Button");
		// BrowserActions.nap(1);
		List<WebElement> lstValue = lstProductSet
				.get(index)
				.findElements(
						By.cssSelector(".value .selection-list >li:not([class*='hide'])"));
		int rand = random.nextInt(lstValue.size());
		System.out.println(lstValue.get(rand).getText());
		BrowserActions.clickOnElement(lstValue.get(rand), driver,
				"Select Value option");
		selectedSize = lstSize.get(rand).getText().trim();
		waitForSpinner();

		return selectedSize;
	}

	/**
	 * To verify whether any size is selected by default for all products in
	 * Product Set
	 * 
	 * @return 'true' - no size is selected default - Default size string
	 *         'Select...' 'false' - size is selected by default
	 * @throws Exception
	 */
	public boolean verifyDefaultSizeStatusInProductSet() throws Exception {

		for (int i = 0; i < lstProductSet.size(); i++) {
			if (!(sizeDefaultValue.get(i).getText().contains("Select")))
				return false;
		}
		return true;
	}

	/**
	 * To set the random size for all products in product set
	 * 
	 * @return String Array - List of selected and set size in product sets
	 * @throws Exception
	 */
	public String[] selectSizeRandomInProductSet() throws Exception {
		String[] selectedSize = new String[lstProductSet.size()];
		Random random = new Random();

		for (int i = 0; i < lstProductSet.size(); i++) {
			List<WebElement> sizeDefaultValue = BrowserActions.checkLocators(
					driver, DEFAULT_SELECTED_VALUE_DROPDOWN);
			BrowserActions.clickOnElement(sizeDefaultValue.get(i), driver,
					"Clicked Size Button");
			List<WebElement> lstSize = lstProductSet
					.get(i)
					.findElements(
							By.cssSelector(".size .selection-list >li:not([class*='hide'])"));
			int rand = random.nextInt(lstSize.size());
			BrowserActions.clickOnElement(lstSize.get(rand), driver,
					"Select size option");
			selectedSize[i] = lstSize.get(rand).getText().trim();
			waitForSpinner();
		}

		return selectedSize;
	}

	public boolean verifyAddToRegistryLinkEnabled() {
		return Utils.waitForElement(driver, btnRegistryAsGuest);
	}

	public String getSuccessMsg() throws Exception {
		String SuccessMsg = BrowserActions.getText(driver,
				SucessMsgForAddToBag, "Sucess Msg For AddToBag");
		return SuccessMsg;
	}

	/**
	 * Navigate to product set Quick View Page and click on the Shop for Entire
	 * Collections to redirect to PDP Page
	 * 
	 * @return
	 * @throws Exception
	 */
	public PdpPage clickOnShopEntire() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.clickOnElement(btnShopEntireProductSet, driver,
				"Click on the Product set on 'Shop Entire Collections'");
		return new PdpPage(driver).get();
	}

	public String getThresholdMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		String freeShippingTxt = BrowserActions.getText(driver,
				shippingThresholdMessage, "Get the Free shipping message");
		return freeShippingTxt;
	}

	/**
	 * Get the Wish list Success message on the Quick view page
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getWishlistSuccessMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		String txtAddWishlist = BrowserActions.getText(driver,
				txtAddToWishlistSuccessMessage,
				"Product Added To Wishlist as a signed in User ");
		return txtAddWishlist;

	}

	// /

	public String getFreeshippingmessage() throws Exception {
		return BrowserActions.getText(driver, txtFreeshippingmessage,
				"Free shipping Message in Quick View");
	}

	public boolean verifyFreeShippingMessageBelowOnlineOnlyMessage()
			throws Exception {
		boolean status = false;
		if (Utils
				.waitForElement(
						driver,
						lblOnlineonlyMessage.findElement(By
								.xpath("following-sibling::div[@class='shipping-promo-container']")))) {
			status = true;
		}
		return status;
	}

	/**
	 * To get the list of size options
	 * 
	 * @return listofsizeoption
	 * @throws Exception
	 */
	public List<String> getSizeOptions() throws Exception {
		List<String> listofsizeoption = new ArrayList<String>();

		// checking size drop down have one value or more that one
		WebElement sizeDefaultValue = BrowserActions.checkLocator(driver,
				DEFAULT_SELECTED_VALUE_DROPDOWN);
		// clicking size drop down
		BrowserActions.clickOnElement(sizeDefaultValue, driver,
				"Clicked Size Button");
		BrowserActions.nap(1);

		List<WebElement> listOfSize = BrowserActions.checkLocators(driver,
				LIST_OF_OPTIONS_IN_DROPDOWN);
		for (WebElement e : listOfSize) {
			listofsizeoption.add(BrowserActions.getText(driver, e,
					"Size drop down option"));
		}
		return listofsizeoption;
	}

	/*
	 * To get the text from added wish list
	 * 
	 * @throws Exception
	 */
	public String getTextFromAddedWishList() throws Exception {
		BrowserActions.scrollToViewElement(lblsuccessMessageWishList, driver);
		String txtAddedWishList = BrowserActions.getText(driver,
				lblsuccessMessageWishList, "added message");
		return txtAddedWishList;
	}

	/**
	 * To get the text from added Registry
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromAddedRegistryForGiftCard() throws Exception {
		BrowserActions.scrollToViewElement(lblsuccessMessageGiftcard, driver);
		String txtAddedRegistry = BrowserActions.getText(driver,
				lblsuccessMessageGiftcard, "added message");
		return txtAddedRegistry;
	}

	

	/**
	 * To get product Details
	 * 
	 * @return
	 * @return
	 * @Return product Details
	 * 
	 *         Digdarshi>>>>
	 */
	public LinkedHashMap<String, String> getProductDetails() throws Exception {
		LinkedHashMap<String, String> productDetail = new LinkedHashMap<String, String>();
		productDetail.put("ProductName", productName.getText());
		productDetail.put("Upc",
				UPCvalue.getText().trim().split("\\:")[1].trim());
		productDetail.put("Color", productcolor.getText());
		String selectedSize = driver
				.findElement(
						By.cssSelector("div[class='value'] div[class*='selected-option']"))
				.getText();
		productDetail
				.put("Size",
						driver.findElement(
								By.cssSelector("div[class='value'] div[class*='selected-option']"))
								.getText());

		productDetail.put("Quantity", productQty.getText());

		return productDetail;
	}

	public void clickOnUpdateShoppingCartlnk() throws Exception {
		Utils.waitForElement(driver, btnUpdateShoppingCart);
		BrowserActions.scrollToViewElement(btnUpdateShoppingCart, driver);
		BrowserActions.javascriptClick(btnUpdateShoppingCart, driver,
				"Update Shopping Cart");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * 
	 * To select size based on index
	 * 
	 * @param index
	 * 
	 * @throws Exception
	 */

	public void selectSizeByIndex(int index) throws Exception {
		clickSizeDropown();
		BrowserActions.javascriptClick(lstSize.get(index - 1), driver, "Size");
		Utils.waitUntilElementDisappear(driver, spinner);

	}
}// QuickView
