package com.belk.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class EnlargeViewPage extends LoadableComponent<EnlargeViewPage>{
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	
	/**********************************************************************************************
	 ********************************* WebElements of Quick view Page ***********************************
	 **********************************************************************************************/
	@FindBy(css = "div[id='pdpMain']")
	WebElement quickViewContent;
	
	@FindBy(css = "div[class='value']>div[class='custom-select']")
	WebElement drpSize;
	
	@FindBy(css=".swatches.color li")
    List<WebElement> lstColor;
    
    @FindBy(css=".color .label span")
    WebElement lblColorSelect;
    
    @FindBy(css=".ui-dialog-content ul[class='swatches color'] .selectable:nth-child(2)  img[src*='belk.']")
    List<WebElement> lstColorSelect;
          
    @FindBy(css = "div[class='click-zoom-img']>img")
	WebElement productPrimaryImage;
          
    @FindBy(css = "div[id='ui-id-1']>ul[class='swatches color']>li.selectable>a>img")
	List<WebElement> lstColorSwatches;    
    
    @FindBy(css = ".loader[style*='block']")
	WebElement PDPspinner;
    
    @FindBy(css = "div[style*='display: none;']")
    WebElement enlargePopUPClose;
    
    @FindBy(css = "span[class='ui-button-icon-primary ui-icon ui-icon-closethick']")
    WebElement iconCloseEnlargeView;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public EnlargeViewPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, quickViewContent))) {
			Log.fail("PDP Page did not open up. Site might be down.", driver);
		}


	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}
	
	public boolean verifyColorMouseHover() throws Exception {
        boolean status = false;
        Actions action = new Actions(driver);
        action.moveToElement(lstColor.get(0).findElement(By.cssSelector("a"))).build().perform();
        if(lstColor.get(0).getCssValue("color").equals("rgba(0, 0, 0, 1)")) {
               status = true;
        }
        return status;
	 }
	 
	 /**
	 * To verify the color selected and the text above the color swatches are matched
	 * @throws Exception
	 */
	 public boolean verifySelectedColorIsDisplayed() throws Exception
	 {
	        boolean status = false;
	        for(WebElement element : lstColor) {
	               if(element.getAttribute("class").contains("selected")) {
	                      if(lblColorSelect.getText().equals(element.findElement(By.cssSelector("img")).getAttribute("alt"))) {
	                            status = true;
	                            break;
	                     }
	               }
	        }
	        return status;
	 }
	
	/**
	 * To select color based on index
	 * @param index
	 * @throws Exception
	 */
	 public void selectColorByIndex(int index) throws Exception {
	        BrowserActions.clickOnElement(lstColor.get(index), driver, "Color");
	 }
	  
	 
	  
	 /*public boolean verifyColorSelected() throws Exception
	 {
		 boolean status = false;		 
		 String swatchColor="";		 			
		 		for(WebElement element : lstColorSelect) {
			 		String enlargeSwatchColor=  BrowserActions.getTextFromAttribute(driver, element, "src", "get src");
			 		swatchColor = enlargeSwatchColor.substring(enlargeSwatchColor.indexOf("?layer=0&") + 1,enlargeSwatchColor.indexOf("&crop="));
			 		//Log.message(" Verify color txt swatchColor ======>"+swatchColor);
		 		 }		 		 
		 		String enlargeProductColor=  BrowserActions.getTextFromAttribute(driver, productColor, "src", "get src");
		 		String prodColor = enlargeProductColor.substring(enlargeProductColor.indexOf("?layer=0&") + 1,enlargeProductColor.indexOf("&layer="));
		 		//Log.message(" Verify color txt prodColor ======>"+prodColor);
		 		
		 		if(swatchColor.equals(prodColor)){		 			
		 			status = true;
		 		}
		 		else{
		 			status = false;
		 		}	 		

		 	return status;  	    	 	
	       
	 }*/
	 
	 /**
		 * To wait for spinner in the PDP
		 */
	public void waitForSpinner() {
		BrowserActions.nap(20);
		Utils.waitUntilElementDisappear(driver, PDPspinner);
	}
	 
	 public String getSelectedSwatchColors(int index) throws Exception {
		BrowserActions.clickOnElement(lstColorSwatches.get(index - 1), driver, "Select Color swatches");		
		waitForSpinner();
		
		Utils.waitForPageLoad(driver);
		return lstColorSwatches.get(index - 1).getAttribute("src").split("&crop")[0];
	}
		
	public String getFaceoutImageColors() throws Exception {	
		Utils.waitForPageLoad(driver);
		return productPrimaryImage.getAttribute("src").split("&layer")[0];
	}


	public int getColorSwatchesCount() throws Exception{
		int size = lstColorSwatches.size();
		return size;
	}
	
	  public void closeEnlargeViewByCloseButton() throws Exception{
		  BrowserActions.clickOnElement(iconCloseEnlargeView, driver, "Enlarge view pop up closed");
	  } 
     
     /**
     * To close the size chart by escape key
     * @throws Exception
     */
     public void closeEnlargeViewByEsckey() throws Exception{
            Actions action = new Actions(driver);
            action.sendKeys(Keys.ESCAPE).build().perform();
     }
     
     
}
