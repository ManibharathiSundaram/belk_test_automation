package com.belk.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class CustomerServiceHomePage extends LoadableComponent<CustomerServiceHomePage> {

	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	
	@FindBy(css=".branding_hearderlogo")
	WebElement readyElement;
	
	//-------------------- Orders Section -----------------------//
	@FindBy(css = "#lefttabs > ul > li.ui-tabs-nav-item.ui-state-default.ui-corner-top.ui-tabs-selected.ui-state-active > a")
	WebElement tabOrders;

	@FindBy(css = "#order_searchform > div.search_order_attributes > h3")
	WebElement lblOrderattributes;

	@FindBy(css = "#OrderSearchForm_OrderNo")
	WebElement txtOrderSearchFormOrderNo;

	@FindBy(css = "#OrderSearchForm_POSNo")
	WebElement txtOrderSearchForm_POSNo;

	@FindBy(css = "#OrderSearchForm_CustomerNo")
	WebElement txtOrderSearchForm_CustomerNo;

	@FindBy(css = "#OrderSearchForm_FirstName")
	WebElement txtOrderSearchForm_FirstName;

	@FindBy(css = "#OrderSearchForm_LastName")
	WebElement txtOrderSearchForm_LastName;

	@FindBy(css = "#OrderSearchForm_CustomerEmail")
	WebElement txtOrderSearchForm_CustomerEmail;

	@FindBy(css = "#order_searchform > div.search_orderstatus_attributes > h3")
	WebElement lblOrderStatus;

	@FindBy(css = "#OrderSearchForm_OrderStatus")
	WebElement drpOrderStatus;

	@FindBy(css = "#OrderSearchForm_ConfirmationStatus")
	WebElement drpOrderConfirmStatus;

	@FindBy(css = "#OrderSearchForm_ShippingStatus")
	WebElement drpOrderShippingStatus;

	@FindBy(css = "#OrderSearchForm_PaymentStatus")
	WebElement drpOrderPayment;

	@FindBy(css = "#OrderSearchForm_ExportStatus")
	WebElement drpOrderExportStatus;

	@FindBy(css = "#order_searchform > div:nth-child(5) > h3")
	WebElement lblOrderCreationDate;

	@FindBy(css = "#OrderSearchForm_OrderStartDate")
	WebElement dateOrderStartDate;

	@FindBy(css = "#OrderSearchForm_OrderEndDate")
	WebElement dateOrderEndDate;

	@FindBy(css = "#order_searchform > div:nth-child(6) > h3")
	WebElement lstSearchResults;

	@FindBy(css = "#OrderSearchForm_ResultSet")
	WebElement lstOrderSearchResultsMaxSize;

	@FindBy(css = "#order_searchform > div.lefttab_searchbutton_container > input")
	WebElement btnOrderSearch;

	@FindBy(css = "#id_order_results_table table tr")
	List<WebElement> tblOrderSearchResults;

	public static final String ICONORDERNUMBER= "td.pointer";
	public static final String EXPORTSTATUS= "td:nth-child(4)";
	public static final String BTNMODIFYORDER= "td.img-button-modify > input";
	
	//-------------------- Customers Section ---------------------//
	
	/**
	 * 
	 * @param driver
	 *            : webdriver
	 */
	public CustomerServiceHomePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		elementLayer = new ElementLayer(driver);
	}// HomePage

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Customer Service Home Page did not open up. Site might be down.", driver);
		}
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		driver.getCurrentUrl();
		Utils.waitForPageLoad(driver);
	}// load
	
	/**
	 * To enter values in Order attributes and customer attributes section
	 * @param txtBoxType - orderNo|POSNo|CustomerNo|FirstName|LastName|Email
	 * @param txtToBeTyped
	 * @throws Exception 
	 */
	public void enterValuesInAttributesSection(String txtBoxType, String txtToBeTyped) throws Exception {
		WebElement element = null;
		switch(txtBoxType) {
			case "orderNo":
				element = txtOrderSearchFormOrderNo;
				break;
			case "POSNo":
				element = txtOrderSearchForm_POSNo;
				break;
			case "CustomerNo":
				element = txtOrderSearchForm_CustomerNo;
				break;
			case "FirstName":
				element = txtOrderSearchForm_FirstName;
				break;
			case "LastName":
				element = txtOrderSearchForm_LastName;
				break;
			case "Email":
				element = txtOrderSearchForm_CustomerEmail;
				break;
		}
		BrowserActions.typeOnTextField(element, txtToBeTyped, driver, txtBoxType);
	}
	
	/**
	 * To select dropdown values in order status
	 * @param txtBoxType - orderNo|POSNo|CustomerNo|FirstName|LastName|Email
	 * @param txtToBeTyped
	 * @throws Exception 
	 */
	public void selectValuesInOrderStatus(String drpBoxType, String dropDownValue) throws Exception {
		WebElement element = null;
		switch(drpBoxType) {
			case "Status":
				element = drpOrderStatus;
				break;
			case "Confirmation":
				element = drpOrderConfirmStatus;
				break;
			case "Shipment":
				element = drpOrderShippingStatus;
				break;
			case "Payment":
				element = drpOrderPayment;
				break;
			case "Export":
				element = drpOrderExportStatus;
				break;
		}
		BrowserActions.selectDropDownValue(driver, element, dropDownValue);
	}
}
