package com.belk.pages;

import java.util.LinkedHashMap;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class CreateAccountPage extends LoadableComponent<CreateAccountPage> {

	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public final LinkedHashMap<String, String> accountDetails = new LinkedHashMap<String, String>();
	public String pwd = "test123";
	/**********************************************************************************************
	 ********************************* WebElements of My Account Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "input[id='dwfrm_profile_customer_firstname']")
	WebElement txtFirstName;

	@FindBy(css = "input[id='dwfrm_profile_customer_lastname']")
	WebElement txtLastName;

	@FindBy(css = "input[id='dwfrm_profile_customer_phone']")
	WebElement txtPhoneNumber;

	@FindBy(css = "input[id='dwfrm_profile_customer_email']")
	WebElement txtEmail;

	@FindBy(css = "input[id='dwfrm_profile_customer_emailconfirm']")
	WebElement txtEmailConfirmation;

	@FindBy(css = "input[class='input-text password required']")
	WebElement txtPassword;

	@FindBy(css = "input[class='input-text password confirmpassword required']")
	WebElement txtConfirmPassword;

	@FindBy(css = "input[id='dwfrm_profile_customer_addToEmailList']")
	WebElement chkAddToEmailList;

	@FindBy(css = "a[class='privacy-policy']")
	WebElement lnkPrivacyPolicy;

	@FindBy(css = "button[name='dwfrm_profile_cancel']")
	WebElement btnCancel;

	@FindBy(css = "button[name='dwfrm_profile_confirm']")
	WebElement btnCreateAccountPage;

	@FindBy(css = "span[class='error']")
	WebElement formError;

	@FindBy(css = "a[title='Logout']")
	WebElement btnLogOut;

	@FindBy(css = ".accountheader")
	WebElement lblCreateAnAccountTitle;
	
	@FindBy(css="button[name='dwfrm_wishlist_search_search']")
	WebElement btnFindRegistry;

	/**
	 * 
	 * Constructor class for Login page Here we initializing the driver for page
	 * factory objects. For ajax element waiting time has added while
	 * initialization
	 * 
	 * @param driver
	 * @param url
	 */
	public CreateAccountPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, txtFirstName))) {
			Log.fail("Create Account page didn't open up", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click LogIn button on signin page
	 * Last modified By - Nandhini & Date modified - 1/2/2017
	 * @throws Exception
	 */
	public Object CreateAccount(CreateAccountPage createAccountPage) throws Exception {
		final long startTime = StopWatch.startTime();
		String email = "belkQA".concat(RandomStringUtils.randomAlphabetic(3)).concat(RandomStringUtils.randomNumeric(3)).concat("@aspiresys.com");
		BrowserActions.typeOnTextField(txtFirstName, RandomStringUtils.randomAlphabetic(5), driver, "First Name");
		BrowserActions.typeOnTextField(txtLastName, "BelkTest", driver, "Last Name");
		BrowserActions.typeOnTextField(txtPhoneNumber, RandomStringUtils.randomNumeric(10), driver, "Phone Number");
		BrowserActions.typeOnTextField(txtEmail, email, driver, "Email id");
		BrowserActions.typeOnTextField(txtEmailConfirmation, email, driver, "Email confirmation");
		BrowserActions.typeOnTextField(txtPassword, pwd, driver, "Password");
		BrowserActions.typeOnTextField(txtConfirmPassword, pwd, driver, "Confirm password");
		
		accountDetails.put("firstName", txtFirstName.getAttribute("value").trim());
		accountDetails.put("LastName", txtLastName.getAttribute("value").trim());
		accountDetails.put("emailId", txtEmail.getAttribute("value").trim());
		
		BrowserActions.clickOnElement(btnCreateAccountPage, driver, "Clicked on Create Account");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Create Account' button on 'MyAccount->Create An Account' page",
				StopWatch.elapsedTime(startTime));
		
		if(Utils.waitForElement(driver, btnLogOut)){
			return new MyAccountPage(driver).get();
		}
		if(Utils.waitForElement(driver, btnFindRegistry)){
			return new WishListPage(driver);
		}
		return new GiftRegistryPage(driver).get();
	}

	public final LinkedHashMap<String, String> getAccountDetails(){
		return accountDetails;
		
	}

	/**
	 * Enter First Name in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstName, firstName, driver,
				"First Name");
	}

	/**
	 * Enter Last Name in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterLastName(String lastName) throws Exception {
		BrowserActions.typeOnTextField(txtLastName, lastName, driver,
				"Last Name");
	}

	/**
	 * Enter Phone Number in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterPhoneNumber(String phoneNumber) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneNumber, phoneNumber, driver,
				"Phone Number");
	}

	/**
	 * Enter EmailID in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterEmailID(String email) throws Exception {
		BrowserActions.typeOnTextField(txtEmail, email, driver, "Email id");
	}

	/**
	 * Enter Confirmation EmailID in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterConfirmationEmailID(String confirmEmail) throws Exception {
		BrowserActions.typeOnTextField(txtEmailConfirmation, confirmEmail,
				driver, "Email confirmation");
	}

	/**
	 * Enter Password in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterPassword(String Password) throws Exception {
		BrowserActions.typeOnTextField(txtPassword, Password, driver,
				"Password");
	}

	/**
	 * Enter Caonfirmation Password in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void enterConfirmPassword(String confirmPassword) throws Exception {
		BrowserActions.typeOnTextField(txtConfirmPassword, confirmPassword,
				driver, "Confirm password");
	}
	
	/**
	 * Get First Name in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getFirstName() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtFirstName, "value", "First Name");
	}

	/**
	 * Get Last Name in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getLastName() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtLastName, "value", "Last Name");
	}

	/**
	 * Get Phone Number in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getPhoneNumber() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtPhoneNumber, "value", "Phone Number");
	}

	/**
	 * Get EmailID in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getEmailID() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtEmail, "value", "Email id");
	}

	/**
	 * Get Confirmation EmailID in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getConfirmationEmailID()  throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtEmailConfirmation, "value", "Email confirmation");
	}

	/**
	 * Get Password in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getPassword() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtPassword, "value", "Password");
	}

	/**
	 * Enter Confirmation Password in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getConfirmPassword() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtConfirmPassword, "value", "Confirm password");
	}

	/**
	 * Click Create Account Button in Create Account Page
	 * 
	 * @throws Exception
	 */
	public void clickCreateCreateAccountBtn() throws Exception {
		final long startTime = StopWatch.startTime();

		BrowserActions.clickOnElement(btnCreateAccountPage, driver,
				"Clicked on Create Account");
		Utils.waitForPageLoad(driver);

		Log.event(
				"Clicked 'Create Account' button on 'MyAccount->Create An Account' page",
				StopWatch.elapsedTime(startTime));
	}

	/**
	 * Get Error Message in Create Account Page
	 * 
	 * @throws Exception
	 */
	public String getCreateAccountFormErrorMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, formError,
				"Create Account page form error");
		return dataToBeReturned;
	}

	/**
	 * get the Title
	 * 
	 * @throws Exception
	 */
	public String getTitle() throws Exception {
		String title = BrowserActions.getText(driver, lblCreateAnAccountTitle,
				"Form Title");
		return title;
	}

}
