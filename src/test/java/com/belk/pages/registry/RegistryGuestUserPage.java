package com.belk.pages.registry;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class RegistryGuestUserPage extends LoadableComponent <RegistryGuestUserPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/
	
	@FindBy(css = ".form-row.form-row-button>button")
	WebElement btnCreateNewAccount;
	
	@FindBy(css = "#dwfrm_giftregistry_event_name")
	WebElement eventName;
	
	@FindBy(css = "#dwfrm_giftregistry_event_date")
	WebElement eventDate;
	
	@FindBy (css =".form-row.required:nth-child(2) .selected-option")
	WebElement btnEventDefaultSelection ;
	
	@FindBy (css = ".custom-select .selected-option") 
	WebElement eventType ;

	@FindBy(css = ".button.simple")
	WebElement cancelButtonRegistryPage;
	
	@FindBy(css = ".form-row.form-row-button>button")
	WebElement continueButtonRegistryPage;
	
	@FindBy(css = "#dwfrm_giftregistry_search > div.form-row.form-row-button > button") 
	WebElement btnFindRegistry;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public RegistryGuestUserPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
		if (isPageLoaded && !(Utils.waitForElement(driver, btnCreateNewAccount))) {
			Log.fail("Header Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	public String verifyEventType() throws Exception {
		String eventtype =	BrowserActions.getText(driver, eventType, "Event link");
		return eventtype;
	}	
	
	public boolean verifyGustRegistryElement(){
		boolean status=false;
		if(Utils.waitForElement(driver, btnFindRegistry)){
			status=true;
		}return status;
	}
}
