package com.belk.pages.registry;

import org.openqa.selenium.Point;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.GiftCardsPage;
import com.belk.pages.HomePage;
import com.belk.pages.MiniCartPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class RegistryInformationPage extends LoadableComponent<RegistryInformationPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/
	@FindBy(css = "div[class='cart-order-totals'] button[class='button-fancy-large']")
	WebElement btnCheckoutInOrderSummary;

	@FindBy(css = "div#primary h1")
	WebElement lblregistry;

	@FindBy(css = "button[class='button-text delete-item simple']")
	List<WebElement> btnRemoveItem;

	@FindBy(css = ".registry-bottom-actions>p")
	WebElement msgNoItemsInRegistry;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	// ".product-list-item .name>a"
	@FindBy(css = ".product-list-item .name>a")
	List<WebElement> lstProductNames;

	@FindBy(css = ".form-row.datepicker-row.required")
	WebElement eventDate;

	@FindBy(css = ".hide-desktop button[name='dwfrm_giftregistry_navigation_navEvent']")
	WebElement tabEventInfoForMobile;

	@FindBy(css = "#dwfrm_giftregistry_event_type option:nth-child(1)")
	WebElement selectEventTypeRandom;

	/***********************************************************************************/
	// Web Elements for Tabs
	/***********************************************************************************/

	@FindBy(css = "button[name='dwfrm_giftregistry_navigation_navRegistry']")
	WebElement tabMyRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_navigation_navEvent']")
	WebElement tabEventInfo;

	@FindBy(css = "button[name='dwfrm_giftregistry_navigation_navShipping']")
	WebElement tabShippingInfo;

	@FindBy(css = "button[name='dwfrm_giftregistry_navigation_navPurchases']")
	WebElement tabPurchases;

	// ******************Find Registry***********************//

	@FindBy(id = "dwfrm_giftregistry_search_advancedsearch_registrantFirstName")
	WebElement txtFirstNameInFindRegistry;

	@FindBy(id = "dwfrm_giftregistry_search_advancedsearch_registrantLastName")
	WebElement txtLastNameInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement selectEventTypeInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement txtCityInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement txtStateInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement selectEventDateMonthInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement selectEventDateYearInFindRegistry;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement txtRegistryIDInFindRegistry;
	
	@FindBy(css=".event-info .user li.list-title.bold")
	WebElement txtEventNameAndDate;
	

	@FindBy(css = ".gift-registry-link .advanced-search")
	WebElement lnkAdvancedSearchInFindRegistry;

	@FindBy(css = ".gift-registry-link .simple-search")
	WebElement lnkSimpleSearchInFindRegistry;

	@FindBy(css = ".eventAddress-state-row .selected-option")
	WebElement selectStateInFindRegistry;

	@FindBy(css = ".month-row .selected-option")
	WebElement selectMonthInFindRegistry;

	@FindBy(css = ".year-row .selected-option")
	WebElement selectYearInFindRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_search_search']")
	WebElement btnFindRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_create']")
	WebElement btnCreateNewRegistry;

	@FindBy(css = "div[class='section-header']:nth-child(2)")
	WebElement headerEventInformation;

	@FindBy(css = "[class='item-list gift-reg-purchases']")
	WebElement msgPurchase;

	@FindBy(css = "div[class='address-before']>h2")
	WebElement headerPreEventShipping;
	/********************************************************************************/
	//
	/********************************************************************************/

	@FindBy(id = "dwfrm_giftregistry_event_name")
	WebElement txtEventname;

	@FindBy(id = "dwfrm_giftregistry_event_town")
	WebElement txtEventCity;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_type']//following-sibling::div")
	WebElement selectEventType;

	@FindBy(css = ".state-row .selected-option")
	WebElement selectState;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_participant_role']//following-sibling::div")
	WebElement selectRegistrantRole;

	@FindBy(id = "dwfrm_giftregistry_event_participant_firstName")
	WebElement txtRegistrantFirstName;

	@FindBy(id = "dwfrm_giftregistry_event_participant_lastName")
	WebElement txtRegistrantLastName;

	@FindBy(id = "dwfrm_giftregistry_event_participant_email")
	WebElement txtRegistrantEmail;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_coParticipant_role']//following-sibling::div")
	WebElement selectCoRegistrantRole;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_firstName")
	WebElement txtCoRegistrantFirstName;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_lastName")
	WebElement txtCoRegistrantLastName;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_email")
	WebElement txtCoRegistrantEmail;

	@FindBy(css = "button[name='dwfrm_giftregistry_event_setParticipants']")
	WebElement btnContinue;

	@FindBy(xpath = "//a[@class='button simple']")
	WebElement btnCancelInShippingInfo;

	@FindBy(css = "button[name='dwfrm_giftregistry_navigation_navRegistry'].simple")
	WebElement btnCancelInEventInfo;

	/********************************************************************************/
	//
	/********************************************************************************/

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressBeforeList']//following-sibling::div")
	WebElement selectSavedAddressInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_addressid")
	WebElement txtAddressTitleInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")
	WebElement txtFirstNameInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")
	WebElement txtLastNameInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")
	WebElement txtAddress1InPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")
	WebElement txtAddress2InPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")
	WebElement txtCityInPreEventShipping;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state']//following-sibling::div")
	WebElement selectStateInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal")
	WebElement txtZipcodeInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")
	WebElement txtPhoneInPreEventShipping;

	@FindBy(xpath = "//h2[text()='Pre-Event Shipping']")
	WebElement preEventShipping;

	@FindBy(xpath = "//h2[text()='Post-Event Shipping']")
	WebElement postEventShipping;

	@FindBy(css = ".hide-desktop button[name='dwfrm_giftregistry_navigation_navShipping']")
	WebElement tabShippingInfoForMobile;

	@FindBy(css = "fieldset[ name='address-before']>div[class='section-header']")
	WebElement SelectOrAddAnAddress;

	@FindBy(css = "button[name='dwfrm_giftregistry_eventaddress_setBeforeAfterAddresses']")
	WebElement btnContinueInShippingInFo;

	/********************************************************************************/
	// Create New Registry Step-2 (PostEvent)
	/********************************************************************************/

	@FindBy(css = ".usepreevent.secondarybutton")
	WebElement btnUsePreEventAddress;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressAfterList']//following-sibling::div")
	WebElement selectSavedAddressInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_addressid")
	WebElement txtAddressTitleInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname")
	WebElement txtFirstNameInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname")
	WebElement txtLastNameInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")
	WebElement txtAddress1InPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_address2")
	WebElement txtAddress2InPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_city")
	WebElement txtCityInPostEventShipping;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state']//following-sibling::div")
	WebElement selectStateInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")
	WebElement txtZipcodeInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_phone")
	WebElement txtPhoneInPostEventShipping;

	/******************************************************************************/

	@FindBy(css = "#dwfrm_giftregistry_event > dl")
	List<WebElement> lblEventInfo;

	@FindBy(id = "dwfrm_giftregistry_event_terms")
	WebElement chkTermsConditions;

	@FindBy(id = "dwfrm_giftregistry_event_knot")
	WebElement chkKnot;

	@FindBy(id = "dwfrm_giftregistry_event_associatehelp")
	WebElement chkAssociate;

	@FindBy(id = "dwfrm_giftregistry_event_employeeid")
	WebElement txtAssociateId;

	@FindBy(id = "dwfrm_giftregistry_event_storeid")
	WebElement txtStoreId;

	@FindBy(css = ".button-fancy-small.add-to-cart")
	List<WebElement> btnAddToBag;

	@FindBy(css = ".product-list-item>.sku .value")
	List<WebElement> lblUpcOfProducts;

	@FindBy(css = ".item-qty-text .selected-option.selected")
	List<WebElement> txtQuantity;

	@FindBy(css = ".mini-cart-content")
	WebElement minicartContent;

	@FindBy(css = "table>tbody>tr>.event-view>a")
	List<WebElement> ViewRegistrylnk;

	@FindBy(css = "[class='item-dashboard']>a:not([id])")
	List<WebElement> ViewRegistrylnkForMobile;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;

	@FindBy(css = ".item-edit-details>a")
	List<WebElement> lnkEditDetails;

	@FindBy(css = ".button-text.update-item.simple")
	List<WebElement> lnkUpdate;

	@FindBy(css = ".button-text.delete-item.simple")
	List<WebElement> lnkRemove;

	@FindBy(css = "#dwfrm_giftregistry_items_i1_quantity")
	WebElement drpWouldLove;

	@FindBy(css = "div[class='item-option option-quantity-desired']")
	List<WebElement> lblWouldLove;

	@FindBy(css = "div[class='item-option option-quantity-purchased']")
	List<WebElement> lblStillNeeds;

	@FindBy(css = ".pagination li")
	List<WebElement> lstPageNos;



	@FindBy(css = ".content-asset>p>span")
	WebElement registryContentSlot;
	/******************************************************************************/
	// Registry Table
	/******************************************************************************/

	@FindBy(css = ".registry-list-table > table > tbody > tr")
	List<WebElement> lstYourRegistries;

	@FindBy(css = ".stock-product")
	WebElement txtmessagestock;

	@FindBy(css = "#add-to-cart")
	WebElement btnUpdateRegistry;

	@FindBy(css = ".cart-cancel.close-dialog")
	WebElement lnkCancel;

	@FindBy(css = ".product-brand>span")
	WebElement productNameInPopup;

	@FindBy(css = ".swatches.color>li")
	List<WebElement> lstColor;
	@FindBy(css = "div[data-attribute='color']>span[class='value']")
	WebElement txtcolor;
	@FindBy(css = "li[class='attribute color']>div[class='label']>span[class='attribute-value']")
	WebElement txtcolorineditpopup;

	@FindBy(css = ".button-text.update-item.simple")
	WebElement linkUpdate;
	@FindBy(css = ".button-text.delete-item.simple")
	WebElement linkRemove;

	@FindBy(css = ".addgiftcert.secondarybutton.button")
	WebElement btnAddGiftCard;

	@FindBy(css = ".button.additemstoregistry")
	WebElement btnAddItemstoRegistry;

	@FindBy(css = ".print-page")
	WebElement lnkPrintRegistry;

	@FindBy(css = ".list-share .share-option.secondarybutton")
	WebElement btnMakeListPrivate;

	@FindBy(css = ".share-options .share-icon")
	List<WebElement> lnkSocialSharing;

	@FindBy(css = ".col-1>h2")
	WebElement txtRegistryNameandEventDate;

	@FindBy(css = ".registryId")
	WebElement txtRegistryId;

	@FindBy(css = "div[class='product-list-item']>div[class='price']>div[class='original-price']")
	WebElement txtOriginalPrice;

	@FindBy(css = "div[class='product-list-item']>div[class='price']>div[class='now-price']")
	WebElement txtSalePrice;

	@FindBy(css = "[class='page-content-tab-navigaton top-nav']>fieldset>span>button[name='dwfrm_giftregistry_navigation_navPurchases']")
	WebElement tabPurchasesForDesktop;

	@FindBy(css = ".hide-desktop button[name='dwfrm_giftregistry_navigation_navPurchases']")
	WebElement tabPurchasesForMobile;

	@FindBy(css = "div [class='product-list-item']>div")
	List<WebElement> productDetailsInPurchaseTab;

	@FindBy(css = "div[id*='barcodeRegistry'] div[style='clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 14px; margin-top: 5px;']")
	WebElement txtBarCode;

	@FindBy(css = ".user li:not([class='list-title bold'])")
	List<WebElement> lstRegistryDesc;

	@FindBy(css = ".gift-registry-empty-message")
	WebElement txtEmptyRegistry;

	@FindBy(css = ".event-info .user li")
	List<WebElement> lstEventDetails;

	@FindBy(css = ".item-image>a>img")
	List<WebElement> lstProductImages;

	@FindBy(css = ".item-purchased-row:nth-child(1)")
	WebElement noOfQtyInPurchaseTab;

	@FindBy(css = ".item-purchased-row:nth-child(2)")
	WebElement nameOfUserInPurchaseTab;

	@FindBy(css = ".item-purchased-row:nth-child(3)")
	WebElement dateFormatInPurchaseTab;
	
	@FindBy(css = ".item-purchased-row:nth-child(3)")
	List<WebElement> dateFormatInPurchase;

	///// Locators added /////////

	@FindBy(css = "table[class='item-list gift-registry-items']>tbody>tr")
	List<WebElement> lstItemsInRegistry;

	@FindBy(css = ".event-code-img")
	WebElement BarCodeInViewRegistry;

	@FindBy(css = ".event-info")
	WebElement EventInfoInFindRegistry;

	@FindBy(css = ".col-2>div")
	WebElement registryBarCode;

	@FindBy(css = ".item-dashboard>form>div[class='form-row option-priority item-option']>div>div[class='custom-select']>div")
	WebElement prority;

	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabregistry-head.nav-head>button")
	WebElement tabMyRegistryMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabevent-head.nav-head>button")
	WebElement tabEventInfoMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabshipping-head.nav-head>button")
	WebElement tabShippingInfoMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabpurchases-head.nav-head>button")
	WebElement tabPurchasesMobile;

	@FindBy(css = ".section-header")
	WebElement EventTitleInFinDRegistry;

	@FindBy(css = "#barcodeRegistry_200000320>div")
	WebElement barcode;

	@FindBy(css = ".registry-bottom-actions>p:nth-child(1)")
	WebElement messageNoitem;

	@FindBy(css = ".sku")
	WebElement lblUPC;

	@FindBy(css = ".item-dashboard>form>div[class='form-row option-priority item-option']>div>div>ul")
	List<WebElement> lstprority;

	@FindBy(css = "div>div[class='custom-select']")
	WebElement btnPrority;

	@FindBy(css = "td[class='cart-details'] .item-links.gift-icon")
	List<WebElement> lsttxtGiftItemIcon;

	@FindBy(css = ".item-dashboard div[class='item-option option-quantity-purchased'] .value")
	WebElement txtstillNeedCount;

	@FindBy(css = "table[class='item-list gift-registry-items'] >tbody>tr")
	List<WebElement> lstOfRegistryItems;
	
    @FindBy(css = ".event-name:nth-child(3)")
	List<WebElement> regId;

	@FindBy(xpath = "//div[@class='item-list-device']/div/div[span and text()]")
	List<WebElement> regDetailsInMobile;
	
	@FindBy(css = "div[class='item-option']>div[class='value']")
	WebElement  txtStillNeeds;
	
	@FindBy(css = "div[class='product-list-item']>div[data-attribute='size']>span[class='value']")
	List<WebElement> lblSizeOfProducts;
	
	@FindBy(css = "div[class='product-list-item']>div[data-attribute='color']>span[class='value']")
	List<WebElement> lsttxtcolor;
	
	@FindBy(css = ".item-option.option-add-to-cart>fieldset>button[class='share-option secondarybutton']")
	WebElement makeThisItemPublic;
	
	
	@FindBy(css = ".option-add-to-cart .item-qty-text")
	WebElement drpQty;
	
	@FindBy(css = ".option-add-to-cart .item-qty-text>span>div>ul")
	public List<WebElement> lstQty;
	
	@FindBy(css = ".option-add-to-cart .item-qty-text>span[class='value']>div>div[class='selected-option selected']")
	WebElement selectedQty;
	



	/******************************************************************************/
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public RegistryInformationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lblregistry))) {
			Log.fail("Header Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	public ShoppingBagPage clickMiniCart() throws Exception {
		BrowserActions.clickOnElement(miniCart, driver, "Clicking on MiniCart Icon");
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * to type the event name in add new registry
	 * 
	 * @param event
	 *            name
	 * @throws Exception
	 */
	public void setEventName(String eventname) throws Exception {
		BrowserActions.typeOnTextField(txtEventname, eventname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the event city in add new registry
	 * 
	 * @param event
	 *            name
	 * @throws Exception
	 */
	public void setEventCity(String city) throws Exception {
		BrowserActions.typeOnTextField(txtEventCity, city, driver, "First Name in Find Registry");
	}

	/**
	 * to select the event type
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectEventType(String eventtype) throws Exception {

		BrowserActions.javascriptClick(selectEventType, driver, "State Drop down");
		List<WebElement> lstElement = selectEventType.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(eventtype)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the state
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectState(String state) throws Exception {
		BrowserActions.javascriptClick(selectState, driver, "State Drop down");
		List<WebElement> lstElement = selectState.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the role
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectRegistrantRole(String role) throws Exception {
		BrowserActions.javascriptClick(selectRegistrantRole, driver, "State Drop down");
		List<WebElement> lstElement = selectRegistrantRole.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(role)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantFirstName, firstName, driver, "First Name in Find Registry");
	}

	/**
	 * to type the last name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantLastName(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantLastName, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the email
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantEmail(String email) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantEmail, email, driver, "First Name in Find Registry");
	}

	/**
	 * to select the role
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectCoRegistrantRole(String role) throws Exception {
		BrowserActions.javascriptClick(selectCoRegistrantRole, driver, "State Drop down");
		List<WebElement> lstElement = selectCoRegistrantRole.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(role)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantFirstName, firstName, driver, "First Name in Find Registry");
	}

	/**
	 * to type the last name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantLastName(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantLastName, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the email
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantEmail(String email) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantEmail, email, driver, "First Name in Find Registry");
	}

	/**
	 * to click on the continue button
	 * 
	 * @throws Exception
	 */
	public void clickContinue() throws Exception {
		BrowserActions.clickOnElement(btnContinue, driver, "Continue button in Create New Registry");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to click on the cancel button
	 * 
	 * @throws Exception
	 */
	public void clickCancelInShippingInfo() throws Exception {
		BrowserActions.clickOnElement(btnCancelInShippingInfo, driver, "Continue button in Create New Registry");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to click on the cancel button
	 * 
	 * @throws Exception
	 */
	public void clickCancelInEventInfo() throws Exception {
		BrowserActions.clickOnElement(btnCancelInEventInfo, driver, "Continue button in Create New Registry");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to type the Address Title in the pre event shipping in create new
	 * registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddressTitleInPreEventShipping(String addressName) throws Exception {
		BrowserActions.typeOnTextField(txtAddressTitleInPreEventShipping, addressName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstNameInPreEventShipping(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInPreEventShipping, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setLastNameInPreEventShipping(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInPreEventShipping, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddress1InPreEventShipping(String address1) throws Exception {
		BrowserActions.typeOnTextField(txtAddress1InPreEventShipping, address1, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setaddress2InPreEventShipping(String address2) throws Exception {
		BrowserActions.typeOnTextField(txtAddress2InPreEventShipping, address2, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCityInPreEventShipping(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCityInPreEventShipping, city, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setZipcodeInPreEventShipping(String zipcode) throws Exception {
		BrowserActions.typeOnTextField(txtZipcodeInPreEventShipping, zipcode, driver, "First Name in Find Registry");
	}

	/**
	 * to select the state in pre event shipping
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInPreEventShipping(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInPreEventShipping, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInPreEventShipping.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setPhoneInPreEventShipping(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneInPreEventShipping, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to type the Address Title in the post event shipping in create new
	 * registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddressTitleInPostEventShipping(String addressName) throws Exception {
		BrowserActions.typeOnTextField(txtAddressTitleInPostEventShipping, addressName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstNameInPostEventShipping(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInPostEventShipping, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setLastNameInPostEventShipping(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInPostEventShipping, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddress1InPostEventShipping(String address1) throws Exception {
		BrowserActions.typeOnTextField(txtAddress1InPostEventShipping, address1, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setaddress2InPostEventShipping(String address2) throws Exception {
		BrowserActions.typeOnTextField(txtAddress2InPostEventShipping, address2, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCityInPostEventShipping(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCityInPostEventShipping, city, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setZipcodeInPostEventShipping(String zipcode) throws Exception {
		BrowserActions.typeOnTextField(txtZipcodeInPostEventShipping, zipcode, driver, "First Name in Find Registry");
	}

	/**
	 * to select the state in post event shipping
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInPostEventShipping(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInPostEventShipping, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInPostEventShipping.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setPhoneInPostEventShipping(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneInPostEventShipping, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to check the terms and conditions checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkTermsConditions(String phone) throws Exception {
		BrowserActions.clickOnElement(chkTermsConditions, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to check the register in knot checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkKnotRegister(String phone) throws Exception {
		BrowserActions.clickOnElement(chkKnot, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to check the associate checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkAssociate(String phone) throws Exception {
		BrowserActions.clickOnElement(chkAssociate, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to type the employee id in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setEmployeeId(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtAssociateId, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to type the store id in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setStoreId(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtStoreId, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to get the list of registries in user account
	 * 
	 * @return linked list of registries 'eventname' - 'registryid' -
	 *         'eventtype' - 'date' - 'location' -
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getYourRegistriesTableByRow() throws Exception {
		LinkedList<LinkedHashMap<String, String>> registryTable = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < 5; i++) {
			LinkedHashMap<String, String> registry = new LinkedHashMap<String, String>();

			String eventName = lstYourRegistries.get(i).findElement(By.cssSelector(".event-name")).getText().trim();
			String registryId = lstYourRegistries.get(i).findElement(By.cssSelector(".event-id")).getText().trim();
			String eventType = lstYourRegistries.get(i).findElement(By.cssSelector(".event-type")).getText().trim();
			String date = lstYourRegistries.get(i).findElement(By.cssSelector(".event-date")).getText().trim();
			String location = lstYourRegistries.get(i).findElement(By.cssSelector(".event-name")).getText().trim();

			registry.put("eventname", eventName);
			registry.put("registryid", registryId);
			registry.put("eventtype", eventType);
			registry.put("date", date);
			registry.put("location", location);

			registryTable.add(registry);
		}

		return registryTable;
	}

	/**
	 * to view the registry by id from registry table
	 * 
	 * @param index
	 *            - nth registry
	 * @throws Exception
	 */
	public void viewRegistryByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(lstYourRegistries.get(index - 1), driver, "view button");
		Utils.waitForPageLoad(driver);
	}

	// **********************************************************
	// My Registry Page - Methods
	// **********************************************************

	/**
	 * To open the my registry tab
	 * 
	 * @throws Exception
	 */
	public void clickOnMyRegistryTab() throws Exception {
		BrowserActions.clickOnElement(tabMyRegistry, driver, "My Registry Tab");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To open the event information tab
	 * 
	 * @throws Exception
	 */
	public void clickOnEventInfoTab() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, tabEventInfoForMobile);
			BrowserActions.scrollToViewElement(tabEventInfoForMobile, driver);
			BrowserActions.javascriptClick(tabEventInfoForMobile, driver, "Event Info Tab");
		} else {
			Utils.waitForElement(driver, tabEventInfo);
			BrowserActions.scrollToViewElement(tabEventInfo, driver);
			BrowserActions.javascriptClick(tabEventInfo, driver, "Event Info Tab");
		}

		Utils.waitForPageLoad(driver);
	}

	// **********************************************************
	// MyRegistry Tab
	// **********************************************************

	@FindBy(css = "button[name='dwfrm_giftregistry_setPrivate']")
	WebElement btnMakeThisListPrivate;

	@FindBy(css = ".share-icon.share-facebook")
	WebElement iconFacebook;

	@FindBy(css = ".share-icon.share-twitter")
	WebElement iconTwitter;

	@FindBy(css = ".share-icon.share-googleplus")
	WebElement iconGooglePlus;

	@FindBy(css = ".share-icon.share-pinternet")
	WebElement iconPinterest;

	@FindBy(css = ".share-icon.share-email")
	WebElement iconEmail;

	@FindBy(css = ".share-icon.share-link")
	WebElement iconGetShareLink;

	/**
	 * To click on Make This List Private button
	 * 
	 * @throws Exception
	 */
	public void clickOnMakeThisListPrivate() throws Exception {
		BrowserActions.clickOnElement(btnMakeThisListPrivate, driver, "Make This List Private Button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Facebook Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnFacebookIcon() throws Exception {
		BrowserActions.clickOnElement(iconFacebook, driver, "Facebook Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Twitter Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnTwitterIcon() throws Exception {
		BrowserActions.clickOnElement(iconTwitter, driver, "Twitter Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on GooglePlus Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnGooglePlusIcon() throws Exception {
		BrowserActions.clickOnElement(iconGooglePlus, driver, "GooglePlus Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Pinterest Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnPinterestIcon() throws Exception {
		BrowserActions.clickOnElement(iconPinterest, driver, "Pinterest Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Email Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnEmailIcon() throws Exception {
		BrowserActions.clickOnElement(iconEmail, driver, "Email Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on ShareLink Icon
	 * 
	 * @throws Exception
	 */
	public void clickOnShareLinkIcon() throws Exception {
		BrowserActions.clickOnElement(iconGetShareLink, driver, "ShareLink Icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to select the event type from Unselected event
	 * 
	 * @throws Exception
	 */
	public void selectEventTypeNonSelected(String EventType) throws Exception {
		Utils.waitForElement(driver, selectEventType);
		BrowserActions.javascriptClick(selectEventType, driver, "State Drop down");
		List<WebElement> lstElement = selectEventType.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li:not([class*='selected'])"));
		for (WebElement e : lstElement) {
			if (!(e.getText().trim().equals("Select"))) {

				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.clickOnElement(e, driver, "list elements");
				break;

			}
		}

	}

	public void clickOnViewInRegistry(int registryOrder) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, ViewRegistrylnkForMobile.get(registryOrder - 1));
			BrowserActions.scrollToViewElement(ViewRegistrylnkForMobile.get(registryOrder - 1), driver);
			BrowserActions.javascriptClick(ViewRegistrylnkForMobile.get(registryOrder - 1), driver, "view Registry");

		} else {

			Utils.waitForElement(driver, ViewRegistrylnk.get(registryOrder - 1));
			BrowserActions.scrollToViewElement(ViewRegistrylnk.get(registryOrder - 1), driver);
			BrowserActions.clickOnElement(ViewRegistrylnk.get(registryOrder - 1), driver, "view Registry");
		}
	}

	public void clickOnAddtoBag() throws Exception {

		BrowserActions.clickOnElement(btnAddToBag.get(0), driver, "Add to Bag");
		Utils.waitForPageLoad(driver, 1);
	}

	/**
	 * To mouse hover to mini cart
	 * 
	 * @return ShoppingBagPage
	 * @throws Exception
	 */
	public ShoppingBagPage clickOnMiniCart() throws Exception {
		if (Utils.waitForElement(driver, minicartContent))
			if (minicartContent.getCssValue("display").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To get the upc value of the product by using index
	 */
	public String getUPCInRegistryPage(int index) throws Exception {

		return BrowserActions.getText(driver, lblUpcOfProducts.get(index - 1), "Upc");

	}

	/**
	 * To get the quantity of the product by using index
	 */
	public String getQunatityOfProduct(int index) throws Exception {
		return BrowserActions.getText(driver, txtQuantity.get(index - 1), "Quantity");
	}

	/**
	 * To open the shipping info tab
	 * 
	 * @throws Exception
	 */
	public void clickOnShippingInfoTab() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, tabShippingInfoForMobile);
			BrowserActions.scrollToViewElement(tabShippingInfoForMobile, driver);
			BrowserActions.javascriptClick(tabShippingInfoForMobile, driver, "shipping Info Tab");
		} else {
			Utils.waitForElement(driver, tabShippingInfo);
			BrowserActions.scrollToViewElement(tabShippingInfo, driver);
			BrowserActions.javascriptClick(tabShippingInfo, driver, "Shipping Info Tab");
		}
		Utils.waitForPageLoad(driver);
	}

	public void clickOnApplyButtonInShippingInfo() throws Exception {
		Utils.waitForElement(driver, btnContinueInShippingInFo);
		BrowserActions.scrollToViewElement(btnContinueInShippingInFo, driver);
		BrowserActions.javascriptClick(btnContinueInShippingInFo, driver, "Click Apply");
		Utils.waitForPageLoad(driver);
	}

	public String StockTxt() throws Exception {
		String stockmessage = BrowserActions.getText(driver, txtmessagestock, " stock message");
		Log.event("The Stock message is: " + stockmessage);
		return stockmessage;
	}

	public void clickProductName(int index) throws Exception {
		BrowserActions.clickOnElement(lstProductNames.get(index - 1), driver, "Select " + index + "th product");

	}

	public boolean VerifyNotViewDetails() throws Exception {
		List<WebElement> link = driver.findElements(By.linkText("View Details"));
		if (link.isEmpty() == true)
			return true;
		else
			return false;

	}

	public void clickCancel() throws Exception {
		BrowserActions.clickOnElement(lnkCancel, driver, "Cancel Link");

	}

	public void clickUpdateRegistry() throws Exception {
		BrowserActions.clickOnElement(btnUpdateRegistry, driver, "update Registry Link");
		BrowserActions.nap(5);

	}

	public void selectColorByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(lstColor.get(index - 1), driver, "Color");
		Utils.waitForPageLoad(driver);

	}

	public String ColorTxt() throws Exception {
		String skucolor = BrowserActions.getText(driver, txtcolor, " Sku Color");
		return skucolor;
	}

	public String ColorTxtInEditPopup() throws Exception {
		String skucolor = BrowserActions.getText(driver, txtcolorineditpopup, " Sku Color");
		return skucolor;
	}

	public void removeItemsFromRegigistry() throws Exception {
		BrowserActions.scrollToViewElement(linkRemove, driver);
		BrowserActions.clickOnElement(linkRemove, driver, " Deleting The Item Added to Registry");
	}

	public HomePage clickOnAddItemsToRegistry() throws Exception {

		BrowserActions.clickOnElement(btnAddItemstoRegistry, driver, "Add items to Registry");
		Utils.waitForPageLoad(driver);

		return new HomePage(driver).get();
	}

	public GiftCardsPage clickOnAddGiftCard() throws Exception {

		BrowserActions.clickOnElement(btnAddGiftCard, driver, "Add Gift Cards in registry");
		Utils.waitForPageLoad(driver);

		return new GiftCardsPage(driver).get();
	}

	/**
	 * Get the first name
	 * 
	 * @throws Exception
	 */
	public String getRegistrantFirstName() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtRegistrantFirstName, "value", "First Name in Registry");
	}

	/**
	 * Get the last name
	 * 
	 * @throws Exception
	 */
	public String getRegistrantLastName() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtRegistrantLastName, "value", "Last Name in Registry");
	}

	public String originalPriceTxt() throws Exception {
		String originalsaleprice = BrowserActions.getText(driver, txtOriginalPrice, "originalAndSalePriceTxt");
		return originalsaleprice;
	}

	public String SalePriceTxt() throws Exception {
		String originalsaleprice = BrowserActions.getText(driver, txtSalePrice, "originalAndSalePriceTxt");
		return originalsaleprice;
	}

	/**
	 * To open the purchase tab
	 * 
	 * @throws Exception
	 */
	public void clickOnPurchasesTab() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, tabPurchasesForMobile);
			BrowserActions.scrollToViewElement(tabPurchasesForMobile, driver);
			BrowserActions.javascriptClick(tabPurchasesForMobile, driver, "Event Info Tab");
		} else {
			Utils.waitForElement(driver, tabPurchasesForDesktop);
			BrowserActions.scrollToViewElement(tabPurchasesForDesktop, driver);
			BrowserActions.javascriptClick(tabPurchasesForDesktop, driver, "Purchase Tab");
		}
		Utils.waitForPageLoad(driver);

	}

	public boolean Verifybarcode(String RegisterId) throws Exception {
		boolean status = false;
		String barCode = BrowserActions.getText(driver, txtBarCode, "Bar code");
		String[] barCodeSeparate = barCode.split("(?!^)");
		int barcodeLength = barCodeSeparate.length;

		if (barCode.contains(RegisterId.trim()) && barCodeSeparate[0].equals("4") && barCodeSeparate[1].equals("9")
				&& (!barCodeSeparate[barcodeLength - 1].equals("0"))) {
			status = true;
		}
		return status;
	}

	public String getTextFromRegistryId() throws Exception {
		return BrowserActions.getText(driver, lstRegistryDesc.get(0), "Registry id");
	}

	public String getTextFromBarCode() throws Exception {
		return BrowserActions.getText(driver, txtBarCode, "Bar code");
	}

	public String getTextFromEmptyRegistry() throws Exception {
		return BrowserActions.getText(driver, txtEmptyRegistry, "Empty Registry");

	}

	/*
	 * To get the EventInfo in Find Registry
	 * 
	 * @return String - Event Info
	 * 
	 * @throws Exception
	 */
	public String getEventInfoFromFindRegistry() throws Exception {
		return (BrowserActions.getText(driver, EventInfoInFindRegistry, " Event Info in Find Registry"));
	}

	public int getCurrentLocationOfPdtByIndex(int index) {
		WebElement image = lstProductImages.get(index - 1);
		Point point = image.getLocation();
		return point.getY();

	}

	public int getQtyInPurchaseTab() throws Exception {
		int Qty = Integer
				.parseInt(BrowserActions.getText(driver, noOfQtyInPurchaseTab, "Qty In Purchase Tab").split(":")[1]
						.replace(" ", ""));
		return Qty;
	}

	public String getNameOfUserInPurchaseTab() throws Exception {
		String Name = BrowserActions.getText(driver, nameOfUserInPurchaseTab, "Qty In Purchase Tab").split(":")[1]
				.replace(" ", "");
		return Name;
	}

	public String getDateFormatInPurchaseTab() throws Exception {
		String dateFormat = BrowserActions.getText(driver, dateFormatInPurchaseTab, "Qty In Purchase Tab").split(":")[1]
				.replace(" ", "");
		String day = dateFormat.split("/")[0];
		if (day.length() == 1) {
			day = "0" + day;
		}
		String month = dateFormat.split("/")[1];
		if (month.length() == 1) {
			month = "0" + month;
		}

		String OrgDate = day + "/" + month + "/" + dateFormat.split("/")[2];

		return OrgDate;

	}

	public String getTextOfRegistryId() throws Exception {
		return BrowserActions.getText(driver, txtRegistryId, "Registry id");
	}

	public String getTextOfEventInfomationHeaderForm() throws Exception {
		return BrowserActions.getText(driver, headerEventInformation, "Get text of 'Event Information Form'");

	}

	public String getTextOfShippingHeaderForm() throws Exception {
		return BrowserActions.getText(driver, headerPreEventShipping, "Get text of 'Pre-Event Shipping Form'");
	}

	public String getTextOfPurchaseMsg() throws Exception {
		return BrowserActions.getText(driver, msgPurchase, "Get text of 'Purchase msg'");
	}

	public String getText() throws Exception {
		String txtAddedWishList = BrowserActions.getText(driver, messageNoitem, "Message");
		return txtAddedWishList;
	}

	public String getUPCValue() throws Exception {
		if (Utils.waitForElement(driver, lblUPC))
			return lblUPC.getText().replace("UPC:", "");
		else
			return "UPC: 000000000";
	}

	public String getQunatityOfPrority(int index) throws Exception {
		return BrowserActions.getText(driver, prority, "Quantity");
	}

	public void SelectProrityByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(btnPrority, driver, "Select prority");
		BrowserActions.clickOnElement(lstprority.get(index - 1), driver, "Select prority");

	}
	/*
	 * To verify Cart Registry Icon and Event Name
	 * 
	 * @return true -if it contains an EventName
	 * 
	 * @throws Exception
	 */

	public boolean VerifyCartRegistryIconAndEventName(String EventName) throws Exception {
		boolean returnValue = false;
		for (int i = 0; i < lsttxtGiftItemIcon.size(); i++) {
			String GiftText = BrowserActions.getText(driver, lsttxtGiftItemIcon.get(i), "Gift Icon").trim();
			if (GiftText.equals(EventName)) {
				returnValue = true;
			}
		}
		return returnValue;
	}

	/*
	 * To get Total Registry Items in registry ResultPage
	 * 
	 * @return totalRegistryItems
	 * 
	 * @throws Exception
	 */
	public int getCountOfTotalRegistryItemInRegistryPage() throws Exception {
		int totalRegistryItems = lstOfRegistryItems.size();
		return totalRegistryItems;
	}

	/*
	 * To get Still Needs Quantity Count in registry ResultPage
	 * 
	 * @return StillNeedsCount
	 * 
	 * @throws Exception
	 */

	public int getStillNeedsQtyCount() throws Exception {
		int StillNeedsCount = Integer
				.parseInt(BrowserActions.getText(driver, txtstillNeedCount, "Still Needs Qty Count"));
		return StillNeedsCount;

	}

	public String getRegId(int index) throws Exception{
		
	return BrowserActions.getText(driver, regId.get(index), "registry id value");
	
	}
	
	public boolean VerifyLinkNotPresent(String lnktxt) throws Exception {
		BrowserActions.nap(3);
		List<WebElement> link = driver.findElements(By.partialLinkText(lnktxt));
		if (link.isEmpty() == true)
			return true;
		else
			return false;

	}

	public boolean VerifyProductNameLinkNotPresent(String lnktxt)
			throws Exception {
		BrowserActions.nap(3);
		List<WebElement> link = driver.findElements(By.partialLinkText(lnktxt));
		if (link.size() <= 1)
			return true;
		else
			return false;

	}
	
	public String stillNeedsTxt() throws Exception {
		String dateadded = BrowserActions.getText(driver, txtStillNeeds,
				"Date Added txt");
		return dateadded;
	}


public void clickupdate() throws Exception {

		BrowserActions.clickOnElement(lnkUpdate.get(0), driver, "update link");
		Utils.waitForPageLoad(driver);

	}
	public String getSizeInRegistryPage(int index) throws Exception {

		return BrowserActions.getText(driver, lblSizeOfProducts.get(index - 1),
				"Size");

	}
	public String getColorInRegistryPage(int index) throws Exception {

		return BrowserActions.getText(driver, lsttxtcolor.get(index - 1),
				"Color");

	}
	public LinkedHashMap<String, String> GetProductDetails(int index) throws Exception {
		LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();

		String productName = getProductNametxt(index);
		String color = getColorInRegistryPage(index);
		String size = getSizeInRegistryPage(index);
		
		String upc = getUPCInRegistryPage(index);

		product.put("ProductName", productName);
		product.put("Upc", upc.trim());
		product.put("Color", color);
		product.put("Size", size);
		
		
		return product;
	}


	public String getProductNametxt(int index) throws Exception {

		String productnametxt = BrowserActions.getText(driver,
				lstProductNames.get(index - 1), "Product Name texr");
		return productnametxt;
	}
	/**
	* to get text of msg display on MyRegistry  page
	* @return
	* @throws Exception
	*/
	public String getTextOfMyRegistryMsg() throws Exception{
	BrowserActions.scrollToView(msgNoItemsInRegistry, driver);
	return BrowserActions.getText(driver, msgNoItemsInRegistry , "Get text of 'MyRegistry msg'");
	}
	
	
	public void clickOnMakeThisItem() throws Exception {
		if (makeThisItemPublic.getAttribute("value").contains("Public")) {
			BrowserActions.scrollToViewElement(makeThisItemPublic, driver);
			BrowserActions.clickOnElement(makeThisItemPublic, driver, "click make this item public");
		} else {
			BrowserActions.scrollToViewElement(makeThisItemPublic, driver);
			BrowserActions.clickOnElement(makeThisItemPublic, driver, "click make this item Private");
		}
		Utils.waitForPageLoad(driver);
	}
	public String getBtnItemname() throws Exception {
		return BrowserActions.getText(driver, makeThisItemPublic,
				"Make Item Publc/Private");
	} 
	
	

	/*
	 * To get the DateFormat for Multiple Products In Purchase Tab(Ex: if date
	 * is 1/15/17(m/dd/yy) then it will return 01/15/17(dd/mm/yy))
	 * 
	 * @return String - Product purchase Date
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getFormatOfDateInPurchaseForMultipleProduct()
			throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();
		ArrayList<String> OriginalDate = new ArrayList<>();

		for (WebElement dateFormat : dateFormatInPurchase) {
			lstValues.add(dateFormat.getText().split(":")[1].replace(" ", ""));
		}
		for (int i = 0; i < lstValues.size(); i++) {
			String dateFormat = lstValues.get(i);
			String month = dateFormat.split("/")[0];
			if (month.length() == 1) {
				month = "0" + month;
			}
			String day = dateFormat.split("/")[1];
			if (day.length() == 1) {
				day = "0" + day;
			}
			String year = dateFormat.split("/")[2];
			if (year.length() == 1) {
				year = "0" + year;
			}
			String DateFormatOrg = month + "/" + day + "/" + year;
			OriginalDate.add(DateFormatOrg);
		}
		return OriginalDate;
	}
	
	
	/**
	 * To get the text from event name and event date
	 * @return
	 * @throws Exception
	 */
	public String getTextFromEventNameAndDate() throws Exception
	{
		return BrowserActions.getText(driver, txtEventNameAndDate, "Event date and name");
	}

	/**
	 * To get the text from city and state
	 * @return
	 * @throws Exception
	 */
	public String getTextFromCityAndState() throws Exception
	{
		return BrowserActions.getText(driver, lstRegistryDesc.get(3), "City and state");
	}
	
	/**
	 * To return the day and month in double digit
	 * @param date 1/1/2016
	 * @return - 01/01/2016
	 * @throws Exception
	 */
	public String formateDayAndMonthIntoDoubleDigit(String date) throws Exception
	{
		String day=date.split("/")[0];
		if(day.length()==1){
			day="0"+day;
		}
		String month=date.split("/")[1];
		if(month.length()==1){
			month="0"+month;
		}
		 
		String OrgDate=day+"/"+month+"/"+date.split("/")[2];
		
		return OrgDate;
	}
	/**
	 * To Verify the item is added in registry
	 * @param productNames
	 * @return
	 * @throws Exception
	 */
	public boolean verifyItemAddedToRegistry(List<String> productNames)
			throws Exception {
		boolean status = true;
		String ProductNameInRegistry = null;
		for (int i = 0; i < lstProductNames.size(); i++) {
			BrowserActions.scrollToView(lstProductNames.get(i), driver);
			
			ProductNameInRegistry = BrowserActions.getText(driver,
					lstProductNames.get(i), "Product Name in Registry Page");
			if (!productNames.get(i).equals(ProductNameInRegistry)) {
				status = false;
				break;
			}
		}
		
		return status;
	}
	
	public boolean IsProductNameDisabled(String lnktxt)
			throws Exception {
		BrowserActions.nap(6);
	WebElement element=driver.findElement(By.linkText(lnktxt));
	boolean isdisabled=element.isEnabled();
	return isdisabled;
	}
	
	public void selectSortByDropDownByIndex(int index) {
		Select selectBox = new Select(drpQty.findElement(By.id("Quantity")));
		selectBox.selectByIndex(index - 1);
		
	}
	
	/**
	 * To get the quantity of the product by using index
	 */
	public String getQunatityCountOfProduct() throws Exception {
		return BrowserActions.getText(driver, selectedQty, "Quantity");
	}
}
