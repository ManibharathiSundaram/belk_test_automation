package com.belk.pages.registry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.account.AddressBookPage;
import com.belk.pages.account.BelkRewardsCreditCardPage;
import com.belk.pages.account.EmailPreferencesPage;
import com.belk.pages.account.FAQPage;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.OrderHistoryPage;
import com.belk.pages.account.PaymentMethodsPage;
import com.belk.pages.account.ProfilePage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.AccountUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import java.util.Date;
import java.util.Iterator;

public class RegistrySignedUserPage extends LoadableComponent<RegistrySignedUserPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = "div[class='registry-bottom-actions']>p:nth-child(1)")
	WebElement msgMyRegistry;

	@FindBy(css = "ul[class='menu-utility-user hide-desktop']>li[class='myaccount medium']")
	WebElement lnkmyAccountMob;

	@FindBy(css = "div[class='regitry-list-cont']")
	WebElement registryHtmlContent;

	@FindBy(css = "div#primary h1")
	WebElement lblregistry;

	@FindBy(css = "div[class='regitry-list-cont']>h1")
	WebElement txtMyRegistry;

	@FindBy(css = "button[class='button-text delete-item simple']")
	List<WebElement> btnRemoveItem;

	@FindBy(css = ".registry-bottom-actions>p")
	WebElement msgNoItemsInRegistry;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	@FindBy(css = ".mini-address-location>span")
	WebElement txtSavedAddres;

	// ".product-list-item .name>a"
	@FindBy(css = ".product-list-item .name>a")
	List<WebElement> lstProductNames;

	@FindBy(css = "table>tbody>tr>.event-details>a")
	WebElement ViewRegistrylnk;

	@FindBy(css = "[class='item-dashboard']>a:not([id])")
	WebElement ViewRegistrylnkForMobile;

	@FindBy(css = "table>tbody>tr>td.event-type")
	WebElement textEventType;

	@FindBy(css = "button[name='dwfrm_giftregistry_setPublic']")
	WebElement btnMakeListPublic;

	@FindBy(css = "button[name='dwfrm_giftregistry_setPrivate']")
	WebElement btnMakeListPrivate;
	
	@FindBy(css = ".registry-list-table > table > tbody > tr")
	List<WebElement> lstYourRegistries;
	
	@FindBy(css = "div[class='item-list-device']>div>div[class='event-id']")
	List<WebElement> lstYourRegistriesMobile;

	// ******************Find Registry***********************//

	@FindBy(css = "input[id='dwfrm_giftregistry_search_advancedsearch_registrantFirstName']")
	WebElement txtFirstNameInFindRegistry;

	@FindBy(id = "dwfrm_giftregistry_search_advancedsearch_registrantLastName")
	WebElement txtLastNameInFindRegistry;

	@FindBy(css = ".form-row.eventType-row .selected-option")
	List<WebElement> selectEventTypeInFindRegistry;

	@FindBy(css = "input[id='dwfrm_giftregistry_search_advancedsearch_eventCity']")
	WebElement txtCityInFindRegistry;

	@FindBy(css = "div[class='form-row eventAddress-state-row required'] div[class='custom-select'] div[class='selected-option']")
	WebElement txtStateInFindRegistry;

	@FindBy(css = "div[class='form-row month-row'] div[class='custom-select'] div[class='selected-option']")
	WebElement selectEventDateMonthInFindRegistry;

	@FindBy(css = "div[class='form-row year-row'] div[class='custom-select'] div[class='selected-option']")
	WebElement selectEventDateYearInFindRegistry;

	@FindBy(css = "input[id='dwfrm_giftregistry_search_advancedsearch_eventRegistryID']")
	WebElement txtRegistryIDInFindRegistry;

	@FindBy(css = ".gift-registry-link .advanced-search")
	WebElement lnkAdvancedSearchInFindRegistry;

	@FindBy(css = ".gift-registry-link .simple-search")
	WebElement lnkSimpleSearchInFindRegistry;

	@FindBy(css = ".eventAddress-state-row .selected-option")
	WebElement selectStateInFindRegistry;

	@FindBy(css = ".month-row .selected-option")
	WebElement selectMonthInFindRegistry;

	@FindBy(css = ".year-row .selected-option")
	WebElement selectYearInFindRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_search_search']")
	WebElement btnFindRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_create']")
	WebElement btnCreateNewRegistry;

	/********************************************************************************/
	// Create New Registry Step-1
	/********************************************************************************/

	@FindBy(id = "dwfrm_giftregistry_event_name")
	WebElement txtEventnameInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_town")
	WebElement txtEventCityInCreateNewRegistry;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_type']//following-sibling::div")
	WebElement selectEventTypeInCreateNewRegistry;

	@FindBy(css = ".state-row .selected-option")
	WebElement selectStateInCreateNewRegistry;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_participant_role']//following-sibling::div")
	WebElement selectRegistrantRoleInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_participant_firstName")
	WebElement txtRegistrantFirstNameInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_participant_lastName")
	WebElement txtRegistrantLastNameInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_participant_email")
	WebElement txtRegistrantEmailInCreateNewRegistry;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_coParticipant_role']//following-sibling::div")
	WebElement selectCoRegistrantRoleInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_firstName")
	WebElement txtCoRegistrantFirstNameInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_lastName")
	WebElement txtCoRegistrantLastNameInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_coParticipant_email")
	WebElement txtCoRegistrantEmailInCreateNewRegistry;

	@FindBy(css = "button[name='dwfrm_giftregistry_event_setParticipants']")
	WebElement btnContinueInCreateNewRegistry;

	@FindBy(xpath = "//a[@class='button simple']")
	WebElement btnCancelInCreateNewRegistry;

	public String selectEventtypeInCreateNewRegistry = "#dwfrm_giftregistry_event_type + .selected-option";
	public String txtEventNameInCreateNewRegistry = "input[id='dwfrm_giftregistry_event_name']";
	public String selectEventDateInCreateRegistry = "input[id='dwfrm_giftregistry_event_date']";
	public String selectStateInCreateRegistry = "#dwfrm_giftregistry_event_eventaddress_states_state + .selected-option";
	public String txtCityInCreateRegistry = "input[id='dwfrm_giftregistry_event_town']";
	public String selectRegistryRoleInCreateReigstry = "#dwfrm_giftregistry_event_participant_role + .selected-option";
	public String txtRegistrantFirstNameCreateRegistry = "input[id='dwfrm_giftregistry_event_participant_firstName']";
	public String txtRegistrantLastNameCreateRegistry = "input[id='dwfrm_giftregistry_event_participant_lastName']";
	public String txtRegistrantEmailCreateRegistry = "input[id='dwfrm_giftregistry_event_participant_email']";
	public String selectCoRegistrantRoleInCreateRegistry = "#dwfrm_giftregistry_event_coParticipant_role + .selected-option";
	public String txtCoRegistrantFirstNameInCreateRegistry = "input[id='dwfrm_giftregistry_event_coParticipant_firstName']";
	public String txtCoRegistrantLastNameInCreateRegistry = "input[id='dwfrm_giftregistry_event_coParticipant_lastName']";
	public String txtCoRegistrantEmailInCreateRegistry = "input[id='dwfrm_giftregistry_event_coParticipant_email']";

	/********************************************************************************/
	// Create New Registry Step-2 (PreEvent)
	/********************************************************************************/

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressBeforeList']//following-sibling::div")
	WebElement selectSavedAddressInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_addressid")
	WebElement txtAddressTitleInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")
	WebElement txtFirstNameInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")
	WebElement txtLastNameInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")
	WebElement txtAddress1InPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")
	WebElement txtAddress2InPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")
	WebElement txtCityInPreEventShipping;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state']//following-sibling::div")
	WebElement selectStateInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal")
	WebElement txtZipcodeInPreEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")
	WebElement txtPhoneInPreEventShipping;

	public String txtAddressHome = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_addressid']";
	public String txtFirstNameInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname']";
	public String txtLastNameInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname']";
	public String txtAddressInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1']";
	public String txtCityInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_city']";
	public String selectStateInPreEvent = "#dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state+.selected-option";
	public String txtZipcodeInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal']";
	public String txtPhoneNeInPreEvent = "input[id='dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone']";

	/********************************************************************************/
	// Create New Registry Step-2 (PostEvent)
	/********************************************************************************/

	@FindBy(css = ".usepreevent.secondarybutton")
	WebElement btnUsePreEventAddress;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressAfterList']//following-sibling::div")
	WebElement selectSavedAddressInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_addressid")
	WebElement txtAddressTitleInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname")
	WebElement txtFirstNameInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname")
	WebElement txtLastNameInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")
	WebElement txtAddress1InPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_address2")
	WebElement txtAddress2InPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_city")
	WebElement txtCityInPostEventShipping;

	@FindBy(xpath = "//select[@id='dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state']//following-sibling::div")
	WebElement selectStateInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")
	WebElement txtZipcodeInPostEventShipping;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterEvent_phone")
	WebElement txtPhoneInPostEventShipping;

	@FindBy(css = "button[name='dwfrm_giftregistry_eventaddress_setBeforeAfterAddresses']")
	WebElement btnContinueInEventShipping;

	public String txtAddressHomeInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_addressid']";
	public String txtFirstNameInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname']";
	public String txtLastNameInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname']";
	public String txtAddressInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_address1']";
	public String txtCityInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_city']";
	public String selectStateInPostEvent = "#dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state+.selected-option";
	public String txtZipCodeInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_postal']";
	public String txtPhoneInPostEvent = "input[id='dwfrm_giftregistry_eventaddress_addressAfterEvent_phone']";
	public String checkTermsAndCondition = "input[id='dwfrm_giftregistry_event_terms']";
	public String btnSubmit = "button[name='dwfrm_giftregistry_event_confirm']";

	/******************************************************************************/

	@FindBy(css = "#dwfrm_giftregistry_event > dl")
	List<WebElement> lblEventInfoInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_terms")
	WebElement chkTermsConditionsInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_knot")
	WebElement chkKnotInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_associatehelp")
	WebElement chkAssociateInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_employeeid")
	WebElement txtAssociateIdInCreateNewRegistry;

	@FindBy(id = "dwfrm_giftregistry_event_storeid")
	WebElement txtStoreIdInCreateNewRegistry;

	/******************************************************************************/
	// Registry Table
	/******************************************************************************/
	
	@FindBy(css = "div[id='dialog-container']>p")
	WebElement txtCopyMessage;

	@FindBy(css = "table[id='registry-results']>tbody>tr")
	public List<WebElement> lstRegistries;

	@FindBy(css = "th[class='eventname']")
	WebElement lblEventName;

	@FindBy(css = "th[class='eventtype']")
	WebElement lblEventType;

	@FindBy(css = "th[class='eventdate']")
	WebElement lblEventDate;

	@FindBy(css = "td[class='eventname']>div>div")
	List<WebElement> txtEventNames;

	@FindBy(css = "td[class='eventtype']")
	List<WebElement> txtEventType;

	@FindBy(css = "td[class='eventdate']")
	List<WebElement> txtEventDates;

	@FindBy(css = ".button.delete-registry")
	WebElement btnYesInDeleteRegistry;

	@FindBy(css = ".cancel.close-dialog.cancel-button.button.simple")
	WebElement btnCancelInDeleteRegistry;

	@FindBy(css = "#dwfrm_giftregistry_event>fieldset>h2")
	WebElement txtRegistrantHeading;

	@FindBy(css = ".coregistrant-optional>h2")
	WebElement txtCoRegistrantHeading;

	@FindBy(css = ".selected-option")
	WebElement drpRoleType;

	@FindBy(css = "#dwfrm_giftregistry_event_participant_firstName")
	WebElement txtBoxFirstName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[2]/div[4]/label/span[2]")
	WebElement txtLastNameRegistry;

	@FindBy(css = "#dwfrm_giftregistry_event_participant_lastName")
	WebElement txtBoxLastName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[2]/div[5]/label/span[2]")
	WebElement txtEmailInForm;

	@FindBy(css = "#dwfrm_giftregistry_event_participant_email")
	WebElement txtBoxEmailInForm;

	@FindBy(css = "#dwfrm_giftregistry_event_type-error")
	WebElement errorMessageEventtype;

	@FindBy(css = "#dwfrm_giftregistry_event_name-error")
	WebElement errorMessageEventFirstName;

	@FindBy(css = "#dwfrm_giftregistry_event_date-error")
	WebElement errorMessageEventLastName;

	@FindBy(css = "#dwfrm_giftregistry_event_town-error")
	WebElement errorMessageEventCity;

	@FindBy(css = "#dwfrm_giftregistry_event_eventaddress_states_state-error")
	WebElement errorMessageState;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event_participant_role-error']")
	WebElement errorMessageRoleField;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event_participant_firstName-error']")
	WebElement errorMessageFirstRoleName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event_participant_lastName-error']")
	WebElement errorMessageLastRoleName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event_participant_email-error']")
	WebElement errorMessageEmailId;

	@FindBy(css = "div[class='error global-error hide']")
	WebElement txtGlobalMessage;

	@FindBy(css = ".regitry-list-cont>p>span>span")
	WebElement fldHelpContentAsset;

	@FindBy(css = "div[class='regitry-list-cont']>p>span")
	WebElement fldRegistryBanner;

	@FindBy(css = ".product-list-item .name")
	WebElement lblProductNme;

	@FindBy(css = "input[id='dwfrm_profile_address_addressid']")
	WebElement txtAddressNameInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_firstname']")
	WebElement txtFirstNameInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_address1']")
	WebElement txtAddressOneInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_address2']")
	WebElement txtAddressTwoInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_city']")
	WebElement txtCityInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_postal']")
	WebElement txtZipCodeInDelAddress;

	@FindBy(css = "div[class='form-row state-row required']>div>div>div")
	WebElement drpStateInDelAddress;

	@FindBy(css = "input[id='dwfrm_profile_address_phone']")
	WebElement txtPhoneInDelAddress;

	@FindBy(css = "button[name='dwfrm_profile_address_create']")
	WebElement btnSaveInDelAddress;

	@FindBy(css = "button[name='dwfrm_profile_address_cancel']")
	WebElement txtCancelInDelAddress;

	@FindBy(css = "div[id='dialog-container']>p")
	WebElement lblResourceMessage;

	@FindBy(css = "div[class='form-row savedaddress-list']>div>div>div")
	WebElement drpSaveAddressInDelAddress;

	/******************************************************************************/

	@FindBy(css = ".product-list-item")
	List<WebElement> lstProducts;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry']/div[1]/fieldset/div[2]/div/div/div/div")
	WebElement drpDownClickOnSavedAddress;

	@FindBy(css = "ul[class='selection-list']")
	public List<WebElement> lstAdd;

	@FindBy(css = ".print-page")
	WebElement iconPrint;

	@FindBy(css = ".button.additemstoregistry")
	WebElement btnAddToRegistry;

	@FindBy(css = ".registry-bottom-actions>p")
	WebElement lblNoItemsInRegistry;

	@FindBy(css = ".createregistry.giftregistry_content.editregistry>p")
	WebElement registryBannerSlot;

	@FindBy(css = "#secondary > nav > div > div > h3 > a")
	WebElement lnkAccount;

	/*****************************************************************************************************************/
	@FindBy(css = "button.menu-toggle.sprite")
	WebElement lnkHamburger;

	@FindBy(css = ".menu-active")
	WebElement islnkHamburgerOpened;

	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	@FindBy(css = ".content-asset.hide-mobile>h3>span>span")
	WebElement bannerHelpContent;

	@FindBy(css = ".content-asset>p>span>span")
	WebElement bannerHelpContentMobile;

	@FindBy(css = ".selected-option")
	WebElement dropDownForSavedAddress;

	@FindBy(css = "#dwfrm_giftregistry > div.address-before > fieldset > div.select-address > div > div > div > ul > li")
	WebElement DropdownAddress;

	@FindBy(css = ".selection-list")
	WebElement selectlist;

	@FindBy(css = ".tooltip")
	WebElement tooltipAPO;

	@FindBy(css = "#dwfrm_giftregistry > div.address-before > fieldset > div.form-row.phone-row.required > div.form-field-tooltip > a")
	WebElement tooltipPhone;

	@FindBy(css = "#dwfrm_giftregistry > div.address-after > h2")
	WebElement postEvent;

	@FindBy(css = "#dwfrm_giftregistry > div.address-after > p")
	WebElement postEventMessage;

	@FindBy(css = "#dwfrm_giftregistry > div.address-after > fieldset > div.form-row.form-row-button > a")
	WebElement previousBtn;

	@FindBy(xpath = ".//*[@id='secondary']/nav/div/div[1]/ul/li[3]/a")
	WebElement lnkAddressBookLeftNav;

	@FindBy(css = "#primary > div")
	WebElement step3CreateRegistry;

	@FindBy(css = "#primary > div > p:nth-child(3)")
	WebElement messageDisplayed;

	@FindBy(css = ".error.global-error.hide")
	WebElement errorMsg;

	@FindBy(css = "#dwfrm_giftregistry_event_terms")
	WebElement chkReadAndAgree;

	@FindBy(css = "#dwfrm_giftregistry_event_knot")
	WebElement chkTheKnot;

	@FindBy(css = ".registryId")
	WebElement lblRegistryConfirmation;

	@FindBy(css = "#dwfrm_giftregistry > div > fieldset > span.tabregistry-head.nav-head.selected > button")
	WebElement tabMyRegistry;

	@FindBy(css = "#dwfrm_giftregistry > div > fieldset > span.tabevent-head.nav-head > button")
	WebElement tabEvent;

	@FindBy(css = "#dwfrm_giftregistry > div > fieldset > span.tabshipping-head.nav-head > button")
	WebElement tabShipping;

	@FindBy(css = "#dwfrm_giftregistry > div > fieldset > span.tabpurchases-head.nav-head > button")
	WebElement tabPurchase;

	@FindBy(css = "h1[class='registry-setps']")
	WebElement txtRegistryStep2;

	// @FindBy(xpath= ".//*[@id='primary']/div/div[1]")
	// WebElement lblregistry;

	@FindBy(css = "div[class='page-content-tab-wrapper'] div[class='address-before']>p")
	WebElement txtResourceMessageOnPreShipping;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[2]/div[2]/label/span[2]")
	WebElement txtRole;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[1]/div[4]/label/span[2]")
	WebElement txtEventName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[1]/div[5]/label/span[2]")
	WebElement txtEventDate;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[1]/div[6]/label/span[2]")
	WebElement txtEventCity;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event_town']")
	WebElement txtBoxEventCity;

	@FindBy(css = ".selected-option")
	WebElement drpStateOptions;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[1]/div[7]/label/span[2]")
	WebElement txtHeadingState;

	@FindBy(css = ".registry-setps")
	WebElement txtCreateNewRegistry;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry_event']/fieldset[1]/div[3]/label/span[2]")
	WebElement txtEventTypeOnEventForm;

	@FindBy(css = ".selected-option")
	WebElement lstDrpDownOptions;

	@FindBy(css = "div[class='pdpgftmessage hide success']")
	WebElement txtSuccessMessage;

	//[class*='gftmessage']
	@FindBy(css = "[class*='gftmessage']")
	WebElement txtItemAddedRegistry;
	
	@FindBy(css = ".registry-setps>span")
	WebElement txtPreEventShippingPage;

	@FindBy(css = "div[class='createregistry step1 '] h1[class='registry-setps']")
	WebElement txtCreateRegistryStep1;

	@FindBy(css = "span[id*='dwfrm_giftregistry_event_town-error']")
	WebElement lblMandatory;
	@FindBy(css = "div[class='breadcrumb']")
	WebElement lblBreadCrumb;

	@FindBy(css = ".registry-list-table>p")
	WebElement txtNoRegistryMsg;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_type']//following-sibling::div")
	WebElement selectEventType;

	@FindBy(css = ".registry-list-table ")
	WebElement YourRegistries;

	@FindBy(css = ".content-asset>p>span")
	WebElement registryContentSlot;

	@FindBy(css = ".content-asset>p>span")
	WebElement registryContentSlotInCreteRegistry;


	@FindBy(css = ".breadcrumb-element")
	List<WebElement> lstBreadcrumb;

	@FindBy(css = "form[name='dwfrm_giftregistry_event']")
	WebElement EventInfo;

	@FindBy(css = "div[class='need-data']")
	WebElement HelpContentSlot;

	@FindBy(css = ".registry-list-table p")
	WebElement msgNoRegitryFound;

	@FindBy(css = ".selection-list>li")
	List<WebElement> lstWouldLove;

	@FindBy(css = "div[class*='selected-option']")
	WebElement drpWouldLove;

	@FindBy(css = ".button.additemstoregistry")
	WebElement btnAddItemsToRegistry;
	@FindBy(css = "div[class='selected-option']")
	WebElement drpPriority;
	@FindBy(css = "div:not([class*='last'])>.sort-by .selection-list li:not(.selected)")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "div[class='custom-select current_item']>ul[class='selection-list']>li")
	List<WebElement> lstPriorityOptions;

	@FindBy(css = ".button-text.update-item.simple")
	List<WebElement> lnkUpdate;
	@FindBy(css = ".button-text.delete-item.simple")
	List<WebElement> lnkRemove;
	@FindBy(css = "fieldset>div[class='item-option item-qty-text']>span[class='value']>div[class='custom-select']>ul[class='selection-list']>li")
	List<WebElement> lstWouldLov;

	@FindBy(css = ".registry-setps.registry-setps-edit")
	WebElement yourRegistryGlobalContent;

	@FindBy(css = ".event-details>a")
	WebElement viewLinkInYourRegistry;

	@FindBy(css = ".list-share>button")
	WebElement btnMakeTheItemPublic;

	@FindBy(css = "	div [id='primary']>div>form>div>fieldset>span>button[name='dwfrm_giftregistry_navigation_navPurchases']")
	WebElement purchaseLink;

	@FindBy(css = ".name>a")
	WebElement getProductName;

	@FindBy(xpath = ".//*[@id='dwfrm_giftregistry']/div/fieldset/span[3]/button")
	WebElement shippingInfoTab;// .page-content-tab-navigaton.top-nav>fieldset>span[class='tabshipping-head
								// nav-head']>button

	@FindBy(css = "#dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal")
	WebElement getZipCodePost;

	@FindBy(css = "# dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")
	WebElement getZipCodePre;

	@FindBy(css = "	.item-details>div[class='product-list-item']>div[class='item-edit-details']>a[name='0438522594538']")
	WebElement editLink;

	@FindBy(xpath = ".//*[@id='primary']/div/div[2]/fieldset/table/tbody/tr[1]/td[3]/form/div[6]/button[2]")
	WebElement removeItem;

	@FindBy(xpath = ".//*[@id='primary']/div/div[2]/fieldset/table/tbody/tr[1]/td[3]/form/div[7]/fieldset/div[1]/span[2]/div/div")
	WebElement qtyChange;

	@FindBy(css = ".col-1>h2")
	WebElement DatenNameInRegistryHeader;

	@FindBy(css = ".page-content-tab-navigaton.top-nav>fieldset>span>button[name='dwfrm_giftregistry_navigation_navEvent']")
	WebElement eventTab;

	@FindBy(css = "div [class='registry-list-table']>table>tbody>tr>td>div")
	WebElement deleteButtonRegistryPage;

	@FindBy(css = ".item-dashboard>a:not([class='viewitems'])")
	WebElement deleteButtonRegistryPageMobile;

	@FindBy(css = "div [class='secondary-navigation']>div>ul>li:nth-child(5)>a")
	WebElement registryLinkSidePanel;

	@FindBy(css = ".button.additemstoregistry")
	WebElement addItemToRegistry;

	@FindBy(css = ".registry-setps>span")
	WebElement registryGlobalContent;

	@FindBy(css = ".regitry-list-cont>h1")
	WebElement myRegistryGlobalContent;

	@FindBy(css = ".cancel.close-dialog.cancel-button.button.simple")
	WebElement cancelButtonInPopUp;

	@FindBy(css = ".button.delete-registry")
	WebElement YesButtonInPopUp;

	@FindBy(css = "form[id='ChangePassowrdForm']>fieldset>div[class='form-row form-row-button']>button[name='dwfrm_profile_changepassword']")
	WebElement btnApplyChanges;

	// ************************************Shipping info tab
	// ********************************************************//

	@FindBy(css = "fieldset>div>button.usepreevent.secondarybutton")
	WebElement UsePreEventAddressButton;

	@FindBy(css = "fieldset[ name='address-before']>div>div>div>div>div")
	WebElement dropdownForPreShipping;

	@FindBy(css = "fieldset[ name='address-before']>div[class='form-row city-row required']>div>a")
	WebElement ApoFpolnkInPreEvent;

	@FindBy(css = "fieldset[ name='address-after']>div[class='form-row city-row required']>div>a")
	WebElement ApoFpolnkInPostEvent;

	@FindBy(css = "fieldset[ name='address-before']>div[class='form-row phone-row required']>div>a")
	WebElement PhoneAssestlnkInPreEvent;

	@FindBy(css = "fieldset[ name='address-after']>div[class='form-row phone-row required']>div>a")
	WebElement PhoneAssestlnkInPostEvent;

	@FindBy(css = "fieldset[ name='address-before']>div[class='form-row city-row required']>div>a[aria-describedby]")
	WebElement ApoFpoToolTipForPreEvent;

	@FindBy(css = "fieldset[ name='address-before']>div[class='form-row phone-row required']>div>a[aria-describedby]")
	WebElement PhoneToolTipForPreEvent;

	@FindBy(css = "fieldset[ name='address-after']>div[class='form-row city-row required']>div>a[aria-describedby]")
	WebElement ApoFpoToolTipForPostEvent;

	@FindBy(css = "fieldset[ name='address-after']>div[class='form-row phone-row required']>div>a[aria-describedby]")
	WebElement PhoneToolTipForPostEvent;

	@FindBy(css = ".registryId")
	WebElement registryId;

	@FindBy(css = ".col-2>div")
	WebElement registryBarCode;

	@FindBy(css = "div [id='secondary']>div>div>div")
	WebElement accountContent;

	@FindBy(xpath = ".//*[@id='header']/div[4]/div[1]/ul[2]/li[2]/a")
	WebElement btnMyAccount;

	@FindBy(xpath = ".//*[@id='primary']/div/div/ul/li[1]/ul/li[3]/a/h2")
	WebElement btnAddressbook;

	@FindBy(css = "div [id='main']>div>div>form>div >fieldset>span>button")
	List<WebElement> lstOfTab;

	@FindBy(css = ".print-page-cont>a")
	WebElement PrintIcon;

	@FindBy(css = "div[class='option-date-added item-option']>div[class='value']")
	WebElement txtDateAdded;

	@FindBy(css = "fieldset>div[class='item-option item-qty-text']>span[class='value']>div[class='custom-select']>div[class='selected-option selected']")
	WebElement drpQty;

	@FindBy(css = "#dwfrm_giftregistry_event > div.form-row.form-row-button > a")
	WebElement btnPreviousInStep3;

	@FindBy(css = "#dwfrm_giftregistry_event_associatehelp")
	WebElement chkSalesAssociate;

	@FindBy(css = "#dwfrm_giftregistry_event_employeeid-error")
	WebElement errorMsgforEmpId;

	@FindBy(css = "#dwfrm_giftregistry_event_storeid-error")
	WebElement errorMsgForStoreId;

	@FindBy(css = "#dwfrm_giftregistry_event_employeeid")
	WebElement txtEmpId;

	@FindBy(css = "#dwfrm_giftregistry_event_storeid")
	WebElement txtStoreId;

	@FindBy(css = "#dwfrm_giftregistry_event > div.registry-terms > div.form-row.terms-condition-row.required.label-inline.form-indent > label > span:nth-child(2)")
	WebElement lblReadAndAgree;

	@FindBy(css = "#dwfrm_giftregistry_event > div.error.global-error.hide")
	WebElement warningMsgForTermsConditions;

	@FindBy(css = ".event-view>a")
	WebElement viewLinkInFindRegistry;

	@FindBy(css = ".registry-user-info>div[class='event-info']>ul[class='user']")
	WebElement registryDetailsOnFindRegistry;

	@FindBy(css = ".gift-registry-empty-message")
	WebElement productDetailsOnFindRegistry;

	@FindBy(css = ".share-icon share-facebook")
	WebElement facebookLinkOnFindRegistry;

	@FindBy(css = ".share-icon share-twitter")
	WebElement twitterLinkOnFindRegistry;

	@FindBy(css = ".share-icon share-googleplus")
	WebElement googleplusLinkOnFindRegistry;

	@FindBy(css = ".share-icon share-pinternet")
	WebElement pinternetLinkOnFindRegistry;

	@FindBy(css = "share-icon share-email")
	WebElement emailLinkOnFindRegistry;

	@FindBy(css = ".share-icon share-link")
	WebElement shareLinkOnFindRegistry;

	@FindBy(css = ".item-edit-details")
	WebElement itemEditDetails;

	@FindBy(css = ".addgiftcert.secondarybutton.button")
	WebElement addGiftCards;

	@FindBy(css = "#dwfrm_giftregistry a.button.additemstoregistry")
	WebElement btnaddItemsToRegistry;

	@FindBy(css = "#primary div.page-content-tab-wrapper table tbody")
	List<WebElement> lstYourRegisteredItems;

	@FindBy(css = "div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.giftregistry-view.quick-view")
	WebElement Editdetailsmodal;

	@FindBy(css = "#product-content div.product-variations li.attribute.color div.label span")
	WebElement lblProductColorinModal;

	@FindBy(css = ".share-option.secondarybutton")
	WebElement btnMakeThisListPrivate;

	@FindBy(css = "#dwfrm_giftregistry_event > fieldset:nth-child(2) > div.section-header")
	WebElement lblRegistrationInfoInEventInfo;

	@FindBy(css = "#main > div.hide-mobile > div > h1 > a[title='Event Shipping']")
	WebElement lblShipping;

	@FindBy(css = "#main > div.hide-mobile > div > h1 > a[title='Purchases']")
	WebElement lblPurchases;

	@FindBy(css = ".page-content-tab-navigaton.top-nav>fieldset>span[class='tabevent-head nav-head']")
	WebElement eventTabMobile;

	@FindBy(css = ".tabshipping-head.nav-head>button")
	WebElement shippingInfoTabMobile;

	@FindBy(css = ".tabpurchases-head.nav-head>button")
	WebElement purchaseInfoTabMobile;

	@FindBy(css = ".address-before>h2")
	WebElement lblPreEventShipping;

	@FindBy(css = ".item-list.gift-reg-purchases>tbody>tr>td")
	WebElement warningMsgInPurchase;

	@FindBy(css = "div.page-content-tab-navigaton.bottom-nav.hide-desktop fieldset span.tabshipping-head.nav-head button")
	WebElement mobileShippingInfoTab;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressBeforeList")
	WebElement SelectSavedAddressInPreEvent;

	@FindBy(id = "dwfrm_giftregistry_eventaddress_addressAfterList")
	WebElement SelectSavedAddressInPostEvent;

	@FindBy(css = ".event-view>a")
	List<WebElement> viewLinkInFindRegistryList;

	@FindBy(css = ".registry-list-table>table")
	WebElement lstYourRegistriesView;

	@FindBy(css = ".selection-list>li")
	List<WebElement> dropDownTextSavedAddess;

	@FindBy(css = "div[class='address-before'] div[class='section-header']")
	WebElement txtSelectAddAddressPreEvent;

	@FindBy(css = "div[class='address-after'] div[class='section-header']")
	WebElement txtSelectAddAddressPostEvent;

	@FindBy(css = "div[class='select-address']>div>label")
	WebElement txtSelectSavedAddress;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname']")
	WebElement lblFirstNamePreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname']")
	WebElement lblLastNamePreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1']")
	WebElement lblAddressPreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_city'")
	WebElement lblCityPreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state']")
	WebElement lblStatePreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal']")
	WebElement lblPostalPreEvent;

	@FindBy(css = "label[for='dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone']")
	WebElement lblPhoneNoPreEvent;

	@FindBy(css = ".form-row>label[for='dwfrm_giftregistry_event_coParticipant_email']")
	WebElement lblEmail;

	@FindBy(css = ".event-location")
	WebElement viewEventLocation;

	@FindBy(css = "div[class='createregistry step3'] h1")
	WebElement txtstep3InCreateRegistry;

	@FindBy(id = "footer")
	WebElement footerPanel;

	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lblBreadCrumbMobile;

	@FindBy(css = ".form-row.option-priority.item-option>label>span")
	WebElement prioritylbl;

	@FindBy(css = ".item-dashboard>form>div[class='item-option option-add-to-cart']>fieldset>div>span[class='label']")
	WebElement Qtylbl;

	@FindBy(css = ".button-fancy-small.add-to-cart")
	WebElement btnadd;

	@FindBy(css = ".item-edit-details>a")
	WebElement btnEdit;

	@FindBy(css = "#dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")
	WebElement preAddressInShippingInfo;

	@FindBy(css = "#dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")
	WebElement preCityInShippingInfo;

	@FindBy(css = "#dwfrm_giftregistry_eventaddress_addressAfterEvent_city")
	WebElement postCityInShippingInfo;

	@FindBy(css = "#dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")
	WebElement postaddressInShippingInfo;

	@FindBy(css = ".item-list.gift-reg-purchases>tbody>tr>td")
	WebElement purchaseBag;

	@FindBy(css = ".button-text.delete-item.simple")
	WebElement btnRemove;

	@FindBy(css = ".form-row.form-row-button>button")
	WebElement btnApply;
	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabregistry-head.nav-head>button")
	WebElement tabMyRegistryDesktop;

	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabevent-head.nav-head>button")
	WebElement tabEventInfoDesktop;

	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabshipping-head.nav-head>button")
	WebElement tabShippingInfoDesktop;

	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabpurchases-head.nav-head>button")
	WebElement tabPurchasesDesktop;

	@FindBy(css = ".page-content-tab-navigaton.top-nav .tabregistry-head.nav-head>button")
	WebElement tabMyRegistryMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabevent-head.nav-head>button")
	WebElement tabEventInfoMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabshipping-head.nav-head>button")
	WebElement tabShippingInfoMobile;

	@FindBy(css = ".page-content-tab-navigaton.bottom-nav .tabpurchases-head.nav-head>button")
	WebElement tabPurchasesMobile;
	
	@FindBy(css= "button[title='Add to Shopping Bag']")
	WebElement btnAddtoShoppingBag;
	
	@FindBy(css = "#ui-id-1")
	WebElement modalDeleteRegistry; 

	//"button[class='registry-select button-secondary']"
	



	// **********************************************************************************************///

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public RegistrySignedUserPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lblregistry))) {
			Log.fail("Header Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver);

	}

	public ShoppingBagPage clickMiniCart() throws Exception {
		BrowserActions.clickOnElement(miniCart, driver, "Clicking on MiniCart Icon");
		return new ShoppingBagPage(driver).get();
	}

	public boolean checkItemAddedToRegistry(String productName) throws Exception {
		boolean status = false;
		String ProductNameInRegistry = null;
		for (int i = 0; i < lstProductNames.size(); i++) {
			BrowserActions.scrollToView(lstProductNames.get(i), driver);
			ProductNameInRegistry = BrowserActions.getText(driver, lstProductNames.get(i),
					"Product Name in Registry Page");
			if (productName.contains(ProductNameInRegistry)) {
				status = true;
				break;
			}
		}

		return status;
	}

	/**
	 * To click Remove Item In whishList
	 * 
	 * @throws Exception
	 */
	public boolean clickDeleteItem() throws Exception {
		boolean status = false;
		String deletedProductName = BrowserActions.getText(driver, lstProductNames.get(0), "Get deleted product Name");
		BrowserActions.scrollToViewElement(btnRemoveItem.get(0), driver);
		BrowserActions.clickOnElement(btnRemoveItem.get(0), driver, " Deleting The Item Added to WishList");

		if (lstProductNames.size() > 0) {
			for (int i = 0; i < lstProductNames.size(); i++) {
				String productName = BrowserActions.getText(driver, lstProductNames.get(i), "Get Product Name");

				if (!productName.equals(deletedProductName)) {
					Log.message("Product is deleted from registry");
					status = true;
					break;
				}
			}
		} else {
			Log.message("No Products found in registry since products are deleted");
			status = true;
		}
		return status;

	}

	/*
	 * @return txtNOItemsInWishList *
	 * 
	 * @throws Exception
	 */
	public String getTextFromNoItemsInRegistry() throws Exception {
		BrowserActions.scrollToViewElement(msgNoItemsInRegistry, driver);
		String txtAddedWishList = BrowserActions.getText(driver, msgNoItemsInRegistry,
				" In Registry  No Items message");
		return txtAddedWishList;
	}

	/**
	 * to type the first name in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstNameInFindRegistry(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInFindRegistry, firstName, driver, "First Name in Find Registry");
	}

	/**
	 * to type the last name in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setLastNameInFindRegistry(String lastName) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInFindRegistry, lastName, driver, "First Name in Find Registry");
	}

	/**
	 * to select the event type in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectEventTypeInFindRegistry(String eventType) throws Exception {
		BrowserActions.javascriptClick(selectEventTypeInFindRegistry.get(0), driver, "Event type Drop down");
		List<WebElement> lstElement = selectEventTypeInFindRegistry.get(0).findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(eventType)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the city in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCityInFindRegistry(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCityInFindRegistry, city, driver, "First Name in Find Registry");
	}

	/**
	 * to type the registry id in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegIdInFindRegistry(String city) throws Exception {
		BrowserActions.typeOnTextField(txtRegistryIDInFindRegistry, city, driver, "First Name in Find Registry");
	}

	/**
	 * to select the month in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectMonthInFindRegistry(String month) throws Exception {
		BrowserActions.javascriptClick(selectMonthInFindRegistry, driver, "Month Drop down");
		List<WebElement> lstElement = selectMonthInFindRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(month)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the year in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectYearInFindRegistry(String year) throws Exception {
		BrowserActions.javascriptClick(selectYearInFindRegistry, driver, "Year Drop down");
		List<WebElement> lstElement = selectYearInFindRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(year)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the state in the advanced search registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInFindRegistry(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInFindRegistry, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInFindRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * To click on Advanced Search Link in Find Registry
	 * 
	 * @throws Exception
	 */
	public void clickOnAdvancedSearch() throws Exception {
		BrowserActions.clickOnElement(lnkAdvancedSearchInFindRegistry, driver, "Advanced Search link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Simple Search Link in Find Registry
	 * 
	 * @throws Exception
	 */
	public void clickOnSimpleSearch() throws Exception {
		BrowserActions.clickOnElement(lnkSimpleSearchInFindRegistry, driver, "Advanced Search link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Find Registry Button in Find Registry
	 * 
	 * @throws Exception
	 */
	public void clickOnFindRegistry() throws Exception {
		BrowserActions.clickOnElement(btnFindRegistry, driver, "Advanced Search link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on create new Registry Button in Find Registry
	 * 
	 * @throws Exception
	 */
	public void clickOnCreateNewRegistry() throws Exception {
		BrowserActions.clickOnElement(btnCreateNewRegistry, driver, "Advanced Search link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to type the event name in add new registry
	 * 
	 * @param event
	 *            name
	 * @throws Exception
	 */
	public void setEventNameInNewRegistry(String eventname) throws Exception {
		BrowserActions.typeOnTextField(txtEventnameInCreateNewRegistry, eventname, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the event city in add new registry
	 * 
	 * @param event
	 *            name
	 * @throws Exception
	 */
	public void setEventCityInNewRegistry(String city) throws Exception {
		BrowserActions.typeOnTextField(txtEventCityInCreateNewRegistry, city, driver, "First Name in Find Registry");
	}

	/**
	 * to select the event type in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectEventTypeInCreateRegistry(String eventtype) throws Exception {
		BrowserActions.javascriptClick(selectEventTypeInCreateNewRegistry, driver, "State Drop down");
		List<WebElement> lstElement = selectEventTypeInCreateNewRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(eventtype)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the state in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInCreateRegistry(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInCreateNewRegistry, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInCreateNewRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to select the role in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectRegistrantRoleInCreateRegistry(String role) throws Exception {
		BrowserActions.javascriptClick(selectRegistrantRoleInCreateNewRegistry, driver, "State Drop down");
		List<WebElement> lstElement = selectRegistrantRoleInCreateNewRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(role)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantFirstNameInCreateRegistry(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantFirstNameInCreateNewRegistry, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantLastNameInCreateRegistry(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantLastNameInCreateNewRegistry, lastname, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the email in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setRegistrantEmailInCreateRegistry(String email) throws Exception {
		BrowserActions.typeOnTextField(txtRegistrantEmailInCreateNewRegistry, email, driver,
				"First Name in Find Registry");
	}

	/**
	 * to select the role in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectCoRegistrantRoleInCreateRegistry(String role) throws Exception {
		BrowserActions.javascriptClick(selectCoRegistrantRoleInCreateNewRegistry, driver, "State Drop down");
		List<WebElement> lstElement = selectCoRegistrantRoleInCreateNewRegistry.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(role)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantFirstNameInCreateRegistry(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantFirstNameInCreateNewRegistry, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantLastNameInCreateRegistry(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantLastNameInCreateNewRegistry, lastname, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the email in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCoRegistrantEmailInCreateRegistry(String email) throws Exception {
		BrowserActions.typeOnTextField(txtCoRegistrantEmailInCreateNewRegistry, email, driver,
				"First Name in Find Registry");
	}

	/**
	 * to click on the continue button in the create new registry
	 * 
	 * @throws Exception
	 */
	public void clickContinueInCreateRegistry() throws Exception {
		BrowserActions.clickOnElement(lblEmail, driver, "click on label Email");
		BrowserActions.javascriptClick(btnContinueInCreateNewRegistry, driver,
				"Continue button in Create New Registry");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to click on the cancel button in the create new registry
	 * 
	 * @throws Exception
	 */
	public void clickCancelInCreateRegistry() throws Exception {
		BrowserActions.clickOnElement(btnCancelInCreateNewRegistry, driver, "Continue button in Create New Registry");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to type the Address Title in the pre event shipping in create new
	 * registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddressTitleInPreEventShipping(String addressName) throws Exception {
		BrowserActions.typeOnTextField(txtAddressTitleInPreEventShipping, addressName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstNameInPreEventShipping(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInPreEventShipping, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setLastNameInPreEventShipping(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInPreEventShipping, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddress1InPreEventShipping(String address1) throws Exception {
		BrowserActions.typeOnTextField(txtAddress1InPreEventShipping, address1, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setaddress2InPreEventShipping(String address2) throws Exception {
		BrowserActions.typeOnTextField(txtAddress2InPreEventShipping, address2, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCityInPreEventShipping(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCityInPreEventShipping, city, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setZipcodeInPreEventShipping(String zipcode) throws Exception {
		BrowserActions.typeOnTextField(txtZipcodeInPreEventShipping, zipcode, driver, "First Name in Find Registry");
	}

	/**
	 * to select the state in pre event shipping in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInPreEventShipping(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInPreEventShipping, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInPreEventShipping.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the pre event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setPhoneInPreEventShipping(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneInPreEventShipping, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to type the Address Title in the post event shipping in create new
	 * registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddressTitleInPostEventShipping(String addressName) throws Exception {
		BrowserActions.typeOnTextField(txtAddressTitleInPostEventShipping, addressName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstNameInPostEventShipping(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInPostEventShipping, firstName, driver,
				"First Name in Find Registry");
	}

	/**
	 * to type the last name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setLastNameInPostEventShipping(String lastname) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInPostEventShipping, lastname, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setAddress1InPostEventShipping(String address1) throws Exception {
		BrowserActions.typeOnTextField(txtAddress1InPostEventShipping, address1, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setaddress2InPostEventShipping(String address2) throws Exception {
		BrowserActions.typeOnTextField(txtAddress2InPostEventShipping, address2, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setCityInPostEventShipping(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCityInPostEventShipping, city, driver, "First Name in Find Registry");
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setZipcodeInPostEventShipping(String zipcode) throws Exception {
		BrowserActions.typeOnTextField(txtZipcodeInPostEventShipping, zipcode, driver, "First Name in Find Registry");
	}

	/**
	 * to select the state in post event shipping in the create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void selectStateInPostEventShipping(String state) throws Exception {
		BrowserActions.javascriptClick(selectStateInPostEventShipping, driver, "State Drop down");
		List<WebElement> lstElement = selectStateInPostEventShipping.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * to type the first name in the post event shipping in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setPhoneInPostEventShipping(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneInPostEventShipping, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to check the terms and conditions checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkTermsConditionsInCreateNewRegistry(String phone) throws Exception {
		BrowserActions.clickOnElement(chkTermsConditionsInCreateNewRegistry, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to check the register in knot checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkKnotRegisterInCreateNewRegistry(String phone) throws Exception {
		BrowserActions.clickOnElement(chkKnotInCreateNewRegistry, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to check the associate checkbox in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void chkAssociateInCreateNewRegistry(String phone) throws Exception {
		BrowserActions.clickOnElement(chkAssociateInCreateNewRegistry, driver, "Terms & Conditions checkbox");
	}

	/**
	 * to type the employee id in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setEmployeeIdInCreateNewRegistry(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtAssociateIdInCreateNewRegistry, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to type the store id in create new registry
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setStoreIdInCreateNewRegistry(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtStoreIdInCreateNewRegistry, phone, driver, "First Name in Find Registry");
	}

	/**
	 * to get all the entered details in step-1, step-2 from step-3
	 * 
	 * @return LinkedHashMap 'eventtype' - 'eventname' - 'eventdate' -
	 *         'eventlocation' - 'eventregistrant' - 'eventcoregistrant' -
	 *         'preeventshipping' - 'posteventshipping' - 'chktermsconditions' -
	 *         'chkregforknot' - 'chkassociate' -
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getRegistryInformationInCreateNewRegistry() throws Exception {
		LinkedHashMap<String, String> registryInfo = new LinkedHashMap<String, String>();

		String eventType = lblEventInfoInCreateNewRegistry.get(0).findElement(By.cssSelector("dd")).getText().trim();
		String eventName = lblEventInfoInCreateNewRegistry.get(1).findElement(By.cssSelector("dd")).getText().trim();
		String eventDate = lblEventInfoInCreateNewRegistry.get(2).findElement(By.cssSelector("dd")).getText().trim();
		String eventLocation = lblEventInfoInCreateNewRegistry.get(3).findElement(By.cssSelector("dd")).getText()
				.trim();
		String eventRegistrant = lblEventInfoInCreateNewRegistry.get(4).findElement(By.cssSelector("dd")).getText()
				.trim();
		String eventCoRegistrant = lblEventInfoInCreateNewRegistry.get(5).findElement(By.cssSelector("dd")).getText()
				.trim();
		String eventPreEveShippingAddr = lblEventInfoInCreateNewRegistry.get(6).findElement(By.cssSelector("dd"))
				.getText().trim();
		String eventPostEveShippingAddr = lblEventInfoInCreateNewRegistry.get(7).findElement(By.cssSelector("dd"))
				.getText().trim();
		/*
		 * String chkTermsAndConditions = chkTermsConditionsInCreateNewRegistry
		 * .getAttribute("checked").equals("checked") ? "YES" : "NO"; String
		 * chkRegForTheKnot = chkKnotInCreateNewRegistry.getAttribute(
		 * "checked").equals("checked") ? "YES" : "NO"; String chkassociate =
		 * chkAssociateInCreateNewRegistry.getAttribute(
		 * "checked").equals("checked") ? "YES" : "NO";
		 */

		registryInfo.put("eventtype", eventType);
		registryInfo.put("eventname", eventName);
		registryInfo.put("eventdate", eventDate);
		registryInfo.put("eventlocation", eventLocation);
		registryInfo.put("eventregistrant", eventRegistrant);
		registryInfo.put("eventcoregistrant", eventCoRegistrant);
		registryInfo.put("preeventshipping", eventPreEveShippingAddr);
		registryInfo.put("posteventshipping", eventPostEveShippingAddr);
		/*
		 * registryInfo.put("chktermsconditions", chkTermsAndConditions);
		 * registryInfo.put("chkregforknot", chkRegForTheKnot);
		 * registryInfo.put("chkassociate", chkassociate);
		 */

		return registryInfo;
	}

	/**
	 * to get the list of registries in user account
	 * 
	 * @return linked list of registries 'eventname' - 'registryid' -
	 *         'eventtype' - 'date' - 'location' -
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getYourRegistriesTableByRow() throws Exception {
		LinkedList<LinkedHashMap<String, String>> registryTable = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < 5; i++) {
			LinkedHashMap<String, String> registry = new LinkedHashMap<String, String>();

			String eventName = lstYourRegistries.get(i).findElement(By.cssSelector(".event-name")).getText().trim();
			String registryId = lstYourRegistries.get(i).findElement(By.cssSelector(".event-id")).getText().trim();
			String eventType = lstYourRegistries.get(i).findElement(By.cssSelector(".event-type")).getText().trim();
			String date = lstYourRegistries.get(i).findElement(By.cssSelector(".event-date")).getText().trim();
			String location = lstYourRegistries.get(i).findElement(By.cssSelector(".event-name")).getText().trim();

			registry.put("eventname", eventName);
			registry.put("registryid", registryId);
			registry.put("eventtype", eventType);
			registry.put("date", date);
			registry.put("location", location);

			registryTable.add(registry);
		}

		return registryTable;
	}

	/**
	 * to view the registry by id from registry table
	 * 
	 * @param index
	 *            - nth registry
	 * @throws Exception
	 */
	public RegistryInformationPage viewRegistryByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(
				lstYourRegistries.get(index - 1).findElement(By.cssSelector(".event-details a[title='View']")), driver,
				"view button");
		Utils.waitForPageLoad(driver);
		return new RegistryInformationPage(driver).get();
	}

	/**
	 * to delete the registry
	 * 
	 * @param index
	 *            - nth registry
	 * @throws Exception
	 */
	public void deleteRegistryByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(
				lstYourRegistries.get(index - 1).findElement(By.cssSelector(".address-delete.delete")), driver,
				"view button");
		Utils.waitForPageLoad(driver);
		if (modalDeleteRegistry.getCssValue("display").equals("block")) {
			BrowserActions.clickOnElement(btnYesInDeleteRegistry, driver, "Yes Button in Delete Registry");
		} else {
			throw new Exception("Confirm Delete Registry Modal Not Opened!");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * filling details in the create New registry page
	 * 
	 * @param registryInfo
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingEventDetails(String registryInfo) throws Exception {
		LinkedHashMap<String, String> registryDetils = new LinkedHashMap<String, String>();
		String registry = checkoutProperty.getProperty(registryInfo);
		String eventType = registry.split("\\|")[0];
		String eventDate = registry.split("\\|")[1];
		String city = registry.split("\\|")[2];
		String state = registry.split("\\|")[3];
		String role = registry.split("\\|")[4];
		String email = registry.split("\\|")[5];

		// filling Event Information
		registryDetils.put("select_eventType_" + eventType, selectEventtypeInCreateNewRegistry);
		String randomEventName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_eventName_event_" + randomEventName, txtEventNameInCreateNewRegistry);
		registryDetils.put("pickdate_eventDate_" + eventDate, selectEventDateInCreateRegistry);
		registryDetils.put("type_city_" + city, txtCityInCreateRegistry);
		registryDetils.put("select_state_" + state, selectStateInCreateRegistry);

		// filling registrant Information
		registryDetils.put("select_registrantrole_" + role, selectRegistryRoleInCreateReigstry);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_reFirstName_re" + randomFirstName, txtRegistrantFirstNameCreateRegistry);
		registryDetils.put("type_reLastName_re" + randomLastName, txtRegistrantLastNameCreateRegistry);
		registryDetils.put("type_registrantemail_" + email, txtRegistrantEmailCreateRegistry);

		// filling co-registrant Information
		registryDetils.put("select_coregistrantrole_" + role, selectCoRegistrantRoleInCreateRegistry);
		registryDetils.put("type_coFirstName_co" + randomFirstName, txtCoRegistrantFirstNameInCreateRegistry);
		registryDetils.put("type_coLastName_co" + randomLastName, txtCoRegistrantLastNameInCreateRegistry);
		registryDetils.put("type_coregistrantemail_" + email, txtCoRegistrantEmailInCreateRegistry);

		AccountUtils.doAccountOperations(registryDetils, driver);
		Log.event("Filled Registry Details");
		return registryDetils;
	}

	/**
	 * Click Continue button
	 * 
	 * @throws Exception
	 */
	public void clickOnContinueButton() throws Exception {
		BrowserActions.clickOnElement(btnContinueInCreateNewRegistry, driver, "Clicked continue button");
	}

	public LinkedHashMap<String, String> fillingPreEventShippingdetails(String shippingInfo) throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String shippingAddress = checkoutProperty.getProperty(shippingInfo);
		String addressHome = shippingAddress.split("\\|")[0];
		String address = shippingAddress.split("\\|")[1];
		String city = shippingAddress.split("\\|")[2];
		String state = shippingAddress.split("\\|")[3];
		String zipcode = shippingAddress.split("\\|")[4];
		String phone = shippingAddress.split("\\|")[5];

		shippingDetails.put("type_preaddressname_" + addressHome, txtAddressHome);
		shippingDetails.put("type_prefirstName_qa" + randomFirstName, txtFirstNameInPreEvent);
		shippingDetails.put("type_prelastName_qa" + randomLastName, txtLastNameInPreEvent);
		shippingDetails.put("type_preaddress_" + address, txtAddressInPreEvent);
		shippingDetails.put("type_precity_" + city, txtCityInPreEvent);
		shippingDetails.put("select_prestate_" + state, selectStateInPreEvent);
		shippingDetails.put("type_prezipCode_" + zipcode, txtZipcodeInPreEvent);
		shippingDetails.put("type_prephoneNo_" + phone, txtPhoneNeInPreEvent);
		BrowserActions.nap(3);
		AccountUtils.doAccountOperations(shippingDetails, driver);
		Log.event("Filled Pre event Details");
		return shippingDetails;
	}

	/**
	 * filling event shipping details
	 * 
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingPostEventShippingdetails(String shippingInfo) throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String shippingAddress = checkoutProperty.getProperty(shippingInfo);
		String addressHome = shippingAddress.split("\\|")[0];
		String address = shippingAddress.split("\\|")[1];
		String city = shippingAddress.split("\\|")[2];
		String state = shippingAddress.split("\\|")[3];
		String zipcode = shippingAddress.split("\\|")[4];
		String phone = shippingAddress.split("\\|")[5];

		// filling post event shipping details
		shippingDetails.put("type_postaddressname_" + addressHome, txtAddressHomeInPostEvent);
		shippingDetails.put("type_postfirstName_qa" + randomFirstName, txtFirstNameInPostEvent);
		shippingDetails.put("type_postlastName_qa" + randomLastName, txtLastNameInPostEvent);
		shippingDetails.put("type_postaddress_" + address, txtAddressInPostEvent);
		shippingDetails.put("type_postcity_" + city, txtCityInPostEvent);
		shippingDetails.put("select_poststate_" + state, selectStateInPostEvent);
		shippingDetails.put("type_postzipCode_" + zipcode, txtZipCodeInPostEvent);
		shippingDetails.put("type_postphoneNo_" + phone, txtPhoneInPostEvent);
		BrowserActions.nap(3);
		AccountUtils.doAccountOperations(shippingDetails, driver);
		Log.event("Filled Post Event Details");
		return shippingDetails;
	}

	/**
	 * Click Continue button
	 * 
	 * @throws Exception
	 */
	public void clickOnContinueButtonInEventShipping() throws Exception {
		BrowserActions.clickOnElement(btnContinueInEventShipping, driver, "Clicked continue button in Event Shipping");
	}

	public LinkedHashMap<String, String> clickOnSubmitButton() throws Exception {
		LinkedHashMap<String, String> registryDetails = new LinkedHashMap<String, String>();
		registryDetails.put("check_termsAndCondition_YES", checkTermsAndCondition);
		registryDetails.put("click_submit", btnSubmit);
		AccountUtils.doAccountOperations(registryDetails, driver);
		Log.event("Filled Registry Details");
		return registryDetails;

	}

	/**
	 * Click view active Registry button
	 * 
	 * @throws Exception
	 */

	public RegistryInformationPage clickOnViewRegistrylnk() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, ViewRegistrylnkForMobile);
			BrowserActions.scrollToViewElement(ViewRegistrylnkForMobile, driver);
			BrowserActions.javascriptClick(ViewRegistrylnkForMobile, driver, "view Registry");
		} else {
			Utils.waitForElement(driver, ViewRegistrylnk);
			BrowserActions.scrollToViewElement(ViewRegistrylnk, driver);
			BrowserActions.clickOnElement(ViewRegistrylnk, driver, "view Registry");
		}

		return new RegistryInformationPage(driver).get();
	}

	/**
	 * Get event type in Active registry
	 * 
	 * @throws Exception
	 */

	public String getEventTypeInMyRegistry() throws Exception {
		Utils.waitForElement(driver, textEventType);
		String eventType = BrowserActions.getText(driver, textEventType, "Event Type");
		return eventType;
	}

	/**
	 * findRegistry
	 * 
	 * @param firstName
	 * @param LastName
	 * @param eventType
	 * @throws Exception
	 */
	public void findRegistry(String firstName, String LastName, String eventType) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInFindRegistry, firstName, driver, "Entered first name");
		BrowserActions.typeOnTextField(txtLastNameInFindRegistry, LastName, driver, "Entered last name");
		selectEventTypeInFindRegistry(eventType);
		BrowserActions.clickOnElement(btnFindRegistry, driver, "Clicked find a registry button");
	}

	public void clickOnPrintIcon() throws Exception {
		BrowserActions.clickOnElement(iconPrint, driver, "Print Icon");
	}

	public void clickOnAddItemsToRegistry() throws Exception {
		BrowserActions.clickOnElement(btnAddToRegistry, driver, "Add Items To Registry");
		BrowserActions.nap(2);
	}

	public String getNoItemsInRegistryMessage() throws Exception {
		return BrowserActions.getText(driver, lblNoItemsInRegistry, "No Items in Registry message");
	}

	public String getMyAccountInfo() throws Exception {
		Log.message("My account et" + BrowserActions.getText(driver, lnkAccount, "My Account"));
		return BrowserActions.getText(driver, lnkAccount, "My Account");
	}

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver, "Profile Link");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver, "Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile, driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver, "Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver, "Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver, "Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver, "Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver, "WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver, "WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver, "Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver, "Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}

	public boolean verifyProductAddedInRegistry(String productAddedInPDP) throws Exception {
		boolean status = false;
		if (lstProductNames.size() > 0) {
			for (int i = 0; i < lstProductNames.size(); i++) {
				String productName = BrowserActions.getText(driver, lstProductNames.get(i), "Get Product Name");
				if (productName.equals(productAddedInPDP)) {
					status = true;
					break;
				}
			}
		}
		return status;
	}

	public void clickOnSavedAddress() throws Exception {
		dropDownForSavedAddress.click();

	}

	public boolean verifyDropdown() throws Exception {
		if (Utils.waitForElement(driver, selectlist)) {
			return true;
		}
		return false;
	}

	public boolean MouseoverAPO() throws Exception {
		if (Utils.waitForElement(driver, tooltipAPO)) {
			Actions toolTip = new Actions(driver);
			toolTip.moveToElement(tooltipAPO).build().perform();
			BrowserActions.nap(2);
			return true;

		}
		return false;
	}

	public boolean MouseoverPhoneNumber() throws Exception {
		if (Utils.waitForElement(driver, tooltipPhone)) {
			Actions toolTip = new Actions(driver);
			toolTip.moveToElement(tooltipPhone).build().perform();
			BrowserActions.nap(2);
			return true;

		}
		return false;
	}

	public boolean verifyPostEvent() throws Exception {
		if (Utils.waitForElement(driver, postEvent)) {
			return true;
		}

		return false;
	}

	public boolean verifyPostEventMessage() throws Exception {
		if (Utils.waitForElement(driver, postEventMessage)) {
			if (BrowserActions.getText(driver, postEventMessage, "Message in Post event")
					.contains("This address is where your registry gifts will be sent after your event.")) {
				return true;

			}
		}

		return false;
	}

	public void clickOnPrevious() throws Exception {
		BrowserActions.clickOnElement(previousBtn, driver, "Clicked on Previous Button");
	}

	public LinkedHashMap<String, String> getValuesInShipping() throws Exception {
		LinkedHashMap<String, String> registryInfo = new LinkedHashMap<String, String>();

		String AddresName = BrowserActions.getTextFromAttribute(driver, txtAddressHome, "value", "Address ");
		String randomFirstName = BrowserActions.getTextFromAttribute(driver, txtFirstNameInPreEvent, "value",
				"txtFirstNameInPreEvent");
		String randomLastName = BrowserActions.getTextFromAttribute(driver, txtLastNameInPreEvent, "value",
				"txtLastNameInPreEvent");
		String address = BrowserActions.getTextFromAttribute(driver, txtAddressInPreEvent, "value", "address");
		String city = BrowserActions.getTextFromAttribute(driver, txtCityInPreEvent, "value", "txtCityInPreEvent");
		// String state=BrowserActions.getTextFromAttribute(driver,
		// selectStateInPreEvent,"value", "selectStateInPreEvent");
		String zipcode = BrowserActions.getTextFromAttribute(driver, txtZipcodeInPreEvent, "value",
				"txtZipcodeInPreEvent");
		String phone = BrowserActions.getTextFromAttribute(driver, txtPhoneNeInPreEvent, "value",
				"txtPhoneNeInPreEvent");

		registryInfo.put("AddresName", AddresName);
		registryInfo.put("randomFirstName", randomFirstName);
		registryInfo.put("randomLastName", randomLastName);
		registryInfo.put("address", address);
		registryInfo.put("city", city);
		// registryInfo.put("state", state);
		registryInfo.put("phone", phone);
		registryInfo.put("zipcode", zipcode);

		return registryInfo;

	}

	public boolean verifyMessage() throws Exception {
		if ((BrowserActions.getText(driver, messageDisplayed, "Message verification")).contains("Submit")) {
			return true;
		}
		return false;

	}

	public void ClickonSubmitToverifyErrorMsgInStep3() throws Exception {
		BrowserActions.clickOnElement(btnSubmit, driver, "Click on Submit");

	}

	public boolean verifyErrorMessage() throws Exception {
		if ((BrowserActions.getText(driver, errorMsg, " Error Message verification"))
				.contains("before creating a registry")) {
			return true;
		}
		return false;

	}

	public void clickOnCheckboxInStep3() throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkTheKnot, "YES");
	}

	/**
	 * Pre Shipping Create Registry Pre Shipping Address Heading
	 * 
	 * @return
	 * @throws Exception
	 */

	public String preShippingCreateRegistryHeading() throws Exception {
		String createRegistryHeadingStep2 = BrowserActions.getText(driver, txtRegistryStep2, "Create Registry Step 2");
		return createRegistryHeadingStep2;
	}

	/**
	 * Pre Shipping Resource Message
	 * 
	 * @return
	 * @throws Exception
	 */
	public String preShippingText() throws Exception {
		String resourceMessage = BrowserActions.getText(driver, txtResourceMessageOnPreShipping,
				"Resource Message On the Pre Shipping Event Page");
		return resourceMessage;
	}

	/**
	 * Pre Shipping Step 2 Create Registry heading
	 * 
	 * @return
	 * @throws Exception
	 */

	public String txtregistryCreateHeading() throws Exception {
		String registryHeading = BrowserActions.getText(driver, txtCreateNewRegistry, "Create Registry Heading");
		return registryHeading;
	}

	/**
	 * To fetch the success message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String registryLoginAddSuccessMessage() throws Exception {

		String successTxtMessageofProduct = BrowserActions.getText(driver, txtSuccessMessage,
				"Success Message when the product added ");
		return successTxtMessageofProduct;

	}
	
	public String gettxtitemAddedRegistry() throws Exception {
		
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, txtItemAddedRegistry, "text Success message that item added to registry");
		
	}

	/**
	 * To get the Pre Event SHipping Text
	 * 
	 * @throws Exception
	 */
	public String preEventShippingText() throws Exception {
		String preEventHeading = BrowserActions.getText(driver, txtPreEventShippingPage,
				"Pre Event Shipping Heading after clicking on the continue button");
		return preEventHeading;
	}

	/**
	 * To click on the event type
	 * 
	 * @throws Exception
	 */
	public void drpGetTxtListValue() throws Exception {
		BrowserActions.clickOnElement(lstDrpDownOptions, driver, "Click on the Drop down box");
	}

	/**
	 * To get the drop down Registry options
	 * 
	 * @throws Exception
	 */
	public String getAlltheDropDownValues() throws Exception {

		String labelRegistryValue = driver.findElement(By.id("dwfrm_giftregistry_event_type")).getText();
		// BrowserActions.clickOnElement(lstDrpDownOptions, driver,
		// "Click on the Drop down box");
		Utils.waitForPageLoad(driver);
		return labelRegistryValue;
	}

	/**
	 * To click on the drop down box of the State
	 * 
	 * @throws Exception
	 */
	public void drpStateBox() throws Exception {
		BrowserActions.clickOnElement(drpStateOptions, driver, "Select the State Drop down box");
	}

	/**
	 * To get the state drop down values including APO and FPO
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getAlltheDropDownStateValues() throws Exception {

		String labelRegistrySateValue = driver.findElement(By.id("dwfrm_giftregistry_event_eventaddress_states_state"))
				.getText();
		Utils.waitForPageLoad(driver);
		return labelRegistrySateValue;
	}

	public String getTextFromBreadCrumb() throws Exception {
		return lblBreadCrumb.getText().trim();
		// return BrowserActions.getText(driver, lblBreadCrumb,
		// "Text from breadcrumb");
	}

	public String gettextFromSelectedEventinDropDown() throws Exception {
		String text = null;
		text = BrowserActions.getText(driver, selectEventTypeInFindRegistry.get(0), "elementDescription");
		return text;
	}

	/**
	 * To get the details of pre-event shipping
	 * 
	 * @throws Exception
	 */

	public LinkedHashMap<String, String> getPreEventShippingDetails() throws Exception {
		BrowserActions.nap(5);
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String addressHome = BrowserActions.getTextFromAttribute(driver, txtAddressHome, "value", "Home Address");
		String firstName = BrowserActions.getTextFromAttribute(driver, txtFirstNameInPreEvent, "value",
				"First Name in Shipping Address");
		String lastName = BrowserActions.getTextFromAttribute(driver, txtLastNameInPreEvent, "value",
				"First Name in Shipping Address");
		String address = BrowserActions.getTextFromAttribute(driver, txtAddressInPreEvent, "value",
				"Address in Shipping Address");

		String city = BrowserActions.getTextFromAttribute(driver, txtCityInPreEvent, "value",
				"City in Shipping Address");
		String state = BrowserActions.getText(driver, selectStateInPreEvent, "City in Shipping Address");
		String phoneNo = BrowserActions.getTextFromAttribute(driver, txtPhoneNeInPreEvent, "value",
				"Phone No in Shipping Address");
		String zipCode = BrowserActions.getTextFromAttribute(driver, txtZipcodeInPreEvent, "value",
				"Phone No in Shipping Address");

		shippingDetails.put("AddressHome", addressHome);
		shippingDetails.put("FirstName", firstName);
		shippingDetails.put("LastName", lastName);
		shippingDetails.put("Address", address);
		shippingDetails.put("City", city);
		shippingDetails.put("State", state);
		shippingDetails.put("PhoneNo", phoneNo);
		shippingDetails.put("Zipcode", zipCode);

		return shippingDetails;

	}

	/**
	 * To get the details of post-event shipping
	 * 
	 * @throws Exception
	 */

	public LinkedHashMap<String, String> getPostEventShippingDetails() throws Exception {
		BrowserActions.nap(5);

		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String addressHome = BrowserActions.getTextFromAttribute(driver, txtAddressHomeInPostEvent, "value",
				"Home Address");
		String firstName = BrowserActions.getTextFromAttribute(driver, txtFirstNameInPostEvent, "value",
				"First Name in Shipping Address");
		String lastName = BrowserActions.getTextFromAttribute(driver, txtLastNameInPostEvent, "value",
				"First Name in Shipping Address");
		String address = BrowserActions.getTextFromAttribute(driver, txtAddressInPostEvent, "value",
				"Address in Shipping Address");

		String city = BrowserActions.getTextFromAttribute(driver, txtCityInPostEvent, "value",
				"City in Shipping Address");
		String state = BrowserActions.getText(driver, selectStateInPostEvent, "City in Shipping Address");
		String phoneNo = BrowserActions.getTextFromAttribute(driver, txtPhoneInPostEvent, "value",
				"Phone No in Shipping Address");
		String zipCode = BrowserActions.getTextFromAttribute(driver, txtZipCodeInPostEvent, "value",
				"Phone No in Shipping Address");

		shippingDetails.put("AddressHome", addressHome);
		shippingDetails.put("FirstName", firstName);
		shippingDetails.put("LastName", lastName);
		shippingDetails.put("Address", address);
		shippingDetails.put("City", city);
		shippingDetails.put("State", state);
		shippingDetails.put("PhoneNo", phoneNo);
		shippingDetails.put("Zipcode", zipCode);

		return shippingDetails;

	}

	public String getTextFromRegistry() throws Exception {
		return BrowserActions.getText(driver, txtNoRegistryMsg, "No Registry Meassage");
	}

	public boolean verifyFirstNameLastNameInFindRegistry(String searchType) throws Exception {
		boolean status = false;
		WebElement element = null;
		if (searchType.equals("Simple Search")) {
			element = lnkSimpleSearchInFindRegistry;
		} else if (searchType.equals("Advanced Search")) {
			element = lnkAdvancedSearchInFindRegistry;
		}
		WebElement firstName = element.findElement(By.xpath("../.."))
				.findElement(By.cssSelector("#dwfrm_giftregistry_search_advancedsearch_registrantFirstName"));
		WebElement lastName = element.findElement(By.xpath("../.."))
				.findElement(By.cssSelector("#dwfrm_giftregistry_search_advancedsearch_registrantLastName"));
		if (Utils.waitForElement(driver, firstName) && Utils.waitForElement(driver, lastName)) {
			status = true;
		}
		return status;
	}

	/**
	 * To get the last value of the breadcrumb
	 * 
	 * @return String - Last value in the breadcrumb
	 * @throws Exception
	 */
	public String getBreadCrumbLastValue() throws Exception {
		return (BrowserActions.getText(driver, lstBreadcrumb.get(lstBreadcrumb.size() - 1), "breadcrumb value"));
	}

	/**
	 * To get the No registries found message
	 * 
	 * @return String - Last value in the breadcrumb
	 * @throws Exception
	 */
	public String getNoRegistryFoundMsg() throws Exception {
		return (BrowserActions.getText(driver, msgNoRegitryFound, "No Registries Found Message"));
	}

	public String getWouldLoveSelectedvalue() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(5);
		return BrowserActions.getText(driver, drpWouldLove, " Upadated Quantity in would Love drop down");
	}

	public int getWouldLoveCount() throws Exception {
		return lstWouldLov.size();
	}

	public int selectWouldLoveIndex(int index) throws Exception {
		clickWouldLoveDropown();
		BrowserActions.clickOnElement(lstWouldLove.get(index - 1), driver, "wouldLoveSelection");
		Utils.waitForPageLoad(driver);
		return index;

	}

	public void clickWouldLoveDropown() throws Exception {
		BrowserActions.nap(3);
		BrowserActions.scrollToView(drpWouldLove, driver);
		BrowserActions.clickOnElement(drpWouldLove, driver, "Size dropdown");
	}

	public void clickAddItemsToRegistry() throws Exception {

		BrowserActions.clickOnElement(btnAddItemsToRegistry, driver, "AddItemsToRegistry button");
	}

	public RegistryInformationPage viewRegistryByIndexMobile(int index) throws Exception {
		BrowserActions.clickOnElement(".viewitems", driver, "view button");
		Utils.waitForPageLoad(driver);
		return new RegistryInformationPage(driver).get();
	}

	public String getPrioritySelectedvalue() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(5);
		return BrowserActions.getText(driver, drpPriority, " Upadated value in Priority drop down");
	}

	public ArrayList<String> getPriorityList() throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();

		BrowserActions.clickOnElement(drpPriority, driver, "Priority dropdown");

		for (WebElement element : lstPriorityOptions) {

			if (!element.getText().equals(""))

				lstValues.add(element.getText().trim());
		}
		BrowserActions.clickOnElement(drpPriority, driver, "Priority dropdown");
		return lstValues;
	}

	public String getTextFromYourRegistryHeader() throws Exception {
		String header = BrowserActions.getText(driver, yourRegistryGlobalContent, "Global Content In Create Registry");
		return header;
	}

	public void clickOnViewlink() throws Exception {
		BrowserActions.clickOnElement(viewLinkInYourRegistry, driver, "View link");
	}

	public String getTextFromFindRegistry() throws Exception {
		String header = BrowserActions.getText(driver, yourRegistryGlobalContent, "Global Content In Create Registry");
		return header;
	}

	public void clickOnMakeItemPublicButton() throws Exception {
		BrowserActions.clickOnElement(btnMakeTheItemPublic, driver, "Make Item Public");
	}

	public void clickOnAddItemToRegistry() throws Exception {
		BrowserActions.clickOnElement(addItemToRegistry, driver, "Add item To Registry");
	}

	public void clickOnPurchaseHeader() throws Exception {
		BrowserActions.clickOnElement(purchaseLink, driver, "Purchase Header");
	}



	public void clickOnShippingInfo() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.scrollToView(shippingInfoTab, driver);
			BrowserActions.clickOnElement(shippingInfoTab, driver, "Click on ShippingInfo tab");
		} else if (Utils.getRunPlatForm() == "mobile") {
			BrowserActions.mouseHover(driver, mobileShippingInfoTab);
			BrowserActions.clickOnElement(mobileShippingInfoTab, driver, "Click on ShippingInfo tab");
		}
		Utils.waitForPageLoad(driver);

	}

	public String getTextFromPreField() throws Exception {
		String Pr = BrowserActions.getText(driver, getZipCodePost, "ZipCode in pre");
		return Pr;
	}

	public void clickOnEventInfo() throws Exception {
		clickOnMyRegistry();
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.clickOnElement(eventTab, driver, "Event Info");
			BrowserActions.nap(5);
		} else {
			BrowserActions.scrollToViewElement(eventTabMobile, driver);
			BrowserActions.nap(3);
			BrowserActions.clickOnElement(eventTabMobile, driver, "Event Info");
			BrowserActions.nap(5);
		}
	}

	/*
	 * public void clickOnApplyEventInfo() throws Exception {
	 * //BrowserActions.clickOnElement(btnApplyInEventInfo, driver,
	 * "Purchase Header"); }
	 */

	public void clickOnDelete() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.mouseHover(driver, deleteButtonRegistryPage);
			BrowserActions.clickOnElement(deleteButtonRegistryPage, driver, "Continue button in shipping");
		} else if (Utils.getRunPlatForm() == "mobile") {
			BrowserActions.mouseHover(driver, deleteButtonRegistryPageMobile);
			BrowserActions.clickOnElement(deleteButtonRegistryPageMobile, driver, "Continue button in shipping");
		}
		Utils.waitForPageLoad(driver);
	}

	public void clickOnRegistrylink() throws Exception {
		BrowserActions.clickOnElement(registryLinkSidePanel, driver, " Clicking Cancel On Pop Up");
	}

	public String getTextFromMyRegistryHeader() throws Exception {
		String header = BrowserActions.getText(driver, myRegistryGlobalContent, "Global Content In Create Registry");
		return header;
	}

	public String getTextFromGlobalRegistryHeader() throws Exception {
		String header = BrowserActions.getText(driver, registryGlobalContent, "Global Content In Create Registry");
		return header;
	}

	public void clickOnYesInPopUp() throws Exception {
		BrowserActions.clickOnElement(YesButtonInPopUp, driver, " Clicking Cancel On Pop Up");
	}

	public void clickOnCancel() throws Exception {
		BrowserActions.clickOnElement(cancelButtonInPopUp, driver, " Clicking Cancel On Pop Up");
	}

	/**********
	 * Click on Button To make list private or public
	 */
	public void clickedOnButton() throws Exception {
		if (btnMakeListPublic.getText().contains("Public")) {
			BrowserActions.clickOnElement(btnMakeListPublic, driver,
					"Clicked  'MakeListPublic' button in Event Shipping");
			Utils.waitForPageLoad(driver);
		} else if (btnMakeListPrivate.getText().contains("Private")) {
			BrowserActions.clickOnElement(btnMakeListPrivate, driver,
					"Clicked  'MakeListPrivate' button in Event Shipping");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To click on 'Use Pre Event Address' button .
	 * 
	 * @throws Exception
	 */
	public void clickOnUsePreEventAddress() throws Exception {
		Utils.waitForElement(driver, UsePreEventAddressButton);
		BrowserActions.clickOnElement(UsePreEventAddressButton, driver, "Pre Event Address Button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To MouseHover on APO/FPO link in pre-event shipping
	 * 
	 * @throws Exception
	 */

	public void hoverOnApoFpolnkInPreEvent() {
		Utils.waitForElement(driver, ApoFpolnkInPreEvent);
		BrowserActions.scrollToViewElement(ApoFpolnkInPreEvent, driver);
		Actions actions = new Actions(driver);
		actions.moveToElement(ApoFpolnkInPreEvent).build().perform();
		if (!Utils.waitForElement(driver, ApoFpoToolTipForPreEvent)) {
			actions.moveToElement(ApoFpolnkInPreEvent).build().perform();
		}
	}

	/**
	 * To MouseHover on phoneAssest link in pre-event shipping
	 * 
	 * @throws Exception
	 */

	public void hoverOnPhoneAssestlnkInPreEvent() {
		Actions actions = new Actions(driver);
		Utils.waitForElement(driver, PhoneAssestlnkInPreEvent);
		BrowserActions.scrollToViewElement(PhoneAssestlnkInPreEvent, driver);
		actions.moveToElement(PhoneAssestlnkInPreEvent).build().perform();
		if (!Utils.waitForElement(driver, PhoneToolTipForPreEvent)) {
			actions.moveToElement(PhoneAssestlnkInPreEvent).build().perform();
		}
	}

	/**
	 * To MouseHover on APO/FPO link in post-event shipping
	 * 
	 * @throws Exception
	 */

	public void hoverOnApoFpolnkInPostEvent() {
		Actions actions = new Actions(driver);
		Utils.waitForElement(driver, ApoFpolnkInPostEvent);
		BrowserActions.scrollToViewElement(ApoFpolnkInPostEvent, driver);
		actions.moveToElement(ApoFpolnkInPostEvent).build().perform();
		if (!Utils.waitForElement(driver, ApoFpoToolTipForPostEvent)) {
			actions.moveToElement(ApoFpolnkInPostEvent).build().perform();
		}
	}

	/**
	 * To MouseHover on phoneAssest link in post-event shipping
	 * 
	 * @throws Exception
	 */

	public void hoverOnPhoneAssestlnkInPostEvent() {
		Actions actions = new Actions(driver);
		Utils.waitForElement(driver, PhoneAssestlnkInPostEvent);
		BrowserActions.scrollToViewElement(PhoneAssestlnkInPostEvent, driver);
		actions.moveToElement(PhoneAssestlnkInPostEvent).build().perform();
		if (!Utils.waitForElement(driver, PhoneToolTipForPostEvent)) {
			actions.moveToElement(PhoneAssestlnkInPostEvent).build().perform();
		}
	}

	/**
	 * Click on the Address book for Desktop from left nav.
	 * 
	 * @throws Exception
	 */
	public void clickOnAddressBookLeftNav() throws Exception {
		BrowserActions.clickOnElement(lnkAddressBookLeftNav, driver, "Click on the Address book on the Left Nav.");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Shipping address book saved in address book
	 * 
	 * @return
	 * @throws Exception
	 */

	public String txtSavedAdddress() throws Exception {
		String txtShippingAddres = BrowserActions.getText(driver, txtSavedAddres,
				"Shipping Address Saved in the Address book");
		return txtShippingAddres;
	}

	/**
	 * click on the Hamburger icon on the for the mobile platform
	 */
	public void clickMobileHamburgerMenu() throws Exception {

		BrowserActions.scrollToViewElement(lnkHamburger, driver);
		BrowserActions.javascriptClick(lnkHamburger, driver, "Mobile Hamburger Link");
		BrowserActions.nap(1);
		// BrowserActions.clickOnElement(lnkHamburger, driver, "Mobile Hamburger
		// Link");
		if (Utils.waitForElement(driver, islnkHamburgerOpened)) {
			Log.event("clicked Hamburger Menu");
		} else {
			throw new Exception("Hamburger Menu didn't open up");
		}
	}

	/**
	 * To click on the My account for mobile platorm
	 * 
	 * @throws Exception
	 */
	public void clickMyAccount() throws Exception {
		BrowserActions.clickOnElement(btnMyAccount, driver, "Click on the My Account for Address book");
		BrowserActions.clickOnElement(btnAddressbook, driver, "Click on the  Address book");
		Utils.waitForPageLoad(driver);
	}

	public String txtSavedAddressMobile() throws Exception {

		String txtShippingAddresMobile = BrowserActions.getText(driver, txtSavedAddres,
				"Shipping Address Saved in the Address book");
		return txtShippingAddresMobile;
	}

	/**
	 * To get the drop down value of the Role type
	 */
	public String getDropDownBoxRole() throws Exception {
		BrowserActions.clickOnElement(drpRoleType, driver, "Click on the Drop down on Role Type");
		String getDropDownValues = driver.findElement(By.id("dwfrm_giftregistry_event_type")).getText();
		Utils.waitForPageLoad(driver);
		return getDropDownValues;

	}

	/**
	 * To verify global message is displayed with red color
	 */
	public boolean verifyGlobalErrorMessageColour() throws Exception {
		boolean status3 = false;
		String cssValueOfColor = "rgb(151, 29, 34)";
		status3 = Utils.verifyCssPropertyForElement(txtGlobalMessage, cssValueOfColor, "");
		return status3;
	}

	public boolean verifyPrintIconExistAboveRegistryBanner() throws Exception {
		boolean status = false;
		if (Utils.waitForElement(driver,
				fldRegistryBanner.findElement(By.xpath("//following-sibling::div[@class='print-page-cont']")))) {
			status = true;
		}
		return status;
	}

	public void clickOnProductName() throws Exception {
		BrowserActions.clickOnElement(lblProductNme, driver, "product name");
	}

	public static boolean isValidFormatregex(String value) throws Exception {

		if (value == null || !value.matches("[01]\\d/[0-3]\\d/\\d{4}"))
			return false;
		SimpleDateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		df.setLenient(false);
		try {
			df.parse(value);
			return true;
		} catch (ParseException ex) {
			return false;
		}

	}

	public String DateAddedTxt() throws Exception {
		String dateadded = BrowserActions.getText(driver, txtDateAdded, "Date Added txt");
		return dateadded;
	}

	public boolean isWouldLoveSelectbox() throws Exception {
		String wouldloveselectedvalue = BrowserActions.getText(driver, drpWouldLove,
				" Selected value in would Love drop down");
		if (wouldloveselectedvalue != null)
			return true;
		else
			return false;
	}

	public String getQtySelectedvalue() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(5);
		return BrowserActions.getText(driver, drpQty, " Upadated Value in Quantity drop down");
	}

	public void clickOnMyRegistry() throws Exception {
		BrowserActions.clickOnElement(tabMyRegistry, driver, "Clicked on My registry tab");
	}

	public void clickOnPurchases() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.clickOnElement(tabPurchase, driver, "Clicked on purchase tab");
			BrowserActions.nap(3);
		} else {
			BrowserActions.clickOnElement(shippingInfoTab, driver, "To close");
			BrowserActions.nap(3);
			BrowserActions.scrollToViewElement(purchaseInfoTabMobile, driver);
			BrowserActions.nap(3);
			BrowserActions.clickOnElement(purchaseInfoTabMobile, driver, "purchase tab");
			BrowserActions.nap(5);
		}
	}

	public LinkedHashMap<String, String> fillingRegistryDetails(String registryInfo) throws Exception {
		LinkedHashMap<String, String> registryDetils = new LinkedHashMap<String, String>();
		String registry = checkoutProperty.getProperty(registryInfo);
		String eventType = registry.split("\\|")[0];
		String eventDate = registry.split("\\|")[1];
		String city = registry.split("\\|")[2];
		String state = registry.split("\\|")[3];
		String role = registry.split("\\|")[4];
		String email = registry.split("\\|")[5];

		// filling Event Information
		registryDetils.put("select_eventType_" + eventType, selectEventtypeInCreateNewRegistry);
		String randomEventName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_eventName_event_" + randomEventName, txtEventNameInCreateNewRegistry);
		registryDetils.put("pickdate_eventDate_" + eventDate, selectEventDateInCreateRegistry);
		registryDetils.put("type_city_" + city, txtCityInCreateRegistry);
		registryDetils.put("select_state_" + state, selectStateInCreateRegistry);

		// filling registrant Information
		registryDetils.put("select_registrantrole_" + role, selectRegistryRoleInCreateReigstry);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_reFirstName_re" + randomFirstName, txtRegistrantFirstNameCreateRegistry);
		registryDetils.put("type_reLastName_re" + randomLastName, txtRegistrantLastNameCreateRegistry);
		registryDetils.put("type_registrantemail_" + email, txtRegistrantEmailCreateRegistry);

		// filling co-registrant Information
		registryDetils.put("select_coregistrantrole_" + role, selectCoRegistrantRoleInCreateRegistry);
		registryDetils.put("type_coFirstName_co" + randomFirstName, txtCoRegistrantFirstNameInCreateRegistry);
		registryDetils.put("type_coregistrantemail_" + email, txtCoRegistrantEmailInCreateRegistry);
		registryDetils.put("type_coLastName_co" + randomLastName, txtCoRegistrantLastNameInCreateRegistry);

		AccountUtils.doAccountOperations(registryDetils, driver);
		Log.event("Filled Registry Details");
		return registryDetils;
	}

	/**
	 * filling event shipping details
	 * 
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingEventShippingdetails(String shippingInfo) throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String shippingAddress = checkoutProperty.getProperty(shippingInfo);
		String addressHome = shippingAddress.split("\\|")[0];
		String address = shippingAddress.split("\\|")[1];
		String city = shippingAddress.split("\\|")[2];
		String state = shippingAddress.split("\\|")[3];
		String zipcode = shippingAddress.split("\\|")[4];
		String phone = shippingAddress.split("\\|")[5];

		// filling pre event shipping details
		shippingDetails.put("type_preaddressname_" + addressHome, txtAddressHome);
		shippingDetails.put("type_prefirstName_qa" + randomFirstName, txtFirstNameInPreEvent);
		shippingDetails.put("type_prelastName_qa" + randomLastName, txtLastNameInPreEvent);
		shippingDetails.put("type_preaddress_" + address, txtAddressInPreEvent);
		shippingDetails.put("type_precity_" + city, txtCityInPreEvent);
		shippingDetails.put("select_prestate_" + state, selectStateInPreEvent);
		shippingDetails.put("type_prezipCode_" + zipcode, txtZipcodeInPreEvent);
		shippingDetails.put("type_prephoneNo_" + phone, txtPhoneNeInPreEvent);

		// filling post event shipping details
		shippingDetails.put("type_postaddressname_" + addressHome, txtAddressHomeInPostEvent);
		shippingDetails.put("type_postfirstName_qa" + randomFirstName, txtFirstNameInPostEvent);
		shippingDetails.put("type_postlastName_qa" + randomLastName, txtLastNameInPostEvent);
		shippingDetails.put("type_postaddress_" + address, txtAddressInPostEvent);
		shippingDetails.put("type_postcity_" + city, txtCityInPostEvent);
		shippingDetails.put("select_poststate_" + state, selectStateInPostEvent);
		shippingDetails.put("type_postzipCode_" + zipcode, txtZipCodeInPostEvent);
		shippingDetails.put("type_postphoneNo_" + phone, txtPhoneInPostEvent);
		AccountUtils.doAccountOperations(shippingDetails, driver);
		Log.event("Filled Registry Details");
		return shippingDetails;
	}

	public String getRegisterId() throws Exception {
		String registerId = BrowserActions.getText(driver, lblRegistryConfirmation, "Regsiter Id ");
		registerId = registerId.split(" ")[2];
		return registerId;
	}

	public boolean verifyRegisterIdFormat(String register) throws Exception {
		String regex = "[0-9]{9}";
		if (register.matches(regex)) {
			return true;
		}
		return false;
	}

	public boolean verifyTheKnotCheckbox() throws Exception {
		if (BrowserActions.isRadioOrCheckBoxSelected(chkTheKnot) == false) {
			return true;
		}
		return false;
	}

	public void clickOnPreviousInStep3() throws Exception {
		BrowserActions.clickOnElement(btnPreviousInStep3, driver, "Clicked on Previous Button");
	}

	public void ClickOnSalesCheckbox() throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkSalesAssociate, "YES");
	}

	public boolean VerifyWarningMsgForNoEmpidStoreId() throws Exception {
		if ((BrowserActions.getText(driver, errorMsgforEmpId, " Error Message verification"))
				.contains("This field is required.")
				&& (BrowserActions.getText(driver, errorMsgForStoreId, " Error Message verification for Store Id"))
						.contains("This field is required.")) {
			return true;
		}
		return false;
	}

	public LinkedHashMap<String, String> enterWrongValuesForEmpStoreId(String WrongValueForEmpStoreId)
			throws Exception {
		LinkedHashMap<String, String> salesDetils = new LinkedHashMap<String, String>();

		String WrongValueForEmpStore = checkoutProperty.getProperty(WrongValueForEmpStoreId);
		String Empid = WrongValueForEmpStore.split("\\|")[0];
		String Storeid = WrongValueForEmpStore.split("\\|")[1];
		BrowserActions.typeOnTextField(txtEmpId, Empid, driver, "Employee id is entered");
		BrowserActions.typeOnTextField(txtStoreId, Storeid, driver, "Store Id Is enetered");
		return salesDetils;

	}

	public boolean VerifyWarningMsgForSizeEmpidStoreId() throws Exception {
		if ((BrowserActions.getText(driver, errorMsgforEmpId, " Error Message verification"))
				.contains("Please enter at least 6 characters.")
				&& (BrowserActions.getText(driver, errorMsgForStoreId, " Error Message verification for Store Id"))
						.contains("Please enter at least 4 characters.")) {
			return true;
		}
		return false;
	}

	public boolean verifycheckTermsAndConditionlabel() throws Exception {
		if (Utils.waitForElement(driver, lblReadAndAgree)) {
			if (BrowserActions.getText(driver, lblReadAndAgree, "Message in read and agree ")
					.contains("I have read and agree to the")) {
				return true;

			}
		}

		return false;
	}

	public boolean verifycheckTermsAndConditionCheckbox() throws Exception {
		if (BrowserActions.isRadioOrCheckBoxSelected(chkReadAndAgree) == false) {
			return true;
		}
		return false;
	}

	public LinkedHashMap<String, String> clickOnSubmitButtonwithoutTerms() throws Exception {
		LinkedHashMap<String, String> registryDetails = new LinkedHashMap<String, String>();
		registryDetails.put("click_submit", btnSubmit);
		AccountUtils.doAccountOperations(registryDetails, driver);
		Log.event("Filled Registry Details");
		BrowserActions.nap(3);
		return registryDetails;
	}

	public boolean verifyWarningMessageForNoTermsAndCondition() throws Exception {
		if (Utils.waitForElement(driver, warningMsgForTermsConditions)) {
			if (BrowserActions.getText(driver, warningMsgForTermsConditions, "Message in read and agree ")
					.contains("You must agree to the terms and conditions")) {
				return true;

			}
		}

		return false;
	}

	public LinkedHashMap<String, String> getRegistryInformation() throws Exception {
		LinkedHashMap<String, String> registryInfo = new LinkedHashMap<String, String>();

		String eventType = lblEventInfoInCreateNewRegistry.get(0).findElement(By.cssSelector("dd")).getText().trim();
		String eventName = lblEventInfoInCreateNewRegistry.get(1).findElement(By.cssSelector("dd")).getText().trim();
		String eventDate = lblEventInfoInCreateNewRegistry.get(2).findElement(By.cssSelector("dd")).getText().trim();
		String eventLocation = lblEventInfoInCreateNewRegistry.get(3).findElement(By.cssSelector("dd")).getText()
				.trim();
		String eventRegistrant = lblEventInfoInCreateNewRegistry.get(4).findElement(By.cssSelector("dd")).getText()
				.trim();
		if (!lblEventInfoInCreateNewRegistry.get(5).findElement(By.cssSelector("dd")).getText().isEmpty()) {
			String eventCoRegistrant = lblEventInfoInCreateNewRegistry.get(5).findElement(By.cssSelector("dd"))
					.getText().trim();
			String eventPreEveShippingAddr = lblEventInfoInCreateNewRegistry.get(6).findElement(By.cssSelector("dd"))
					.getText().trim();
			String eventPostEveShippingAddr = lblEventInfoInCreateNewRegistry.get(7).findElement(By.cssSelector("dd"))
					.getText().trim();
			registryInfo.put("eventcoregistrant", eventCoRegistrant);
			registryInfo.put("preeventshipping", eventPreEveShippingAddr);
			registryInfo.put("posteventshipping", eventPostEveShippingAddr);
		} else {
			Log.message("The Co-Registrant details are not entered");
		}
		registryInfo.put("eventtype", eventType);
		registryInfo.put("eventname", eventName);
		registryInfo.put("eventdate", eventDate);
		registryInfo.put("eventlocation", eventLocation);
		registryInfo.put("eventregistrant", eventRegistrant);

		return registryInfo;
	}

	public LinkedHashMap<String, String> fillingRegistryDetailsWithOneRegistrant(String registryInfo) throws Exception {
		LinkedHashMap<String, String> registryDetils = new LinkedHashMap<String, String>();
		String registry = checkoutProperty.getProperty(registryInfo);
		String eventType = registry.split("\\|")[0];
		String eventDate = registry.split("\\|")[1];
		String city = registry.split("\\|")[2];
		String state = registry.split("\\|")[3];
		String role = registry.split("\\|")[4];
		String email = registry.split("\\|")[5];

		// filling Event Information
		registryDetils.put("select_eventType_" + eventType, selectEventtypeInCreateNewRegistry);
		String randomEventName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_eventName_event_" + randomEventName, txtEventNameInCreateNewRegistry);
		registryDetils.put("pickdate_eventDate_" + eventDate, selectEventDateInCreateRegistry);
		registryDetils.put("type_city_" + city, txtCityInCreateRegistry);
		registryDetils.put("select_state_" + state, selectStateInCreateRegistry);

		// filling registrant Information
		registryDetils.put("select_registrantrole_" + role, selectRegistryRoleInCreateReigstry);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		registryDetils.put("type_reFirstName_re" + randomFirstName, txtRegistrantFirstNameCreateRegistry);
		registryDetils.put("type_reLastName_re" + randomLastName, txtRegistrantLastNameCreateRegistry);
		registryDetils.put("type_registrantemail_" + email, txtRegistrantEmailCreateRegistry);

		AccountUtils.doAccountOperations(registryDetils, driver);
		Log.event("Filled Registry Details");
		return registryDetils;
	}

	/**
	 * click on view
	 * 
	 * @throws Exception
	 *             updated by deepak
	 */
	public void clickOnViewlinkFindRegistry() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.clickOnElement(viewLinkInFindRegistryList.get(0), driver, "View link");
		} else if (Utils.getRunPlatForm() == "mobile") {
			BrowserActions.clickOnElement(ViewRegistrylnkForMobile, driver, "View link");
		}
	}

	public boolean verifyRegistrySigninElement() {
		boolean status = false;
		if (Utils.waitForElement(driver, lblregistry)) {
			status = true;
		}
		return status;
	}

	public void clickonToviewregistry(String regnumber) {
		int regsize = lstYourRegistries.size();
		for (int i = 0; i <= regsize; i++) {

			if ((lstYourRegistries.get(i).findElement(By.cssSelector(".event-id")).getText().trim())
					.equalsIgnoreCase(regnumber)) {
				lstYourRegistries.get(i).findElement(By.cssSelector(".event-details")).click();
			} else
				Log.message("Please try with Valid Event Register ID");

		}
	}

	public void clickRegistedProductEditDetails(String Productname) {
		int regsize = lstYourRegisteredItems.size();
		regsize = regsize * 2;
		for (int i = 2; i <= regsize; i += 2) {

			if ((lstYourRegisteredItems.get(i).findElement(By.cssSelector("tr div.name a")).getText().trim())
					.equalsIgnoreCase(Productname)) {
				lstYourRegisteredItems.get(i).findElement(By.cssSelector("div.item-edit-details a")).click();
				break;
			} else
				Log.message("Product is not listed under the Registed Product list..");

		}
	}

	public boolean isRegisteredProducteditDetailsvalidation() {
		return Utils.waitForElement(driver, Editdetailsmodal);
	}

	public void clickRegistedProductImage(String Productname) {
		int regsize = lstYourRegisteredItems.size();
		regsize = regsize * 2;

		for (int i = 2; i <= regsize; i += 2) {

			if ((lstYourRegisteredItems.get(i).findElement(By.cssSelector("tr div.name a")).getText().trim())
					.equalsIgnoreCase(Productname)) {
				lstYourRegisteredItems.get(i).findElement(By.cssSelector(".item-image a img")).click();
				break;
			} else
				Log.message("Product is not listed under the Registed Product list..");
		}
	}

	public String getRegisteredProductColor(String Productname) {
		int regsize = lstYourRegisteredItems.size();
		regsize = regsize * 2;

		for (int i = 2; i <= regsize; i += 2) {

			if ((lstYourRegisteredItems.get(i).findElement(By.cssSelector("tr div.name a")).getText().trim())
					.equalsIgnoreCase(Productname)) {
				lstYourRegisteredItems.get(i).findElement(By.cssSelector("td.item-details div:nth-child(3) span.value"))
						.click();
				break;
			} else
				Log.message("Product is not listed under the Registed Product list..");
		}
		return Productname;
	}

	public String getRegisteredProductColorFrommodal() throws Exception {

		Utils.waitForElement(driver, Editdetailsmodal);

		return BrowserActions.getText(driver, lblProductColorinModal,
				"Product color Field value from Product QuickView window");

	}

	/**
	 * To select the saved address on Create New Registry page
	 * 
	 * @param PostorPrevent
	 *            ("PreEvent or PostEvent")
	 */
	public void selectSavedPrePostEventAddressByIndex(int index, String PostorPreEvent) {
		Utils.waitForElement(driver, SelectSavedAddressInPreEvent);
		Select select = null;
		if (PostorPreEvent.equalsIgnoreCase("PreEvent")) {
			select = new Select(SelectSavedAddressInPreEvent);
			select.selectByIndex(index);
		} else if (PostorPreEvent.equalsIgnoreCase("PostEvent")) {
			Utils.waitForElement(driver, SelectSavedAddressInPostEvent);
			select = new Select(SelectSavedAddressInPostEvent);
			select.selectByIndex(index);
		}
	}

	/**
	 * To select the saved Address from the drop down box
	 * 
	 * @throws Exception
	 */
	public void selectSavedAddressbyIndex(int index) throws Exception {
		clickOnSavedAddress();
		BrowserActions.clickOnElement(dropDownTextSavedAddess.get(index - 1), driver,
				"Select the Address from the saved address");
	}

	public String getEventLocation() throws Exception {

		String Event_location = BrowserActions.getText(driver, viewEventLocation, "Clicked on view location");
		return Event_location;
	}

	public String gettxtStep3InCreateRegistry() throws Exception {

		String txtStep3 = BrowserActions.getText(driver, txtstep3InCreateRegistry, "text step3 in CreateReistry");
		return txtStep3;
	}

	public RegistryInformationPage clickOnFindRegistry(String searchType) throws Exception {
		WebElement element = null;
		if (searchType.equals("Simple Search")) {
			element = lnkSimpleSearchInFindRegistry;
		} else if (searchType.equals("Advanced Search")) {
			element = lnkAdvancedSearchInFindRegistry;
		}
		WebElement btnFindRegistry = element.findElement(By.xpath("../.."))
				.findElement(By.cssSelector("button[name='dwfrm_giftregistry_search_search']"));
		if (Utils.waitForElement(driver, btnFindRegistry)) {
			BrowserActions.clickOnElement(btnFindRegistry, driver, "Find a Registry Button");
		}

		return new RegistryInformationPage(driver).get();
	}

	public String getEventNameByRow(int row) {
		return txtEventNames.get(row).getText();
	}

	public String getEventTypeByRow(int row) {
		return txtEventType.get(row).getText();
	}

	public String getEventDateByRow(int row) {
		return txtEventDates.get(row).getText();
	}

	public String getTextFromBreadCrumbMobile() throws Exception {
		return lblBreadCrumbMobile.getText().trim();
	}

	/**
	 * to get text of msg display on MyRegistry page
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextOfMyRegistryMsg() throws Exception {
		BrowserActions.scrollToView(msgNoItemsInRegistry, driver);
		return BrowserActions.getText(driver, msgNoItemsInRegistry, "Get text of 'MyRegistry msg'");
	}

	/**
	 * To click on the My account for mobile platform
	 * 
	 * @throws Exception
	 */
	public void clickMyAccountLink() throws Exception {
		BrowserActions.clickOnElement(lnkmyAccountMob, driver, "Click on the My Account link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on apply button
	 * 
	 * @throws Exception
	 */
	public void clickOnApplyInEditRegistry() throws Exception {

		BrowserActions.clickOnElement(btnApply, driver, "Apply button in edit registry");
		BrowserActions.nap(5);

	}

	/**
	 * To click event infotab in edit registry
	 * 
	 * @throws Exception
	 */
	public void clickOnEventInfoInEditRegistry() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.javascriptClick(tabEventInfoDesktop, driver, "event tab");
		}

		else {

			BrowserActions.clickOnElement(tabEventInfoMobile, driver, "event tab");
		}

		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on shipping info tab
	 * 
	 * @throws Exception
	 */
	public void clickOnShippingInfoInEditRegistry() throws Exception {
		if (Utils.getRunPlatForm() == "desktop") {
			BrowserActions.clickOnElement(tabShippingInfoDesktop, driver, "shipping tab");
		}

		else {

			BrowserActions.clickOnElement(tabShippingInfoMobile, driver, "shipping tab");
		}
	}

	/**
	 * To verify Event details
	 * 
	 * @param registryDetailsBefore
	 * @param registryDetailsAfter
	 * @return
	 */
	public boolean verifyEventDetails(LinkedHashMap<String, String> registryDetailsBefore,
			LinkedHashMap<String, String> registryDetailsAfter) {
		boolean status = true;
		try {
			Set<String> setBefore = registryDetailsBefore.keySet();
			Set<String> setAfter = registryDetailsAfter.keySet();
			Iterator<String> iteratorBefore = setBefore.iterator();
			Iterator<String> iteratorAfter = setAfter.iterator();

			while (iteratorBefore.hasNext()) {
				if (registryDetailsBefore.get(iteratorBefore.next()) == registryDetailsAfter
						.get(iteratorAfter.next())) {
					status = false;
					break;
				}

			}

		} catch (Exception e) {

			System.out.print(status);
			// TODO: handle exception
		}

		return status;
	}

	/**
	 * To get Event registry details
	 * 
	 * @return registryInfo
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getEventRegistryDetails() throws Exception {

		BrowserActions.nap(5);
		LinkedHashMap<String, String> registryInfo = new LinkedHashMap<String, String>();
		String eventType = BrowserActions.getText(driver, selectEventtypeInCreateNewRegistry,
				"event type in event info");
		String eventName = BrowserActions.getTextFromAttribute(driver, txtEventNameInCreateNewRegistry, "value",
				"event name in event info");
		String eventDate = BrowserActions.getTextFromAttribute(driver, selectEventDateInCreateRegistry, "value",
				"event date in event info");
		String eventCity = BrowserActions.getTextFromAttribute(driver, txtCityInCreateRegistry, "value",
				"event city in event info");

		String eventState = BrowserActions.getText(driver, selectStateInCreateRegistry, "event state in event info");
		String registrantRole = BrowserActions.getText(driver, selectRegistryRoleInCreateReigstry,
				"registrant role in event info");
		String registrantFirstName = BrowserActions.getTextFromAttribute(driver, txtRegistrantFirstNameCreateRegistry,
				"value", "registrant first name in event info");
		String registrantLastName = BrowserActions.getTextFromAttribute(driver, txtRegistrantLastNameCreateRegistry,
				"value", "registrant last name in event info");
		String registrantEmail = BrowserActions.getTextFromAttribute(driver, txtRegistrantEmailCreateRegistry, "value",
				"registrant email in event info");
		String coregistrantRole = BrowserActions.getText(driver, selectCoRegistrantRoleInCreateRegistry,
				"coregistrant role in event info");
		String coregistrantFirstName = BrowserActions.getTextFromAttribute(driver,
				txtCoRegistrantFirstNameInCreateRegistry, "value", "coregistrant first name in event info");
		String coregistrantLastName = BrowserActions.getTextFromAttribute(driver,
				txtCoRegistrantLastNameInCreateRegistry, "value", "coregistrant last name in event info");
		String coregistrantEmail = BrowserActions.getTextFromAttribute(driver, txtCoRegistrantEmailInCreateRegistry,
				"value", "coregistrant email in event info");

		registryInfo.put("eventType", eventType);
		registryInfo.put("eventName", eventName);
		registryInfo.put("eventDate", eventDate);
		registryInfo.put("eventCity", eventCity);
		registryInfo.put("eventState", eventState);
		registryInfo.put("regRole", registrantRole);
		registryInfo.put("regFName", registrantFirstName);
		registryInfo.put("regLName", registrantLastName);
		registryInfo.put("regEmail", registrantEmail);
		registryInfo.put("coregRole", coregistrantRole);
		registryInfo.put("coregFName", coregistrantFirstName);
		registryInfo.put("coregLName", coregistrantLastName);
		registryInfo.put("coregEmail", coregistrantEmail);
		return registryInfo;

	}

	/**
	 * To click tab button
	 * 
	 * @throws Exception
	 */
	public void pressTab() throws Exception {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).build().perform();
	}

	/**
	 * to click on delete a registry button
	 * 
	 * @param index
	 *            - nth registry
	 * @throws Exception
	 */
	public void clickDeleteRegistryByIndex(int index) throws Exception {

		BrowserActions.clickOnElement(lstYourRegistries.get(index - 1), driver, "view button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the drop down value of the Saved Address from delete address
	 * window
	 */
	public String getDropDownAddress() throws Exception {
		BrowserActions.clickOnElement(drpSaveAddressInDelAddress, driver, "Click on the drop down of Saved Address");
		String getDropDownValues = driver
				.findElement(By.cssSelector("div[class='custom-select current_item']>ul>li:nth-child(2)")).getText();
		Utils.waitForPageLoad(driver);
		return getDropDownValues;
	}

	/*
	 * @return txtNOItemsInWishList *
	 * 
	 * @throws Exception
	 */
	public String getTextFromAddressOne() throws Exception {
		BrowserActions.scrollToViewElement(msgNoItemsInRegistry, driver);
		String txtAddressOne = BrowserActions.getTextFromAttribute(driver, txtAddressInPostEvent, "value",
				"Address in Shipping Address");
		return txtAddressOne;
	}
	
	/**
	 * addItemToBag
	 * @param productName
	 * @throws Exception
	 * created By - Nandhini.Bala 01/28/2016
	 */
	public void addItemToBag(String productName) throws Exception{
		for (int i =0;i <lstProductNames.size(); i++){
			if(lstProductNames.get(i).getText().contains(productName)){
				BrowserActions.clickOnElement(btnAddtoShoppingBag, driver, "Shopping Bag");
				Utils.waitForPageLoad(driver);
			}
		}
	}
	
	public String getProductName() throws Exception {
		String Pr = BrowserActions.getText(driver, getProductName,
				"Billing Address1 value");
		String Pr1 = Pr.trim().replace("Velcro&#174; Sandal", "");
		return Pr1;
	}

	public String getRegistryidByindex() throws Exception {
		
		String registryId ="";
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			registryId = BrowserActions.getText(driver, lstYourRegistriesMobile.get(0), "Registry ID");
		}
		else{
		registryId = lstYourRegistries.get(0)
				.findElement(By.cssSelector(".event-id")).getText().trim();
		}

		return registryId;
	}
	


}
