package com.belk.pages.registry;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.CreateAccountPage;
import com.belk.pages.ElementLayer;
import com.belk.pages.HomePage;
import com.belk.pages.PdpPage;
import com.belk.pages.footers.Footers;
import com.belk.pages.footers.socialmediapages.PintrestPage;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class GiftRegistryPage extends LoadableComponent<GiftRegistryPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Gift Registry Page **************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_gift-registry")
	WebElement divGiftRegisty;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;
	
	@FindBy(css = "button.menu-toggle.sprite")
	WebElement lnkHamburger;

    @FindBy(css = ".menu-active")
	WebElement islnkHamburgerOpened;


	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = "button[name='dwfrm_giftregistry_create']")
	WebElement btnCreateNewRegistry;

	@FindBy(css = ".event-details>a")
	WebElement lnkView;

	@FindBy(css = ".button.additemstoregistry")
	WebElement btnAddToRegistry;
	
	

	@FindBy(css = ".print-page")
	WebElement iconPrint;

	@FindBy(css = ".event-details>a")
	WebElement lnkViewRegistry;

	@FindBy(css = "div[class*='giftregistry_content']>p>span")
	WebElement registryBanner;

	@FindBy(css = "a[title='Add Gift Cards']")
	WebElement btnAddGiftCards;

	@FindBy(css = "a[title='Add Items to Registry']")
	WebElement btnAddRegistry;

	@FindBy(css = ".secondary-navigation div[class='content-asset']>p")
	WebElement txtAccountNavUnregpanel;

	@FindBy(css = ".account-nav-asset div[class='content-asset']>p>span")
	WebElement txtAccounthelpcontent;

	@FindBy(css = ".username input")
	WebElement txtUserName;

	@FindBy(css = ".password input")
	WebElement txtPassWord;

	@FindBy(name = "dwfrm_login_login")
	WebElement btnLoginIn;

	@FindBy(css = "span[id*='dwfrm_login_username']")
	WebElement txtEmailErrorMsg;

	@FindBy(css = ".regitry-list-cont>h2")
	WebElement titleFindSomeoneregistry;

	@FindBy(css = "#dwfrm_giftregistry_search")
	WebElement fldFindSomeoneregistry;

	@FindBy(css = ".registry-list-table")
	WebElement fldYourRegistry;

	@FindBy(css = "button[name='dwfrm_login_register']")
	WebElement btnCreateAccount;
	@FindBy(css = ".col-2")
	WebElement NewCustomerPanel;

	@FindBy(css = ".gift-registry-link .advanced-search")
	WebElement lnkAdvancedSearchInFindRegistry;

	@FindBy(css = "#dwfrm_giftregistry_search_advancedsearch_eventCity")
	WebElement fldCityInFindRegistry;

	@FindBy(css = ".eventAddress-state-row .selected-option")
	WebElement selectStateInFindRegistry;

	@FindBy(css = ".eventdatas-row")
	WebElement fldEventDateinAdvFindRegistry;

	@FindBy(css = ".form-row.eventRegistryID-row")
	WebElement fldRegistryIdinAdvFindRegistry;

	@FindBy(css = ".gift-registry-link .simple-search")
	WebElement lnkSimpleSearchInFindRegistry;

	@FindBy(css = "div[id='dialog-container']")
	WebElement passwordRecoveryPage;

	@FindBy(css = "div[class='success']")
	WebElement txtSuccsessPasswordResetMail;

	@FindBy(css = "#PasswordResetForm button[name*='password_send']")
	WebElement btnSendinPasswordResetPage;

	@FindBy(css = "#PasswordResetForm button[name*='password_cancel']")
	WebElement btnCancelinPasswordResetPage;

	@FindBy(css = "#dwfrm_requestpassword_email")
	WebElement txtEmailfldInPasswordRecoveryPage;

	@FindBy(css = ".registry_results div[class='registry-list-table']")
	WebElement registryresults;

	@FindBy(css = ".account-nav-asset")
	WebElement accountnavpanel;

	@FindBy(css = ".form-row.eventType-row selected-option")
	WebElement selectEventTypeInFindRegistry;

	@FindBy(css = "div[ class='password-row']>a[id='password-reset']")
	WebElement verifyForgetPassword;

	@FindBy(css = ".form-row.eventType-row>label>span")
	WebElement lblEventType;

	@FindBy(css = ".advanced-search")
	WebElement lnkAdvanceSearch;

	@FindBy(css = ".label-caption>span")
	List<WebElement> fldAdvanceSearch;

	@FindBy(css = ".registry-list-table>p")
	WebElement txtNoRegistryMsg;

	@FindBy(xpath = "//*[@id='dwfrm_giftregistry_event_type']//following-sibling::div")
	WebElement selectEventType;
	
	@FindBy(xpath = ".//*[@id='header']/div[4]/div[1]/ul[2]/li[5]/div/ul/li[2]/a")
	WebElement registryPanelMobile;

	@FindBy(css = ".regitry-list-desc")
	WebElement txtFindRegistryMsg;

	@FindBy(css = ".wishlist-list-table .view>a")
	WebElement lnkViewOnWishListResultdesktop;

	@FindBy(css = "div[class='item-list-device hide-desktop'] .view>a")
	WebElement lnkViewOnWishListResultMobile;

	@FindBy(css = ".login-giftregistry-benefits.clearfix")
	WebElement txtRegistryBenefitContentAsset;

	@FindBy(css = ".regitry-list-cont>h1")
	WebElement lblRegistry;

	@FindBy(css = ".regitry-list-cont>h2")
	WebElement lblFindRegistry;

	@FindBy(css = ".form-row>label[for='dwfrm_giftregistry_search_advancedsearch_registrantFirstName']>span")
	WebElement lblFirstName;

	@FindBy(css = ".form-row>label[for='dwfrm_giftregistry_search_advancedsearch_registrantLastName']>span")
	WebElement lblLastName;

	@FindBy(css = ".share-options>a[class='share-icon share-facebook']")
	WebElement facebookTab;

	@FindBy(css = ".share-options>a[class='share-icon share-twitter']")
	WebElement twitterTab;

	@FindBy(css = ".share-options>a[class='share-icon share-googleplus']")
	WebElement googleplusTab;

	@FindBy(css = ".share-options>a[class='share-icon share-pinternet']")
	WebElement pinternetTab;

	@FindBy(css = ".share-options>a[class='share-icon share-email']")
	WebElement emailTab;

	@FindBy(css = ".share-options>a[class='share-icon share-link']")
	WebElement linkTab;

	@FindBy(xpath = ".//*[@id='secondary']/nav/div/div[1]/ul/li[5]/a")
	WebElement registryPanel;

	@FindBy(css = "table>tbody>tr>.event-details>a")
	WebElement ViewRegistrylnk;

	@FindBy(css = ".share-options>a[class='share-icon share-pinternet']")
	WebElement pininterestTab;

	@FindBy(css = "[class='item-dashboard']>a:not([id])")
	WebElement ViewRegistrylnkForMobile;

	@FindBy(css = ".list-share button[class*='secondarybutton']")
	WebElement btnMakeThisList;

	@FindBy(css = "div.registry-user-info h2")
	WebElement lblEventName;

	@FindBy(css = "div.registry-user-info .registryId")
	WebElement lblRegistryId;

	@FindBy(css = "div.registry-user-info div[id*='barcodeRegistry']")
	WebElement lblBarCode;

	@FindBy(css = "[class='viewitems']")
    WebElement lnkViewDetails;
	
	@FindBy(css = "div[class='error-form']")
	WebElement InvalidLoginErrormsg;
	
	@FindBy(css = "#dwfrm_giftregistry_search_advancedsearch_registrantFirstName")
	WebElement txtFirstNameInFindRegistry;
	@FindBy(css = "#dwfrm_giftregistry_search_advancedsearch_registrantLastName")
	WebElement txtLastNameInFindRegistry;
		@FindBy(css = ".selected-option")
	WebElement drpEvent;
	
	@FindBy(css = ".login-search-gift.clearfix")
	WebElement findRegistryPanel;
	
	@FindBy(css = "button[name='dwfrm_giftregistry_search_search']")
	WebElement btnFindRegistry;
	
	@FindBy(css = "div[class='custom-select current_item']>ul[class='selection-list']>li:not([class='selected'])")
	List<WebElement> lstEvent;
	
	@FindBy(css = ".registry-list-table>p")
	WebElement txtnosearchYourRegistry;
	
	@FindBy(css = ".event-view>a")
	WebElement linkView;
	
	@FindBy(css = ".item-dashboard>a")
	WebElement linkViewMobile;
	
	@FindBy(css ="div[class='registry-list-table'] .item-dashboard>a")
    WebElement lnkviewOnRegistryResultMobile;

    
    @FindBy(css ="div[class='registry-list-table'] .event-view>a")
    WebElement lnkviewOnRegistryResultDesktop;
    
    @FindBy(css=".event-details>a")
    WebElement viewActiveRegistry;
    
    @FindBy(css=".viewitems")
	WebElement viewActiveRegistryMoblie;

	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public GiftRegistryPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divGiftRegisty))) {
			Log.fail("Gift Registry page did not open up.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	/**
	 * Navigate to registry signed user page
	 * 
	 * @return
	 * @throws Exception
	 */
	public RegistrySignedUserPage clickOnCreateRegistrybutton() throws Exception {
		BrowserActions.clickOnElement(btnCreateNewRegistry, driver, "click on create new registry button");
		return new RegistrySignedUserPage(driver).get();

	}

	public void clickOnViewLink() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, ViewRegistrylnkForMobile);
			BrowserActions
					.scrollToViewElement(ViewRegistrylnkForMobile, driver);
			BrowserActions.javascriptClick(ViewRegistrylnkForMobile, driver,
					"view Registry");
		}
		else
			BrowserActions.clickOnElement(lnkView, driver, "View");
	}

	/**
	 * clickOnAddToGiftCardButton
	 * 
	 * @return
	 * @throws Exception
	 */
	public PdpPage clickOnAddToGiftCardButton() throws Exception {
		BrowserActions.clickOnElement(btnAddGiftCards, driver, "click Gift card");
		return new PdpPage(driver).get();
	}

	/**
	 * clickOnAddToGiftCardButton
	 * 
	 * @return
	 * @throws Exception
	 */
	public HomePage clickOnAddItemToRegistryButton() throws Exception {
		BrowserActions.clickOnElement(btnAddRegistry, driver, "click Add registry");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver, driver.getCurrentUrl()).get();
	}

	/**
	 * To click LogIn button on signin page
	 * 
	 * @throws Exception
	 */
	public void clickBtnSignIn() throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.clickOnElement(btnLoginIn, driver, "Login");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Login' button on SignIn page", StopWatch.elapsedTime(startTime));
	}

	public void enterEmailID(String emailid) throws Exception {
		Utils.waitForElement(driver, txtUserName);
		BrowserActions.typeOnTextField(txtUserName, emailid, driver, "Email id");
		Log.event("Entered the Email Id: " + emailid);
	}

	public void enterEmailIDInPasswordRecoveryPage(String emailid) throws Exception {
		Utils.waitForElement(driver, txtEmailfldInPasswordRecoveryPage);
		BrowserActions.typeOnTextField(txtEmailfldInPasswordRecoveryPage, emailid, driver, "Email id");
		Log.event("Entered the Email Id: " + emailid + "to recover the password");
	}

	/**
	 * Enter password
	 * 
	 * @param pwd
	 *            as string
	 * @throws Exception
	 */
	public void enterPassword(String pwd) throws Exception {
		Utils.waitForElement(driver, txtPassWord);
		BrowserActions.typeOnTextField(txtPassWord, pwd, driver, "Password");
		Log.event("Entered the Password: " + pwd);
	}

	/**
	 * To get the Email field's error msg
	 * 
	 * @return dataToBeReturned - Email error msg
	 * @throws Exception
	 */
	public String getEmailErrorMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, txtEmailErrorMsg, "Email field error msg");
		return dataToBeReturned;
	}

	/**
	 * To click create new account button on registry page
	 * 
	 * @throws Exception
	 */
	public CreateAccountPage clickCreateAccount() throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.clickOnElement(btnCreateAccount, driver, "Login");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Create Account' button on registry page", StopWatch.elapsedTime(startTime));
		return new CreateAccountPage(driver).get();
	}

	/**
	 * To get the new customer content msg
	 * 
	 * @return dataToBeReturned - New Customer Content MSg
	 * @throws Exception
	 */
	public String getTextFromNewCustomer() throws Exception {
		String txtNewCustomer = BrowserActions.getText(driver, NewCustomerPanel, "New Customer Panel");
		return txtNewCustomer;
	}

	/**
	 * To click cancel button in password reset page
	 * 
	 * @throws Exception
	 */
	public void clickCancel() throws Exception {
		BrowserActions.clickOnElement(btnCancelinPasswordResetPage, driver, "CancelButton");
	}

	/**
	 * To get the password reset success msg
	 * 
	 * @return dataToBeReturned - New Customer Content MSg
	 * @throws Exception
	 */

	public String getTextFromPasswordResetSuccessPage() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, txtSuccsessPasswordResetMail, "Password Reset Success msg");
		//BrowserActions.nap(3);
		return dataToBeReturned;
	}

	public void clickSendButton() throws Exception {
		BrowserActions.clickOnElement(btnSendinPasswordResetPage, driver, "Send Button");
	}

	public void clikForgotPwd() throws Exception {
		BrowserActions.clickOnElement(verifyForgetPassword, driver, "Forget PassWord");
	}

	public void clickOnAdvancedSearchLnk() throws Exception {
		BrowserActions.clickOnElement(lnkAdvancedSearchInFindRegistry, driver, "Link Advanced Search");
	}

	public String getTextFromleftAccountPanel() throws Exception {

		String dataTobeReturned = null;
		dataTobeReturned = BrowserActions.getText(driver, accountnavpanel, "elementDescription");
		return dataTobeReturned;

	}

	public String gettextFromSelectedEventinDropDown() throws Exception {
		String text = null;
		text = BrowserActions.getText(driver, selectEventTypeInFindRegistry, "elementDescription");
		return text;
	}

	public RegistrySignedUserPage signInToMyAccount(String emailid, String password) throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new RegistrySignedUserPage(driver).get();
	}

	public void clickOnAdvanceSearch() throws Exception {
		BrowserActions.clickOnElement(lnkAdvanceSearch, driver, "Link Advance search");

	}

	public String getTextFromGiftRegistry() throws Exception {
		return BrowserActions.getText(driver, txtFindRegistryMsg, "txtFindRegistry");
	}

	public boolean verifyBreadCrumb(List<String> breadCrumbText) {
		if (runPltfrm == "desktop") {
			int flag = 0;
			for (String text : breadCrumbText) {
				if (text.contains("Registry Login")) {
					flag = 1;
					break;
				}

			}
			if (flag == 1)
				return true;
			else
				return false;
		} else {
			if (breadCrumbText.get(0).contains("Back to Home"))
				return true;
			else
				return false;
		}
	}


	public RegistryInformationPage clickOnViewRegistrylnk() throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForElement(driver, ViewRegistrylnkForMobile);
			BrowserActions.scrollToViewElement(ViewRegistrylnkForMobile, driver);
			BrowserActions.javascriptClick(ViewRegistrylnkForMobile, driver, "view Registry");
		} else {
			Utils.waitForElement(driver, ViewRegistrylnk);
			BrowserActions.scrollToViewElement(ViewRegistrylnk, driver);
			BrowserActions.clickOnElement(ViewRegistrylnk, driver, "view Registry");
		}

		return new RegistryInformationPage(driver).get();
	}

	/**
	 * Navigate to social links Pininterest Page
	 * 
	 * @return
	 * @throws Exception
	 */

	public PintrestPage navigateToPinterestPage() throws Exception {

		BrowserActions.javascriptClick(pininterestTab, driver, "clicking Social Media Pinterest Icon");
		return new PintrestPage(driver);
	}

	/**
	 * clickOnMakeThisList
	 * 
	 * @throws Exception
	 */
	public void clickOnMakeThisList() throws Exception {
		if (btnMakeThisList.getAttribute("value").contains("Public")) {
			BrowserActions.clickOnElement(btnMakeThisList, driver, "click make this list public");
		} else {
			BrowserActions.clickOnElement(btnMakeThisList, driver, "click make this list Private");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * makeThisListButtonType
	 * 
	 * @return
	 */
	public boolean makeThisListButtonType() {
		boolean returnValue = false;

		if (!btnMakeThisList.getAttribute("value").contains("Public")) {
			returnValue = true;
		} else if (!btnMakeThisList.getAttribute("value").contains("Private")) {
			returnValue = true;
		}
		return returnValue;
	}
	
	/***
	 * to click view details link
	 * @throws Exception
	 */
	public void clickOnViewDetailsLink() throws Exception {
		BrowserActions.scrollToView(lnkViewDetails, driver);
		BrowserActions.clickOnElement(lnkViewDetails, driver, "View");
	}

	public void setFirstNameInFindRegistry(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstNameInFindRegistry, firstName,
				driver, "First Name in Find Registry");
	}

	public void setLastNameInFindRegistry(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtLastNameInFindRegistry, firstName,
				driver, "Last Name in Find Registry");
	}

	public void selectEventTypeInFindRegistry(int index) throws Exception {
		clickEventDropown();
		BrowserActions.clickOnElement(lstEvent.get(index - 1), driver, "Event");
		Utils.waitForPageLoad(driver);
	}

	public void clickEventDropown() throws Exception {
		BrowserActions.scrollToView(drpEvent, driver);
		BrowserActions.clickOnElement(drpEvent, driver, "Event dropdown");
	}

	public void clickOnFindRegistry() throws Exception {
		BrowserActions.clickOnElement(btnFindRegistry, driver,
				"Find Registry Button");
		Utils.waitForPageLoad(driver);
	}

	public String getNoSearchTxt() throws Exception {
		String noseacrhmessage = BrowserActions.getText(driver,
				txtnosearchYourRegistry, " stock message");
		return noseacrhmessage;

	}
	
	public void clickViewLink() throws Exception {
	        
        if (Utils.getRunPlatForm().equals("mobile")) {
            BrowserActions.scrollToViewElement(lnkviewOnRegistryResultMobile, driver);
            BrowserActions.clickOnElement(lnkviewOnRegistryResultMobile, driver, " Clicking View link in Registry Result");
        } else {
            BrowserActions.clickOnElement(lnkviewOnRegistryResultDesktop, driver, " Clicking View link in Registry Result");
        }
    }
		
	/**
	 * To click on the Mobile Hamburger
	 * @throws Exception
	 */
	
	public void clickMobileHamburgerMenu() throws Exception {

		BrowserActions.scrollToViewElement(lnkHamburger, driver);
		BrowserActions.javascriptClick(lnkHamburger, driver, "Mobile Hamburger Link");
		BrowserActions.nap(1);
		if (Utils.waitForElement(driver, islnkHamburgerOpened)) {
			Log.event("clicked Hamburger Menu");
		} else {
			throw new Exception("Hamburger Menu didn't open up");
		}
	}
	
	/**
	 * To click on the registry panel from Left nav. fo desktop and mobile.
	 * @throws Exception
	 */
	public void clickOnRegistryPanel() throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			clickMobileHamburgerMenu();
			Utils.waitForElement(driver, registryPanelMobile);
			BrowserActions.scrollToViewElement(registryPanelMobile, driver);
			BrowserActions.javascriptClick(registryPanelMobile, driver, "Registry Link Not found In mobile");
		} else {
			Utils.waitForElement(driver, registryPanel);
			BrowserActions.scrollToViewElement(registryPanel, driver);
			BrowserActions.clickOnElement(registryPanel, driver, "Registry Link Not found");
		}
	}
	
	
	public RegistrySignedUserPage clickonViewRegister()throws Exception{
		
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(viewActiveRegistryMoblie, driver,"Clicked View on Active Registry");
			return new RegistrySignedUserPage(driver).get();

		
		}else{			
			BrowserActions.clickOnElement(viewActiveRegistry, driver,"Clicked View on Active Registry");
			return new RegistrySignedUserPage(driver).get();
		}
	}
	
	/**
     * To get the Invalid Credentials error msg
     * 
     * @return dataToBeReturned - Invalid Credentials error msg
     * @throws Exception
     */
    public String getTextFromInvalidCredentialsLoginErrorMsg() throws Exception {
        String invaliderrormsg = BrowserActions.getText(driver, InvalidLoginErrormsg, " Invaliderrormsg");
        return invaliderrormsg;
    }

}// GiftRegistryPage
