package com.belk.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class Pagecontent extends LoadableComponent<Pagecontent> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Quick view Page ***********************************
	 **********************************************************************************************/
	@FindBy(css = "div[id='main']")
	WebElement pageContent;

	@FindBy(css = "a[class='breadcrumb-element']")
	WebElement pagecontentheading;
	
	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement pageHeading;

	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public Pagecontent(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, pageContent))) {
			Log.fail("Page Content did not open up. Site might be down.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	public String getSearchPageContent() throws Exception {
		String searchHeaderText = null;
		searchHeaderText = BrowserActions.getText(driver, pagecontentheading, "Page content Heading!");
		return searchHeaderText;
	}
	
	public String getPageContent() throws Exception {
		String HeaderText = BrowserActions.getText(driver, pageHeading, "Page content Heading!").replace(" ", "").trim();
		return HeaderText;
	}


}
