package com.belk.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class GiftCardsPage extends LoadableComponent<GiftCardsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	// --------- BreadCrumb Section ---------- //

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrumb a[class*='hide-desktop']")
	List<WebElement> txtProductInBreadcrumbMobile;

	@FindBy(css = ".mini-cart-content")
	WebElement minicartContent;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;

	@FindBy(css = "div [id='item-0480004388708']>div>div>ul>li[class='set-quantity']>div>input")
	WebElement increaseQty;
	
	@FindBy(css="div[class='selected-option']")
	WebElement clickDropDown;
	
	@FindBy(xpath=".//*[@id='item-58007150005101EA']/div[3]/ul/li[1]/div/div/ul/li")
	List<WebElement> lstSelect;
	
//	@FindBy(css = "div[id='item-0480004388708']>div[class='product-add-to-cart add-sub-product']>form>button")
//	WebElement ClickOnContinue; 
	
	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart")
	List<WebElement> ClickOnContinue; 

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException
	 */
	public GiftCardsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		// Utils.waitForPageLoad(driver);
		if (isPageLoaded && !(driver.getCurrentUrl().contains("gift"))) {
			Log.fail("Gift Cards page didn't open up.", driver);
		}
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		List<WebElement> lstElement = null;
		if (Utils.getRunPlatForm() == "mobile") {
			lstElement = txtProductInBreadcrumbMobile;
		} else {
			lstElement = lstTxtProductInBreadcrumb;
		}
		for (WebElement element : lstElement) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;
	}

//	public ShoppingBagPage clickAddToBag() throws Exception {
//		BrowserActions
//				.clickOnElement(ClickOnContinue, driver, "Gift Card Link");
//		Utils.waitForPageLoad(driver);
//		return new ShoppingBagPage(driver).get();
//	}

	public ShoppingBagPage clickOnMiniCart() throws Exception {
		if (Utils.waitForElement(driver, minicartContent))
			if (minicartContent.getCssValue("display").contains("block"))
				BrowserActions.nap(5);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public void increaseQty() throws Exception {
		BrowserActions.typeOnTextField(increaseQty, "5", driver,
				"Increase Quantity");
	}
	

	
	
	public void clickOnSelectValue(int dropdownindex) throws Exception {
		BrowserActions.clickOnElement(clickDropDown, driver, "SelectDropDown");
		BrowserActions.clickOnElement(lstSelect.get(dropdownindex - 1), driver, "Select the Values from Gift Card");
		Utils.waitForPageLoad(driver);
	
	} 


public ShoppingBagPage clickAddToBag() throws Exception {
		BrowserActions
				.clickOnElement(ClickOnContinue.get(0), driver, "Gift Card Link");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}
}
