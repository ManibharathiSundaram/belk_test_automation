package com.belk.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.footers.socialmediapages.FacebookPage;
import com.belk.pages.footers.socialmediapages.GooglePlusPage;
import com.belk.pages.footers.socialmediapages.PintrestPage;
import com.belk.pages.footers.socialmediapages.TwitterPage;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;
import com.btr.proxy.util.PlatformUtil.Browser;

public class PdpPage extends LoadableComponent<PdpPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public HomePage homePage;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	public MiniCartPage minicart;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of PDP Page ***********************************
	 **********************************************************************************************/

	private static final String DEFAULT_SELECTED_VALUE_DROPDOWN = "div[class='value'] div[class*='selected-option']";
	private static final String LIST_OF_OPTIONS_IN_DROPDOWN = "div.current_item ul li:not([class*='hide']):not([class*='selected'])";
	private static final String LIST_OF_COLOR = "ul.swatches.color>li:not([class*='selected'])";
	private static final String SELECTED_COLOR = "ul[class='swatches color']>li.selected>a>img";
	private static final String SELECTED_COLOR_ZOOM_WINDOW = ".click-zoomwindow-dialog  ul[class='swatches color']>li.selected>a>img";
	private static final String SELECTED_QTY = ".product-add-to-cart .quantity .selected-option";
	private static final String SOCIAL_MEDIA = "div[class='socialsharing']";
	private static final String LIST_OF_COLOR_SELECTABLE = "ul.swatches.color>li.selectable:not([class*='selected'])";

	@FindBy(css = "#primary div[id='pdpMain']")
	WebElement pdPContent;

	@FindBy(css = ".loader[style*='block']")
	WebElement PDPspinner;

	@FindBy(css = ".product-price")
	List<WebElement> txtPriceInProductSet;

	@FindBy(css = ".add-to-cart-bonus")
	//@FindBy(css="button[type='submit']>span")
	WebElement btnSelectFreeGift;

	@FindBy(css = "form[name='simpleSearch'] button")
	WebElement btnSearch;

	@FindBy(css = ".thumb-prodDetailLink")
	WebElement detailsLink;

	@FindBy(css = ".choice-bonus-title")
	WebElement titleGElModel;

	@FindBy(css = ".ui-button-icon-primary.ui-icon.ui-icon-closethick")
	WebElement linkCloseIcon;

	@FindBy(css = ".standardprice")
	WebElement txtstandardPrice;

	@FindBy(css = "a[id='mini-cart-link-checkout']")
	WebElement btncheckout;

	@FindBy(css = "div[class='mini-cart-products'] div[class='mini-cart-diffPay success']")
	WebElement miniCartSuccessMsg;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement bcLastCategoryName;

	@FindBy(css = "div[class='breadcrumb'] a[href*='home']")
	WebElement bcHome;

	@FindBy(css = ".product-brand>span>a")
	WebElement lnkBrandName;

	@FindBy(css = "h1.product-name")
	WebElement fldProductName;

	@FindBy(css = "h1 .product-brand")
	WebElement productBrandInProductSet;

	@FindBy(css = "a.find-a-store-pdp")
	WebElement lnkFindInStore;

	@FindBy(css = ".shipping-promotion")
	WebElement txtFreeShipping;

	@FindBy(css = "button[id='add-to-cart']")
	WebElement btnAddtoBag;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = "div[class='breadcrumb']")
	WebElement lnkBreadCrumb;

	@FindBy(css = ".hide-desktop.breadcrumb-element")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".product-image.main-image img")
	WebElement primaryImage;

	@FindBy(css = ".thumbnail-link img")
	List<WebElement> lstImageThumbnail;

	@FindBy(css = "a[data-action='wishlist']")
	// @FindBy(xpath = ".//*[@id='product-content']/div[2]/a[2]")
	WebElement lnkAddToWishList;

	@FindBy(xpath = ".//*[@id='product-content']/div[4]")
	WebElement txtWishListMessage;

	@FindBy(css = "div[class='value'] div[class*='selected-option']")
	WebElement drpSize;

	@FindBy(css = "div#dialog-container.dialog-content.ui-dialog-content.ui-widget-content")
	WebElement popUpRegistryBox;

	@FindBy(css = "div[class='value'] div[class*='selected-option']")
	public List<WebElement> drpSizes;

	@FindBy(css = ".quantity div[class='custom-select']")
	// li[class='attribute variant-dropdown
	// size']>div>div>div[class='selected-option']
	WebElement drpQty;

	@FindBy(css = ".size ul[class='selection-list'] li:not([class*='hide']):not([class*='selected'])")
	List<WebElement> lstSize;

	@FindBy(css = ".quantity ul[class='selection-list']>li")
	public List<WebElement> lstQty;

	@FindBy(css = "span[class='size-chart-link']")
	WebElement lnkSizechart;

	@FindBy(css = ".cart-promo.cart-promo-approaching>b")
	WebElement getCost;

	@FindBy(css = "div[id='primary'] h1")
	WebElement txtPrivacyPolicy;

	@FindBy(css = "div[class='nofit'] p a")
	WebElement lnkPrivacyPolicy;

	@FindBy(css = "span[class='ui-button-icon-primary ui-icon ui-icon-closethick']")
	WebElement iconCloseSizeChart;

	@FindBy(css = "div[id='primary'] h1")
	WebElement lblPrivacyPolicy;

	@FindBy(css = ".product-brand>span>a")
	WebElement getBrandNameInPdp;

	@FindBy(css = ".ui-widget-overlay")
	WebElement lnkCustomerService;

	@FindBy(css = "span[class='ui-dialog-title']")
	WebElement dialogSizechart;

	@FindBy(css = ".breadcrumb-refinement-value>span")
	WebElement lblBreadCrumpBrand;

	@FindBy(css = ".color .label span")
	WebElement lblColorSelect;

	@FindBy(css = ".primary-image")
	WebElement imgProduct;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(css = ".button.simple.gift-registry-unregistered")
	WebElement btnRegistryAsGuest;

	@FindBy(css = "a[data-action='gift-registry']")
	WebElement btnRegistrySigneduser;

	@FindBy(css = "div.success")
	WebElement lblsuccessMessage;
	
	@FindBy(css=".max_buy_qty_message.error")
	WebElement lblErrorMsg;

	@FindBy(css = "td[class='eventtype'] span[class='bold hide-desktop']")
	WebElement lblMobileEventType;

	@FindBy(css = "td[class='eventdate'] span[class='bold hide-desktop']")
	WebElement lblMobileEventDate;

	@FindBy(css = "td[class='eventname'] span[class='bold hide-desktop']")
	WebElement lblMobileEventName;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-pinternet']")
	WebElement lnkPintrest;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-facebook']")
	WebElement lnkFacebook;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-twitter']")
	WebElement lnkTwitter;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-googleplus']")
	WebElement lnkGooglePlus;

	@FindBy(css = SOCIAL_MEDIA + " a[class='share-icon share-email']")
	WebElement lnkEmail;

	@FindBy(css = SELECTED_COLOR)
	WebElement selectedColor;

	@FindBy(css = "li[class='selectable']")
	public List<WebElement> unselectedColors;

	@FindBy(css = "div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front search-store-dialog ui-dialog-buttons ui-draggable']")
	WebElement findStoreDialog;
	@FindBy(css = ".loader[style*='block']")
	WebElement spinner;

	@FindBy(css = LIST_OF_COLOR_SELECTABLE)
	List<WebElement> lstColorSelectable;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQuantity;

	@FindBy(css = LIST_OF_COLOR)
	List<WebElement> lstColor;

	@FindBy(css = ".loader[style*='block']")
	WebElement pdpSpinner;

	@FindBy(css = ".primary-image")
	WebElement imgPrimary;

	@FindBy(css = ".price-standard")
	WebElement standardPrice;

	@FindBy(css = ".swatches.color li.selectable")
	List<WebElement> listColor;

	@FindBy(css = ".click-to-zoom")
	WebElement lnkClickToZoom;

	@FindBy(css = ".click-zoom-dialog")
	WebElement clickToZoomDialog;

	@FindBy(css = ".click-zoom-dialog > .swatches.color")
	WebElement clickToZoomSwatchColor;

	@FindBy(css = ".click-zoom-dialog li.thumb.selected .productthumbnail")
	WebElement clickToZoomThumbSelected;

	@FindBy(css = ".click-zoom-dialog .primary-image")
	WebElement clickToZoomPrimaryImage;

	@FindBy(css = ".product-image-container .jcarousel>ul>li")
	List<WebElement> thumbsInContainer;

	@FindBy(css = ".click-zoom-dialog .jcarousel>ul>li")
	List<WebElement> thumbsInModal;

	@FindBy(css = ".click-zoom-dialog .selected " + SELECTED_COLOR)
	WebElement selectedColorInModal;

	@FindBy(css = ".product-col-2.product-detail.product-set")
	WebElement productSet;

	@FindBy(css = "div[class='product-badge']")
	WebElement productBadge;

	@FindBy(css = ".product-UPC")
	WebElement lblUPC;

	@FindBy(css = ".product-UPC")
	List<WebElement> getUPC;

	@FindBy(css = ".click-to-zoom")
	WebElement lnkClickToEnlarge;

	// @FindBy(css =
	// "#pdpMain > div > div.product-col-1.product-image-container > div.product-zoom-action > span.hide-mobile")
	// WebElement txtHoverToZoom;

	@FindBy(css = ".product-zoom-action span.hide-mobile")
	WebElement txtHoverToZoom;

	@FindBy(css = "div[class='product-primary-image']>a[class='product-image main-image']")
	static WebElement productMainImage;
	
	@FindBy(css="label[for='Free In-Store Pickup'] input[name='store-ship-radio']")
	WebElement rdoInStorePickup;

	@FindBy(css = "#add-all-to-cart")
	WebElement addAllToShoppingBagForProductSet;

	@FindBy(css = ".standardprice")
	WebElement lblProductStandardPrice;

	@FindBy(css = ".price-sales")
	WebElement lblProductNowPrice;

	// @FindBy(css = "#ui-id-1")
	@FindBy(css = "div[style*='display: block;']")
	WebElement enlargePopUP;

	@FindBy(css = "ul.swatches.color>li.selectable")
	List<WebElement> lstColorSwatches;

	@FindBy(css = "ul.swatches.color>li>a")
	List<WebElement> lstSelectColor;

	@FindBy(css = "a[class='jcarousel-nav jcarousel-next']")
	WebElement btnNextImage;

	@FindBy(css = "a[class='jcarousel-nav jcarousel-prev']")
	WebElement btnPreviousImage;

	@FindBy(css = ".thumbnail-link")
	List<WebElement> lstThumbnailImages;

	@FindBy(css = "div[id='thumbnails']>a")
	List<WebElement> rightleftImageArrows;

	@FindBy(css = "div[class='jcarousel']>ul")
	WebElement carouselStyle;

	@FindBy(css = "div[class='value'] div[class*='custom-select'] select")
	List<WebElement> drpSizeInProductSet;

	@FindBy(css = "div[class='product-add-to-cart'] button")
	WebElement btnAddAllToCart;

	@FindBy(xpath = ".//div[@class='product-name']")
	List<WebElement> lstProductNameInProductSet;

	@FindBy(xpath = ".//div[@class='product-review']")
	List<WebElement> lstProductReviewInProductSet;

	@FindBy(xpath = ".//span[@class='product-price']")
	List<WebElement> lstProductPriceInProductSet;

	@FindBy(css = "span[class='product-price']")
	WebElement productPrice;

	@FindBy(css = "a[data-action='wishlist']")
	List<WebElement> lstAddToWishListInProductSet;

	@FindBy(css = "li.attribute.variant-dropdown.size div.custom-select select")
	List<WebElement> lstGiftcardvalue;

	@FindBy(css = "li.attribute.variant-dropdown.size > div > div")
	List<WebElement> drpGiftcardvalue;

	@FindBy(css = "#product-set-list .product-set-item")
	List<WebElement> lstProductSet;

	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart")
	List<WebElement> btnAddToShoppingBag;

	@FindBy(css = "div[class='product-set-list']>div")
	public List<WebElement> childProductSets;

	@FindBy(css = "li[class='attribute variant-dropdown size']>div>div>div[class='selected-option']")
	public List<WebElement> childSizeDropDowns;

	@FindBy(css = DEFAULT_SELECTED_VALUE_DROPDOWN)
	List<WebElement> sizeDefaultValue;

	@FindBy(css = ".product-set-details .product-price>div")
	List<WebElement> priceSection;

	@FindBy(css = ".product-set-buyall")
	WebElement lblBuyAll;

	@FindBy(css = "div[class*='product-add-to-cart'] button")
	WebElement AddallToCart;

	@FindBy(css = ".loader[style*='block']")
	WebElement searchResultsSpinner;

	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart")
	WebElement addToShoppingBagForProductSet;

	@FindBy(css = "a[class='mini-cart-link']")
	WebElement BagForProductSet;

	@FindBy(css = "div[class='sku']")
	List<WebElement> UPCinBagPage;

	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart")
	WebElement AddToBag;

	@FindBy(css = ".inventory-error-msg.success")
	List<WebElement> lstWishListMessage;

	@FindBy(css = "div[class*='product-col-2 product-detail']")
	WebElement lblProductSet;

	@FindBy(css = ".brand-name")
	WebElement lblProductName;

	@FindBy(css = ".mini-cart-content")
	WebElement minicartcontent;

	@FindBy(css = ".mini-cart-diffPay.success")
	WebElement SuccessMessage;

	@FindBy(css = "div[id='mini-cart'] div[style='display: block;']")
	WebElement addedToMyBagPopUp;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement ClikOnViewBag;

	@FindBy(css = "ul.swatches.color>li.selectable")
	List<WebElement> lstListOfColor;

	@FindBy(css = "picture img")
	WebElement imgOfProduct;

	@FindBy(xpath = "//div[contains(@class,'product-image-container')]/ancestor::div[@id='primary']/preceding-sibling::div[@class='breadcrumb']")
	WebElement breadcrumbAboveMainImage;

	@FindBy(css = "div[class='message-qv']")
	WebElement onlineOnlyMessage;

	@FindBy(css = "div[class='quantity']")
	WebElement lblQuantity;

	@FindBy(css = "a[data-action='gift-registry']")
	List<WebElement> btnRegistrySigneduserInProductSet;

	@FindBy(css = "div[class='shipping-promotion']")
	WebElement shippingPromotion;

	@FindBy(css = ".coupons>a")
	WebElement lnkCoupons;

	@FindBy(css = ".mini-cart-products.custom-scrollbar")
	WebElement minicartOverLay;

	@FindBy(css = "div[class*='ui-draggable'][style*='block']")
	WebElement fldSizeDialogueBox;

	@FindBy(css = "div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front bonusproduct-modal ui-draggable']")
	WebElement selectFreeGiftModal;

	@FindBy(css = "button[class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']")
	WebElement btnCloseFreeGiftModal;

	@FindBy(css = "div[class='standardprice']>span")
	WebElement productPriceInPdp;
	
	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;

	@FindBy(css = "div[class='pdpgftmessage hide success']")
	WebElement txtSuccessMessage;

	@FindBy(css = "div[class*='pdp-oos-msg']")
	WebElement txtmessageoutofstock;

	@FindBy(css = ".mini-cart-content[style*='block']")
	WebElement minicartContent;

	@FindBy(css = "div[class*='ui-draggable'][style*='block']")
	WebElement fldFindStore;

	@FindBy(css = "div[class='tab-header hide-mobile'] label[class='tab-label']")
	WebElement txtDescriptionPdpdesktop;

	@FindBy(css = "div[class='copyline-tab']")
	WebElement txtProductDescription;

	@FindBy(css = ".breadcrumb")
	WebElement lblBreadCrump;

	@FindBy(css = "div[class='set-preferred-store'] i[class='sprite']")
	WebElement btnFindStore;

	@FindBy(css = ".share-icon.share-email")
	WebElement emailLinkInPdp;

	@FindBy(css = "span[class='ui-button-icon-primary ui-icon ui-icon-closethick']")
	WebElement btnCloseFindStore;

	@FindBy(css = "div[class='tab-content tab-1 active'] label[class='tab-label hide-desktop']")
	WebElement txtDescriptionPdpmobile;

	@FindBy(css = "html body")
	WebElement clickAnyWhereFindStore;

	@FindBy(css = "div[class='product-brand']")
	WebElement belkBrandName;

	@FindBy(css = "div[class='ui-widget-overlay ui-front']")
	WebElement clickAnyWhere;

	// Free gift message

	@FindBy(css = ".thumb-prodDetailLink")
	WebElement txtFreeGiftMessage;

	@FindBy(css = ".primary-logo>a")
	WebElement belkLogo;

//	@FindBy(css = ".standardprice>span")
//	WebElement productPriceInPdp;

	@FindBy(css = "a[class='item-name']")
	List<WebElement> txtChildProductNames;

	@FindBy(css = ".price-sales")
	List<WebElement> lblChildProductCurrentPrices;

	@FindBy(css = ".standardprice")
	List<WebElement> lblChildProductStandardPrices;

	@FindBy(css = "li[class='attribute color']>div>span")
	List<WebElement> lblselectedColors;

	@FindBy(css = "li[class='set-quantity']>div>div>div[class*='selected-option']")
	List<WebElement> qtyDefaultValue;

	@FindBy(css = ".product-UPC")
	List<WebElement> lblUPCs;

	@FindBy(css = ".add-to-cart-bonus")
	WebElement btnGiftSelect;

	@FindBy(css = "div[class='product-col-2 product-detail']>div[class='product-detail-info']>h3:nth-child(1)")
	WebElement giftProductName;

	@FindBy(css = ".bonus-product-title")
	WebElement lblGiftProduct;

	@FindBy(css = ".button.simple.close-dialog")
	WebElement lblNoThanks;

	@FindBy(css = ".size .selected-option.selected")
	WebElement txtSelectedSize;

	@FindBy(css = "div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front bonusproduct-modal ui-draggable']")
	WebElement giftProductModal;

	@FindBy(css = "div[class='product-primary-image']>.primary-image")
	WebElement imgGiftProduct;

	@FindBy(css = ".promotion-callout a.thumb-prodDetailLink")
	WebElement lblPromotionmessage;

	@FindBy(css = ".bonus-product-list .product-add-to-cart")
	List<WebElement> bonusProductlist;

	@FindBy(css = "span[class*='ui-button-icon-primary']")
	WebElement btnClosedBonusOffer;

	@FindBy(css = ".bonus-product-list h3.product-name")
	List<WebElement> bonusProductName;

	@FindBy(css = ".stock-product.size")
	WebElement txtmessagestock;

	@FindBy(css = "a[data-action='gift-registry']")
	WebElement addItemToRegistry;

	@FindBy(xpath = ".//*[@id='header']/div[2]/div/ul/li[3]/div/ul/li[2]/a")
	WebElement btnRegistryPdpPage;

	
	@FindBy(css = "a[data-action='gift-registry']")
	WebElement btnApplyButton;

	@FindBy(css = ".gft-dialog-close.button.simple")
	WebElement btnCancelOnRegistryModal;

	@FindBy(css = ".gftmessage.success.hide")
	WebElement txtSuccessApplyMessage;

	@FindBy(xpath = ".//*[@id='dialog-container']/h1")
	WebElement txtSelectRegistry;

	@FindBy(xpath = ".//*[@id='dialog-container']/p")
	WebElement txtSubtitleSelectRegistry;

	@FindBy(css = ".price-standard")
	WebElement txtOriginalPrice;

	@FindBy(css = ".price-sales")
	WebElement txtSalePrice;

	@FindBy(css = "#bonus-product-list div.bonus-product-bottom-msg a")
	WebElement lnkNotinterestedonBonus;

	@FindBy(css = "th.eventname")
	WebElement txtSelectModalEventName;

	@FindBy(css = ".pdp-rebate-details")
	WebElement msgPromotion;

	@FindBy(css = ".pdp-rebate-details>a")
	WebElement lnkPromotionViewDetail;

	@FindBy(css = "div[class='product-badge']")
	WebElement txtProductBadge;

	@FindBy(css = ".swatches.color .unselectable")
	WebElement greyedOutColor;

	@FindBy(css = "th.eventtype")
	WebElement txtSelectModalEventType;

	@FindBy(css = "th.eventdate")
	WebElement txtSelectModalEventDate;

	@FindBy(css = "div[class='mini-cart-content'][style*='block']")
	WebElement cartOverlay;

	@FindBy(css = "ul.swatches.color>li[class='selectable selected']>a>img")
	WebElement ImgSrcOfSelectedSwach;

	@FindBy(css = ".product-price")
	WebElement txtPrice;

	@FindBy(css = "#pdpMain .product-primary-image")
	WebElement imgInPdp;

	@FindBy(css = ".ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix.ui-draggable-handle>button>span[class='ui-button-icon-primary ui-icon ui-icon-closethick']")
	WebElement closeInPopUp;

	@FindBy(css = "	div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front bonusproduct-modal ui-draggable']>div[id='dialog-container']>div")
	WebElement freeGiftPopUp;

	@FindBy(css = ".button.simple.close-dialog")
	WebElement noThankMnotInterested;

	@FindBy(css = ".button-fancy-medium.sub-product-item.add-to-cart:not([disabled='disabled'])")
	WebElement btnAddToBagInGiftCardPage;

	@FindBy(css = "div[class='product-col-2 product-detail']")
	public WebElement regularProductPDP;

	@FindBy(css = ".mini-cart-subtotals")
	WebElement ordertotalinMiniCart;

	@FindBy(css = ".product-detail.product-set")
	public WebElement productSetPDP;

	@FindBy(css = "div[class*='sizechart-cont']")
	WebElement windowSizeChart;

	@FindBy(css = "div[class*='sizechart-dialog'][style*='block']")
	WebElement windowSizeChartOpened;

	@FindBy(css = "div[class*='sizechart-dialog'][style*='none']")
	WebElement windowSizeChartClosed;

	@FindBy(css = ".callout-message >a")
	WebElement lnkGwpModal;

	@FindBy(css = ".bonusproduct-modal:not([style*='display: none']) .choice-bonus-product")
	WebElement GWPmodalPopUp;

	@FindBy(css = ".change-preferred-store")
	WebElement btnChangePreferredStore;

	@FindBy(css = ".store-change-location")
	WebElement lnkChangeLocation;

	@FindBy(css = "#user-zip")
	WebElement txtZipcode;

	@FindBy(css = ".ui-dialog-buttonset button")
	WebElement btnSearchStore;

	@FindBy(css = ".select-store-button")
	WebElement btnSelectStore;

	@FindBy(css = ".store-address input[name='store-ship-radio']")
	WebElement rdoStoreShip;

	@FindBy(css = ".promotion .callout-message")
	WebElement calloutMessage;

	@FindBy(css = "div[class *='tab-3 active']")
	WebElement offersandRebatesTab;

	//	@FindBy(css = ".rebateavailable")
	//	WebElement lnkViewDetails;

	@FindBy(css = "div[class='pdp-rebate-details']")
	WebElement txtRebateMessage;

	@FindBy(css = ".promotion-callout .callout-message:nth-child(1) a.thumb-prodDetailLink")
	WebElement msgGWP;

	@FindBy(css = ".product-col-2.product-detail>div>div[class='promotion']>div>div[class='callout-message']")
	WebElement promoMessage;

	@FindBy(css = ".tab-label.tab-offers")
	WebElement offerAndRebateLink;

	@FindBy(css = ".disclaimer")
	WebElement disclaimer_Text;

	@FindBy(css = ".price-sales")
	WebElement productCostInPdpPage;

	@FindBy(css = "div[class='promotion-callout']>div[class='callout-message']")
	WebElement offerAndRebatePromoMessage;

	@FindBy(css = "div[id='primary']>div[class='pdp-main']>span>div>div[class='product-col-2 product-detail']>div>div[class='promotion']>div>a:nth-child(3)")
	WebElement ViewDetailsFirst;

	@FindBy(css = ".tab-title")
	WebElement offerAndRebateLinkMobile;

	@FindBy(css = "ul[class='swatches color']>li.selected>a>img")
	WebElement selectedColorInPDP;

	@FindBy(css= "div[class='quantity']>div>ul")
	WebElement lstQtydropdown;

	@FindBy(css=".callout-message:nth-child(1)")
	WebElement txtBogoMessage;

	@FindBy(css="div[class='choice-bonus-desc'] div[class='bold']")
	WebElement txtBonusDescription;

	@FindBy(css="img[alt='FREE Salt Life Cool Towel']")
	WebElement imgPopUPFreeOffer;

	@FindBy(css = "div[class *='tab-3 active']>h2")
	WebElement offersandRebateHeading;

	@FindBy(css = "div[class *='tab-3 active'] .callout-message")
	List<WebElement> offersandRebateMessage;

	@FindBy(css = "div[class *='tab-3 active'] .disclaimer")
	List<WebElement> offersandRebateDisclaimertxt;

	@FindBy(css = ".rebateavailable")
	List<WebElement> lnkViewDetails;

	@FindBy(css="div[style*='block'] #dialog-container")
	WebElement FindInstorePopUp;
	
	
	@FindBy(css=".change-preferred-store a")
	WebElement lnkChangeStoreAvailability;
	
	@FindBy(css=".store-heading a")
	WebElement lnkChangeYourLocation; 
	
	@FindBy(css=".store-heading")
	WebElement lnkStoreHeading;
	
	@FindBy(css = "#dialog-container #user-zip")
	WebElement InputtxtFieldEnterZipCode;
	
	//.ui-dialog-buttonset button[class*='ui-button-text-only']
	@FindBy(css = ".ui-dialog-buttonset button[class*='ui-button-text-only']")
	WebElement btnSearchZipCode;
	
	@FindBy(css = ".store-heading")
	WebElement storeHeading;
	
	@FindBy(css = ".store-tile")
	WebElement storeResult;
	
	@FindBy(css = ".tab-header >label[for='tab-5']")
	WebElement reviewTab;
	
	/**********************************************************************************************
	 ********************************* WebElements of PDP Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public PdpPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, pdPContent))) {
			Log.fail("PDP Page did not open up. Site might be down.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		minicart = new MiniCartPage(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(2);
		Utils.waitUntilElementDisappear(driver, PDPspinner);
		BrowserActions.nap(2);
	}

	/**
	 * To get the product name in the bread crumbs
	 * 
	 * @return txtLastBreadCrumb
	 * @throws Exception
	 */
	public String getProductNameFromBreadCrumb() throws Exception {
		String txtLastBreadCrumb = BrowserActions.getText(driver,
				bcLastCategoryName,
				"fetching text from last bread crumb in pdp page");
		return txtLastBreadCrumb;
	}

	/**
	 * To get the brand name
	 * 
	 * @return txtProductBrandName
	 * @throws Exception
	 */
	public String getProductBrandName() throws Exception {
		String txtProductBrandName = BrowserActions.getText(driver,
				lnkBrandName, "Product Brand Name").trim();
		return txtProductBrandName;
	}

	/**
	 * To get the product name
	 * 
	 * @return txtProductName
	 * @throws Exception
	 * Last Modified by : Nandhini 04/02/2017
	 */
	public String getProductName() throws Exception {
		String txtProductName = null;

		/*WebElement productName = lblProductSet.findElement(By
				.cssSelector("div"));*/
		/*if (productName.getAttribute("class").contains("product-brand")) {

		 * if(BrowserActions.getText(driver, fldProductName, "product name"
		 * ).contains("\\n")){ txtProductName =
		 * BrowserActions.getText(driver, fldProductName, "product name"
		 * ).trim().split("\\n")[1]; }else{ txtProductName =
		 * BrowserActions.getText(driver, fldProductName, "product name"
		 * ).trim(); }

			txtProductName = fldProductName.getText().replace(fldProductName.findElement(By.cssSelector(".product-brand")).getText(),"").trim();
		} else {
			txtProductName = BrowserActions.getText(driver, fldProductName,
					"product name").trim();
		}*/

		txtProductName = driver.findElement(By.cssSelector(".brand-name")).getText().trim();

		return txtProductName;
	}

	/**
	 * To select the size from size drop down
	 * 
	 * @param
	 * @return selectedSize
	 * @throws Exception
	 */
	public String selectSize(String... size) throws Exception {
		String selectedSize = null;

		// checking size drop down have one value or more that one
		WebElement sizeDefaultValue = BrowserActions.checkLocator(driver,
				DEFAULT_SELECTED_VALUE_DROPDOWN);
		BrowserActions.scrollToViewElement(sizeDefaultValue, driver);
		if (!BrowserActions
				.getText(driver, sizeDefaultValue,
						"Size drop down selected option").trim().toLowerCase()
						.contains("select")) {
			return BrowserActions.getText(driver, sizeDefaultValue,
					"Size drop down selected option");
		}
		// clicking size drop down
		BrowserActions.clickOnElement(sizeDefaultValue, driver,
				"Clicked Size Button");
		BrowserActions.nap(1);

		List<WebElement> listOfSize = BrowserActions.checkLocators(driver,
				LIST_OF_OPTIONS_IN_DROPDOWN);
		if (size.length > 0) {
			String optionValue = size[0].toString();
			WebElement sizeoption = BrowserActions
					.getMachingTextElementFromList(listOfSize, optionValue,
							"equals");
			BrowserActions.clickOnElement(sizeoption, driver,
					"Size drop down options");
			waitForSpinner();
			selectedSize = driver
					.findElement(
							By.cssSelector("div[class='value'] div[class*='selected-option']"))
							.getText();

		} else if (listOfSize.size() >= 0) {
			int rand = Utils.getRandom(0, listOfSize.size());
			BrowserActions.clickOnElement(listOfSize.get(rand), driver,
					"Select size option");
			waitForSpinner();
			// In future it will be updated
			selectedSize = driver.findElement(
					By.cssSelector(DEFAULT_SELECTED_VALUE_DROPDOWN)).getText();
			// selectedSize = BrowserActions.getText(driver, sizeDefaultValue,
			// "Size drop down selected option");
		} else {
			throw new Exception("Size drop down didn't have no values");
		}
		Utils.waitForPageLoad(driver);
		return selectedSize;
	}

	/**
	 * To get the list of size options
	 * 
	 * @return listofsizeoption
	 * @throws Exception
	 */
	public List<String> getSizeOptions() throws Exception {
		List<String> listofsizeoption = new ArrayList<String>();

		// checking size drop down have one value or more that one
		WebElement sizeDefaultValue = BrowserActions.checkLocator(driver,
				DEFAULT_SELECTED_VALUE_DROPDOWN);
		// clicking size drop down
		BrowserActions.clickOnElement(sizeDefaultValue, driver,
				"Clicked Size Button");
		BrowserActions.nap(1);

		List<WebElement> listOfSize = BrowserActions.checkLocators(driver,
				LIST_OF_OPTIONS_IN_DROPDOWN);
		for (WebElement e : listOfSize) {
			listofsizeoption.add(BrowserActions.getText(driver, e,
					"Size drop down option"));
		}
		return listofsizeoption;
	}

	/**
	 * To select the color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public String selectColor() throws Exception {
		String selectedColorName = null;
		List<WebElement> listOfColor = driver.findElements(By
				.cssSelector(LIST_OF_COLOR));

		if ((listOfColor.size() == 0)) {
			WebElement defaultColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			BrowserActions.scrollToViewElement(defaultColor, driver);
			return selectedColorName = BrowserActions.getTextFromAttribute(
					driver, defaultColor, "alt", "Alt attribute value");
		}

		if (listOfColor.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0,
					listOfColor.size());
			BrowserActions.scrollToViewElement(listOfColor.get(rand), driver);
			BrowserActions.clickOnElement(listOfColor.get(rand), driver,
					"select color");
			waitForSpinner();
			WebElement selectedColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			selectedColorName = BrowserActions.getTextFromAttribute(driver,
					selectedColor, "alt", "Alt attribute value");
		} else {
			throw new Exception(
					"Color swatched not available (or) not selectable in the UI");
		}
		Utils.waitForPageLoad(driver);
		return selectedColorName;

	}

	public String selectQuantity(String... qty) throws Exception {
		// checking size drop down have one value or more that one
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver,
				SELECTED_QTY);
		BrowserActions.scrollToViewElement(qtyDefaultValue, driver);
		if (qty.length > 0) {
			BrowserActions.clickOnElement(qtyDefaultValue, driver,
					"Clicked Size Button");
			BrowserActions.nap(1);
			try {
				List<WebElement> listOfqty = driver.findElements(By
						.cssSelector("div[class='quantity'] ul>li"));
				if (qty[0].equalsIgnoreCase("random")) {
					int rand = Utils.getRandom(0, listOfqty.size());
					BrowserActions.clickOnElement(listOfqty.get(rand), driver,
							"Select size option");

				} else if (listOfqty.size() > 0) {
					String optionValue = qty[0].toString();
					WebElement qtyoption = BrowserActions
							.getMachingTextElementFromList(listOfqty,
									optionValue, "equals");
					BrowserActions.clickOnElement(qtyoption, driver,
							"Size drop down options");

				}
			} catch (Exception e) {
				Log.event("Quantity is selected as default");
			}
		}
		waitForSpinner();
		return qtyDefaultValue.getText();
		// return BrowserActions.checkLocator(driver,
		// DEFAULT_SELECTED_VALUE_DROPDOWN).getText();
	}

	/*
	 * public String selectQuantity(String... qty) throws Exception { String
	 * selectedSize = null;
	 * 
	 * // checking size drop down have one value or more that one WebElement
	 * qtyDefaultValue = BrowserActions.checkLocator(driver, SELECTED_QTY);
	 * BrowserActions.scrollToViewElement(qtyDefaultValue, driver); if
	 * (!BrowserActions .getText(driver, qtyDefaultValue,
	 * "Size drop down selected option").trim().toLowerCase()
	 * .contains("select")) { return BrowserActions.getText(driver,
	 * qtyDefaultValue, "Size drop down selected option"); } // clicking size
	 * drop down BrowserActions.clickOnElement(qtyDefaultValue, driver,
	 * "Clicked Size Button"); BrowserActions.nap(1);
	 * 
	 * List<WebElement> listOfqty = BrowserActions.checkLocators(driver,
	 * "div[class='quantity'] ul>li"); if (qty.length > 0) { String optionValue
	 * = qty[0].toString(); WebElement qtyoption = BrowserActions
	 * .getMachingTextElementFromList(listOfqty, optionValue, "equals");
	 * BrowserActions.clickOnElement(qtyoption, driver, "Size drop down options"
	 * ); waitForSpinner(); selectedSize = driver .findElement( By.cssSelector(
	 * "div[class='value'] div[class='selected-option']")) .getText();
	 * 
	 * } else if (listOfqty.size() >= 0) { int rand =
	 * ThreadLocalRandom.current().nextInt(0, listOfqty.size());
	 * BrowserActions.clickOnElement(listOfqty.get(rand), driver,
	 * "Select size option"); waitForSpinner(); // In future it will be updated
	 * selectedSize = driver.findElement(
	 * By.cssSelector(DEFAULT_SELECTED_VALUE_DROPDOWN)).getText(); //
	 * selectedSize = BrowserActions.getText(driver, sizeDefaultValue, //
	 * "Size drop down selected option"); } else { throw new Exception(
	 * "Size drop down didn't have no values"); } Utils.waitForPageLoad(driver);
	 * return selectedSize; }
	 */

	/**
	 * To get the list of size options
	 * 
	 * @return listofsizeoption
	 * @throws Exception
	 */
	public List<String> getQuantityOptions() throws Exception {
		List<String> listofquantityoption = new ArrayList<String>();

		// checking size drop down have one value or more that one
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver,
				SELECTED_QTY);
		BrowserActions.clickOnElement(qtyDefaultValue, driver,
				"Quantity drop down");
		BrowserActions.nap(2);
		List<WebElement> listOfSize = BrowserActions.checkLocators(driver,
				LIST_OF_OPTIONS_IN_DROPDOWN);
		for (WebElement e : listOfSize) {
			listofquantityoption.add(BrowserActions.getText(driver, e,
					"Quantity drop down option"));
		}
		Utils.waitForPageLoad(driver);
		return listofquantityoption;
	}

	/**
	 * To click the add to bag button
	 * 
	 * @throws Exception
	 */
	public void clickAddToBag(String... productSet) throws Exception {
		waitForSpinner();
		WebElement element = null;
		if (productSet.length > 0) {
			element = addAllToShoppingBagForProductSet;
		} else {
			element = btnAddtoBag;
		}
		Utils.waitForElement(driver, element);
		BrowserActions.scrollToViewElement(element, driver);
		BrowserActions.javascriptClick(element, driver, "Add to bag button");
		// Utils.waitForPageLoad(driver);
		// BrowserActions.nap(5);
		Utils.waitUntilElementDisappear(driver, minicartContent);
	}

	/**
	 * To click the add to bag button
	 * 
	 * @throws Exception
	 */
	public void clickAddToBagButton() throws Exception {
		Utils.waitForElement(driver, btnAddtoBag);
		BrowserActions.scrollToViewElement(btnAddtoBag, driver);
		BrowserActions
		.javascriptClick(btnAddtoBag, driver, "Add to bag button");
		Utils.waitForElement(driver, minicartContent);
	}

	/**
	 * To navigating to shopping bag page by clicking 'View Bag' in mini cart
	 * 
	 * @return ShoppingBagPage
	 * @throws Exception
	 */
	public ShoppingBagPage navigateToBagPage(String... successMessage)
			throws Exception {
		if (!(successMessage.length > 0))
			mouseOverMiniCart();
		BrowserActions.scrollToViewElement(ClikOnViewBag, driver);
		BrowserActions.clickOnElement(ClikOnViewBag, driver,
				"Add to bag button");
		return new ShoppingBagPage(driver).get();
	}

	public String addProductToBag(String... qtyValue) throws Exception {
		String colorSizeQuantityUpcPriceValue = null;
		try {
			BrowserActions.scrollToView(btnAddtoBag, driver);
		} catch (Exception e) {
			BrowserActions.scrollToView(addAllToShoppingBagForProductSet,
					driver);
		}
		try {
			while (btnAddtoBag.getAttribute("disabled").equals("true")
					|| addAllToShoppingBagForProductSet.getAttribute("class")
					.contains("disabled")) {
				colorSizeQuantityUpcPriceValue = selectColor();
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + selectSize();
				if (qtyValue.length > 0) {
					colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
							+ "|" + selectQuantity(qtyValue);
				} else {
					colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
							+ "|1";
				}
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + getUPCValue();
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + getProductPrice();
			}
		} catch (Exception e) {
			colorSizeQuantityUpcPriceValue = getSelectedColor().replace(
					"Color: ", "");
			try {
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + getSelectedSize();
			} catch (Exception e2) {
				Log.event("No size available");
			}
			if (qtyValue.length > 0) {
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + selectQuantity(qtyValue);
			} else {
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|1";
			}
			try {
				colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
						+ "|" + getUPCValue();
			} catch (Exception e2) {
				// TODO: handle exception
			}
			colorSizeQuantityUpcPriceValue = colorSizeQuantityUpcPriceValue
					+ "|" + getProductPrice();
		}
		clickAddToBag();
		Utils.waitForPageLoad(driver);
		return colorSizeQuantityUpcPriceValue;
	}

	// ************************ Mini Bag**************************************//

	/**
	 * To mouse hover to mini cart
	 * 
	 * @throws Exception
	 */
	public void mouseOverMiniCart() throws Exception {
		BrowserActions.nap(10);
		if (runPltfrm == "desktop")
			BrowserActions.mouseHover(driver, miniCart);
		else if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(miniCart, driver, "mini Cart icon");
		}
	}

	// Pdp Page

	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To click and navigate to a category in breadcrumb
	 * 
	 * @param categoryName
	 * @throws Exception
	 */
	public PlpPage clickCategoryByNameInBreadcrumb(String categoryName)
			throws Exception {
		for (WebElement element : lstTxtProductInBreadcrumb) {
			if (element.getText().equalsIgnoreCase(categoryName)) {
				BrowserActions.clickOnElement(element, driver,
						"Category in breadcrumb");
				break;
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * To verify Primary image As default
	 * 
	 * @return status
	 * @throws Exception
	 */
	public boolean verifyPrimaryImageAsDefault() throws Exception {
		boolean status = false;
		BrowserActions.scrollToViewElement(lstImageThumbnail.get(0), driver);
		if (lstImageThumbnail.get(0).getAttribute("src")
				.equals(primaryImage.getAttribute("src"))) {
			status = true;
		}
		return status;
	}

	/**
	 * Click "Add to wishlist" link in the PDP page
	 * 
	 */
	public void clickAddToWishListLink() throws Exception {
		BrowserActions.scrollToViewElement(lnkAddToWishList, driver);
		Utils.waitForElement(driver, lnkAddToWishList);
		// if
		// (lnkAddToWishList.findElement(By.xpath("..")).getAttribute("class")
		// .contains("disabled")) {
		selectColor();
		selectSize();
		// }
		BrowserActions.clickOnElement(lnkAddToWishList, driver,
				"Add to wishList link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click Size dropdown
	 * 
	 * @throws Exception
	 */
	public void clickSizeDropown() throws Exception {
		BrowserActions.scrollToViewElement(drpSize, driver);
		BrowserActions.clickOnElement(drpSize, driver, "Size dropdown");
	}

	/**
	 * To get the title of Privacypolicy
	 * 
	 * @throws Exception
	 */
	public String getTitleofPrivacyPage() throws Exception {
		return BrowserActions.getText(driver, lblPrivacyPolicy,
				"Privacy policy header");
	}

	/**
	 * To click the size chart link
	 * 
	 * @throws Exception
	 */
	public void clickSizeChartLink() throws Exception {
		try {
			BrowserActions.scrollToViewElement(lnkSizechart, driver);
			BrowserActions.clickOnElement(lnkSizechart, driver, "Sizechart");
			Utils.waitForElement(driver, iconCloseSizeChart);
		} catch (Exception e) {
			throw new Exception(
					"Size chart link is not available in the PDP Page");
		}
	}

	/**
	 * To click the privacy policy link
	 * 
	 * @throws Exception
	 */
	public PrivacyAndPolicyPage clickPrivacyPolicyLink() throws Exception {
		BrowserActions.scrollToView(lnkPrivacyPolicy, driver);
		BrowserActions.javascriptClick(lnkPrivacyPolicy, driver,
				"Privacypolicy");
		return new PrivacyAndPolicyPage(driver).get();
	}

	/**
	 * To close the size chart by closs button
	 * 
	 * @throws Exception
	 */
	public void closeSizeChartByCloseButton() throws Exception {
		BrowserActions.scrollToView(iconCloseSizeChart, driver);
		BrowserActions.javascriptClick(iconCloseSizeChart, driver,
				"close the size chart link");
		Utils.waitForElement(driver, lnkSizechart);
	}

	/**
	 * To close the size chart by other element
	 * 
	 * @throws Exception
	 */
	public void closeSizechartByClickingOtherElement(HomePage homePage)
			throws Exception {
		BrowserActions.nap(3);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0);");
		BrowserActions.actionClick(homePage.txtSearch, driver, "");
		BrowserActions.nap(1);
	}

	/**
	 * To close the size chart by escape key
	 * 
	 * @throws Exception
	 */
	public void closeSizechartByEsckey() throws Exception {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).build().perform();
	}

	/**
	 * To Click the brand name in pdp page
	 * 
	 * @throws Exception
	 */
	public SearchResultPage clickBrandname() throws Exception {
		BrowserActions.clickOnElement(lnkBrandName, driver, "Brand link");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To verify the color selected and the text above the color swatches are
	 * matched
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifySelectColor() throws Exception {
		boolean status = false;
		BrowserActions.clickOnElement(lblColorSelect, driver,
				"Color is selected");
		if (lblColorSelect.getAttribute("alt").equals("Black")) {
			status = true;
		}
		return status;
	}

	/**
	 * To get the text from brandlink
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getTextFromBrand() throws Exception {
		return BrowserActions.getText(driver, lnkBrandName,
				"Text from Brandname");

	}

	/**
	 * To get the text from breadcrump page
	 * 
	 * @return String
	 * @throws Exception
	 */

	public String getTextFromBreadCrumb() throws Exception {
		return BrowserActions.getText(driver, lblBreadCrumpBrand,
				"Text from breadcrumb");

	}

	/**
	 * To verify the color selected and the text above the color swatches are
	 * matched
	 * 
	 * @throws Exception
	 */
	public boolean verifySelectedColorIsDisplayed() throws Exception {
		boolean status = false;
		for (WebElement element : lstListOfColor) {
			if (element.getAttribute("class").contains("selected")) {
				if (lblColorSelect.getText().equals(
						element.findElement(By.cssSelector("img"))
						.getAttribute("alt"))) {
					status = true;
					break;
				}
			}
		}
		return status;
	}

	/**
	 * To get the product name by index
	 * 
	 * @return String
	 */
	public String getProductImageUrl() {
		return imgProduct.getAttribute("src");

	}

	/**
	 * To get the list of brand names
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getBrandNameInProductlist() throws Exception {

		ArrayList<String> arrlist = new ArrayList<String>();
		for (WebElement element : lstBrandName) {
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from breadcrumb"));
		}
		return arrlist;

	}

	/**
	 * To navigating to registry signed user page after clicking add to registry
	 * button
	 * 
	 * @return RegistrySignedUserPage
	 * @throws Exception
	 */
	public RegistrySignedUserPage navigateToRegistryAsSignedUser()
			throws Exception {
		Utils.waitForElement(driver, btnRegistrySigneduser);
		clickOnRegistrySign();
		return new RegistrySignedUserPage(driver).get();
	}

	public void clickOnRegistrySign() throws Exception {
		Utils.waitForElement(driver, btnRegistrySigneduser);
		BrowserActions.clickOnElement(btnRegistrySigneduser, driver,
				"click on registry button as a signed user");

	}

	public String getTextFromAddedRegistry() throws Exception {
		String txtAddedRegistry = BrowserActions.getText(driver,
				lblsuccessMessage, "added message");
		return txtAddedRegistry;
	}

	/**
	 * To navigating to pintrest page after clicking pintrest
	 * 
	 * @return PintrestPage
	 * @throws Exception
	 */
	public PintrestPage naviagteToPintrest() throws Exception {
		BrowserActions.clickOnElement(lnkPintrest, driver, "Link to pintrest");
		return new PintrestPage(driver).get();
	}

	/**
	 * To navigating to sign in page after clicking add to registry button
	 * 
	 * @return RegistryGuestUserPage
	 * @throws Exception
	 */
	public RegistryGuestUserPage navigateToRegistryAsGuest() throws Exception {
		Utils.waitForElement(driver, btnRegistryAsGuest);
		BrowserActions.clickOnElement(btnRegistryAsGuest, driver,
				"click on registry button as a guest");
		return new RegistryGuestUserPage(driver).get();
	}

	/**
	 * To select the each color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public int getColorSwatchCount() throws Exception {
		return lstColorSelectable.size();
	}

	/**
	 * 
	 * To select color based on index
	 * 
	 * @param index
	 * 
	 * @throws Exception
	 */
	public void selectColorByIndex(int index) throws Exception {
		try {
			Utils.waitForElement(driver, lstColor.get(index - 1));
			BrowserActions.clickOnElement(lstColor.get(index - 1), driver,
					"Color");
			Utils.waitUntilElementDisappear(driver, pdpSpinner);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * To get color src value of primary image
	 * 
	 * @return String
	 */
	public String getPrimaryImageColor() throws Exception {
		return imgPrimary.getAttribute("src").split("&layer")[0];

	}

	/**
	 * To get color src value of swatches by index
	 * 
	 * @param index
	 * @return String
	 */
	public String getSelectedSwatchColor(int index) throws Exception {

		return driver.findElements(By.cssSelector("ul.swatches.color>li"))
				.get(index - 1).findElement(By.cssSelector("img"))
				.getAttribute("src").split("&crop")[0];

	}

	public String getSelectedColor() throws Exception {
		List<WebElement> listOfColor;
		if (Utils.waitForElement(driver, dialogSizechart)) {
			listOfColor = driver.findElements(By
					.cssSelector(SELECTED_COLOR_ZOOM_WINDOW));
		} else {
			listOfColor = driver.findElements(By.cssSelector(SELECTED_COLOR));
		}

		String selectedColorName = null;
		if ((listOfColor.size() == 1)) {
			WebElement defaultColor = BrowserActions.checkLocator(driver,
					SELECTED_COLOR);
			BrowserActions.scrollToViewElement(defaultColor, driver);
			selectedColorName = BrowserActions.getTextFromAttribute(driver,
					defaultColor, "alt", "Alt attribute value");
			return selectedColorName;
		} else {
			// if (listOfColor.size() > 0) {
			// int rand = ThreadLocalRandom.current().nextInt(0,
			// listOfColor.size());
			// BrowserActions.scrollToViewElement(listOfColor.get(rand),
			// driver);
			// BrowserActions.clickOnElement(listOfColor.get(rand), driver,
			// "select color");
			// waitForSpinner();
			// WebElement selectedColor = BrowserActions.checkLocator(driver,
			// SELECTED_COLOR);
			// selectedColorName = BrowserActions.getTextFromAttribute(driver,
			// selectedColor, "alt", "Alt attribute value");
			// } else {
			throw new Exception(
					"Color swatched not available (or) not selectable in the UI");
		}
	}

	public String getSelectedSize() throws Exception {
		BrowserActions.scrollToView(txtSelectedSize, driver);
		return BrowserActions.getText(driver, txtSelectedSize, "Selected Size");
	}

	public String getSelectedQuantity() throws Exception {
		return BrowserActions.getText(driver,
				driver.findElement(By.cssSelector(SELECTED_QTY)),
				"Selected Quantity");
	}

	public String getMouseHoveredColorByRow(int i) throws Exception {
		Actions action = new Actions(driver);
		action.moveToElement(
				lstColor.get(i - 1).findElement(By.cssSelector("a"))).build()
				.perform();
		return lstColor.get(i - 1).getCssValue("color");
	}

	public String getHighlightedColor() throws Exception {
		Actions action = new Actions(driver);
		WebElement element = lstColorSelectable.get(0).findElement(
				By.cssSelector("a"));
		action.moveToElement(element).build().perform();

		return element.getCssValue("border-color");
	}

	// /**
	// * To select the eachcolor from color swatches
	// *
	// * @return selectedColorName
	// * @throws Exception
	// */
	// public int getColorSwatchCount() throws Exception {
	// return lstColor.size();
	// }

	// /**
	// * To get color src value of primary image
	// * @return String
	// */
	// public String getPrimaryImageColor() throws Exception {
	// return imgPrimary.getAttribute("src").split("&layer")[0];
	// }

	// /**
	// * To get color src value of swatches by index
	// * @param index
	// * @return String
	// */
	// public String getSelectedSwatchColor(int index) throws Exception {
	// return
	// lstColor.get(index-1).findElement(By.cssSelector("img")).getAttribute("src").split("&crop")[0];
	// }

	public boolean verifyElementType(String actualElementType) {
		return actualElementType.contains(drpQuantity.getTagName());
	}

	// /**
	// * To get color src value of swatches by index
	// *
	// * @param index
	// * @return String
	// */
	// public String getSelectedSwatchColor(int index) throws Exception {
	//
	// return lstColor.get(index - 1).findElement(By.cssSelector("img"))
	// .getAttribute("src").split("&crop")[0];
	//
	// }

	/**
	 * 
	 * To select size based on index
	 * 
	 * @param index
	 * 
	 * @throws Exception
	 */

	public void selectSizeByIndex(int index) throws Exception {
		clickSizeDropown();
		BrowserActions.clickOnElement(lstSize.get(index - 1), driver, "Size");
		Utils.waitUntilElementDisappear(driver, spinner);
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To get product Price
	 * 
	 * @return String Last modified By - Anjana & Date modified - 1/31/2017
	 */

	public String getProductPrice() {
		String productPrices;
		if (productPrice.getText().contains("Now")) {
			productPrices = lblProductNowPrice.getText().replace("Now $", "");

		} else {
			productPrices = lblProductStandardPrice.getText().replace("$", "");
		}
		return productPrices.trim();
	}

	public String getStandardPrice() {
		//		String standardprice = standardPrice.getText().toString();
		//		if (standardprice.length() > 6 && standardprice.contains(" ")) {
		//			standardprice = standardprice.split(" ", 2)[0];
		//		}
		//		return standardprice;

		String standardprice = standardPrice.getText().toString();
		if(standardprice.contains("Orig")){
			standardprice=standardprice.replace("Orig. $", "");
		}else{
			standardprice=standardprice.replace("$", "");
		}
		return standardprice.trim();
	}

	public void selectQtyByIndex(int index) throws Exception {
		clickQtyDropown();
		BrowserActions
		.clickOnElement(lstQty.get(index - 1), driver, "Quantity");
		// driver.findElement(By.cssSelector(".quantity
		// ul[class='selection-list']>li:nth-child("+index+")")).click();
		Utils.waitUntilElementDisappear(driver, spinner);
	}

	/**
	 * To click Size dropdown
	 * 
	 * @throws Exception
	 */
	public void clickQtyDropown() throws Exception {
		BrowserActions.clickOnElement(drpQty, driver, "Size dropdown");
	}

	/**
	 * To select the each color from color swatches
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public int getColorSwatchCount(String... allColorSwatches) throws Exception {
		List<WebElement> lstElements = null;
		if (allColorSwatches.length > 0) {
			lstElements = listColor;
		} else {
			lstElements = lstColor;
		}
		return lstElements.size();
	}

	/**
	 * 
	 * To select color based on index
	 * 
	 * @param index
	 * 
	 * @throws Exception
	 */
	public void selectColorByIndex(int index, String... allColorSwatches)
			throws Exception {
		List<WebElement> lstElements = null;
		if (allColorSwatches.length > 0) {
			lstElements = listColor;
		} else {
			lstElements = lstColor;
		}
		BrowserActions.clickOnElement(lstElements.get(index - 1), driver,
				"Color");
		Utils.waitUntilElementDisappear(driver, pdpSpinner);
	}

	/**
	 * To get color src value of swatches by index
	 * 
	 * @param index
	 * @return String
	 */
	public String getSelectedSwatchColor(int index, String... allColorSwatches)
			throws Exception {
		List<WebElement> lstElements = null;
		if (allColorSwatches.length > 0) {
			lstElements = listColor;
		} else {
			lstElements = lstColor;
		}
		return lstElements.get(index - 1).findElement(By.cssSelector("img"))
				.getAttribute("src").split("&crop")[0];

	}

	public int getImageThumbnailsCount() throws Exception {
		return thumbsInContainer.size();
	}

	public void clickToEnlarge() throws Exception {
		Utils.waitForElement(driver, lnkClickToZoom);
		BrowserActions.clickOnElement(lnkClickToZoom, driver,
				"Clicking 'Click To Zoom' Link");
	}

	public String[] getImageFromZoomModal() throws Exception {
		Utils.waitForElement(driver, clickToZoomThumbSelected);
		Utils.waitForElement(driver, clickToZoomPrimaryImage);

		String[] srcToReturn = new String[2];
		srcToReturn[0] = clickToZoomThumbSelected.getAttribute("src").split(
				"&layer")[0].trim();
		srcToReturn[1] = clickToZoomPrimaryImage.getAttribute("src").split(
				"&layer")[0].trim();
		return srcToReturn;
	}

	public String getImageOfProductSetByRow(int index) throws Exception {
		return productSet.findElements(By.cssSelector(".product-thumbnail"))
				.get(index - 1).getAttribute("src").split("_")[0].trim();
	}

	public void clickProductSetBadge() throws Exception {
		BrowserActions.clickOnElement(productBadge, driver,
				"Clicked on Product badge");
	}

	public void verifyProductMouseHover() throws Exception {
		BrowserActions.scrollToViewElement(imgPrimary, driver);
		BrowserActions.mouseHover(driver, imgPrimary);
	}

	public EnlargeViewPage verifyClickToEnlarge() throws Exception {
		BrowserActions.scrollToViewElement(lnkClickToEnlarge, driver);
		BrowserActions.clickOnElement(lnkClickToEnlarge, driver,
				"Click To Enalrge Link");
		return new EnlargeViewPage(driver).get();
	}

	public int getAllColorSwatchCount() throws Exception {
		return lstColorSwatches.size();
	}

	public String getSelectedSwatchColors(int index) throws Exception {
		BrowserActions.clickOnElement(lstSelectColor.get(index - 1), driver,
				"Select Color swatches");
		waitForSpinner();
		Log.message(lstSelectColor.get(index - 1).getAttribute("src")
				.split("&crop")[0]);
		return lstSelectColor.get(index - 1).getAttribute("src").split("&crop")[0];
	}

	/**
	 * To get the count of thumbnail images
	 * 
	 * @return int
	 */
	public int getCountOfThumbnailImages() throws Exception {
		return lstThumbnailImages.size();
	}

	/**
	 * To click on image Next and Previous button in Quick view popup
	 * 
	 * @param navType
	 *            - 'Next' or 'Previous'
	 */
	public void clickImageNavArrowButton(String navType) throws Exception {
		WebElement element = null;
		if (navType.equals("Next")) {
			element = btnNextImage;
		} else if (navType.equals("Previous")) {
			element = btnPreviousImage;
		}
		BrowserActions.clickOnElement(element, driver, "Image nav button");
		try {
			Utils.waitUntilElementDisappear(driver, lstThumbnailImages.get(0),
					5);
		} catch (Exception e) {
			Log.event("Waiting after clicking");
		}
	}

	/**
	 * To mouse hover and verify the color is highlighted
	 * 
	 */
	public boolean verifyColorOfMouseHoverForNonSelectedSwathches()
			throws Exception {
		boolean status = false;
		String txt1 = lstColor.get(0).findElement(By.cssSelector("a"))
				.getCssValue("color");
		Log.message(txt1);
		for (int i = 0; i < lstColor.size(); i++) {
			BrowserActions.mouseHover(driver, lstColor.get(i));
			status = Utils.verifyCssPropertyForElement(lstColor.get(i),
					"color", "rgba(0, 0, 0, 1)");

		}

		return status;
	}

	public boolean verifyColorOfMouseHoverForSelectedSwatch() throws Exception {
		boolean status = false;
		String txt1 = selectedColor.findElement(By.xpath("..")).getCssValue(
				"border-color");
		Log.message(txt1);

		status = Utils.verifyCssPropertyForElement(
				selectedColor.findElement(By.xpath("..")), "border-color",
				"rgb(237, 103, 0)");
		return status;
	}

	public FacebookPage navigateToFacebookPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkFacebook, driver);
		BrowserActions.clickOnElement(lnkFacebook, driver,
				"clicking Social Media Facebook Icon");
		Utils.waitForPageLoad(driver);
		return new FacebookPage(driver);
	}

	public TwitterPage navigateToTwitterPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkTwitter, driver);
		BrowserActions.clickOnElement(lnkTwitter, driver,
				"clicking Social Media Twitter Icon");
		return new TwitterPage(driver);
	}

	public GooglePlusPage navigateToGooglePlusPage() throws Exception {

		BrowserActions.clickOnElement(lnkGooglePlus, driver,
				"clicking Social Media GooglePlus Icon");
		return new GooglePlusPage(driver);
	}

	public PintrestPage navigateToPinterestPage() throws Exception {

		BrowserActions.clickOnElement(lnkPintrest, driver,
				"clicking Social Media Pinterest Icon");
		return new PintrestPage(driver);
	}

	public void clickLeftImageArrow() throws Exception {
		if (!rightleftImageArrows.get(0).getAttribute("class")
				.contains("inactive")) {
			BrowserActions.clickOnElement(rightleftImageArrows.get(0), driver,
					"Clicked on Left Arrow");
		}

	}

	public void clickRightImageArrow() throws Exception {
		if (!rightleftImageArrows.get(1).getAttribute("class")
				.contains("inactive")) {
			BrowserActions.clickOnElement(rightleftImageArrows.get(1), driver,
					"Clicked on Right Arrow");
		} else {
			throw new Exception("The right image nav arrow is not available");
		}

	}

	public String getCarouselStyle() {
		return carouselStyle.getAttribute("style");
	}

	/**
	 * To Select all the drop
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean selectSizeDropDownInProductSet() throws Exception {
		boolean status = false;
		if (btnAddAllToCart.getAttribute("class").contains("disabled")) {
			for (int i = 0; i < drpSizeInProductSet.size(); i++) {
				Select dropdown = new Select(drpSizeInProductSet.get(i));
				dropdown.selectByIndex(1);
				status = true;

			}
		}
		return status;

	}

	public boolean verifyRateAndReviewBeforeSelectSize() throws Exception {
		boolean status = false;
		for (int i = 0; i < lstProductNameInProductSet.size(); i++) {
			if (Utils
					.waitForElement(
							driver,
							lstProductNameInProductSet
							.get(i)
							.findElement(
									By.xpath("following-sibling::div[@class='product-review']")))
									&& Utils.waitForElement(
											driver,
											lstProductNameInProductSet
											.get(i)
											.findElement(
													By.xpath("following-sibling::span[@class='product-price']")))) {

				status = true;
			}
		}

		return status;
	}

	public boolean verifyRateAndReviewAfterSelectSize() throws Exception {
		boolean status = false;
		for (int i = 0; i < lstProductReviewInProductSet.size(); i++) {
			if (Utils
					.waitForElement(
							driver,
							lstProductReviewInProductSet
							.get(i)
							.findElement(
									By.xpath("preceding-sibling::div[@class='product-number']/span")))
									&& Utils.waitForElement(
											driver,
											lstProductPriceInProductSet
											.get(i)
											.findElement(
													By.xpath("preceding-sibling::div[@class='product-number']/span"))))

			{
				status = true;
			}
		}
		return status;
	}

	/**
	 * To set the random size for all products in product set
	 * 
	 * @return String Array - List of selected and set size in product sets
	 * @throws Exception
	 */
	public String[] selectSizeRandomInProductSet() throws Exception {
		String[] selectedSize = new String[lstProductSet.size()];
		for (int i = 0; i < lstProductSet.size(); i++) {
			List<WebElement> sizeDefaultValue = BrowserActions.checkLocators(
					driver, DEFAULT_SELECTED_VALUE_DROPDOWN);

			BrowserActions.scrollToViewElement(sizeDefaultValue.get(i), driver);
			if (!BrowserActions
					.getText(driver, sizeDefaultValue.get(i),
							"Size drop down selected option").trim()
							.toLowerCase().contains("please select")) {
				selectedSize[i] = BrowserActions.getText(driver,
						sizeDefaultValue.get(i),
						"Size drop down selected option");
			}
			// clicking size drop down
			// BrowserActions.scrollToViewElement(sizeDefaultValue.get(i),
			// driver);
			BrowserActions.clickOnElement(sizeDefaultValue.get(i), driver,
					"Clicked Size Button");
			BrowserActions.nap(1);

			// if (sizeDefaultValue.size() > 0) {
			// BrowserActions.scrollToViewElement(sizeDefaultValue.get(i),
			// driver);
			// BrowserActions.javascriptClick(sizeDefaultValue.get(i), driver,
			// "Clicked Size Button");
			List<WebElement> lstSize = lstProductSet
					.get(i)
					.findElements(
							By.cssSelector("li.size .selection-list >li:not([class*='hide'])"));

			if (lstSize.size() > 0) {
				int rand = Utils.getRandom(0, lstSize.size());
				BrowserActions.javascriptClick(lstSize.get(rand), driver,
						"Select size option");
				waitForSpinner();
				BrowserActions.nap(2);
				sizeDefaultValue = BrowserActions.checkLocators(driver,
						DEFAULT_SELECTED_VALUE_DROPDOWN);
				selectedSize[i] = sizeDefaultValue.get(i).getText();
			}
			// }
		}

		return selectedSize;
	}

	public String[] selectQuantityRandomInProductSet(String... qty)
			throws Exception {
		// checking size drop down have one value or more that one
		String[] selectedQty = new String[lstProductSet.size()];
		List<WebElement> qtyDefaultValue = BrowserActions.checkLocators(driver,
				SELECTED_QTY);

		for (int i = 0; i < lstProductSet.size(); i++) {
			BrowserActions.scrollToViewElement(qtyDefaultValue.get(i), driver);
			if (qty.length > 0) {
				BrowserActions.clickOnElement(qtyDefaultValue.get(i), driver,
						"Clicked Size Button");
				BrowserActions.nap(1);
				try {
					List<WebElement> listOfqty = lstProductSet
							.get(i)
							.findElements(
									By.cssSelector("div[class='quantity'] ul>li"));
					if (qty[0].equalsIgnoreCase("random")) {
						int rand = Utils.getRandom(0, listOfqty.size());
						BrowserActions.clickOnElement(listOfqty.get(rand),
								driver, "Select size option");

					} else if (listOfqty.size() > 0) {
						String optionValue = qty[0].toString();
						WebElement qtyoption = BrowserActions
								.getMachingTextElementFromList(listOfqty,
										optionValue, "equals");
						BrowserActions.clickOnElement(qtyoption, driver,
								"Size drop down options");

					}
				} catch (Exception e) {
					Log.event("Quantity is selected as default");
				}
			}
			selectedQty[i] = qtyDefaultValue.get(i).getText().trim();
		}
		waitForSpinner();
		return selectedQty;
		// return BrowserActions.checkLocator(driver,
		// DEFAULT_SELECTED_VALUE_DROPDOWN).getText();
	}

	public String[] selectColorRandomInProductSet() throws Exception {
		String[] selectedColor = new String[lstProductSet.size()];

		for (int i = 0; i < lstProductSet.size(); i++) {
			// WebElement colorSelectedByDefault =
			// lstProductSet.get(i).findElement(By.cssSelector(SELECTED_COLOR));

			List<WebElement> colorSelectable = lstProductSet.get(i)
					.findElements(By.cssSelector(LIST_OF_COLOR_SELECTABLE));
			if ((colorSelectable.size() == 0)) {
				WebElement defaultColor = lstProductSet.get(i).findElement(
						By.cssSelector(SELECTED_COLOR));
				BrowserActions.scrollToViewElement(defaultColor, driver);
				selectedColor[i] = BrowserActions.getTextFromAttribute(driver,
						defaultColor, "alt", "Alt attribute value");
			} else if (colorSelectable.size() > 0) {
				int rand = ThreadLocalRandom.current().nextInt(0,
						colorSelectable.size());
				BrowserActions.scrollToViewElement(colorSelectable.get(rand),
						driver);
				BrowserActions.clickOnElement(colorSelectable.get(rand),
						driver, "select color(" + i + ")");
				waitForSpinner();
				WebElement selectedColors = BrowserActions.checkLocator(driver,
						SELECTED_COLOR);
				selectedColor[i] = BrowserActions.getTextFromAttribute(driver,
						selectedColors, "alt", "Alt attribute value");
			} else {
				throw new Exception(
						"Color swatched not available (or) not selectable in the UI");
			}
			Utils.waitForPageLoad(driver);
		}

		return selectedColor;

	}

	/**
	 * To click on 'Add to Wishlist' link by product set index
	 * 
	 * @param index
	 *            - n'th Product in product set
	 * @throws Exception
	 */
	public void clickAddToWishListLinkByProduct(int index) throws Exception {
		BrowserActions.scrollToViewElement(
				lstAddToWishListInProductSet.get(index - 1), driver);
		BrowserActions.clickOnElement(
				lstAddToWishListInProductSet.get(index - 1), driver,
				"Add to wishList link");
		BrowserActions.nap(1);
	}

	/**
	 * To verify whether any size is selected by default for all products in
	 * Product Set
	 * 
	 * @return 'true' - no size is selected default - Default size string
	 *         'Select...' 'false' - size is selected by default
	 * @throws Exception
	 */
	public boolean verifyDefaultSizeStatusInProductSet() throws Exception {

		for (int i = 0; i < lstProductSet.size(); i++) {
			if (!(sizeDefaultValue.get(i).getText().contains("Select")))
				return false;
		}
		return true;
	}

	/**
	 * to click on 'Buy All For $xx.xx'
	 * 
	 * @throws Exception
	 */
	public void clickBuyForAll() throws Exception {
		BrowserActions.clickOnElement(lblBuyAll, driver,
				"Lable Buy For All $xx.xx");
	}

	/**
	 * to verify sum of individual products in product set equals to value in
	 * 'Buy All For $xx.xx'
	 * 
	 * @return 'true' - if both values equal 'false' - if both values not equal
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	// Commit cod-

	public boolean verifySumByAllChildProduct() throws Exception {
		return isPageLoaded;

	}

	public boolean verifyTotalSumInBuyForAll() throws Exception {
		float total = 0;
		String price;
		float byAllPrice = 0;
		for (int i = 0; i < priceSection.size(); i++) {
			if (priceSection.get(i).getAttribute("class")
					.contains("price-sales")) {
				price = priceSection.get(i).getText().trim()
						.replace("Now $", "");
				// if(price.contains("-")){
				// price=price.trim().split("-")[01].replace("$", "").trim();
				// price2=price.trim().split("-")[1].replace("$", "").trim();
				// }

				total += Float.parseFloat(price);
				// total1 += Float.parseFloat(price2);

			} else if (priceSection.get(i).getAttribute("class")
					.contains("standardprice")) {
				price = priceSection.get(i).getText().trim().replace("$", "");
				total += Float.parseFloat(price);
				// total1 += Float.parseFloat(price);
			} else {
				if (!priceSection.get(i).getAttribute("class")
						.contains("price-standard")) {
					price = priceSection.get(i).getText().trim()
							.replace("Now $", "");
					// if(price.contains("-")){
					// price=price.trim().split("-")[0].replace("$", "").trim();
					// price2=price.trim().split("-")[1].replace("$",
					// "").trim();
					// }

					total += Float.parseFloat(price);
					// total1 += Float.parseFloat(price2);
				}
			}
		}

		String byAllPrices = lblBuyAll
				.findElement(By.cssSelector(".salesprice")).getText().trim()
				.replace("$", "");
		// if(byAllPrices.contains("-")){
		// byAllPrice =
		// Float.parseFloat(byAllPrices.trim().split("-")[0].replace("$", ""));
		// byAllPrice2 =
		// Float.parseFloat(byAllPrices.trim().split("-")[1].replace("$", ""));
		// }
		byAllPrice = Float.parseFloat(byAllPrices.trim().replace("$", ""));
		if (total == byAllPrice) // && (total1 ==byAllPrice2))
			return true;
		else
			return false;
	}

	public boolean selectSizeDropDownsInProductSet() throws Exception {
		boolean status = false;

		if (addAllToShoppingBagForProductSet.getAttribute("class").contains(
				"disabled")) {
			for (int i = 0; i < drpSizeInProductSet.size(); i++) {

				Select dropdown = new Select(drpSizeInProductSet.get(i));
				dropdown.selectByIndex(1);
				status = true;

			}
		}
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
		return status;
	}

	public ArrayList<String> getUPCofProductSetPage() throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();

		for (WebElement element : getUPC) {
			BrowserActions.scrollToViewElement(element, driver);
			lstValues.add(element.getText());
		}
		return lstValues;
	}

	public boolean verifyAddAlltoBagIsEnabled() throws Exception {
		boolean status = true;
		BrowserActions.scrollToView(addAllToShoppingBagForProductSet, driver);
		if ((addAllToShoppingBagForProductSet.getAttribute("class")
				.contains("disabled"))) {
			status = false;
		}
		// Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
		// BrowserActions.clickOnElement(addAllToShoppingBagForProductSet,
		// driver, "Add All to Shopping Bag");
		// Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
		// BrowserActions.clickOnElement(addToShoppingBagForProductSet, driver,
		// "Add to Shopping Bag");
		// Utils.waitUntilElementDisappear(driver, addedToMyBagPopUp);
		return status;
	}

	/*	*//**
	 * To clik on to mini cart button
	 * 
	 * @throws Exception
	 */
	/*
	 * public void ClikOnBagForProductSet() throws Exception { String runPltfrm
	 * = Utils.getRunPlatForm(); if (runPltfrm == "mobile") {
	 * BrowserActions.clickOnElement(BagForProductSet, driver, "Bag link");
	 * BrowserActions.clickOnElement(ClikOnViewBag, driver, "Bag link"); } else
	 * { BrowserActions.clickOnElement(BagForProductSet, driver, "Bag link"); }
	 * }
	 */

	public ArrayList<String> getUPCofProductSetPageInBag() throws Exception {

		ArrayList<String> lstValues = new ArrayList<>();

		for (WebElement element : UPCinBagPage) {
			lstValues.add(element.getText());
		}
		return lstValues;
	}

	public boolean verifyAddtoBagIsEnabled() throws Exception {
		boolean status = true;
		if ((btnAddtoBag.getAttribute("class").contains("disabled"))) {
			status = false;
		}
		Utils.waitUntilElementDisappear(driver, searchResultsSpinner);
		// BrowserActions.clickOnElement(addAllToShoppingBag, driver, "Add All
		// to Shopping Bag");
		return status;
	}

	/**
	 * to verify success message color,tick mark and presence when click on 'Add
	 * to Wishlist' link
	 * 
	 * @param pdpPage
	 *            - PDP Page object to verify elements
	 * @return 'true' - if success message have green color, tick mark and
	 *         presence
	 * @throws Exception
	 */
	public boolean verifyWishListMessageByProduct(int index, PdpPage pdpPage)
			throws Exception {
		if (elementLayer.verifyPageListElements(Arrays.asList("lstWishListMessage"), pdpPage)) {
			String color = SearchUtils.getLabelColor(lstWishListMessage
					.get(index - 1));
			if (!(color.equals("rgba(78, 121, 56, 1)")))
				return false;
		}
		return true;
	}

	/**
	 *
	 * To Check whether the size is in Ascending Order
	 *
	 * return status
	 *
	 * @throws Exception
	 */

	public boolean checkSizeOrder() throws Exception {
		boolean status = true;
		BrowserActions.clickOnElement(drpSize, driver, "Size dropdown");
		ArrayList<String> size = new ArrayList<String>();

		for (int i = 0; i < lstSize.size(); i++) {
			size.add(lstSize.get(0).toString());
		}
		Collections.sort(size);
		for (int i = 0; i < lstSize.size(); i++) {
			if (!(size.get(i).equals(lstSize.get(i).toString()))) {
				status = false;
				break;
			}
		}
		Utils.waitUntilElementDisappear(driver, spinner);
		return status;
	}

	/**
	 * To get add to bag success message
	 * 
	 * @return String
	 * @throws Exception
	 *             Last Modified By - Gowri & Last Modified Date - 1/4/2017
	 */
	public String getAddToBagMessage() throws Exception {
		Utils.waitForElement(driver, SuccessMessage);
		return BrowserActions
				.getText(driver, SuccessMessage, "Success Message");

	}

	public void clickOnFindStore() throws Exception {
		BrowserActions.scrollToViewElement(lnkFindInStore, driver);
		BrowserActions.clickOnElement(lnkFindInStore, driver, "Find Store");

	}

	/**
	 * To verify product name is displayed under brand name
	 */
	public boolean verifyProductNameDisplayedUnderBrandName() throws Exception {
		boolean status = false;
		if (Utils
				.waitForElement(
						driver,
						driver.findElement(By
								.xpath(".//span[@class='product-name']/preceding-sibling::span[@class='product-brand']")))) {
			status = true;
		}
		return status;
	}

	/**
	 * To close the size chart by closs button
	 * 
	 * @throws Exception
	 */
	public void closeDialogueBoxByCloseButton() throws Exception {
		BrowserActions.clickOnElement(iconCloseSizeChart, driver,
				"close the size chart link");
	}

	/**
	 * To close the size chart by other element
	 * 
	 * @throws Exception
	 */
	public void closeDialogueBoxByClickingOtherElement() throws Exception {
		Actions action = new Actions(driver);
		action.moveToElement(lnkCustomerService, 0, 0).click().build()
		.perform();
		;
	}

	/**
	 * To close the size chart by escape key
	 * 
	 * @throws Exception
	 */
	public void closeDialogueBoxByEsckey() throws Exception {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).build().perform();
	}

	/**
	 * To get upc value of the displayed product
	 * 
	 * @return selectedColorName
	 * @throws Exception
	 */
	public String getUPCValue() throws Exception {
		if (Utils.waitForElement(driver, lblUPC))
			return lblUPC.getText().split("\\:")[1];
		else
			return "UPC: 000000000";
	}

	public String getOnlineOnlyMessage() throws Exception {
		return onlineOnlyMessage.getText();
	}

	public boolean verifyOnlineOnlyMessageBelowQTY() throws Exception {
		boolean status = false;
		if (Utils.waitForElement(driver, lblQuantity.findElement(By
				.xpath("following-sibling::div[@class='message-qv']")))) {
			status = true;
		}
		return status;
	}

	/**
	 * To get color src value of swatches by index (PDP)
	 * 
	 * @param index
	 * @return String
	 */
	public String getSelectedSwatchColors(int index, String... allColorSwatch)
			throws Exception {
		List<WebElement> lstElements = null;
		if (allColorSwatch.length > 0) {
			lstElements = listColor;
		} else {
			lstElements = lstSelectColor;
		}
		return lstElements.get(index - 1).findElement(By.cssSelector("img"))
				.getAttribute("src").split("&crop=")[0];

	}

	public RegistryGuestUserPage clickOnRegistrySignInProductSet(int index)
			throws Exception {
		// Utils.waitForElement(driver, btnRegistrySigneduserInProductSet);
		BrowserActions.clickOnElement(
				btnRegistrySigneduserInProductSet.get(index - 1), driver,
				"click on registry button as a signed user");
		Utils.waitForPageLoad(driver);
		return new RegistryGuestUserPage(driver).get();
	}

	// ----------Product set---------//

	/**
	 * To click on random 'Add To Registory' button in product set page
	 * 
	 * @throws Exception
	 */
	public void clickOnRegistrySignInProductSet() throws Exception {
		Utils.waitForElement(driver, btnRegistrySigneduser);
		int rand = Utils.getRandom(0, btnRegistrySigneduserInProductSet.size());
		BrowserActions.clickOnElement(
				btnRegistrySigneduserInProductSet.get(rand), driver,
				"click on registry button as a signed user");

	}

	/**
	 * To mouse hover to mini cart
	 * 
	 * @return
	 * @throws Exception
	 */
	public ShoppingBagPage clickOnMiniCart() throws Exception {
		if (Utils.waitForElement(driver, minicartContent))
			if (minicartContent.getCssValue("display").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public boolean verifyMiniCartOverlayDisappears() throws Exception {
		boolean status = false;
		BrowserActions.mouseHover(driver, lnkCoupons);

		if (!minicartOverLay.isDisplayed()) {
			status = false;
		} else {
			status = true;
		}
		return status;
	}

	public ShoppingBagPage ClikOnBagForProductSet() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(BagForProductSet, driver, "Bag link");
			BrowserActions.clickOnElement(ClikOnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(BagForProductSet, driver, "Bag link");
		}

		return new ShoppingBagPage(driver).get();
	}

	public String getsuccesstextmessage() {
		String successMessage = txtSuccessMessage.getText();
		return successMessage;

	}

	public String outOfStockTxt() throws Exception {
		String outOfStock = BrowserActions.getText(driver,
				txtmessageoutofstock, "Out of stock message");
		Log.event("The Stock message is: " + outOfStock);
		return outOfStock;
	}

	/**
	 * To close the size chart by other element
	 * 
	 * @throws Exception
	 */
	public void closeDialogueBoxByClickingOtherElementImage() throws Exception {
		Actions action = new Actions(driver);

		action.moveToElement(primaryImage, 0, 0).click().build().perform();

	}

	/**
	 * To click the thumbnail image by index
	 * 
	 * @param index
	 * @throws Exception
	 */
	public void clickThumbnailImageByIndex(int index) throws Exception {
		BrowserActions.scrollToViewElement(thumbsInContainer.get(index - 1),
				driver);
		BrowserActions.clickOnElement(thumbsInContainer.get(index - 1), driver,
				"Clicked random thumbnail");

	}

	/**
	 * To get the thumbnail image url
	 * 
	 * @param index
	 * @return String - Image url
	 * @throws Exception
	 */
	public String getThumbnailImageUrl(int index) throws Exception {
		BrowserActions.scrollToViewElement(thumbsInContainer.get(index - 1),
				driver);
		BrowserActions.clickOnElement(thumbsInContainer.get(index - 1), driver,
				"Clicked random thumbnail");
		return (BrowserActions.getTextFromAttribute(driver, thumbsInContainer
				.get(index - 1).findElement(By.tagName("a")), "href",
				"Thumbnail image href"));

	}

	/**
	 * Click on anywhere to close the Find store box
	 * 
	 * @throws Exception
	 */

	public void closeFindStoreClickingAnywhere() throws Exception {
		BrowserActions.clickOnElement(clickAnyWhereFindStore, driver,
				"Clicking belk logo");
	}

	/**
	 * To scroll down to see the mobile description
	 * 
	 * @throws InterruptedException
	 */
	public void scrollDownMobile() throws InterruptedException {

		BrowserActions.scrollToViewElement(txtDescriptionPdpmobile, driver);
	}

	public void clickFindStore() throws Exception {
		BrowserActions.mouseHover(driver, btnFindStore);
		BrowserActions.clickOnElement(btnFindStore, driver,
				"Click on the find Store button");

	}

	/**
	 * To click on the Find Store Box by clicking on the close [X] icon
	 * 
	 * @return
	 * @throws Exception
	 */
	public PdpPage closeFindStoreBoxByCrossIcon() throws Exception {
		BrowserActions.clickOnElement(btnCloseFindStore, driver,
				"Find Store Close Button");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver);
	}

	/**
	 * To close the Find Store Box by Pressing on the ESC key
	 * 
	 * @return
	 * @throws Exception
	 */
	public PdpPage closeFindStoreByEsc() throws Exception {
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ESCAPE);
		r.keyRelease(KeyEvent.VK_ESCAPE);
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver);
	}

	public String getTextFromBreadCrumb1() throws Exception {
		return BrowserActions.getText(driver, lblBreadCrump,
				"Text from breadcrumb");

	}

	// Commit code

	/**
	 * To get the Text of the Free Shipping from PDP
	 * 
	 * @throws Exception
	 */
	public String getTextFreeShipping() throws Exception {
		String getFreeText = BrowserActions.getText(driver, txtFreeShipping,
				"Free Shipping text message on the PDP page");
		return getFreeText;

	}

	public void clickOnMiniCartIcon() throws Exception {
		BrowserActions.nap(3);
		BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnLogo() throws Exception {
		BrowserActions.javascriptClick(belkLogo, driver, "Belk Logo");
	}

	/**
	 * 
	 * To click on Add to bag of child products by index
	 * 
	 * @param index
	 * 
	 * @throws Exception
	 */

	public void clickAddToBagByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(btnAddToShoppingBag.get(index - 1),
				driver, "Add to Bag button");
		Utils.waitUntilElementDisappear(driver, spinner);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the all the child product names
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getAllChildProductNames() throws Exception {

		ArrayList<String> allChildproductNames = new ArrayList<String>();
		for (WebElement element : txtChildProductNames) {
			allChildproductNames.add(BrowserActions.getText(driver, element,
					"Child Product set product name"));
		}
		return allChildproductNames;
	}

	/**
	 * To get all the child product prices
	 * 
	 * @return String
	 */
	public ArrayList<String> getAllChildCurrentPrice() {
		ArrayList<String> allChildprodPrice = new ArrayList<String>();
		if (Utils.waitForElement(driver, lblProductNowPrice)) {
			for (WebElement element : lblChildProductCurrentPrices) {
				String productPrice = element.getText().toString();
				if (productPrice.contains("Now $")) {
					allChildprodPrice.add(productPrice.trim().replace("Now $",
							""));
				} else if (productPrice.contains("Now Now $")) {
					allChildprodPrice.add(productPrice.trim().replace(
							"Now Now $", ""));
				}
			}
		} else if (Utils.waitForElement(driver, lblProductStandardPrice)) {
			for (WebElement element : lblChildProductStandardPrices) {
				allChildprodPrice.add(element.getText().toString().trim()
						.replace("$", ""));
			}
		}
		return allChildprodPrice;
	}

	/**
	 * To get the all the child product selected colors
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getAllChildProductColors() throws Exception {
		ArrayList<String> allChildproductColors = new ArrayList<String>();
		for (WebElement element : lblselectedColors) {
			allChildproductColors.add(BrowserActions.getText(driver, element,
					"Child Product set product colors"));
		}
		return allChildproductColors;
	}

	/**
	 * To get the all the child product selected size values
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getAllChildSelectedProductSizesValues()
			throws Exception {
		ArrayList<String> allChildproductSizes = new ArrayList<String>();
		for (WebElement element : sizeDefaultValue) {
			allChildproductSizes.add(BrowserActions.getText(driver, element,
					"Child Product set product sizes"));
		}
		return allChildproductSizes;
	}

	/**
	 * To get the all the child product selected size values
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getAllChildSelectedProductQtyValues()
			throws Exception {
		ArrayList<String> allChildproductQty = new ArrayList<String>();
		for (WebElement element : qtyDefaultValue) {
			allChildproductQty.add(BrowserActions.getText(driver, element,
					"Child Product set product sizes"));
		}
		return allChildproductQty;
	}

	/**
	 * To get the all the child product upcs
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getAllChildProductUPCs() throws Exception {
		ArrayList<String> allChildproductUPCs = new ArrayList<String>();
		for (WebElement element : lblUPCs) {
			allChildproductUPCs.add(BrowserActions.getText(driver, element,
					"Child Product set upc"));
		}
		return allChildproductUPCs;
	}

	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInProductSet()
			throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();

		if (Utils.waitForElement(driver, productSet)) {
			List<WebElement> productName = productSet.findElements(By
					.cssSelector(".item-name"));
			for (int i = 0; i < btnAddToShoppingBag.size(); i++) {
				LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();

				String productName1 = productName.get(i).getText().trim()
						.contains(productBrandInProductSet.getText().trim()) ? productName
								.get(i).getText().trim()
								.replace(productBrandInProductSet.getText().trim(), "")
								.trim()
								: productName.get(i).getText().trim();
								String color = lstProductSet
										.get(i)
										.findElement(
												By.cssSelector("ul[class='swatches color']>li.selected>a>img"))
												.getAttribute("alt").trim();
								String size = lstProductSet
										.get(i)
										.findElement(
												By.cssSelector("div[class='value'] div[class*='selected-option']"))
												.getText().trim();
								String quantity = lstProductSet
										.get(i)
										.findElement(
												By.cssSelector("li.set-quantity .selected-option"))
												.getText().trim();

								String price = new String();
								productName1 = productName.get(i).getText()
										.replace(productBrandInProductSet.getText(), "").trim();

								if (lstProductSet.get(i).getText().contains("Clearance")) {
									price = lstProductSet.get(i)
											.findElement(By.cssSelector(".price-sales"))
											.getText().trim().split("\\$")[1].trim();
								} else if (lstProductSet.get(i).getText().contains("Now")) {
									price = lstProductSet.get(i)
											.findElement(By.cssSelector(".price-sales"))
											.getText().trim().split("\\$")[1].trim();
								} else if (Utils.waitForElement(driver, lstProductSet.get(i)
										.findElement(By.cssSelector(".standardprice")))) {
									price = lstProductSet
											.get(i)
											.findElement(
													By.cssSelector(".standardprice span[itemprop='price']"))
													.getText().trim();
								}

								product.put("ProductName", productName1);
								product.put("Upc", getAllChildProductUPCs().get(i).toString()
										.trim().split("\\:")[1].trim());
								product.put("Color", color);
								product.put("Size", size);
								product.put("Price", price);
								product.put("Quantity", quantity);
								productDetails.add(product);

			}
		}

		return productDetails;
	}

	/**
	 * To select the variation and get the details of product
	 * 
	 * @param qtyToSet
	 * @return
	 * @throws Exception
	 * 
	 *             Last Modified By : Dhanapal.K ---- 04/01/2017
	 */
	public LinkedHashMap<String, String> setGetProductDetails(
			String... qtyToSet) throws Exception {
		LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();

		if (Utils.waitForElement(driver, selectedColorInPDP)) {
			String productName1 = getProductName();
			String color = selectColor();
			String size = selectSize();
			String quantity = selectQuantity(qtyToSet);
			String price = getProductPrice();
			String upc = getUPCValue();

			product.put("ProductName", productName1);
			product.put("Upc", upc.trim());
			product.put("Color", color);
			product.put("Size", size);
			product.put("Price",
					price.contains("Now") ? price.trim().split("\\$")[1]
							: price.trim().replace("$", ""));
			product.put("Quantity", quantity);
		} else {
			String productName1 = getProductName();
			String quantity = selectQuantity(qtyToSet);
			String upc = getUPCValue();
			String price = getProductPrice();

			product.put("ProductName", productName1);
			product.put("Upc", upc.trim());
			product.put("Price",
					price.contains("Now") ? price.trim().split("\\$")[1]
							: price.trim().replace("$", ""));
			product.put("Quantity", quantity);

		}

		return product;
	}

	public void clikAddToBagInGiftCardPage() throws Exception {
		Utils.waitForElement(driver, btnAddToBagInGiftCardPage);
		BrowserActions.scrollToViewElement(btnAddToBagInGiftCardPage, driver);
		BrowserActions.javascriptClick(btnAddToBagInGiftCardPage, driver,
				"AddToBag In GiftCardPage");
	}

	public String getSelectedGiftProduct() throws Exception {
		return BrowserActions.getText(driver, giftProductName,
				"Gift Product Name");
	}

	public void clickSelectGiftProduct() throws Exception {
		Utils.waitForElement(driver, btnGiftSelect);
		BrowserActions.javascriptClick(btnGiftSelect, driver,
				"Select Gift Product");
		BrowserActions.nap(3);
	}

	/**
	 * getPromotionMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getPromotionMessage() throws Exception {
		return BrowserActions.getText(driver, lblPromotionmessage,
				"Promition message").trim();
	}

	/**
	 * selectBonusProduct
	 * 
	 * @return productName
	 * @throws Exception
	 */
	public String selectBonusProduct() throws Exception {
		String productName;
		if (bonusProductlist.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(0,
					bonusProductlist.size());
			productName = BrowserActions.getText(driver,
					bonusProductName.get(rand), "Product Name");
			BrowserActions.clickOnElement(bonusProductlist.get(rand), driver,
					"click select button");
			waitForSpinner();
		} else {
			throw new Exception("Bonus offer product is not shown in the UI");
		}
		return productName;
	}

	/**
	 * closeBonusOffer
	 * 
	 * @throws Exception
	 */
	public void closeBonusOffer() throws Exception {
		BrowserActions.clickOnElement(btnClosedBonusOffer, driver,
				"Click on bonus offer '(X)' button");
	}

	public String StockTxt() throws Exception {
		String stockmessage = BrowserActions.getText(driver, txtmessagestock,
				" stock message");
		Log.event("The Stock message is: " + stockmessage);
		return stockmessage;
	}

	public boolean verifyAddToRegistryLinkEnabled() {
		return Utils.waitForElement(driver, btnRegistryAsGuest);
	}

	public void clickOnAddItemToRegistry() throws Exception {
		BrowserActions.clickOnElement(addItemToRegistry, driver,
				"Add item To Registry");
	}

	public void clickOnRegistryLink() throws Exception {
		BrowserActions.clickOnElement(btnRegistryPdpPage, driver,
				"Registry Link");
	}

	/**
	 * Click on the Apply button on the registry modal and get the success
	 * message.
	 */
	public String clickOnApplyButtonOnRegistryModal() throws Exception {
		BrowserActions
		.clickOnElement(btnApplyButton, driver,
				"Click on the Apply button on the Registry Modal for multiple Registry");
		String applySuccessMessageOnRegistryModal = BrowserActions.getText(
				driver, txtSuccessApplyMessage,
				"Click on the Apply button the Product added to the registry");
		Utils.waitForPageLoad(driver);
		return applySuccessMessageOnRegistryModal;
	}

	/**
	 * Click on the cancel button
	 */
	public void clickCancelButton() throws Exception {
		BrowserActions.clickOnElement(btnCancelOnRegistryModal, driver,
				"Click on the cancel button on the registry modal");
		;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the title of the select registry from select modal
	 * 
	 * @return
	 * @throws Exception
	 */
	public String registrySelectOnSelectModal() throws Exception {
		String registrySelect = BrowserActions.getText(driver,
				txtSelectRegistry, "select Registry text on the pop_up ");
		return registrySelect;

	}

	/**
	 * To get the subtitle heading from the registry modal
	 * 
	 * @return
	 * @throws Exception
	 */
	public String registrySubtitleSelectOnSelectModal() throws Exception {
		String registrySubtitleSelect = BrowserActions.getText(driver,
				txtSubtitleSelectRegistry, "Subtitle of the Select Registry");
		return registrySubtitleSelect;

	}

	public String originalPriceTxt() throws Exception {
		String originalsaleprice = BrowserActions.getText(driver,
				txtOriginalPrice, "originalAndSalePriceTxt");
		return originalsaleprice;
	}

	public String salePriceTxt() throws Exception {
		String originalsaleprice = BrowserActions.getText(driver, txtSalePrice,
				"originalAndSalePriceTxt");
		return originalsaleprice;
	}

	public PdpPage clickOnNoThanks() throws Exception {
		BrowserActions
		.clickOnElement(lnkNotinterestedonBonus, driver,
				"No Thanks,Not Interested. button on Select Free Gift modal window");
		return new PdpPage(driver).get();
	}

	public boolean isselectFreeGiftModal() throws Exception {
		boolean status = false;
		if (Utils.waitForElement(driver, selectFreeGiftModal)) {
			status = true;
		}
		return status;
	}

	/**
	 * To fetch the success message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String registryLoginAddSuccessMessage() throws Exception {
		Utils.waitForElement(driver, txtSuccessMessage, 5);
		String successTxtMessageofProduct = BrowserActions.getText(driver,
				txtSuccessMessage, "Success Message when the product added ");
		return successTxtMessageofProduct;

	}

	/**
	 * To wait for mini cart overlay disappears
	 */
	public void waitForMiniCartOverlayDisappears() {
		Utils.waitUntilElementDisappear(driver, cartOverlay);
	}

	/**
	 * To check the Pop Up for the Free gift product is displayed and select the
	 * item from the pop_up box
	 * 
	 * @throws Exception
	 */

	public void selectFreeGiftItem() throws Exception {
		BrowserActions.clickOnElement(btnSelectFreeGift, driver,
				"Select the free gift product");
		Utils.waitForPageLoad(driver);

	}

	public String getProductPriceFromPdp() throws Exception {
		String price = BrowserActions.getText(driver, productPriceInPdp,
				"Product Price in Pdp Page ");
		return price;
	}

	public String getSelectedSwatchColorSrcCode() throws Exception {
		String str = BrowserActions.getTextFromAttribute(driver,
				ImgSrcOfSelectedSwach, "src", "Src value").split("&crop")[0];
		waitForSpinner();
		return str;
	}

	/**
	 * to get text of the 'Promotion msg' in 'PDP' page
	 */
	public String getTextforPromotionMessage() throws Exception {
		String msg = BrowserActions.getText(driver, msgPromotion,
				"Getting text from 'Promotion' message in 'PDP' page");
		return msg;
	}

	public String getText() throws Exception {
		String BrandName = BrowserActions.getText(driver, getBrandNameInPdp,
				"Brand Name In PDP page");
		return BrandName;
	}

	/**
	 * To get number of child products
	 * 
	 * @return int number of child products
	 * 
	 */
	public int getNumberofchildProducts() {
		return childProductSets.size();
	}

	/**
	 * To Select all the drop
	 * 
	 * @return
	 * @throws Exception
	 */
	public void selectValueDropDownInProductSet() throws Exception {

		for (int i = 0; i < childProductSets.size(); i++) {
			Select dropdown = new Select(lstGiftcardvalue.get(i));
			dropdown.selectByIndex(1);

		}
	}

	/**
	 * To get the thumbnail image url
	 * 
	 * @param index
	 * @return String - Image url
	 * @throws Exception
	 */
	public String getImageUrl() throws Exception {
		return (BrowserActions.getTextFromAttribute(driver,
				imgInPdp.findElement(By.tagName("a")), "href", "image href"));

	}

	/**
	 * To verify the badge text & color options in the CLP page
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean verifyBadgeRefinementText() throws Exception {
		boolean status = false;
		String cssAttr = "color";
		String cssValue = "rgba(255, 255, 255, 1)";
		status = Utils.verifyCssPropertyForElement(txtProductBadge, cssAttr,
				cssValue);
		return status;

	}

	public void clickOnTocloseInPopUp() throws Exception {
		BrowserActions.clickOnElement(closeInPopUp, driver,
				"Close link in pop up");
	}

	public void clickOnNoThankMnotInterested() throws Exception {
		BrowserActions.clickOnElement(noThankMnotInterested, driver,
				"Close link in pop up");
	}

	public boolean verifyspacesconvertedtohyphen(
			String Productnamefrombreadcrumb, String ProductnamefromUrl)
					throws Exception {
		String Productnamefrombreadcrumb1 = Productnamefrombreadcrumb.replace(
				"& ", "").replaceAll(" ", "-");
		if (Productnamefrombreadcrumb1.equalsIgnoreCase(ProductnamefromUrl))
			return true;
		else
			return false;

	}

	public boolean isURLInLowercase(String str) throws Exception {
		int count = 0;
		for (char c : str.toCharArray()) {
			if (Character.isUpperCase(c)) {
				count++;
			}
		}
		if (count == 0)
			return true;
		else
			return false;
	}

	/**
	 * To click GWP link
	 * 
	 * @throws Exception
	 */
	public void clickOnGwpModalLink() throws Exception {
		BrowserActions.clickOnElement(lnkGwpModal, driver, "GWP Modal Link");
	}

	/**
	 * selectStore
	 * 
	 * @param zipcode
	 * @throws Exception
	 */
	public void selectStore(String zipcode) throws Exception {
		BrowserActions.clickOnElement(btnChangePreferredStore, driver,
				"change-preferred-store");
		if (Utils.waitForElement(driver, lnkChangeLocation)) {
			BrowserActions.clickOnElement(lnkChangeLocation, driver,
					"change location");
		}
		BrowserActions.typeOnTextField(txtZipcode, zipcode, driver, "zip code");
		BrowserActions.clickOnElement(btnSearchStore, driver, "button search");
		BrowserActions.clickOnElement(btnSelectStore, driver,
				"click on select a store");
	}

	/**
	 * selectFreeInStore
	 * 
	 * @param checkRadio
	 */
	public void selectFreeInStore(String checkRadio) {
		BrowserActions.selectRadioOrCheckbox(rdoStoreShip, checkRadio);
	}

	public String getPromotionalCalloutMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		String getPromoCallOutMessage= BrowserActions.getText(driver, calloutMessage, "callout message").trim();
		return getPromoCallOutMessage;
	}



	/**
	 * To get the rebate message
	 * 
	 * @throws Exception
	 * 
	 */
	public String txtRebateMessageOnPDP() throws Exception {
		String getTxtOfRebate = BrowserActions.getText(driver,
				txtRebateMessage, "Rebate text displayed on the PDP page");
		return getTxtOfRebate;

	}

	/**
	 * getPromotionMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getPromotionMessageInPDP() throws Exception {
		return BrowserActions.getText(driver, msgGWP, "Promotion message")
				.trim();
	}

	public String getTextPromoMessage() throws Exception {
		return BrowserActions.getText(driver, promoMessage,
				"Fetching the Promotion Message On PDP Page");
	}

	public void clickOnOfferAndRebateLink() throws Exception {
		if (runPltfrm == "mobile") {
			BrowserActions
			.scrollToViewElement(offerAndRebateLinkMobile, driver);
		} else {
			BrowserActions.scrollToViewElement(offerAndRebateLink, driver);
			BrowserActions.clickOnElement(offerAndRebateLink, driver,
					"Offer And Rebate Link");
		}

	}

	public String getTextPromoMessageInOfferAndRebate() throws Exception {
		BrowserActions.scrollToView(offerAndRebatePromoMessage, driver);
		return BrowserActions.getText(driver, offerAndRebatePromoMessage,
				"Fetching the Promotion Message On PDP Page");
	}

	public void clickOnViewLink() throws Exception {
		BrowserActions.scrollToViewElement(ViewDetailsFirst, driver);
		BrowserActions.clickOnElement(ViewDetailsFirst, driver, "View Link");
	}

	public String getCostForFreeShipping() throws Exception {
		return BrowserActions.getText(driver, getCost, "Promo Message");
	}

	public boolean verifyMinicartisDisplayed() throws Exception {
		boolean status = false;
		String stylevalue = BrowserActions.getTextFromAttribute(driver,
				minicartcontent, "style", "Minicart style");
		Log.message(stylevalue);
		if (stylevalue.contains("display: block;")) {
			status = true;
		}
		return status;
	}

	public String getProductRateInPDPPage() throws Exception {
		Utils.waitForElement(driver, productCostInPdpPage);
		BrowserActions.scrollToViewElement(productCostInPdpPage, driver);
		String rate = BrowserActions.getText(driver, productCostInPdpPage,
				"get Product Rate");
		String rte = rate.trim().replace("Now ", "");
		return rte;

	}

	public void clickOnOfferDetails() throws Exception {
		BrowserActions.scrollToView(detailsLink, driver);
		BrowserActions.clickOnElement(detailsLink, driver,
				"view details link");
	}

	public void clickOnCloseIcon() throws Exception {
		BrowserActions.scrollToView(linkCloseIcon, driver);
		BrowserActions.clickOnElement(linkCloseIcon, driver,
				"view details link");
	}

	public String getBogoMessage() throws Exception{
		String txtMessageOfBuyOneProduct= BrowserActions.getText(driver, txtBogoMessage, "Get the text message of the buy one get one free product");
		return txtMessageOfBuyOneProduct;
	}

	/**
	 * to get the text On the GWP pop up
	 * @return
	 * @throws Exception
	 */
	public String getFreeOfferTxt() throws Exception{
		String txtFreeOffers=BrowserActions.getText(driver, txtBonusDescription, "Text Decription On the GWP offer pop_up");
		return txtFreeOffers;
	}



	public void clickOnViewDetails() throws Exception {
		BrowserActions.clickOnElement(lnkViewDetails.get(0), driver,
				"view details link");
		BrowserActions.nap(5);
	}
	
	/**
	 * To verify color greyed out
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyColorGreyedOut() throws Exception {
		boolean status = false;
		List<WebElement> lstAllColors = driver.findElements(By.cssSelector(".swatches.color>li[class*='selectable']"));
		for(WebElement element : lstAllColors) {
			BrowserActions.scrollToViewElement(element, driver);
			if(BrowserActions.getTextFromAttribute(driver, element, "class", "Swatch icons").contains("unselectable"))
				status = true;
		}
		return status;
	}
	
	/**
	 * To verify thumbnail image is displayed under primary image
	 * @param noOfImages
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyThumbnailImgDisplayedUnderPrimaryImg(int noOfImages) throws Exception {
		boolean status = false;
		List<WebElement> lstElements = driver.findElements(By.cssSelector(".product-primary-image + .product-zoom-action + #thumbnails .thumbnail-link"));
		if(lstElements.size() == noOfImages) 
			status = true;
		return status;
	}
   /**
    * To click instore pickup radio button
    * @throws Exception
    */
   public void clickOnInStorePickUpRadio() throws Exception
	{
		BrowserActions.selectRadioOrCheckbox(rdoInStorePickup, "YES");
	}
	/**
	 * To click Change your location link
	 * @throws Exception
	 */
	public void clickOnChangeYourLocation() throws Exception
	{
		BrowserActions.clickOnElement(lnkChangeYourLocation, driver, "Change your location");
	}
	/**
	 * To enter the zipcode
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public boolean EnterZipCode(String textToSearch) throws Exception {
		boolean status = false;
		SearchUtils.doSearch(InputtxtFieldEnterZipCode, btnSearchZipCode, textToSearch, driver);
		status = true;
		return status;
	}
	
	public void enterZipcodeForStoreSearch(String zipcode) throws Exception {		
		if (Utils.waitForElement(driver, lnkChangeLocation)) {
			BrowserActions.clickOnElement(lnkChangeLocation, driver,
					"change location");
		}
		BrowserActions.typeOnTextField(txtZipcode, zipcode, driver, "zip code");
		BrowserActions.clickOnElement(btnSearchStore, driver, "button search");		
	}
	
	public void scrollDownToReviewTab() throws Exception {
		BrowserActions.scrollToViewElement(reviewTab, driver);
		BrowserActions.clickOnElement(reviewTab, driver,"ReviewTab");
	}
}
