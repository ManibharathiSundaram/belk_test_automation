package com.belk.pages;

import io.netty.util.internal.ThreadLocalRandom;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class CouponsPage extends LoadableComponent<CouponsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public static final String URL = "Coupon-Show";
	String runPltfrm = Utils.getRunPlatForm();
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	public MiniCartPage miniCartPage;

	
	@FindBy(css = "#primary>.coupon-row")
	List<WebElement> lstCouponRow;
	
	@FindBy(css = ".add-coupon-to-cart")
	List<WebElement> CouponCode;
	

	@FindBy(css = "div[id='primary']>div[class='coupon-row']")
	List<WebElement> couponPanel;

	@FindBy(css = ".coupon-row-body>h3>strong>span")
	List<WebElement> lblCouponName;

	@FindBy(css = ".coupon-row-title")
	List<WebElement> lblExpiryDate;

	@FindBy(css = ".breadcrumb>a[href*='/home']")
	WebElement lnkDesktopHome;

	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lnkMobileHome;

	@FindBy(css = ".coupon-row-title")
	List<WebElement> lstCouponTitle;

	@FindBy(css = ".coupon-row-body")
	List<WebElement> lstCouponBody;

	@FindBy(css = ".coupon-row-code")
	List<WebElement> lstTxtCouponCode;

	
	@FindBy(xpath = ".//*[@id='primary']/div[1]/div[1]/div[2]/a[1]")
	WebElement addToBag;
	
	@FindBy(xpath = ".//*[@id='primary']/div[1]/div[1]/div[2]/a[2]")
	WebElement viewInStore;
	@FindBy(css = "#primary>h1")
	WebElement txtCouponHeading;

	@FindBy(css = "i.fa.fa-angle-right")
	List<WebElement> lstIconExclusionExpand;

	@FindBy(css = "i.fa.fa-angle-down")
	List<WebElement> lstIconExclusionCollapse;

	@FindBy(css = ".add-coupon-to-cart")
	WebElement btnAddCouponToBag;

	@FindBy(css = "div[class*='add-to-cart-success'][style*='block']")
	WebElement txtCouponAddedMessage;

	@FindBy(css = ".coupon-show-exlcusions")
	List<WebElement> lstCouponsExclusions;

	// -------------Left Navigation Section----------------//
	@FindBy(css = "h3[class='toggle']")
	WebElement lblLeftTopNavigationpane;

	@FindBy(css = "div[class='coupon_ads'] h2")
	WebElement lblLeftMiddleNavpane;

	@FindBy(css = ".join-now")
	WebElement lblLeftbottomNavPane;

	@FindBy(css = "div[class='coupon_offers'] ul li")
	List<WebElement> lblLeftTopNavigationContent;

	@FindBy(css = ".coupon_ads>img")
	WebElement lblLeftMiddleNavigationContent;

	@FindBy(css = ".explore-now>p")
	WebElement lblLeftbottomNavigationContent;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".view-in-store-coupon")
	List<WebElement> lstBtnViewInStore;

	@FindBy(css = "a[class='mini-cart-link']")
	WebElement BagForProductSet;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement ClikOnViewBag;

	@FindBy(css = ".minicart-icon")
	WebElement BagForNoProduct;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 * @throws InterruptedException
	 */
	public CouponsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		// Utils.waitForPageLoad(driver);
		if (isPageLoaded && !(lstCouponRow.size() > 0)) {
			Log.fail("Coupons page didn't open up.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		miniCartPage = new MiniCartPage(driver).get();
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To click and verify Home link in the coupon page for redirecting the page
	 */
	public boolean clickAndVerifyHomeLink() throws Exception {
		boolean status = false;
		if (runPltfrm == "desktop") {
			BrowserActions.clickOnElement(lnkDesktopHome, driver, "Breadcrumb link");
		} else if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(lnkMobileHome, driver, "Breadcrumb link");
		}
		if (driver.getCurrentUrl().contains("home")) {
			status = true;
		}
		return status;
	}

	/**
	 * To click on expand icon in coupon page
	 */
	public void clickOnExpandExclusion() throws Exception {
		int rand = ThreadLocalRandom.current().nextInt(1, lstIconExclusionExpand.size());
		BrowserActions.clickOnElement(lstIconExclusionExpand.get(rand - 1), driver, "Select quantity option");
	}

	/**
	 * To click on Add Coupon to bag button in coupon page
	 */
	public void clickAddCouponToBag() throws Exception {

		BrowserActions.clickOnElement(btnAddCouponToBag, driver, "Add coupon to bag button");
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	/**
	 * To get coupon code
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public ArrayList<String> getCouponCode() throws Exception {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtCouponCode.get(0), driver);
		for (WebElement element : lstTxtCouponCode) {
			if (!element.getText().equals(""))
				breadcrumbText.add(element.getText());
		}
		return breadcrumbText;

	}

	@FindBy(css = "div[class='coupon-exlcusions-container'] div[style='display: block;']")
	List<WebElement> lstCouponsexclusionsExpand;

	public void clickOnShowExclusionsLink() throws Exception {
		for (WebElement element : lstCouponsExclusions) {
			BrowserActions.scrollToViewElement(element, driver);
			BrowserActions.clickOnElement(element, driver, "Coupon exclusion");
		}
	}

	/**
	 * To get the success message of "Add Coupon to bag"
	 * 
	 * @return String - Success message
	 * @throws Exception
	 */
	public String getSuccessMessage() throws Exception {

		return (BrowserActions.getText(driver, txtCouponAddedMessage, "Coupon added success message"));

	}

	public ShoppingBagPage ClikOnBagForWithNoProducts() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(BagForNoProduct, driver, "Bag link");
			// BrowserActions.clickOnElement(ClikOnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(BagForNoProduct, driver, "Bag link");
		}

		return new ShoppingBagPage(driver).get();
	}

	public void getCouponPanelCount() throws Exception {
		String endDate = "", couponName = "";
		for (int j = 0; j < couponPanel.size(); j++) {
			try {
				endDate = BrowserActions.getText(driver, lblExpiryDate.get(j), "Get Expiry Date");
				Log.message("Coupon Expiry Date::" + endDate);

				couponName = BrowserActions.getText(driver, lblCouponName.get(j), "Get Coupon name");
				Log.message("Coupon Name::" + couponName);

			} catch (Exception e) {
				Log.message("Coupon Name is not configured in BM");
			}
		}

	}
	public void addToBag() throws Exception {
		BrowserActions.clickOnElement(addToBag, driver, "add-to-bag In coupon page");
		Utils.waitForPageLoad(driver);

	}
	
	
	public void viewInStoreCoupon() throws Exception {
		BrowserActions.clickOnElement(viewInStore, driver, "Click On View in Store");
		Utils.waitForPageLoad(driver);

	}
	
	/**
	 * To Add coupon in bag
	 * 
	 * @throws Exception
	 */
	public void clickAddCouponByIndex(int index) throws Exception {
		
		BrowserActions.javascriptClick(CouponCode.get(index - 1), driver, "coupon");
		Utils.waitForPageLoad(driver);
		
	}
}
