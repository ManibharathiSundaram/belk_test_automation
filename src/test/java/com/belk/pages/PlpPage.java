package com.belk.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

import io.netty.util.internal.ThreadLocalRandom;

public class PlpPage extends LoadableComponent<PlpPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of PLP Page - Ends ****************************
	 **********************************************************************************************/

	private static final String SIZE = "div.refinementSize";
	private static final String BRAND = "div[class='refinement brand ']";
	private static final String LNK_VIEW_MORE_LESS = ".view-more-less";

	private static final String COLOR = "div.refinementColor";

	// --------------- Header Section ---------------------- //
	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement bcLastCategoryName;

	@FindBy(css = ".breadcrumb-refinement")
	WebElement lblBreadcrumbRefinement;

	@FindBy(css = "div[class='backtorefine']")
	WebElement btnBackarrowLeftNav;

	@FindBy(className = "pt_product-search-result")
	WebElement readyElement;

	@FindBy(css = "label[for='q'] span[class='hide-mobile']")
	WebElement lblSearchText;

	@FindBy(css = "label[for='q'] span[class='hide-desktop']")
	WebElement lblSearchTextMobile;

	@FindBy(css = "h3.refinement-header+div.category-refinement>ul>li>h4>a")
	public List<WebElement> lstSubCategoryRefinement;

	// --------------- Breadcrumb Section ---------------------- //

	@FindBy(className = "breadcrumb-refinement-value")
	List<WebElement> txtBreadCrumbValue;

	@FindBy(css = "span[class='breadcrumb-element breadcrumb-result-text']")
	WebElement txtSearchResultInBreadcrumb;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	// --------------- Left Navigation Section ----------------- //

	@FindBy(css = "div[class='refineby-attribute']")
	WebElement sectionFilterBy;

	@FindBy(css = ".device-clear-all")
	WebElement lnkClearAll;

	// ----------- Refinement - Refinement Size ------------ //

	@FindBy(css = SIZE)
	WebElement btnsize;

	@FindBy(css = SIZE + " ul[style*='none']")
	public WebElement btnSizeExpand;

	@FindBy(css = SIZE + " i[class='toggle-plus-minus']")
	WebElement btnSizeCollapse;

	@FindBy(css = SIZE + " ul li a")
	List<WebElement> listSelectSize;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilter;

	// ----------- Refinement - Brand ------------ //

	@FindBy(css = BRAND)
	WebElement lblBrand;

	@FindBy(css = BRAND + " ul li")
	List<WebElement> lstBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> lstSubBrand;

	@FindBy(css = ".refinement")
	List<WebElement> lstRefinementFilterOptions;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = BRAND + " span[class='viewmore-refinement']")
	WebElement btnBrandViewMore;

	@FindBy(css = BRAND + " ul li")
	List<WebElement> listBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> listSubBrand;

	// -------------- Result product section ------------------ //

	@FindBy(css = "#results-products")
	WebElement txtProductFound;

	// ---------------- Search Result Filter Section ------------- //

	@FindBy(css = "div[class='search-result-options']")
	WebElement sectionSearchResult;

	@FindBy(css = ".sort-by .custom-select")
	WebElement drpSortBy;

	@FindBy(css = ".sort-by .selection-list li")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "i[data-option='column']")
	WebElement toggleGrid;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .custom-select")
	WebElement drpItemsPerPage;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .custom-select")
	WebElement drpItemsPerPageBottom;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptions;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptionsBottom;

	// ---------------- Result Hit Section ------------- //

	@FindBy(css = ".results-hits")
	WebElement txtResultHits;

	// --------------- Pagination Section ------------- //

	@FindBy(css = ".pagination li")
	List<WebElement> lstPageNos;

	@FindBy(css = ".page-next i")
	WebElement iconNextPage;

	@FindBy(css = ".page-last i")
	WebElement iconLastPage;

	@FindBy(css = ".page-previous i")
	WebElement iconPreviousPage;

	@FindBy(css = ".page-first i")
	WebElement iconFirstPage;

	@FindBy(css = "div[class*='category-refinement']>ul>li.current a")
	WebElement Style;

	// -------------- Product List Section ----------- //

	@FindBy(css = "ul[id='search-result-items']>li")
	List<WebElement> lstProducts;

	@FindBy(css = ".product-swatches")
	List<WebElement> lstProductsWithColorSwatch;

	@FindBy(className = "name-link")
	List<WebElement> lstProductNames;

	@FindBy(css = ".thumb-link>picture>img")
	List<WebElement> lstProductImages;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	// ---------- Spinner ------------ //

	@FindBy(css = ".loader[style*='block']")
	WebElement plpSpinner;

	@FindBy(css = ".filterby-refinement.hide-desktop>a")
	WebElement btnFilterByMobile;

	@FindBy(css = "#category-level-1 li a")
	List<WebElement> lstCatagory;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement iconCloseMenu;

	@FindBy(css = "div[id='secondary'] div[class*='refinement refinementSize'] h3")
	WebElement txtSize;

	@FindBy(css = "div.size>h3>i")
	WebElement btnSizetoggle;

	@FindBy(css = ".selected>a>span:nth-child(2)")
	WebElement getBrandName;

	@FindBy(css = ".refinement-header")
	List<WebElement> lstCategoryRefinement;

	@FindBy(css = ".refinement-header")
	WebElement textCatagoryrefinement;

	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lnkBreadcrumbInmobile;

	@FindBy(css = "div[class='search-result-options last']>div[class='backtotop']>a")
	WebElement txtBacktoTop;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleListBottom;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleGridBottom;

	@FindBy(css = "ul[id='search-result-items']>li")
	public List<WebElement> totalProducts;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(xpath = ".//*[@id='d4b4cd10e98331ab08749eeeff']/div[1]/div[1]/a[1]/picture/img")
	WebElement imgProductPlp;

	@FindBy(css = "div[id='wrapper']>div[id='main']>div[class='refinements']")
	WebElement leftNavigationMobile;

	@FindBy(css = COLOR)
	WebElement btnColor;

	@FindBy(css = COLOR + " i[class='toggle-plus-minus']")
	WebElement btnColorCollapse;

	@FindBy(css = "div.refinementColor> ul li a>i")
	List<WebElement> listSelectColor;

	@FindBy(css = COLOR
			+ " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilterColor;

	@FindBy(css = "div.refinementColor> ul>.unselectable.hide")
	WebElement UnSelectable;

	@FindBy(css = ".viewmore-refinement")
	WebElement viewMoreColors;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Juliana Rail Straight Jeans']")
	WebElement productBrand1;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Size Colored Denim Jeans']")
	WebElement productbrand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(3)")
	WebElement Brand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(4) > ul > li > a > i")
	WebElement Brand2Product;

	@FindBy(css = BRAND + " i[class='toggle-plus-minus']")
	WebElement btnBrandCollapse;

	@FindBy(css = ".page-3")
	WebElement page3;

	@FindBy(css = ".toggle-grid>i[data-option='wide']")
	WebElement listView;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	WebElement FirstSelectedFilter;

	@FindBy(css = "div[id='secondary']")
	WebElement leftNavPanel;

	@FindBy(css = ".toggle-grid>i")
	WebElement fldToggleView;

	@FindBy(css = "#secondary .refinement,div[class='filterby-refinement hide-desktop']")
	List<WebElement> lstLeftNavRefinement;

	@FindBy(css = "a.thumb-link>picture>img")
	List<WebElement> lstImgPrimary;

	@FindBy(css = ".breadcrumb-refinement-value>span")
	WebElement filteredValue;

	@FindBy(css = "div.breadcrumb.hide-mobile > span.breadcrumb-refinement > span:nth-child(3) > span")
	WebElement filteredSecondValue;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a")
	WebElement pajamaSets;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a>i:nth-child(1)")
	WebElement arrowInPajamaSets;

	@FindBy(css = "#category-level-1>li[class='expandable toggle-down']")
	WebElement expandanbleToggle;

	@FindBy(css = ".refinement-header.toggle ")
	List<WebElement> lstCategoryRefinementToggle;

	@FindBy(css = "div ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> chkRefinMentActive;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement IconCloseMenu;

	@FindBy(css = ".loader[style*='block']")
	WebElement searchResultsSpinner;

	@FindBy(css = ".loader[style*='block']")
	WebElement Spinner;

	@FindBy(css = "#main")
	WebElement pageSource;

	@FindBy(css = ".grid-clm>p>a>img")
	WebElement ImgBanner;

	@FindBy(css = "div[class='primary-content']>div[class='search-result-content ']>ul>li:nth-child(1)>div")
	// @FindBy(css =
	// "div[class='primary-content']>div[class='search-result-content ']>ul>li:nth-child(8)>div>div[class='product-promo']>div")
	WebElement promoMessage;

	@FindBy(css = "div[id='dab0de1244e8706a5fc82f67ad']>div[class='promotional-message']>a")
	WebElement promoMessageShirt;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(1)")
	WebElement Brand1;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(2) > ul > li:nth-child(1) > a > i")
	WebElement Brand1Product;

	@FindBy(css = ".search-result-options>div>span>i[data-option='wide']")
	WebElement toggleList;

	@FindBy(css = "span.toggle-grid:nth-child(6) > i[data-option='column']")
	WebElement girdView;

	@FindBy(css = ".shipping-promotion")
	WebElement txtFreeShipping;

	/**
	 * To get the product image srs
	 * 
	 * @return String
	 */
	public String getProductImageUrl() {
		return imgProductPlp.getAttribute("src");

	}

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public PlpPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("PLP Page did not open up. Site might be down.", driver);
		}
	}

	// --------------- Header Section ---------------------- //
	/**
	 * 
	 * @throws Exception
	 */
	public void clickSearchTextBox() throws Exception {
		BrowserActions.clickOnElement(lblSearchText, driver, "Search Text Box");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get a text from search result page
	 * 
	 * @return: String - text from search result page
	 * @throws Exception
	 */
	public String getTextFromSearchResult() throws Exception {
		String txtSearchTextBox = BrowserActions.getText(driver,
				txtSearchResultInBreadcrumb,
				"Fetching the search text value  in the search result page");
		return txtSearchTextBox;
	}

	/**
	 * To get a text from search result page
	 * 
	 * @param lblSearchText
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from search result page
	 * @throws Exception
	 */
	public String getTextFromSearchTextBox() throws Exception {
		String txtSearchTextBox = null;
		if (Utils.getRunPlatForm() == "mobile")
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchTextMobile,
							"Fetching the search text value  in the search result page");
		else
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchText,
							"Fetching the search text value  in the search result page");
		return txtSearchTextBox;
	}

	// --------------- Breadcrumb Section ---------------------- //

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 */
	public ArrayList<String> getTextInBreadcrumb() {
		ArrayList<String> breadcrumbText = new ArrayList<>();
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To get breadcrumb last value
	 * 
	 * @throws Exception
	 */
	public String getBreadCrumbLastValue() throws Exception {
		return (BrowserActions.getText(driver, txtBreadCrumbValue.get(0),
				"breadcrumb value")).replace(" x", "");
	}

	/**
	 * To click and navigate to a category in breadcrumb
	 * 
	 * @param categoryName
	 * @throws Exception
	 */
	public void clickCategoryByNameInBreadcrumb(String categoryName)
			throws Exception {
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (element.getText().equalsIgnoreCase(categoryName)) {
					BrowserActions.clickOnElement(element, driver,
							"Category in breadcrumb");
					break;
				}
			}
		} else {
			BrowserActions.clickOnElement(txtProductInBreadcrumbMobile, driver,
					"Category in breadcrumb");
		}

	}

	// --------------- Left Navigation Section ----------------- //

	/**
	 * To get the List of Items' text in Left Navigation - Category Refinement
	 * 
	 * @return List<String> - List of Items' Text
	 * @throws Exception
	 */
	public List<String> getLeftNavigationListText() throws Exception {
		List<String> l3CategoryText = new ArrayList<String>();

		for (int i = 0; i < lstSubCategoryRefinement.size(); i++) {

			String temp = BrowserActions.getText(driver,
					lstSubCategoryRefinement.get(i), "left nav category name");
			l3CategoryText.add(temp.trim());
		}
		return l3CategoryText;
	}

	/**
	 * To get the List of Items' Count in Left Navigation - Category Refinement
	 * 
	 * @return List<String> - List of Items' Count
	 * @throws Exception
	 */
	public List<String> getCategoryRefinementItemsCount() throws Exception {
		List<String> l3CategoryText = new ArrayList<String>();

		for (int i = 0; i < lstSubCategoryRefinement.size(); i++) {
			String temp = lstSubCategoryRefinement.get(i)
					.findElement(By.cssSelector(".refine-count"))
					.getAttribute("innerHTML").replace("(", "")
					.replace(")", "").trim();
			l3CategoryText.add(temp);
		}
		return l3CategoryText;
	}

	/**
	 * To click the collapse/expand button of 'filterType' in the filter panel
	 * 
	 * @param filterType
	 *            - Eg: color, size
	 * @throws Exception
	 */
	public void clickCollapseExpandToggle(String filterType) throws Exception {
		WebElement element = null;
		for (WebElement element1 : lstRefinementFilterOptions) {
			if (element1.getAttribute("class").contains(filterType)) {
				element1 = element1.findElement(By.cssSelector("h3 i"));
				element = element1;
				break;
			}
		}
		BrowserActions
				.clickOnElement(element, driver, "Collapse/Expand toggle");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To click the view more/less link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickViewMoreOrLessLnk(String filterType) throws Exception {
		WebElement element = BrowserActions.getMachingTextElementFromList(
				lstRefinementFilterOptions, filterType, "contains");
		BrowserActions.clickOnElement(
				element.findElement(By.cssSelector(LNK_VIEW_MORE_LESS)),
				driver, "click on the Tab");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To click 'Clear All' link
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public void clickClearAllLnk(String filterType) throws Exception {
		BrowserActions.clickOnElement(lnkClearAll, driver, "Clear all link");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To verify expand toggle is displayed for 'filterType'
	 * 
	 * @param filterType
	 * @throws Exception
	 */
	public boolean verifyExpandToggle(String filterType) throws Exception {
		boolean status = false;
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (filterType.equals("price")) {
				filterType = filterType.replace("price", " ");
			}

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (toggle.getAttribute("class").contains("expanded")) {
			status = true;
		}
		return status;
	}

	// ----------- Refinement - Brand ------------ //

	/**
	 * To verify the brand group
	 * 
	 * @throws Exception
	 */
	public void verifyBrandGroup() throws Exception {
		String pattern = ("[A-Z]+");
		Pattern p = Pattern.compile(pattern);
		for (int brand = 0; brand < lstBrand.size(); brand++) {
			String brandname = lstBrand.get(brand).getText();
			Matcher m = p.matcher(brandname);
			if (m.find()) {
				lstBrand.get(brand).click();
				if (lstSubBrand.size() > 5) {
					((JavascriptExecutor) driver)
							.executeScript("scroll(0,250);");
					break;
				}
			}

		}
	}

	/**
	 * To select first brand
	 * 
	 * @throws Exception
	 */
	public void selectFirstBrand() throws Exception {
		for (int brand = 0; brand < listBrand.size(); brand++) {
			listBrand.get(brand).click();
			SearchUtils.selectRefinementFirstValue(listSubBrand, driver);
			break;
		}
	}

	/**
	 * To unselect first brand
	 * 
	 * @throws Exception
	 */
	public void UnSelectSelectedFirstBrand() throws Exception {
		for (int brand = 0; brand < listBrand.size(); brand++) {
			listBrand.get(brand).click();
			SearchUtils.selectRefinementFirstValue(listSubBrand, driver);
			Utils.waitForPageLoad(driver);
			SearchUtils.unSelectRefinementFirstValue(listSubBrand, driver);
			Utils.waitForPageLoad(driver);
			break;
		}
	}

	/**
	 * To verify the multiple selected brand
	 * 
	 * @throws Exception
	 */
	public void verifySelectMultipleBrand() throws Exception {
		String pattern = ("[A-Z]+");
		Pattern p = Pattern.compile(pattern);
		for (int brand = 0; brand < lstBrand.size(); brand++) {
			String brandname = lstBrand.get(brand).getText();
			Matcher m = p.matcher(brandname);
			if (m.find()) {
				lstBrand.get(brand).click();
				for (int subBrand = 0; subBrand < lstSubBrand.size(); subBrand++) {
					lstSubBrand.get(subBrand).click();
					if (subBrand == '2') {
						break;
					}
				}
			}
		}

	}

	// -------------- Result product section ------------------ //

	/**
	 * To verify the multiple selected brand
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getProductsFound() throws Exception {
		return BrowserActions
				.getText(driver, txtProductFound, "Products Found");

	}

	// ---------------- Search Result Filter Section ------------- //

	/**
	 * To select option from sort by dropdown randomly
	 * 
	 * @return String
	 */
	public String selectSortByDropDownRandom() {
		Select selectBox = new Select(drpSortBy);
		int rand = ThreadLocalRandom.current().nextInt(1,
				lstSortByOptions.size());
		selectBox.selectByIndex(rand - 1);
		waitUntilPlpSpinnerDisappear();
		return lstSortByOptions.get(rand - 1).getText();
	}

	/**
	 * To select option from sort by dropdown randomly
	 * 
	 * @param index
	 * @return String
	 */
	public String selectSortByDropDownByIndex(int index) {
		Select selectBox = new Select(drpSortBy.findElement(By
				.id("grid-sort-header")));
		selectBox.selectByIndex(index - 1);
		waitUntilPlpSpinnerDisappear();
		return lstSortByOptions.get(index - 1).getText();
	}

	/**
	 * To click Grid/List toggle
	 * 
	 * @param toggleType
	 *            - 'Grid' or 'List'
	 * @throws Exception
	 */
	public void clickGridListToggle(String toggleType) throws Exception {
		WebElement element = null;
		if (toggleType.equals("Grid")) {
			element = toggleGrid;
		} else if (toggleType.equals("List")) {
			element = listView;
		}
		BrowserActions.clickOnElement(element, driver, "" + toggleType
				+ " toggle");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To select option from Items Per Page dropdown randomly
	 * 
	 * @param bottomType
	 *            - 'bottom'
	 * @return String
	 * @throws Exception
	 */
	public String selectItemsPerPageDropDownRandom(String... bottomType)
			throws Exception {
		WebElement element = null;
		List<WebElement> lstElements = null;
		if (bottomType.length != 0) {
			element = drpItemsPerPageBottom;
			lstElements = lstItemsPerPageOptionsBottom;
		} else {
			element = drpItemsPerPage;
			lstElements = lstItemsPerPageOptions;
		}
		BrowserActions.clickOnElement(element, driver,
				"Items per page dropdown");
		int rand = ThreadLocalRandom.current().nextInt(1, lstElements.size());
		BrowserActions.clickOnElement(lstElements.get(rand - 1), driver,
				"Items per page dropdown");
		waitUntilPlpSpinnerDisappear();
		return lstElements.get(rand - 1).getText();
	}

	/**
	 * To select option from Items Per Page by dropdown randomly
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String selectItemsPerPageDropDownByIndex(int index,
			String... bottomType) throws Exception {
		WebElement element = null;
		List<WebElement> lstElements = null;
		if (bottomType.length != 0) {
			element = drpItemsPerPageBottom;
			lstElements = lstItemsPerPageOptionsBottom;
		} else {
			element = drpItemsPerPage;
			lstElements = lstItemsPerPageOptions;
		}
		BrowserActions.clickOnElement(element, driver,
				"Items per page dropdown");
		BrowserActions.clickOnElement(lstElements.get(index - 1), driver,
				"Items per page dropdown");
		waitUntilPlpSpinnerDisappear();
		return lstElements.get(index - 1).getText();
	}

	// ---------------- Result Hit Section ------------- //

	/**
	 * To get results hits text
	 * 
	 * @return String
	 */
	public String getResultHitsText() {
		return txtResultHits.getText();
	}

	// --------------- Pagination Section ------------- //

	/**
	 * To navigate to page in the pagination
	 * 
	 * @param pageNo
	 * @throws Exception
	 */
	public void navigateToPage(String pageNo) throws Exception {
		if (pageNo.equals("Last")) {
			BrowserActions.clickOnElement(iconLastPage, driver, "Last Page");
		} else if (pageNo.equals("First")) {
			BrowserActions.clickOnElement(iconFirstPage, driver, "First Page");
		} else {
			WebElement element = BrowserActions.getMachingTextElementFromList(
					lstPageNos, pageNo, "equals");
			BrowserActions.clickOnElement(element, driver, "Page No " + pageNo
					+ "");
		}
		waitUntilPlpSpinnerDisappear();
	}

	// -------------- Product List Section ----------- //

	/**
	 * To select the product by index
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage selectProductByIndex(int index) throws Exception {
		BrowserActions.scrollToViewElement(lstProducts.get(index - 1), driver);
		BrowserActions.clickOnElement(lstProducts.get(index - 1), driver,
				"Select quantity option");
		return new PdpPage(driver).get();
	}

	/**
	 * To select the product by index
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage selectProductWithColorSwatch() throws Exception {
		BrowserActions.scrollToViewElement(lstProductsWithColorSwatch.get(0),
				driver);
		WebElement element = lstProductsWithColorSwatch.get(0)
				.findElement(By.xpath(".."))
				.findElement(By.cssSelector(".product-name a"));
		BrowserActions
				.clickOnElement(element, driver, "Select quantity option");
		return new PdpPage(driver).get();
	}

	/**
	 * To get the product name by index
	 * 
	 * @index index of an image
	 * @return String
	 * @throws Exception
	 */
	public String getProductImageUrlByIndex(int index) throws Exception {

		return lstProductImages.get(index - 1).getAttribute("src");

	}

	/**
	 * To navigate to PDPPage with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDPWithProductId() throws Exception {
		if (lstProducts.size() > 0) {
			BrowserActions.clickOnElement(lstProducts.get(0), driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new PdpPage(driver).get();
	}

	/**
	 * To navigating to PDPPage
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage navigateToPDP() throws Exception {
		if (lstProductImages.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(1,
					lstProducts.size());
			BrowserActions.scrollToViewElement(lstProductImages.get(rand - 1),
					driver);
			BrowserActions.clickOnElement(lstProductImages.get(rand - 1)
					.findElement(By.xpath("..")), driver,
					"Select quantity option");
		}
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickView() throws Exception {
		if (lstProductImages.size() > 0) {
			int rand = ThreadLocalRandom.current().nextInt(1,
					lstProductImages.size());
			BrowserActions.mouseHover(driver, lstProductImages.get(rand - 1));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	/**
	 * To navigate to quickView Page with productID
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public QuickViewPage navigateToQuickViewWithProductId() throws Exception {
		if (lstProductImages.size() > 0) {
			BrowserActions.mouseHover(driver, lstProductImages.get(0));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	// ---------- Spinner ------------ //

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, plpSpinner);
	}

	/**
	 * To navigate to Category LandingP age
	 * 
	 * @return PdpPage
	 * @throws Exception
	 */
	public CategoryLandingPage navigateToCLP() throws Exception {
		if (lstCatagory.size() > 0) {
			int rand = Utils.getRandom(0, lstCatagory.size());
			BrowserActions.scrollToViewElement(lstCatagory.get(rand), driver);
			BrowserActions.clickOnElement(lstCatagory.get(rand), driver,
					"Select ");
		}
		return new CategoryLandingPage(driver).get();
	}

	/**
	 * To navigate to CategoryLandingPage
	 * 
	 * @return CategoryLandingPage
	 * @throws Exception
	 */
	public PlpPage selectCatgeoryRefinement() throws Exception {
		List<WebElement> lstElement = null;
		String txtsubcategory = null;
		WebElement element = null;
		for (WebElement element1 : lstCategoryRefinement) {
			element = element1;
			break;
		}

		lstElement = element
				.findElements(By
						.xpath("//following-sibling::div[not(contains(@class,'hide'))]/ul/li"));
		for (int i = 0; i < lstElement.size(); i++) {
			WebElement e = lstElement.get(i);
			if (e.findElement(By.xpath("..")).getAttribute("class")
					.contains("sub-cat-3")) {
				String txtcategory = e.getText();
				if (txtcategory != null) {
					e.click();
					Utils.waitForPageLoad(driver);
				}
				if (e.findElement(By.xpath("..")).getAttribute("class")
						.contains("sub-cat-4")) {
					txtsubcategory = e.getText();
					if (txtsubcategory != null) {
						e.click();
					}

				}
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * To click the Filter By button in Mobile
	 * 
	 * @throws Exception
	 */
	public void clickFilterByInMobile() throws Exception {
		BrowserActions.clickOnElement(btnFilterByMobile, driver,
				"Filter By option in mobile");
	}

	/**
	 * To verify search refinement size,price brand,color is displayed properly
	 * 
	 * @param filterType
	 *            - Ex. color, size
	 * @return
	 * @throws Exception
	 */
	public boolean verifyRefinement(String filterType) throws Exception {
		boolean status = false;
		for (WebElement lstElement : lstRefinementFilterOptions) {
			List<WebElement> lstEle = lstElement.findElements(By
					.cssSelector(".toggle"));
			for (WebElement element : lstEle) {
				if (BrowserActions
						.getText(driver, element, "Refinement option")
						.equalsIgnoreCase(filterType)) {
					status = true;
					break;
				}
			}
		}

		return status;
	}

	/**
	 * To click on close icon in search refinement section
	 */
	public void clickOnCloseIcon() throws Exception {
		BrowserActions.clickOnElement(iconCloseMenu, driver, "Refinement Page");
	}

	/**
	 * To verify the color of the Size text displayed as blue
	 * 
	 * @return boolean - status
	 * @throws Exception
	 */
	public boolean verifyColorOfSizeRefinementText() throws Exception {
		boolean status = false;
		String cssAttr = "color";
		String color = txtSize.getCssValue("color");
		BrowserActions.scrollToViewElement(txtSize, driver);
		status = Utils.verifyCssPropertyForElement(txtSize, cssAttr, color);
		return status;
	}

	/**
	 * To get refinement list options
	 * 
	 * @throws Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();
		// List<WebElement> refinementcount =
		// BrowserActions.checkLocators(driver, lstRefinementFilterOptions + "
		// h3");

		// Getting the web element that matches the 'filterType'
		for (int i = 0; i < lstRefinementFilterOptionsInMobile.size(); i++) {

			WebElement element = lstRefinementFilterOptionsInMobile.get(i)
					.findElement(By.xpath(".."));
			String[] refinementText = element.getAttribute("class").split(" ");
			// List<String> refinementText =
			// Arrays.asList(element.getAttribute("class").trim().split(" "));
			if (refinementText.length == 1) {
				refinement.add("price");
			} else
				refinement.add(refinementText[1]);
		}
		return refinement;
	}

	/**
	 * To verify Catagory refinement is expanded properly
	 * 
	 * @param txtcatagory
	 * @return status
	 * @throws Exception
	 */
	public boolean verifyExpandToggleOfCatagoryRefineMent(String txtcatagory)
			throws Exception {
		boolean status = false;

		if (BrowserActions.getText(driver, textCatagoryrefinement,
				"Catagory refinement").equals(txtcatagory)) {
			// check expand toggle in the catagory refinement
			if (textCatagoryrefinement.getAttribute("class").contains(
					"expanded")) {
				status = true;
			}
		}
		return status;
	}

	/**
	 * To verify Filter By button is displayed with blue color find and blue
	 * outside border
	 */
	public boolean verifyFilterByInMobile() throws Exception {
		boolean status1 = false;
		boolean status2 = false;
		String cssAttrColor = "color";
		String cssValueOfColor = "rgba(16, 114, 181, 1)";
		String cssAttrOfBorder = "border";
		String cssValueOfBorder = "2px solid rgb(16, 114, 181)";

		status1 = Utils.verifyCssPropertyForElement(btnFilterByMobile,
				cssAttrColor, cssValueOfColor);
		status2 = Utils.verifyCssPropertyForElement(btnFilterByMobile,
				cssAttrOfBorder, cssValueOfBorder);
		return (status1 && status2);
	}

	/**
	 * To verify the back to home breadcrumb link is displayed
	 * 
	 * @param categoryName
	 * @throws Exception
	 */
	public boolean VerifyHomeInBreadCrumb() throws Exception {
		boolean status = false;
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (element.getText().equals("Home")) {
					status = true;
				}
			}
		} else if (runPltfrm == "mobile") {
			if (BrowserActions.getText(driver, lnkBreadcrumbInmobile,
					"Back to Home Breadcrumb").equals("Back to Home")) {
				status = true;
			}
		}
		return status;
	}

	public void clickBackToTop() throws Exception {
		BrowserActions.scrollToViewElement(txtBacktoTop, driver);
		BrowserActions.clickOnElement(txtBacktoTop, driver,
				"Click on Back to Top link");
		BrowserActions.nap(2);
	}

	/**
	 * To verify the color of refinement headings. return status-boolean
	 */
	public boolean verifyColorOfRefinementHeadings(String filterType)
			throws Exception {
		boolean status = false;

		for (int i = 1; i < lstRefinementFilterOptions.size() - 1; i++) {
			WebElement toggle = lstRefinementFilterOptions.get(i).findElement(
					By.cssSelector("h3"));
			if (BrowserActions.getText(driver, toggle, "refinement options")
					.trim().equals(filterType)) {
				// Clicking expand toggle if filter list is not expanded
				String cssAttrColor = "color";
				String cssValueOfColor = "rgba(16, 114, 181, 1)";
				status = Utils.verifyCssPropertyForElement(toggle,
						cssAttrColor, cssValueOfColor);
				break;

			}

		}
		return status;
	}

	public PdpPage navigateToPDPByRow(int index) throws Exception {
		if (totalProducts.size() > 0) {
			BrowserActions.clickOnElement(totalProducts.get(index - 1), driver,
					"Select quantity option");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	public QuickViewPage navigateToQuickViewByIndex(int index) throws Exception {
		if (lstProductImages.size() > 0) {
			BrowserActions.mouseHover(driver, lstProductImages.get(index - 1));
			BrowserActions.clickOnElement(btnQuickView, driver,
					"select any product");
		} else {
			throw new Exception("Product is not show in the search result page");
		}
		return new QuickViewPage(driver).get();
	}

	/**
	 * To get the image name along with the URL
	 * 
	 * @param index
	 * @return
	 * @throws Exception
	 */
	public String getImageName(int index) throws Exception {
		String imageUrl = BrowserActions
				.getTextFromAttribute(driver, lstProductImages.get(index - 1),
						"src", "href of indexed image");
		String[] imageUrlSplit = imageUrl.split("&layer=");
		imageUrl = imageUrlSplit[0];
		return imageUrl;
	}

	/**
	 * To get the list of brand names
	 * 
	 * @return ArrayList<String>
	 * @throws Exception
	 */
	public ArrayList<String> getBrandNameInProductlist() throws Exception {

		ArrayList<String> arrlist = new ArrayList<String>();
		for (WebElement element : lstBrandName) {
			arrlist.add(BrowserActions.getText(driver, element,
					"Text from breadcrumb"));
		}
		return arrlist;
	}

	/**
	 * Click on the back button in the Left nav.Desktop
	 * 
	 * @throws Exception
	 */
	public void clickOnBackButton() throws Exception {
		// BrowserActions.mouseHover(driver, btnBackarrowLeftNav);
		BrowserActions.clickOnElement(btnBackarrowLeftNav, driver,
				"Click on the back button in the Left Nav.");

	}

	/**
	 * Verify the back to category option is displayed in the Breadcrumb.
	 * 
	 * @param breadcrumbCategoryy
	 * @return
	 * @throws Exception
	 */

	public boolean verifyBackToCategoyInBreadCrumb(String breadcrumbCategory)
			throws Exception {
		boolean status = false;
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (element.getText().trim()
						.equalsIgnoreCase("Back to " + breadcrumbCategory)) {
					status = true;
				}
			}
		} else if (runPltfrm == "mobile") {
			if (BrowserActions
					.getText(driver, lnkBreadcrumbInmobile,
							"Back to Home Breadcrumb").trim()
					.equalsIgnoreCase("Back to " + breadcrumbCategory)) {
				status = true;
			}
		}
		return status;
	}

	/**
	 * Click on the back to category in breadcrumb and navigate to L1 category
	 * and Home option is displayed.
	 * 
	 * @param breadcrumCategory
	 * @throws Exception
	 */
	public void backToCategoyInBreadCrumbCategory(String breadcrumCategory)
			throws Exception {
		if (BrowserActions
				.getText(driver, lnkBreadcrumbInmobile,
						"Back to Home Breadcrumb").trim()
				.equalsIgnoreCase("Back to " + breadcrumCategory)) {
			BrowserActions.javascriptClick(lnkBreadcrumbInmobile, driver,
					"Click on Category" + breadcrumCategory);

		}
	}

	/**
	 * To select option from sort by dropdown By Index
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedSortByValue() throws Exception {
		return drpSortBy.findElement(By.className("selected-option")).getText()
				.trim();
	}

	/**
	 * To get selected option from sort by dropdown in breadcrumb
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getSelectedSortByValueInBreadcrumb() throws Exception {
		return BrowserActions.getText(driver, lblBreadcrumbRefinement,
				"selected option from sort by dropdown in breadcrumb");
	}

	public boolean verifyFilterByTxtColor() throws Exception {
		boolean status3 = false;
		String cssValueOfColor = "rgba(16, 114, 181, 1)";
		String filterByTxtColor = btnFilterByMobile.getCssValue("color");
		if (cssValueOfColor.equals(filterByTxtColor)) {
			status3 = true;
		}
		return status3;
	}

	public boolean verifyFilterByBtnBorderColor() throws Exception {
		boolean status3 = false;
		String cssValueOfBorder = "rgb(16, 114, 181)";
		String filterByborder = btnFilterByMobile.getCssValue("border");
		if (filterByborder.contains(cssValueOfBorder)) {
			status3 = true;
		}
		return status3;
	}

	/**
	 * To select first size
	 * 
	 * @throws Exception
	 */
	public boolean selectFirstSize() throws Exception {
		boolean status = false;
		for (int size = 0; size < listSelectSize.size(); size++) {
			listSelectSize.get(size).click();
			BrowserActions.nap(3);
			if (BrowserActions.isRadioOrCheckBoxSelected(FirstSelectedFilter)) {
				status = true;
			}
			break;
		}
		return status;
	}

	public boolean selectthirdSize() throws Exception {
		boolean status = false;
		for (int size = 3; size < listSelectSize.size(); size++) {
			listSelectSize.get(size).click();
			BrowserActions.nap(3);
			if (BrowserActions.isRadioOrCheckBoxSelected(FirstSelectedFilter)) {
				status = true;
			}
			break;
		}
		return status;
	}

	public void clickOnSizeInRefinementMobile() throws Exception {
		BrowserActions.javascriptClick(btnSizeCollapse, driver,
				"Clicked on Size Toggle");
	}

	public void clickOnColorInRefinementMobile() throws Exception {
		BrowserActions.javascriptClick(btnColorCollapse, driver,
				"Clicked on Size Toggle");
	}

	public void clickonviewMoreColors() throws Exception {

		BrowserActions.javascriptClick(viewMoreColors, driver,
				"Clicked on view more color");
	}

	public void clickOnBrandInRefinementMobile() throws Exception {
		BrowserActions.javascriptClick(btnBrandCollapse, driver,
				"Clicked on Size Toggle");
	}

	public void clickOnListView() throws Exception {
		BrowserActions.clickOnElement(listView, driver, "Clicked on List view");
	}

	public String getText() throws Exception {
		String paymentType = BrowserActions.getText(driver, getBrandName,
				"Brand Name In PDP page");
		return paymentType;
	}

	public String getImageUrlInListPage(int index) throws Exception {
		return (BrowserActions.getTextFromAttribute(driver,
				lstImgPrimary.get(index - 1), "src", "image src"));

	}

	public boolean verifyProductSize() throws Exception {
		boolean status = false;
		for (int i = 0; i < lstProductImages.size(); i++) {
			WebElement e = lstProductImages.get(i);
			int width = e.getSize().width;
			int height = e.getSize().height;
			if (width == 216 && height == 307) {
				status = true;
			} else {
				status = false;
			}
		}
		return status;
	}

	public boolean verifyLeftPaneInCategoryPage() throws Exception {
		boolean status = false;
		if (lstRefinementFilterOptions.size() > 0) {
			status = true;
		} else {
			status = false;
		}
		return status;
	}

	public String getTextOfFilterValue() throws Exception {
		String FilterValue = BrowserActions.getText(driver, filteredValue,
				"Filtered value displayed ");
		return FilterValue;
	}

	public String getTextOfSecondFilterValue() throws Exception {
		String FilterValue = BrowserActions.getText(driver,
				filteredSecondValue, "Filtered 2nd value displayed ");
		FilterValue = FilterValue.replaceAll("\\s+", "");
		return FilterValue;
	}

	public boolean VerifyCategoryToggle() throws Exception {
		boolean returnValue = false;
		for (int i = 0; i < lstCategoryRefinementToggle.size(); i++) {
			BrowserActions.clickOnElement(lstCategoryRefinementToggle.get(i),
					driver, "Category Refinements Expand Toggle");
			WebElement toggle = lstCategoryRefinementToggle.get(i);
			if (!toggle.getAttribute("class").contains("expanded")) {
				returnValue = true;
				BrowserActions.clickOnElement(
						lstCategoryRefinementToggle.get(i), driver,
						"Category Refinements Expand Toggle");
			}
		}
		return returnValue;
	}

	/**
	 * To wait for Search results spinner
	 */
	public void waitUntilplpSpinnerDisappear() {
		BrowserActions.nap(3);
		Utils.waitUntilElementDisappear(driver, Spinner);
	}

	/**
	 * To click the collapse/expand button of 'filterType' in the filter panel
	 * 
	 * @param filterType
	 *            - Eg: color, size
	 * @throws Exception
	 */
	public void clickCollapseExpandToggle(String filterType, String... collapse)
			throws Exception {
		WebElement element = null;

		for (WebElement element1 : lstRefinementFilterOptions) {
			String a = element1.getAttribute("class");

			if (!filterType.equals(" ")) {
				a = a.trim();
			}
			if (a.equals("refinement " + filterType)) {
				element = element1;
				break;
			}
		}

		WebElement toggle = element.findElement(By.cssSelector("h3"));
		if (collapse.length > 0
				&& toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		} else if (!toggle.getAttribute("class").contains("expanded")) {
			BrowserActions.scrollToViewElement(toggle, driver);
			BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
		}
		waitUntilplpSpinnerDisappear();
	}

	/**
	 * To select/unselect the refinements in the search refinement panel
	 * 
	 * @param filterType
	 * @param optionValue
	 * @return String
	 * @throws Exception
	 */
	public String selectUnselectFilterOptions(String filterType,
			String... optionValue) throws Exception {
		WebElement element = null;
		List<WebElement> lstElement = null;
		String filterValue = null;
		// Getting the web element that matches the 'filterType'

		if (pageSource.getText().contains("We're sorry")) {
			Log.fail(
					"Refinement is not shown in the search result page due to testdata",
					driver);
		} else {
			for (WebElement element1 : lstRefinementFilterOptions) {
				String a = element1.getAttribute("class");

				if (!filterType.equals(" ")) {
					a = a.trim();
				}
				if (a.equals("refinement " + filterType)) {
					element = element1;
					break;
				}
			}
			// Clicking expand toggle if filter list is not expanded

			WebElement toggle = element.findElement(By.cssSelector("h3"));
			if (!toggle.getAttribute("class").contains("expanded")) {
				BrowserActions.scrollToViewElement(toggle, driver);
				BrowserActions.clickOnElement(toggle, driver, "Expand toggle");
			}

			// Getting the list of web elements below the 'filterType'
			lstElement = element.findElement(By.xpath("..")).findElements(
					By.cssSelector("div[class='refinement " + filterType.trim()
							+ " ']>ul>li:not([class*='hide'])"));
			if (!(optionValue.length > 0)) {
				if (lstElement.size() == 1) {
					WebElement e = lstElement.get(0);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				} else {
					int rand = ThreadLocalRandom.current().nextInt(1,
							lstElement.size());
					WebElement e = lstElement.get(rand - 1);
					// Clicking random filter values if not mentioned
					BrowserActions.scrollToViewElement(e, driver);
					filterValue = e.getText();
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Filter values");
				}

			} else {
				WebElement e = BrowserActions.getMachingTextElementFromList(
						lstElement, optionValue[0], "contains");
				// Clicking filter option based on 'optionValue'
				// BrowserActions.clickOnElement(e.findElement(By.cssSelector("a
				// i")),
				// driver, "Filter values");
				WebElement selectoption = e.findElement(By.cssSelector("a i"));
				if (selectoption.getAttribute("class").contains(
						"toggle-check-active")) {
					BrowserActions.javascriptClick(e.findElement(By
							.cssSelector("a i[class*='toggle-check-active']")),
							driver, "Un selecting the Filter values");
				} else {
					BrowserActions.javascriptClick(
							e.findElement(By.cssSelector("a i")), driver,
							"Selectin the Filter values");
				}
				waitUntilplpSpinnerDisappear();
				filterValue = optionValue[0].toString();

			}
		}

		Utils.waitForPageLoad(driver);

		return filterValue;
	}

	/**
	 * To get breadcrumb last value
	 * 
	 * @throws Exception
	 */
	public ArrayList<String> getBreadCrumbLastValues() throws Exception {
		// BrowserActions.scrollToViewElement(txtRefinementBreadCrumb, driver);
		ArrayList<String> values = new ArrayList<>();
		for (WebElement element : txtBreadCrumbValue) {
			values.add(BrowserActions
					.getText(driver, element, "breadcrumb value")
					.replace("x", "").trim());
		}
		return values;
	}

	/**
	 * To click on Quick View based on Product Name
	 * 
	 * @param Product
	 *            Name
	 * @return
	 * 
	 * @throws Exception
	 * 
	 */
	public QuickViewPage navigateToQuickViewbyProductName(String productname)
			throws Exception {
		int productlistsize = lstProductNames.size();
		System.out.println("Number of products displayed in PLP"
				+ productlistsize);
		for (int i = 0; i < productlistsize; i++) {
			System.out.println(lstProductNames.get(i).getText());
			if (productname.contains(lstProductNames.get(i).getText().trim())) {
				System.out.println("Products Match Found");
				BrowserActions.mouseHover(driver, lstProductImages.get(i));
				BrowserActions.clickOnElement(btnQuickView, driver,
						"select any product");
			}
		}
		return new QuickViewPage(driver).get();
	}

	/**
	 * To click on Quick View based on Product Name
	 * 
	 * @param subcategory
	 *            Name
	 * @return
	 * 
	 * @throws Exception
	 * 
	 */
	public void selectsubcatgory(String category) {
		for (int i = 0; i < lstCatagory.size(); i++) {
			if (lstCatagory.get(i).getText().equalsIgnoreCase(category)) {
				lstCatagory.get(i).click();
				break;
			}
		}
	}

	public String getTextPromoMessage() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.scrollToView(promoMessage, driver);
		return BrowserActions.getText(driver, promoMessage,
				"Fetching the Promotion Message On PLP Page");

	}

	public String getTextPromoMessageShirt() throws Exception {
		BrowserActions.scrollToView(promoMessageShirt, driver);
		return BrowserActions.getText(driver, promoMessageShirt,
				"Fetching the Promotion Message On PLP Page");

	}

	public boolean selectBrandOne() throws Exception {
		BrowserActions.javascriptClick(Brand1, driver, "clicked on brand 2");
		BrowserActions.nap(2);
		BrowserActions.javascriptClick(Brand1Product, driver,
				"Clicked on Brand name");
		BrowserActions.nap(3);
		BrowserActions.javascriptClick(Brand1, driver, "clicked on brand 2");
		BrowserActions.nap(3);
		if (BrowserActions.isRadioOrCheckBoxSelected(Brand1Product)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean selectSecondBrand() throws Exception {
		BrowserActions.javascriptClick(Brand2, driver, "clicked on brand 2");
		BrowserActions.nap(2);
		BrowserActions.javascriptClick(Brand2Product, driver,
				"Clicked on Brand name");
		BrowserActions.nap(3);
		BrowserActions.javascriptClick(Brand2, driver, "clicked on brand 2");
		BrowserActions.nap(3);
		if (BrowserActions.isRadioOrCheckBoxSelected(Brand2Product)) {
			return true;
		} else {
			return false;
		}
	}

	public void clickOnPage3() throws Exception {
		BrowserActions.clickOnElement(page3, driver,
				"clicked on Page 3 element");
		BrowserActions.nap(4);
	}

	public String verifyLThreeCatagoryTitleBold() throws Exception {
		String Actualcolor = Style.getCssValue("font-family").split(",")[0];
		return Actualcolor;

	}

	/**
	 * To Expand first brand
	 * 
	 * @throws Exception
	 */
	public void ExpandFirstBrand() throws Exception {
		for (int brand = 0; brand < listBrand.size(); brand++) {
			listBrand.get(brand).click();
			break;
		}

	}

}
