
package com.belk.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.ShippingPageUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class ShipToMultipleAddressPage extends LoadableComponent<ShipToMultipleAddressPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	public String txtFirstname = "#dwfrm_multishipping_editAddress_addressFields_firstName";
	public String txtLastname = "#dwfrm_multishipping_editAddress_addressFields_lastName";
	public String btnCountry = "select[id='dwfrm_singleshipping_shippingAddress_addressFields_country']";
	public String txtZipcode = "#dwfrm_multishipping_editAddress_addressFields_postal";
	public String txtAddress = "#dwfrm_multishipping_editAddress_addressFields_address1";
	public String txtAddress2 = "#dwfrm_multishipping_editAddress_addressFields_address2";
	public String txtCity = "#dwfrm_multishipping_editAddress_addressFields_city";
	public String btnState = ".state-row .selected-option";
	public String txtPhoneNo = "#dwfrm_multishipping_editAddress_addressFields_phone";
	public String btnContinueToBilling = "button[name='dwfrm_singleshipping_shippingAddress_save']";
	public String txtAddressName = "#dwfrm_multishipping_editAddress_addressid";

	/**********************************************************************************************
	 ********************************* WebElements of Multi Shipping Page **************************
	 **********************************************************************************************/
	@FindBy(css = ".primary-logo>a>img")
	WebElement imgHomePage;

	@FindBy(css = ".edit")
	WebElement btnAddAddress;

	@FindBy(xpath = "/html/body/div[1]/div/div[2]/div/form/div/table/tbody/tr[3]/td[3]/div[1]/div")
	WebElement drpAdd2;
	
	@FindBy(css = ".editaddress")
	WebElement btnEditAddress;
	
	@FindBy(css = "button[value='Save']")
	public WebElement btnSave;
	
	@FindBy(xpath = "/html/body/div[1]/div/div[2]/div/form/div/table/tbody/tr[1]/td[3]/div[1]/div")
	WebElement drpAdd1;
	
	@FindBy(css = ".selection-list>:nth-child(2)")
	public WebElement firstAddress;

	@FindBy(css = ".selection-list>:nth-child(3)")
	public WebElement secondAddress;

	@FindBy(css = "#dwfrm_multishipping_editAddress_addressFields_firstName")
	public WebElement txtfirstname;

	@FindBy(css = ".selection-list>li")
	public List<WebElement> lstAdd;

	@FindBy(css = ".button-fancy-large")
	public WebElement btnContinue;

	@FindBy(css = ".selected-option")
	public WebElement drpState;

	@FindBy(css = "#dwfrm_addForm_useOrig")
	public WebElement btContinue;

	@FindBy(xpath = ".selected-option.selected")
	WebElement drpAddInModel;

	@FindBy(css = ".backto-cart>a")
	WebElement lnkReturnToShippingAddress;

	@FindBy(css = ".item-details")
	WebElement itemDetailsdiv;

	@FindBy(css = "#dwfrm_addForm_selectAddr")
	WebElement btnContinueInPopUp;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.qas-dialog.ui-draggable")
	WebElement addressValidationModal;

	@FindBy(css = "input[name='dwfrm_addForm_selection'][value='useOriginal']")
	WebElement radiobtnOriginalAddress;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public ShipToMultipleAddressPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, btnAddAddress))) {
			Log.fail("Shopping Bag page did not open up.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	public void selectAdd1ByIndex(int index) throws Exception {
		clickAddressDropown1();
		BrowserActions.clickOnElement(lstAdd.get(index - 1), driver, "Address");

		// Utils.waitUntilElementDisappear(driver, spinner);
	}

	public void clickAddressDropown1() throws Exception {
		BrowserActions.clickOnElement(drpAdd1, driver, "Size dropdown");
	}

	public void selectAdd2ByIndex(int index) throws Exception {
		clickAddressDropown2();
		BrowserActions.clickOnElement(lstAdd.get(index - 1), driver, "Address");

		// Utils.waitUntilElementDisappear(driver, spinner);
	}

	public void clickAddressDropown2() throws Exception {
		BrowserActions.clickOnElement(drpAdd2, driver, "Size dropdown");
	}

	public ShippingMethodPage clickContinue() throws Exception {
		BrowserActions.clickOnElement(btnContinue, driver, "Continue Button");
		Utils.waitForPageLoad(driver);
		return new ShippingMethodPage(driver).get();
	}

	public void clickaddLink() throws Exception {

		BrowserActions.clickOnElement(btnAddAddress, driver, "Clicking add link");
		Utils.waitForPageLoad(driver);
	}

	public void clickEditLink() throws Exception {

		BrowserActions.clickOnElement(btnEditAddress, driver, "Clicking add link");
		Utils.waitForPageLoad(driver);
	}

	public void selectAddInAddModelByIndex(int index) throws Exception {
		clickAddressDropownInModel();
		BrowserActions.clickOnElement(lstAdd.get(index - 1), driver, "Address");

		// Utils.waitUntilElementDisappear(driver, spinner);
	}

	public void clickAddressDropownInModel() throws Exception {
		BrowserActions.clickOnElement(drpAddInModel, driver, "Size dropdown");
	}

	public String fillingShippingDetailsAsSignedInUser(String saveToAddressBook, String useAddressForBilling)
			throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_qaFirst" + randomFirstName, txtFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_qaLast" + randomLastName, txtLastname);

		String address = checkoutProperty.getProperty(useAddressForBilling);
		String address1 = address.split("\\|")[0];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		shippingDetails.put("type_address_" + address1, txtAddress);
		shippingDetails.put("type_address_a", txtAddress2);
		shippingDetails.put("type_city_" + city, txtCity);
		shippingDetails.put("select_state_" + state, btnState);
		shippingDetails.put("type_zipcode_" + zipcode, txtZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtPhoneNo);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		return address;
	}

	public String GetAddress(int addressselectboxindex, int addressindex) throws Exception {
		List<WebElement> element = driver.findElements(By.cssSelector(".custom-select div"));
		BrowserActions.clickOnElement(element.get(addressselectboxindex - 1), driver, "SelectDropDown");
		List<WebElement> elementAddress = driver.findElements(By
				.cssSelector(".item-list>tbody>tr:nth-child(1) ul[class='selection-list']>li:not([class='selected'])"));
		String add = BrowserActions.getText(driver, elementAddress.get(addressindex - 1), "Address");
		return add;

	}
	
	public String GetFirstAddress() throws Exception {

		String add = BrowserActions.getText(driver, firstAddress, "Address");
		return add;
	}

	public String GetSecondAddress() throws Exception {

		String add = BrowserActions.getText(driver, secondAddress, "Address");
		return add;
	}

	public void ClickSaveBtn() throws Exception {
		BrowserActions.clickOnElement(btnSave, driver, "Clicking save Button");
		Utils.waitForPageLoad(driver);
	}

	public void ClickHomePage() throws Exception {
		BrowserActions.clickOnElement(imgHomePage, driver, "Clicking Home Page image");
		Utils.waitForPageLoad(driver);
	}

	public void ClickReturnToShippingAddresslink() throws Exception {
		BrowserActions.clickOnElement(lnkReturnToShippingAddress, driver, "ReturnToShippingAddress link");
		Utils.waitForPageLoad(driver);
	}

	public void selectdropdownandAddress(int dropdownindex, int Addressindex) throws Exception {
		clickAddressDropown(dropdownindex);
		BrowserActions.clickOnElement(lstAdd.get(Addressindex - 1), driver, "Address");

		// Utils.waitUntilElementDisappear(driver, spinner);
	}

	public void clickAddressDropown(int dropdownindex) throws Exception {
		List<WebElement> element = driver.findElements(By.cssSelector(".custom-select div"));
		BrowserActions.clickOnElement(element.get(dropdownindex-1), driver, "SelectDropDown");

	}

	public boolean ContinueAddressValidationModalWithDefaults() throws Exception {
		if (Utils.waitForElement(driver, btnContinueInPopUp)) {
			BrowserActions.clickOnElement(btnContinueInPopUp, driver, "Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);
			ClickSaveBtn();
			return true;
		}
		return false;
	}

	public void SelectRadioButton() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.selectRadioOrCheckbox(radiobtnOriginalAddress, "YES");
			Utils.waitForPageLoad(driver);
		}

	}

	public void ClickContinueBtn() throws Exception {
		BrowserActions.clickOnElement(btnContinueInPopUp, driver, "Clicking continue Button");
		Utils.waitForPageLoad(driver);
	}

	public LinkedHashMap<String, String> fillingShippingDetailsInMultiShip(String saveToAddressBook,
			String useAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_qaFirst" + randomFirstName, txtFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_qaLast" + randomLastName, txtLastname);

		String address = checkoutProperty.getProperty(useAddressForBilling);
		String address1 = address.split("\\|")[0];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		shippingDetails.put("type_address_" + address1, txtAddress);
		shippingDetails.put("type_address_a", txtAddress2);
		shippingDetails.put("type_city_" + city, txtCity);
		shippingDetails.put("select_state_" + state, btnState);
		shippingDetails.put("type_zipcode_" + zipcode, txtZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtPhoneNo);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		return shippingDetails;
	}

	public void selectFirstAddressInAddressDropDown() throws Exception {
		List<WebElement> lstProductInShipToMultipleAddress = driver
				.findElements(By.cssSelector("table[class='item-list'] > tbody > tr"));
		for (int i = 0; i < lstProductInShipToMultipleAddress.size(); i = i + 2) {
			WebElement firstValueInDrp = driver
					.findElement(By.cssSelector("table[class='item-list'] > tbody > tr:nth-child(" + (i + 1)
							+ ") .shippingaddress.required .custom-select >ul >li:not([class='selected']):nth-child(2)"));
			WebElement addressDropDown = driver
					.findElement(By.cssSelector("table[class='item-list'] > tbody > tr:nth-child(" + (i + 1)
							+ ") .shippingaddress.required .selected-option"));

			BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElement(firstValueInDrp, driver, "First Address in Dropdown");
			Utils.waitForPageLoad(driver);
		}
	}
	
	public int ListOfAddedAddressCount() throws Exception {
		List<WebElement> elementAddress = driver.findElements(By
				.cssSelector(".item-list>tbody>tr:nth-child(1) ul[class='selection-list']>li:not([class='selected'])"));
		int TotalAddrresCount = elementAddress.size();
		return TotalAddrresCount;
	}
	
}// ShipToMultipleAddressPage