package com.belk.pages.account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class BelkRewardsCreditCardPage extends LoadableComponent<BelkRewardsCreditCardPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = ".accountheader")
	WebElement lblEditHeader;

	/**************** Section - Links *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	/*********************************************************************/

	// *******mobile elements***************

	/*
	 * @FindBy(css = "h3 > a[title='']") WebElement lnkMyAccountMobile;
	 */

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;
	/**********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public BelkRewardsCreditCardPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lblEditHeader))) {
			Log.fail("Edit Page did not open!", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;
	
	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver, "Profile Link");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver, "Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile, driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver, "Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver, "Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver, "Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver, "Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver, "WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver, "WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver, "Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver, "Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}
}
