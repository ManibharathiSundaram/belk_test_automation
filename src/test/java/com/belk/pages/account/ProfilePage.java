package com.belk.pages.account;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.AddressBookUtils;
import com.belk.reusablecomponents.PaymentUtils;
import com.belk.reusablecomponents.BillingPageUtils;
import com.belk.reusablecomponents.StateUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;
import com.btr.proxy.util.PlatformUtil.Browser;

public class ProfilePage extends LoadableComponent<ProfilePage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader
			.getInstance("checkout");
	public Headers headers;
	public Footers footers;
	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	private static String MSG_UNSUBSCRIBE_CONFIRMATION = "You have been successfully unsubscribed from Belk emails.";
	@FindBy(css = ".accountheader")
	WebElement lblEditHeader;

	/**************** Section - Links *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	/*********************************************************************/
	// /////// ------Edit Profile Field --------////////////////

	@FindBy(css = "div[class='account-register account-register-auth']")
	WebElement divEditProfile;

	@FindBy(css = "input[id='dwfrm_profile_customer_firstname']")
	WebElement txtFirstName;

	@FindBy(css = "input[id='dwfrm_profile_customer_lastname']")
	WebElement txtLastName;

	@FindBy(css = "input[id='dwfrm_profile_customer_phone']")
	WebElement txtPhoneNumber;

	@FindBy(css = "input[id='dwfrm_profile_customer_email']")
	WebElement txtEmail;

	@FindBy(css = "input[id='dwfrm_profile_customer_emailconfirm']")
	WebElement txtEmailConfirmation;

	@FindBy(css = "input[id*='dwfrm_profile_login_password']")
	WebElement txtPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_currentpassword']")
	WebElement txtCurrentPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_newpassword']")
	WebElement txtNewPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_newpasswordconfirm']")
	WebElement txtConfirmPassword;

	@FindBy(css = "input[id='dwfrm_profile_customer_addToEmailList']")
	WebElement chkAddToEmailList;

	@FindBy(xpath = "//input[@id='dwfrm_profile_customer_addToEmailList']/parent::div")
	WebElement chkbxAddToEmailList;

	@FindBy(css = "a[class='privacy-policy']")
	WebElement lnkPrivacyPolicy;

	@FindBy(css = "button[name='dwfrm_profile_cancel']")
	WebElement btnCancel;

	@FindBy(css = "button[name='dwfrm_profile_confirm']")
	WebElement btnProfileApplyChanges;

	@FindBy(css = "button[name='dwfrm_profile_changepassword']")
	WebElement btnPasswordApplyChanges;

	@FindBy(css = "div[class='form-caption error-message']")
	WebElement msgEmailAlreadyInUse;

	@FindBy(css = ".default-billing-address .section-header-note")
	WebElement lnkChangeDefaultBilling;

	@FindBy(css = ".default-shipping-address .section-header-note")
	WebElement lnkChangeDefaultShipping;

	@FindBy(css = ".add-card")
	WebElement lnkChangeDefaultPayment;

//	@FindBy(css=".adddefault.adddefault-first>a")
//	WebElement btnSetDefaultAddress;
	
	@FindBy(css=".adddefault.adddefault-first>a")
	WebElement btnSetDefaultAddress;

	@FindBy(css = "div[class='adddefault']>a")
	WebElement btnSetDefaultBilling;	

	// *******mobile elements***************

	/*
	 * @FindBy(css = "h3 > a[title='']") WebElement lnkMyAccountMobile;
	 */

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	@FindBy(css = ".form-row.form-row-button>button[name='dwfrm_profile_confirm']")
	WebElement btnApplyChanges;

	// added from school net

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = "span[id='dwfrm_profile_customer_email-error']")
	WebElement msgInvalidEmailAddress;

	@FindBy(css = ".default-payment-method .cc-type")
	WebElement lblCardType;

	@FindBy(css = ".default-payment-method .cc-owner")
	WebElement lblCardOwner;

	@FindBy(css = ".default-payment-method .cc-number")
	WebElement lblCardNumber;

	@FindBy(css = ".default-payment-method .cc-exp")
	WebElement lblCardExpire;

	@FindBy(css = "a[class='address-create button secondarybutton'][title='Create New Address for this account']")
	WebElement btnSetAsDefaultBilling;

	@FindBy(css = ".savedaddress-list>.field-wrapper>.custom-select>.selected-option")
	WebElement drpSavedAddressInSetDefaultBilling;

	@FindBy(css = ".savedaddress-list>.field-wrapper>.custom-select>.selection-list li:not(.selected)")
	List<WebElement> lstSavedAddressInSetDefaultBillingSortByOptions;

	@FindBy(css = ".savedaddress-list>.field-wrapper>.custom-select>.selection-list .selected")
	WebElement drpSavedAddressSelectedOptionInSetDefaultBilling;

	@FindBy(css = "h1[class='changedefault']")
	WebElement lblTitleInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_addressid']")
	WebElement lblAddressNameInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_firstname']")
	WebElement lblFirstNameInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_lastname']")
	WebElement lblLastNameInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_address1']")
	WebElement lblAddressOneInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_address2']")
	WebElement lblAddressTwoInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_city']")
	WebElement lblCityNameInSetDefaultBilling;

	@FindBy(css = ".state-row>.field-wrapper>.custom-select>.selected-option")
	WebElement drpStateInSetDefaultBilling;

	@FindBy(css = ".state-row>.field-wrapper>.custom-select>.selection-list li:not(.selected)")
	List<WebElement> lstStateInSetDefaultBillingSortByOptions;

	@FindBy(css = ".state-row>.field-wrapper>.custom-select>.selection-list .selected")
	WebElement drpStateSelectedOptionInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_postal']")
	WebElement lblZipcodeInSetDefaultBilling;

	@FindBy(css = "input[id='dwfrm_profile_address_phone']")
	WebElement lblPhoneInSetDefaultBilling;

	@FindBy(css = "button[class='apply-button setdefault-button']")
	WebElement btnSetAsDefaultInSetDefaultBilling;

	@FindBy(css = "button[name='dwfrm_paymentinstruments_creditcards_create']")
	WebElement btnSetAsDefaultInSetDefaultPayment;

	@FindBy(css = "button[class='cancel cancel-button simple'][name='dwfrm_profile_address_cancel']")
	WebElement btnCancelInSetDefaultBilling;

	@FindBy(css = "button[class='cancel cancel-button simple'][name='dwfrm_paymentinstruments_creditcards_cancel']")
	WebElement btnCancelInSetDefaultPayment;

	@FindBy(id = "dwfrm_profile_customer_employeeInfo")
	WebElement chkIAmBelkEmployee;
	
	@FindBy(xpath="//*[@id='RegistrationForm']/fieldset[2]/div[7]/div/div[1]/span")
	WebElement chkIAmBelkEmployeeSpan;

	@FindBy(css = ".employeemsg")
	WebElement chkIAmBelkEmployeeMessage;

	@FindBy(css = "input[id='dwfrm_profile_customer_enableExpressCheckout']")
	WebElement chkExpressCheckout;

	@FindBy(css= ".exp-checkout-description")
	WebElement lblExpressCheckoutDescription;

	@FindBy(id = "dwfrm_profile_customer_employeeNumber")
	WebElement txtEmployeeID;

	@FindBy(id = "dwfrm_profile_customer_storeNumber")
	WebElement txtStoreID;

	@FindBy(xpath = "//input[@id='dwfrm_profile_customer_employeeInfo']/parent::div")
	WebElement ChkboxBelkEmployee;

	@FindBy(css = "div[class='employeeInfo'] > div:nth-of-type(1) > div > div[class='form-caption error-message']")
	WebElement msgInvalidEmployeeID;

	@FindBy(css = "div[class='employeeInfo'] > div:nth-of-type(2) > div > div[class='form-caption error-message']")

	WebElement msgInvalidStoreID;

	@FindBy(css = ".default-payment-method")
	WebElement defaultPaymentMethodPane;

	@FindBy(css = ".default-shipping-address")
	WebElement defaultShippingAddressPane;


	@FindBy(css = ".default-billing-address")
	WebElement defaultBillingAddressPane;

	@FindBy(id = "dialog-container")
	WebElement lblAddressFormDialogContainer;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_owner")
	WebElement txtCardOwnerInSetDefaultPayment;
	

	@FindBy(xpath = "//input[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_defaultcard']/parent::div")
	WebElement chkbxMakePaymentMethod;

	String txtAddressNameLocator = "input[id='dwfrm_profile_address_addressid']";
	String txtFirstNameLocator = "input[id='dwfrm_profile_address_firstname']";
	String txtLastNameLocator = "input[id='dwfrm_profile_address_lastname']";
	String txtAddress1Locator = "input[id='dwfrm_profile_address_address1']";
	String txtAddress2Locator = "input[id='dwfrm_profile_address_address2']";
	String txtCityLocator = "input[id='dwfrm_profile_address_city']";
	String btnSelectedStateLocator = ".state-row .selected-option";
	String txtZipcodeLocator = "input[id='dwfrm_profile_address_postal']";
	String txtPhoneLocator = "input[id='dwfrm_profile_address_phone']";
	String chkMakeDefaultShippingAddrLocator = "input[id='dwfrm_profile_address_defaultAddress_preferredShipAddr']";
	String chkMakeDefaultBillingAddrLocator = "input[id='dwfrm_profile_address_defaultAddress_preferredBillAddr']";
	String txtSelectedSavedAddressLocator = ".savedaddress-list .selected-option";
	
	@FindBy(css="input[id='dwfrm_profile_address_addressid']")
	WebElement txtAddressName;

//	@FindBy(css=".add-card.secondarybutton.button")
//	WebElement btnSetAsDefaultPayment;
	
	@FindBy(css=".add-card")
	WebElement btnSetAsDefaultPayment;
	
	@FindBy(id="applyBtn")
	WebElement btnSetAsDefaultInDialogContainer;
	

	public String btnCardType = "#dwfrm_paymentinstruments_creditcards_newcreditcard_type + .selected-option";
	public String txtNameOnCard = "input[id='dwfrm_paymentinstruments_creditcards_newcreditcard_owner']";
	public String txtCardNo = "input[id*='dwfrm_paymentinstruments_creditcards_newcreditcard_number']";
	public String btnCardExpMonth = ".month .selected-option";
	public String btnCardExpYear = ".year .selected-option";

	@FindBy(css = "div[id='dialog-container']>h1")
	WebElement lblTitleInSetDefaultPayment;

	public String chkMakeDefaltPaymentMethod = "#dwfrm_paymentinstruments_creditcards_newcreditcard_defaultcard";

	@FindBy(css = "span[id*='error']")
	List<WebElement> formError;

	@FindBy(css = "div[class='form-caption error-message']")
	List<WebElement> editPaymentFormError;

	@FindBy(css = "#deliver-options-home")
	WebElement rbUseSuggestedAddress;

	@FindBy(css = "#deliver-options-original")
	WebElement rbUseOriginalAddress;

	@FindBy(css = "#dwfrm_addForm_useOrig")
	WebElement btnContinueAddressSuggestionForNoAddressPopup;
	
	@FindBy(css = "#dwfrm_addForm_selectAddr")
	WebElement btnContinueAddressSuggestion;
	
	
	@FindBy(css="a[class='address-create button secondarybutton'][title='Set Default Address']")
	WebElement btnSetAsDefaultShipping;
	
	@FindBy(css = "div.exp-checkout-description.disabled-desc")	
	WebElement MsgExpresscheckout;
	
	@FindBy(css=".savedcardlist-row")
	WebElement lblSavedCardFromDialog;
	
	@FindBy(css=".unsubscribe-confirmation .success")
	WebElement msgUnsubscribeConfirmationSuccess;
	
	@FindBy(css = "div[class*='paymentmethod-dialog'][style*='block']")
	WebElement paymentMethodDialogOpened;
	
	@FindBy(css = "div[class*='paymentmethod-dialog'][style*='none']")
	WebElement paymentMethodDialogClosed;
	
	@FindBy(css=".border-divider")
	List<WebElement> lstBorderLine;
	
	@FindBy(css="span[id*='dwfrm_profile_login_newpassword']")
	List<WebElement> lstPasswordError;
	
	@FindBy(css = "div[class='employeeInfo'][style*='block']")
	WebElement txtEmployeeInfo;
	
	/**********************************************************************************************/
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public ProfilePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lblEditHeader))) {
			Log.fail("Edit Page did not open!", driver);
		}

	}

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver,
							"Profile Link");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
							driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver,
							"Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile,
							driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver,
							"Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver,
							"Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver,
							"Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
							driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver,
							"Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver,
							"Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver,
							"WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver,
							"WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver,
						"Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver,
						"Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}

	/**
	 * To type the first name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void typeOnFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstName, firstName, driver,
				"First name");
	}

	/**
	 * To type the last name
	 * 
	 * @param lastName
	 * @throws Exception
	 */
	public void typeOnLastName(String lastName) throws Exception {
		BrowserActions.typeOnTextField(txtLastName, lastName, driver,
				"Last name");
	}

	/**
	 * To type the phone number
	 * 
	 * @param phone
	 * @throws Exception
	 */
	public void typeOnPhoneNumber(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneNumber, phone, driver,
				"Phone number");
	}

	/**
	 * To get the user name field's error msg
	 * 
	 * @return dataToBeReturned - username error msg
	 * @throws Exception
	 */
	public String getExpressCheckoutWarningMsg() throws Exception
	 {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, MsgExpresscheckout, "Express Checkout warning msg");
		return dataToBeReturned;
	}
	
	/**
	 * To type the current password
	 * 
	 * @param password
	 * @throws Exception
	 */
	public void typeOnCurrentPassword(String password) throws Exception {
		BrowserActions.typeOnTextField(txtCurrentPassword, password, driver,
				"Current Password");
	}

	/**
	 * To type on new password
	 * 
	 * @param password
	 * @throws Exception
	 */
	public void typeOnNewPassword(String password) throws Exception {
		BrowserActions.typeOnTextField(txtNewPassword, password, driver,
				"New Password");
	}

	/**
	 * To type on confirm new password
	 * 
	 * @param password
	 * @throws Exception
	 */

	public void typeOnConfirmNewPassword(String password) throws Exception{
		BrowserActions.scrollToViewElement(txtConfirmPassword, driver);
		BrowserActions.typeOnTextField(txtConfirmPassword, password, driver, "Confirm New password");

	}

	// Enter Text on email field
	public void typeOnEmailField(String txtToType) throws Exception {
		BrowserActions.scrollToViewElement(txtEmail, driver);
		BrowserActions.typeOnTextField(txtEmail, txtToType, driver, "Email text field");
	}

	// Enter Text on confirm email field
	public void typeOnConfirmEmailField(String txtToType) throws Exception {
		BrowserActions.scrollToViewElement(txtEmailConfirmation, driver);
		BrowserActions.typeOnTextField(txtEmailConfirmation, txtToType, driver, "Confirm Email text field");
	}

	// Enter Text on password field
	public void typeOnPasswordField(String txtToType) throws Exception {
		BrowserActions.scrollToViewElement(txtPassword, driver);
		BrowserActions.typeOnTextField(txtPassword, txtToType, driver, "Password text field");
	}

	// Click on 'Apply Changes' button
	public void clickOnProfileApplyChanges() throws Exception {
		BrowserActions.scrollToViewElement(btnProfileApplyChanges, driver);
		BrowserActions.clickOnElement(btnProfileApplyChanges, driver,
				"Clicked on Profile Apply changes");
		Utils.waitForPageLoad(driver);
	}

	// gets 'Already in use' error message
	public String getEmailAlreadyInUseErrorMessage() throws Exception {
		return BrowserActions.getText(driver, msgEmailAlreadyInUse,
				"gets 'Already in use' error message");
	}

	// Click on 'Email Subscription' button(Check the checkbox)
	public void checkOnEmailSubscriptionCheckbox() throws Exception {
		try {
			BrowserActions.clickOnElement(chkAddToEmailList, driver,
					"Clicked on Email Subscription checkbox");
		} catch (Exception e) {
			BrowserActions.clickOnElement(chkbxAddToEmailList, driver,
					"Clicked on Email Subscription checkbox");
		}
	}

	// Click on 'Email Subscription' button(Uncheck the checkbox)
	public void uncheckOnEmailSubscriptionCheckbox() throws Exception {
		try {
			BrowserActions.scrollToViewElement(chkAddToEmailList, driver);
			BrowserActions.clickOnElement(chkAddToEmailList, driver,
					"Clicked on Email Subscription checkbox");
		} catch (Exception e) {
			BrowserActions.clickOnElement(chkbxAddToEmailList, driver,
					"Clicked on Email Subscription checkbox");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Click on cancel button
	 * 
	 * @throws Exception
	 */
	public MyAccountPage clickOnCancel() throws Exception {
		BrowserActions.scrollToView(btnCancel, driver);
		BrowserActions.clickOnElement(btnCancel, driver, "Cancel button");
		return new MyAccountPage(driver).get();
	}

	/**
	 * To click on Password Apply changes
	 * 
	 * @return
	 * @throws Exception
	 */
	public MyAccountPage clickOnPasswordApplyChanges() throws Exception {
		BrowserActions.scrollToView(btnPasswordApplyChanges, driver);
		BrowserActions.clickOnElement(btnPasswordApplyChanges, driver,
				"Password Apply changes button ");
		return new MyAccountPage(driver).get();
	}

	/**
	 * Click on the change default in the shipping address
	 * 
	 * @throws Exception
	 */
	public void clickOnChangeDefaultInShipping() throws Exception {
		BrowserActions.scrollToView(lnkChangeDefaultShipping, driver);
		BrowserActions.clickOnElement(lnkChangeDefaultShipping, driver,
				"Change default link");

	}

	/**
	 * Click on the change default in the billing address
	 * 
	 * @throws Exception
	 */
	public void clickOnChangeDefaultInBilling() throws Exception {
		BrowserActions.scrollToView(lnkChangeDefaultBilling, driver);
		BrowserActions.clickOnElement(lnkChangeDefaultBilling, driver,
				"Change default link");
	}

	/**
	 * Click on the change default in the payment method
	 * 
	 * @throws Exception
	 */
	public void clickOnChangeDefaultInPayment() throws Exception {
		BrowserActions.scrollToViewElement(lnkChangeDefaultPayment, driver);
		BrowserActions.clickOnElement(lnkChangeDefaultPayment, driver,
				"Change default link");
		Utils.waitForPageLoad(driver);
	}	
	
	/**
	 * Is change default in the payment method displayed
	 * 
	 * @throws Exception
	 */
	public Boolean isChangeDefaultInPaymentDisplayed() throws Exception {
		BrowserActions.scrollToView(lnkChangeDefaultPayment, driver);
		return lnkChangeDefaultPayment.isDisplayed();
		
	}

	@FindBy(css = ".default-shipping-address .title")
	WebElement lblTitleInShipping;

	@FindBy(css = ".default-shipping-address > div > div:nth-child(2)")
	WebElement lblNameInShipping;

	@FindBy(css = ".default-shipping-address > div > div:nth-child(3)")
	WebElement lblAddressInShipping;

	@FindBy(css = ".default-shipping-address > div>span:nth-child(5)")
	WebElement lblStateAndZipCodeInShipping;

	@FindBy(css = ".default-shipping-address > div>span:nth-child(6)")
	WebElement lblPhoneNumberInShipping;

	public LinkedHashMap<String, String> getDefaultShippingAddress()
			throws Exception {
		LinkedHashMap<String, String> defaultShippingDetails = new LinkedHashMap<String, String>();
		String titleInShipping = BrowserActions.getText(driver,
				lblTitleInShipping, "Title In shipping");
		String nameInShipping = BrowserActions.getText(driver,
				lblNameInShipping, "Name In shipping");
		String addressInShipping = BrowserActions.getText(driver,
				lblAddressInShipping, "Address In shipping");
		String cityInShipping = BrowserActions.getText(driver,
				lblStateAndZipCodeInShipping, "City In shipping");
		cityInShipping = cityInShipping.split(",")[0];
		String stateInShipping = BrowserActions.getText(driver,
				lblStateAndZipCodeInShipping, "State In shipping");
		String[] stateInShippingArray = stateInShipping.split(" ");
		stateInShipping = stateInShippingArray[stateInShippingArray.length - 2];
		String zipCodeInShipping = BrowserActions.getText(driver,
				lblStateAndZipCodeInShipping, "Zipcode In shipping");
		String[] zipCodeInShippingArray = zipCodeInShipping.split(" ");
		zipCodeInShipping = zipCodeInShippingArray[zipCodeInShippingArray.length - 1];
		String phoneNumberInShipping = BrowserActions.getText(driver,
				lblPhoneNumberInShipping, "PhoneNumber In shipping");

		defaultShippingDetails.put("title", titleInShipping);
		defaultShippingDetails.put("name", nameInShipping);
		defaultShippingDetails.put("address", addressInShipping);
		defaultShippingDetails.put("city", cityInShipping);
		defaultShippingDetails.put("state", stateInShipping);
		defaultShippingDetails.put("zipcode", zipCodeInShipping);
		defaultShippingDetails.put("phonenumber", phoneNumberInShipping);

		return defaultShippingDetails;
	}

	@FindBy(css = ".default-billing-address .title")
	WebElement lblTitleInBilling;

	@FindBy(css = ".default-billing-address > div > div:nth-child(2)")
	WebElement lblNameInBilling;

	@FindBy(css = ".default-billing-address > div > div:nth-child(3)")
	WebElement lblAddressInBilling;
	
	@FindBy(css = ".default-billing-address > div > div:nth-child(4)")
	WebElement lblAddress2InBilling;

	@FindBy(css = ".default-billing-address > div>span:nth-child(5)")
	WebElement lblStateAndZipCodeInBilling;

	@FindBy(css = ".default-billing-address > div>span:nth-child(6)")
	WebElement lblPhoneNumberInBilling;

	public LinkedHashMap<String, String> getDefaultBillingAddress()
			throws Exception {
		LinkedHashMap<String, String> defaultBillingDetails = new LinkedHashMap<String, String>();
		String titleInBilling = BrowserActions.getText(driver,
				lblTitleInBilling, "Title In Billing");
		String nameInBilling = BrowserActions.getText(driver, lblNameInBilling,
				"Name In Billing");
		String addressInBilling = BrowserActions.getText(driver,
				lblAddressInBilling, "Address In Billing");
		String cityInBilling = BrowserActions.getText(driver,
				lblStateAndZipCodeInBilling, "City In Billing");
		cityInBilling = cityInBilling.split(",")[0];
		String stateInBilling = BrowserActions.getText(driver,
				lblStateAndZipCodeInBilling, "State In Billing");
		String[] stateInBillingArray = stateInBilling.split(" ");
		stateInBilling = stateInBillingArray[stateInBillingArray.length - 2];
		String zipCodeInBilling = BrowserActions.getText(driver,
				lblStateAndZipCodeInBilling, "Zipcode In Billing");
		String[] zipCodeInBillingArray = zipCodeInBilling.split(" ");
		zipCodeInBilling = zipCodeInBillingArray[zipCodeInBillingArray.length - 1];
		String phoneNumberInBilling = BrowserActions.getText(driver,
				lblPhoneNumberInBilling, "PhoneNumber In Billing");
		if(Utils.waitForElement(driver, lblPhoneNumberInBilling)){
			String address2InBilling = BrowserActions.getText(driver,
					lblAddress2InBilling, "Address 2 In Billing");
			defaultBillingDetails.put("address2", address2InBilling);
		}

		defaultBillingDetails.put("title", titleInBilling);
		defaultBillingDetails.put("firstname", nameInBilling.split(" ")[0]);
		defaultBillingDetails.put("lastname", nameInBilling.split(" ")[1]);
		defaultBillingDetails.put("address1", addressInBilling);
		defaultBillingDetails.put("city", cityInBilling);
		defaultBillingDetails.put("state", StateUtils.getStateName(stateInBilling));
		defaultBillingDetails.put("zipcode", zipCodeInBilling);
		defaultBillingDetails.put("phonenumber", phoneNumberInBilling);

		return defaultBillingDetails;
	}

	/**
	 * To click on the set default address
	 * 
	 * @throws Exception
	 */
	public void clickOnSetDefaultAddress() throws Exception {
		BrowserActions.scrollToViewElement(btnSetDefaultAddress, driver);
		BrowserActions.clickOnElement(btnSetDefaultAddress, driver,
				"Set Default Address");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on the set default billing
	 * 
	 * @throws Exception
	 */
	public void clickOnSetDefaultBilling() throws Exception {
		BrowserActions.scrollToViewElement(btnSetDefaultBilling, driver);
		BrowserActions.clickOnElement(btnSetDefaultBilling, driver,
				"Set Default billing");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (Utils.getRunPlatForm().equals("desktop")) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	// gets 'Invalid Email Address' error message
	public String getInvalidEmailAddressErrorMessage() throws Exception {
		return BrowserActions.getText(driver, msgInvalidEmailAddress,
				"gets 'Invalid Email Address' error message");
	}

	// gets chkAddToEmailList check box is checked
	public String isAddToEmailListChecked() throws Exception {
		return BrowserActions.getRadioOrCheckboxChecked(chkAddToEmailList);
	}

	// Enter Text on First name field
	public void typeOnFirstNameField(String txtToType) throws Exception {
		BrowserActions.typeOnTextField(txtFirstName, txtToType, driver,
				"First name field");
	}

	// Enter Text on Last name field
	public void typeOnLastNameField(String txtToType) throws Exception {
		BrowserActions.typeOnTextField(txtLastName, txtToType, driver,
				"Last name field");
	}

	// Enter Text on Phone field
	public void typeOnPhoneField(String txtToType) throws Exception {
		BrowserActions.typeOnTextField(txtPhoneNumber, txtToType, driver,
				"Phone number field");
	}

	// gets and return Text on First name field
	public String getFirstNameFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtFirstName,
				"value", "First name field");
	}

	// gets and return Text on Last name field
	public String getLastNameFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtLastName,
				"value", "Last name field");
	}

	// gets and return Text on Phone field
	public String getPhoneFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtPhoneNumber,
				"value", "Phone number field");
	}

	// gets and return Text on email field
	public String getEmailFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtEmail, "value",
				"Email text field");
	}
	
	// gets and return Text on new password field
	public String getNewPasswordFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtNewPassword, "value",
				"New password text field");
	}

	// gets and return Text on new password field
	public String getConfirmPasswordFieldValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtConfirmPassword, "value",
				"Cofirm password text field");
	}
	
	/**
	 * To get default payment details
	 * 
	 * @return LinkedHashMap with all payment details
	 * @throws InterruptedException
	 */
	public LinkedHashMap<String, String> getDefaultPaymentOption()
			throws Exception {
		LinkedHashMap<String, String> defaultPaymentDetails = new LinkedHashMap<String, String>();
		String cardType = BrowserActions.getText(driver, lblCardType,
				"Card Type In Payment option");
		String cardOwner = BrowserActions.getText(driver, lblCardOwner,
				"Card Owner In Payment option");
		String cardNumber = BrowserActions.getText(driver,
				lblCardNumber, "Card Number In Payment Option");
		String CardExpire = BrowserActions.getText(driver,
				lblCardExpire, "Card Expire date In Payment option");

		defaultPaymentDetails.put("cardtype", cardType);
		defaultPaymentDetails.put("cardowner", cardOwner);
		defaultPaymentDetails.put("cardnumber", cardNumber);
		defaultPaymentDetails.put("cardexpire", CardExpire);

		return defaultPaymentDetails;
	}

	/**
	 * Click on the set as default button
	 * 
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultBilling() throws Exception {
		BrowserActions.scrollToView(btnSetDefaultBilling, driver);
		BrowserActions.clickOnElement(btnSetDefaultBilling, driver,
				"Set as default billing button");
	}

	// select available addresses from Saved Address dropdown
	// by passing index
	public void selectSavedAddressByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(drpSavedAddressInSetDefaultBilling,
				driver, "Saved Address Dropdown.");
		BrowserActions.clickOnElement(
				lstSavedAddressInSetDefaultBillingSortByOptions.get(index - 1),
				driver, "SortBy dropdown list");
		Utils.waitForPageLoad(driver);
	}

	// get text of the selected address from Saved Address dropdown
	public String getselectedAddressFromSavedAddressDropdown() throws Exception {
		return BrowserActions.getText(driver,
				drpSavedAddressSelectedOptionInSetDefaultBilling,
				"Selected address In SetDefaultBillingAddressDialogContainer");
	}

	// get text of the selected State from State dropdown
	public String getselectedStateFromStateDropdown() throws Exception {
		return BrowserActions.getText(driver,
				drpStateSelectedOptionInSetDefaultBilling,
				"Selected address In SetDefaultBillingAddressDialogContainer");
	}

	public LinkedHashMap<String, String> getSetDefaultBillingAddressDialogContainer()
			throws Exception {
		LinkedHashMap<String, String> setDefaultBillingDetails = new LinkedHashMap<String, String>();

		String savedAddressInBilling = getselectedAddressFromSavedAddressDropdown();

		String titleInBilling = BrowserActions.getText(driver,
				lblTitleInSetDefaultBilling,
				"Title In SetDefaultBillingAddressDialogContainer");

		String addressNameInBilling = BrowserActions.getTextFromAttribute(
				driver, lblAddressNameInSetDefaultBilling, "value",
				"Address name In SetDefaultBillingAddressDialogContainer");

		String firstNameInBilling = BrowserActions.getTextFromAttribute(driver,
				lblFirstNameInSetDefaultBilling, "value",
				"First name In SetDefaultBillingAddressDialogContainer");

		String lastNameInBilling = BrowserActions.getTextFromAttribute(driver,
				lblLastNameInSetDefaultBilling, "value",
				"Last name In SetDefaultBillingAddressDialogContainer");

		String addressOneInBilling = BrowserActions.getTextFromAttribute(
				driver, lblAddressOneInSetDefaultBilling, "value",
				"Address1 In SetDefaultBillingAddressDialogContainer");

		String addressTwoInBilling = BrowserActions.getTextFromAttribute(
				driver, lblAddressTwoInSetDefaultBilling, "value",
				"Address2 In SetDefaultBillingAddressDialogContainer");

		String cityInBilling = BrowserActions.getTextFromAttribute(driver,
				lblCityNameInSetDefaultBilling, "value",
				"City In SetDefaultBillingAddressDialogContainer");

		String selectedStateInBilling = getselectedStateFromStateDropdown();

		String zipcodeInBilling = BrowserActions.getTextFromAttribute(driver,
				lblZipcodeInSetDefaultBilling, "value",
				"Zipcode In SetDefaultBillingAddressDialogContainer");

		String phoneNumberInBilling = BrowserActions.getTextFromAttribute(
				driver, lblPhoneInSetDefaultBilling, "value",
				"PhoneNumber In SetDefaultBillingAddressDialogContainer");

		setDefaultBillingDetails.put("savedAddress", savedAddressInBilling);
		setDefaultBillingDetails.put("title", titleInBilling);
		setDefaultBillingDetails.put("addressName", addressNameInBilling);
		setDefaultBillingDetails.put("firstName", firstNameInBilling);
		setDefaultBillingDetails.put("lastName", lastNameInBilling);
		setDefaultBillingDetails.put("address1", addressOneInBilling);
		setDefaultBillingDetails.put("address2", addressTwoInBilling);
		setDefaultBillingDetails.put("city", cityInBilling);
		setDefaultBillingDetails.put("state", selectedStateInBilling);
		setDefaultBillingDetails.put("zipcode", zipcodeInBilling);
		setDefaultBillingDetails.put("phonenumber", phoneNumberInBilling);

		return setDefaultBillingDetails;
	}

	/**
	 * Click on the set default in the set default billing address dialog
	 * 
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultInSetDefaultBillingDialog() throws Exception {
		BrowserActions.clickOnElement(btnSetAsDefaultInSetDefaultBilling,
				driver, "Set As Default Button In SetDefaultBillingDialog");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To check I am a Belk Employee Check Box
	 * @param checkType - 'check'/'uncheck'
	 * @throws Exception
	 */
	public void checkIAmBelkEmployee(String... checkType) throws Exception {
		BrowserActions.scrollToViewElement(chkIAmBelkEmployee, driver);
		if(checkType.length>0&& checkType[0].equalsIgnoreCase("uncheck")) {
			BrowserActions.selectRadioOrCheckbox(chkIAmBelkEmployee, "NO");
		} else {
			BrowserActions.selectRadioOrCheckbox(chkIAmBelkEmployee, "YES");
		}
	}

	/**
	 * To get Text from I'm Belk Employee check box *
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getImBelkEmployeeMessage() throws Exception {

		return BrowserActions.getText(driver,chkIAmBelkEmployeeMessage, "Message under I'm Belk Employee");

	}

	public boolean isExpressCheckoutCheckboxEnabled() throws Exception {
		boolean status = false;
		try {
			String disabled = chkExpressCheckout.getAttribute("disabled");
			if (!disabled.equals("true")) {
				status = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			status = false;
		}
		return status;
	}
	public boolean isExpressCheckoutCheckboxChecked() throws Exception {
		boolean status = false;
		try {
			String checked = chkExpressCheckout.getAttribute("checked");
			if (checked.equals("true")) {
				status = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			status = false;
		}
		return status;
	}

	// Enter Text on Employee Id field
	public void typeOnEmployeeIdField(String txtToType) throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.typeOnTextField(txtEmployeeID, txtToType, driver,
				"Employee id field");
	}

	// Enter Text on Store Id field
	public void typeOnStoreIdField(String txtToType) throws Exception {
		BrowserActions.typeOnTextField(txtStoreID, txtToType, driver,
				"Store id field");
		Utils.waitForPageLoad(driver);
	}

	public String getEmployeeId() throws Exception {
		Utils.waitForPageLoad(driver);
		String employeeId = txtEmployeeID.getAttribute("value");
		return employeeId;
	}

	public String getStoreId() throws Exception {
		Utils.waitForPageLoad(driver);
		String storeId = txtStoreID.getAttribute("value");
		return storeId;
	}

	// Click on 'Belk employee' button
	public void clickOnIamaBelkemployee() throws Exception {
		BrowserActions.clickOnElement(ChkboxBelkEmployee, driver,
				"Clicked on Profile Apply changes");
	}

	public String getExpressCheckOutDescription() throws Exception {
		String description = lblExpressCheckoutDescription.getText();
		return description;
	}

	/**
	 * To get Invalid Employee id error message *
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getInvalidEmployeeIdMessage() throws Exception {

		return BrowserActions.getText(driver,msgInvalidEmployeeID, "Invalid Employee Id message on entering invalid number");

	}

	/**
	 * To get Invalid Store id error message *
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getInvalidStoreIdMessage() throws Exception {

		return BrowserActions.getText(driver,msgInvalidStoreID, "Invalid Store Id message on entering invalid number");

	}

	/**
	 * Check I am a Belk Employee Check Box
	 * @param actionType - 'Check' or 'Uncheck' 
	 * @throws Exception
	 */
	public void checkExpressCheckout(String actionType) throws Exception {
		BrowserActions.scrollToViewElement(chkExpressCheckout, driver);
		if (actionType.equalsIgnoreCase("Check"))
			BrowserActions.selectRadioOrCheckbox(chkExpressCheckout, "YES");
		else if (actionType.equalsIgnoreCase("Uncheck"))
			BrowserActions.selectRadioOrCheckbox(chkExpressCheckout, "NO");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Click on cancel Default Billing / Shipping Address button
	 * 
	 * @throws Exception
	 */
	public void clickOnCancelSetDefaultShippingBillingAddress()
			throws Exception {
		BrowserActions.scrollToViewElement(btnCancelInSetDefaultBilling, driver);
		BrowserActions.clickOnElement(btnCancelInSetDefaultBilling, driver,
				"Cancel button");
	}

	/**
	 * Click on cancel Default Payment button
	 * 
	 * @throws Exception
	 */
	public void clickOnCancelSetDefaultPayment() throws Exception {
		BrowserActions.scrollToViewElement(btnCancelInSetDefaultPayment, driver);
		BrowserActions.clickOnElement(btnCancelInSetDefaultPayment, driver,
				"Cancel button");
	}

	/**
	 * /** Click on the set default in the set default Payment dialog
	 * 
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultInSetDefaultPaymentDialog() throws Exception {
		BrowserActions.clickOnElement(btnSetAsDefaultInSetDefaultPayment,
				driver, "Set As Default Button In SetDefaultPaymentDialog");
		Utils.waitForPageLoad(driver);

	}

	/**
	 * Click on the set as default Payment button
	 * 
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultPayment() throws Exception {
		BrowserActions.scrollToView(btnSetAsDefaultPayment, driver);
		BrowserActions.clickOnElement(btnSetAsDefaultPayment, driver,
				"Set as default billing button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * get the Form Content
	 * 
	 * @throws Exception
	 */
	public String getFormHelpContent() throws Exception {
		Utils.waitForPageLoad(driver);
		String[] lines = BrowserActions.getText(driver,
				lblAddressFormDialogContainer, "Form Dailog").split("\n");
		return lines[1];
	}

	public List<String> getsetDefaultAddressFormErrorMsg() throws Exception {
		List<String> errorMsgs = new ArrayList<String>();
		for (int i = 0; i < formError.size(); i++) {
			errorMsgs.add(BrowserActions.getText(driver, formError.get(i),
					"Set Default Address Form error"));
		}
		return errorMsgs;
	}

	public List<String> getsetDefaultPaymentFormErrorMsg() throws Exception {
		List<String> errorMsgs = new ArrayList<String>();
		for (int i = 1; i < editPaymentFormError.size(); i++) {
			errorMsgs.add(BrowserActions.getText(driver,
					editPaymentFormError.get(i),
					"Set Default Payment Form error"));
		}
		return errorMsgs;
	}

	/**
	 * Clicking on Original Address Radio Button in Address Validation Popup
	 * 
	 * @throws Exception
	 */
	public void selectUseOriginalAddress() throws Exception {
		BrowserActions.selectRadioOrCheckbox(rbUseOriginalAddress, "YES");
	}

	/**
	 * Clicking on Save Changes button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickContinueAddressSuggestion() throws Exception {
		BrowserActions.clickOnElement(btnContinueAddressSuggestionForNoAddressPopup, driver,
				"Continue in Address Suggestion");
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Clicking on Save Changes button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickContinueAddressSuggestionInAddressValidation() throws Exception {
		BrowserActions.clickOnElement(btnContinueAddressSuggestion, driver,
				"Continue in Address Suggestion");
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * Click on the set as default button
	 * 
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultShipping() throws Exception {
		BrowserActions.scrollToView(btnSetAsDefaultShipping, driver);
		BrowserActions.javascriptClick(btnSetAsDefaultShipping, driver,"Set as default Shipping button");
	}

	/**
	 * To fill the Add New Default Shipping / Billing Address Modal in Profile
	 * page
	 * 
	 * @param useAddressForDefaultShipping
	 *            , useAddressForDefaultBilling
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingSetDefaultAddressDetails(
			LinkedHashMap<String, String> addressDetails) throws Exception {
		final long startTime = StopWatch.startTime();

		LinkedHashMap<String, String> typeAddressDetails = new LinkedHashMap<String, String>();

		if(Utils.waitForElement(driver, txtAddressName)){
			String addressName = addressDetails.get("title");
			typeAddressDetails.put("type_firstname_" + addressName,
					txtAddressNameLocator);
		}
		
		String firstName = addressDetails.get("firstname");
		typeAddressDetails.put("type_firstname_" + firstName,
				txtFirstNameLocator);
		String lastName = addressDetails.get("lastname");
		typeAddressDetails
				.put("type_firstname_" + lastName, txtLastNameLocator);

		String address1 = addressDetails.get("address1");
		String address2 = addressDetails.get("address2");
		String city = addressDetails.get("city");
		String state = addressDetails.get("state");
		String zipcode = addressDetails.get("zipcode");
		String phoneNo = addressDetails.get("phone").replace("-", "");

		typeAddressDetails.put("type_address_" + address1, txtAddress1Locator);

		if (address2 != "")
			typeAddressDetails.put("type_address_" + address2,
					txtAddress2Locator);

		typeAddressDetails.put("type_city_" + city, txtCityLocator);
		typeAddressDetails
				.put("select_state_" + state, btnSelectedStateLocator);
		typeAddressDetails.put("type_zipcode_" + zipcode, txtZipcodeLocator);
		typeAddressDetails.put("type_phoneno_" + phoneNo, txtPhoneLocator);

		AddressBookUtils.enterAddressDetails(typeAddressDetails, driver);
		Log.event("Filled Billing Details as a Guest user",
				StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return typeAddressDetails;

	}

	/**
	 * To fill the Card Details
	 * 
	 * @param MakeDefaultPayment
	 * @param cardDetails
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingCardDetails(String cardDetails)
			throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetail = new LinkedHashMap<String, String>();
		String cutomercardDetails = checkoutProperty.getProperty(cardDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];
		cardDetail.put("select_cardtype_" + cardType, btnCardType);
		cardDetail.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetail.put("type_cardno_" + cardNo, txtCardNo);
		String cardExpireMonth = cutomercardDetails.split("\\|")[3];
		String cardExpireYear = cutomercardDetails.split("\\|")[4];
		cardDetail.put("select_expmonth_" + cardExpireMonth, btnCardExpMonth);
		cardDetail.put("select_expyr_" + cardExpireYear, btnCardExpYear);

		PaymentUtils.enterCardDetails(cardDetail, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return cardDetail;
	}

	// get title of default payment dialog
	public String getTitleOfSetDefaultPayment() throws Exception {
		return BrowserActions.getText(driver, lblTitleInSetDefaultPayment,
				"Title In SetDefaultPaymentDialog");
	}

	// Click on 'Make Default Payment' button
	public void clickOnMakeDefaultPayment() throws Exception {
		BrowserActions.clickOnElement(chkbxMakePaymentMethod, driver,
				"Clicked on Make Default Payment");
	}


	/**
	 * 
	 */
	public LinkedHashMap<String, String> fillingCardDetails(String customerDetails,
			String... iteration) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
		String cardExpireMonth = "";
		String cardExpireYear = "";

		String cutomercardDetails = checkoutProperty
				.getProperty(customerDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardExpireMonth = cutomercardDetails.split("\\|")[3];
			cardExpireYear = cutomercardDetails.split("\\|")[4];
		}

		if (iteration.length > 0) {
			cardDetails.put("select1_cardtype_" + cardType, btnCardType);
		} else {
			cardDetails.put("select_cardtype_" + cardType, btnCardType);
		}

		cardDetails.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetails.put("type_cardno_" + cardNo, txtCardNo);

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardDetails.put("select_expmonth_" + cardExpireMonth,
					btnCardExpMonth);
			cardDetails.put("select_expyr_" + cardExpireYear, btnCardExpYear);
		} 

		enterCardDetails(cardDetails, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return cardDetails;
	}

	@SuppressWarnings("rawtypes")
	final public static void enterCardDetails(LinkedHashMap <String, String> cardDetails, WebDriver driver) throws Exception {

		//final long startTime = StopWatch.startTime();
		try {
			Set cardDetailsSet = cardDetails.entrySet();
			Iterator cardDetailsIterator = cardDetailsSet.iterator();

			while (cardDetailsIterator.hasNext()) {

				Map.Entry mapEntry = (Map.Entry) cardDetailsIterator.next();
				String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
				String locator = mapEntry.getValue().toString();

				switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {

				case "type":
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "click":
					BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "select": {

					WebElement element = driver.findElement(By.cssSelector(locator));
					BrowserActions.javascriptClick(element, driver, "Drop down");				
					List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
					for(WebElement e: lstElement) {						
						if(e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {						
							BrowserActions.scrollToViewElement(e, driver);
							BrowserActions.javascriptClick(e, driver, "list elements");
							break;
						}
					}
					break;
				}
				case "select1": {

					WebElement element = driver.findElement(By.cssSelector(locator));
					BrowserActions.javascriptClick(element, driver, "Drop down");				
					List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
					for(WebElement e: lstElement) {	

						if(e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {						
							BrowserActions.scrollToViewElement(e, driver);
							BrowserActions.javascriptClick(e, driver, "list elements");
							break;
						}
					}
					break;
				}
				case "check": {
					BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)), keyWithElementTypeAndDescriptionAndTextToType[2]);
					break;
				}
				default:
					break;

				}// Switch

				Utils.waitForPageLoad(driver);

			}// While
		} catch (Exception e) {
			// TODO: handle exception
		}
	}// enterCardDetails
	
	public void clickOnSetDefaultInPayment() throws Exception{
		BrowserActions.clickOnElement(btnSetAsDefaultInDialogContainer, driver, "Set as default button");
		Utils.waitForPageLoad(driver);
	}
	
	public boolean paymentMethodVisiblity(){
		if(Utils.waitForElement(driver, defaultPaymentMethodPane))
			return true;
		return false;
	}
	
    public String getTextFromAccountHeader() throws Exception{
    	return BrowserActions.getText(driver, lblEditHeader, "Edit Page Header");
    }
    
    public boolean verifyDefaultPaneAvailabilityByName(String section){
    	if(section.equals("shipping"))
    		return Utils.waitForElement(driver, defaultShippingAddressPane);
    	else if(section.equals("billing"))
    		return Utils.waitForElement(driver, defaultBillingAddressPane);
    	else if(section.equals("payment"))
    		return Utils.waitForElement(driver, defaultPaymentMethodPane);
    	else
    		return false;
    }
    /**
     * To verify Unsubscribe Confirmation Success Message
     * 
     * @return boolean 
     * @throws Exception 
     */
   public boolean verifyUnsubscribeConfirmationSuccessMsg() throws Exception {
	   return BrowserActions.getText(driver, msgUnsubscribeConfirmationSuccess, "Unsubscribe Confirmation Success Message").equals(MSG_UNSUBSCRIBE_CONFIRMATION);
   }
   
   
   /**
	 * To fill the Add New Address Modal in My Accout page
	 * 
	 * @param useAddressForDefaultShipping
	 *            , useAddressForDefaultBilling
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingAddNewAddressDetail(
			String addressDetails,
			String makeDefaultAddressForShipping,
			String makeDefaultAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();

		LinkedHashMap<String, String> typeAddressDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(addressDetails);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if(addressDetails.contains("_1")){
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}
		String addressName = "Address Name";
		typeAddressDetails.put("type_addressname_" + addressName,
				txtAddressNameLocator);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		typeAddressDetails.put("type_firstname_qaFirst" + randomFirstName,
				txtFirstNameLocator);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		typeAddressDetails.put("type_lastname_qaLast" + randomLastName,
				txtLastNameLocator);
		typeAddressDetails.put("type_address_" + address1, txtAddress1Locator);
		if(addressDetails.contains("_1"))
			typeAddressDetails.put("type_address_" + address2, txtAddress2Locator);
		typeAddressDetails.put("type_city_" + city, txtCityLocator);
		typeAddressDetails.put("select_state_" + state, btnSelectedStateLocator);
		typeAddressDetails.put("type_zipcode_" + zipcode, txtZipcodeLocator);
		typeAddressDetails.put("type_phoneno_" + phoneNo, txtPhoneLocator);

		if (makeDefaultAddressForShipping.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Shipping Address_YES",
					chkMakeDefaultShippingAddrLocator);
		}

		if (makeDefaultAddressForBilling.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Billing Address_YES",
					chkMakeDefaultBillingAddrLocator);
		}

		AddressBookUtils.enterAddressDetails(typeAddressDetails, driver);
		Log.event("Filled Billing Details as a Guest user",
				StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return typeAddressDetails;

	}
	
	public int getBorderCount(){
	 return lstBorderLine.size();
	}
	
	public List<String> getTextFromPasswordError() throws Exception{
		List<String> errorMessage = new ArrayList<String>();
		for(int i=0;i<lstPasswordError.size();i++)
			errorMessage.add(BrowserActions.getText(driver, lstPasswordError.get(i), "password error message"));
		return errorMessage;
	}
}