package com.belk.pages.account;

import java.util.LinkedHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.CreateAccountPage;
import com.belk.pages.ElementLayer;
import com.belk.pages.HomePage;
import com.belk.pages.MiniCartPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.footers.Footers;
import com.belk.pages.footers.socialmediapages.FacebookPage;
import com.belk.pages.footers.socialmediapages.GooglePlusPage;
import com.belk.pages.footers.socialmediapages.PintrestPage;
import com.belk.pages.footers.socialmediapages.TwitterPage;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;
import com.btr.proxy.util.PlatformUtil.Browser;

public class WishListPage extends LoadableComponent<WishListPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of Wish List Page ***********************************
	 **********************************************************************************************/
	public static final String CREATENEWTEXT= "Please create an account before creating your wish list.";
	private static final String SELECTED_QTY = ".quantity div[class*='selected-option']";
	private static final String WISHLIST_POPUP_SELECTED_QTY = ".product-add-to-cart .quantity div[class*='selected-option']";
	
	public static final String WISHLISTTABLESTART="table tr:nth-child(";
	public static final String WISHLISTTABLEEND=") .item-edit-details >a";
	private static final String WISHLISTPRIORITYTXT=") .option-priority .value";

	
	@FindBy(css = ".pt_wish-list")
	WebElement divWishList;

	@FindBy(css = ".breadcrumb-element")
	List<WebElement> lstBreadcrumb;
	
	@FindBy(css=".breadcrum-device")
	WebElement breadcrumbMobile;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement txtWishListHeading;

	@FindBy(css = ".accountheader")
	WebElement txtWishlistMobileHeading;

	@FindBy(css = "button[class='button-text delete-item simple']")
	List<WebElement> btnRemoveItem;

	@FindBy(css = ".create-new-wishlist>p']")
	WebElement msgNoItemsInWishList;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	/****************
	 * Left Nav Section - Links
	 *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	
	@FindBy(css = ".noresults")
	WebElement noWishListResult;

    @FindBy(css = "div[class*='login-wishlist'] .login-box-content>p")
	WebElement txtWishListDescription;
    
    @FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;
	
	/*********************************************************************/

	// *******mobile elements***************

	/*
	 * @FindBy(css = "h3 > a[title='']") WebElement lnkMyAccountMobile;
	 */

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	// Print icon
	@FindBy(css = ".print-page-cont>a")
	WebElement lnkPrinter;

	/*********************************************************************/
	// Find a Wish List
	/*********************************************************************/
	@FindBy(id = "dwfrm_wishlist_search_firstname")
	WebElement txtWishListSearchFirstName;

	@FindBy(id = "dwfrm_wishlist_search_lastname")
	WebElement txtWishListSearchLastName;

	@FindBy(id = "dwfrm_wishlist_search_email")
	WebElement txtWishListSearchEmail;

	@FindBy(css = "button[name='dwfrm_wishlist_search_search']")
	WebElement btnFindWishList;

	// Wish List Result
	@FindBy(css = ".wishlist-list-table")
	WebElement wishListTable;

	@FindBy(css = ".wishlist-list-table .view>a")
	WebElement lnkViewOnWishListResult;

	// Set List Public or Private && Share Icons

	@FindBy(css = "button[name='dwfrm_wishlist_setListPublic']")
	WebElement btnMakeThisListPublic;

	@FindBy(css = "button[name='dwfrm_wishlist_setListPrivate']")
	WebElement btnMakeThisListPrivate;

	@FindBy(css = ".share-options a.share-facebook")
	WebElement lnkShareFacebook;

	@FindBy(css = ".share-options a.share-twitter")
	WebElement lnkShareTwitter;

	@FindBy(css = ".share-options a.share-googleplus")
	WebElement lnkShareGooglePlus;

	@FindBy(css = ".share-options a.share-pinternet")
	WebElement lnkSharePinterNet;

	@FindBy(css = ".share-options a.share-email")
	WebElement lnkShareEmail;

	@FindBy(css = ".share-options a.share-link")
	WebElement lnkShareLink;

	@FindBy(css = ".share-link-content.active")
	WebElement shareLinkContent;

	@FindBy(css = ".share-link-content.active>a")
	WebElement lnkShareLinkContent;

	// Add items to Wish List
	@FindBy(css = ".button.additemstoregistry")
	WebElement btnAddItemsToWishList;

	// Add Gift Cards
	@FindBy(css = "button[value='Add Gift Cards']")
	WebElement btnAddGiftCards;

	@FindBy(css = ".button-fancy-small.wishlist-addtobag.add-to-cart")
	WebElement btnAddToBag;

	@FindBy(css = "a[class='print-page']")
	WebElement lnkPrintOption;

	@FindBy(css = ".secondary-navigation div[class='content-asset']>p")
	WebElement txtAccountNavUnregpanel;

	@FindBy(css = ".account-nav-asset div[class='content-asset']>p>span")
	WebElement txtAccounthelpcontent;

	@FindBy(css = ".login-box.login-create-account.clearfix div[class='content-asset'] ")
	WebElement WishlistcontentforGuestCustomer;

	@FindBy(css = ".wishlist-list-cont>table")
	WebElement wishListResultContent;

	@FindBy(css = ".item-dashboard")
	WebElement fldItemAdded;

	@FindBy(css = "div[class='need-data']")
	WebElement HelpContentSlot;

	
	@FindBy(css = ".error-form")
	WebElement errorLogIn;
	@FindBy(css = "td.first-name")
	WebElement wishListFirstName;
	
	@FindBy(css = ".item-list-device .first-name")
	WebElement wishListFirstNameMobile;
	
	@FindBy(css = ".item-list-device-row div[class='last-name']")
	WebElement wishListLastNameMobile;
	
	@FindBy(css = "td.last-name")
	WebElement wishListLastName;
	
	
	@FindBy(css = ".wishlist-list-table .view>a")
	WebElement lnkViewOnWishListResultdesktop;
	
	@FindBy(css = ".wishlist-registry-items")
	List<WebElement> productList;
	
	@FindBy(css = ".product-list-item .name a")
	List<WebElement> lstProductNames;
	
	@FindBy(css = "div[class='sku']")
	List<WebElement> lstUPC;
	
	@FindBy(css = ".list-share button[class*='secondarybutton']")
	WebElement btnMakeThisList;
	
	@FindBy(css =".product-list-item>div[data-attribute='color']>span[class='value']")
	List<WebElement> lstColor;
	
	@FindBy(css=".product-list-item>div[data-attribute='size']>span[class='value']")
	List<WebElement> lstSize;
	
	@FindBy(css="div[class='price']")
	List<WebElement> lblPriceSectionInProduct;
	
	@FindBy(css = "div[class='price'] div.standard-price")
	List<WebElement> lblStandardPrice;

	@FindBy(css = "div[class='item-list-device hide-desktop'] .view>a")
	WebElement lnkViewOnWishListResultMobile;

	@FindBy(css = "div.login-box.login-wishlist-track > h2")
	WebElement txtFindWishList;
	

	@FindBy(css = ".form-row.form-row-button>button[name='dwfrm_login_register']")
	WebElement btnCreateNewAccount;

	
	
	@FindBy(css = ".wishlist-list-cont table tr")
	List<WebElement> wishlistSearchResults;

	@FindBy(css = ".noresults>p")
	WebElement lblNoResultsFound;
	
	@FindBy(css = ".createbenefits .content-asset>p")
	WebElement lblCreateNewAccount;


	@FindBy(css = "div[class='price'] div.now-price")
	List<WebElement> lblNowPrice;
	
	


	@FindBy(css = ".loader[style*='block']")
	WebElement wishListpinner;
	
	@FindBy(css="button#add-to-cart")
	WebElement btnUpdateWishList;
	
	@FindBy(css = ".product-col-2.product-detail>div>form>fieldset>div>a")
	WebElement btnCancelInPopUp;
	
	@FindBy(css = ".item-edit-details>a")
	WebElement editLink;
	
	//:nth-child(1)
	@FindBy(css = ".productcontent-wrapper>div")
	WebElement PopUp;
	
	@FindBy(xpath = "//*[@id='dwfrm_wishlist_items_i0_quantity']//following-sibling::div")
	WebElement selectquantity;
	
	@FindBy(xpath = "//*[@id='dwfrm_wishlist_items_i0_priority']//following-sibling::div")
	WebElement selectpriority;
	
	@FindBy(css="#add-to-cart")
	WebElement lnkUpdate;

    @FindBy(css=".wishlist-search")
	WebElement fldWishListSearchDescription;	
	
	@FindBy(css=".header-banner-promotion-bottom")
	WebElement fldGlobalContentSlot; 	

	@FindBy(css = " .secondary-navigation .content-asset ul li a[title='Create an Account']")
	WebElement lnkCreateAccount;
	
	@FindBy(css = " .secondary-navigation .content-asset ul li a[title='View Privacy Policy']")
	WebElement lnkPrivacypolicy;
	
	
	@FindBy(css = " .secondary-navigation .content-asset ul li a[title='Secure Shopping']")
	WebElement lnkSecureShopping;

	@FindBy(css = "div[class='login-box-content returning-customers clearfix']  p")
	WebElement txtReturningCustomers;
	
	@FindBy(css = ".item-option .item-qty-text .custom-select")
	WebElement SelectedQuantity;
	
	@FindBy(id = "QuickViewDialog")
	WebElement modalWishListEditDetails;
	
	
	@FindBy(css = "#primary > div > table > tbody > tr > td.item-dashboard > form > fieldset:nth-child(2) > div > div > div > div > div > div")
	WebElement drpQty;
	
	@FindBy(css="div[class='wishlist-list-cont']>div[class='create-new-wishlist']>p")
	WebElement msgWishlist; 
	
	@FindBy(css = ".wishlist-list-cont")
    WebElement wishListHtmlContent;
	
	@FindBy(css="button[title='Add to Shopping Bag']")
	WebElement btnShoppingBag;
	
		
	/**********************************************************************************************
	 ********************************* WebElements of Wish List Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public WishListPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// WishListPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divWishList))) {
			Log.fail("Wish List page did not open up.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(2);
		Utils.waitUntilElementDisappear(driver, wishListpinner);
		BrowserActions.nap(2);
	}
	
	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver,
							"Profile Link");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
							driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver,
							"Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile,
							driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver,
							"Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver,
							"Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver,
							"Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
							driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver,
							"Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver,
							"Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver,
							"WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver,
							"WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver,
						"Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver,
						"Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}

	/**
	 * To get the last value of the breadcrumb
	 * 
	 * @return String - Last value in the breadcrumb
	 * @throws Exception
	 */
	public String getBreadCrumbLastValue() throws Exception {
		return (BrowserActions
				.getText(driver, lstBreadcrumb.get(lstBreadcrumb.size() - 1),
						"breadcrumb value"));
	}

	/**
	 * To click Remove Item In whishList
	 * 
	 * @throws Exception
	 */
	public void clickDeleteItem() throws Exception {
		BrowserActions.clickOnElement(btnRemoveItem.get(0), driver,
				" Deleting The Item Added to WishList");
	}

	/*
	 * @return txtNOItemsInWishList *
	 * 
	 * @throws Exception
	 */
	public String getTextFromNoItemsInWishList() throws Exception {
		BrowserActions.scrollToViewElement(msgNoItemsInWishList, driver);
		String txtAddedWishList = BrowserActions.getText(driver,
				msgNoItemsInWishList, "added message");
		return txtAddedWishList;
	}

	/**
	 * To mouse hover to mini cart
	 * 
	 * @return
	 * @throws Exception
	 */
	public ShoppingBagPage clickOnMiniCart() throws Exception {
		if (Utils.waitForElement(driver, miniCart))
			if (miniCart.getCssValue("display").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/*****************************************/
	// Find a wish list
	/*****************************************/

	/**
	 * To enter first name in find a wish list
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void enterFindWishListFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtWishListSearchFirstName, firstName,
				driver, "Find a wish list's first name");
	}

	/**
	 * To enter last name in find a wish list
	 * 
	 * @param lastName
	 * @throws Exception
	 */
	public void enterFindWishListLastName(String lastName) throws Exception {
		BrowserActions.typeOnTextField(txtWishListSearchLastName, lastName,
				driver, "Find a wish list's last name");
	}

	/**
	 * To enter email in find a wish list
	 * 
	 * @param emailID
	 * @throws Exception
	 */
	public void enterFindWishListEmailId(String emailID) throws Exception {
		BrowserActions.typeOnTextField(txtWishListSearchEmail, emailID, driver,
				"Find a wish list's last name");
	}

	/**
	 * To click the find wish list button
	 * 
	 * @throws Exception
	 */
	public void clickFindWishList() throws Exception {
		BrowserActions.clickOnElement(btnFindWishList, driver,
				"Find Wish List button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click make this list public
	 * 
	 * @throws Exception
	 */
	public void clickMakeThisListPublic() throws Exception {
		BrowserActions.clickOnElement(btnMakeThisListPublic, driver,
				"Make This List Public button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click make this list private
	 * 
	 * @throws Exception
	 */
	public void clickMakeThisListPrivate() throws Exception {
		BrowserActions.clickOnElement(btnMakeThisListPrivate, driver,
				"Make This List Private button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to face book page
	 * 
	 * @return
	 * @throws Exception
	 */
	public FacebookPage navigateToFacebookPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkShareFacebook, driver);
		BrowserActions.clickOnElement(lnkShareFacebook, driver,
				"clicking Social Media Facebook Icon");
		return new FacebookPage(driver);
	}

	/**
	 * To navigate to twitter page
	 * 
	 * @return
	 * @throws Exception
	 */
	public TwitterPage navigateToTwitterPage() throws Exception {
		BrowserActions.scrollToViewElement(lnkShareTwitter, driver);
		BrowserActions.clickOnElement(lnkShareTwitter, driver,
				"clicking Social Media Twitter Icon");
		return new TwitterPage(driver);
	}

	/**
	 * To navigate to google plus page
	 * 
	 * @return
	 * @throws Exception
	 */
	public GooglePlusPage navigateToGooglePlusPage() throws Exception {

		BrowserActions.clickOnElement(lnkShareGooglePlus, driver,
				"clicking Social Media GooglePlus Icon");
		return new GooglePlusPage(driver);
	}

	/**
	 * To navigate to pinterest page
	 * 
	 * @return
	 * @throws Exception
	 */
	public PintrestPage navigateToPinterestPage() throws Exception {

		BrowserActions.clickOnElement(lnkSharePinterNet, driver,
				"clicking Social Media Pinterest Icon");
		return new PintrestPage(driver);
	}

	/**
	 * To click get share link Icon
	 * 
	 * @throws Exception
	 */
	public void ClickGetShareLinkIcon() throws Exception {
		BrowserActions.clickOnElement(lnkShareLink, driver,
				"clicking Social Share link Icon");
	}

	/**
	 * To get the share link URL
	 * 
	 * @return sharable link as string
	 * @throws Exception
	 */
	public String getShareLinkURL() throws Exception {
		return BrowserActions.getText(driver, shareLinkContent,
				"Get share link content msg");
	}

	/**
	 * To click the sharable URL
	 * 
	 * @throws Exception
	 */
	public void ClickGetSharableLink() throws Exception {
		BrowserActions.clickOnElement(lnkShareLinkContent, driver,
				"clicking sharable link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click the Add Items To Wish List button
	 * 
	 * @return HomePage
	 * @throws Exception
	 */
	public HomePage ClickAddItemsToWishList() throws Exception {
		BrowserActions.clickOnElement(btnAddItemsToWishList, driver,
				"Add Items To Wish List button");
		return new HomePage(driver);
	}

	/**
	 * To click the Add Items To Wish List button
	 * 
	 * @return HomePage
	 * @throws Exception
	 */
	public void ClickAddGiftCards() throws Exception {
		BrowserActions.clickOnElement(btnAddGiftCards, driver,
				"Add Gift Cards button");
		Utils.waitForPageLoad(driver);
	}

	

	public String getTextFromWishListResults() throws Exception {
		String datatoBeReturned = null;
		datatoBeReturned = BrowserActions.getText(driver, wishListTable,
				"Wish List Result Content");
		return datatoBeReturned;
	}

	public void clickOnRemoveLink() throws Exception {
		for (WebElement element : btnRemoveItem) {
			BrowserActions.clickOnElement(element, driver, "Remove item");
			Utils.waitForPageLoad(driver);
		}

	}

	
	public String verifyWishListInvalidSignIn() throws Exception {
		
		Utils.waitForPageLoad(driver);
		return errorLogIn.getText();
	}

	public LinkedHashMap<String, String> fillFindWishList(String firstName, String lastName, String emailID) throws Exception{
		
		LinkedHashMap<String, String> findWishListDetails = new LinkedHashMap<String, String>();
		findWishListDetails.put("firstName", firstName);
		findWishListDetails.put("lastName", lastName);
		findWishListDetails.put("emailID", emailID);
		enterFindWishListFirstName(firstName);
		enterFindWishListLastName(lastName);
		enterFindWishListEmailId(emailID);
		clickFindWishList();
		return findWishListDetails;
	}
	
	public boolean verifyNameInFindWishList(LinkedHashMap<String, String> findWishListDetails){
		
		
		String fname = findWishListDetails.get("firstName");
		String lname = findWishListDetails.get("lastName");
		
		if(Utils.getRunPlatForm()=="desktop"){
			BrowserActions.scrollToViewElement(wishListFirstName, driver);
			String fnameWishList = wishListFirstName.getText().trim();
			String lnameWishList = wishListLastName.getText().trim();
			if((fname.equals(fnameWishList))&&(lname.equals(lnameWishList)))
				return true;
			
			
			return false;
		}
		else{
			BrowserActions.scrollToViewElement(wishListFirstNameMobile, driver);
			String fnameWishListMobile = null;
			String lnameWishListMobile = null;
			fnameWishListMobile = wishListFirstNameMobile.getText().split("\\:")[1].trim();
			lnameWishListMobile = wishListLastNameMobile.getText().split("\\:")[1].trim();
			
			if((fname.equals(fnameWishListMobile))&&(lname.equals(lnameWishListMobile)))
				return true;
			
			
			return false;
			
		}
	}
	/**
	 * To clickOnViewLink
	 * @throws Exception
	 */
	public void clickOnViewLink() throws Exception {
		if (Utils.getRunPlatForm().equals("mobile")) {
			BrowserActions.scrollToViewElement(lnkViewOnWishListResultMobile, driver);
			BrowserActions.clickOnElement(lnkViewOnWishListResultMobile, driver, " Clicking wishlist link");
		} else {
			BrowserActions.clickOnElement(lnkViewOnWishListResultdesktop, driver, " Clicking wishlist link");
		}
	}

	/**
	 * To findWishList
	 * @param firstName
	 * @param LastName
	 * @param emailId
	 * @throws Exception
	 */
	public void findWishList(String firstName, String LastName, String emailId) throws Exception{
		BrowserActions.clickOnElement(btnMakeThisListPublic, driver, "public button");
		BrowserActions.typeOnTextField(txtWishListSearchFirstName, firstName, driver, "first Name");
		BrowserActions.typeOnTextField(txtWishListSearchLastName, LastName, driver, "last name");
		BrowserActions.typeOnTextField(txtWishListSearchEmail, emailId, driver, "email id");
		BrowserActions.clickOnElement(btnFindWishList, driver, "find wish list button");
	}
	
	/**
	 * To get product Details
	 * 
	 * @return
	 * @return
	 * @Return product Details
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInWishList() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productSet = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0, j = 0; i < productList.size(); i++) {
			LinkedHashMap<String, String> productDetail = new LinkedHashMap<String, String>();
			BrowserActions.scrollToView(lstProductNames.get(i), driver);
			productDetail.put("ProductName", lstProductNames.get(i).getText());
			productDetail.put("Upc", lstUPC.get(i).getText());
			productDetail.put("Color", lstColor.get(i).getText());
			productDetail.put("Size", lstSize.get(i).getText());

			System.out.print(lblPriceSectionInProduct.get(i).getText());
			if (lblPriceSectionInProduct.get(i).getText().contains("Now")) {
				productDetail.put("Price", lblNowPrice.get(j).getText());
				j++;
			} else {
				productDetail.put("Price", lblStandardPrice.get(i).getAttribute("innerHTML"));
			}
			productSet.add(productDetail);
		}
		return productSet;
	}
	
	/**
	 * To get Find WishList Text
	 * 
	 * @return HomePage
	 * @throws Exception
	 */
	public String gettxtFindWishList() throws Exception {		
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, txtFindWishList, "Find WishList Label"); 
	}
	
	
	/**
	 * To Navigate to WishList Results Page
	 * 
	 * @return HomePage
	 * @throws Exception
	 */	
	public void navigateToWishListResults() throws Exception
	{
		BrowserActions.clickOnElement(lnkViewOnWishListResult, driver, "Click on View link in WishList table");	
	}

	/**
	 * To Search WishList as A Guest using Email ID
	 * @param EmailID
	 * @return HomePage
	 * @throws Exception
	 */	
	public void wishListSearch(String emailid) throws Exception
	{
		enterFindWishListEmailId(emailid);
		clickFindWishList();
	}
	
	/**
	 * To Verify WishList Results are displayed or Not
	 * @return HomePage
	 * @throws Exception
	 */	
	
	public String validateWishListSearch() throws Exception
	{
		int wishListSize=0;
		String results=null;
		if(wishlistSearchResults.size()>0)
		{
			results = String.valueOf(wishListSize);
			Log.message(wishListSize+" Products displayed in the wishlist Search Results", driver);
		}else
		{
			results= BrowserActions.getText(driver, lblNoResultsFound, "No Results Found Text is displayed");
			
			Log.assertThat("We are sorry. No wish list could be found for  . Please try again.".equalsIgnoreCase(results), 
					"No resulst found information message is displayed", "No resulst found information message is displayed");
		}
		return results;
	}
	
	/**
	 * To Get the Create Account Information in WishList page
	 * @return HomePage
	 * @throws Exception
	 */	
	
	public String gettextCreateNewAccount() throws Exception
	{
		String text=BrowserActions.getText(driver, lblCreateNewAccount, "Create New Account information on WishList page");;
		return text;
	}
	
	/**
	 * To Click on Create Account button in WishList page
	 * @return HomePage
	 * @throws Exception
	 */	
	public CreateAccountPage clickOnCreateAccount() throws Exception
	{
		Utils.waitForElement(driver, btnCreateNewAccount);
		BrowserActions.clickOnElement(btnCreateNewAccount, driver, "Click on Create New Account");
		
		return  new CreateAccountPage(driver).get();
	}
	
	
	/**
	 * clickOnProductName
	 * @throws Exception
	 */
	public void clickOnProductName(String ProductName) throws Exception{
		for(int i =0; i < lstProductNames.size(); i++){
			if(lstProductNames.get(i).getText().contains(ProductName)){
				BrowserActions.clickOnElement(lstProductNames.get(0), driver, "Product Name");
				break;
			}
		}
	}
	
	
	public String selectQuantity(String... qty) throws Exception {
		// checking size drop down have one value or more that one
		WebElement qtyDefaultValue = BrowserActions.checkLocator(driver,
				WISHLIST_POPUP_SELECTED_QTY);
		if (qty.length > 0) {
			BrowserActions.clickOnElement(qtyDefaultValue, driver,
					"Clicked Size Button");
			BrowserActions.nap(1);
			try {
				List<WebElement> listOfqty = driver.findElements(By
						.cssSelector("div.product-add-to-cart div[class='quantity'] ul>li"));
				if (qty[0].equalsIgnoreCase("random")) {
					int rand = Utils.getRandom(0, listOfqty.size());
					BrowserActions.clickOnElement(listOfqty.get(rand), driver,
							"Select size option");

				} else if (listOfqty.size() > 0) {
					String optionValue = qty[0].toString();
					WebElement qtyoption = BrowserActions
							.getMachingTextElementFromList(listOfqty,
									optionValue, "equals");
					BrowserActions.clickOnElement(qtyoption, driver,
							"Size drop down options");

				}
			} catch (Exception e) {
				Log.event("Quantity is selected as default");
			}
		}
		waitForSpinner();
		return BrowserActions.checkLocator(driver, WISHLIST_POPUP_SELECTED_QTY).getText();
	}
	
	/**
	 * clickOnUpdateWishList
	 * @throws Exception
	 */
	public void clickOnUpdateWishList() throws Exception{
		BrowserActions.clickOnElement(btnUpdateWishList, driver, "Update wish list button");
	}
	
	/**
	 * selectedQuantity
	 * @return
	 * @throws Exception
	 */
	public String getQuantity() throws Exception{
		return BrowserActions.getText(driver, SELECTED_QTY, "Quantity value");
	}
	
	public void ClickOnEditLink() throws Exception {
		BrowserActions.clickOnElement(editLink, driver,
				"Edit Link");
	}
	public void ClickOnCancelInPopUp() throws Exception {
		BrowserActions.clickOnElement(btnCancelInPopUp, driver,
			"Cancel Button");
	}
	/**
	 * To get No 'Wishlist Result' Error Message
	 * 
	 * @return  No 'Wishlist Result' Error Message
	 * @throws Exception
	 */
	
	public String getTextFromNOWishListErrorMsg() throws Exception {
		String datatoBeReturned=null;		
		datatoBeReturned = BrowserActions.getText(driver,noWishListResult, "NO Wish List Result Error Msg");
		return datatoBeReturned;
	}


	/**
	 * To get 'Wishlist Description' Message
	 * 
	 * @return  Wishlist Description Message
	 * @throws Exception
	 */

	public String getTextFromWishListDescription() throws Exception {
		String datatoBeReturned=null;
		datatoBeReturned = BrowserActions.getText(driver,txtWishListDescription, "NO Wish List Result Error Msg");
		return datatoBeReturned;
	}
	
	public String selectQunatity() throws Exception
	{
		String selectedQuantity=null;
		Utils.waitForElement(driver, selectquantity);
		BrowserActions.javascriptClick(selectquantity, driver,
				"State Drop down");
		 List<WebElement> lstElement = selectquantity.findElement(
				By.xpath("..")).findElements(
				By.cssSelector("ul li:not([class*='selected'])"));
		for (WebElement e : lstElement) {
			if (!(e.getText().trim().equals("Select"))) {

				BrowserActions.scrollToViewElement(e, driver);
				selectedQuantity=BrowserActions.getText(driver, e, "quantity");
				BrowserActions.clickOnElement(e, driver, "list elements");
				break;

			}
		}
		return selectedQuantity;
		
	}
	public String selectPriority() throws Exception
	{
		String selectedPriority=null;
		Utils.waitForElement(driver, selectpriority);
		BrowserActions.javascriptClick(selectpriority, driver,
				"State Drop down");
		 List<WebElement> lstElement = selectpriority.findElement(
				By.xpath("..")).findElements(
				By.cssSelector("ul li:not([class*='selected'])"));
		for (WebElement e : lstElement) {
			if (!(e.getText().trim().equals("Select"))) {

				BrowserActions.scrollToViewElement(e, driver);
				selectedPriority=BrowserActions.getText(driver, e, "priority text");
				BrowserActions.clickOnElement(e, driver, "list elements");
				
				break;
				

			}
		}
	return selectedPriority;
		
	}
	
	public void clickOnUpdate() throws Exception {
		BrowserActions.clickOnElement(lnkUpdate, driver, "Update link");
	}

	public String getselectedOptionFromQuantity() throws Exception {
		return BrowserActions.getText(driver, selectquantity, "Quantity");

	}
	
	public String getSelectedQuantity() throws Exception {
		return BrowserActions.getText(driver, drpQty, "Quantity");

	}

	public String getSelectedOptionFromPriority() throws Exception {
		return BrowserActions.getText(driver, selectpriority, "Priority");

	}

	public void clickOnPrintIcon() throws Exception {
		BrowserActions.clickOnElement(lnkPrinter, driver, "Print");
		Utils.waitForPageLoad(driver);
	}
	

	/**
	 * To click the CreateAccount link
	 * 
	 * @throws Exception
	 */
	public void ClickonCreateAccount() throws Exception {
		BrowserActions.clickOnElement(lnkCreateAccount, driver, "CreateAccount link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click the PrivacyPolicy link
	 * 
	 * @throws Exception
	 */
	public void ClickonPrivacypolicy() throws Exception {
		BrowserActions.clickOnElement(lnkCreateAccount, driver, "CreateAccount link");
		Utils.waitForPageLoad(driver);
	}
	/**
	 * To click the Secure Shopping link
	 * 
	 * @throws Exception
	 */
	public void ClickonSecureShopping() throws Exception {
		BrowserActions.clickOnElement(lnkSecureShopping, driver, "Secure Shopping link");
		Utils.waitForPageLoad(driver);
	}
	

	/**
	 * To Click on Edit details in WishList Product
	 * @return HomePage
	 * @throws Exception
	 */	
	
	public void clickOnWishListEditDetailsByIndex(int index) throws Exception{
		WebElement editdetails = driver.findElement(By.cssSelector(WISHLISTTABLESTART+index+WISHLISTTABLEEND));		
		//int rows=index+1;
	//	editdetails= wishlistSearchResults.get(index).findElement(By.cssSelector(".item-edit-details"));
		 BrowserActions.scrollToView(editdetails,driver);
		 BrowserActions.clickOnElement(editdetails, driver, "Edit Details link in WishList Page");
		 Log.message("EditDetails link is clicked in wishlistpage");
	}
	/**
	 * To Verify Edit details Modal window is displayed or not
	 * 
	 * @return boolean
	 */
	public boolean verifyEditDetailsModal()
	{
		boolean status=false;
		Utils.waitForElement(driver, modalWishListEditDetails);
		if(modalWishListEditDetails.isDisplayed())
		{
			status=true;
		}
		return status;
	}

	/**
	 * To get Priority in WishList Results
	 * @return HomePage
	 * @throws Exception
	 */	
	
	public String gettextWishListPriorityByIndex(int index) throws Exception
	{
		String runPltfrm=Utils.getRunPlatForm();
		String txtpriority = null;
		if(runPltfrm == "desktop"){
		WebElement priority = driver.findElement(By.cssSelector(WISHLISTTABLESTART+index+WISHLISTPRIORITYTXT));
		BrowserActions.scrollToView(priority,driver);
		txtpriority = BrowserActions.getText(driver, priority, "Wish List prority selected value");
		}else if(runPltfrm == "mobile"){
			WebElement priority = driver.findElement(By.cssSelector(WISHLISTTABLESTART+index+WISHLISTPRIORITYTXT));
			BrowserActions.scrollToView(priority,driver);
			txtpriority = BrowserActions.getText(driver, priority, "Wish List prority selected value");
		}return txtpriority;
	
		
	}
	/**
	 * clickOnMakeThisList in Wishlist
	 * 
	 * @throws Exception
	 */
	public void clickOnMakeThisList() throws Exception {
		if (btnMakeThisList.getAttribute("value").contains("Public")) {
			BrowserActions.clickOnElement(btnMakeThisList, driver, "click make this list public");
		} else {
			BrowserActions.clickOnElement(btnMakeThisList, driver, "click make this list Private");
		}
		Utils.waitForPageLoad(driver);
	}
	

	public String getTextFrmWishlistMsg()throws Exception{
		BrowserActions.nap(3);
		BrowserActions.scrollToViewElement(msgWishlist, driver);
		String Msg = BrowserActions.getText(driver, msgWishlist ,"Wish List Message");
		return Msg;
	}
	
	public String getTextFromBreadcrumb()throws Exception{
		BrowserActions.nap(3);
		if(Utils.getRunPlatForm()=="mobile"){
			return BrowserActions.getText(driver, breadcrumbMobile, "breadcrumb for mobile");			
		}
		String Category=BrowserActions.getText(driver,txtWishListHeading,"Breadcrumb text message");
		return Category;
	}

	
	public void ClickOnAddItemsToWishList() throws Exception {
		BrowserActions.clickOnElement(btnAddItemsToWishList, driver,
				"Add Items To Wish List button");

	}
	
	public boolean VerifyLinkNotPresent(String lnktxt) throws Exception {
		BrowserActions.nap(3);
		List<WebElement> link = driver.findElements(By
				.linkText(lnktxt));
		if (link.isEmpty() == true)
			return true;
		else
			return false;
	}
	
	/**
	 * addProductToBag-e2e 
	 * @param productName
	 * @throws Exception
	 * created by Nandhini.bala 01/28/2017
	 */
	public void addProductToBag(String productName) throws Exception{
		for(int i =0; i< productList.size();i++){
			if(lstProductNames.get(i).getText().contains(productName)){
				BrowserActions.clickOnElement(btnShoppingBag, driver, "Shopping Bag");
				Utils.waitForPageLoad(driver);
			}
		}
		
	}
	
	public boolean verifyProductNameInWishlist(String productName) throws Exception{
		boolean status = false;
		for(int i =0; i< lstProductNames.size();i++){			
			if(lstProductNames.get(i).getText().trim().equals(productName.trim())){
				status = true;
				break;
			}
		}
		return status;
	}
		
	public String getProductName() throws Exception{
		return lstProductNames.get(0).getText();
	}
	
	
	
	
}// WishListPage
