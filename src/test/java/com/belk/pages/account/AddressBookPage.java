package com.belk.pages.account;

import hermes.browser.actions.BrowserAction;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.SearchResultPage;
import com.belk.pages.footers.ApoFpoPage;
import com.belk.pages.footers.WhyIsRequiredPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.AddressBookUtils;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.reusablecomponents.StateUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;
import com.btr.proxy.util.PlatformUtil.Browser;

public class AddressBookPage extends LoadableComponent<AddressBookPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public SidePannel sidePannel;
	public NeedHelpSection needHelpSection;

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader
			.getInstance("checkout");

	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = ".address-create.button")
	WebElement btnAddNewAddress;

	@FindBy(css = ".ui-dialog ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.address-dialog.account-address-dialog")
	WebElement modalAddNewAddress;

	@FindBy(id = "dwfrm_profile_address_addressid")
	WebElement txtAddressName;

	@FindBy(css=".savedaddress-list .selected-option")
	WebElement txtSelectedSavedAddress;

	String txtAddressNameLocator = "input[id='dwfrm_profile_address_addressid']";
	String txtFirstNameLocator = "input[id='dwfrm_profile_address_firstname']";
	String txtLastNameLocator = "input[id='dwfrm_profile_address_lastname']";
	String txtAddress1Locator = "input[id='dwfrm_profile_address_address1']";
	String txtAddress2Locator = "input[id='dwfrm_profile_address_address2']";
	String txtCityLocator = "input[id='dwfrm_profile_address_city']";
	String btnSelectedStateLocator = ".state-row .selected-option";
	String txtZipcodeLocator = "input[id='dwfrm_profile_address_postal']";
	String txtPhoneLocator = "input[id='dwfrm_profile_address_phone']";
	String chkMakeDefaultShippingAddrLocator = "input[id='dwfrm_profile_address_defaultAddress_preferredShipAddr']";
	String chkMakeDefaultBillingAddrLocator = "input[id='dwfrm_profile_address_defaultAddress_preferredBillAddr']";
	String txtSelectedSavedAddressLocator = ".savedaddress-list .selected-option";

	@FindBy(id = "dwfrm_profile_address_firstname")
	WebElement txtFirstName;

	@FindBy(id = "dwfrm_profile_address_lastname")
	WebElement txtLastName;

	@FindBy(id = "dwfrm_profile_address_address1")
	WebElement txtAddress1;

	@FindBy(id = "dwfrm_profile_address_address2")
	WebElement txtAddress2;

	@FindBy(id = "dwfrm_profile_address_city")
	WebElement txtCity;

	@FindBy(css = ".form-row.state-row.required > div > div > div[class ^='selected-option']")
	// state will be modified during method creation
	WebElement txtSelectedState;

	@FindBy(id = "dwfrm_profile_address_postal")
	WebElement txtZipcode;

	@FindBy(id = "dwfrm_profile_address_phone")
	WebElement txtPhone;

	@FindBy(id = "dwfrm_profile_address_defaultAddress_preferredShipAddr")
	WebElement chkMakeDefaultShippingAddr;

	@FindBy(id = "dwfrm_profile_address_defaultAddress_preferredBillAddr")
	WebElement chkMakeDefaultBillingAddr1;
	//differs in school net team
	@FindBy(css = "div>input[id='dwfrm_profile_address_defaultAddress_preferredBillAddr']")
	WebElement chkMakeDefaultBillingAddr;

	@FindBy(css = "button[name='dwfrm_profile_address_cancel']")
	WebElement btnCancelInAddNewAddressModal;

	@FindBy(css = "button[name='dwfrm_profile_address_create']")
	WebElement btnSubmitInAddNewAddressModal;

	@FindBy(css = ".mini-address-location")
	List<WebElement> lblAddressLocation;

	@FindBy(css = ".account-mini-items")
	List<WebElement> lstSavedAddress;

	@FindBy(css = ".address-delete.delete")
	List<WebElement> lnkDeleteAddress;

	//differs from school net team
	//@FindBy(id = "dwfrm_profile_address_defaultAddress_shippingaddress")
	//List<WebElement> rdoMakeDefaultShippingAddress;
	//differs from school net team
	//@FindBy(id = "dwfrm_profile_address_defaultAddress_billingaddress")
	//List<WebElement> rdoMakeDefaultBillingAddress;

	//differs from school net team - Corrected
	@FindBy(css = "input[name='dwfrm_profile_address_defaultAddress_shippingaddress']")
	List<WebElement> rdoMakeDefaultShippingAddress;

	//differs from school net team
	@FindBy(css = "input[name='dwfrm_profile_address_defaultAddress_billingaddress']")
	List<WebElement> rdoMakeDefaultBillingAddress;

	@FindBy(css="label[class='radio-label'] #shippingaddress-1 + span")
	WebElement rdoMakeDefaultShippingForFirstAddress;

	@FindBy(css="label[class='radio-label'] #billingaddress-1 + span")
	WebElement rdoMakeDefaultBillingForFirstAddress;

	@FindBy(id = "dialog-container")
	WebElement lblAddressFormDialogContainer;

	@FindBy(css = "#dialog-container>h1")
	WebElement lblAddressFormDialogContainerHeader;

	@FindBy(css = "span[id*='error']")
	List<WebElement> formError;
	/**************** Section - Links *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	/*********************************************************************/

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	@FindBy(css = "div [class='form-row form-row-button']>button:not([class*='cancel'])")
	WebElement saveBtn;

	@FindBy(css = "button[class='submit-button']")
	WebElement ctnInAddressPopUp;

	@FindBy(css = "input[name='q']")
	WebElement txtSearch;

	@FindBy(css = ".header-search button")
	WebElement btnSearch;

	//////////Addresses//////////////////////

	@FindBy(css=".address-edit")
	List<WebElement> lstEditAddressLink;

	@FindBy(css=".address-delete")
	List<WebElement> lstDeleteAddressLink;

	@FindBy(css=".address-tile")
	List<WebElement> addressTite;

	@FindBy(css = "#main>div[id='primary']>div>a")
	WebElement addAddress;

	@FindBy (css=".mini-address-name")
	List<WebElement> lblNameFromAddress;


	//copied from school net team

	@FindBy(css = "input[id*='shippingaddress']")
	List<WebElement> chkSetAsDefaultShipping;

	@FindBy(css = "input[id*='billingaddress']")
	List<WebElement> chkSetAsDefaultBilling;

	@FindBy(css = "button[class='button-fancy-large secondarybutton address-savechanges']")
	WebElement btnSaveChanges;

	@FindBy(css = "button[name='dwfrm_profile_address_edit']")
	WebElement btnSubmitInAddNewAddressModalAfterEdit;

	@FindBy(css = ".address-edit")
	List<WebElement> lnkEditAddress;

	@FindBy(id = "dwfrm_addForm_selectAddr")
	WebElement btnContinueAddressSuggestion;

	@FindBy(css = ".address-delete > .button")
	WebElement btnConfirmDeleteAddress;

	@FindBy(css = "li[class='address-tile  default']")
	List<WebElement> lstDefaultAddress;

	@FindBy(css = "button[name='dwfrm_profile_address_edit']")
	WebElement btnSubmitInEditAddressModal;

	@FindBy(css = ".address-delete > [class ^='cancel']")
	WebElement btnCancelDeleteAddress;

	@FindBy(css = ".city-row>.form-field-tooltip>a[href='/s/Belk/apo-fpo.html']")
	WebElement lnkApoFpo;

	@FindBy(css = ".city-row.required > div.form-field-tooltip > a > div.tooltip-content > p")
	WebElement lnkApoFpoTooltipContent;

	@FindBy(css = ".phone-row>.form-field-tooltip>a[href='/s/Belk/help-telephone.html']")
	WebElement lnkWhyIsReq;

	@FindBy(css = ".phone-row>.form-field-tooltip>a>.tooltip-content>p:nth-child(2)")
	WebElement lnkShippigPhoneTooltipContent;

	@FindBy(css = ".phone-row>.form-field-tooltip>a>.tooltip-content>p:nth-child(4)")
	WebElement lnkBillingPhoneTooltipContent;

	@FindBy(css = ".noaddress-details>.header_prompt>.address-header")
	WebElement titleNoAddressFoundDialog;

	@FindBy(css = ".noaddress-details>.header_prompt>.header-message")
	WebElement msgNoAddressFoundDialog;

	@FindBy(css = ".noaddress-details>.col-items>button[id='dwfrm_addForm_useOrig']")
	WebElement btnContinueNoAddressFoundDialog;

	@FindBy(css = "label[for='deliver-options-home']")
	WebElement rbUseSuggestedAddress;

	@FindBy(css = "label[for='deliver-options-original']")
	WebElement rbUseOriginalAddress;

	@FindBy(css = "label[class ='radio-label']>input[name='dwfrm_addForm_selection']")
	WebElement rbEnterAdditionalAddress;

	@FindBy(css = "#dwfrm_addForm_cancel")
	WebElement btnCancelAddressSuggestion;

	@FindBy(css = ".content-asset>.need-data>.section-header")
	WebElement lblHelpContentHeader;	

	@FindBy(css = ".content-asset>.need-data>.customer-service")
	WebElement lblHelpContentText;

	@FindBy(css = ".footer-help")
	WebElement lblAddressValidationFooter;

	@FindBy(css = ".address-header")
	WebElement lblAddressValidationHeader;

	@FindBy(css = ".header-message")
	WebElement lblAddressValidationHeaderMsg;

	@FindBy(css = ".old-details")
	List<WebElement> lblOriginalAddress;

	@FindBy(css = ".setdefault-button")
	WebElement btnSetAsDefault;

	@FindBy(id = "dwfrm_addForm_useOrig")
	WebElement btnContinueNoAddressFound;

	@FindBy(css = "input[name='dwfrm_addForm_street']")
	WebElement txtStreetNumber;
	
	@FindBy(css="input[name='dwfrm_addForm_apt']")
	WebElement txtApartmentNumber;

	@FindBy(css = ".partial-addr")
	WebElement lblStreetNumberAddressSuggestion;

	@FindBy(css=".address-title .bold")
	WebElement addressSectionHeaderName;

	@FindBy(css = ".form-row.savedaddress-list > .field-wrapper >.custom-select>.selection-list >li")
	List<WebElement> lstSuggestedAddressInAddressSuggestionSortByOptions;

	@FindBy(css = ".form-row.savedaddress-list > .field-wrapper >.custom-select>.selected-option")
	WebElement drpSuggestedAddressSelectedOptionInAddressSuggestion;


	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css=".picklist .partialPickList > li")
	List<WebElement> lstTxtPossibleAddress;

	@FindBy(css=".qas-address-list")
	WebElement drpPossibleAddress;

	@FindBy(css=".radio-label>input")
	List<WebElement> lstRadioInDialog;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.qas-dialog.ui-draggable")
	WebElement modaldialogAddressValidation;

	@FindBy(css=".account-mini-items>.mini-address-location > span")
	List<WebElement> lstAddressSpan;

	@FindBy(css=".address-edit")
	WebElement EditAddressLink;

	@FindBy(css = ".oldAddress")
	WebElement addressInAddNewAddressPopup;

	@FindBy(css = ".qas-address-list ul>li")
	List<WebElement> lstAddress;

	@FindBy(css = ".qas-address-list .showHide")
	WebElement addressdropdown;
	
	
	@FindBy(css = ".mini-address-location>span:nth-child(2)")
	WebElement txtDefaultAddressOne;
	
	@FindBy(css = ".mini-address-location>span:nth-child(3)")
	WebElement txtDefaultCityName;
	
	@FindBy(css = ".mini-address-location>span:nth-child(4)")
	WebElement txtDefaultPhoneNo;

	/**********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public AddressBookPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		needHelpSection = new NeedHelpSection(driver);
		sidePannel = new SidePannel(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, btnAddNewAddress))) {
			Log.fail("Edit Page did not open!", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
				case "profile":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkProfile, driver,
								"Profile Link");
					else
						BrowserActions.clickOnElement(lnkProfileMobile,
								driver, "Profile Link");
					Utils.waitForPageLoad(driver);
					objToReturn = new ProfilePage(driver).get();
					break;

				case "orderhistory":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkOrderHistory, driver,
								"Order History");
					else
						BrowserActions.clickOnElement(lnkOrderHistoryMobile,
								driver, "Order History");
					Utils.waitForPageLoad(driver);
					objToReturn = new OrderHistoryPage(driver).get();
					break;

				case "addressbook":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkAddressBook, driver,
								"Address Book");
					else
						BrowserActions.clickOnElement(lnkAddressBookMobile, driver,
								"Address Book");
					Utils.waitForPageLoad(driver);
					objToReturn = new AddressBookPage(driver).get();
					break;

				case "paymentmethods":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkPaymentMethods, driver,
								"Payment Methods");
					else
						BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
								driver, "Payment Methods");
					Utils.waitForPageLoad(driver);
					objToReturn = new PaymentMethodsPage(driver).get();
					break;

				case "registry":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkRegistry, driver,
								"Registry");
					else
						BrowserActions.clickOnElement(lnkRegistryMobile, driver,
								"Registry");
					Utils.waitForPageLoad(driver);
					objToReturn = new RegistrySignedUserPage(driver).get();
					break;

				case "faq":
					BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
					Utils.waitForPageLoad(driver);
					objToReturn = new FAQPage(driver).get();
					break;

				case "wishlist":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkWishList, driver,
								"WishList");
					else
						BrowserActions.clickOnElement(lnkWishListMobile, driver,
								"WishList");
					Utils.waitForPageLoad(driver);
					objToReturn = new WishListPage(driver).get();
					break;

				case "belkrewardcreditcard":
					BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver,
							"Belk Reward Credit Card");
					Utils.waitForPageLoad(driver);
					objToReturn = new BelkRewardsCreditCardPage(driver).get();
					break;

				case "emailpreferences":
					BrowserActions.clickOnElement(lnkEmailPreferences, driver,
							"Email Preferences");
					Utils.waitForPageLoad(driver);
					objToReturn = new EmailPreferencesPage(driver).get();
					break;
			}
		}

		return objToReturn;
	}

	/**
	 * To fill the Add New Address Modal in My Accout page
	 * 
	 * @param useAddressForDefaultShipping
	 *            , useAddressForDefaultBilling
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingAddNewAddressDetails(
			String addressToUse, String makeDefaultAddressForShipping,
			String makeDefaultAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> addressDetails = new LinkedHashMap<String, String>();
		String randomAddressName = RandomStringUtils.randomAlphanumeric(7)
				.toLowerCase();
		addressDetails.put("type_addressname_qa" + randomAddressName,
				txtAddressNameLocator);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		addressDetails.put("type_firstname_qaFirst" + randomFirstName,
				txtFirstNameLocator);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		addressDetails.put("type_lastname_qaLast" + randomLastName,
				txtLastNameLocator);

		String address = checkoutProperty.getProperty(addressToUse);
		String address1 = address.split("\\|")[0];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		addressDetails.put("type_address_" + address1, txtAddress1Locator);
		addressDetails.put("type_address_a", txtAddress2Locator);
		addressDetails.put("type_city_" + city, txtCityLocator);
		addressDetails.put("select_state_" + state, btnSelectedStateLocator);
		addressDetails.put("type_zipcode_" + zipcode, txtZipcodeLocator);
		addressDetails.put("type_phoneno_" + phoneNo, txtPhoneLocator);

		if (makeDefaultAddressForShipping.equals("YES")) {
			addressDetails.put("check_Make Default Shipping Address_YES",
					chkMakeDefaultShippingAddrLocator);
		}

		if (makeDefaultAddressForBilling.equals("YES")) {
			addressDetails.put("check_Make Default Billing Address_YES",
					chkMakeDefaultBillingAddrLocator);
		}

		AddressBookUtils.enterAddressDetails(addressDetails, driver);
		Log.event("Filled Address Details ",
				StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return addressDetails;

	}

	/**
	 * To get the Saved Address location on screen in My Accout page
	 * 
	 * @param index
	 * @return Location on screen
	 * @throws Exception
	 */
	public String getLocationofSavedAddress(int index)
	{
		String location = null;		
		location = lstSavedAddress.get(index-1).getLocation().toString();		
		return location;
	}

	/**
	 * Verify the Saved default Address displayed in Gray color
	 * 
	 * @param index 
	 * @throws Exception
	 */
	public void verifyDefaultAddressColor(int index)
	{				
		String Actualcolor = lstSavedAddress.get(index-1).getCssValue("background-color");
		Log.assertThat(Actualcolor.equals("rgba(242, 242, 242, 1)"),
				"The back ground color of the Default address should be gray"
				,"The back ground color of the default address is not displayed in gray");				
	}

	/**
	 * Verify the Saved Additional Address displayed in Gray color
	 * 
	 * @param index
	 * @throws Exception
	 */
	public boolean verifyAdditionalAddressColor(int index)
	{			
		boolean status=false;
		String Actualcolor = lstSavedAddress.get(index-1).getCssValue("background-color");
		if(Actualcolor.equals("rgba(255, 255, 255, 1)")) {
			status = true;
		}
		return status;
	}

	/**
	 * To open Add New Address modal
	 * 
	 * @throws Exception
	 */
	public void clickAddNewAddress() throws Exception {
		BrowserActions.clickOnElement(btnAddNewAddress, driver,
				"Add New Address button");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, modalAddNewAddress);
	}

	/**
	 * Clicking on Cancel button in Add New Address Modal
	 * 
	 * @throws Exception
	 */
	public void clickOnCancelInAddNewAddressModal() throws Exception {
		BrowserActions.clickOnElement(btnCancelInAddNewAddressModal, driver,
				"Cancel");
		Utils.waitForPageLoad(driver);
		// driver.switchTo().frame(frame);
	}


	/**
	 * To set Default shipping address
	 * 
	 * @param address
	 *            index
	 * @throws Exception
	 */
	public void setDefaultShippingAddressByIndex(int index) throws Exception {
		BrowserActions.selectRadioOrCheckbox(
				rdoMakeDefaultShippingAddress.get(index), "YES");
	}

	/**
	 * To set default billing address
	 * 
	 * @param address
	 *            index
	 * @throws Exception
	 */
	public void setDefaultBillingAddressByIndex(int index) throws Exception {
		BrowserActions.selectRadioOrCheckbox(
				rdoMakeDefaultBillingAddress.get(index), "YES");
	}

	public String getTextFromAdditionalAddresses(int index) throws Exception {
		String txtAddress = null;

		txtAddress = BrowserActions.getText(driver,
				lblAddressLocation.get(index),
				"Fetching the search text value  in the additional address");

		txtAddress = txtAddress.replace("\n", " ");
		return txtAddress;
	}

	public void ClickOnSave() throws Exception {
		Utils.waitForElement(driver, saveBtn);
		BrowserActions.javascriptClick(saveBtn, driver, "Save Button");
		Utils.waitForPageLoad(driver);
	}

	public void ClickOnContinueInValidationPopUp() throws Exception {
		Utils.waitForElement(driver, ctnInAddressPopUp);
		BrowserActions.javascriptClick(ctnInAddressPopUp, driver,
				"Save Button In Pop Up");
		Utils.waitForPageLoad(driver);
	}

	public SearchResultPage searchProductKeyword(String textToSearch)
			throws Exception {
		final long startTime = StopWatch.startTime();
		Utils.waitForElement(driver, txtSearch);
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));
		return new SearchResultPage(driver).get();
	}

	public String getTextFromAddress() throws Exception {
		String address = null;
		address = BrowserActions.getText(driver, txtAddress1,
				"Fetching the search text value  in the search result page");

		return address;
	}

	public int getTotalAddressCount(){
		return lstSavedAddress.size();
	}

	public int getLocation(int index){
		return addressTite.get(index-1).getLocation().getY();
	}

	public String getClassNameOfAddNewAddress() throws Exception{
		return BrowserActions.getTextFromAttribute(driver, addAddress, "class", "class name");
	}

	/////
	@FindBy(css=".address-title .bold")
	List<WebElement> lstAddressSectionHeader;


	public boolean verifySetDefaultBillingShippingIsChecked(){
		boolean status=true;
		for(WebElement e:lstAddressSectionHeader){
			if((e.getText().trim()).equals("Additional Addresses")){
				List<WebElement> element1=e.findElements(By.xpath("//div/following-sibling::label/input"));
				for(WebElement e2:element1){
					try {
						if(e2.getAttribute("checked").equals("checked")){
							status=false;
						}
					} catch (Exception e3) {
						// TODO: handle exception
						status=true;
					}
				}
			}
		}

		return status;
	}

	public void clickOnEditAddressByIndex(int index) throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(3);
		BrowserActions.clickOnElement(lstEditAddressLink.get(index-1), driver, "Edit link");
		BrowserActions.nap(3);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To type the last name
	 * @param lastName
	 * @throws Exception
	 */
	public void typeOnLastName(String lastName) throws Exception{
		BrowserActions.typeOnTextField(txtLastName, lastName, driver, "Last name");
	}

	public String getNameFromAddressByIndex(int index) throws Exception{
		return BrowserActions.getText(driver, lblNameFromAddress.get(index-1), "Name on Address");
	}

	public boolean verifyDefaultShippingAndBillingAddressInAddNewAddress()
			throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1",
				chkMakeDefaultBillingAddr);
		executor.executeScript("arguments[0].style.opacity=1",
				chkMakeDefaultShippingAddr);

		if (Utils.waitForElement(driver, chkMakeDefaultBillingAddr)
				&& Utils.waitForElement(driver, chkMakeDefaultShippingAddr))
			return true;

		return false;
	}

	public boolean verifyDefaultShippingAndBillingAddressInSavedAddress(
			int index) throws Exception {
		WebElement DefaultShipping = chkSetAsDefaultShipping.get(index);
		WebElement DefaultBilling = chkSetAsDefaultBilling.get(index);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1", DefaultShipping);
		executor.executeScript("arguments[0].style.opacity=1", DefaultBilling);

		if (Utils.waitForElement(driver, DefaultShipping)
				&& Utils.waitForElement(driver, DefaultBilling))
			return true;

		return false;
	}

	/**
	 * Clicking on Save Changes button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickSaveChanges() throws Exception {
		BrowserActions.clickOnElement(btnSaveChanges, driver, "Save Changes");
		Utils.waitForPageLoad(driver);
	}

	public boolean isDefaultShippingAddressByIndexChecked(int index)
			throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(rdoMakeDefaultShippingAddress.get(index));
	}

	public boolean isDefaultBillingAddressByIndexChecked(int index)
			throws Exception {
		BrowserActions.isRadioOrCheckBoxSelected(rdoMakeDefaultBillingAddress.get(index));
		return BrowserActions.isRadioOrCheckBoxSelected(rdoMakeDefaultBillingAddress.get(index));
	}

	/**
	 * To uncheck Default shipping address
	 * 
	 * @throws Exception
	 */
	public void unCheckDefaultShippingAddressInAddNewAddress() throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkMakeDefaultShippingAddr, "NO");
	}

	/**
	 * To uncheck default billing address
	 * 
	 * @throws Exception
	 */
	public void unCheckDefaultBillingAddressInAddNewAddress() throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkMakeDefaultBillingAddr, "NO");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Clicking on Save button in Add New Address Modal
	 * 
	 * @throws Exception
	 */
	public void clickOnSaveInAddNewAddressModalAfterEdit() throws Exception {
		BrowserActions.clickOnElement(btnSubmitInAddNewAddressModalAfterEdit,
				driver, "Save After Editing Address");
		Utils.waitForPageLoad(driver);
		// driver.switchTo().frame(frame);
	}

	/**
	 * To check on Edit link in Address by Index is displayed
	 * 
	 * @param index
	 *            - nth address
	 * @throws Exception
	 */
	public boolean isEditAddressDisplayedByIndex(int index) throws Exception {
		return lnkEditAddress.get(index).isDisplayed();
	}

	/**
	 * To check on Delete link in Address by Index is displayed
	 * 
	 * @param index
	 *            - nth address
	 * @throws Exception
	 */
	public boolean isDeleteAddressDisplayedByIndex(int index) throws Exception {
		return lnkDeleteAddress.get(index).isDisplayed();
	}

	/**
	 * to get the address details in Add / Edit address book Form
	 * 
	 * @return linked hash map HashMap Key Value 'title' - 'firstName' -
	 *         'lastName' - 'address1' - 'address2' - 'city' - 'state' -
	 *         'zipcode' - 'phone' -
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getAddressDetailsFromAddAddressSection()
			throws Exception {

		LinkedHashMap<String, String> address = new LinkedHashMap<String, String>();

		// String title =
		// lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location .mini-address-title")).getText().trim();
		String addressName = txtAddressName.getAttribute("value").trim();
		String firstName = txtFirstName.getAttribute("value").trim();
		String lastName = txtLastName.getAttribute("value").trim();
		String address1 = txtAddress1.getAttribute("value").trim();
		String address2 = txtAddress2.getAttribute("value").trim();
		String city = txtCity.getAttribute("value").trim();
		String state = txtSelectedState.getText().trim();
		String zipcode = txtZipcode.getAttribute("value").trim();
		String phone = txtPhone.getAttribute("value").trim();

		// address.put("title", title);
		address.put("addressName", addressName);
		address.put("firstname", firstName);
		address.put("lastname", lastName);
		address.put("address1", address1);
		address.put("address2", address2);
		address.put("city", city);
		address.put("state", state);
		address.put("zipcode", zipcode);
		address.put("phone", phone);

		return address;
	}


	/**
	 * get the Form Title
	 * 
	 * @throws Exception
	 */
	public String getFormTitle() throws Exception {
		String[] lines = BrowserActions.getText(driver,
				lblAddressFormDialogContainer, "Form Dailog").split("\n");
		return lines[0];
	}

	/**
	 * Submit Address Details - New Click Save Button
	 * 
	 * @throws Exception
	 */
	public void clickSaveAddressNewForm() throws Exception {
		BrowserActions.clickOnElement(btnSubmitInAddNewAddressModal, driver,
				"Save Button - New Address Form");
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}

	public List<String> getAddEditAddressFormErrorMsg() throws Exception {
		List<String> errorMsgs = new ArrayList<String>();
		for (int i = 1; i < formError.size(); i++) {
			errorMsgs.add(BrowserActions.getText(driver, formError.get(i),
					"Add/Edit Address Form error"));
		}
		return errorMsgs;
	}

	// select available addresses from Saved Address dropdown
	// by passing index
	public void selectSuggestedAddressByIndex(int index) throws Exception 
	{
		BrowserActions.clickOnElement(drpSuggestedAddressSelectedOptionInAddressSuggestion,driver, "Suggested Address Dropdown.");
		BrowserActions.clickOnElement(lstSuggestedAddressInAddressSuggestionSortByOptions.get(index),driver, "SortBy dropdown list");
		Utils.waitForPageLoad(driver);
	}

	// get text of the selected address from Saved Address dropdown
	public String getselectedAddressFromSavedAddressDropdown() throws Exception 
	{		
		return BrowserActions.getText(driver,drpSuggestedAddressSelectedOptionInAddressSuggestion,"Selected address In SuggestedAddressDialogContainer");
	}

	/**
	 * To fill the Add New Address Modal in My Accout page
	 * 
	 * @param useAddressForDefaultShipping
	 *            , useAddressForDefaultBilling
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingAddNewAddressDetails(
			LinkedHashMap<String, String> addressDetails,
			String makeDefaultAddressForShipping,
			String makeDefaultAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();

		LinkedHashMap<String, String> typeAddressDetails = new LinkedHashMap<String, String>();

		String addressName = addressDetails.get("title");
		typeAddressDetails.put("type_firstname_" + addressName,
				txtAddressNameLocator);
		String firstName = addressDetails.get("firstname");
		typeAddressDetails.put("type_firstname_" + firstName,
				txtFirstNameLocator);
		String lastName = addressDetails.get("lastname");
		typeAddressDetails
		.put("type_firstname_" + lastName, txtLastNameLocator);

		String address1 = addressDetails.get("address1");
		String address2 = addressDetails.get("address2");
		String city = addressDetails.get("city");
		String state = addressDetails.get("state");
		String zipcode = addressDetails.get("zipcode");
		String phoneNo = addressDetails.get("phone");

		typeAddressDetails.put("type_address_" + address1, txtAddress1Locator);

		if (address2 != "")
			typeAddressDetails.put("type_address_" + address2,
					txtAddress2Locator);

		typeAddressDetails.put("type_city_" + city, txtCityLocator);
		typeAddressDetails
		.put("select_state_" + state, btnSelectedStateLocator);
		typeAddressDetails.put("type_zipcode_" + zipcode, txtZipcodeLocator);
		typeAddressDetails.put("type_phoneno_" + phoneNo, txtPhoneLocator);

		if (makeDefaultAddressForShipping.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Shipping Address_YES",
					chkMakeDefaultShippingAddrLocator);
		}

		if (makeDefaultAddressForBilling.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Billing Address_YES",
					chkMakeDefaultBillingAddrLocator);
		}

		AddressBookUtils.enterAddressDetails(typeAddressDetails, driver);
		Log.event("Filled Billing Details as a Guest user",
				StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return typeAddressDetails;

	}

	/**
	 * To fill the Add New Address Modal in My Accout page
	 * 
	 * @param useAddressForDefaultShipping
	 *            , useAddressForDefaultBilling
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingAddNewAddressDetail(
			String addressDetails,
			String makeDefaultAddressForShipping,
			String makeDefaultAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();

		LinkedHashMap<String, String> typeAddressDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(addressDetails);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if(addressDetails.contains("_1")){
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}
		String addressName = "Address Name";
		typeAddressDetails.put("type_addressname_" + addressName,
				txtAddressNameLocator);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		typeAddressDetails.put("type_firstname_qaFirst" + randomFirstName,
				txtFirstNameLocator);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		typeAddressDetails.put("type_lastname_qaLast" + randomLastName,
				txtLastNameLocator);
		typeAddressDetails.put("type_address_" + address1, txtAddress1Locator);
		if(addressDetails.contains("_1"))
			typeAddressDetails.put("type_address_" + address2, txtAddress2Locator);
		typeAddressDetails.put("type_city_" + city, txtCityLocator);
		typeAddressDetails.put("select_state_" + state, btnSelectedStateLocator);
		typeAddressDetails.put("type_zipcode_" + zipcode, txtZipcodeLocator);
		typeAddressDetails.put("type_phoneno_" + phoneNo, txtPhoneLocator);

		if (makeDefaultAddressForShipping.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Shipping Address_YES",
					chkMakeDefaultShippingAddrLocator);
		}

		if (makeDefaultAddressForBilling.equalsIgnoreCase("Yes")) {
			typeAddressDetails.put("check_Make Default Billing Address_YES",
					chkMakeDefaultBillingAddrLocator);
		}

		AddressBookUtils.enterAddressDetails(typeAddressDetails, driver);
		Log.event("Filled Billing Details as a Guest user",
				StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return typeAddressDetails;

	}

	/**
	 * Clicking on Original Address Radio Button in Address Validation Popup
	 * 
	 * @throws Exception
	 */
	public void selectUseOriginalAddress() throws Exception {
		if(Utils.waitForElement(driver, rbUseOriginalAddress))
			BrowserActions.selectRadioOrCheckbox(rbUseOriginalAddress, "YES");
	}

	/**
	 * Clicking on Save Changes button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickContinueAddressSuggestion() throws Exception {
		if(Utils.waitForElement(driver, btnContinueAddressSuggestion))
			BrowserActions.clickOnElement(btnContinueAddressSuggestion, driver,
					"Continue in Address Suggestion");
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * get Street Address from Address Suggestion modal
	 * 
	 * @throws Exception
	 */
	public String getStreetNumberInAddressSuggestion() throws Exception {
		return BrowserActions.getText(driver, lblStreetNumberAddressSuggestion, "Street Number in Address Suggestion modal");
	}

	/**
	 * To click on Delete link in Address by Index
	 * 
	 * @param index
	 *            - nth address
	 * @throws Exception
	 */
	public void deleteAddressByIndex(int index) throws Exception {
		clickOnDeleteAddressByIndex(index);
		BrowserActions.javascriptClick(btnConfirmDeleteAddress, driver,
				"Confirm Delete in Delete address section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Cancel Address Details - Add/Edit Click Save Button
	 * 
	 * @throws Exception
	 */
	public void clickCancelAddressAddEditForm() throws Exception {
		BrowserActions.clickOnElement(btnCancelInAddNewAddressModal, driver,
				"Cancel Button - Add/Edit Address Form");
	}

	/**
	 * Get default address by index
	 * 
	 * @param index
	 *            - nth address
	 * @return LinkedHashMap
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getDefaultAddressByIndex(int index)
			throws Exception {
		return getDefaultAddress().get(index);
	}

	/**
	 * to get the saved default address details in address book
	 * 
	 * @return linked list of linked hash map HashMap Key Value 'title' -
	 *         'firstName' - 'lastName' - 'address1' - 'address2' - 'city' -
	 *         'state' - 'zipcode' - 'phone' - 'defaultshipping' - 'YES' or 'NO'
	 *         'defaultbilling' - 'YES' or 'NO'
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getDefaultAddress()
			throws Exception {
		LinkedList<LinkedHashMap<String, String>> addressDetails = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < lstDefaultAddress.size(); i++) {
			LinkedHashMap<String, String> address = new LinkedHashMap<String, String>();
			String address2=null;
			String title = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-title"))
							.getText().trim();
			String firstName = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location .mini-address-name"))
							.getText().trim().split(" ")[0];
			String lastName = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location .mini-address-name"))
							.getText().trim().split(" ")[1];
			String address1 = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child(2)"))
							.getText().trim();
			int j=3;
			if(lstAddressSpan.size()>=4){
				address2 = lstDefaultAddress
						.get(i)
						.findElement(
								By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child("+j+")"))
								.getText().trim();				
				j++;
			}
			String city = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child("+j+")"))
							.getText().trim().split(",")[0];
			String state = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child("+j+")"))
							.getText().trim().split(", ")[1].trim().split(" ")[0];
			String zipcode = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child("+j+")"))
							.getText().trim().split(",")[1].trim().split(" ")[1];
			String phone = lstDefaultAddress
					.get(i)
					.findElement(
							By.cssSelector(".account-mini-items>.mini-address-location .formatPhone"))
							.getText().trim();
			String defaultshipping;
			try {
				defaultshipping = lstDefaultAddress
						.get(i)
						.findElement(
								By.cssSelector(".radio-label>input[name='dwfrm_profile_address_defaultAddress_shippingaddress']"))
								.getAttribute("checked").trim().equals("true") ? "YES"
										: "NO";
			} catch (Exception e) {
				defaultshipping = "NO";
			}
			String defaultbilling;
			try {
				defaultbilling = lstDefaultAddress
						.get(i)
						.findElement(
								By.cssSelector(".radio-label>input[name='dwfrm_profile_address_defaultAddress_billingaddress']"))
								.getAttribute("checked").equals("true") ? "YES" : "NO";
			} catch (Exception e) {
				defaultbilling = "NO";

			}

			address.put("title", title);
			address.put("firstname", firstName);
			address.put("lastname", lastName);
			address.put("address1", address1);
			address.put("address2", address2);
			address.put("city", city);
			address.put("state", state);
			address.put("zipcode", zipcode);
			address.put("phone", phone);
			address.put("defaultshipping", defaultshipping);
			address.put("defaultbilling", defaultbilling);

			addressDetails.add(address);
		}

		return addressDetails;
	}

	/**
	 * Submit Address Details - Edit Click Save Button
	 * 
	 * @throws Exception
	 */
	public void clickSaveAddressEditForm() throws Exception {
		BrowserActions.clickOnElement(btnSubmitInEditAddressModal, driver,
				"Save Button - Edit Address Form");
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Cancel button in Confirm Delete Address Form
	 * 
	 * @throws Exception
	 */
	public void clickCancelDeleteAddress() throws Exception {
		BrowserActions.javascriptClick(btnCancelDeleteAddress, driver,
				"Cancel Delete in Delete address section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Yes button in Confirm Delete Address Form
	 * 
	 * @throws Exception
	 */
	public void clickConfirmDeleteAddress() throws Exception {
		BrowserActions.javascriptClick(btnConfirmDeleteAddress, driver,
				"Confirm Delete in Delete address section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * get the Form Content
	 * 
	 * @throws Exception
	 */
	public String getFormContent() throws Exception {
		String[] lines = BrowserActions.getText(driver,
				lblAddressFormDialogContainer, "Form Dailog").split("\n");
		return lines[1];
	}

	/**
	 * get the text from Apo/Fpo tooltip in Edit address dialog
	 * 
	 * @throws Exception
	 */
	public String getApoFpoTooltipText() throws Exception {
		mouseOverAPOFPO();
		return BrowserActions.getText(driver, lnkApoFpoTooltipContent,
				"Apo/Fpo Tooltip Content");
	}

	/**
	 * get the text shipping phone tooltip in Edit address dialog
	 * 
	 * @throws Exception
	 */
	public String getShippingPhoneTooltipText() throws Exception {
		mouseOverWHYISREQ();
		return BrowserActions.getText(driver, lnkShippigPhoneTooltipContent,
				"Shipping Phone Tooltip Content");
	}

	/**
	 * get the text billing phone tooltip in Edit address dialog
	 * 
	 * @throws Exception
	 */
	public String getBillingPhoneTooltipText() throws Exception {
		mouseOverWHYISREQ();
		return BrowserActions.getText(driver, lnkBillingPhoneTooltipContent,
				"Billing Phone Tooltip Content");
	}

	public void mouseOverAPOFPO() throws InterruptedException {
		BrowserActions.scrollToView(txtCity, driver);
		BrowserActions.mouseHover(driver, lnkApoFpo);
		BrowserActions.nap(3);
	}

	public void mouseOverWHYISREQ() throws InterruptedException {
		BrowserActions.scrollToView(txtZipcode, driver);
		BrowserActions.mouseHover(driver, lnkWhyIsReq);
		BrowserActions.nap(3);
	}

	/**
	 * Clicking on ApoFpoLink
	 * 
	 * @throws Exception
	 *             returns ApoFpoPage
	 */
	public ApoFpoPage clickOnApoFpoTooltipLinkInAddNewAddressModal()
			throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.javascriptClick(lnkApoFpo, driver,
					"clicking ApoFpo tooltip");
			BrowserActions.nap(1);
			BrowserActions.javascriptClick(lnkApoFpo, driver,
					"clicking ApoFpo tooltip");
			BrowserActions.nap(1);
		} else {
			mouseOverAPOFPO();
			BrowserActions.javascriptClick(lnkApoFpo, driver,
					"clicking ApoFpo tooltip");
			BrowserActions.nap(1);
		}
		// Utils.waitForPageLoad(driver);
		// BrowserActions.clickOnElement(lnkApoFpo, driver,
		// "clicking ApoFpo tooltip");
		return new ApoFpoPage(driver).get();
	}

	/**
	 * Clicking on PhoneLink returns
	 * 
	 * @throws Exception
	 *             WhyIsRequiredPage
	 */
	public WhyIsRequiredPage clickOnPhoneTooltipLinkInAddNewAddressModal()
			throws Exception {
		mouseOverWHYISREQ();
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.javascriptClick(lnkWhyIsReq, driver,
					"clicking Why Is Required tooltip");
			BrowserActions.nap(1);
			BrowserActions.javascriptClick(lnkWhyIsReq, driver,
					"clicking Why Is Required tooltip");
			BrowserActions.nap(1);
		} else {
			BrowserActions.javascriptClick(lnkWhyIsReq, driver,
					"clicking Why Is Required tooltip");
			BrowserActions.nap(1);
		}
		return new WhyIsRequiredPage(driver).get();
	}

	/**
	 * get the title of no address detail dialog
	 * 
	 * @throws Exception
	 */
	public String getTitleInNoAddressDetailsDialog() throws Exception {
		return BrowserActions.getText(driver, titleNoAddressFoundDialog,
				"title of no address detail dialog");
	}

	/**
	 * get the message content of no address detail dialog
	 * 
	 * @throws Exception
	 */
	public String getMessageContentInNoAddressDetailsDialog() throws Exception {
		return BrowserActions.getText(driver, msgNoAddressFoundDialog,
				"message content of no address detail dialog");
	}

	/**
	 * click button continue no address detail dialog
	 * 
	 * @throws Exception
	 */
	public void clickCountinueInNoAddressDetailsDialog() throws Exception {
		BrowserActions.clickOnElement(btnContinueNoAddressFoundDialog, driver,
				"Continue of no address detail dialog");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Clicking on Suggested Address Radio Button in Address Validation Popup
	 * 
	 * @throws Exception
	 */
	public void selectUseSuggestedAddress() throws Exception {
		BrowserActions.selectRadioOrCheckbox(rbUseSuggestedAddress, "YES");
	}

	/**
	 * Check Suggested Address Radio Button in Address Validation Popup is
	 * displayed
	 * 
	 * @throws Exception
	 */
	public Boolean isUseSuggestedAddressRadioButtonDisplayed() throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(rbUseSuggestedAddress);
	}

	/**
	 * Check Original Address Radio Button in Address Validation Popup is
	 * displayed
	 * 
	 * @throws Exception
	 */
	public Boolean isUseOriginalAddressRadioButtonDisplayed() throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(rbUseOriginalAddress);
	}

	public boolean verifyDialogContainerDisplayed(){
		boolean status=false;
		if(Utils.waitForElement(driver, lblAddressFormDialogContainer))
			status = true;
		return status;
	}

	/**
	 * Check Enter Additional Address Details Radio Button in Address Validation Popup is
	 * Selected by default
	 * 
	 * @throws Exception
	 */
	public Boolean isEnterAdditionalAddressRadioButtonSelected() throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(rbEnterAdditionalAddress);
	}

	/**
	 * Enter Street number in Additional Address Details section in Address Validation Popup is
	 * Selected by default
	 * 
	 * @throws Exception
	 */
	public void typeStreetNumber(String streetNumber) throws Exception {		
		BrowserActions.typeOnTextField(txtStreetNumber, streetNumber, driver, "Street number in the Address Validatio popup");
	}

	/**
	 * to get the address1 from saved address details
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String getAddressOneFromSavedAddress(int i) {
		String address1 = lstSavedAddress
				.get(i)
				.findElement(
						By.cssSelector(".account-mini-items>.mini-address-location > span:nth-child(2)"))
						.getText().trim();
		return address1;
	}

	/**
	 * Clicking on Cancel button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickCancelAddressSuggestion() throws Exception {
		BrowserActions.clickOnElement(btnCancelAddressSuggestion, driver,
				"Cancel in Address Suggestion");
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * get the Address Validation Form Footer
	 * @throws Exception
	 */
	public String getAddressValidationFooter() throws Exception
	{
		return BrowserActions.getText(driver, lblAddressValidationFooter, "Address Validation Form Footer");

	}

	/**
	 * get the Address Validation Form Title
	 * @throws Exception
	 */
	public String getAddressValidationHeader() throws Exception
	{
		return BrowserActions.getText(driver, lblAddressValidationHeader, "Address Validation Form Footer");

	}

	/**
	 * get the Help Content header message Form Footer
	 * @throws Exception
	 */
	public String getHelpContentHeaderTitle() throws Exception
	{
		return BrowserActions.getText(driver, lblHelpContentHeader, "Help Content Title Form Footer");

	}

	/**
	 * get the Help Content message Form Footer
	 * @throws Exception
	 */
	public String getHelpContentMessage() throws Exception
	{
		return BrowserActions.getText(driver, lblHelpContentText, "Help Content Message Form Footer");

	}


	/**
	 * get the Address Validation Form Title Msg
	 * @throws Exception
	 */
	public String getAddressValidationHeaderMsg() throws Exception
	{
		return BrowserActions.getText(driver, lblAddressValidationHeaderMsg, "Address Validation Form Footer");

	}

	/**
	 * get the Address Validation Form Title Msg
	 * @throws Exception
	 */
	public String getOriginalAddressInAddressValidation() throws Exception
	{
		String OriginalAddress =  BrowserActions.getText(driver, lblOriginalAddress.get(0), "SuggestedAddress");
		OriginalAddress = OriginalAddress + BrowserActions.getText(driver, lblOriginalAddress.get(1), "SuggestedAddress");
		return OriginalAddress;
	}

	/**
	 * To check Default shipping address
	 * @throws Exception
	 */
	public void checkDefaultShippingAddressInAddNewAddress()throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkMakeDefaultShippingAddr, "YES");
	}

	/**
	 * To check default billing address
	 * @throws Exception
	 */
	public void CheckDefaultBillingAddressInAddNewAddress()throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkMakeDefaultBillingAddr, "YES");
	}

	/**
	 * Is default shipping address checked?
	 * @throws Exception
	 */
	public boolean isDefaultShippingAddressInAddNewAddressChecked()throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(chkMakeDefaultShippingAddr);
	}

	/**
	 * Is default billing address checked?
	 * @throws Exception
	 */
	public boolean isDefaultBillingAddressInAddNewAddressChecked()throws Exception {
		return BrowserActions.isRadioOrCheckBoxSelected(chkMakeDefaultBillingAddr);
	}

	/**
	 * Clicking on Set As Default button in Add New Address Modal
	 * @throws Exception
	 */
	public void clickOnSetAsDefaultInAddNewAddressModal() throws Exception
	{
		BrowserActions.clickOnElement(btnSetAsDefault, driver, "Set As Default");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Clicking on Continue button in No Address Found popup
	 * @throws Exception
	 */
	public void clickOnContinueInNoAddressFoundPopup() throws Exception
	{
		BrowserActions.clickOnElement(btnContinueNoAddressFound, driver, "Continue");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * get the Set As Default Form description
	 * @throws Exception
	 */
	public String getFormDescription() throws Exception
	{
		String[] lines = BrowserActions.getText(driver, lblAddressFormDialogContainer, "Form Title").split("\n");
		return lines[1];
	}

	/**
	 * To select value in Saved Address dropdown
	 * @throws Exception
	 */
	public void selectFromSavedAddress(String valueToBeSelected)throws Exception {
		LinkedHashMap<String, String> addressDetails = new LinkedHashMap<String, String>();
		addressDetails.put("select_savedAddress_"+valueToBeSelected,txtSelectedSavedAddressLocator);
		AddressBookUtils.enterAddressDetails(addressDetails, driver);
	}


	/**
	 * get the Address Section Header name(Default/Additional) Address
	 * return Address Header name
	 * @throws Exception
	 */
	public String getAddressSectionHeader() throws Exception
	{
		return BrowserActions.getText(driver, addressSectionHeaderName, "Address Section Header Name");

	}

	/**
	 * Gets the Text in the breadcrumbs as a list
	 * @return  breadcrumbs as a list
	 */
	public List<String> getTextInBreadcrumb() {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
		if (Utils.getRunPlatForm().equals("desktop")) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To get the header text from the dialog container
	 * @return
	 * @throws Exception
	 */
	public String getTextFromDialogContainerHeader() throws Exception{
		return BrowserActions.getText(driver, lblAddressFormDialogContainerHeader, "Dialog container header");
	}

	public boolean VerifyAddressAvailability(){
		if (lstSavedAddress.size() > 0) 
			return Utils.waitForElement(driver, lstSavedAddress.get(0));
		else
			return false;
	}

	public String getAddressFromPossibleMatchPopupByIndex(int index) throws Exception{
		return BrowserActions.getText(driver, lstTxtPossibleAddress.get(index-1), "Address From POPUP dropdown");
	}

	public int getCountOfRadioInDialog(){
		return lstRadioInDialog.size();
	}

	public String getTextOfRadioFromDialogByIndex(int index) throws Exception{
		return BrowserActions.getText(driver, lstRadioInDialog.get(index-1).findElement(By.xpath("following-sibling::span")), "Radio button String");
	}

	public boolean verifyAddressDropdownInDialog() {
		if(Utils.waitForElement(driver,drpPossibleAddress))
			return true;
		return false;
	}

	public boolean isSuggestedAddressModalwindowDisplayed(){
		if (modaldialogAddressValidation.isDisplayed()) 
			return Utils.waitForElement(driver, modaldialogAddressValidation);
		else
			return false;
	}

	/**
	 * to get the saved address details in address book
	 * 
	 * @return linked list of linked hash map HashMap Key Value 'title' -
	 *         'firstName' - 'lastName' - 'address1' - 'address2' - 'city' -
	 *         'state' - 'zipcode' - 'phone' - 'defaultshipping' - 'YES' or 'NO'
	 *         'defaultbilling' - 'YES' or 'NO'
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getSavedAddress()throws Exception{
		LinkedList<LinkedHashMap<String, String>> addressDetails = new LinkedList<LinkedHashMap<String, String>>();
		//BrowserActions.scrollToView((WebElement) lstSavedAddress, driver);
		for(int i=0 ; i < lstSavedAddress.size() ; i++)
		{
			LinkedHashMap<String, String> address = new LinkedHashMap<String, String>();

			String[] savedAddressDetails = lstSavedAddress.get(i).findElement(By.cssSelector(".account-mini-items>.mini-address-location")).getText().split("\n");;




			String title = lstSavedAddress.get(i).findElement(By.cssSelector(".account-mini-items>.mini-address-title")).getText().trim();
			String firstName = savedAddressDetails[0].trim().split(" ")[0];
			String lastName = savedAddressDetails[0].trim().split(" ")[1];
			String address1 = savedAddressDetails[1].trim();
			String address2 = "";
			int cityPos = 2;

			if(savedAddressDetails.length == 5)
			{
				address2 = savedAddressDetails[2].trim();
				cityPos=3;
			}

			String city = savedAddressDetails[cityPos].trim().split(",")[0];

			String state = StateUtils.getStateName(savedAddressDetails[cityPos].trim().split("\\,")[1].trim().split(" ")[0]);

			String zipcode = savedAddressDetails[cityPos].trim().split(",")[1].trim().split(" ")[1];

			String phone = savedAddressDetails[cityPos+1].trim().replace("-", "");


			/*    String title = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location .mini-address-title")).getText().trim();
                        String firstName = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location .mini-address-name")).getText().trim().split("\\n")[0];
                        String lastName = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location .mini-address-name")).getText().trim().split("\\n")[1];
                        String address1 = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location > span:nth-child(2)")).getText().trim();
                         String address2 = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location > span:nth-child(3)")).getText().trim();
              String city = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location > span:nth-child(4)")).getText().trim().split(",")[0];
                        String state = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location > span:nth-child(4)")).getText().trim().split(",")[1].trim().split(" ")[0];
                        String zipcode = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location > span:nth-child(4)")).getText().trim().split(",")[1].trim().split(" ")[1];
                        String phone = lstSavedAddress.get(i).findElement(By.cssSelector(".mini-address-location .payment-phone")).getText().trim();
			 */       	
			String defaultshipping = BrowserActions.getRadioOrCheckboxChecked(lstSavedAddress.get(i).findElement(By.cssSelector("input[name='dwfrm_profile_address_defaultAddress_shippingaddress']")));  //lstSavedAddress.get(i).findElement(By.cssSelector("input[name='dwfrm_profile_address_defaultAddress_shippingaddress']")).getAttribute("checked").equals("checked") ? "YES" : "NO";
			String defaultbilling = BrowserActions.getRadioOrCheckboxChecked(lstSavedAddress.get(i).findElement(By.cssSelector("input[name='dwfrm_profile_address_defaultAddress_billingaddress']")));  //lstSavedAddress.get(i).findElement(By.cssSelector("input[name='dwfrm_profile_address_defaultAddress_billingaddress']")).getAttribute("checked").equals("checked") ? "YES" : "NO";

			address.put("Title", title);
			address.put("FirstName", firstName);
			address.put("LastName", lastName);
			address.put("Address1", address1);
			address.put("Address2", address2);
			address.put("City", city);
			address.put("State", state);
			address.put("Zipcode", zipcode);
			address.put("PhoneNo", phone);
			address.put("Defaultshipping", defaultshipping);
			address.put("Defaultbilling", defaultbilling);



			addressDetails.add(address);

		}

		return addressDetails;
	}


	/**
	 * To click on Delete link in Address by Index
	 * 
	 * @param index
	 *            - nth address
	 * @throws Exception
	 */
	public void clickOnDeleteAddressByIndex(int index) throws Exception {
		BrowserActions.scrollToViewElement(lnkDeleteAddress.get(index), driver);
		BrowserActions.javascriptClick(lnkDeleteAddress.get(index), driver,
				"Delete Link in address section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Get saved address by index
	 * 
	 * @param index
	 *            - nth address
	 * @return LinkedHashMap
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getSavedAddressByIndex(int index)
			throws Exception {
		return getSavedAddress().get(index);
	}

	/**
	 * To set Address Title
	 * 
	 * @param address
	 *            name
	 * @throws Exception
	 */
	public void setAddressTitle(String addressTitle) throws Exception {
		BrowserActions.typeOnTextField(txtAddressName, addressTitle, driver,
				"Address Name");
	}

	/**
	 * To type first name
	 * 
	 * @param firstName
	 * @throws Exception
	 */
	public void setFirstName(String firstName) throws Exception {
		BrowserActions.typeOnTextField(txtFirstName, firstName, driver,
				"First Name");
	}

	/**
	 * To type last name
	 * 
	 * @param lastname
	 * @throws Exception
	 */
	public void setLastName(String lastName) throws Exception {
		BrowserActions.typeOnTextField(txtLastName, lastName, driver,
				"Last Name");
	}

	/**
	 * To type address1
	 * 
	 * @param address1
	 * @throws Exception
	 */
	public void setAddress1(String address1) throws Exception {
		BrowserActions.typeOnTextField(txtAddress1, address1, driver,
				"Address1 Name");
	}

	/**
	 * To type address2
	 * 
	 * @param address2
	 * @throws Exception
	 */
	public void setAddress2(String address2) throws Exception {
		BrowserActions.typeOnTextField(txtAddress2, address2, driver,
				"Address2 Name");
	}

	/**
	 * To type city
	 * 
	 * @param city
	 * @throws Exception
	 */
	public void setCity(String city) throws Exception {
		BrowserActions.typeOnTextField(txtCity, city, driver,
				"City Name");
	}

	/**
	 * To select state
	 * 
	 * @param state
	 * @throws Exception
	 */
	public void selectState(String state) throws Exception {
		WebElement element = driver.findElement(By
				.cssSelector(btnSelectedStateLocator));
		BrowserActions.javascriptClick(element, driver, "State Drop down");
		List<WebElement> lstElement = element.findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul li"));
		for (WebElement e : lstElement) {
			if (e.getText().trim().equals(state)) {
				BrowserActions.scrollToViewElement(e, driver);
				BrowserActions.javascriptClick(e, driver, "list elements");
				break;
			}
		}
	}

	/**
	 * To type zipcode
	 * 
	 * @param zipcode
	 * @throws Exception
	 */
	public void setZipcode(String zipcode) throws Exception {
		BrowserActions.typeOnTextField(txtZipcode, zipcode, driver,
				"Zipcode Name");
	}

	/**
	 * To type phone number
	 * 
	 * @param phone
	 * @throws Exception
	 */
	public void setPhone(String phone) throws Exception {
		BrowserActions.typeOnTextField(txtPhone, phone, driver,
				"Phone Name");
	}


	public void clickOnEditAddress() throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(3);
		BrowserActions.clickOnElement(EditAddressLink, driver, "Edit link");
		BrowserActions.nap(3);
		Utils.waitForPageLoad(driver);
	}

	public String getTextFromDropdown() throws Exception {

		String toBeReturned = BrowserActions.getText(driver, addressdropdown, "Text in the drop down");
		return toBeReturned;

	}

	public void clickAddressDropown() throws Exception {

		BrowserActions.clickOnElement(addressdropdown, driver, "SelectDropDown");
	}

	public void selectAddressByIndex(int addressindex) throws Exception {

		WebElement element=lstAddress.get(addressindex-1);
		BrowserActions.scrollToViewElement(element, driver);
		BrowserActions.clickOnElement(element, driver, "Address");

	}
	
	@FindBy(css=".showHide")
	WebElement txtSelectedAddressUSPC;
	
	@FindBy(css=".refineField")
	WebElement txtStreetNumberInUSPC;
	
	
	public String getSelectedAddressFromUSPC() throws Exception{
		return BrowserActions.getText(driver,txtSelectedAddressUSPC,"SelectedAddress in USPC");
	}
	
	public void typeOnStreetNumber(String streetNumber) throws Exception{
		BrowserActions.typeOnTextField(txtStreetNumberInUSPC, streetNumber, driver, "Street number");
	}
	
	public LinkedHashMap<String, String> getDefaultShippingAddressDetails() throws Exception {
		BrowserActions.nap(5);
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

		String addressOne = BrowserActions.getText(driver, txtDefaultAddressOne, "Address 1 in Shipping Address");

		String city = BrowserActions.getText(driver, txtDefaultCityName, "City in Shipping Address");
		String phoneNo = BrowserActions.getText(driver, txtDefaultPhoneNo, "Phone No in Shipping Address");
		phoneNo = phoneNo.replaceAll("-", "");

		shippingDetails.put("AddressOne", addressOne);
		shippingDetails.put("City", city);
		shippingDetails.put("PhoneNo", phoneNo);

		return shippingDetails;

	}
	
	/**
	 * Enter Apartment number in Additional Address Details section in Address Validation Popup is
	 * Selected by default
	 * 
	 * @throws Exception
	 */
	public void typeApartmentNumber(String apartmentNumber) throws Exception {		
		BrowserActions.typeOnTextField(txtApartmentNumber, apartmentNumber, driver, "Apartment number in the Address Validatio popup");
	}

}
