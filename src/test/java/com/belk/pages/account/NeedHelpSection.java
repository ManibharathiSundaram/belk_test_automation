package com.belk.pages.account;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class NeedHelpSection extends LoadableComponent<NeedHelpSection> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = ".section-header")
	WebElement lnkNeedHelpSectionHeader;
	
	@FindBy(css="div[class*='account-nav-asset']")
	WebElement needHelpSection;
	
	@FindBy(css=".account-nav-asset .hide-mobile")
	WebElement needHelpSectionDesktop;
	
	@FindBy(css=".account-nav-asset.hide-desktop")
	WebElement needHelpSectionMobile;
	
	@FindBy(css = ".need-data .customer-service")
	WebElement customerServiceContentPaneDesktop;

	@FindBy(css = ".account-nav-asset.hide-desktop .customer-service")
	WebElement customerServiceContentPaneMobile;

	/**********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public NeedHelpSection(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, customerServiceContentPaneDesktop))) {
			Log.fail("Edit Page did not open!", driver);
		}

	}
	
	/*
	 * To Fetch Contents of Need Help Section
	 * @throws Exception
	 */
	public String getNeedHelpSectionText() throws Exception 
	{
		if(Utils.getRunPlatForm()=="mobile"){
			BrowserActions.scrollToView(customerServiceContentPaneMobile, driver);
			return BrowserActions.getText(driver, customerServiceContentPaneMobile,"Need Help Section");
		}else{
			BrowserActions.scrollToView(customerServiceContentPaneDesktop, driver);
			return BrowserActions.getText(driver, customerServiceContentPaneDesktop,"Need Help Section");	
		}
		
	}
	
}

