package com.belk.pages.account;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.HomePage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class OrderHistoryPage extends LoadableComponent <OrderHistoryPage>{

	private WebDriver driver;
	public ElementLayer elementLayer;
	private boolean isPageLoaded;


	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = "div[class='pt_orderhistory']")
	WebElement frmOrderHistory;

	/****************Section - Links*********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	/*********************************************************************/

	//*******mobile elements***************

	/*@FindBy(css = "h3 > a[title='']")
		WebElement lnkMyAccountMobile;*/

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;


	//////////////////////////////////////////////////////////////////////////////
	@FindBy(css=".order-number .value")
	List<WebElement> lstOrderNumber;

	@FindBy(css=".order-date .value")
	List<WebElement> lstDateOrdered;

	@FindBy(css=".order-status:nth-child(3)>span:nth-child(2)")
	List<WebElement> lstOrderTotal;

	@FindBy(css=".order-status:nth-child(4)>span:nth-child(2)")
	List<WebElement> lstOrderStatus;

	@FindBy(css="button[name*='dwfrm_orders_orderlist']")
	List<WebElement> lstViewDetails;

	@FindBy(css=".results-hits")
	WebElement lblResultHits;

	@FindBy(css=".pagination>ul>li")
	List<WebElement> lstPagination;	

	@FindBy(css = ".order-shipments>.order-shipments-row>.section-header")
	WebElement lblShipmentNumber;

	@FindBy(css=".search-result-items > li")
	List<WebElement> noOfOrders;

	@FindBy(css=".order-date:nth-child(2)>span[class='value']")
	WebElement orderDate;

	@FindBy(css=".order-date:nth-child(3)>span[class='value']")
	WebElement orderTime;

	@FindBy(css=".order-date:nth-child(4)>span[class='value']")
	WebElement orderNumber;

	@FindBy(css=".order-date:nth-child(5)>span[class='value']")
	WebElement orderStatus;

	@FindBy (css=".order-payment-instruments-list .cc-type")
	WebElement lblCardType;

	@FindBy (css=".order-payment-instruments-list .cc-owner")
	WebElement lblNameOnCard;

	@FindBy (css=".order-payment-instruments-list .cc-card-group")
	WebElement lblCardNumber;	

	@FindBy (css=".order-payment-instruments-list .order-date")
	WebElement lblAmount;

	@FindBy(css=".billing-information .order-date")
	List<WebElement> lstbillingAddress;

	@FindBy(css=".order-info .value")
	List<WebElement> paymentdetails;

	@FindBy(css=".print-page")
	WebElement btnPrinter;

	@FindBy (css=".write-review")
	WebElement lnkWriteReview;

	@FindBy(css=".order-shipping-details a[class*='secondarybutton']")
	WebElement btnReturnToShopping;

	@FindBy (css=".order-back-history>a")
	WebElement lnkBackToOrderHistory;

	@FindBy(css=".pagination")
	WebElement lblPagination;	

	@FindBy(css=".current-page")
	WebElement lblCurrentPage;

	@FindBy(css=".page-next .sprite")
	WebElement paginationGraterThanSymbol;

	@FindBy(css=".page-previous .sprite")
	WebElement paginationLesserThanSymbol;

	@FindBy(css=".page-first .sprite")
	WebElement paginationFirstPageSymbol;

	@FindBy(css=".page-last .sprite")
	WebElement paginationLastPageSymbol;

	@FindBy(css=".item-name > a")
	List<WebElement> lstOrderItemName;

	@FindBy(css=".item-upc > .product-UPC")
	List<WebElement> lstOrderItemUPC;

	@FindBy(css=".attribute[data-attribute='color'] > .value")
	List<WebElement> lstOrderItemColor;

	@FindBy(css=".attribute[data-attribute='size'] > .value")
	List<WebElement> lstOrderItemSize;

	@FindBy(css=".line-item-quantity> .value")
	List<WebElement> lstOrderItemQuantity;

	@FindBy(css=".orderhistorydetails > h1")
	WebElement txtOrderHistoryTitle;

	@FindBy(css=".ordercancel-button.button")
	WebElement btnYesInOrderCancelPopUp;

	@FindBy(css="item-priceadj> .value")
	List<WebElement> lstOrderItemPrice;

	@FindBy(css="div[class='line-item-price-det']")
	List<WebElement> lstOrderItemTotal;

	@FindBy(css="div[class='line-item-price-det bold']")
	List<WebElement> lstOrderItemSubTotal;

	@FindBy(css = ".breadcrumb-element")
	List<WebElement> lstBreadcrumb;
	
	@FindBy(css=".hide-desktop .breadcrumb-element")
	WebElement breadcrumbMobile;
	//////////////////////////////////////////////////////////////////////////////

	/**********************************************************************************************/



	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public OrderHistoryPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);	

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}		
		if (isPageLoaded && !(Utils.waitForElement(driver, frmOrderHistory))) {
			Log.fail("Edit Page did not open!", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
				case "profile":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkProfile, driver, "Profile Link");
					else
						BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Profile Link");
					Utils.waitForPageLoad(driver);
					objToReturn = new ProfilePage(driver).get();
					break;

				case "orderhistory":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkOrderHistory, driver, "Order History");
					else
						BrowserActions.clickOnElement(lnkOrderHistoryMobile, driver, "Order History");
					Utils.waitForPageLoad(driver);
					objToReturn = new OrderHistoryPage(driver).get();
					break;

				case "addressbook":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkAddressBook, driver, "Address Book");
					else
						BrowserActions.clickOnElement(lnkAddressBookMobile, driver, "Address Book");
					Utils.waitForPageLoad(driver);
					objToReturn = new AddressBookPage(driver).get();
					break;

				case "paymentmethods":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkPaymentMethods, driver, "Payment Methods");
					else
						BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Payment Methods");
					Utils.waitForPageLoad(driver);
					objToReturn = new PaymentMethodsPage(driver).get();
					break;

				case "registry":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
					else
						BrowserActions.clickOnElement(lnkRegistryMobile, driver, "Registry");
					Utils.waitForPageLoad(driver);
					objToReturn = new RegistrySignedUserPage(driver).get();
					break;

				case "faq":
					BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
					Utils.waitForPageLoad(driver);
					objToReturn = new FAQPage(driver).get();
					break;

				case "wishlist":
					if (platForm.equals("desktop"))
						BrowserActions.clickOnElement(lnkWishList, driver, "WishList");
					else
						BrowserActions.clickOnElement(lnkWishListMobile, driver, "WishList");
					Utils.waitForPageLoad(driver);
					objToReturn = new WishListPage(driver).get();
					break;

				case "belkrewardcreditcard":
					BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver, "Belk Reward Credit Card");
					Utils.waitForPageLoad(driver);
					objToReturn = new BelkRewardsCreditCardPage(driver).get();
					break;

				case "emailpreferences":
					BrowserActions.clickOnElement(lnkEmailPreferences, driver, "Email Preferences");
					Utils.waitForPageLoad(driver);
					objToReturn = new EmailPreferencesPage(driver).get();
					break;
			}
		}

		return objToReturn;
	}



	/**
	 * To get the order details
	 * @return LinkedList<LinkedHashMap<String, String>> - orderHistoryDetailsSet
	 */
	public LinkedList<LinkedHashMap<String, String>> getOrderHistoryDetails(){
		LinkedList<LinkedHashMap<String, String>> orderHistoryDetailsSet = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < lstOrderNumber.size(); i++) {
			LinkedHashMap<String, String> orderHistoryDetails = new LinkedHashMap<String, String>();
			orderHistoryDetails.put("orderNumber", lstOrderNumber.get(i).getText());
			orderHistoryDetails.put("dateOrdered", lstDateOrdered.get(i).getText());
			orderHistoryDetails.put("orderTotal", lstOrderTotal.get(i).getText());
			orderHistoryDetails.put("orderStatus", lstOrderStatus.get(i).getText());
			orderHistoryDetailsSet.add(orderHistoryDetails);
		}
		return orderHistoryDetailsSet;	
	}

	/**
	 * To click on view details by index
	 * @param index
	 * @throws Exception
	 */
	public void clickOnViewDetailsByIndex(int index) throws Exception{
		BrowserActions.scrollToViewElement(lstViewDetails.get(index-1), driver);
		BrowserActions.clickOnElement(lstViewDetails.get(index-1), driver, "View Details");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the Text from Result hits
	 * @return String - Ex: Viewing 1 - 5 of 10
	 * @throws Exception
	 */
	public String getTextFromResultHits() throws Exception{
		return BrowserActions.getText(driver, lblResultHits, "ResultHits");
	}

	/**
	 * To click on Pagination by page number
	 * @param index
	 * @throws Exception
	 */
	public void clickOnPaginationByPageNumber(int index) throws Exception{
		BrowserActions.clickOnElement(lstPagination.get(index-1), driver, "Pagination");
		Utils.waitForPageLoad(driver);
		Log.assertThat(getCurrentPageNumber() == index
				, "Navigated to the expected page number "+index
				, "Not navigated to the expected page number "+index);
	}

	/**
	 * To get the Text from OrderNumber
	 * @return String - Ex: <number>
	 * @throws Exception
	 */
	public String getTextFromOrderNumber() throws Exception{
		return BrowserActions.getText(driver, orderNumber, "FirstOrderNumber");
	}


	/**
	 * To click on Next Pagination symbol
	 * @param index
	 * @throws Exception
	 */
	public void clickOnNextPageSymbol() throws Exception{

		int beforePageNumber = getCurrentPageNumber();
		BrowserActions.clickOnElement(paginationGraterThanSymbol, driver, "Pagination");		
		Utils.waitForPageLoad(driver);
		int afterPageNumber = getCurrentPageNumber();
		Log.assertThat(afterPageNumber == beforePageNumber + 1
				, "Navigated to the expected page"
				, "Not navigated to the expected page number");
	}
	/**
	 * To click on Previous Pagination symbol
	 * @param index
	 * @throws Exception
	 */
	public void clickOnPreviousPageSymbol() throws Exception{

		int beforePageNumber = getCurrentPageNumber();
		BrowserActions.clickOnElement(paginationLesserThanSymbol, driver, "Pagination");
		Utils.waitForPageLoad(driver);
		int afterPageNumber = getCurrentPageNumber();
		Log.assertThat(afterPageNumber == beforePageNumber - 1
				, "Navigated to the expected page"
				, "Not navigated to the expected page number");
	}
	/**
	 * To click on Last Pagination symbol
	 * @param index
	 * @throws Exception
	 */
	public void clickOnLastPageSymbol() throws Exception
	{				
		BrowserActions.clickOnElement(paginationLastPageSymbol, driver, "Pagination");		
		Utils.waitForPageLoad(driver);
		String afterPageNumber = getTextFromResultHits();		
		//To verify the display of last page check Last Order details are displaying (Viewing 8 - 10 of 10)
		String currentNoOfOrder = afterPageNumber.split(" ")[3];
		String totalNoOfOrder = afterPageNumber.split(" ")[5];
		Log.assertThat(currentNoOfOrder.equals(totalNoOfOrder)
				, "Navigated to the expected page"
				, "Not navigated to the expected page number");
	}
	/**
	 * To click on First Pagination symbol
	 * @param index
	 * @throws Exception
	 */
	public void clickOnFirstPageSymbol() throws Exception
	{	
		BrowserActions.clickOnElement(paginationFirstPageSymbol, driver, "Pagination");		
		Utils.waitForPageLoad(driver);		
		Log.assertThat(getCurrentPageNumber() == 1
				, "Navigated to the expected page"
				, "Not navigated to the expected page number");
	}

	/**
	 * To click on Pagination by page number
	 * @return int
	 * @throws Exception
	 */
	public int getCurrentPageNumber( ) throws Exception{
		return Integer.parseInt(BrowserActions.getText(driver, lblCurrentPage, "Current Page Number"));

	}

	/**
	 * To get the order information from order details
	 * @return LinkedHashMap<String, String> - orderInformation
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getOrderInformationFromOrderDetails() throws Exception{
		LinkedHashMap<String, String> orderInformation = new LinkedHashMap<String, String>();
		String orderedDate=BrowserActions.getText(driver, orderDate, "Order Date");
		String orderedTime=BrowserActions.getText(driver, orderTime, "Order Time");
		String orderedNumber=BrowserActions.getText(driver, orderNumber, "Order Number");
		String orderedStatus=BrowserActions.getText(driver, orderStatus, "Order Status");

		orderInformation.put("orderDate", orderedDate);
		orderInformation.put("orderTime", orderedTime);
		orderInformation.put("orderNumber", orderedNumber);
		orderInformation.put("orderStatus", orderedStatus);

		return orderInformation;	
	}

	/**
	 * To get the billing address in order details page
	 * @return LinkedHashMap<String, String> - billingAddressInOrderHistory
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBillingAddressInOrderDetails() throws Exception{
		LinkedHashMap<String, String> billingAddressInOrderHistory = new LinkedHashMap<String, String>();
		String nameInBilling=BrowserActions.getText(driver, lstbillingAddress.get(0), "Name In Billing");
		String addressInBilling=BrowserActions.getText(driver, lstbillingAddress.get(1), "Address In Billing");
		int i=1;
		String address1InBilling=null;
		if(lstbillingAddress.size()>4){
			i=2;
			address1InBilling=BrowserActions.getText(driver, lstbillingAddress.get(i), "Address1 In Billing");
		}

		String cityInBilling=BrowserActions.getText(driver, lstbillingAddress.get(i=i+1), "city In Billing");
		cityInBilling=cityInBilling.split(",")[0];
		String stateInBilling=BrowserActions.getText(driver, lstbillingAddress.get(i), "State In Billing");
		String[] stateInBillingArray=stateInBilling.split(" ");
		stateInBilling=stateInBillingArray[stateInBillingArray.length-2];
		String zipCodeInBilling=BrowserActions.getText(driver, lstbillingAddress.get(i), "Zipcode In Billing");
		String[] zipCodeInBillingArray=zipCodeInBilling.split(" ");
		zipCodeInBilling=zipCodeInBillingArray[zipCodeInBillingArray.length-1];
		String phoneNumberInBilling=BrowserActions.getText(driver, lstbillingAddress.get(i=i+1), "PhoneNumber In Billing");

		billingAddressInOrderHistory.put("name", nameInBilling);
		billingAddressInOrderHistory.put("address", addressInBilling);
		billingAddressInOrderHistory.put("address1", address1InBilling);
		billingAddressInOrderHistory.put("city", cityInBilling);
		billingAddressInOrderHistory.put("state", stateInBilling);
		billingAddressInOrderHistory.put("zipcode", zipCodeInBilling);
		billingAddressInOrderHistory.put("phonenumber", phoneNumberInBilling);

		return billingAddressInOrderHistory;
	}

	/**
	 * To get payment method in order details page
	 * @return LinkedHashMap<String, String> - paymentMethodInOrderHistory
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getPaymentMethodInOrderDetails() throws Exception{

		LinkedHashMap<String, String> paymentMethodInOrderHistory = new LinkedHashMap<String, String>();

		String cardType=BrowserActions.getText(driver, lblCardType, "Type of Card");
		String nameOnCard=BrowserActions.getText(driver, lblNameOnCard, "Name on card");
		String cardNumber=BrowserActions.getText(driver, lblCardNumber, "Card number");
		String purchaseAmount=BrowserActions.getText(driver, lblAmount, "Purchase amount");

		paymentMethodInOrderHistory.put("cardtype", cardType);
		paymentMethodInOrderHistory.put("nameoncard", nameOnCard);
		paymentMethodInOrderHistory.put("cardNumber", cardNumber);
		paymentMethodInOrderHistory.put("purchaseAmount", purchaseAmount);

		return paymentMethodInOrderHistory;
	}

	/**
	 * To get payment details in order details
	 * @return LinkedHashMap<String, String> - paymentDetailsInOrderHistory
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getPaymentDetailsInOrderHistory() throws Exception{

		LinkedHashMap<String, String> paymentDetailsInOrderHistory = new LinkedHashMap<String, String>();

		String MerchandiseTotal=BrowserActions.getText(driver, paymentdetails.get(0), "Marchandise Total");
		String Shipping=BrowserActions.getText(driver, paymentdetails.get(1), "Shipping");
		String SalesTax=BrowserActions.getText(driver, paymentdetails.get(2), "Sales Tax");
		String OrderTotal=BrowserActions.getText(driver, paymentdetails.get(3), "Order Total");

		paymentDetailsInOrderHistory.put("merchandiseTotal", MerchandiseTotal);
		paymentDetailsInOrderHistory.put("shipping", Shipping);
		paymentDetailsInOrderHistory.put("salesTax", SalesTax);
		paymentDetailsInOrderHistory.put("orderTotal", OrderTotal);

		return paymentDetailsInOrderHistory;
	}

	/**
	 * To click on the printer icon
	 * @throws Exception
	 */
	public void clickOnPrinterIcon() throws Exception{
		BrowserActions.clickOnElement(btnPrinter, driver, "printer icon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on write a review link
	 * @throws Exception
	 */
	public void clickOnWriteReview() throws Exception{
		BrowserActions.clickOnElement(lnkWriteReview, driver, "Write a Review");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Return To Shopping button
	 * @throws Exception
	 */
	public HomePage clickOnReturnToShopping() throws Exception{
		BrowserActions.clickOnElement(btnReturnToShopping, driver, "Return to shopping button");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To click on Back To Order from order details
	 * @throws Exception
	 */
	public void clickOnBackToOrder() throws Exception{
		BrowserActions.clickOnElement(lnkBackToOrderHistory, driver, "Back To OrderHistory link");
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get Shipping Order Title from order details
	 * 
	 * @throws Exception
	 */
	public String getShipmentNumberHeader() throws Exception {
		return BrowserActions.getText(driver, lblShipmentNumber,
				"Shipment Order Title");
	}

	/**
	 * To get Shipping Order Item Name from order details
	 * 
	 * @throws Exception
	 */
	public String getItemName(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemName.get(index-1),"Shipment Order Item Name");
	}

	/**
	 * To get Shipping Order Item UPC from order details
	 * 
	 * @throws Exception
	 */
	public String getItemUPC(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemUPC.get(index-1),"Shipment Order Item UPC");
	}

	/**
	 * To get Shipping Order Item Color from order details
	 * 
	 * @throws Exception
	 */
	public String getItemColor(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemColor.get(index-1),"Shipment Order Item Color");
	}

	/**
	 * To get Shipping Order Item Size from order details
	 * 
	 * @throws Exception
	 */
	public String getItemSize(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemSize.get(index-1),"Shipment Order Item Size");
	}

	/**
	 * To get Shipping Order Item Quantity from order details
	 * 
	 * @throws Exception
	 */
	public String getItemQuantity(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemQuantity.get(index-1),"Shipment Order Item Quantity");
	}

	public String getPageTitle() throws Exception{
		return BrowserActions.getText(driver, txtOrderHistoryTitle, "page title");
	}

	public void clickOnYesInOrderCancelPopUp() throws Exception{
		BrowserActions.clickOnElement(btnYesInOrderCancelPopUp, driver, "Yes Button to cancel the order");
		Utils.waitForPageLoad(driver);
	}

	public String getItemPrice(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemPrice.get(index-1),"Shipment Order Item Price");
	}

	public String getItemTotal(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemTotal.get(index-1),"Shipment Order Item Total");
	}
	public String getItemSubTotal(int index) throws Exception {
		return BrowserActions.getText(driver, lstOrderItemSubTotal.get(index-1),"Shipment Order Item SubTotal");
	}
	
	public String getTextFromBreadcrumb()throws Exception{
		BrowserActions.nap(3);
		if(Utils.getRunPlatForm()=="mobile"){
			try {
				return BrowserActions.getText(driver, breadcrumbMobile, "breadcrumb for mobile");							
			} catch (Exception e) {
				// TODO: handle exception
				return "BreadcrumbMissing";
			}

		}
		return (BrowserActions
				.getText(driver, lstBreadcrumb.get(lstBreadcrumb.size() - 2),
						"breadcrumb value"));
	}
	
	public String getOrderNumberByIndex(int index) throws Exception{
		return BrowserActions.getText(driver, lstOrderNumber.get(index-1), "Order number");
	}
}
