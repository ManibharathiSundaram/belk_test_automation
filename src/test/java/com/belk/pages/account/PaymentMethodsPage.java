package com.belk.pages.account;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.ElementLayer;
import com.belk.pages.ElementLayer;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.PaymentUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class PaymentMethodsPage extends LoadableComponent<PaymentMethodsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Footers footers;
	public Headers headers;
	public NeedHelpSection needHelpSection;
	public SidePannel sidePannel;

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");

	/**********************************************************************************************
	 *********************** WebElements of RegistryGuestUserPage *********************************
	 **********************************************************************************************/

	@FindBy(css = "div[class*='paymentslist'] a[class*='add-card']")
	WebElement btnAddPaymentMethod;

	/**************** Section - Links *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	/*********************************************************************/

	// *******mobile elements***************

	/*
	 * @FindBy(css = "h3 > a[title='']") WebElement lnkMyAccountMobile;
	 */

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	public String btnCardType = "#dwfrm_paymentinstruments_creditcards_newcreditcard_type + .selected-option";

	public String txtNameOnCard = "#dwfrm_paymentinstruments_creditcards_newcreditcard_owner";

	public String txtCardNo = "input[id*='dwfrm_paymentinstruments_creditcards_newcreditcard_number']";

	public String btnCardExpMonth = "#dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month + .selected-option";

	public String btnCardExpYear = "#dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year + .selected-option";

	public String chkMakeDefaltPaymentMethod = "#dwfrm_paymentinstruments_creditcards_newcreditcard_defaultcard";

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_defaultcard")
	WebElement chkDefaltPaymentMethod;

	@FindBy(css = ".error>.field-wrapper>.error-message")
	WebElement ermsgInvalidCardNumber;

	@FindBy(css = "#applyBtn")
	WebElement btnSave;

	@FindBy(css = ".cancel.cancel-button")
	WebElement btnCancel;

	@FindBy(css = ".button-fancy-large.secondarybutton.address-savechanges")
	WebElement btnSaveChanges;

	@FindBy(css = "div.account-payment-list-empty > p")
	private WebElement lblNoPaymentMethodsAddedMessage;

	@FindBy(css = "#CreditCardForm")
	private WebElement frmAddPaymentMethodForm;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".account-mini-items")
	List<WebElement> lstSavedPayment;

	@FindBy(css = "input[name='dwfrm_paymentinstruments_defaultPaymentMethod_defaultPayment']")
	List<WebElement> rdoMakeDefaultPayment;

	@FindBy(css = ".payment-remove.delete")
	List<WebElement> lnkDeletePayment;

	@FindBy(css = ".address-delete > .button")
	WebElement btnConfirmDeletePayment;


	@FindBy(css = ".address-delete .cancel")
	WebElement btnCancelDeletePayment;     

	@FindBy(css = "#dialog-container > p")
	WebElement lblMsgInDeletePaymentDialog; 


	@FindBy(css=".payment-remove.delete")
	List<WebElement> lnkDeleteFromPaymentMethod;

	@FindBy(css = "#dialog-container")
	WebElement PaymentPopUp;

	@FindBy(css=".address-title .bold")
	List<WebElement> lstPaymentSectionHeader;
	
	@FindBy(css="a[title*='Delete'][class='button']")
	WebElement btnYesInDeletePayment;
	
	@FindBy(css="div[class='address-delete'] a[class*='close-dialog']")
	WebElement btnNoInDeletePayment;
	
	@FindBy(css = "#dialog-container>h1")
	WebElement lblAddressFormDialogContainerHeader;


	/**********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public PaymentMethodsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, btnAddPaymentMethod))) {
			Log.fail("Edit Page did not open!", driver);
		}
		elementLayer = new ElementLayer(driver);

		headers= new Headers(driver);
		footers= new Footers(driver);
		elementLayer= new ElementLayer(driver);
		sidePannel= new SidePannel(driver);
		needHelpSection = new NeedHelpSection(driver);

	}

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver, "Profile Link");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver, "Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile, driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver, "Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver, "Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver, "Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver, "Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver, "Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver, "WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver, "WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver, "Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver, "Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}

	/**
	 * To click on add payment method button
	 * 
	 * @throws Exception
	 */
	public void clickOnAddPaymentMethod() throws Exception {
		BrowserActions.scrollToViewElement(btnAddPaymentMethod, driver);
		BrowserActions.clickOnElement(btnAddPaymentMethod, driver, "AddPaymentMethod button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To fill the Card Details
	 * 
	 * @param MakeDefaultPayment
	 * @param cardDetails
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingCardDetails(String MakeDefaultPayment, String cardDetails)
			throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetail = new LinkedHashMap<String, String>();
		String cutomercardDetails = checkoutProperty.getProperty(cardDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];
		cardDetail.put("select_cardtype_" + cardType, btnCardType);
		cardDetail.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetail.put("type_cardno_" + cardNo, txtCardNo);
		if(!cardType.equals("Belk Rewards Credit Card")) {
			String cardExpireMonth = cutomercardDetails.split("\\|")[3];
			String cardExpireYear = cutomercardDetails.split("\\|")[4];
			cardDetail.put("select_expmonth_" + cardExpireMonth, btnCardExpMonth);
			cardDetail.put("select_expyr_" + cardExpireYear, btnCardExpYear);
		}
		if (MakeDefaultPayment.equals("YES")) {
			cardDetail.put("check_Make default Payment_YES", chkMakeDefaltPaymentMethod);
		}
		PaymentUtils.enterCardDetails(cardDetail, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return cardDetail;
	}

	/**
	 * To click on the save button from the add payment popup
	 * 
	 * @throws Exception
	 */
	public void clickOnSave() throws Exception {
		BrowserActions.clickOnElement(btnSave, driver, "Save button");
		waitUntilSearchSpinnerDisappear();
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To click on the cancel button from the add payment popup
	 * 
	 * @throws Exception
	 */
	public void clickOnCancel() throws Exception {
		BrowserActions.clickOnElement(btnCancel, driver, "Cancel button");
	}

	/**
	 * To click on save changes button
	 * 
	 * @throws Exception
	 */
	public void clickOnSaveChanges() throws Exception {
		BrowserActions.scrollToViewElement(btnSaveChanges, driver);
		BrowserActions.clickOnElement(btnSaveChanges, driver, "Save changes");
	}

	public String getNoPaymentMethodsAddedMessage() {

		return lblNoPaymentMethodsAddedMessage.getText();
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
		if (Utils.getRunPlatForm().equals("desktop")) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	/**
	 * Get saved payment by index
	 * 
	 * @param index
	 *            - nth payment
	 * @return LinkedHashMap
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getSavedPaymentByIndex(int index)
			throws Exception {
		return getSavedPayment().get(index);
	}

	public void mouseOverDelete() throws InterruptedException {
		BrowserActions.scrollToView(lnkDeletePayment.get(0), driver);
		BrowserActions.mouseHover(driver, lnkDeletePayment.get(0));
		BrowserActions.nap(3);
	}

	/**
	 * get the text from Delete tooltip in Edit address dialog
	 * 
	 * @throws Exception
	 */
	public String getDeleteTooltipText() throws Exception {
		mouseOverDelete();
		return BrowserActions.getTextFromAttribute(driver, lnkDeletePayment.get(0), "title", "Delete Link Tooltip");
	}

	/**
	 * To click Delete Payment link
	 * 
	 * @throws Exception
	 */
	public void clickOnDeletePaymentByIndex(int index) throws Exception{
		BrowserActions.scrollToViewElement(lnkDeletePayment.get(index), driver);
		BrowserActions.clickOnElement(lnkDeletePayment.get(index), driver, "Delete link");
		waitUntilSearchSpinnerDisappear();
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get Message on Delete Payment Modal
	 * return String
	 * @throws Exception
	 */
	public String getMessageInDeletePaymentModal() throws Exception{
		return BrowserActions.getText(driver, lblMsgInDeletePaymentDialog, "Message on Delete Payment Modal");

	}


	/**
	 * To click Confirm on Delete Payment Modal
	 * 
	 * @throws Exception
	 */
	public void clickConfirmInDeletePaymentModal() throws Exception{
		BrowserActions.javascriptClick(btnConfirmDeletePayment, driver,"Confirm in Delete payment section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click Cancel on Delete Payment Modal
	 * 
	 * @throws Exception
	 */
	public void clickCancelInDeletePaymentModal() throws Exception{
		BrowserActions.javascriptClick(btnCancelDeletePayment, driver,"Cancel button in Delete payment modal");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Delete link in Payment by Index
	 * 
	 * @param index
	 *            - nth address
	 * @throws Exception
	 */
	public void deletePaymentByIndex(int index) throws Exception {
		clickOnDeletePaymentByIndex(index);
		BrowserActions.javascriptClick(btnConfirmDeletePayment, driver,"Confirm Delete in Delete payment section");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to get the saved Payment details in Payment Details page
	 * 
	 * @return linked list of linked hash map HashMap Key Value 'cardType' -
	 *         'cardOwner' - 'cardNO' - 'cardExpireMonth' - 'cardExpireYear' -
	 *			'defaultPayment' - 'YES' or 'NO'
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getSavedPayment()throws Exception {
		LinkedList<LinkedHashMap<String, String>> paymentDetails = new LinkedList<LinkedHashMap<String, String>>();
		String cardExpiryDate = null;
		String defaultPayment = null;
		for (int i = 0; i < lstSavedPayment.size(); i++) {
			LinkedHashMap<String, String> payment = new LinkedHashMap<String, String>();

			String cardType = lstSavedPayment.get(i).findElement(By.cssSelector(".cc-type")).getText().trim();				
			String cardOwner = lstSavedPayment.get(i).findElement(By.cssSelector(".cc-owner")).getText().trim();
			String cardNO = lstSavedPayment.get(i).findElement(By.cssSelector(".cc-card-group>.cc-number")).getText().trim();
			if(!cardType.equals("Belk Reward Credit Card")) {
				cardExpiryDate = lstSavedPayment.get(i).findElement(By.cssSelector(".cc-card-group>.cc-exp")).getText().trim();
				defaultPayment = BrowserActions.getRadioOrCheckboxChecked(lstSavedPayment.get(i).findElement(By.cssSelector("input[name='dwfrm_paymentinstruments_defaultPaymentMethod_defaultPayment']")));
			}

			payment.put("cardtype", cardType);
			payment.put("cardowner", cardOwner);
			payment.put("cardno", cardNO);
			if(!cardType.equals("Belk Reward Credit Card")) {
				payment.put("cardexpirydate", cardExpiryDate);
				payment.put("defaultpayment", defaultPayment);
			}

			paymentDetails.add(payment);
		}

		return paymentDetails;
	}


	/**
	 * To set Default Payment
	 * 
	 * @param payment
	 *            index
	 * @throws Exception
	 */
	public void setDefaultPaymentByIndex(int index) throws Exception {
		BrowserActions.selectRadioOrCheckbox(rdoMakeDefaultPayment.get(index), "YES");
	}

	/**
	 * Clicking on Save Changes button in Address Book
	 * 
	 * @throws Exception
	 */
	public void clickSaveChanges() throws Exception {
		BrowserActions.clickOnElement(btnSaveChanges, driver, "Save Changes");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the Card Details in Add Payment Form
	 * 
	 * @param MakeDefaultPayment
	 * @param cardDetails
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getCardDetailsAddForm()throws Exception 
	{

		LinkedHashMap<String, String> cardDetail = new LinkedHashMap<String, String>();

		String cardType = BrowserActions.getText(driver, btnCardType, "Card Type").trim();
		String cardOwner = BrowserActions.getText(driver, txtNameOnCard, "Card Owner").trim();
		String cardNo = BrowserActions.getText(driver, txtCardNo, "Card Owner").trim();

		String cardExpireMonth = BrowserActions.getText(driver, btnCardExpMonth, "Card Owner").trim();
		String cardExpireYear = BrowserActions.getText(driver, btnCardExpYear, "Card Owner").trim();

		String cardDefaultPayment = BrowserActions.getRadioOrCheckboxChecked(chkDefaltPaymentMethod);


		cardDetail.put("cardType", cardType);
		cardDetail.put("cardOwner", cardOwner);
		cardDetail.put("cardNo", cardNo);
		cardDetail.put("cardExpireMonth", cardExpireMonth);
		cardDetail.put("cardExpireYear", cardExpireYear);
		cardDetail.put("cardDefaultPayment", cardDefaultPayment);

		BrowserActions.nap(2);
		return cardDetail;
	}

	/**
	 * get text of invalid card number
	 * 
	 * @throws Exception
	 */
	public String getErrorMesssageOfInvalidCardNumber() throws Exception {
		return BrowserActions.getText(driver, ermsgInvalidCardNumber,
				"Invalid Card Number Error Message.");
	}

	public int getTotalPaymentCount(){
		return lstSavedPayment.size();
	}

	public boolean verifyPopupAvailability(){
		waitUntilSearchSpinnerDisappear();
		return !(Utils.waitForElement(driver, PaymentPopUp));
	}

	// ---------- Spinner ------------ //

	@FindBy(css = ".loader[style*='block']")
	WebElement paymentMethodsSpinner;

	/**
	 * To wait for Search results spinner
	 */
	public void waitUntilSearchSpinnerDisappear() {
		BrowserActions.nap(3);
		Utils.waitUntilElementDisappear(driver, paymentMethodsSpinner);
	}

	/**
	 * To get the Saved Address location on screen in My Accout page
	 * 
	 * @param index
	 * @return Location on screen
	 * @throws Exception
	 */
	public int getLocationofSavedPayment(int index)
	{
		int	location = lstSavedPayment.get(index-1).getLocation().getY();		
		return location;
	}

	public int verifyPaymentBelowAdditionalAndReturnSize(){
		List<WebElement> e=lstPaymentSectionHeader.get(0).findElements(By.xpath("../following-sibling::li"));
		return e.size();
	}

	public boolean verifySaveChangesBelowAddress(){
		if(Utils.waitForElement(driver, lstSavedPayment.get(0).findElement(By.xpath("../../following-sibling::button"))))
			return true;
		return false;
	}
	
	public void clickOnNoInDeleteConfirmation() throws Exception{
		Utils.waitForElement(driver, btnNoInDeletePayment);
		BrowserActions.clickOnElement(btnNoInDeletePayment, driver, "No in Delete payment");
		Utils.waitForPageLoad(driver);
	}
	
	public void clickOnYesInDeleteConfirmation() throws Exception{
		Utils.waitForElement(driver, btnYesInDeletePayment);
		BrowserActions.clickOnElement(btnYesInDeletePayment, driver, "No in Delete payment");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify Payment Method Tile
	 * @param titleName - 'Default Payment Method'/'Additional Payment Methods'
	 * @return
	 * @throws Exception
	 */
	public boolean verifyPaymentMethodTitle(String titleName) throws Exception {
		boolean status = false;
		for(WebElement element:lstPaymentSectionHeader) {
			element.getText();
			if(BrowserActions.getText(driver, element, "Payment Method").trim().equalsIgnoreCase(titleName)) {
				status = true;
				break;
			}
		}
		return status;
	}
	
	/**
	 * To get the header text from the dialog container
	 * @return
	 * @throws Exception
	 */
	public String getTextFromDialogContainerHeader() throws Exception{
		return BrowserActions.getText(driver, lblAddressFormDialogContainerHeader, "Dialog container header");
	}
}
