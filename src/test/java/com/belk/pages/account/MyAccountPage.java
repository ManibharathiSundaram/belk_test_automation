package com.belk.pages.account;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.CreateAccountPage;
import com.belk.pages.ElementLayer;
import com.belk.pages.MiniCartPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class MyAccountPage extends LoadableComponent<MyAccountPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	public MiniCartPage miniCartPage;
	public CreateAccountPage createAccountPage;

	String runPltfrm = Utils.getRunPlatForm();
	/**********************************************************************************************
	 ********************************* WebElements of My Account Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".account-overview")
	WebElement divMyAccount;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	@FindBy(css = "a.mini-cart-link")
	WebElement lnkMiniCart;

	@FindBy(css = ".mini-cart-content[style*='block']")
	WebElement miniCartContent;

	@FindBy(css = ".fa-payment.account-landing-payments")
	WebElement iconPaymethod;

	@FindBy(css = ".cc-type")
	WebElement fldSavedCardtype;

	@FindBy(css = "div[class='user-actions'] a")
	WebElement lnkdeletecard;

	@FindBy(css = ".cc-owner")
	WebElement fldCardOwner;

	@FindBy(css = ".address-delete a.button[title='Delete this address']:nth-child(1)")
	WebElement lnkYesInDeleteCard;

	/**************** Section - Links *********************************/
	@FindBy(css = "h3 > a[title='Profile']")
	WebElement lnkMyAccount;

	@FindBy(css = "a[title='Order History']")
	WebElement lnkOrderHistory;

	@FindBy(css = "a[title='Address Book']")
	WebElement lnkAddressBook;

	@FindBy(css = "a[title='Payment Methods']")
	WebElement lnkPaymentMethods;
	
	@FindBy(css=".secondary-navigation a[title='Registry']")
	WebElement lnkRegistryLeftNav;

	@FindBy(css = "a[title='Registry']")
	WebElement lnkRegistry;

	@FindBy(css = "a[title='Registry FAQs (TEST)']")
	WebElement lnkFAQ;
	
	@FindBy(css=".secondary-navigation a[title='Wish List']")
	WebElement lnkWishListLeftNav;

	@FindBy(css = "a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "a[title='Belk Rewards Credit Card']")
	WebElement lnkBelkRewardsCreditCard;

	@FindBy(css = "a[title='Email Preferences']")
	WebElement lnkEmailPreferences;
	
	@FindBy(css = ".fa-address.account-landing-addresses")
	WebElement lnkMyAccountPageMobile;

	// *******mobile elements***************

	/*
	 * @FindBy(css = "h3 > a[title='']") WebElement lnkMyAccountMobile;
	 */

	@FindBy(css = "a[title='Show or update your personal information']")
	WebElement lnkProfileMobile;

	@FindBy(css = "a[title='Check the status of your orders or see past orders']")
	WebElement lnkOrderHistoryMobile;

	@FindBy(css = "a[title='Manage your billing and shipping addresses']")
	WebElement lnkAddressBookMobile;

	@FindBy(css = "a[title='Manage credit cards']")
	WebElement lnkPaymentMethodsMobile;

	@FindBy(css = "a[title='View and modify your gift registries']")
	WebElement lnkRegistryMobile;

	@FindBy(css = "a[title='View and modify items on your list or invite friends']")
	WebElement lnkWishListMobile;

	// @FindBy(css=".content-asset>ul>li>a[title='Profile']")
	// WebElement lnkProfile;

	@FindBy(css = ".default-shipping-address>div>div:nth-child(2)")
	WebElement txtShippingName;

	@FindBy(css = ".default-shipping-address>div>div:nth-child(3)")
	WebElement txtAddressOne;

	@FindBy(css = ".default-shipping-address>div>span:nth-child(5)")
	WebElement txtCity;

	@FindBy(css = ".default-shipping-address>div>span:nth-child(6)")
	WebElement txtPhoneNo;

	@FindBy(css = ".default-billing-address>div>div:nth-child(2)")
	WebElement txtBillingName;

	@FindBy(css = ".default-billing-address>div>div:nth-child(3)")
	WebElement txtBillingAddressOne;

	@FindBy(css = ".default-billing-address>div>span:nth-child(5)")
	WebElement txtBillingCity;

	@FindBy(css = ".default-billing-address>div>span:nth-child(6)")
	WebElement txtBillingPhoneNo;

	@FindBy(css = "#dwfrm_profile_login_currentpassword_d0asrsjbmgjo")
	WebElement txtOldPwd;

	@FindBy(css = "#dwfrm_profile_login_newpassword_d0agynzqymmb")
	WebElement txtNewPwd;

	@FindBy(css = "#dwfrm_profile_login_newpasswordconfirm_d0jtpvjfuyud")
	WebElement txtConfirmNewPwd;

	@FindBy(css = "form[id='ChangePassowrdForm']>fieldset>.form-row.form-row-button>button")
	WebElement btnPwdApplyChanges;

	@FindBy(css = ".default-shipping-address")
	WebElement txtdefaultShippingAddressinMyAccountpage;

	@FindBy(css = ".default-billing-address")
	WebElement txtdefaultBillingAddressinMyAccountpage;

	@FindBy(css = ".mini-cart-content")
	WebElement minicartContent;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement btnViewBag;

	@FindBy(css = "div[id='main']>div[id='secondary']>div[class='secondary-navigation']>nav>div>ul>li:nth-child(3)>a")
	WebElement lnkMyAccountPage;

	@FindBy(css = "#main>div[id='primary']>div>a")
	WebElement addAddress;
	
	@FindBy(css = ".button-text.delete-item.simple")
	WebElement lnkRemove;

	//added from school net team
	
	@FindBy(css = ".account-options>li>ul>li>a")
	List<WebElement> ContentOptionsInLandingPage;
	
	@FindBy(css=".hide-desktop .need-data")
	WebElement txtAccountNavigationInMobile;

	@FindBy(css=".hide-mobile .need-data")
	WebElement txtAccountNavigationInDesktop;
	
	@FindBy(css=".fa-registry.account-landing-registry")
	WebElement iconRegistry;
	
	/*********************************************************************/
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public MyAccountPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// MyAccountPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divMyAccount))) {
			Log.fail("My Account page did not open up.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		miniCartPage = new MiniCartPage(driver).get();
		createAccountPage = new CreateAccountPage(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load

	@FindBy(css = "li > a[title='Profile']")
	WebElement lnkProfile;

	/**
	 * To navigate to desired section
	 * 
	 * @param section
	 *            - without parameter - MyAccountPage 'profile' - ProfilePage
	 *            'orderhistory' - OrderHistoryPage 'addressbook' -
	 *            AddressBookPage 'paymentmethods' - PaymentMethodsPage
	 *            'registry' - RegistrySignedUserPage 'faq' - FAQPage 'wishlist'
	 *            - WishListPage 'belkrewardcreditcard' -
	 *            BelkRewardsCreditCardPage 'emailpreferences' -
	 *            EmailPreferencesPage
	 * @return AddressBookPage instance
	 * @throws Exception
	 */
	public Object navigateToSection(String... section) throws Exception {
		Object objToReturn = null;
		String platForm = Utils.getRunPlatForm();
		if (section.length < 0) {
			BrowserActions.clickOnElement(lnkMyAccount, driver, "My Account");
			Utils.waitForPageLoad(driver);
			objToReturn = new MyAccountPage(driver).get();
		} else {
			switch (section[0].toLowerCase()) {
			case "profile":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkProfile, driver,
							"Profile Link");
				else
					BrowserActions.clickOnElement(lnkProfileMobile,
							driver, "Profile Link");
				Utils.waitForPageLoad(driver);
				objToReturn = new ProfilePage(driver).get();
				break;

			case "orderhistory":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkOrderHistory, driver,
							"Order History");
				else
					BrowserActions.clickOnElement(lnkOrderHistoryMobile,
							driver, "Order History");
				Utils.waitForPageLoad(driver);
				objToReturn = new OrderHistoryPage(driver).get();
				break;

			case "addressbook":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkAddressBook, driver,
							"Address Book");
				else
					BrowserActions.clickOnElement(lnkAddressBookMobile, driver,
							"Address Book");
				Utils.waitForPageLoad(driver);
				objToReturn = new AddressBookPage(driver).get();
				break;

			case "paymentmethods":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkPaymentMethods, driver,
							"Payment Methods");
				else
					BrowserActions.clickOnElement(lnkPaymentMethodsMobile,
							driver, "Payment Methods");
				Utils.waitForPageLoad(driver);
				objToReturn = new PaymentMethodsPage(driver).get();
				break;

			case "registry":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkRegistry, driver,
							"Registry");
				else
					BrowserActions.clickOnElement(lnkRegistryMobile, driver,
							"Registry");
				Utils.waitForPageLoad(driver);
				objToReturn = new RegistrySignedUserPage(driver).get();
				break;

			case "faq":
				BrowserActions.clickOnElement(lnkFAQ, driver, "FAQ");
				Utils.waitForPageLoad(driver);
				objToReturn = new FAQPage(driver).get();
				break;

			case "wishlist":
				if (platForm.equals("desktop"))
					BrowserActions.clickOnElement(lnkWishList, driver,
							"WishList");
				else
					BrowserActions.clickOnElement(lnkWishListMobile, driver,
							"WishList");
				Utils.waitForPageLoad(driver);
				objToReturn = new WishListPage(driver).get();
				break;

			case "belkrewardcreditcard":
				BrowserActions.clickOnElement(lnkBelkRewardsCreditCard, driver,
						"Belk Reward Credit Card");
				Utils.waitForPageLoad(driver);
				objToReturn = new BelkRewardsCreditCardPage(driver).get();
				break;

			case "emailpreferences":
				BrowserActions.clickOnElement(lnkEmailPreferences, driver,
						"Email Preferences");
				Utils.waitForPageLoad(driver);
				objToReturn = new EmailPreferencesPage(driver).get();
				break;
			}
		}

		return objToReturn;
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb() throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (runPltfrm == "desktop") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;

	}

	public MiniCartPage mouseOverMiniCart() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (!Utils.waitForElement(driver, miniCartContent, 2)) {
			if (runPltfrm == "desktop")
				BrowserActions.mouseHover(driver, miniCart);
		}

		if (Utils.waitForElement(driver, miniCartContent, 2)) {
			Log.event("Mini cart overlay already opened");
		} else {
			if (runPltfrm == "desktop")
				BrowserActions.mouseHover(driver, miniCart);
			else if (runPltfrm == "mobile")
				BrowserActions.javascriptClick(lnkMiniCart, driver,
						"mini cart ");
		}
		Utils.waitForPageLoad(driver);
		return new MiniCartPage(driver).get();
	}

	public void navigateToPayMethod() throws Exception {
		BrowserActions.clickOnElement(iconPaymethod, driver,
				"Icon payment method");

	}

	public String getTextFromCardType() throws Exception {
		return BrowserActions.getText(driver, fldSavedCardtype, "Card type");
	}

	public String getTextFromCardOwner() throws Exception {
		return BrowserActions.getText(driver, fldCardOwner, "Card type");
	}

	/**
	 * To delete the payment card from the account
	 * 
	 * @throws Exception
	 */
	public void deleteCardFromAccount() throws Exception {
		BrowserActions.clickOnElement(lnkdeletecard, driver, "deleted link");
		BrowserActions
				.clickOnElement(lnkYesInDeleteCard, driver, "Yes in card");
		Utils.waitForPageLoad(driver);
	}

	// //////////////////// Muthu//////////////////////////////

	@FindBy(css = ".account-logout>a")
	WebElement lnkLogout;

	@FindBy(css = ".myaccount-username")
	WebElement lblUserName;

	// ////////// Left Navigation//////////////////

	@FindBy(css = ".secondary-navigation .content-asset > ul > li > a")
	List<WebElement> lstLeftNavigation;

	@FindBy(css = ".account-options>li>ul>li>a>h2")
	List<WebElement> lstOptionsInLandingPage;

	/**
	 * To click on the tab by name in account landing
	 * 
	 * @param tab
	 *            - Ex: Profile, Order History, Address Book, Payment Methods,
	 *            Registry, FAQs, Wish List
	 * @throws Exception
	 */
	public void clickOnTabFromAccountLanding(String tab) throws Exception {
		try {
			WebElement element = BrowserActions.getMachingTextElementFromList(
					lstOptionsInLandingPage, tab, "contains");
			BrowserActions.clickOnElement(element, driver, tab);
		} catch (Exception e) {
			Log.event("The tab is not available or the tab name is mismatched.");
		}
	}

	/**
	 * To click on the Logout link
	 * 
	 * @return SignIn page
	 * @throws Exception
	 */
	public SignIn clickOnLogout() throws Exception {

		BrowserActions.clickOnElement(lnkLogout, driver, "Logout link");
		return new SignIn(driver).get();

	}

	/**
	 * To get the username
	 * 
	 * @return String - Username (FirstName and LastName)
	 * @throws Exception
	 */
	public String getUsername() throws Exception {
		return BrowserActions.getText(driver, lblUserName, "User name");
	}

	public AddressBookPage navigateToAddressBook() throws Exception {
		if (runPltfrm == "desktop")
		BrowserActions.clickOnElement(lnkAddressBook, driver, "Address Book");
		else if(runPltfrm == "mobile")
			BrowserActions.clickOnElement(lnkAddressBookMobile, driver, "Address Book");

		return new AddressBookPage(driver).get();
	}

	public ProfilePage navigateToEditProfilePage() throws Exception {
		if (runPltfrm == "desktop") 
			BrowserActions.clickOnElement(lnkProfile, driver,
					"Clicked on 'MyAccount-> Profile' link");
			if (runPltfrm == "mobile")
				BrowserActions.clickOnElement(lnkProfileMobile, driver,
						"Clicked on 'MyAccount-> Profile' link");
			return new ProfilePage(driver).get();
	}

	/**
	 * To mouse hover to mini cart
	 * 
	 * @return
	 * @throws Exception
	 */
	public ShoppingBagPage clickOnMiniCart() throws Exception {
		if (Utils.waitForElement(driver, minicartContent))
			if (minicartContent.getCssValue("display").contains("block"))
				BrowserActions.nap(10);
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			MiniCartPage minicart = new MiniCartPage(driver).get();
			int productCount = Integer.parseInt(minicart.getCartProductCount());
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
			if (productCount != 0)
				BrowserActions.clickOnElement(btnViewBag, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public LinkedHashMap<String, String> getDefaultShippingDetails()
			throws Exception {
		BrowserActions.nap(5);
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

		String firstName = BrowserActions.getText(driver, txtShippingName,
				"First Name in Shipping Address");
		String addressOne = BrowserActions.getText(driver, txtAddressOne,
				"Address 1 in Shipping Address");

		String city = BrowserActions.getText(driver, txtCity,
				"City in Shipping Address");
		String phoneNo = BrowserActions.getText(driver, txtPhoneNo,
				"Phone No in Shipping Address");

		shippingDetails.put("FirstName", firstName);
		shippingDetails.put("AddressOne", addressOne);
		shippingDetails.put("City", city);
		shippingDetails.put("phoneNo", phoneNo);

		return shippingDetails;

	}

	public LinkedHashMap<String, String> getDefaultBillingDetails()
			throws Exception {
		BrowserActions.nap(5);
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();

		String firstName = BrowserActions.getText(driver, txtBillingName,
				"First Name in Shipping Address");
		String addressOne = BrowserActions.getText(driver,
				txtBillingAddressOne, "Address 1 in Shipping Address");

		String city = BrowserActions.getText(driver, txtBillingCity,
				"City in Shipping Address");
		String phoneNo = BrowserActions.getText(driver, txtBillingPhoneNo,
				"Phone No in Shipping Address");

		billingDetails.put("FirstName", firstName);
		billingDetails.put("AddressOne", addressOne);
		billingDetails.put("City", city);
		billingDetails.put("phoneNo", phoneNo);

		return billingDetails;

	}

	/**
	 * To get text from Shipping Address in my account
	 * @return String
	 * @throws Exception
	 * Last modified By- Anjana & Last modified Date - 1/2/2017 
	 */
	public String gettextfromShippingAddressInMyAccount() throws Exception {

		if (runPltfrm == "desktop") {
			BrowserActions.clickOnElement(lnkProfile, driver, "Profile LinK");
			Utils.waitForPageLoad(driver);
		} else if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(lnkProfileMobile, driver, "Profile Link");
			Utils.waitForPageLoad(driver);
		}
		String shippingaddress = BrowserActions.getText(driver, txtdefaultShippingAddressinMyAccountpage,
				"Get text from default address");
		return shippingaddress;
	}

	public String getextfromBillingAddressInMyAccount() throws Exception {
		String billingaddress = BrowserActions.getText(driver,
				txtdefaultBillingAddressinMyAccountpage,
				"Get text from default address");

		return billingaddress;
	}

	

	public void clickOnAddAddress() throws Exception {
		BrowserActions.clickOnElement(addAddress, driver, "My Account link");
	}
	
	/**
	 * Get the inner content of section by name in account landing
	 * 
	 * @param section
	 *            - Ex: Profile, Order History, Address Book, Payment Methods,
	 *            Registry, FAQs, Wish List
	 * @throws Exception
	 */
	public String getContentInAccountLanding(String section) throws Exception {
		String innerContent = null;
		try {
			WebElement element = BrowserActions.getMachingTextElementFromList(ContentOptionsInLandingPage, section, "contains");		
			innerContent = BrowserActions.getText(driver, element, "Inner content from the section landing page");
		} catch (Exception e) {
			Log.event("The tab is not available or the tab name is mismatched.");
		}
		return innerContent;
	}

	/**
	 * To verify the display of logout link
	 * 
	 * @return string
	 * @throws InterruptedException
	 */
	public String getLogoutext() throws Exception {
		return BrowserActions.getText(driver, lnkLogout, "Logout");
	}

	public OrderHistoryPage navigateToOrderHistoryPage() throws Exception {

		if (runPltfrm == "desktop")
			BrowserActions.clickOnElement(lnkOrderHistory, driver, "Clicked on 'MyAccount-> OrderHistory' link");
		else if (runPltfrm == "mobile")
			BrowserActions.clickOnElement(lnkOrderHistoryMobile, driver, "Clicked on 'MyAccount-> OrderHistory' link");
		return new OrderHistoryPage(driver).get();
	}

    public PaymentMethodsPage navigateToPaymentMethodsPage()throws Exception  {
          if (runPltfrm == "desktop")
          BrowserActions.clickOnElement(lnkPaymentMethods, driver, "Address Book");
          else if(runPltfrm == "mobile")
                BrowserActions.clickOnElement(lnkPaymentMethodsMobile, driver, "Address Book");

          return new PaymentMethodsPage(driver).get();
          
    }
    



	
	public void navigateToMyAddressBook() throws Exception {
		if (runPltfrm == "mobile"){
		BrowserActions.clickOnElement(lnkMyAccountPageMobile, driver,"My Account link");	
		}else{
		BrowserActions.clickOnElement(lnkMyAccountPage, driver,"My Account link");
	}
	}  
	
	public String getTextFromAttributeOfBRCC() throws Exception{
		return BrowserActions.getTextFromAttribute(driver, lnkBelkRewardsCreditCard, "href", "belk reward credit card");
	}
	
	public void ClickOnRegistryicon() throws Exception
    {
    	BrowserActions.clickOnElement(iconRegistry, driver, "Registry icon in Center");
    }
    
    
}// MyAccountPage
