package com.belk.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;

public class FindInStorePage extends LoadableComponent<FindInStorePage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	/**********************************************************************************************
	 ********************************* WebElements of Find Store Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_store-locator")
	WebElement divFindStores;
	
	@FindBy(id = "dwfrm_storelocator_postalCode")
	WebElement fldZipcode;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;
	
	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;
	/**********************************************************************************************
	 ********************************* WebElements of Find Store Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public FindInStorePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// WishListPage

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divFindStores))) {
			Log.fail("Find in Store page did not open up.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}// load
	
	/**
	 * To type zipcode value in Zipcode field
	 * @param zipCodeValue
	 * @throws Exception 
	 */
	public void typeZipCode(String zipCodeValue) throws Exception {
		BrowserActions.typeOnTextField(fldZipcode, zipCodeValue, driver, "Zipcode field");
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb(String... runMode)
			throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (runPltfrm == "desktop" || (runMode.length > 0)) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}
	
	/**
	 * To get entered zipcode value in Zipcode field
	 * @throws Exception 
	 */
	public String getZipCodeEnteredValue() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, fldZipcode, "value", "Value displayed in Zipcode field");
	}
}// WishListPage
