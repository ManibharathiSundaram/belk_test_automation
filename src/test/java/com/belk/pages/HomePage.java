package com.belk.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.Footers;
import com.belk.pages.footers.StoreLocationPage;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.SearchUtils;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class HomePage extends LoadableComponent<HomePage> {

	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public MiniCartPage minicart;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	public static final String SEARCH_KEYWORD_STRING = "What can we help you find?";
	public static final String SEARCH_KEYWORD_STRING_MOBILE = "Search Message";
	public static final String EMAIL_SIGNUP_ERROR_MSG = "Please enter a valid email address.";
	public static final String lISTOFSEARCHSUGGESTIONS = ".product-suggestions";
	public static final String NORMALPRICE = ".product-price div[class*='no-standard-price']";

	/**********************************************************************************************
	 ********************************* WebElements of Home Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".header-search #q")
	public WebElement txtSearch;
	
	@FindBy(xpath =".//*[@class='hitgroup']/div[1]/a")
	WebElement txtPageContents;

	@FindBy(xpath = "//*[@class='search-suggestion-wrapper full']//*[@class='phrase-suggestions']//*[contains(text(),'buying shoes 2')]")
	WebElement txtPageContent;
	
	@FindBy(css = ".product-sales-price.no-standard-price")
	List<WebElement> srchSuggtnPrice;

	@FindBy(css = "form[name='simpleSearch'] button")
	WebElement btnSearch;

	@FindBy(css = "button[title='Close']")
	WebElement btnEmailClosePopup;

	@FindBy(css = "label[for='q'] span")
	WebElement lblSearchText;

	@FindBy(css = "label[for='q'] span")
	WebElement lblSearchTextMobile;

	@FindBy(css = ".search-phrase a span")
	WebElement lnksearch;

	@FindBy(css = "div[id='mini-cart']")
	WebElement miniCart;

	@FindBy(css = "div[class='phrase-suggestions'] div[class='hitgroup'] a[class='hit']")
	List<WebElement> searchLink;

	@FindBy(css = "div[class='search-suggestion-wrapper full']")
	WebElement autoSuggestPopup;

	@FindBy(css = ".slot-data")
	WebElement homePageSlotData;

	@FindBy(css = ".slot-data>.grid-layout")
	List<WebElement> contentSlots;

	@FindBy(css = "button.menu-toggle.sprite")
	WebElement lnkHamburger;

	// ************* mini-cart*********************//
	@FindBy(css = "div[id='mini-cart'] span[class='minicart-quantity']")
	WebElement itemCountFromMiniCart;

	@FindBy(css = "div[id='mini-cart'] i[class='minicart-icon']")
	WebElement belkLogo;

	@FindBy(css = "div[class='mini-cart-products'] div[class='mini-cart-diffPay success']")
	WebElement bagOverlay;

	@FindBy(css = "div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-name']")
	WebElement srchSuggtnPDP;

	@FindBy(css = ".product-suggestions .product-image")
	List<WebElement> lstProductSuggestions;

	// ********Error msg******************//
	@FindBy(css = "p.error-text")
	WebElement searchErrorMsg;

	// ------------- Coupons section --------- //
	@FindBy(css = ".coupons>a")
	WebElement lnkCouponDesktop;

	@FindBy(css = ".header-nav ul[class*='menu-utility-user'] div.content-asset ul li.coupons>a")
	WebElement lnkCouponMobile;

	@FindBy(css = ".product-link .product-image")
	List<WebElement> imgSearchSuggestionProducts;

	@FindBy(css = ".product-suggestions div[class='product-suggestion']")
	List<WebElement> lstProductcountInSearchSuggestions;

	@FindBy(css = ".phrase-suggestions h4")
	List<WebElement> lstPhraseTitleInSearchSuggestion;

	@FindBy(css = "div[class='phrase-suggestions']>div[class='hitgroup'] h4")
	List<WebElement> lblSearchSuggestions;

	@FindBy(css = "div[class='product-suggestions']>div[class='product-suggestion']")
	List<WebElement> productSuggestions;

	@FindBy(css = "#primary h1")
	WebElement lblPageNotFound;

	@FindBy(css = "div[class='error-page-message'] div p")
	WebElement lblErrorPage;

	@FindBy(css = ".product-suggestions .product-name")
	List<WebElement> lstProductNameInPdtSuggestions;

	@FindBy(css = ".error-header")
	WebElement lblCorrectedSearchtxt;

	@FindBy(css = "a[class='mini-cart-link']")
	WebElement BagForProductSet;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement ClikOnViewBag;

	@FindBy(css = ".minicart-icon")
	WebElement BagForNoProduct;

	@FindBy(css = ".mini-cart-content[style*='block']")
	WebElement miniCartContent;

	@FindBy(css = "a.mini-cart-link")
	WebElement lnkMiniCart;

	@FindBy(css = "a[class='button mini-cart-link-cart']")
	WebElement btnViewBag;

	@FindBy(xpath = ".//*[@id='header']/div[2]/div/div[1]/div/ul/li[1]/a")
	WebElement giftCardLink;

	@FindBy(css = "li:nth-child(4)>div>ul>li:not([class*='coupons'])>a")
	WebElement giftCardLinkMobile;
		
	@FindBy(css = ".content-asset>ul>li>a[title='Wish List']")
	WebElement lnkWishList;

	@FindBy(css = "#header > div.header-nav > div.wrapper-content > ul.menu-utility-user.hide-desktop > li:nth-child(5) > div > ul > li:nth-child(1) > a")
	WebElement lnkWishListMobile;
	
	@FindBy(css = ".menu-footer.menu.pipe>li:nth-child(6)>a")
	WebElement lnkStoreLocations;
	
	@FindBy(css = ".searchstore-button")
	WebElement btnSearchStore;
	
	@FindBy(css = "div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-price'] div[class='product-sales-price']")
	List<WebElement> lstsrchSuggtnProductPrice;
	
	@FindBy(css = ".hit")
	List<WebElement> lnkFromSearchPopupPane;
	
	@FindBy(css=".hitgroup .hit")
	List<WebElement> lstBrandNameInSearchSuggestion;
	
	

	/**********************************************************************************************
	 ********************************* WebElements of Home Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */
	public HomePage(WebDriver driver, String url) {
		appURL = url;
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// HomePage

	/**
	 * 
	 * @param driver
	 *            : webdriver
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
  		minicart = new MiniCartPage(driver).get();
		elementLayer = new ElementLayer(driver);
	}// HomePage

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, btnSearch))) {
			Log.fail("Home Page did not open up. Site might be down.", driver);
		}
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		driver.get(appURL);
		Utils.waitForPageLoad(driver);
	}// load

	/**
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public PdpPage searchProduct(String textToSearch) throws Exception {
		final long startTime = StopWatch.startTime();
		if(textToSearch.contains("S_")){
			textToSearch=textToSearch.replace("S_", "");
		}
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));
		return new PdpPage(driver).get();
	}

	/**
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public SearchResultPage searchProductKeyword(String textToSearch)
			throws Exception {
		final long startTime = StopWatch.startTime();
		if(textToSearch.startsWith("S_")) {
			textToSearch = textToSearch.split("S_")[1];
		}
		SearchUtils.doSearch(txtSearch, btnSearch, textToSearch, driver);
		Log.event("Searched the provided product!",
				StopWatch.elapsedTime(startTime));
		return new SearchResultPage(driver).get();
	}

	/**
	 * To search any product with clicking 'Enter' Key in the keyboard The
	 * parameterized product name
	 * 
	 * @param String
	 *            txtTo Search
	 * @throws Exception
	 */
	public SearchResultPage searchProductWithKeyBoard(String textToSearch)
			throws Exception {
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Entering category in the search field");
		txtSearch.sendKeys(Keys.ENTER);
		return new SearchResultPage(driver).get();
	}

	/**
	 * To search
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public SearchResultPage searchProductWithAutoSuggestion(String textToSearch)
			throws Exception {
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Enter text in the search box ");
		Utils.waitForPageLoad(driver);
		SearchUtils.selectFromSearchAutoProductSuggestion(txtSearch,
				textToSearch, searchLink.get(0), driver);
		return new SearchResultPage(driver).get();
	}

	/**
	 * To get a text from Home page
	 * 
	 * @param lblSearchText
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Home page
	 * @throws Exception
	 */

	public String getTextFromSearchTextBox() throws Exception {
		String txtSearchTextBox = null;
		if (Utils.getRunPlatForm() == "mobile")
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchTextMobile,
							"Fetching the search text value  in the search result page");
		else
			txtSearchTextBox = BrowserActions
					.getText(driver, lblSearchText,
							"Fetching the search text value  in the search result page");

		return txtSearchTextBox;
	}

	/**
	 * To search any product with out click search button The parameterized
	 * product name
	 * 
	 * @param String
	 *            txtTo Search
	 * @throws Exception
	 */

	public void enterSearchTextBox(String txtToSearch) throws Exception {
		BrowserActions.typeOnTextField(txtSearch, txtToSearch, driver,
				"Enter product in the search text box");
		Utils.waitForElement(driver, autoSuggestPopup, 2);
	}

	/**
	 * To click the search text box
	 * 
	 * @throws Exception
	 */
	public void clickSearchTextBox() throws Exception {
		BrowserActions.javascriptClick(txtSearch, driver, "Search Text Box");
	}

	/**
	 * To get the search text box's class attribute
	 * 
	 * @throws Exception
	 */
	public String getSearchTextBoxClassAttribute() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getTextFromAttribute(driver,
				txtSearch, "class", "Search Box class attribute");
		return dataToBeReturned;
	}

	/**
	 * To Verify Search Box behavior
	 * 
	 * @throws Exception
	 */

	public void clickSearchIcon() throws Exception {
		BrowserActions.clickOnElement(btnSearch, driver,
				"clicking on search button");
	}

	/**
	 * To verify the content slot count in the home page, if content slot count
	 * should be less than or equal to 15, then it will return 'true' else
	 * false.
	 * 
	 * @return boolean value
	 */
	public boolean verifyContentSlotCount() {
		return contentSlots.size() <= 15;
	}

	/**
	 * To get the item count from mini cart
	 * 
	 * @return txtItemCount
	 * @throws Exception
	 */
	public String getItemCountFromMiniCart() throws Exception {
		String txtItemCount = BrowserActions.getText(driver,
				itemCountFromMiniCart, "item count");
		return txtItemCount;
	}

	/**
	 * To search
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public String searchProductWithAutoSuggestionsToPDP(String textToSearch)
			throws Exception {
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Enter text in the search box ");
		Utils.waitForPageLoad(driver);
		String selectedProduct = SearchUtils
				.selectFromSearchAutoProductSuggestion(txtSearch, textToSearch,
						srchSuggtnPDP, driver);
		return selectedProduct;
	}

	/**
	 * To navigate to Coupons page
	 */
	public CouponsPage navigateToCouponsPage() throws Exception {
		if (runPltfrm == "desktop") {
			BrowserActions.clickOnElement(lnkCouponDesktop, driver,
					"Coupon link");
		} else if (runPltfrm == "mobile") {
			headers.clickMobileHamburgerMenu();
			BrowserActions.scrollToView(lnkCouponMobile, driver);
			BrowserActions.clickOnElement(lnkCouponMobile, driver,
					"Mobile coupon link");
		}
		return new CouponsPage(driver).get();
	}

	/**
	 * To search
	 * 
	 * @param textToSearch
	 * @return txtErrorMessage
	 * @throws Exception
	 */
	public String getInValidSearchResult() throws Exception {
		String txtErrorMessage = BrowserActions.getText(driver, searchErrorMsg,
				"Error Message");
		return txtErrorMessage;
	}

	/**
	 * To click on the product from the search suggestion based on the index
	 * value
	 * 
	 * @param index
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage clickOnProductFromProductSuggestion(int index)
			throws Exception {
		BrowserActions.clickOnElement(lstProductSuggestions.get(index - 1),
				driver, "product from product suggestion");
		return new PdpPage(driver).get();
	}

	/**
	 * To click on the product from the search suggestion based on the index
	 * value
	 * 
	 * @param index
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage clickOnProductFromSearchSuggestion(int index)
			throws Exception {
		BrowserActions.clickOnElement(
				imgSearchSuggestionProducts.get(index - 1), driver,
				"product from search suggestion");
		return new PdpPage(driver).get();
	}

	/**
	 * To close the search suggestion panel
	 * 
	 * @throws Exception
	 */
	public void closeSearchSuggestionPanelByEsckey() throws Exception {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).build().perform();
		Utils.waitUntilElementDisappear(driver, autoSuggestPopup);
	}

	/**
	 * To get the product count in search suggestions panel
	 * 
	 * @return int - productCount
	 * @throws Exception
	 */
	public int getProductCountInSuggestion() throws Exception {
		return lstProductcountInSearchSuggestions.size();

	}

	/**
	 * To enter the text in search field
	 * 
	 * @param txtToSearch
	 * 
	 */
	public void enterTextInSearchfield(String txtToSearch) throws Exception {
		BrowserActions.typeOnTextField(txtSearch, txtToSearch, driver,
				"Search Box");
	}

	/**
	 * To verify search suggestions list
	 * 
	 * @param phraseTitle
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifySearchSuggestionsList(String phraseTitle)
			throws Exception {
		boolean status = false;
		for (WebElement element : lstPhraseTitleInSearchSuggestion) {
			if (element.getText().equals(phraseTitle)) {
				List<WebElement> element1 = element.findElement(By.xpath(".."))
						.findElements(By.cssSelector("a"));
				if (element1.size() > 0) {
					status = true;
				}
			}
		}
		return status;
	}

	/**
	 * To verify the category search suggestion
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyCategorySearchSuggestion(int index) throws Exception {
		boolean status = false;
		for (WebElement element : lblSearchSuggestions) {
			if (element.getText().equals("Categories")) {
				status = true;
				List<WebElement> lstElement = element.findElement(
						By.xpath("..")).findElements(By.cssSelector("a"));
				BrowserActions.javascriptClick(lstElement.get(index - 1),
						driver, "Categories link");
				break;
			}
		}

		if (!status)
			throw new Exception(
					"'Categories' items is not displayed in the search auto suggestion");
		return status;
	}

	/**
	 * to verify the product search suggestion
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean verifyProductSearchSuggestion() throws Exception {
		boolean status = false;
		for (WebElement e : productSuggestions) {
			if (productSuggestions.size() > 0) {
				BrowserActions.clickOnElement(productSuggestions.get(10),
						driver, "select any product");
				status = true;
				break;
			}
		}
		return status;
	}

	/**
	 * To search the product suggestion
	 * 
	 * @param textToSearch
	 * @throws Exception
	 */
	public void searchProductSuggestion(String textToSearch) throws Exception {
		SearchUtils.searchAutoProductSuggestion(txtSearch, textToSearch,
				productSuggestions, driver);
	}

	/**
	 * To search the category suggestion
	 * 
	 * @param textToSearch
	 * @throws Exception
	 */
	public void searchCategorySuggestion(String textToSearch) throws Exception {
		SearchUtils.searchAutoCategorySuggestion(txtSearch, textToSearch,
				lblSearchSuggestions, driver);
	}

	/**
	 * To click search suggestions list
	 * 
	 * @param productName
	 *
	 * @return SearchResultPage
	 * @throws Exception
	 */
	public SearchResultPage clickOnSearchSuggestions(String productName)
			throws Exception {
		BrowserActions.typeOnTextField(txtSearch, productName, driver,
				"Entering category in the search field");
		BrowserActions.clickOnElement(lnksearch, driver,
				"Click on search suggestion");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To verify the price is greater than zero or not
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyPriceWithGreaterThenZero() throws Exception {
		boolean returnValue = false;
		List<WebElement> lstSuggestProduct = BrowserActions.checkLocators(
				driver, NORMALPRICE);
		for (int productSuggestionCount = 0; productSuggestionCount < lstSuggestProduct
				.size(); productSuggestionCount++) {
			String txtprice = BrowserActions
					.getText(driver,
							lstSuggestProduct.get(productSuggestionCount),
							"price text");
			if ((txtprice.replace("$", "").replace(".00", "")) != "00") {
				returnValue = true;
			}
		}
		return returnValue;
	}

	/**
	 * To get product name from the search suggestion based on the index value
	 * 
	 * @param index
	 * @return String
	 * @throws Exception
	 */
	public String getProductNameFromProductSuggestion(int index)
			throws Exception {
		return BrowserActions.getText(driver,
				lstProductNameInPdtSuggestions.get(index - 1),
				"product from product suggestion");
	}

	/**
	 * To select search suggestion link by index
	 * 
	 * @param index
	 * @throws Exception
	 */
	public void selectSearchSuggestionProductWithIndex(int index)
			throws Exception {
		BrowserActions.clickOnElement(searchLink.get(index - 1), driver,
				"Click element from phrase suggestion");
	}

	/**
	 * To Click on the Page content from auto suggestions options and navigate
	 * to Page content Page.
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public Pagecontent searchProductWithAutoSuggestionPageContent(
			String textToSearch) throws Exception {
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Enter text in the search box ");
		Utils.waitForPageLoad(driver);
		SearchUtils.selectFromSearchAutoProductSuggestion(txtSearch,
				textToSearch, txtPageContent, driver);
		return new Pagecontent(driver).get();
	}

	/**
	 * To click on the bag when the view bag having zero product
	 * 
	 * @return Shopping bag
	 * @throws Exception
	 */
	public ShoppingBagPage ClikOnBagForWithNoProducts() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			BrowserActions.clickOnElement(BagForNoProduct, driver, "Bag link");
		} else {
			BrowserActions.clickOnElement(BagForNoProduct, driver, "Bag link");
		}

		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To mouse hover on the mini cart
	 * 
	 * @throws Exception
	 */
	public void mouseOverMiniCart() throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (!Utils.waitForElement(driver, miniCartContent, 2)) {
			if (runPltfrm == "desktop")
				BrowserActions.mouseHover(driver, miniCart);
		}

		if (Utils.waitForElement(driver, miniCartContent, 2)) {
			Log.event("Mini cart overlay already opened");
		} else {
			if (runPltfrm == "desktop")
				BrowserActions.mouseHover(driver, miniCart);
			else if (runPltfrm == "mobile")
				BrowserActions.javascriptClick(lnkMiniCart, driver,
						"mini cart ");
		}
	}

	/**
	 * To check the view bag is displayed or not
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean checkViewBag() throws Exception {

		boolean status = false;
		HomePage homepage = new HomePage(driver);
		mouseOverMiniCart();
		status = elementLayer.verifyPageElements(Arrays.asList("btnViewBag"),
				homepage);
		return status;
	}

	public ShoppingBagPage clickOnMiniCart() throws Exception {
		BrowserActions.clickOnElement(miniCart, driver, "Mini Cart Button");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public GiftCardsPage clickOnGiftCardLink() throws Exception {
		if (runPltfrm == "desktop") {
			BrowserActions.clickOnElement(giftCardLink, driver,
					"Gift Card Link");
			Utils.waitForPageLoad(driver);
		} else if (runPltfrm == "mobile") {
			headers.clickMobileHamburgerMenu();
			BrowserActions.scrollToView(giftCardLinkMobile, driver);
			BrowserActions.clickOnElement(giftCardLinkMobile, driver,
					"Mobile Gift Card link");
		}
		return new GiftCardsPage(driver).get();
	}
	
	public WishListPage clickOnWishList() throws Exception{
		if (runPltfrm == "desktop") {		
		BrowserActions.clickOnElement(lnkWishList, driver, "clicked on wish list");
		Utils.waitForPageLoad(driver);
		}else if (runPltfrm == "mobile") {
			headers.clickMobileHamburgerMenu();
			BrowserActions.scrollToView(lnkWishListMobile, driver);
			BrowserActions.clickOnElement(lnkWishListMobile, driver, "clicked on wish list");
		}			
		
		return new WishListPage(driver).get();
	}
	
	public StoreLocationPage clickStoreLocationsLink() throws Exception{
		BrowserActions.clickOnElement(lnkStoreLocations, driver, "Store locations");
		return new StoreLocationPage(driver).get();
	}
	
	/**
	 * To get ProductDetails From Auto Suggestions
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getSalesPriceProductWithAutoSuggestion(String textToSearch) throws Exception {
		ArrayList<String> ProductDetails = new ArrayList<String>();
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver, "Enter text in the search box ");
		Utils.waitForPageLoad(driver);
		int random = ThreadLocalRandom.current().nextInt(1, lstsrchSuggtnProductPrice.size());
	
		ProductDetails.add(0,
				BrowserActions.getText(driver, lstsrchSuggtnProductPrice.get(random - 1), "Product  Sales Price"));
	
		WebElement element1 = lstsrchSuggtnProductPrice.get(random - 1).findElement(By.xpath("../.."))
				.findElement(By.cssSelector(
						"div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-price']"));
		ProductDetails.add(1, BrowserActions.getText(driver, element1, "Product Name "));
		WebElement element2 = lstsrchSuggtnProductPrice.get(random - 1).findElement(By.xpath("../.."))
				.findElement(By.cssSelector(
						"div[class='product-suggestions'] div[class='product-suggestion'] div[class='product-details'] div[class='product-name']"));
		ProductDetails.add(2, BrowserActions.getText(driver, element2, "Product Name "));
		Utils.waitForPageLoad(driver);
		txtSearch.sendKeys(Keys.ENTER);
		return ProductDetails;
	}
	
	public boolean verifyHyperLinkforLinksFromSearchPopupPane() throws Exception {
		boolean dataToReturned = true;
		for(int i=0;i<lnkFromSearchPopupPane.size();i++)
		{
			
		String txtbeforehover=lnkFromSearchPopupPane.get(i).getCssValue("text-decoration"); 
		if(txtbeforehover.equals("none"))
			dataToReturned&=true;
			
		Actions actions = new Actions(driver);
		actions.moveToElement(lnkFromSearchPopupPane.get(i)).build().perform();
		String txtafterhover=lnkFromSearchPopupPane.get(i).getCssValue("text-decoration"); 
		if(txtbeforehover.equals("underline"))
			dataToReturned&=true;
		
		}
		return dataToReturned;
	}
	
	
	/**
	 * To Click on the Page content from auto suggestions options and navigate
	 * to Page content Page.
	 * 
	 * @param textToSearch
	 * @return
	 * @throws Exception
	 */
	public Pagecontent searchWithAutoSuggestionPageContent(
			String textToSearch) throws Exception {
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,
				"Enter text in the search box ");
		Utils.waitForPageLoad(driver);
		SearchUtils.selectFromSearchAutoProductSuggestion(txtSearch,
				textToSearch, txtPageContents, driver);
		return new Pagecontent(driver).get();
	}
	
	public String getEnteredTextFromSearchTextBox() throws Exception {		
		String	txtSearchTextBox = txtSearch.getAttribute("value");
					return txtSearchTextBox;
	
	}
	
	public SearchResultPage clickOnBrandNameInSearchSuggestionByIndex(int index) throws Exception{
		BrowserActions.clickOnElement(lstBrandNameInSearchSuggestion.get(index-1), driver, "brand name");
		return new SearchResultPage(driver).get();
	}
	
}// HomePage
