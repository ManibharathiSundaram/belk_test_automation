package com.belk.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.RegistryInformationPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class SignIn extends LoadableComponent<SignIn> {

	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public RegistrySignedUserPage registrySignedUserPage;

	String runPltfrm = Utils.getRunPlatForm();

	public final static String INLINE_USERNAME_ERROR = "Please enter your email address";
	public final String RESOURCEMESSAGE_BELOW_GUESTCHECKOUT = "You will have the opportunity to create an account and track your order once you have completed checkout.";
	public String RESOURCEMESSAGE_BELOW_RETURNINGUSER = "Please enter your email address and password to login to your account.";
	public String RESET_PWD_MESG = "Request to reset your password";

	// *********Realize Login***************
	@FindBy(css = ".login-box")
	WebElement divLoginBox;

	@FindBy(css = ".username input")
	WebElement txtUserName;

	@FindBy(css = ".password input")
	WebElement txtPassWord;

	@FindBy(name = "dwfrm_login_login")
	WebElement btnLoginIn;

	@FindBy(id = "dwfrm_login_rememberme")
	WebElement chkRememberme;

	// @FindBy(id = "dwfrm_login_expresscheckout") --- not working
	// WebElement chkExpressCheckOut;

	@FindBy(id = "dwfrm_login_useexpresscheckout")
	WebElement chkExpressCheckOut;

	@FindBy(css = ".error-form")
	WebElement formError;

	@FindBy(css = ".username input+span.error")
	WebElement userNameInlineError;

	@FindBy(css = "button[name='dwfrm_login_register']")
	WebElement btnCreateAccount;

	@FindBy(css = "button[name='dwfrm_login_unregistered']")
	WebElement btnCheckoutAsGuest;

	@FindBy(css = "div[ class='login-box-content clearfix']>p")
	WebElement getTextBelowCheckoutAsGuest;

	@FindBy(css = ".form-row.formbuttonrow>button")
	WebElement CheckoutAsGuest;

	@FindBy(css = ".login-box-content.returning-customers.clearfix>p")
	WebElement getTextBelowReturningUser;

	@FindBy(css = "div[class='error-form']")
	WebElement invalidLoginErrorMsg;

	@FindBy(css = "div[ class='password-row']>a[id='password-reset']")
	WebElement verifyForgetPassword;

	@FindBy(css = "div[id='dialog-container']")
	WebElement passwordRecoveryPage;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".step-1")
	WebElement tabShipping;

	@FindBy(id = "dwfrm_ordertrack_orderEmail")
	WebElement txtOrderEmail;

	@FindBy(id = "dwfrm_ordertrack_orderNumber")
	WebElement txtOrderNumber;

	@FindBy(css = "button[name='dwfrm_ordertrack_findorder']")
	WebElement btnViewOrderDetails;

	@FindBy(css = ".ordernot-found.error-form")
	WebElement invalidCheckOrderErrorMsg;

	// ///////Left Navigation////////////////////////////////

	@FindBy(css = ".secondary-navigation")
	WebElement secondaryNavigation;

	@FindBy(css = ".content-asset>h1")
	List<WebElement> lstContentAssetTitle;

	@FindBy(css = "a[title='Create an Account']")
	WebElement lnkCreateAccount;

	@FindBy(css = "a[title='View Privacy Policy']")
	WebElement lnkPrivacyPolicy;

	@FindBy(css = "a[title='Secure Shopping']")
	WebElement lnkSecureShopping;

	@FindBy(css = ".need-data")
	WebElement needHelpPane;

	@FindBy(css=".account-nav-asset.hide-desktop")
	WebElement needHelpPaneMobile;

	@FindBy(css = ".need-data .section-header")
	WebElement lblNeedHelp;

	@FindBy(css = ".need-data .customer-service")
	WebElement customerServiceContentPane;

	// from schoolnet team

	@FindBy(id = "dwfrm_requestpassword_email")
	WebElement txtResetPasswordEmail;

	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement btnResetPwdsent;

	@FindBy(css = ".cancel-button.simple.close-dialog")
	WebElement btnResetPwdCancel;

	@FindBy(css = "div[class *='forgotpassword-dialog'] > #dialog-container > .success")
	WebElement lblResetPwdSuccessHeader;

	@FindBy(css = "div[class *='forgotpassword-dialog'] > #dialog-container > p")
	WebElement lblResetPwdSuccessMsg;

	@FindBy(css = "div[class*='ui-dialog-titlebar']>button[title='Close']")
	WebElement btnCloseResetPwdSuccessDialog;

	@FindBy(css = "div[class='field-wrapper'] input[id='dwfrm_wishlist_search_email']")
	WebElement txtEmailId;

	@FindBy(css = "div[class='field-wrapper'] input[id='dwfrm_wishlist_search_firstname']")
	WebElement txtBoxFistName;

	@FindBy(css = "div[class='field-wrapper'] input[id='dwfrm_wishlist_search_lastname']")
	WebElement txtBoxLastName;

	@FindBy(css = "div[class='form-row form-row-button'] button[name='dwfrm_wishlist_search_search']")
	WebElement btnWishlist;

	@FindBy(css = ".ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix.ui-draggable-handle")
	WebElement forgotpasswordmodal;


	@FindBy(css = "#dwfrm_requestpassword_email")
	WebElement txtForgotpasswordEmail;

	@FindBy(css = "#PasswordResetForm button:nth-child(1)")
	WebElement btnForgotpasswordreset;

	@FindBy(css = ".cancel.cancel-button.simple")
	WebElement btnCancelForgotpassword;

	@FindBy(css = "div.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix.ui-draggable-handle")
	WebElement verifypasswordresetsuccess;

	@FindBy(css = ".secondary-navigation div[class='content-asset']>p")
	WebElement txtAccountNavUnregpanel;

	@FindBy(css = ".account-nav-asset div[class='content-asset']>p>span")
	WebElement txtAccounthelpcontent;

	@FindBy(css = "#dwfrm_requestpassword_email")
	WebElement txtBoxEmailInResetPasswordPopup;

	@FindBy(css = "#dwfrm_requestpassword_email-error")
	WebElement txtwarningmessageInResetPasswordPopup;

	@FindBy(css = ".form-row.form-row-button>button[name='dwfrm_requestpassword_send']")
	WebElement btnSend;

	//>>>>>>>>>>>>>>>>>>>>wishlist and registry >>>>>>>>>>>>>>>>>>>>>>>>>>>>//

	@FindBy(css = ".login-search-gift.clearfix>h2")
	WebElement lblfindRegistry;

	@FindBy(xpath = ".//*[@class='registry-search']/div[1]")
	WebElement txtFistNameRegistry;

	@FindBy(xpath = ".//*[@class='registry-search']/div[2]")
	WebElement txtLastNameRegistry;

	@FindBy(xpath = ".//*[@class='registry-search']/div[3]")
	WebElement eventTypeInRegistry;

	@FindBy(css = "#dwfrm_giftregistry_search_advancedsearch_eventCity")
	WebElement cityInAdvancedRegistry;

	@FindBy(xpath = ".//*[@class='registry-advancedsearch']/div[5]")
	WebElement stateInAdvancedRegistry;

	@FindBy(xpath = ".//*[@class='registry-advancedsearch']/div[7]")
	WebElement eventDateInAdvancedRegistry;

	@FindBy(css = "#dwfrm_giftregistry_search_advancedsearch_eventRegistryID")
	WebElement regIdInAdvancedRegistry;

	@FindBy(css = ".advanced-search")
	WebElement advanceSearchBtn;

	@FindBy(css = "button[value='Find Wish List']")
	WebElement findWishListBtn;

	@FindBy(css = "td>a>span")
	WebElement clickOnViewButtonInWishList;

	@FindBy(css = "div[class='item-list-device hide-desktop']>div>div>a>span")
	WebElement clickOnViewButtonInWishListForMob;

	@FindBy(css = "div[class*='forgotpassword-dialog'][style*='block']")
	WebElement forgotpasswordDialogOpened;

	@FindBy(css = "div[class*='forgotpassword-dialog'][style*='none']")
	WebElement forgotpasswordDialogClosed;        

	// /////////////////////////////////////////////////////

	/**
	 * 
	 * Constructor class for Login page Here we initializing the driver for page
	 * factory objects. For ajax element waiting time has added while
	 * initialization
	 * 
	 * @param driver
	 * @param url
	 */
	public SignIn(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divLoginBox))) {
			Log.fail("SignIn page didn't open up", driver);
		}

		elementLayer = new ElementLayer(driver);
		registrySignedUserPage = new RegistrySignedUserPage(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * 
	 * @param emailid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public MyAccountPage signInToMyAccount(String emailid, String password)
			throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new MyAccountPage(driver).get();
	}

	/**
	 * Enter email id
	 * 
	 * @param emailid
	 *            as string
	 * @throws Exception
	 */
	public void enterEmailID(String emailid) throws Exception {
		Utils.waitForElement(driver, txtUserName);
		BrowserActions
		.typeOnTextField(txtUserName, emailid, driver, "Email id");
		Log.event("Entered the Email Id: " + emailid);
	}

	/**
	 * Enter password
	 * 
	 * @param pwd
	 *            as string
	 * @throws Exception
	 */
	public void enterPassword(String pwd) throws Exception {
		Utils.waitForElement(driver, txtPassWord);
		BrowserActions.typeOnTextField(txtPassWord, pwd, driver, "Password");
		Log.event("Entered the Password: " + pwd);
	}

	/**
	 * To click LogIn button on signin page
	 * 
	 * @throws Exception
	 */
	public void clickBtnSignIn() throws Exception {
		final long startTime = StopWatch.startTime();
		Utils.waitForElement(driver, btnLoginIn);
		BrowserActions.clickOnElement(btnLoginIn, driver, "Login");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Login' button on SignIn page",
				StopWatch.elapsedTime(startTime));
	}

	/**
	 * To click the remember me check box
	 * 
	 * @throws Exception
	 */
	public void clickOnRememberMecheckBox() throws Exception {
		BrowserActions.clickOnElement(chkRememberme, driver,
				"Rememberme checkbox");
		Log.event("Checked the 'Rememberme' checkbox");
	}

	/**
	 * To click the Express checkout check box
	 * 
	 * @throws Exception
	 */
	public void clickOnExpressCheckOutcheckBox() throws Exception {
		BrowserActions.selectRadioOrCheckbox(chkExpressCheckOut, "YES");
		Log.event("Checked the 'Express Checkout' checkbox");
	}

	/**
	 * To get the user name field's error msg
	 * 
	 * @return dataToBeReturned - username error msg
	 * @throws Exception
	 */
	public String getUserNameInlineErrorMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, userNameInlineError,
				"User name field error msg");
		return dataToBeReturned;
	}

	public String getSignInFormErrorMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver, formError,
				"SignIn page form error");
		return dataToBeReturned;
	}

	/**
	 * To click Create Account button on signin page
	 * 
	 * @throws Exception
	 */
	public CreateAccountPage clickCreateAccount() throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.clickOnElement(btnCreateAccount, driver, "Login");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'Create Account' button on SignIn page",
				StopWatch.elapsedTime(startTime));
		return new CreateAccountPage(driver).get();
	}

	public CheckoutPage clickOnCheckoutAsGuest() throws Exception {
		BrowserActions.clickOnElement(btnCheckoutAsGuest, driver,
				"Checkout As Guest Button");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}

	public CheckoutPage clickOnCheckoutAsUser(String username, String passwd)
			throws Exception {
		BrowserActions.typeOnTextField(txtUserName, username, driver,
				"Username");
		BrowserActions.typeOnTextField(txtPassWord, passwd, driver, "Password");
		BrowserActions.clickOnElement(btnLoginIn, driver,
				"Checkout As Guest Button");
		Utils.waitForPageLoad(driver);
		if (!Utils.waitForElement(driver, tabShipping)) {
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();
			shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Utils.waitForPageLoad(driver);
		}
		return new CheckoutPage(driver).get();
	}

	public boolean isUseExpressCheckoutChecked() throws Exception {
		if (BrowserActions.isRadioOrCheckBoxSelected(chkExpressCheckOut))
			return true;
		return false;
	}

	/**
	 * To get the Resource message BelowCheckoutAsGuest.
	 * 
	 * @throws Exception
	 */
	public String getResourceMessageBelowCheckoutGuest() throws Exception {
		return BrowserActions.getText(driver, getTextBelowCheckoutAsGuest,
				"Resource message");
	}

	/**
	 * To clik on the Checkout as Guest
	 * 
	 * @throws Exception
	 */
	public CheckoutPage clikOnCheckoutAsGuest() throws Exception {
		BrowserActions.clickOnElement(CheckoutAsGuest, driver,
				"Checkout as Guest");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}

	/**
	 * To get the Resource message BelowCheckoutAsGuest.
	 * 
	 * @throws Exception
	 */
	public String getResourceMessageBelowReturningUser() throws Exception {
		String Text = BrowserActions.getText(driver, getTextBelowReturningUser,
				"Resource message");
		return Text;
	}

	/**
	 * To mouse hover on forget password link.
	 * 
	 * @throws Exception
	 */
	public String hoverOnForgetPwdLink() throws Exception {
		BrowserActions.mouseHover(driver, verifyForgetPassword);
		String recoveryMsg = BrowserActions.getTextFromAttribute(driver,
				verifyForgetPassword, "title", "Forget Password");
		return recoveryMsg;
	}

	/**
	 * To click on forget password link.
	 * 
	 * @throws Exception
	 */
	public void clikForgotPwd() throws Exception {
		BrowserActions.clickOnElement(verifyForgetPassword, driver,
				"Forget PassWord");
		Utils.waitForPageLoad(driver);
		
	}

	/**
	 * To get text in breadcrumb
	 * 
	 * @return status
	 * @throws InterruptedException
	 */
	public List<String> getTextInBreadcrumb(String... runMode)
			throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (runPltfrm == "desktop" || (runMode.length > 0)) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	public CheckoutPage signIn(String emailid, String password)
			throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new CheckoutPage(driver).get();
	}

	/**
	 * 
	 * @param orderemail
	 * @param ordernumber
	 * @return
	 * @throws Exception
	 */
	public void signInToViewOrderDetail(String orderemail, String ordernumber)
			throws Exception {
		enterOrderEmail(orderemail);
		enterOrderNumber(ordernumber);
		clickBtnViewOrderDetails();
	}

	/**
	 * Enter order email
	 * 
	 * @param orderemail
	 *            as string
	 * @throws Exception
	 */
	public void enterOrderEmail(String orderemail) throws Exception {
		Utils.waitForElement(driver, txtOrderEmail);
		BrowserActions.typeOnTextField(txtOrderEmail, orderemail, driver,
				"Order Email");
		Log.event("Entered the Order Email: " + orderemail);
	}

	/**
	 * Enter order number
	 * 
	 * @param ordernumber
	 *            as string
	 * @throws Exception
	 */
	public void enterOrderNumber(String ordernumber) throws Exception {
		Utils.waitForElement(driver, txtOrderNumber);
		BrowserActions.typeOnTextField(txtOrderNumber, ordernumber, driver,
				"Order Number");
		Log.event("Entered the Order Number: " + ordernumber);
	}

	/**
	 * To click View Order Details button on signin page
	 * 
	 * @throws Exception
	 */
	public void clickBtnViewOrderDetails() throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.clickOnElement(btnViewOrderDetails, driver,
				"ViewOrderDetails");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked 'View Order Details' button on SignIn page",
				StopWatch.elapsedTime(startTime));
	}

	/**
	 * get invalid Order email and Order number error message on signin page
	 * 
	 * @throws Exception
	 */
	public String getInvalidCheckOrderErrorMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver,
				invalidCheckOrderErrorMsg,
				"Invalid CheckOrder Error Msg in SignIn page");
		return dataToBeReturned;
	}

	public void signInToAccount(String emailid, String password)
			throws Exception {

		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();

	}

	public CheckoutPage signInToExpressCheckout(String emailid, String password)
			throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		BrowserActions.selectRadioOrCheckbox(chkExpressCheckOut, "YES");
		clickBtnSignIn();
		return new CheckoutPage(driver);
	}

	/**
	 * 
	 * @param emailid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public ShoppingBagPage signInFromShoppingBag(String emailid, String password)
			throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new ShoppingBagPage(driver).get();
	}



	/**
	 * To login in MyRegistryAccount
	 * 
	 * @param emailid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public RegistrySignedUserPage signInToMyRegistryAccount(String emailid,
			String password) throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new RegistrySignedUserPage(driver).get();
	}

	public String getLeftNavAttributeByTitle(String title, String tagName)
			throws Exception {
		if (title.equalsIgnoreCase("createAccount")) {
			return BrowserActions.getTextFromAttribute(driver,
					lnkCreateAccount, tagName, tagName + " Attribute");
		} else if (title.equalsIgnoreCase("privacyPolicy")) {
			return BrowserActions.getTextFromAttribute(driver,
					lnkPrivacyPolicy, tagName, tagName + " Attribute");
		} else if (title.equalsIgnoreCase("secureShopping")) {
			return BrowserActions.getTextFromAttribute(driver,
					lnkSecureShopping, tagName, tagName + " Attribute");
		}
		return null;
	}

	public String getNeedHelpText() throws Exception {
		return BrowserActions.getText(driver, customerServiceContentPane,
				"Need help 'Customer service' content data");
	}

	/**
	 * Enter Reset Password email
	 * 
	 * @param Reset
	 *            Password email as string
	 * @throws Exception
	 */
	public void enterResetPasswordEmail(String resetPasswordEmail)
			throws Exception {
		Utils.waitForElement(driver, txtOrderEmail);
		BrowserActions.typeOnTextField(txtResetPasswordEmail,
				resetPasswordEmail, driver, "Order Email");
		Log.event("Entered the Order Email: " + resetPasswordEmail);
		BrowserActions.clickOnElement(btnResetPwdsent, driver,
				"Reset password Sent");
		Utils.waitForPageLoad(driver);
		Log.event("Clicked Reset Password Email Send");
	}

	/**
	 * get Reset Password Header on signin page
	 * 
	 * @throws Exception
	 */
	public String getResetPasswordHeaderContent() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver,
				lblResetPwdSuccessHeader, "Reset Password Msg in SignIn page");
		return dataToBeReturned;
	}

	/**
	 * get Reset Password Message on signin page
	 * 
	 * @throws Exception
	 */
	public String getResetPasswordMsg() throws Exception {
		String dataToBeReturned = null;
		dataToBeReturned = BrowserActions.getText(driver,
				lblResetPwdSuccessMsg, "Reset Password Msg in SignIn page");
		return dataToBeReturned;
	}

	public void clickWishlistButton() throws Exception {

		BrowserActions.clickOnElement(btnWishlist, driver,
				"Find Wishlist Button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To fill the Wishlist form to find Wishlist
	 * 
	 * 
	 */
	public WishListPage detailsOfWishlistForm(String firstnamewishlist,
			String enterLastName, String emailwishlist) throws Exception {
		enterWishlistEmailid(emailwishlist);
		enterFirstName(firstnamewishlist);
		enterLastName(enterLastName);

		clickWishlistButton();
		return new WishListPage(driver).get();
	}

	public void enterFirstName(String firstnamewishlist) throws Exception {
		Utils.waitForElement(driver, txtBoxFistName);
		BrowserActions.typeOnTextField(txtBoxFistName, firstnamewishlist,
				driver, "Fist Name");
		Log.event("Enter the FirstName: " + firstnamewishlist);
	}

	public void enterLastName(String lastnamewishlist) throws Exception {
		Utils.waitForElement(driver, txtBoxLastName);
		BrowserActions.typeOnTextField(txtBoxLastName, lastnamewishlist,
				driver, "Last Name");
		Log.event("Enter the LastName: " + lastnamewishlist);
	}

	/**
	 * 
	 * @param emailid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public WishListPage signInToWishList(String emailid, String password) throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new WishListPage(driver).get();
	}


	/**
	 * To Reset the User Password from wishlist login  page
	 * @param emailid	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean verifyforgotpasswordmodalwindow() throws Exception {
		boolean status=false;
		if(forgotpasswordmodal.isDisplayed()){
			status=true;
		}

		return status;
	}

	/**
	 * To Enter the EmailID in Forget Password Popup
	 * @param emailid	 * 
	 * @return
	 * @throws Exception
	 */
	public void enterForgotPasswordEmail(String emailid) throws Exception
	{
		Set <String>handles = driver.getWindowHandles();//To handle multiple windows
		String firstWinHandle = driver.getWindowHandle();
		handles.remove(firstWinHandle);
		String winHandle=handles.iterator().next();
		if (winHandle!=firstWinHandle){
			String secondWinHandle = winHandle;

			driver.switchTo().window(secondWinHandle);   //Switch to popup window
			Thread.sleep(2000);
			Utils.waitForElement(driver, txtForgotpasswordEmail);
			BrowserActions.typeOnTextField(txtForgotpasswordEmail, emailid, driver, "Enter the Email ID to Forgot Password to Reset");
		}
	}

	/**
	 * To Click on Send button in Forget Password Popup 
	 * @return
	 * @throws Exception
	 */

	public void clickOnSendForgotPassword() throws Exception
	{
		Utils.waitForElement(driver, btnForgotpasswordreset);
		BrowserActions.clickOnElement(btnForgotpasswordreset, driver, "Send button in Forgot password popup");
	}


	/**
	 * To Click on Cancel button in Forget Password Popup

	 * @return
	 * @throws Exception
	 */
	public void clickOnCancelForgotPassword() throws Exception{
		Utils.waitForElement(driver, btnCancelForgotpassword);
		BrowserActions.clickOnElement(btnCancelForgotpassword, driver, "Send button in Forgot password popup");
	}


	/**
	 * To Verify PasswordReset Success modal window
	 * @return
	 * @throws Exception
	 */
	public boolean verifyPasswordResetSuccessModal()
	{
		return Utils.waitForElement(driver, verifypasswordresetsuccess);
	}
	/**
	 * To Verify PasswordReset Success modal window
	 * @param emailid	 * 
	 * @return
	 * @throws Exception
	 */

	public boolean verifySigninPage(){
		return Utils.waitForElement(driver, btnLoginIn);
	}

	/**
	 * Click 'Close' button on Reset Password Dialog
	 * 
	 * @throws Exception
	 */
	public void clickCloseOnResetPasswordRecoveryDialog() throws Exception {
		BrowserActions.clickOnElement(btnCloseResetPwdSuccessDialog, driver, "Close button in Reset Password Recovery Dialog.");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Click 'Cancel' button on Reset Password Dialog
	 * 
	 * @throws Exception
	 */
	public void clickCancelOnResetPasswordRecoveryDialog() throws Exception {
		BrowserActions.clickOnElement(btnResetPwdCancel, driver, "Cancel button in Reset Password Recovery Dialog.");
		Utils.waitForPageLoad(driver);
	}

	public void enterEmailInResetPasswordPopup(String email) throws Exception {
		Utils.waitForElement(driver, txtBoxLastName);
		BrowserActions.typeOnTextField(txtBoxEmailInResetPasswordPopup, email,
				driver, "Email");

	}

	public String warningmesaageInResetPasswordPopup() throws Exception {
		String warningmessageInResetPasswordPopup=BrowserActions.getText(driver,txtwarningmessageInResetPasswordPopup , "warning message");
		return warningmessageInResetPasswordPopup;
	}

	public void clickBtnSend() throws Exception {

		BrowserActions.clickOnElement(btnSend, driver, "Send");
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To Login to the application from Checkout page
	 * @param emailid,Password
	 * @return status
	 * @throws InterruptedException
	 */
	public RegistrySignedUserPage signInFromRegistryPage(String emailid, String password) throws Exception {
		enterEmailID(emailid);
		enterPassword(password);
		clickBtnSignIn();
		return new RegistrySignedUserPage(driver).get();
	}

	public void scrollToAdvanceSearch(){
		Utils.waitForElement(driver, advanceSearchBtn);
		BrowserActions.scrollToViewElement(advanceSearchBtn, driver);
	}

	public void clickOnAdvanceSearchBtn() throws Exception{
		Utils.waitForElement(driver, advanceSearchBtn);
		BrowserActions.scrollToViewElement(advanceSearchBtn, driver);
		BrowserActions.javascriptClick(advanceSearchBtn, driver, "advance Search Btn");
	}


	public void clickOnFindWishListBtn() throws Exception{
		Utils.waitForElement(driver, findWishListBtn);
		BrowserActions.scrollToViewElement(findWishListBtn, driver);
		BrowserActions.javascriptClick(findWishListBtn, driver, "advance Search Btn");
	}

	public RegistryInformationPage clickOnViewBtnInWishList() throws Exception{
		if(Utils.getRunPlatForm()=="mobile"){
			Utils.waitForElement(driver, clickOnViewButtonInWishListForMob);
			BrowserActions.scrollToViewElement(clickOnViewButtonInWishListForMob, driver);
			BrowserActions.javascriptClick(clickOnViewButtonInWishListForMob, driver, "advance Search Btn");
		}
		else{
			Utils.waitForElement(driver, clickOnViewButtonInWishList);
			BrowserActions.scrollToViewElement(clickOnViewButtonInWishList, driver);
			BrowserActions.javascriptClick(clickOnViewButtonInWishList, driver, "advance Search Btn");
		}
		return new RegistryInformationPage(driver).get();

	}

	public void enterWishlistEmailid(String emailwishlist) throws Exception {
		Utils.waitForElement(driver, txtEmailId);
		BrowserActions.scrollToViewElement(txtEmailId, driver);
		BrowserActions.typeOnTextField(txtEmailId, emailwishlist, driver,
				"Last Name");
		Log.event("Enter the Email_id: " + emailwishlist);
	}
	
	/**
	 * To click left navigation links
	 * 
	 * @param linkName
	 * @throws Exception
	 */
	public void clickLeftNavLinks(String linkName)
			throws Exception {
		WebElement element = null;
		if (linkName.equalsIgnoreCase("createAccount")) {
			element = lnkCreateAccount;
		} else if (linkName.equalsIgnoreCase("privacyPolicy")) {
			element = lnkPrivacyPolicy;
		} else if (linkName.equalsIgnoreCase("secureShopping")) {
			element = lnkSecureShopping;
		}
		BrowserActions.scrollToViewElement(element, driver);
		BrowserActions.clickOnElement(element, driver, "Left Navigation Links");
	}
}