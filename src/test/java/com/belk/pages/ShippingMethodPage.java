package com.belk.pages;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.footers.Footers;
import com.belk.pages.headers.Headers;
import com.belk.support.BrowserActions;
import com.belk.support.Log;
import com.belk.support.Utils;



public class ShippingMethodPage extends LoadableComponent <ShippingMethodPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Shopping Bag Page **************************
	 **********************************************************************************************/


	@FindBy(css = ".selected-option.selected")
	WebElement drpShippingMethod;

	@FindBy(css = ".backto-cart>a")
	WebElement lnkReturnToShippingAddress;		
	@FindBy(css = ".button-fancy-large")
	WebElement btnContinue;	

	@FindBy(css = ".section-header-note")
	WebElement lnkEdit;

	@FindBy(css = ".std-shipping-only>a")
	WebElement lnkWhyIsStandardShippingMyOnlyOption;
	@FindBy(css = ".ship-modal-name")
	WebElement lnkProductNameInWhyIsStandardShippingMyOnlyOptionModel;
	@FindBy(css = ".sku.sku-qty")
	WebElement lnkQtyInWhyIsStandardShippingMyOnlyOptionModel;
	@FindBy(css = ".standard-price")
	WebElement lnkPriceInWhyIsStandardShippingMyOnlyOptionModel;
	@FindBy(css = ".ship-modal-image>img")
	WebElement productImgInWhyIsStandardShippingMyOnlyOptionModel;
	@FindBy(css = "")
	WebElement lnkProductcolorInWhyIsStandardShippingMyOnlyOptionModel;
	@FindBy(css = "")
	WebElement lnkProductsizeInWhyIsStandardShippingMyOnlyOptionModel;

	@FindBy(css = ".button.simple")
	WebElement lnkGoToShoppingBagInWhyIsStandardShippingMyOnlyOptionModel;

	@FindBy(css = "div[class*='step-2 active']")
	WebElement shippingMethodInHeaderEnabled;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public ShippingMethodPage(WebDriver driver) {		
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);		
	}// GiftRegistryPage

	@Override
	protected void isLoaded() {   

		if (!isPageLoaded) {
			Assert.fail();
		}		

		if (isPageLoaded && !(Utils.waitForElement(driver, drpShippingMethod))) {
			Log.fail("Shopping Bag page did not open up.", driver);
		}


		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {		

		isPageLoaded = true;	
		Utils.waitForPageLoad(driver);
	}// load	

	public void ClickReturnToShippingAddresslink() throws Exception
	{
		BrowserActions.clickOnElement(lnkReturnToShippingAddress, driver, "ReturnToShippingAddress link");
		Utils.waitForPageLoad(driver);
	}

	public void ClickContinue() throws Exception
	{
		BrowserActions.clickOnElement(btnContinue, driver, "Continue Button");
		Utils.waitForPageLoad(driver);
	}

	public void ClickWhyIsStandardShippingMyOnlyOption() throws Exception
	{
		BrowserActions.clickOnElement(lnkWhyIsStandardShippingMyOnlyOption, driver, "WhyIsStandardShippingMyOnlyOption Link");
		Utils.waitForPageLoad(driver);
	}
	public  ShoppingBagPage ClickEditLinkFromOrderSummary() throws Exception
	{
		BrowserActions.clickOnElement(lnkEdit, driver, "Edit link from Order Summary");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}
	public  String getProductNameFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String txtproductname=BrowserActions.getText(driver, lnkProductNameInWhyIsStandardShippingMyOnlyOptionModel, "ProductNameInWhyIsStandardShippingMyOnlyOptionModel Text");
		Utils.waitForPageLoad(driver);
		return txtproductname;

	}

	public  String getQtyFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String txtqty=BrowserActions.getText(driver, lnkQtyInWhyIsStandardShippingMyOnlyOptionModel, "ProductNameInWhyIsStandardShippingMyOnlyOptionModel Text");
		Utils.waitForPageLoad(driver);
		return txtqty;

	}
	public  String getPriceFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String txtqty=BrowserActions.getText(driver, lnkPriceInWhyIsStandardShippingMyOnlyOptionModel, "ProductNameInWhyIsStandardShippingMyOnlyOptionModel Text");
		Utils.waitForPageLoad(driver);
		return txtqty;

	}
	public  String getProductUrlFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String imageUrl=BrowserActions.getTextFromAttribute(driver,productImgInWhyIsStandardShippingMyOnlyOptionModel , "src", "href of indexed image");
		Utils.waitForPageLoad(driver);
		return imageUrl;

	}
	public  String getproductcolorFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String txtqty=BrowserActions.getText(driver, lnkPriceInWhyIsStandardShippingMyOnlyOptionModel, "ProductNameInWhyIsStandardShippingMyOnlyOptionModel Text");
		Utils.waitForPageLoad(driver);
		return txtqty;

	}
	public  String getproductsizeFromWhyIsStandardShippingMyOnlyOptionModel() throws Exception
	{
		String txtqty=BrowserActions.getText(driver, lnkPriceInWhyIsStandardShippingMyOnlyOptionModel, "ProductNameInWhyIsStandardShippingMyOnlyOptionModel Text");
		Utils.waitForPageLoad(driver);
		return txtqty;

	}
	public void ClickGoToShoppingBagInWhyIsStandardShippingMyOnlyOption() throws Exception
	{
		BrowserActions.clickOnElement(lnkGoToShoppingBagInWhyIsStandardShippingMyOnlyOptionModel, driver, "GoToShoppingBagInWhyIsStandardShippingMyOnlyOptionModel Link");
		Utils.waitForPageLoad(driver);
	}
	/**
	 * To verify whether navigated to Shipping Method page
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyNavigatedToShippingMethodPage() throws Exception {
		boolean status= false;
		if(BrowserActions.getText(driver, shippingMethodInHeaderEnabled, "Shipping method Header").trim().replaceAll("\\\n", " ").equals("2 Shipping Method")) {
			status = true;
		}
		return status;
	}

}// ShippingMethodPage


