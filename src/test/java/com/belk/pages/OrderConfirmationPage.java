package com.belk.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.Point;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.AccountUtils;
import com.belk.reusablecomponents.PaymentUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

import net.sf.ehcache.util.FindBugsSuppressWarnings;

public class OrderConfirmationPage extends LoadableComponent<OrderConfirmationPage> {
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;

	@CacheLookup
	@FindBy(css = ".order-summary-title")
	WebElement txtOrderno;

	@FindBy(css = ".order-shipping>td:nth-child(2)") // .order-shipping>td:nth-child(2)
	// .order-shipping>td
	WebElement getShippingRate;

	@CacheLookup
	@FindBy(css = "div[class='shipping-address']>div:nth-child(3)")
	WebElement lblShippingAddress;

	@FindBy(css = ".confirmation-message h1")
	WebElement txtResourcemsg;

	@FindBy(css = "div[class='orderpaymentinstrumentsgc']>span.bold")
	WebElement PaymentType;

	// /////// ----Create Account---//////////////////

	@FindBy(css = "input[id='dwfrm_profile_customer_firstname']")
	WebElement txtFirstname;

	@FindBy(css = "input[id='dwfrm_profile_customer_lastname']")
	WebElement txtLastname;

	@FindBy(css = "input[id='dwfrm_profile_customer_email']")
	WebElement txtEmail;

	@FindBy(css = "input[id='dwfrm_profile_customer_emailconfirm']")
	WebElement txtConformEmail;

	@FindBy(css = "input[id*='dwfrm_profile_login_password']")
	WebElement txtPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_passwordconfirm']")
	WebElement txtConformPassword;

	@FindBy(css = "button[name='dwfrm_profile_confirm']")
	WebElement btnCreateAccount;

	@FindBy(css = ".order-shipsurcharge>td")
	List<WebElement> fldShippingCharge;

	@FindBy(css = "#order-summary > tbody > tr > td.payment-total > div > div > span.label")
	WebElement txtPaymentMethodType;

	@FindBy(css = "#main > div > div > div.backtoshop.cusumer-auth > a")
	WebElement btnReturnToShopping;

	@FindBy(css = ".order-total>td")
	List<WebElement> fldOrderTotal;

	@FindBy(css = ".order-subtotal>td:nth-child(2)")
	WebElement txtMerchandiseTotalCost;

	@FindBy(css = ".order-shipping>td:nth-child(2)")
	WebElement txtShippingCost;

	@FindBy(css = ".order-total>td:nth-child(2)")
	WebElement txtEstimatedTotal;

	@FindBy(css = "[class='order-value']")
	WebElement orderValue;

	@FindBy(css = "div[class='address']")
	WebElement shippingwholeAddress;

	@FindBy(css = "[class='payment-city']")
	WebElement shippingCity;

	@FindBy(css = "[class='payment-addr']")
	WebElement shippingAddress;

	@FindBy(css = ".cc-type")
	WebElement CrdType;

	@FindBy(css = ".free-shiorderNumberpping")
	WebElement freeShipping;

	@FindBy(css = ".payment-city")
	WebElement PincodeAddress;

	@FindBy(css = ".order-shipping>td:nth-child(2)")
	WebElement txtShippingValue;

	@FindBy(css = ".order-shipsurcharge>td:nth-child(2)")
	WebElement fldShippingSurCharge;

	@FindBy(css = ".order-subtotal>td")
	List<WebElement> fldMerchandiseTotal;

	@FindBy(css = ".order-total>td")
	List<WebElement> fldEstimatedTotal;

	@FindBy(css = "div[class='order-information']>div:nth-child(3)>span[class='value']")
	WebElement orderNumber;

	@FindBy(css = ".name>a")
	WebElement getProductName;

	@FindBy(css="#dwfrm_profile_customer_emailconfirm-error")
	WebElement txtErrorInEmail;


	@FindBy(css = ".order-sales-tax>td")
	List<WebElement> fldSalesTax;

	@FindBy(css = ".order-discount>td")
	List<WebElement> fldCoupanSavings;

	@FindBy(css = ".order-totals-table-cont .order-subtotal >td:nth-child(2)")
	WebElement lblMerchandiseTotalValue;

	@FindBy(css = ".address")
	List<WebElement> multishipAddress;

	@FindBy(css = "table[id='order-shipment-tablelist']:nth-child(1)>thead>tr")
	WebElement OrderShippingAddress1;

	@FindBy(css = "table[id='order-shipment-tablelist']:nth-child(2)>thead>tr")
	WebElement OrderShippingAddress2;

	@FindBy(css = "table[id='order-shipment-tablelist']:nth-child(3)>thead>tr")
	WebElement OrderShippingAddress3;

	@FindBy(css = ".order-discount td:nth-child(2)")
	WebElement lblAssociateDiscount;

	@FindBy(css = ".order-brd>td")
	WebElement fldRewardDollar;

	@FindBy(css = "div[class='promo  first ']")
	WebElement txtPromoMessageOnOrderSummary;

	@FindBy(css = "tr[class='order-brd']>td:nth-child(1)")
	WebElement brdApplied;

	@FindBy(css = ".orderdetails #order-summary-payment")
	WebElement lblpaymentinfoWithHeader;

	@FindBy(css = ".orderdetails #order-shipment-tablelist")
	WebElement lblshippinginfoWithHeader;

	@FindBy(css = "div[class*='backtoshop'] a")
	WebElement returnToShoppingBtn;

	@FindBy(css= "tr[class='line-item']:nth-child(3)>td>div>div>div[class='sku item-qty']>span:nth-child(2)")
	WebElement UpcFreeGiftProduct;

	@FindBy(css= "tr[class='line-item']:nth-child(2)>td>div>div>div[class='sku sku-qty']>span:nth-child(2)")
	WebElement qtyProductFirst;

	@FindBy(css = ".order-subtotal>td:nth-child(2)")
	WebElement articlePriceInOrderConfirmation;
	
	@FindBy(css=".confirmation-message>span")
	WebElement confirmationMessage;
	
	@FindBy(css=".continue")
	WebElement returnToShopping;
	
	@FindBy(css=".largebutton")
	WebElement createAccontButton;
	
	@FindBy(css=".login-box-content.clearfix>p")
	WebElement messageCreateAccount;
	
	@FindBy(css="div [class='field-wrapper checkbox-wrapper']>input")
	WebElement emailRadioButton;
	
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("order confirmation ");
	public String Firstname = "input[id='dwfrm_profile_customer_firstname']";
	public String Lastname = "input[id='dwfrm_profile_customer_lastname']";
	public String email = "#dwfrm_profile_customer_email";
	public String confirmEmail = "#dwfrm_profile_customer_emailconfirm";
	public String password = "div[class='form-row passrowd-row required']>div>input";
	public String confirmPassword = "div[class='form-row cnfpassrowd-row required']>div>input";
	

	public OrderConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, txtOrderno))) {
			Log.fail("Order Confirmation Page did not open up. Site might be down.", driver);
		}
		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver);
	}// isLoaded

	@Override
	protected void load() {

		isPageLoaded = true;
		Utils.waitForPageLoad(driver);

		try {
			Utils.switchWindows(driver, "OrderConfirmation Page", "title", "false");
		} catch (Exception e) {
		}

		Utils.waitForElement(driver, txtOrderno);

	}// load

	public void verifyOrderNomber() throws Exception {
		final long startTime = StopWatch.startTime();
		PaymentUtils.getTextFromPaymentPage(txtOrderno, driver).contains("DH");
		Log.event("Verified Order number", StopWatch.elapsedTime(startTime));
	}

	/*
	 * public void verifyShippingAddressDetails() throws Exception{ final long
	 * startTime = StopWatch.startTime();
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("California");
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("Address");
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress, driver).equals(
	 * "1746 Bay St"); PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("San Francisco");
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("CA");
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("94123");
	 * PaymentUtils.getTextFromPaymentPage(lblShippingAddress, driver).equals(
	 * "UNITED STATES"); PaymentUtils.getTextFromPaymentPage(lblShippingAddress,
	 * driver).equals("9876543210"); Log.event("Verified shipping Details",
	 * StopWatch.elapsedTime(startTime)); }
	 */
	/**
	 * To get the text from Success message
	 * 
	 * @return
	 * @throws Exception
	 */
	public String GetTextFromSuccessMessage() throws Exception {
		return BrowserActions.getText(driver, txtResourcemsg, "Resource message");
	}

	public String GetTextFromResource() throws Exception {
		return BrowserActions.getText(driver, txtResourcemsg, "Resource message");
	}

	public String getPaymentMethodType() throws Exception {
		String paymentMethod = BrowserActions.getText(driver, txtPaymentMethodType, "Payment method");
		Log.message("paymentMethod ===>" + paymentMethod);
		return paymentMethod;
	}

	// ------------------E2E Elements-----------------------//

	@FindBy(css = ".checkout-summary-error .error")
	WebElement errMsgCheckoutSummary;

	@FindBy(id = "order-summary")
	WebElement tblOrderSummary;

	@FindBy(css = "table[id='order-summary'] > thead > tr > th")
	List<WebElement> orderSummaryTableHeader;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td")
	List<WebElement> orderSummaryTableData;

	@FindBy(css = "table[id='order-shipment-tablelist'] > thead > tr > th")
	List<WebElement> shippingAddressTableHeader;

	@FindBy(css = "#order-summary >tbody>tr>td")
	List<WebElement> lstOrderSummaryLocation;

	// ======================================================//

	/**
	 * To get the linked list of hashmap of every table column data from order
	 * summary page
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getOrderSummaryDesktop() throws Exception {
		LinkedList<LinkedHashMap<String, String>> orderSummary = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < orderSummaryTableHeader.size(); i++) {
			LinkedHashMap<String, String> tableData = new LinkedHashMap<String, String>();

			WebElement paymentSummary = tblOrderSummary.findElement(By.cssSelector("tbody > tr > td.payment-summary"));
			WebElement paymentAddress = tblOrderSummary.findElement(By.cssSelector("tbody > tr > td.payment-address"));
			WebElement paymentTotal = tblOrderSummary.findElement(By.cssSelector("tbody > tr > td.payment-total"));
			WebElement paymentMeth = tblOrderSummary.findElement(By.cssSelector("tbody > tr > td.payment-meth"));
			if (orderSummaryTableHeader.get(0).getText() == "Order Information") {
				List<WebElement> orderInfoKey = paymentSummary.findElements(By.cssSelector(".order-date > span"));
				List<WebElement> orderInfoValue = paymentSummary.findElements(By.cssSelector(".order-date > span"));
				if (orderInfoKey.size() == orderInfoValue.size()) {
					for (int j = 0; j < orderInfoKey.size(); i++)
						tableData.put(orderInfoKey.get(j).getText(), orderInfoValue.get(j).getText());
				}
				// System.out.println(productDetails.add(product));
				orderSummary.add(tableData);
			}
			if (orderSummaryTableHeader.get(0).getText() == "Order Information") {
				String[] lblNames = paymentAddress.findElement(By.cssSelector(".mini-address-location > div")).getText()
						.trim().split("\\n");

				tableData.put("FirstName", lblNames[0]);
				tableData.put("LastName", lblNames[1]);

				List<WebElement> address = paymentAddress.findElements(By.cssSelector(".mini-address-location > span"));

				String lblAddress1 = address.get(0).getText();
				String lblCityStateZipCode = address.get(1).getText();
				String lblPhone = address.get(2).getText();

				tableData.put("Address1", lblAddress1);
				// tableData.put("Address2", lblAddress2);
				tableData.put("city", lblCityStateZipCode.split(",")[0]);
				tableData.put("state", lblCityStateZipCode.split(" ")[1]);
				tableData.put("zipcode", lblCityStateZipCode.split(" ")[2]);
				tableData.put("phone", lblPhone);
				// System.out.println(productDetails.add(product));
				orderSummary.add(tableData);
			}
			if (orderSummaryTableHeader.get(0).getText() == "Payment Method") {
				WebElement lblCardType = paymentTotal
						.findElement(By.cssSelector(".order-payment-instruments .cc-type"));
				WebElement lblNameOnCard = paymentTotal
						.findElement(By.cssSelector(".order-payment-instruments .cc-owner"));
				WebElement lblCardNumber = paymentTotal
						.findElement(By.cssSelector(".order-payment-instruments .cc-card-group .cc-number"));
				WebElement lblCardExpiry = paymentTotal
						.findElement(By.cssSelector(".order-payment-instruments .cc-card-group .cc-exp"));
				WebElement lblAmount = paymentTotal
						.findElement(By.cssSelector(".order-payment-instruments .payment-amount"));

				tableData.put("CardType", lblCardType.getText());
				tableData.put("NameOnCard", lblNameOnCard.getText());
				tableData.put("CardNumber", lblCardNumber.getText());
				tableData.put("CardExpiry", lblCardExpiry.getText());
				tableData.put("Amount", lblAmount.getText().trim().split("\\$")[1]);
				// System.out.println(productDetails.add(product));
				orderSummary.add(tableData);
			}
			if (orderSummaryTableHeader.get(0).getText() == "Payment Details") {
				WebElement lblMerchandiseTotal = paymentMeth
						.findElement(By.cssSelector(".order-totals-table .order-subtotal > td:nth-child(2)"));
				WebElement lblShipping = paymentMeth.findElement(By.cssSelector(".order-totals-table .order-shipping"));
				WebElement lblSalesTax = paymentMeth
						.findElement(By.cssSelector(".order-totals-table .order-sales-tax"));
				WebElement lblOrderTotal = paymentMeth.findElement(By.cssSelector(".order-totals-table ."));

				tableData.put("MerchandiseTotal", lblMerchandiseTotal.getText().trim().replace("$", ""));
				tableData.put("Shipping", lblShipping.getText().trim().replace("$", ""));
				tableData.put("SalesTax", lblSalesTax.getText().trim().replace("$", ""));
				tableData.put("OrderTotal", lblOrderTotal.getText().trim().replace("$", ""));
				// System.out.println(productDetails.add(product));
				orderSummary.add(tableData);
			}
		}
		System.out.println(orderSummary);
		return orderSummary;
	}

	/******************************************************************************/
	/* individual column in order summary table */
	/******************************************************************************/

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary")
	WebElement orderInformation;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address")
	WebElement billingAddress;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-total")
	// class
	// name
	// is
	// invalid
	// from
	// dev
	// side.
	// if
	// dev
	// find
	// and
	// change
	// this
	// will
	// fail
	// as
	// with
	// payment
	// details
	// element
	WebElement paymentMethod;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-meth")
	WebElement paymentDetails;

	/******************************************************************************/
	/* individual webelement in order summary table */
	/******************************************************************************/

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(1) > .label")
	WebElement lblOrderDateKey;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(1) > .value")
	WebElement lblOrderdateValue;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(2) > .label")
	WebElement lblOrderTimeKey;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(2) > .value")
	WebElement lblOrderTimeValue;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(3) > .label")
	WebElement lblOrderNumberKey;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-summary div.order-information > div:nth-child(3) > .value")
	WebElement lblOrderNumberValue;

	/******************************************************************************/

	/**
	 * To get the orderInformation details
	 * 
	 * @return LinkedHashMap with "OrderDate" - Order Date "OrderTime" - Order
	 *         Time "OrderNumber" - Order Number
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getOrderInfoInOrderSummary() throws Exception {
		LinkedHashMap<String, String> orderInformation = new LinkedHashMap<String, String>();

		// create order data keys from site label
		String orderDateKey = lblOrderDateKey.getText().trim().replace(" ", "").replace(":", "");
		String orderTimeKey = lblOrderTimeKey.getText().trim().replace(" ", "").replace(":", "");
		String orderNumberKey = lblOrderNumberKey.getText().trim().replace(" ", "").replace(":", "");

		// create order data values from site label
		String orderDateValue = lblOrderdateValue.getText().trim();
		String orderTimeValue = lblOrderTimeValue.getText().trim();
		String orderNumberValue = lblOrderNumberValue.getText().trim();

		orderInformation.put(orderDateKey, orderDateValue);
		orderInformation.put(orderTimeKey, orderTimeValue);
		orderInformation.put(orderNumberKey, orderNumberValue);

		return orderInformation;
	}

	/******************************************************************************/
	/* individual webelement in Billing Addre table */
	/******************************************************************************/

	// WebElement paymentAddress =
	// tblOrderSummary.findElement(By.cssSelector("tbody > tr >
	// td.payment-address

	@FindBy(css = ".mini-address-location>span")
	List<WebElement> lstAddressInBillingAddress;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address .mini-address-location > div")
	WebElement lblFirstNameLastname;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address .mini-address-location > span:nth-child(2)")
	WebElement lblBillingAddress1;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address .mini-address-location > span:nth-child(3)")
	WebElement lblBillingAddress2;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address .mini-address-location > span:nth-child(2)")
	WebElement lblCityStateZip;

	@FindBy(css = "table[id='order-summary'] > tbody > tr > td.payment-address .mini-address-location > span.payment-phone")
	WebElement lblTelePhone;

	/******************************************************************************/

	/**
	 * To get the Billing Address details
	 * 
	 * @return LinkedHashMap with
	 *         'FirstName','LastName','Address1','Address2','City','State','
	 *         Zipcode','Phone'
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBillingAddressInOrderSummary() throws Exception {
		LinkedHashMap<String, String> billingAddress = new LinkedHashMap<String, String>();

		String[] lblNames = lblFirstNameLastname.getText().contains(System.getProperty("line.separator"))
				? lblFirstNameLastname.getText().trim().split("\\n") : lblFirstNameLastname.getText().trim().split(" ");
				String lblAddress2 = null;
				billingAddress.put("FirstName", lblNames[0]);
				billingAddress.put("LastName", lblNames[1]);
				int i = 0;
				String lblAddress1 = lstAddressInBillingAddress.get(i++).getText().trim();
				billingAddress.put("Address", lblAddress1);
				if (lstAddressInBillingAddress.size() > 3) {
					lblAddress2 = lstAddressInBillingAddress.get(i++).getText().trim();
					billingAddress.put("Address2", lblAddress2);
				}
				String lblCityStateZipCode = lstAddressInBillingAddress.get(i++).getText().trim();
				billingAddress.put("City", lblCityStateZipCode.split(",")[0]);

				billingAddress.put("State", lblCityStateZipCode.split("\\,")[1].trim().split(" ")[0]);
				billingAddress.put("Zipcode", lblCityStateZipCode.split("\\,")[1].trim().split(" ")[1]);
				String lblPhone = lstAddressInBillingAddress.get(i++).getText().trim();
				billingAddress.put("Phone", lblPhone.replaceAll("-", ""));

				return billingAddress;
	}

	/******************************************************************************/
	/* individual webelement in Payment Methd table */
	/******************************************************************************/

	@FindBy(css = ".cc-type")
	WebElement lblCardType;

	@FindBy(css = ".cc-owner")
	WebElement lblNameOnCard;

	@FindBy(css = ".cc-number")
	WebElement lblCardNumber;

	@FindBy(css = ".cc-exp")
	WebElement lblCardExpiry;

	// @FindBy(css = ".payment-amount")
	@FindBy(xpath = "//div[@class='cc-type']/following-sibling::div[@class='payment-amount'] / span[@class='value']")
	WebElement lblAmount;

	@FindBy(css = "div[class='orderpaymentinstrumentsgc']")
	List<WebElement> listGiftCardMethod;

	@FindBy(css = "div[class='orderpaymentinstrumentsgc']")
	WebElement signleGiftCardMethod;

	@FindBy(css = ".orderpaymentinstrumentsgc.order-payment-instruments-list")
	List<WebElement> lstTxtBRDNumberInSection;

	@FindBy(css = ".orderpaymentinstrumentsgc.order-payment-instruments-list")
	WebElement singleTxtBRDNumberInSection;

	/******************************************************************************/

	/**
	 * To get the payment method details
	 * 
	 * @return 'CardType','NameOnCard','CardNumber','CardExpiry','Amount'
	 * @throws Exception
	 */
	public LinkedHashMap<String, LinkedHashMap<String, String>> getPaymentMethodInOrderSummary_e2e() throws Exception {
		LinkedHashMap<String, LinkedHashMap<String, String>> paymentMethod = new LinkedHashMap<String, LinkedHashMap<String, String>>();

		if (Utils.waitForElement(driver, signleGiftCardMethod, 3)) {
			int size = listGiftCardMethod.size();
			for (int i = 0; i < size; i++) {
				LinkedHashMap<String, String> giftCardDetails = new LinkedHashMap<String, String>();
				String cardNumber = listGiftCardMethod.get(i).findElements(By.cssSelector(".value")).get(0).getText().trim();
				String amount = listGiftCardMethod.get(i).findElement(By.cssSelector(".payment-amount .value")).getText().trim();

				amount = amount.replace("$", "");
				cardNumber = cardNumber.substring(cardNumber.length() - 4);
				giftCardDetails.put("GiftCardNumber", cardNumber);
				giftCardDetails.put("Amount", amount);
				paymentMethod.put("GiftCard" + (i + 1), giftCardDetails);
			}
		}

		if (Utils.waitForElement(driver, singleTxtBRDNumberInSection, 3)) {
			int size = lstTxtBRDNumberInSection.size();
			for (int i = 0; i < size; i++) {
				LinkedHashMap<String, String> brdDetails = new LinkedHashMap<String, String>();
				String cardNumber = lstTxtBRDNumberInSection.get(i).findElement(By.cssSelector("div.value")).getText().trim();
				String amount = lstTxtBRDNumberInSection.get(i).findElement(By.cssSelector(".payment-amount .value")).getText().trim();

				amount = amount.replace("$", "");
				cardNumber = cardNumber.substring(cardNumber.length() - 4);
				brdDetails.put("BRDNumber", cardNumber);
				brdDetails.put("Amount", amount);
				paymentMethod.put("BRD" + (i + 1), brdDetails);
			}
		}

		if (Utils.waitForElement(driver, lblCardType, 3)) {
			LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
			String cardType = new String();
			String nameOnCard = new String();
			String cardNumber = new String();
			String amount = new String();

			cardType = lblCardType.getText().trim();
			nameOnCard = lblNameOnCard.getText().trim();
			cardNumber = lblCardNumber.getText().replace("*", "").trim();
			amount = lblAmount.getText().replace("$", "").trim();

			cardDetails.put("CardType", cardType);
			cardDetails.put("NameOnCard", nameOnCard);
			cardDetails.put("CardNumber", cardNumber);
			cardDetails.put("Amount", amount);

			paymentMethod.put("CreditCard", cardDetails);
		}

		return paymentMethod;
	}

	public LinkedHashMap<String, String> getPaymentMethodInOrderSummary() throws Exception {
		LinkedHashMap<String, String> paymentMethod = new LinkedHashMap<String, String>();
		paymentMethod.put("CardType", lblCardType.getText());
		paymentMethod.put("NameOnCard", lblNameOnCard.getText());
		paymentMethod.put("CardNumber", lblCardNumber.getText());

		try {
			String month = lblCardExpiry.getText().split("\\/")[0];
			String year = lblCardExpiry.getText().split("\\/")[1];
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
			simpleDateFormat.setCalendar(calendar);
			String monthName = simpleDateFormat.format(calendar.getTime());
			paymentMethod.put("CardExpiry", monthName + "|20" + year);
		} catch (Exception e) {
			// TODO: handle exception
		}

		paymentMethod.put("Amount", lblAmount.getText().replace("$", "").trim());

		return paymentMethod;
	}

	// ======================================================================================
	@FindBy(css = ".orderpaymentinstrumentsgc>span.value")
	WebElement lblGiftCardNumber;

	@FindBy(css = ".payment-amount")
	WebElement lblGiftCardAmount;

	/**
	 * To get the payment method details
	 * 
	 * @return 'Gift Card number', 'Gift Card Amount'
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getGiftCardPaymentDetailsInOrderSummary() throws Exception {
		LinkedHashMap<String, String> paymentMethod = new LinkedHashMap<String, String>();
		paymentMethod.put("GiftCardNumber", lblGiftCardNumber.getText());
		if (lblAmount.getText().contains("$")) {
			paymentMethod.put("Amount", lblGiftCardAmount.getText().trim().split("\\:")[1].trim().replace("$", ""));
		} else {
			paymentMethod.put("Amount", lblGiftCardAmount.getText());
		}
		return paymentMethod;
	}

	/******************************************************************************/
	/* individual webelement in Payment Details table */
	/******************************************************************************/

	@FindBy(css = ".order-subtotal>td:nth-child(2)")
	WebElement lblMerchandiseTotal;

	@FindBy(css = ".order-shipping>td:nth-child(2)")
	WebElement lblShipping;

	@FindBy(css = ".order-sales-tax>td:nth-child(2)")
	WebElement lblSalesTax;

	@FindBy(css = ".order-total>td:nth-child(2)")
	WebElement lblOrderTotal;

	@FindBy(css = ".order-shipsurcharge td:nth-child(2)")
	WebElement lblSurcharge;

	@FindBy(css = ".order-giftbox>td:nth-child(2)")
	WebElement txtGiftBoxValue;

	@FindBy(css = ".order-totals-table")
	WebElement lblOrderAmountDetails;

	/******************************************************************************/

	@FindBy(css = ".order-discount.discount >td:nth-child(2)")
	WebElement txtTotalCouponSavings;

	/**
	 * To get the payment details
	 * 
	 * @return 'MerchandiseTotal','Shipping','SalesTax','OrderTotal',
	 *         SurchargeAmount
	 * @throws Exception
	 *             Last modified By - Nandhini & Date modified - 1/2/2017
	 */
	public LinkedHashMap<String, String> getPaymentDetailsInOrderSummary() throws Exception {
		LinkedHashMap<String, String> paymentDetails = new LinkedHashMap<String, String>();
		// 1
		paymentDetails.put("MerchandiseTotal",
				Utils.checkPriceWithDollar(lblMerchandiseTotal.getText()).trim().replace("$", ""));

		// 2
		try {
			String totalCouponSavings = BrowserActions.getText(driver, txtTotalCouponSavings, "Total Coupon Savings");
			// 2
			paymentDetails.put("CouponSavings", totalCouponSavings.replace("-$", ""));
		} catch (Exception e) {
			Log.event("Total Coupon Savings is not displayed");
		}

		if (lblOrderAmountDetails.getText().contains("Gift Box")) {
			String giftBoxValue = BrowserActions.getText(driver, txtGiftBoxValue, "gift Box amount");
			// 3
			paymentDetails.put("Giftbox", giftBoxValue.replace("$", ""));
		}

		if (lblOrderAmountDetails.getText().contains("Surcharge")) {
			// 4
			paymentDetails.put("Surcharge", Utils.checkPriceWithDollar(lblSurcharge.getText()).trim().replace("$", ""));
		}
		// 5
		paymentDetails.put("EstimatedShipping",
				Utils.checkPriceWithDollar(lblShipping.getText()).trim().replace("$", ""));
		// 6
		paymentDetails.put("SalesTax", Utils.checkPriceWithDollar(lblSalesTax.getText()).trim().replace("$", ""));
		// 7
		paymentDetails.put("EstimatedOrderTotal", lblOrderTotal.getText().trim().replace("$", ""));

		return paymentDetails;
	}

	/**
	 * To get the payment details
	 * 
	 * @return 'MerchandiseTotal','Shipping','SalesTax','OrderTotal',
	 *         SurchargeAmount
	 * @throws Exception
	 *             Last modified By - Nandhini & Date modified - 1/2/2017
	 */
	public float getCalculateOrderTotal() throws Exception {
		float orderTotal = 0;
		float merchandiseTotal = (float) getMerchandiseTotal();
		float associateDiscount = 0;
		String shippingValue = getTextFromShipping();
		String salesTax = Utils.checkPriceWithDollar(getTextFromSalesTax()).replace("$", "");
		float giftBox = 0;
		float surcharge = 0;

		if (Utils.waitForElement(driver, lblAssociateDiscount)) {
			associateDiscount = Float.parseFloat(
					BrowserActions.getText(driver, lblAssociateDiscount, "Dicount amount").replace("-$", ""));
		}

		if (Utils.waitForElement(driver, txtGiftBoxValue)) {
			giftBox = Float
					.parseFloat(BrowserActions.getText(driver, txtGiftBoxValue, "gift box amount").replace("$", ""));
		}

		if (Utils.waitForElement(driver, lblSurcharge)) {
			surcharge = Float
					.parseFloat(BrowserActions.getText(driver, lblSurcharge, "surcharge amount").replace("$", ""));
		}

		orderTotal = merchandiseTotal + Float.parseFloat(shippingValue) + Float.parseFloat(salesTax) + giftBox
				+ surcharge - associateDiscount;
		return orderTotal;

	}

	/******************************************************************************/
	/* individual webelement in shipping addres table */
	/******************************************************************************/

	@FindBy(css = "#order-shipment-tablelist .order-shipment-address")
	List<WebElement> lblShippingAddressList;

	@FindBy(css = "#order-shipment-tablelist .order-shipment-table .shipping-method .value")
	List<WebElement> lblShippingMethod;

	@FindBy(css = "#order-shipment-tablelist .order-shipment-address .address .payment-name")
	WebElement lblNameInShippingAddress;

	@FindBy(css = "#order-shipment-tablelist .order-shipment-address .address .payment-addr")
	WebElement lblAddress1InShippingAddress;

	@FindBy(css = "#order-shipment-tablelist .order-shipment-address .address .payment-city")
	WebElement lblCityStateZipInShippingAddress;

	@FindBy(css = "#order-shipment-tablelist .order-shipment-address .address .payment-phone")
	WebElement lblPhoneShippingAddress;

	/******************************************************************************/

	/**
	 * To get the shipping addresses
	 * 
	 * @return 'FirstName','LastName','Address1','Address2','City','State','
	 *         ZipCode','Phone','ShippingMethod'
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getShippingAddressInOrderSummary() throws Exception {
		LinkedList<LinkedHashMap<String, String>> shippingAddressDetails = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < lblShippingAddressList.size(); i++) {
			LinkedHashMap<String, String> shippingAddress = new LinkedHashMap<String, String>();

			WebElement name = lblShippingAddressList.get(i).findElement(By.cssSelector(".address .payment-name"));
			List<WebElement> address = lblShippingAddressList.get(i)
					.findElements(By.cssSelector(".address .payment-addr"));
			WebElement cityStateZip = lblShippingAddressList.get(i)
					.findElement(By.cssSelector(".address .payment-city"));
			WebElement telephone = lblShippingAddressList.get(i).findElement(By.cssSelector(".address .formatPhone"));

			String firstName = name.getText().trim().split(" ")[0];
			String lastName = name.getText().trim().split(" ")[1];
			String address1 = address.get(0).getText().trim();
			String city = cityStateZip.getText().trim().split(",")[0];
			String state = cityStateZip.getText().trim().split(",")[1].trim().split(" ")[0];
			String zipcode = cityStateZip.getText().trim().split(",")[1].trim().split(" ")[1];
			String phone = telephone.getText().trim();
			String shippingmethod = lblShippingMethod.get(i).getText().trim();

			shippingAddress.put("FirstName", firstName);
			shippingAddress.put("LastName", lastName);
			shippingAddress.put("Address", address1);
			if (address.size() == 2) {
				String address2 = address.get(1).getText().trim();
				shippingAddress.put("Address2", address2);
			}
			shippingAddress.put("City", city);
			shippingAddress.put("State", state);
			shippingAddress.put("Zipcode", zipcode);
			shippingAddress.put("Phone", phone.replaceAll("-", ""));
			shippingAddress.put("ShippingMethod", shippingmethod);

			shippingAddressDetails.add(shippingAddress);
		}

		return shippingAddressDetails;
	}
	
	/**
	 * To get the shipping addresses
	 * 
	 * @return 'FirstName','LastName','Address1','Address2','City','State','
	 *         ZipCode','Phone','ShippingMethod'
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getBopisShippingAddressInOrderSummary() throws Exception {
		LinkedList<LinkedHashMap<String, String>> shippingAddressDetails = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < lblShippingAddressList.size(); i++) {
			LinkedHashMap<String, String> shippingAddress = new LinkedHashMap<String, String>();

			WebElement name = lblShippingAddressList.get(i).findElement(By.cssSelector(".address .payment-name"));
			List<WebElement> address = lblShippingAddressList.get(i)
					.findElements(By.cssSelector(".address .payment-addr"));
			WebElement cityStateZip = lblShippingAddressList.get(i)
					.findElement(By.cssSelector(".address .payment-city"));
			WebElement telephone = lblShippingAddressList.get(i).findElement(By.cssSelector(".address .formatPhone"));

			String firstName = name.getText().trim();
			String address1 = address.get(0).getText().trim();
			String city = cityStateZip.getText().trim().split(",")[0];
			String state = cityStateZip.getText().trim().split(",")[1].trim().split(" ")[0];
			String zipcode = cityStateZip.getText().trim().split(",")[1].trim().split(" ")[1];
			String phone = telephone.getText().trim();
			String shippingmethod = lblShippingMethod.get(i).getText().trim();

			shippingAddress.put("FirstName", firstName);
			shippingAddress.put("Address", address1);
			shippingAddress.put("City", city);
			shippingAddress.put("State", state);
			shippingAddress.put("Zipcode", zipcode);
			shippingAddress.put("Phone", phone.replaceAll("-", ""));
			shippingAddress.put("ShippingMethod", shippingmethod);

			shippingAddressDetails.add(shippingAddress);
		}

		return shippingAddressDetails;
	}
	
	

	/******************************************************************************/
	/* individual webelement in product details table */
	/******************************************************************************/

	@FindBy(css = "#order-shipment-tablelist .line-item")
	List<WebElement> lblProductList;

	/******************************************************************************/

	/**
	 * To get the shipping addresses
	 * 
	 * @return 'ProductName','UPC','Color','Size','Quantity','Price',' Last
	 *         modified By - Nandhini & Date modified - 1/25/2017
	 * @throws Exception
	 * 
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInOrderSummary() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();

		for (int i = 0; i < lblProductList.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();

			WebElement ele_name = lblProductList.get(i).findElement(By.cssSelector(".name > a"));
			WebElement ele_upc = lblProductList.get(i).findElement(By.cssSelector(".product-UPC"));
			WebElement ele_color = lblProductList.get(i)
					.findElement(By.cssSelector("div[data-attribute='color'] .value"));
			WebElement ele_size = lblProductList.get(i)
					.findElement(By.cssSelector("div[data-attribute='size'] .value"));
			WebElement ele_quantity = lblProductList.get(i).findElement(By.cssSelector(".value.qty-value"));
			WebElement ele_price = lblProductList.get(i).findElement(By.cssSelector(".base-product-price"));
			if (Utils.getRunPlatForm().equals("mobile")) {
				ele_price = lblProductList.get(i).findElement(By.cssSelector(" .line-item-price.hide-desktop"));
			}

			// WebElement ele_totalprice =
			// lblProductList.get(i).findElement(By.cssSelector(".subtotal-price"));

			String name = ele_name.getAttribute("title").trim().split("\\:")[1].trim();
			String upc = ele_upc.getText().trim();
			String color = ele_color.getText().trim();
			String size = ele_size.getText().trim();
			String quantity = ele_quantity.getText().trim();
			String price = Utils.checkPriceWithDollar(ele_price.getText()).trim().replace("$", "").trim();
			// String totalprice =
			// ele_totalprice.getText().trim().split("\\$")[1].trim();

			product.put("ProductName", name);
			product.put("Upc", upc.trim());
			product.put("Color", color);
			product.put("Size", size);
			product.put("Price", price);
			product.put("Quantity", quantity);
			// product.put("TotalPrice", totalprice);

			productDetails.add(product);
		}

		return productDetails;
	}

	public String GetOrderNumber() throws Exception {
		return BrowserActions.getText(driver, orderNumber, "Order Number Value");
	}

	public String GetOrderTotalValue() throws Exception {
		return BrowserActions.getText(driver, orderValue, "Order Total value");
	}

	public String GetCardType() throws Exception {
		return BrowserActions.getText(driver, CrdType, "Card Type");
	}

	public String GetShippingtype() throws Exception {
		return BrowserActions.getText(driver, freeShipping, "Card Type");
	}

	public String getTextFromAddress() throws Exception {
		String address = null;
		address = BrowserActions
				.getText(driver, PincodeAddress, "Fetching the search text value  in the search result page")
				.replace("CA, CA ", "");

		return address;
	}

	/**
	 * To get the text from mrechandise total
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromOrderSummary() throws Exception {
		return BrowserActions.getText(driver, fldMerchandiseTotal.get(1), "Merchantise total value");
	}

	/**
	 * To get the text from shipping value
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromShipping() throws Exception {
		return BrowserActions.getText(driver, txtShippingValue, "Shipping value").replace("$", "");
	}

	/**
	 * To get the text from sales tax
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromSalesTax() throws Exception {
		return BrowserActions.getText(driver, fldSalesTax.get(1), "Sales Tax");
	}


	/**
	 * To get the text from coupan savings
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromCoupanSavings() throws Exception {
		return BrowserActions.getText(driver, fldCoupanSavings.get(1), "Coupan Savings");
	}

	/**
	 * To get the text from order total
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromOrderTotal() throws Exception {
		return BrowserActions.getText(driver, fldEstimatedTotal.get(1), "Sales Tax");
	}

	/**
	 * To get the text from shipping surcharge
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromShipSurCharge() throws Exception {
		if (Utils.waitForElement(driver, fldShippingSurCharge)) {
			return BrowserActions.getText(driver, fldShippingSurCharge, "Shipping surcharge");
		} else {
			return "0";
		}
	}

	public String getPaymentTypeInPlaceOrderPage() throws Exception {

		Utils.waitForElement(driver, PaymentType);
		BrowserActions.getText(driver, PaymentType, "payment type");
		String paymenttype = BrowserActions.getText(driver, PaymentType, "payment type");
		return paymenttype;

	}

	/**
	 * To get Shipping Address in OrderConfirmation Page
	 * 
	 * 
	 */
	public String getTextFromShippingAddressInOrderPage(int index) throws Exception {

		WebElement ShippingAddress = driver.findElement(By.cssSelector("div[class='order-shipments']>table:nth-child("
				+ index + ")>tbody>tr[class='order-shipment-table']>td>div[class='order-shipment-address']"));
		String ShpAdd = BrowserActions.getText(driver, ShippingAddress, "Getting Shipping Addess ");
		return ShpAdd;
	}

	/**
	 * To get Shipping Method in OrderConfirmation Page
	 * 
	 * 
	 */
	public String getTextFromShippingMethodInOrderPage(int index) throws Exception {

		WebElement ShippingMethod = driver.findElement(By.cssSelector("div[class='order-shipments']>table:nth-child("
				+ index + ")>tbody>tr[class='order-shipment-table']>td>div[class='shipping-method']"));
		String ShpMthd = BrowserActions.getText(driver, ShippingMethod, "Getting Shipping Addess ");
		return ShpMthd;
	}

	public double getMerchandiseTotal() throws Exception {

		double merchandiseTotalValue = Double.parseDouble(
				BrowserActions.getText(driver, lblMerchandiseTotalValue, "MerchandiseTotalValue").replace("$", ""));
		return merchandiseTotalValue;
	}

	public double calculatePrice(int productCount, String productPrice) {

		double total = 0;
		try {
			total = productCount * Double.parseDouble(productPrice);
		} catch (Exception e) {
			System.out.print(false);

			// TODO: handle exception
		}

		return total;
	}

	public String getShippingMethod() {

		String shippingmethod = lblShippingMethod.get(0).getText().trim();
		return shippingmethod;
	}

	public LinkedHashMap<String, String> getMultiShippingMethods() throws Exception {
		LinkedHashMap<String, String> shipMethod = new LinkedHashMap<String, String>();

		List<WebElement> lstShippingMethod = driver.findElements(By.cssSelector(".shipping-method .value"));
		int size = lstShippingMethod.size();
		for (int i = 0; i < size; i++) {
			shipMethod.put("ShippingMethod" + (i + 1), lstShippingMethod.get(i).getText().trim());
		}

		return shipMethod;
	}

	public String getShippingRateInOrderConfirmationPage() throws Exception {
		Utils.waitForElement(driver, getShippingRate);
		BrowserActions.scrollToViewElement(getShippingRate, driver);
		String rate = BrowserActions.getText(driver, getShippingRate, "get Shipping Rate");
		return rate;
	}

	/**
	 * To get the OrderInformation and Billing Address
	 * 
	 * @return order info and Billing address including header
	 * @throws Exception
	 */

	public String gettextfromordersummaryIncludingHeader() throws Exception {
		String ordertext = BrowserActions.getText(driver, tblOrderSummary, "order information");
		return ordertext;

	}

	/**
	 * To get the Payment method and Payment details including Header
	 * 
	 * @return Payment method and Payment details including header
	 * @throws Exception
	 */

	public String gettextfrompaymentsummaryIncludingHeader() throws Exception {
		String paymenttext = BrowserActions.getText(driver, lblpaymentinfoWithHeader, "payment information");
		return paymenttext;
	}

	/**
	 * To get the shipping Details/product details including Header
	 * 
	 * @return shipping Details/product details including header
	 * @throws Exception
	 */
	public String gettextfromshippingsummaryIncludingHeader() throws Exception {
		String shippingtext = BrowserActions.getText(driver, lblshippinginfoWithHeader, "shipping information");
		return shippingtext;
	}

	public String GetShippingAddress() throws Exception {

		String shpCity = BrowserActions.getText(driver, shippingCity, "Shipping Address in Order Confirmation Page");
		String address = shpCity.trim().replace("Farmington, CT ", "");
		return address;
	}

	public void ClickOnReturnToShoppingBtn() throws Exception {
		Utils.waitForElement(driver, returnToShoppingBtn);
		BrowserActions.scrollToViewElement(returnToShoppingBtn, driver);
		BrowserActions.javascriptClick(returnToShoppingBtn, driver, "return To Shopping Btn");
	}

	/******************************************************************************/
	/* individual webelement in Shipping registry Address */
	/******************************************************************************/

	@FindBy(css = ".order-shipment-address div[class*='gift-icon']>span")
	WebElement registryAddress;

	@FindBy(css = ".shipping-method .value")
	WebElement shippingMethod;

	/**
	 * To get Registry Shipping Address
	 * 
	 * @return registry address, shipping method
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getRegistryShippingAddress() throws InterruptedException {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.scrollToView(registryAddress, driver);
		shippingDetails.put("registryShipment", registryAddress.getText());
		shippingDetails.put("shippingMethod", shippingMethod.getText());
		return shippingDetails;

	}

	public String gettextFromPayment() throws Exception {
		String paymenttext = BrowserActions.getText(driver, brdApplied, "BRD information").replace(":", "");
		return paymenttext;
	}



	public String getProductName() throws Exception {
		String Pr = BrowserActions.getText(driver, getProductName, "Get Product Name");
		String Pr1 = Pr.trim().replace("Velcro® Sandal", "");
		return Pr1;
	}

	/**
	 * To get the current X coordinate value Of OrderSummary Details In mobile
	 * 
	 * @index index
	 * @return int - X coordinate value
	 */
	public int getCurrentLocationOfOrderAndPaymentDetailsByIndex(int index) {
		WebElement details = lstOrderSummaryLocation.get(index - 1);
		Point point = details.getLocation();
		return point.getX();

	}

	/**
	 * To enter random email id
	 * @throws Exception
	 */
	public void enterRandomEmailIdInCreateAccount() throws Exception {
		txtEmail.clear();
		String randomEmail = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		BrowserActions.typeOnTextField(txtEmail, randomEmail+"@gmail.com", driver, "Search Box");
		BrowserActions.typeOnTextField(txtConformEmail, randomEmail+"@gmail.com", driver, "Search Box");

	}

	public void enterPasswordInCreateAccount(String pasword) throws Exception
	{
		txtPassword.clear();
		BrowserActions.typeOnTextField(txtPassword, pasword, driver, "Search Box");
		BrowserActions.typeOnTextField(txtConformPassword, pasword, driver, "Search Box");

	}
	/**
	 * To click the submit button
	 */
	public void clickOnSubmitInCreateAccount() throws Exception {

		BrowserActions.javascriptClick(btnCreateAccount, driver, "Create account");
		Utils.waitForPageLoad(driver);
	}



	/**
	 * UPC text of free gift item
	 */
	public String getPromoMsgforFreeGiftItem() throws Exception{
		BrowserActions.scrollToView(UpcFreeGiftProduct, driver);
		String Upc = BrowserActions.getText(driver, UpcFreeGiftProduct,
				"Upc Free Gift ");
		return Upc;
	} 


	/**
	 * QTY of First Item
	 */
	public String getQtyforFirstProduct() throws Exception{
		BrowserActions.scrollToView(qtyProductFirst, driver);
		String QTY = BrowserActions.getText(driver, qtyProductFirst,
				"QTY First Product");
		return QTY;
	} 

	
	public String getConfirmationMessage() throws Exception {
		BrowserActions.scrollToView(confirmationMessage, driver);
		String Message = BrowserActions.getText(driver, confirmationMessage, "Get Confirmation Message");
		return Message;
	}

	
	
	public boolean verifyEmailCheckBox()
	{
		boolean istatus=emailRadioButton.isSelected();
		return istatus;
	}

	public void ClickOnCreateAccount() throws Exception {
		Utils.waitForElement(driver, createAccontButton);
		BrowserActions.scrollToViewElement(createAccontButton, driver);
		BrowserActions.javascriptClick(createAccontButton, driver, "Create Account");
	}
	

	
	public String getCreateNewAccountMessage() throws Exception {
		BrowserActions.scrollToView(messageCreateAccount, driver);
		String Message = BrowserActions.getText(driver, messageCreateAccount, "Get Create New Account Message");
		return Message;
	}
	
	/**
	 * To create account in order confirmation page
	 * @param UserDetails
	 * @return
	 * @throws Exception
	 */
	
	public LinkedHashMap<String, String> fillingCustomerDetails(String UserDetails) throws Exception {
		LinkedHashMap<String, String> newCustomerDetails = new LinkedHashMap<String, String>();

		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		newCustomerDetails.put("type_firstname_QaFirst" + randomFirstName, Firstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		newCustomerDetails.put("type_firstname_QaLast" + randomLastName, Lastname);

		String details = checkoutProperty.getProperty(UserDetails);
		String email1 = details.split("\\|")[0];
		String confirmemail = details.split("\\|")[1];
		String Password = details.split("\\|")[2];
		String Confirmpassword = details.split("\\|")[3];
		
		newCustomerDetails.put("type_email_" + email1 ,email);
		newCustomerDetails.put("type_confirmEmail_" + confirmemail, confirmEmail);
		newCustomerDetails.put("type_password_" + Password, password);
		newCustomerDetails.put("type_confirmPassword_" + Confirmpassword, confirmPassword);
		AccountUtils.doAccountOperations(newCustomerDetails, driver);
		return newCustomerDetails;
	}
	
	public String getProductCost() throws Exception {
		BrowserActions.scrollToView(txtMerchandiseTotalCost, driver);
		String Price = BrowserActions.getText(driver, txtMerchandiseTotalCost, "Get Cost Of the Product").trim().replace("$ ", "").trim();
		return Price;
	} 
	public String getProductRateInOrderConfirmationPage() throws Exception {
		Utils.waitForElement(driver, articlePriceInOrderConfirmation);
		BrowserActions.scrollToViewElement(articlePriceInOrderConfirmation, driver);
		String rate = BrowserActions.getText(driver, articlePriceInOrderConfirmation, "get Product Rate").trim().replace("$", "").trim();
		return rate;

	}
	public float getMerchandiseTotals() throws Exception {

		float merchandiseTotalValue = Float.parseFloat(
				BrowserActions.getText(driver, lblMerchandiseTotalValue, "MerchandiseTotalValue").replace("$", ""));
		return merchandiseTotalValue;
	}
}
