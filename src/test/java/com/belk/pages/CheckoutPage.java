package com.belk.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.belk.pages.footers.CustomerServicePage;
import com.belk.pages.footers.FaqPage;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.headers.Headers;
import com.belk.reusablecomponents.BillingPageUtils;
import com.belk.reusablecomponents.PaymentUtils;
import com.belk.reusablecomponents.ShippingPageUtils;
import com.belk.reusablecomponents.StateUtils;
import com.belk.support.BrowserActions;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.StopWatch;
import com.belk.support.Utils;

public class CheckoutPage extends LoadableComponent<CheckoutPage> {

	private WebDriver driver;
	private boolean isPageLoaded;

	public ElementLayer elementLayer;

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");

	public static final String DEFAULT_STATE = "div[class='form-row state-row required'] div[class='selected-option']";
	public String btnGuest = "button[name='dwfrm_login_unregistered']";
	public String txtGuestEmail = "input[id='dwfrm_singleshipping_email_emailAddress']";
	public String lblSurchargeMessage = "Surcharge Applied";
	public String lblExtraAssociateDiscount = "Order Discount: Belk Pacesetter Discount - 30% OFF Your Order";
	public String lblAssociateDiscount = "Order Discount: Belk Employee Discount - 20% OFF Your Order";

	// public String txtFirstname =
	// "input[id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']";
	// public String txtLastname =
	// "input[id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']";
	private static final String txtFirstname = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']";
	private static final String txtLastname = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']";

	public String btnCountry = "select[id='dwfrm_singleshipping_shippingAddress_addressFields_country']";
	public String txtZipcode = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_postal']";
	public String txtAddress = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address1']";
	public String txtAddress2 = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address2']";
	public String txtCity = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_city']";
	public String btnState = ".state-row .selected-option";
	public String txtPhoneNo = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_phone']";
	public String btnContinueToBilling = "button[name='dwfrm_singleshipping_shippingAddress_save']";
	public String txtAddressName = "input[id='dwfrm_singleshipping_shippingAddress_addressid']";
	public String chkSaveToAddressBook = "input[id='dwfrm_singleshipping_shippingAddress_addToAddressBook']";
	public String chkUseThisAddressForBilling = "#dwfrm_singleshipping_shippingAddress_useAsBillingAddress";
	public String rdoTestShipment = "input[id='shipping-method-Test_Shipment']";
	public static final String rdoStandard = "input[id='shipping-method-Ground']";
	public String rdoExpress = "input[id='shipping-method-2DAY']";
	public String rdoOvernight = "input[id='shipping-method-OVNT']";
	public String registryAddress = "div[class='details'] span[class='gift-icon bold']";
	public String rdoAPOFPOStandard = "input[id='shipping-method-APOF']";

	/**********************************************************************************************
	 ********************************* WebElements of Billing Address****************************
	 **********************************************************************************************/
	public String txtBillingAddressname = "input[id='dwfrm_billing_billingAddress_addressid']";
	public String txtBillingFirstname = "input[id='dwfrm_billing_billingAddress_addressFields_firstName']";
	public String txtBillingLastname = "input[id='dwfrm_billing_billingAddress_addressFields_lastName']";
	public String txtBillingAddressOne = "input[id='dwfrm_billing_billingAddress_addressFields_address1']";
	public String txtBillingAddressTwo = "input[id='dwfrm_billing_billingAddress_addressFields_address2']";
	public String txtBillingCity = "input[id='dwfrm_billing_billingAddress_addressFields_city']";
	public String btnBillingState = ".state-row .selected-option";
	public String txtBillingZipcode = "input[id='dwfrm_billing_billingAddress_addressFields_postal']";
	public String txtBillingPhoneNo = "input[id='dwfrm_billing_billingAddress_addressFields_phone']";
	public String txtBillingEmail = "input[id='dwfrm_billing_billingAddress_email_emailAddress']";
	public String chkBillingEmail = "input[id='dwfrm_billing_billingAddress_addToEmailList']";
	public String chkSaveToBillingAddressBook = "input[id='dwfrm_billing_billingAddress_addToAddressBook']";
	public String lblShippingName = ".mini-shipment .address span[class='payment-name']";
	public String lblShippingAddress1 = ".mini-shipment .address span[class='payment-addr']:nth-child(2)";
	public String lblShippingAddress2 = ".mini-shipment .address span[class='payment-addr']:nth-child(3)";
	public String lblShippingCity = ".mini-shipment .address span[class='payment-city']";
	public String lblShippingPhone = ".mini-shipment .address span[class='formatPhone']";
	public String lblShippmentMethod = ".minishipments-method>span:nth-child(2)";
	public String lblBillingName = ".mini-billing-address .address span[class='payment-name']";
	public String lblBillingAddress1 = ".mini-billing-address .address span[class='payment-addr']:nth-child(2)";
	public String lblBillingAddress2 = ".mini-billing-address .address span[class='payment-addr']:nth-child(3)";
	public String lblBillingCity = ".mini-billing-address .address span[class='payment-city']";
	public String lblBillingPhone = ".mini-billing-address .address span[class='formatPhone']";

	/**********************************************************************************************
	 ********************************* WebElements of Credit card ****************************
	 **********************************************************************************************/

	public String defaultSavedCard = ".form-row.savedcardlist-row .selected-option";
	public String btnCardType = "#dwfrm_billing_paymentMethods_creditCard_type + .selected-option";
	public String txtNameOnCard = "input[id='dwfrm_billing_paymentMethods_creditCard_owner']";
	public String txtCardNo = "input[id*='dwfrm_billing_paymentMethods_creditCard_number']";
	public String btnCardExpMonth = ".payment-method-cont .cc_fields .month .selected-option";
	public String btnCardExpYear = ".payment-method-cont .cc_fields .year .selected-option";
	public String txtCvnNo = "input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']";
	public String chkSaveToPaymentMethod = "input[id='dwfrm_billing_paymentMethods_creditCard_saveCard']";
	private static final String btnContinueToPlaceOrder = "#primary button[name='dwfrm_billing_save']";

	/**********************************************************************************************
	 ********************************* WebElements of Belk Gift card ****************************
	 **********************************************************************************************/

	public String txtGiftCardNumber = "input[id='dwfrm_billing_giftCertCode']";
	public String txtGiftPinNumber = "input[id='dwfrm_billing_giftCardpin']";
	public String btnAddGiftCard = "button[id='add-giftcert']";
	public String btnCheckGiftCardBalance = "button[id='gcCheckbalance']";

	/**********************************************************************************************
	 ********************************* WebElements of Belk Reward dollars ****************************
	 **********************************************************************************************/

	public String txtCertificateNumber = "input[id='dwfrm_billing_brdNum']";
	public String btnApplyBelkRewardDollars = "button[id='add-brd']";

	/**********************************************************************************************
	 ********************************* WebElements of Ship to Multiple Address ****************************
	 **********************************************************************************************/

	public String txtMultipleShippingFirstname = "input[id='dwfrm_multishipping_editAddress_addressFields_firstName']";
	public String txtMultipleShippingLastname = "input[id='dwfrm_multishipping_editAddress_addressFields_lastName']";
	// public String btnMultipleShippingCountry =
	// "select[id='dwfrm_singleshipping_shippingAddress_addressFields_country']";
	public String txtMultipleShippingZipcode = "input[id='dwfrm_multishipping_editAddress_addressFields_postal']";
	public String txtMultipleShippingAddress = "input[id='dwfrm_multishipping_editAddress_addressFields_address1']";
	public String txtMultipleShippingCity = "input[id='dwfrm_multishipping_editAddress_addressFields_city']";
	public String btnMultipleShippingState = ".state-row .selected-option";
	public String txtMultipleShippingPhoneNo = "input[id='dwfrm_multishipping_editAddress_addressFields_phone']";
	public String btnMultipleShippingSave = "button[name='dwfrm_multishipping_editAddress_save']";

	// public String WrongCVN =".form-caption.error-message";

	public static final String BillingMethod = "#dwfrm_billing>legend";
	@FindBy(css = ".order-shipping>td:nth-child(2)") // .order-shipping>td:nth-child(2)
	// .order-shipping>td
	WebElement getShippingRate;

	@FindBy(css = "#dwfrm_addForm_useOrig")
	WebElement btnContinue;

	@FindBy(css = ".checkout-footer-middle")
	WebElement checkoutFooterMiddle;
	/**********************************************************************************************
	 ***********************************************************************************************/

	@FindBy(css = ".order-summary-footer.hide-desktop .button-fancy-large.submit-order-button")
	WebElement btnPlaceOrderMobile;

	@FindBy(css = ".order-summary-footer.hide-mobile .button-fancy-large.submit-order-button")
	WebElement btnPlaceOrderDesktop;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_useAsBillingAddress")
	WebElement Checkbox;

	@CacheLookup
	@FindBy(css = "button[name='dwfrm_login_login']")
	WebElement btnSignin;

	@FindBy(css = ".backto-cart>a")
	WebElement lnkReturnToShoppingBag;

	@FindBy(css = ".step-1")
	WebElement tabShipping;

	@FindBy(css = ".step-2")
	WebElement tabBilling;

	@FindBy(css = ".step-3")
	WebElement tabPlaceOrder;

	@FindBy(css = "button[name='dwfrm_singleshipping_shipToMultiple']")
	WebElement btnYesForMultipleAddressShipping;

	@FindBy(css = ".section-header a")
	WebElement lnkEditOrderSummary;

	@FindBy(css = ".name>a")
	List<WebElement> lstProductNamesInOrderSummary;

	@FindBy(css = ".ui-dialog")
	WebElement dialog;

	@FindBy(css = "#dwfrm_addForm_selMult>span")
	WebElement btnContinueInPopUp;

	@FindBy(css = "button[name='dwfrm_singleshipping_shippingAddress_save']")
	WebElement btnContinueToBill;

	@FindBy(css = "button[name='dwfrm_multishipping_shippingOptions_save']")
	WebElement btnContinueToBillInMultipleShipping;

	@FindBy(css = "div[class='form-row state-row required'] ul>li")
	List<WebElement> lstState;

	@FindBy(css = DEFAULT_STATE)
	WebElement defaultState;

	@FindBy(css = "div[class*='step-1'] > a")
	WebElement lnkShippingInHeader;

	@FindBy(css = "div[class*='step-1 active']")
	WebElement ShippingInHeaderEnabled;

	@FindBy(css = "div[class*='step-2 active']")
	WebElement BillingInHeaderEnabled;

	@FindBy(css = "div[class*='step-3 active']")
	WebElement BillingInHeaderEnabledInMultiAddress;

	@FindBy(css = "div[class*='step-4 active']")
	WebElement PlaceOrderInHeaderEnabledInMultiShipping;

	@FindBy(css = ".order-component-block .address")
	List<WebElement> shippingAddress;

	@FindBy(css = "div[class*='step-3 active']")
	WebElement PlaceOrderInHeaderEnabled;

	@FindBy(css = "div[class*='step-4']")
	WebElement verifyMultipleShipping;

	@FindBy(css = ".section-header")
	List<WebElement> lstSectionHeader;

	/**********************************************************************************************
	 ********************************* Shipping page Web Elements****************************
	 **********************************************************************************************/
	@FindBy(css = "a[class='section-header-note']")
	List<WebElement> editOrderSummary;

	@FindBy(css = "button[id='dwfrm_addForm_cancel']>span")
	WebElement btnCancelAddressValidationpop_up;

	@FindBy(css = "button[id='dwfrm_addForm_selectAddr']>span")
	WebElement btnContinueAddressValidationpop_up;

	@FindBy(css = "label[class='radio-label'] span[class='label-msg']")
	WebElement lblSuggestedAddressLabel;

	@FindBy(css = "label[class='radio-label original-address-radio'] span[class='label-msg']")
	WebElement lblOriginalAddressLabel;

	@FindBy(css = "div[class='line-items custom-scrollbar ps-container ps-theme-default']")
	WebElement productDetailsOrderSummary;

	@FindBy(css = "div[class='order-totals-table-cont']")
	WebElement pricingDetails;

	@FindBy(css = "tr[class='order-total']")
	WebElement estimatedOrderTotal;

	@FindBy(css = "tr[class='order-total']>td:nth-child(1)")
	WebElement estimatedOrderTotalLabel;

	@FindBy(css = "tr[class='order-total']>td:nth-child(2)")
	WebElement estimatedOrderTotalPrice;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress> fieldset > div.form-row.select-address > div > div > div")
	WebElement drpSavedAddressShipping;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressid']")
	WebElement txtShippingAddressName;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_firstName")
	WebElement txtFirstNameInShipping;

	@FindBy(css = ".cart-row.item-shipping-address>td")
	List<WebElement> lstMultiShippingAddress;

	@FindBy(css = ".brd-error")
	WebElement txtBRDError;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_lastName")
	WebElement txtLastNameInShipping;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_address1")
	WebElement txtAddress_1InShipping;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_address2")
	WebElement txtAddress_2InShipping;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_city")
	WebElement txtCityInShipping;

	@FindBy(css = ".state-row .selected-option")
	WebElement drpStateInShipping;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_postal")
	WebElement txtZipcodeInShipping;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_phone")
	WebElement txtPhoneInShipping;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_firstName")
	WebElement txtFirstNameInBilling;

	@FindBy(css = "input[id='dwfrm_billing_billingAddress_addressFields_lastName']")
	WebElement txtLastNameInBilling;

	@FindBy(css = "input[id='dwfrm_billing_billingAddress_addressFields_phone']")
	WebElement txtPhoneNoInBilling;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_owner")
	WebElement txtNameInCreditCard;

	@FindBy(css = "#shipping-method-Test_Shipment")
	WebElement radioTestShipment;

	@FindBy(css = "div[class='mini-shipment order-component-block  first  ']>h3 ")
	WebElement lblMiniShippingAddressInPlaceOrder;

	@FindBy(css = "div[class='nav summary']>h1")
	WebElement lblMiniOrderSummaryInPlaceOrder;

	@FindBy(css = "#shipping-method-Ground")
	WebElement radioStandard;

	@FindBy(css = "#shipping-method-APOF")
	WebElement radioAPOFPOStandard;

	@FindBy(css = "#shipping-method-2DAY")
	WebElement radioExpress;

	@FindBy(css = "#shipping-method-OVNT")
	WebElement radioOverNight;

	@FindBy(css = "div[class='content-asset']>h2")
	WebElement lblShopWithConfidence;

	@FindBy(xpath = "//ul[@class='checkout-footer-top']/li/a[contains(.,'Shipping Rates')]")
	WebElement lnkShippingRates;

	@FindBy(xpath = "//ul[@class='checkout-footer-top']/li/a[contains(.,'Easy Returns')]")
	WebElement lnkEasyReturns;

	@FindBy(xpath = "//ul[@class='checkout-footer-top']/li/a[contains(.,'FAQs')]")
	WebElement lnkSFAQs;

	@FindBy(xpath = "//ul[@class='checkout-footer-top']/li/a[contains(.,'Customer Service')]")
	WebElement lnkCustomerService;

	@FindBy(xpath = "//ul[@class='checkout-footer-top']/li/a[contains(.,'Privacy and Security Guarantee')]")
	WebElement lnkPrivacyandSecurityGuarantee;

	@FindBy(css = "div[class='mini-billing-address order-component-block']>div[class='details']>div>span")
	List<WebElement> BillingDetailsInPlaceOrder;

	@FindBy(css = "input[type='radio']:checked")
	WebElement slectedShippingType;

	@FindBy(xpath = ".//*[@id='dwfrm_billing']/fieldset[1]/div[4]/label/span[2]")
	WebElement firstNameInBilling;

	@FindBy(xpath = ".//*[@id='dwfrm_billing']/fieldset[1]/div[5]/label/span[2]")
	WebElement lastNameInBilling;

	@FindBy(xpath = ".//*[@id='dwfrm_billing']/fieldset[1]/div[5]/label/span[2]")
	WebElement addressInBilling;

	@FindBy(xpath = ".//*[@id='dwfrm_billing']/fieldset[1]/div[12]/label/span[2]")
	WebElement phn_numberInBilling;
	@FindBy(css = "div[class='form-row form-indent label-inline']")
	public List<WebElement> ShippingMethodslist;

	@FindBy(css = "label[class='radio-label original-address-radio']>input")
	WebElement chkOriginal;

	@FindBy(css = ".original-price")
	WebElement OrgPriceInBillingPageOnOrderSummary;

	@FindBy(css = ".now-price")
	WebElement NowPriceInBillingPageOnOrderSummary;

	@FindBy(css = ".order-shipping-discount")
	WebElement shippingDiscountInOrdersummary;

	/**********************************************************************************************
	 ********************** order summary in checkout page web elements****************************
	 **********************************************************************************************/

	@FindBy(css = ".checkout-order-totals .name>a")
	List<WebElement> lnkProductNameInOrderSummary;

	@FindBy(css = ".checkout-order-totals div[data-attribute='color']")
	List<WebElement> lblColor;

	@FindBy(css = ".checkout-order-totals div[data-attribute='size']")
	List<WebElement> lblSize;

	@FindBy(css = ".cardnumber-pi")
	WebElement lblCardNumberInApplyGiftCard;

	@FindBy(css = ".checkout-order-totals div[class='sku sku-qty']")
	List<WebElement> lblQuantity;

	@FindBy(css = "span[class='value qty-value']")
	WebElement lblQuantityValue;

	@FindBy(css = ".checkout-order-totals div[class='line-item-price']")
	List<WebElement> lblPrice;

	@FindBy(css = ".order-totals-table-cont .order-subtotal >td:nth-child(2)")
	WebElement lblMerchandiseTotalValue;

	@FindBy(css = ".order-totals-table-cont .order-shipsurcharge >td:nth-child(2)")
	WebElement lblShippingSurchargeValue;

	@FindBy(css = "div[class='order-totals-table-cont'] tr[class*='order-shipping'] >td:nth-child(2)")
	WebElement lblEstimatedShippingValue;

	@FindBy(css = "div[class='order-totals-table-cont'] tr[class='order-sales-tax'] >td:nth-child(2)")
	WebElement lblEstimatedSalesTaxValue;

	@FindBy(css = "div[class='order-totals-table-cont'] tr[class='order-total'] >td:nth-child(2)")
	WebElement lblOrderTotalValue;

	@FindBy(css = ".order-component-block .address")
	List<WebElement> shppingAddress;

	@FindBy(css = ".ps-scrollbar-y")
	WebElement scrollBarInBillingPage;

	@FindBy(css = "div[class='line-item mini-cart-product collapsed']:nth-child(1) .mini-cart-toggle")
	WebElement clikOnToggle;

	@FindBy(css = "div[ class='line-item mini-cart-product']")
	WebElement orderSummeryPane;

	@FindBy(css = "div[ class='product-list-item'] .name")
	List<WebElement> ProductCountInBillingPage;

	@FindBy(css = "div [class='standard-price']")
	WebElement priceTypeInBillingPageOnOrderSummary;

	@FindBy(css = ".order-brd>td:nth-child(2)")
	WebElement txtBelkRewardDollarAmount;
	/**********************************************************************************************
	 **********************************************************************************************/

	/**********************************************************************************************
	 ********************** Ship to Multiple Address in checkout page web
	 * elements****************************
	 **********************************************************************************************/

	@FindBy(css = ".item-image")
	List<WebElement> productImageInShipToMultipleAddress;

	@FindBy(css = ".checkoutmultishipping .name>a")
	List<WebElement> lnkProductNameInShipToMultipleAddress;

	@FindBy(css = ".checkoutmultishipping .edit")
	List<WebElement> lnkAddInShipToMultipleAddress;

	@FindBy(css = ".checkoutmultishipping .editaddress")
	List<WebElement> lnkEditInShipToMultipleAddress;

	@FindBy(css = ".checkoutmultishipping .selected-option")
	List<WebElement> selectAddressInShipToMultipleAddress;

	@FindBy(css = ".cart-promo span.value")
	List<WebElement> txtEmployeediscount;

	/**********************************************************************************************
	 **********************************************************************************************/

	/**********************************************************************************************
	 ********************** Address validation modal web elements****************************
	 **********************************************************************************************/

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.qas-dialog.ui-draggable")
	WebElement addressValidationModal;

	@FindBy(css = "button[id*='dwfrm_addForm_']")
	WebElement btnContinueInAddressValidationModal;

	@FindBy(css = "#deliver-options-home")
	WebElement rdoSuggestedAddress;

	@FindBy(css = "input[name='dwfrm_addForm_selection'][value='useOriginal']")
	WebElement rdoOriginalAddress;

	@FindBy(css = "#dwfrm_addForm_cancel")
	WebElement btnCancelInAddressValidationModal;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_useAsBillingAddress']")
	String chkUseBillingAddressAsAGuest;
	/**********************************************************************************************
	 **********************************************************************************************/
	@FindBy(css = ".partial-addr:nth-child(3)")
	WebElement txtSuggestedAddressCity;

	@FindBy(css = ".payment-city")
	WebElement txtShippingCityInBillingPanel;

	@FindBy(css = ".checkout-footer-top>li>a")
	List<WebElement> lnkFooter;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_type")
	WebElement drpCardType;

	@FindBy(css = "div[class='form-row cvn cvn-row required']>div[class='form-field-tooltip']>a")
	WebElement hoverOnwhatIsThis;

	@FindBy(css = "input[ id*='dwfrm_billing_paymentMethods_creditCard_owner']")
	WebElement clikOnNameOnCard;

	@FindBy(css = "input[ id*='dwfrm_billing_paymentMethods_creditCard_number']")
	WebElement clikOnCardNumber;

	@FindBy(css = "input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']")
	WebElement clikOnCvv;

	@FindBy(css = "span[id*='dwfrm_billing_paymentMethods_creditCard_owner-error']")
	WebElement nameOnCard;

	@FindBy(css = "span[ id*='dwfrm_billing_paymentMethods_creditCard_number']")
	WebElement CardNumber;

	@FindBy(css = "span[ id*='dwfrm_billing_paymentMethods_creditCard_cvn']")
	WebElement cvvOfCard;

	@FindBy(css = "	div[class='form-row cvn cvn-row required']>div[class='form-field-tooltip']>a[aria-describedby]")
	WebElement toolTips;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement txtEmailID;

	@FindBy(css = "div[id='secondary']>h3:nth-child(8)>a[class='section-header-note']")
	WebElement btnPaymentEdit;

	@FindBy(css = "#dwfrm_billing > fieldset > div.form-row.select-address > div > div > div")
	WebElement drpSavedBillingAddress;

	@FindBy(css = "div:not([class*='last'])>div>div>ul>li")
	List<WebElement> lstSavedAddreses;

	@FindBy(css = "#secondary > div.mini-payment-instrument.order-component-block > div > div.cc-type")
	WebElement txtCreditCardType;

	@FindBy(css = ".free-shipping")
	WebElement shippingType;

	@FindBy(css = "#secondary > div.mini-payment-instrument.order-component-block > div > div.cc-owner")
	WebElement txtNameOnCreditCardInPaymentMethod;

	@FindBy(css = "#secondary > div.mini-payment-instrument.order-component-block > div > div.cc-card-group .cc-number")
	WebElement txtCardNumberInPaymentMethod;

	@FindBy(css = ".details div:not([class])")
	WebElement txtGiftCardNumberInPaymentMethod;

	@FindBy(css = "#secondary > div.mini-payment-instrument.order-component-block > div > div.credit-cart-name .minibillinginfo-amount")
	WebElement txtAmountInPaymentMethod;

	@FindBy(css = "#dwfrm_billing > fieldset > div > div.payment-method.payment-method-expanded > div.form-row.required > div > div.custom-select.current_item > div")
	WebElement drpCreditCardType;

	@FindBy(css = "#secondary > div.mini-billing-address.order-component-block > div > div > span.payment-addr")
	WebElement txtBillingAddOne;

	@FindBy(css = "div[class='mini-shipment order-component-block  first  ']")
	WebElement shippingModule;

	@FindBy(css = "div[class='mini-shipment order-component-block  first  ']> h3 > a")
	WebElement shippingModuleEditLink;

	@FindBy(css = "div[class='mini-billing-address order-component-block']")
	WebElement billingModule;

	@FindBy(css = "div[class='mini-billing-address order-component-block'] >h3 >a")
	WebElement billingModuleEditLink;

	@FindBy(name = "dwfrm_multishipping_addressSelection_save")
	WebElement btnContinueInMultiShipping;

	@FindBy(css = ".payment-name")
	List<WebElement> lstFirstNameInAddressPane;

	@FindBy(css = ".cc-owner")
	WebElement lblNameOnCard;

	@FindBy(css = "div[class='order-summary-footer hide-mobile'] div[class='form-row'] a[class='back-to-cart simple'] ")
	WebElement btnCancelInPlaceOrderDesktop;

	@FindBy(css = "div[class='order-summary-footer hide-desktop'] div[class='form-row'] a[class='back-to-cart simple'] ")
	WebElement btnCancelInPlaceOrderMobile;

	@FindBy(css = ".cart-order-totals .cart-actions .cart-action-continue-shopping")
	WebElement btnContinueShopping;

	@FindBy(css = ".label-msg")
	public List<WebElement> lblShippingMethod;

	@FindBy(css = "#shipping-method-list>fieldset")
	WebElement shippingMethodSection;

	@FindBy(css = "footer[id='footer'] div[class='content-asset'] ul[class='checkout-footer-top']")
	WebElement fldShopWithConfidenceSection;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_address1")
	WebElement txtAddress_1InBilling;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_address2")
	WebElement txtAddress_2InBilling;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_city")
	WebElement txtCityInBilling;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_postal")
	WebElement txtZipcodeInBilling;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_expiration_month")
	WebElement drpMonth;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_type")
	WebElement drpYear;

	@FindBy(css = ".button-fancy-large.submit-order-button")
	WebElement btnPlaceOrderInPlaceOrderPage;

	@FindBy(css = "#secondary > div.mini-billing-address.order-component-block > h3>a")
	WebElement lnkEditBillAddress;

	@FindBy(css = ".subtotal-label.bold")
	WebElement txtSubtotal;

	@FindBy(css = ".name>a")
	WebElement txtProductName;

	@FindBy(css = "div[class='product-list-item']>div[data-attribute='color']>span[class='value']")
	WebElement txtColor;

	@FindBy(css = "div[class='product-list-item']>div[data-attribute='size']>span[class='value']")
	WebElement txtSize;

	@FindBy(css = "div[class='item-quantity']")
	WebElement drpQuantity;

	@FindBy(css = ".backto-cart>a")
	WebElement ReturnToshipPage;

	@FindBy(css = "div[class='mini-billing-address order-component-block']>div[class='details']>div[class='address']>span[class='payment-name']")
	WebElement BillingName;

	@FindBy(css = "div[class='mini-billing-address order-component-block']>div[class='details']>div[class='address']>span[class='formatPhone']")
	WebElement BillingPhone;

	@FindBy(css = "#shipping-method-list>fieldset")
	WebElement shippmentMethodSection;

	@FindBy(css = "#primary button[name='dwfrm_billing_save']")
	WebElement continueInBilling;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_saveCard")
	WebElement chkSaveCard;

	@FindBy(css = "#primary button[name='dwfrm_billing_save']")
	WebElement btnContinutToPlaceOrder;

	@FindBy(css = ".payment-method-cont div[data-method='CREDIT_CARD'] .selected-option")
	WebElement btnPaymentCardType;

	@FindBy(css = "div[class='form-row form-row-button checkout-continue']>.button-fancy-large")
	WebElement btnInBilling;

	@FindBy(css = "#secondary > div.mini-shipment.order-component-block.first > h3 > a")
	WebElement LnkEditShipAddress;

	@FindBy(css = "#secondary > div.mini-shipment.order-component-block.first > div > div.address > span.payment-phone")
	WebElement ShippiningPhone;

	@FindBy(css = "#secondary div[class*='mini-payment-instrument order-component-block']")
	WebElement txtPaymentDetails;

	@FindBy(css = ".ship-to-multiple.clear-border>span")
	WebElement lblShipToMultipleAddress;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress>fieldset")
	WebElement fldShippingAddressSection;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_city-error")
	WebElement errMsgCityInShipping;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_postal-error")
	WebElement errMsgZipcodeInShipping;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_phone-error")
	WebElement errMsgPhoneInShipping;

	@FindBy(css = "#secondary > div.mini-billing-address.order-component-block > div > div > span.payment-name")
	WebElement txtBillingAddName;

	@FindBy(css = "#secondary > div.mini-billing-address.order-component-block > div > div > span.payment-city")
	WebElement txtBillingAddCity;

	@FindBy(css = "#dwfrm_billing > fieldset:nth-child(5) > div > legend")
	WebElement btnEmptyspace;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_phone")
	WebElement billingPhoneInBillingPage;

	@FindBy(css = ".form-caption.error-message")
	WebElement WrongCVN;

	@FindBy(css = ".selected-option.selected")
	WebElement selectList;

	@FindBy(css = "div[class='custom-select current_item']>ul>li")
	List<WebElement> lstSelect;

	@FindBy(css = ".selection-list>li:nth-child(3)")
	WebElement overNight;

	@FindBy(css = ".selection-list>li:nth-child(2)")
	WebElement express;

	@FindBy(css = ".payment-name")
	WebElement billingPageShippingName;

	@FindBy(css = "#secondary > div.mini-shipment.order-component-block.first > div > div.address > span.payment-addr")
	WebElement billingPageShippingAddress1;

	@FindBy(css = "#secondary > div.mini-shipment.order-component-block.first > div > div.address > span.payment-city")
	WebElement billingPageShippingCity;

	@FindBy(css = ".payment-phone")
	WebElement billingPageShippingPhone;

	@FindBy(css = "#scrollbar-0 > div.line-item.mini-cart-product.collapsed > div.line-item-details > div > div.name > a")
	WebElement txtProductNameInShipping;

	@FindBy(css = "#scrollbar-0 > div.line-item.mini-cart-product.collapsed > div.line-item-details > div > div:nth-child(6) > span.value")
	WebElement txtSizeInShipping;

	@FindBy(css = "#scrollbar-0 > div.line-item.mini-cart-product.collapsed > div.line-item-details > div > div:nth-child(5) > span.value")
	WebElement txtColorInShipping;

	@FindBy(css = "#scrollbar-0 > div.line-item.mini-cart-product.collapsed > div.line-item-price > div")
	WebElement txtSubtotalInShipping;

	@FindBy(css = ".address>span:nth-child(1)")
	WebElement txtShippingName;

	@FindBy(css = ".address>span:nth-child(2)")
	WebElement txtAddressOne;

	@FindBy(css = ".address>span:nth-child(3)")
	WebElement txtShippingCity;

	@FindBy(css = ".address>span:nth-child(4)")
	WebElement txtShippingPhoneNo;

	@FindBy(css = ".address>span:nth-child(1)")
	WebElement txtBillingFirstName;

	@FindBy(css = ".address>span:nth-child(2)")
	WebElement txtBillingAddress;

	@FindBy(css = ".address>span:nth-child(3)")
	WebElement txtBillingAddCityInOS;

	@FindBy(css = ".address>span:nth-child(4)")
	WebElement txtBillinghoneNo;

	@FindBy(css = "#gcCheckbalance")
	WebElement btnCheckGiftCard;

	@FindBy(css = ".gift_card-balance.success")
	public WebElement txtGiftCardMessage;

	@FindBy(css = ".loader[style*='block']")
	WebElement PDPspinner;

	@FindBy(css = "div[class='gift_card-balance success']")
	WebElement txtbalanceGiftCard;

	@FindBy(css = "div[class='gift-cert-used form-indent info']")
	WebElement txtNoBalanceAvailableforAppliedGiftCard;

	@FindBy(css = ".giftcert-pi")
	WebElement fldAppliedGiftCard;

	@FindBy(css = "span[class='change-preferred-store']")
	WebElement changeStoreLnk;

	@FindBy(css = "div[class='ship-to-multiple clear-border']")
	WebElement fldShiptoMultiple;

	@FindBy(css = BillingMethod)
	WebElement fldAddBillingMethod;

	@FindBy(css = "div[class='billing-coupon-code'] legend")
	WebElement lblBelkGiftCard;

	@FindBy(css = "div[class='brd-cont'] legend")
	WebElement lblBelkRewardDollar;

	@FindBy(css = ".order-shipsurcharge>td:nth-child(2)")
	WebElement fldShippingSurCharge;

	@FindBy(css = ".order-subtotal>td")
	List<WebElement> fldMerchandiseTotal;

	@FindBy(css = ".order-total>td")
	List<WebElement> fldEstimatedTotal;

	@FindBy(css = ".order-total>td")
	List<WebElement> fldOrderTotal;

	@FindBy(css = ".item-image>img")
	WebElement imgItem;

	@FindBy(css = ".summary-product-header")
	WebElement lblProductHeader;

	/***********************************/

	@FindBy(css = ".form-caption.error-message")
	WebElement lblCreditCardError;

	@FindBy(css = ".order-shipping>td:nth-child(2)")
	WebElement ShipCost;

	@FindBy(css = "#secondary > div.mini-shipment.order-component-block.first > div > div.minishipments-method > span:nth-child(2)")
	WebElement Shipmethod;

	@FindBy(css = "#order-shipment-tablelist > tbody > tr.order-shipment-table > td > div.shipping-method > span.value")
	WebElement ShipMethodInOrderRecipt;

	@FindBy(css = "#order-summary > tbody > tr > td.payment-meth > div > div > div > table > tbody > tr.order-shipping > td:nth-child(2)")
	WebElement ShipCostInorderReceipt;

	@FindBy(css = ".custom-select div>span")
	List<WebElement> shippingmethodInMultiShipping;

	@FindBy(css = ".order-subtotal td:nth-child(2)")
	WebElement txtMerchandiseTotalCost;

	@FindBy(css = ".order-discount.discount >td:nth-child(2)")
	WebElement txtTotalCouponSavings;

	@FindBy(css = ".order-shipping td:nth-child(2)")
	WebElement txtShippingCost;

	@FindBy(css = ".order-sales-tax td:nth-child(2)")
	WebElement txtEstimatedSalesTaxCost;

	@FindBy(css = ".order-value")
	WebElement txtTotalOrderValure;

	@FindBy(css = ".order-shipsurcharge>td:nth-child(2)")
	WebElement txtSurchargeValue;

	@FindBy(css = ".order-giftbox>td:nth-child(2)")
	WebElement txtGiftBoxValue;

	@FindBy(css = ".express-checkout.details")
	WebElement txtPaymentDetailsInExpressCheckout;

	@FindBy(css = "input[id*='dwfrm_billing_paymentMethods_creditCard_number']")
	WebElement txtfieldCreditCard;

	@FindBy(css = ".secondarybutton")
	WebElement btnApplyInPlaceOrderPage;

	@FindBy(css = "span[id*='dwfrm_billing_paymentMethods_creditCard_cvn_']")
	WebElement CvvErrorMsg;

	@FindBy(css = ".order-summary-footer.hide-mobile .button-fancy-large.submit-order-button")
	WebElement btnPlaceOrderdesktop;

	@FindBy(css = ".order-summary-footer.hide-desktop .button-fancy-large.submit-order-button")
	WebElement btnPlaceOrdermobile;

	@FindBy(css = "button[name='dwfrm_billing_redeemGiftCard']")
	WebElement lnkApplyGiftCard;

	@FindBy(css = ".expresscard-links>a:nth-child(2)")
	WebElement lnkApplyRewardCard;

	@FindBy(css = "ul.selection-list")
	WebElement lstShipppingMethods;

	@FindBy(css = ".custom-select")
	WebElement drpDownShipppingMethods;

	@FindBy(css = "div[class='std-shipping-only'] a")
	WebElement lnkWhyStandardShipping;

	@FindBy(css = "button[title='Close']")
	WebElement closeShippingModelPopup;

	@FindBy(css = "#dialog-container")
	WebElement standardShippingModelPopUp;

	@FindBy(css = "button[class='close-dialog']")
	WebElement btnContinueInStandardShippingInPopup;

	@FindBy(css = "div[class='bonus-product-bottom-msg'] a")
	WebElement lnkGoToShoppingBagINpopup;

	@FindBy(css = "div[class='mini-expresspayment-instrument order-component-block']")
	WebElement txtPaymentDetailsInMiniExpressCheckout;

	@FindBy(css = ".cc-type")
	WebElement lblCardType;

	@FindBy(css = ".cc-number")
	WebElement txtCardNumber;

	@FindBy(css = txtFirstname)
	WebElement txtFldFirstName;

	@FindBy(css = txtLastname)
	WebElement txtFldLastName;

	@FindBy(css = "input[class='input-text postal required valid']")
	WebElement txtFldZipCode;

	@FindBy(css = "#dwfrm_addForm_useOrig")
	WebElement shippingModalPopUpValidationbox;

	@FindBy(css = ".button-fancy-large")
	WebElement btnContinueNaviageteToBillingPage;

	@FindBy(css = ".mini-shipment .address span[class='payment-name']")
	WebElement lblShippingPanelName;

	@FindBy(css = "#rgc-6006110000000025013>span")
	WebElement btnRemoveGiftCard;

	@FindBy(css = ".transactionamount-pi")
	WebElement lblTransAmtForGiftCard;

	@FindBy(css = ".payment-method-cont>legend")
	WebElement lblPaymentMethod;

	@FindBy(css = ".mini-shipment .address span[class='payment-addr']")
	WebElement lblShipingPanelAddress;

	@FindBy(css = ".mini-shipment .address span[class='payment-city']")
	WebElement lblShippingPanelCity;

	@FindBy(css = ".mini-shipment .address span[class='payment-phone']")
	WebElement lblShippmenPaneltMethod;

	@FindBy(css = "div[class='mini-shipment order-component-block  first  ']>div>div[class='address']")
	WebElement shippngAddress1;

	@FindBy(css = "div[class='mini-shipment order-component-block  last ']>div>div[class='address']")
	WebElement shippngAddress2;

	@FindBy(css = ".mini-billing-address .section-header")
	WebElement lblMiniBillingAddressInPlaceOrder;

	@FindBy(css = ".section-header>span")
	WebElement txtPaymentMethod;

	@FindBy(css = "[class='order-value']")
	WebElement orderValue;

	@FindBy(css = "div[class='enter_partial']")
	WebElement shippingAddressInPopUpAddressValidation;

	@FindBy(css = "[class='partial-addr']:nth-child(3)")
	WebElement suggestedAddress;

	@FindBy(css = "div[class='enter_partial'] input[name='zip']")
	WebElement suggestedAddressZip;

	@FindBy(xpath = ".//*[@id='secondary']/h3[1]")
	WebElement navigateOrderSummary;

	@FindBy(xpath = ".//*[@id='secondary']/div[2]/h3")
	WebElement navigateShippingAddress;

	@FindBy(xpath = ".//*[@id='secondary']/h3[2]")
	WebElement navigatePaymentMethod;

	@FindBy(xpath = ".//*[@id='secondary']/h3[3]")
	WebElement navigatePaymentMethodDollar;

	@FindBy(css = ".payment-city")
	WebElement PincodeAddress;

	@FindBy(css = "#dwfrm_billing_giftCertCode")
	WebElement belkGiftCard;

	@FindBy(css = "#dwfrm_billing_giftCardpin")
	WebElement belkGiftCardCvv;

	@FindBy(css = rdoStandard)
	WebElement standardShipping;

	@FindBy(css = "div[class='checkoutmultishipping-methods']>table:nth-child(1)>tbody>tr:nth-child(1)>td")
	WebElement shipMethdInStorePickUp;

	@FindBy(css = "div[class='checkoutmultishipping-methods']>table:nth-child(1)>tbody>tr:nth-child(3)")
	WebElement addrInStorePickUp;

	// @FindBy(css = ".details .credit-cart-name .minibillinginfo-amount")
	@FindBy(css = "div.details:contains('Gift Card') .minibillinginfo-amount")
	WebElement lblAmount;

	@FindBy(css = ".checkoutmultishipping .cart-row")
	List<WebElement> lstProductInMultiShip;

	@FindBy(css = ".mini-shipment .address span[class='formatPhone']")
	WebElement lblShippmenPanelPhone;

	@FindBy(css = ".mini-shipment .minishipments-method")
	WebElement lblShipmentMethod;

	@FindBy(css = "div[class*='step-1'] span[class='hide-mobile']")
	WebElement txtheadingShippingOrder;

	@FindBy(css = ".checkout-summary-error .error")
	WebElement errMsgCheckoutSummary;

	@FindBy(css = ".cc-payment-amount")
	WebElement txtCardPaymnet;

	@FindBy(css = ".promo")
	WebElement lblPromo;

	@FindBy(css = ".order-totals-table-cont .order-discount.discount >td:nth-child(2)")
	WebElement lblAssociateDiscountValue;

	@FindBy(css = "#secondary > h3:nth-child(8) > a")
	WebElement lnkPaymentEdit;

	@FindBy(css = "#secondary > h3:nth-child(2) > a")
	WebElement lnkOrdersummaryEdit;

	@FindBy(css = ".brd-error.error")
	WebElement errMsgBRD;

	@FindBy(css = "div[style*='block'].brd-error")
	public WebElement errMsgBRDdisplayed;

	@FindBy(css = ".giftcert-error")
	public WebElement errMsgGiftCard;

	@FindBy(css = ".free-shipping")
	WebElement shiipingCostBelowtotalCost;

	@FindBy(css = ".checkoutmultishipping-methods .selected-option.selected")
	List<WebElement> lstShippingMethods;

	@FindBy(css = ".item-list")
	List<WebElement> lstShippingMethodDiv;

	// @FindBy(css=btnContinueToPlaceOrder)
	// WebElement btnContinuePlaceOrder;

	@FindBy(css = "li.remove-pi>a")
	WebElement lnkRemoveGiftCard;

	@FindBy(css = "table tbody > tr.cart-row.product-row > td.item-details > div > div.name > a")
	List<WebElement> lstProductName;

	@FindBy(css = "table tbody > tr.cart-row.product-row > td.item-image img")
	List<WebElement> lstProductthumbnail;

	@FindBy(css = "table tbody > tr.cart-row.product-row > td.item-details div:nth-child(3) > span.value")
	List<WebElement> lstProductcolor;

	@FindBy(css = "table tbody > tr.cart-row.product-row > td.item-details div:nth-child(4) > span.value")
	List<WebElement> lstProductSize;

	@FindBy(css = "table tbody > tr.cart-row.product-row > td.item-details div.sku.sku-qty > span.value.qty-value")
	List<WebElement> lstProductQty;

	@FindBy(css = "tr.cart-row.shippingInfo-row > td > div > div.selected-option.selected > span")
	WebElement txtShippingMethods;

	@FindBy(css = "div[class='item-links gift-icon']")
	WebElement lblRegistryName;

	@FindBy(css = ".order-totals-table-cont>table>tbody>tr[class='order-shipping']>td:nth-child(2)")
	WebElement TypeShipping;

	@FindBy(css = btnContinueToPlaceOrder)
	WebElement btnContinuePlaceOrder;

	@FindBy(css = "[id='dwfrm_singleshipping_addressList']")
	WebElement wAddress;

	@FindBy(css = ".radio-label.original-address-radio>input")
	WebElement chkOriginalInMultiShip;

	@FindBy(css = ".mini-shipment")
	List<WebElement> shippingAddresses;

	@FindBy(css = ".expresscard-links>a:nth-child(1)")
	WebElement lnkApplyGiftCardInExpressCheckout;

	@FindBy(css = ".expresscard-links>a:nth-child(2)")
	WebElement lnkApplyRewardCardInExpressCheckOut;

	@FindBy(css = "#dwfrm_billing > fieldset:nth-child(6) div.giftcert-error.error")
	WebElement txtgiftcarderrormessage;

	@FindBy(css = ".selection-list>li")
	public List<WebElement> lstAdd;

	@FindBy(css = ".cart-promo")
	WebElement lblDiscountMessage;

	@FindBy(id = "dwfrm_billing_brdNum")
	WebElement txtBRDNumberInInputField;

	@FindBy(css = ".billing-coupon-code .reward-message")
	WebElement lblRewardMsg;

	@FindBy(css = "button[id='add-giftcert']")
	WebElement btnGiftCard;

	@FindBy(css = ".brd-success")
	public WebElement brdSuccessMsg;

	@FindBy(css = ".payment-method-cont")
	WebElement sectionPaymentMethod;

	@FindBy(id = "div.backtoshop.cusumer-auth > a")
	WebElement btnReturntoShopping;

	@FindBy(css = ".primary-logo>a")
	WebElement belkLogo;

	@FindBy(css = "div[class='name']>a")
	WebElement productName;

	@FindBy(css = ".total-price")
	List<WebElement> lblPriceSectionInProduct;

	@FindBy(css = ".price-coupon")
	WebElement txtCoupon;

	@FindBy(css = ".cart-row.item-shipping-address")
	List<WebElement> multiShippingAddress;

	@FindBy(css = ".brd-list .cardnumber-pi")
	WebElement lblCardNumberInBRD;

	@FindBy(css = ".brd-error.error.hide")
	WebElement brdErrorMessage;

	@FindBy(css = ".brd-list .remove-pi>a")
	WebElement btnRemoveInBRD;

	@FindBy(css = ".brd-list .transactionamount-pi")
	WebElement trnsctnAmount;

	@FindBy(css = "div[class*='address-dialog']")
	WebElement dialogMultiShippingAddress;

	@FindBy(css = "#dwfrm_singleshipping_addressList")
	WebElement drpSavedAddress;

	@FindBy(css = "div[class='selected-option selected']")
	List<WebElement> drpSelectedOptions;

	@FindBy(css = "div[class='product-list-item'] div[class='promo']")
	WebElement txtSurchargeAppliedMsg;

	@FindBy(css = ".checkoutmultishipping tr.instore-shipment td.shippingaddress div:nth-child(1)")
	WebElement txtInStorePickupFree;

	@FindBy(css = ".checkoutmultishipping tr.instore-shipment td.shippingaddress div:nth-child(2)")
	WebElement txtInStoreName;

	@FindBy(css = ".checkoutmultishipping tr.instore-shipment td.shippingaddress div:nth-child(3)")
	WebElement txtInStorePickupAddress;

	@FindBy(css = ".checkoutmultishipping tr.instore-shipment td.shippingaddress div:nth-child(4)")
	WebElement txtInStorePhone;

	@FindBy(css = "tr.order-discount td:nth-child(1)")
	List<WebElement> lstorderdiscount;

	@FindBy(css = ".checkoutmultishipping tr.instore-shipment td.shippingaddress div:nth-child(5)")
	WebElement txtInStoreState;

	@FindBy(xpath = "//span[.='Payment Method #1']/../following-sibling::div[1]//span[.='Belk Reward Dollars:']")
	WebElement paymentBRD;

	@FindBy(xpath = "//span[.='Payment Method #2']/../following-sibling::div[1]//span[.='Gift Card:']")
	WebElement paymentCreditCard;

	@FindBy(xpath = "//span[.='Payment Method #3']/../following-sibling::div[1]//div[.='Belk Reward Credit Card']")
	WebElement paymentBRC;

	@FindBy(css = "div[class='brd-success success hide']")
	WebElement msgAppliedBelkRewardDollars;

	@FindBy(css = "[class='brd-error error hide']")
	WebElement msgInvalidBelkRewardDollar;

	@FindBy(css = "div[class='error brd-creditcard-error hide']")
	WebElement msgErrorBelkReward;

	/********************************************************************************/
	public CheckoutPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, tabShipping))) {
			Log.fail("Checkout Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);
	}// isLoaded

	@Override
	protected void load() {

		Utils.waitForPageLoad(driver);
		isPageLoaded = true;

	}// load

	public LinkedHashMap<String, String> fillingShippingDetailsAsGuest(String useAddressForShipping,
			String shippingmethod) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaFirst" + randomFirstName, txtFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_lastname_QaLast" + randomLastName, txtLastname);

		String address = checkoutProperty.getProperty(useAddressForShipping);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if (useAddressForShipping.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}

		shippingDetails.put("type_address_" + address1, txtAddress);
		if (useAddressForShipping.contains("_1"))
			shippingDetails.put("type_address1_" + address2, txtAddress2);
		shippingDetails.put("type_city_" + city, txtCity);
		shippingDetails.put("select_state_" + state, btnState);
		shippingDetails.put("type_zipcode_" + zipcode, txtZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtPhoneNo);
		WebElement element = null;
		if (shippingmethod != null) {
			switch (shippingmethod) {
			case "TestShipment":
				shippingDetails.put("check_Shipping method_YES", rdoTestShipment);
				element = radioTestShipment;
				break;
			case "Standard":
				if (city.contains("APO")) {
					shippingDetails.put("check_Shipping method_YES", rdoAPOFPOStandard);
					element = radioAPOFPOStandard;
				} else {
					shippingDetails.put("check_Shipping method_YES", rdoStandard);
					element = radioStandard;
				}
				break;
			case "Express":
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				element = radioExpress;
				break;
			case "Overnight":
				shippingDetails.put("check_Shipping method_YES", rdoOvernight);
				element = radioOverNight;
				break;

			}
		}

		// shippingDetails.put("click_Clicking Continue to click billing
		// button",btnContinueToBilling
		// );
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		shippingDetails.remove("check_Shipping method_YES");
		element = element.findElement(By.xpath("../..")).findElement(By.cssSelector(".label-msg"));
		shippingDetails.put("Shipping_method", BrowserActions.getText(driver, element, "Shipping method"));
		Log.event("Entered Shipping Details as a Guest user", StopWatch.elapsedTime(startTime));
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
		return shippingDetails;

	}

	public LinkedHashMap<String, String> fillingBillingDetailsAsGuest(String useAddressForBilling) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaFirst" + randomFirstName, txtBillingFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaLast" + randomLastName, txtBillingLastname);

		String address = checkoutProperty.getProperty(useAddressForBilling);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];
		String email = address.split("\\|")[5];

		if (useAddressForBilling.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
			email = address.split("\\|")[6];
		}

		billingDetails.put("type_address_" + address1, txtBillingAddressOne);
		if (useAddressForBilling.contains("_1"))
			billingDetails.put("type_address2_" + address2, txtBillingAddressTwo);
		billingDetails.put("type_city_" + city, txtBillingCity);
		billingDetails.put("select_state_" + state, btnBillingState);
		billingDetails.put("type_zipcode_" + zipcode, txtBillingZipcode);
		billingDetails.put("type_phoneno_" + phoneNo, txtBillingPhoneNo);
		billingDetails.put("type_phoneno_" + phoneNo, txtBillingPhoneNo);
		billingDetails.put("type_email_" + email, txtBillingEmail);

		BillingPageUtils.enterBillingDetails(billingDetails, driver);
		Log.event("Filled Billing Details as a Guest user", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return billingDetails;

	}

	public OrderConfirmationPage placeOrder() throws Exception {
		BrowserActions.nap(5);
		Utils.waitForPageLoad(driver);
		final long startTime = StopWatch.startTime();
		if (Utils.getRunPlatForm().equals("mobile"))
			BrowserActions.javascriptClick(btnPlaceOrderMobile, driver, "Palce order button");
		else
			PaymentUtils.submitOrder(btnPlaceOrderDesktop, driver);
		Log.event("Clicked Place order button", StopWatch.elapsedTime(startTime));

		if (Utils.waitForElement(driver, errMsgCheckoutSummary))
			Log.fail("Cannot Place order. Error : " + errMsgCheckoutSummary.getText().trim(), driver);
		return new OrderConfirmationPage(driver).get();
	}

	public LinkedHashMap<String, String> fillingShippingDetailsAsSignedInUser(String saveToAddressBook,
			String useAddressForBilling, String shippingmethod, String customerDetails) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		shippingDetails.put("type_Addres Name_Home", txtAddressName);
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaFirst" + randomFirstName, txtFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaLast" + randomLastName, txtLastname);

		String address = checkoutProperty.getProperty(customerDetails);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if (customerDetails.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}

		shippingDetails.put("type_address_" + address1, txtAddress);
		if (customerDetails.contains("_1"))
			shippingDetails.put("type_address1_" + address2, txtAddress2);
		shippingDetails.put("type_city_" + city, txtCity);
		shippingDetails.put("select_state_" + state, btnState);
		shippingDetails.put("type_zipcode_" + zipcode, txtZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtPhoneNo);
		if (saveToAddressBook.equals("YES")) {
			shippingDetails.put("check_Save to Address Book_YES", chkSaveToAddressBook);
		}
		if (useAddressForBilling.equals("YES")) {
			shippingDetails.put("check_Use Address For Billing_YES", chkUseThisAddressForBilling);
		}
		WebElement element = null;
		if (shippingmethod != null) {
			switch (shippingmethod) {
			case "TestShipment":
				shippingDetails.put("check_Shipping method_YES", rdoTestShipment);
				element = radioTestShipment;
				break;
			case "Standard":
				if (city.contains("APO")) {
					shippingDetails.put("check_Shipping method_YES", rdoAPOFPOStandard);
					element = radioAPOFPOStandard;
				} else {
					shippingDetails.put("check_Shipping method_YES", rdoStandard);
					element = radioStandard;
				}
				break;
			case "Express":
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				element = radioExpress;
				break;
			case "Overnight":
				shippingDetails.put("check_Shipping method_YES", rdoOvernight);
				element = radioOverNight;
				break;

			}
		}

		// shippingDetails.put("click_Clicking Continue to click billing
		// button",btnContinueToBilling
		// );
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		shippingDetails.remove("check_Shipping method_YES");
		// element =
		// element.findElement(By.xpath("../..")).findElement(By.cssSelector(".label-msg"));
		// shippingDetails.put("Shipping_method", BrowserActions.getText(driver,
		// element, "Shipping method"));
		Log.event("Filled Shipping Details as a Guest user", StopWatch.elapsedTime(startTime));
		/*
		 * if (Utils.waitForElement(driver, dialog)) {
		 * BrowserActions.clickOnElement(btnContinueInAddressValidationModal,
		 * driver,"click_Clicking Continuce button in Dialog");
		 * BrowserActions.clickOnElement(btnContinueToBilling, driver,
		 * "Continue to click billing button"); }
		 */
		return shippingDetails;

	}

	public void selectState() throws Exception {
		int rand = Utils.getRandom(0, lstState.size());
		BrowserActions.clickOnElement(defaultState, driver, "Default state");
		BrowserActions.clickOnElement(lstState.get(rand), driver, "state option");
	}

	public void clickOnContinueInShipping() throws Exception {
		if (Utils.waitForElement(driver, verifyMultipleShipping)) {
			BrowserActions.scrollToView(btnContinueToBillInMultipleShipping, driver);
			BrowserActions.clickOnElement(btnContinueToBillInMultipleShipping, driver, "Continue button in shipping");
		} else {
			BrowserActions.scrollToView(btnContinueToBill, driver);
			BrowserActions.clickOnElement(btnContinueToBill, driver, "Continue button in shipping");
		}
		Utils.waitForPageLoad(driver);
	}

	public void clickOnShippingInHeader() throws Exception {
		BrowserActions.clickOnElement(lnkShippingInHeader, driver, "Continue button in shipping");
		Utils.waitForPageLoad(driver);
	}

	public boolean ContinueAddressValidationModalWithDefaults() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.clickOnElement(btnContinueInAddressValidationModal, driver,
					"Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);
			clickOnContinueInShipping();
			return true;
		}
		return false;
	}

	public boolean isAddressValidationModalShowing() throws Exception {
		return Utils.waitForElement(driver, addressValidationModal);
	}

	public String getFirstNameInShippingAddress() throws Exception {
		return txtFirstNameInShipping.getText();
	}

	public LinkedHashMap<String, String> fillingBillingDetailsAsSignedInUser(String saveToAddressBook,
			String useAddressForBilling, String customerDetails) throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		billingDetails.put("type_Addres Name_Home", txtBillingAddressname);

		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaFirst" + randomFirstName, txtBillingFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaLast" + randomLastName, txtBillingLastname);

		String address = checkoutProperty.getProperty(customerDetails);
		String address1 = address.split("\\|")[0];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];
		String address2 = new String();
		if (customerDetails.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}

		billingDetails.put("type_address_" + address1, txtBillingAddressname);
		billingDetails.put("type_address_" + address1, txtBillingAddressOne);
		if (customerDetails.contains("_1"))
			billingDetails.put("type_address2_" + address2, txtBillingAddressTwo);
		billingDetails.put("type_city_" + city, txtBillingCity);
		billingDetails.put("select_state_" + state, btnBillingState);
		billingDetails.put("type_zipcode_" + zipcode, txtBillingZipcode);
		billingDetails.put("type_phoneno_" + phoneNo, txtBillingPhoneNo);
		// BillingPageUtils.enterBillingDetails(billingDetails, driver);

		if (saveToAddressBook.equals("YES")) {
			billingDetails.put("check_Save to Address Book_YES", chkSaveToBillingAddressBook);
		}
		if (useAddressForBilling.equals("YES")) {
			billingDetails.put("check_Use Address For Billing_YES", chkBillingEmail);
		}
		BillingPageUtils.enterBillingDetails(billingDetails, driver);
		Log.event("Filled Billing Address Details");
		return billingDetails;
	}

	public LinkedHashMap<String, String> fillingBillingDetailsAsGuestUser(String saveToAddressBook,
			String customerDetails) throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		billingDetails.put("type_Addres Name_Home", txtBillingFirstname);

		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaFirst" + randomFirstName, txtBillingFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		billingDetails.put("type_firstname_QaLast" + randomLastName, txtBillingLastname);

		String address = checkoutProperty.getProperty(customerDetails);
		String address1 = address.split("\\|")[0];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];
		String email = address.split("\\|")[5];

		billingDetails.put("type_address_" + address1, txtBillingAddressOne);
		// billingDetails.put("type_address_a", txtBillingAddressTwo);
		billingDetails.put("type_city_" + city, txtBillingCity);
		billingDetails.put("select_state_" + state, btnBillingState);
		billingDetails.put("type_zipcode_" + zipcode, txtBillingZipcode);
		billingDetails.put("type_phoneno_" + phoneNo, txtBillingPhoneNo);
		billingDetails.put("type_email_" + email, txtBillingEmail);
		BillingPageUtils.enterBillingDetails(billingDetails, driver);

		if (saveToAddressBook.equals("YES")) {
			billingDetails.put("check_Save to Address Book_YES", chkBillingEmail);
		}
		BillingPageUtils.enterBillingDetails(billingDetails, driver);
		Log.event("Filled Billing Address Details");
		return billingDetails;
	}

	public LinkedHashMap<String, String> fillingCardDetails(String SaveToPaymentMethod, String customerDetails,
			String... iteration) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
		String cardExpireMonth = "";
		String cardExpireYear = "";
		String cardCVN = "";

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardExpireMonth = cutomercardDetails.split("\\|")[3];
			cardExpireYear = cutomercardDetails.split("\\|")[4];
			cardCVN = cutomercardDetails.split("\\|")[5];
		}
		if (cardType.trim().equals("Belk Rewards Credit Card")) {
			cardCVN = cutomercardDetails.split("\\|")[3];
		}

		if (iteration.length > 0) {
			cardDetails.put("select1_cardtype_" + cardType, btnCardType);
		} else {
			cardDetails.put("select_cardtype_" + cardType, btnCardType);
		}

		cardDetails.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetails.put("type_cardno_" + cardNo, txtCardNo);

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardDetails.put("select_expmonth_" + cardExpireMonth, btnCardExpMonth);
			cardDetails.put("select_expyr_" + cardExpireYear, btnCardExpYear);
			cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);
		} else {
			cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);
		}

		if (SaveToPaymentMethod.equals("YES")) {
			// cardDetails.put("check_Use_Save to Payment_YES",
			// chkSaveToPaymentMethod);
			cardDetails.put("check_Save to Payment_YES", chkSaveToPaymentMethod);

		}
		BillingPageUtils.enterBillingDetails(cardDetails, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);

		return cardDetails;
	}

	public LinkedHashMap<String, String> fillingAddressDetailsInMultipleShipping(String customerDetails)
			throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaFirst" + randomFirstName, txtMultipleShippingFirstname);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		shippingDetails.put("type_firstname_QaLast" + randomLastName, txtMultipleShippingLastname);

		String address = checkoutProperty.getProperty(customerDetails);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];
		String zipcode = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[4];

		if (customerDetails.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}

		shippingDetails.put("type_address_" + address1, txtMultipleShippingAddress);
		shippingDetails.put("type_city_" + city, txtMultipleShippingCity);
		shippingDetails.put("select_state_" + state, btnMultipleShippingState);
		shippingDetails.put("type_zipcode_" + zipcode, txtMultipleShippingZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtMultipleShippingPhoneNo);

		shippingDetails.put("click_Clicking Save button", btnMultipleShippingSave);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		Log.event("Filled Shipping Details as a Guest user", StopWatch.elapsedTime(startTime));
		/*
		 * if (Utils.waitForElement(driver, dialog)) {
		 * BrowserActions.clickOnElement(btnContinueInAddressValidationModal,
		 * driver,"click_Clicking Continuce button in Dialog");
		 * BrowserActions.clickOnElement(btnMultipleShippingSave, driver,
		 * "Continue to click billing button"); }
		 */
		return shippingDetails;
	}

	public String getFirstNameInBillingAddress() throws Exception {
		return txtFirstNameInBilling.getText();
	}

	public String getTextInNameOnCard() throws Exception {
		return txtNameInCreditCard.getAttribute("value");
	}

	public void clickOnYesInShipToMultipleAddress() throws Exception {
		BrowserActions.clickOnElement(btnYesForMultipleAddressShipping, driver,
				"Yes Button in Ship To Multiple Addresses");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnAddInShipToMulipleAddress() throws Exception {
		BrowserActions.clickOnElement(lnkAddInShipToMultipleAddress.get(0), driver,
				"Add Link in Product List in Ship to Multiple Address");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnAddInShipToMulipleAddressByRow(int row) throws Exception {
		BrowserActions.clickOnElement(lnkAddInShipToMultipleAddress.get(row - 1), driver,
				"Add Link in Product List in Ship to Multiple Address");
		Utils.waitForPageLoad(driver);
	}

	public boolean verifyAddressInShipToMultipleAddress() throws Exception {
		WebElement addressDropDown = driver.findElement(By.cssSelector(
				"table[class='item-list'] > tbody > tr:nth-child(1) .shippingaddress.required .selected-option"));
		BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown Button");
		Utils.waitForPageLoad(driver);
		List<WebElement> lstAddress = selectAddressInShipToMultipleAddress.get(0).findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul>li"));
		if (lstAddress.size() > 1)
			return true;
		return false;
	}

	/**
	 * To choose the Original Address radio button in modal
	 * 
	 * @throws Exception
	 * 
	 *             Last Modified By Dhanapal.K --- 04/01/2017
	 */
	public void chooseOriginalAddressInModal() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].style.opacity=1", rdoOriginalAddress);
			if (Utils.waitForElement(driver, rdoOriginalAddress)) {
				BrowserActions.selectRadioOrCheckbox(rdoOriginalAddress, "YES");
				Utils.waitForPageLoad(driver);
			}
		}
	}

	public String getProductNameColorInOrderSummary() throws Exception {
		return lnkProductNameInOrderSummary.get(0).getCssValue("color");
	}

	public boolean verifyPriceHasCurrencySymbol() throws Exception {
		for (int i = 0; i < lblPrice.size(); i++) {
			if (!(BrowserActions.getText(driver, lblPrice.get(i), "Price Label in Order Summary").contains("$")))
				return false;
		}
		return true;
	}

	public LinkedHashMap<String, String> getShippingAddress() throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.scrollToView(driver.findElement(By.cssSelector(lblShippingName)), driver);
		String shippingCustomerName = BrowserActions.getText(driver, lblShippingName, "Shipping name");
		String shippingCustomerAddress1 = BrowserActions.getText(driver, lblShippingAddress1, "Shipping Address1");
		String shippingCustomerCity = BrowserActions.getText(driver, lblShippingCity, "Shipping city");
		String shippingCustomerPhoneNo = BrowserActions.getText(driver, lblShippingPhone, "Shipping Phone No");
		String shippingType = BrowserActions.getText(driver, lblShippmentMethod, "Shipping Type");

		shippingDetails.put("FirstName", shippingCustomerName.split(" ")[0]);
		shippingDetails.put("LastName", shippingCustomerName.split(" ")[1]);
		shippingDetails.put("Address", shippingCustomerAddress1);
		try {
			String billingCustomerAddress2 = BrowserActions.getText(driver, lblShippingAddress2, "Billing Address2");
			shippingDetails.put("Address2", billingCustomerAddress2);
		} catch (Exception e) {
			Log.event("Address 2 is not displayed");
		}
		shippingDetails.put("City", shippingCustomerCity.split(",")[0]);
		shippingDetails.put("State", shippingCustomerCity.split(",")[1].trim().split(" ")[0]);
		shippingDetails.put("Zipcode", shippingCustomerCity.split(",")[1].trim().split(" ")[1]);
		shippingDetails.put("Phone", shippingCustomerPhoneNo.replace("-", ""));
		shippingDetails.put("ShippingMethod", shippingType);
		return shippingDetails;
	}

	@FindBy(css = ".mini-shipment")
	List<WebElement> shippingAddressSection;

	public LinkedList<LinkedHashMap<String, String>> getMultiShippingAddress() throws Exception {
		LinkedList<LinkedHashMap<String, String>> shippingAddress = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < shippingAddressSection.size(); i++) {
			LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

			BrowserActions.scrollToView(shippingAddressSection.get(i), driver);
			String shippingCustomerName = shippingAddressSection.get(i)
					.findElement(By.cssSelector(".address span[class='payment-name']")).getText();
			String shippingCustomerAddress1 = shippingAddressSection.get(i)
					.findElement(By.cssSelector(".address span[class='payment-addr']:nth-child(2)")).getText();
			String shippingCustomerCity = shippingAddressSection.get(i)
					.findElement(By.cssSelector(".address span[class='payment-city']")).getText();
			String shippingCustomerPhoneNo = shippingAddressSection.get(i)
					.findElement(By.cssSelector(".address span[class='formatPhone']")).getText();

			shippingDetails.put("FirstName", shippingCustomerName.split(" ")[0].trim());
			shippingDetails.put("LastName", shippingCustomerName.split(" ")[1].trim());
			shippingDetails.put("Address", shippingCustomerAddress1);
			try {
				String billingCustomerAddress2 = BrowserActions.getText(driver, lblShippingAddress2, "Billing Address2")
						.trim();
				shippingDetails.put("Address2", billingCustomerAddress2);
			} catch (Exception e) {
				Log.event("Address 2 is not displayed");
			}
			shippingDetails.put("City", shippingCustomerCity.split("\\,")[0].trim());
			shippingDetails.put("State", shippingCustomerCity.split("\\,")[1].trim().split(" ")[0].trim());
			shippingDetails.put("Zipcode",
					shippingCustomerCity.split("\\,")[1].trim().replace(shippingDetails.get("State"), "").trim());
			shippingDetails.put("Phone", shippingCustomerPhoneNo.replace("-", "").trim());

			shippingAddress.add(shippingDetails);
		}
		return shippingAddress;
	}

	public ShoppingBagPage clickReturnToShoppingBag() throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.clickOnElement(lnkReturnToShoppingBag, driver,
				"Clicked on 'Return to Shopping Bag' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public void clickShippingRates() throws Exception {
		BrowserActions.clickOnElement(lnkShippingRates, driver,
				"Clicked on 'Shipping Rates' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
	}

	public void clickEasyReturns() throws Exception {
		BrowserActions.clickOnElement(lnkEasyReturns, driver,
				"Clicked on 'Easy Returns' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
	}

	public FaqPage clickFAQs() throws Exception {
		BrowserActions.clickOnElement(lnkSFAQs, driver, "Clicked on 'FAQs' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
		return new FaqPage(driver).get();
	}

	public CustomerServicePage clickCustomerService() throws Exception {
		BrowserActions.clickOnElement(lnkCustomerService, driver,
				"Clicked on 'Customer Service' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
		return new CustomerServicePage(driver).get();
	}

	public PrivacyAndPolicyPage clickPrivacyandSecurityGuarantee() throws Exception {
		BrowserActions.clickOnElement(lnkPrivacyandSecurityGuarantee, driver,
				"Clicked on 'Privacy and Security Guarantee' link in Checkout-Shipping page");
		Utils.waitForPageLoad(driver);
		return new PrivacyAndPolicyPage(driver).get();
	}

	public boolean verifySippmentType() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1", radioStandard);
		executor.executeScript("arguments[0].style.opacity=1", radioExpress);
		executor.executeScript("arguments[0].style.opacity=1", radioOverNight);

		if (Utils.waitForElement(driver, radioStandard) && Utils.waitForElement(driver, radioExpress)
				&& Utils.waitForElement(driver, radioOverNight))
			return true;

		return false;
	}

	public boolean verifyRadioOptionsInAddressValidationModal() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1", rdoSuggestedAddress);
		executor.executeScript("arguments[0].style.opacity=1", rdoOriginalAddress);

		if (Utils.waitForElement(driver, rdoSuggestedAddress) && Utils.waitForElement(driver, rdoOriginalAddress))
			return true;

		return false;
	}

	public int getValueFromQuantityInOrderSummary() throws Exception {
		return Integer.parseInt(BrowserActions.getText(driver, lblQuantityValue, "TextQTY"));
	}

	public void selectFirstAddressInAddressDropDown() throws Exception {
		List<WebElement> lstProductInShipToMultipleAddress = driver
				.findElements(By.cssSelector("table[class='item-list'] > tbody > tr"));
		for (int i = 0; i < lstProductInShipToMultipleAddress.size(); i++) {
			WebElement firstValueInDrp = driver
					.findElement(By.cssSelector("table[class='item-list'] > tbody > tr:nth-child(" + (i + 1)
							+ ") .shippingaddress.required .custom-select >ul >li:not([class='selected']):nth-child(2)"));
			WebElement addressDropDown = driver
					.findElement(By.cssSelector("table[class='item-list'] > tbody > tr:nth-child(" + (i + 1)
							+ ") .shippingaddress.required .selected-option"));

			BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElement(firstValueInDrp, driver, "First Address in Dropdown");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To click on the shipping button
	 */
	public void clickOnShippingAddress() throws Exception {
		BrowserActions.clickOnElement(txtheadingShippingOrder, driver, "Shipping address");
	}

	/**
	 * To get the text from suggested address
	 */
	public String returnZipCodeInSuggestedAddress() throws Exception {
		String txtSuggestAddress = BrowserActions.getText(driver, txtSuggestedAddressCity, "Txt suggested address");
		String zipCode[] = txtSuggestAddress.split(" ");
		return zipCode[2];

	}

	/**
	 * To get the text from the zip code text field after
	 */

	public String returnShippingZipCodeInBillingAddressPanel() throws Exception {
		String txtSuggestAddress = BrowserActions.getText(driver, txtShippingCityInBillingPanel,
				"Txt suggested address");
		String zipCode[] = txtSuggestAddress.split(" ");
		return zipCode[2];
	}

	public void clickOnSaveInAddEditAddressModal() throws Exception {
		BrowserActions.clickOnElement(btnMultipleShippingSave, driver, "Continue button in shipping");
		Utils.waitForPageLoad(driver);
		Log.event("Save Button in Add or Edit Address modal in Ship to Multiple Address");
	}

	public boolean ContinueAddressValidationModalInMultipleShipping() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.clickOnElement(btnContinueInAddressValidationModal, driver,
					"Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);
			clickOnSaveInAddEditAddressModal();
			return true;
		}
		return false;
	}

	public void enterEmailId(String strEmailID) throws Exception {
		BrowserActions.typeOnTextField(txtEmailID, strEmailID, driver, "Enter email Id in the Email Id Field ");
	}

	public void clickEditPayment() throws Exception {
		BrowserActions.clickOnElement(btnPaymentEdit, driver, "Continue Button");
	}

	public String selectSavedAddressByIndex(int index) throws Exception {
		String savedAddress = null;
		BrowserActions.clickOnElement(drpSavedBillingAddress, driver, "SortBy dropdown");
		BrowserActions.clickOnElement(lstSavedAddreses.get(index), driver, "Saved Address dropdown list");

		savedAddress = driver
				.findElement(
						By.cssSelector("#dwfrm_billing > fieldset > div.form-row.select-address > div > div > div"))
				.getText();
		savedAddress = savedAddress.split(" ")[2];
		return savedAddress;
	}

	public String getCreditCardType() throws Exception {
		String creditCardType = null;
		creditCardType = BrowserActions.getText(driver, txtCreditCardType, "Credit Card type in Payment Section");
		return creditCardType;
	}

	public String getCreditCardTypeDropDown() throws Exception {
		String creditCardType = null;
		creditCardType = driver
				.findElement(By.cssSelector(
						"#dwfrm_billing > fieldset > div > div.payment-method.payment-method-expanded > div.form-row.required > div > div.custom-select > div"))
				.getText();
		return creditCardType;
	}

	public String getAddress1Value() throws Exception {
		String address1 = BrowserActions.getText(driver, txtBillingAddOne, "Billing Address1 value");
		return address1;
	}

	/**
	 * To click the continue button
	 * 
	 * @throws Exception
	 */
	public void clickOnContinueInMultiAddress() throws Exception {
		BrowserActions.clickOnElement(btnContinueInMultiShipping, driver,
				"Continue Button in Ship To Multiple Addresses");
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To click on the edit by section name
	 * 
	 * @param sectionName
	 *            - Ex. ordersummary, shippingaddress, billingaddress,
	 *            paymentmethod
	 * @throws Exception
	 */
	public void clickOnEditBySectionName(String sectionName) throws Exception {
		if (sectionName == "ordersummary") {
			BrowserActions.scrollToView(editOrderSummary.get(0), driver);
			BrowserActions.clickOnElement(editOrderSummary.get(0), driver, "Edit in Order summary");
		} else if (sectionName == "shippingaddress") {
			BrowserActions.scrollToView(editOrderSummary.get(1), driver);
			BrowserActions.clickOnElement(editOrderSummary.get(1), driver, "Edit in Shipping address");
		} else if (sectionName == "billingaddress") {
			BrowserActions.scrollToView(editOrderSummary.get(2), driver);
			BrowserActions.clickOnElement(editOrderSummary.get(2), driver, "Edit in Shipping address");
		} else if (sectionName == "paymentmethod") {
			BrowserActions.scrollToView(editOrderSummary.get(3), driver);
			BrowserActions.clickOnElement(editOrderSummary.get(3), driver, "Edit in Payment method");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To edit the first name in the address pane by selection
	 * 
	 * @param section
	 *            - Ex. shippingaddress, billingaddress
	 * @param text
	 * @throws Exception
	 */
	public void editFirstNameInAddressBySection(String section, String[] text) throws Exception {
		if (section == "shippingaddress") {
			BrowserActions.typeOnTextField(txtFirstNameInShipping, text[0], driver, "Edit First name");
			BrowserActions.nap(5);
			clickOnContinueInShipping();
		} else if (section == "billingaddress") {
			BrowserActions.typeOnTextField(txtFirstNameInBilling, text[0], driver, "Edit First name");
			fillingCardDetails("NO", "card_Visa");
			BrowserActions.nap(5);
			clickOnContinueInBilling();
		}
	}

	/**
	 * To edit the name and cvv number in the payment method
	 * 
	 * @param text
	 * @throws Exception
	 */
	public void editNameOnCardInPaymentMethod(String text[]) throws Exception {
		BrowserActions.typeOnTextField(txtNameInCreditCard, text[0], driver, "Edit name on card");
		BrowserActions.typeOnTextField(txtCvnNo, text[1], driver, "CVV Number");
		BrowserActions.nap(5);
		clickOnContinueInBilling();
	}

	/**
	 * To get the first name in the address pane by selection
	 * 
	 * @param section
	 *            - Ex. shippingaddress, billingaddress, paymentmethod
	 * @return firstname
	 * @throws Exception
	 */
	public String getFirstNameInAddressBySection(String section) throws Exception {
		String firstName = null;
		if (section == "shippingaddress") {
			firstName = BrowserActions.getText(driver, lstFirstNameInAddressPane.get(0),
					"FirstName in Shipping address");
		} else if (section == "billingaddress") {
			firstName = BrowserActions.getText(driver, lstFirstNameInAddressPane.get(1),
					"FirstName in Billing address");
		} else if (section == "paymentmethod") {
			firstName = BrowserActions.getText(driver, lblNameOnCard, "Name on card in Payment method");
		}
		return firstName;
	}

	/**
	 * To get the class name of Billing/Shipping/Placeholder
	 * 
	 * @param step
	 *            - Ex. shipping , billing , placeholder
	 * @return classname
	 * @throws Exception
	 */
	public String getClassNameOfStepsInCheckoutPage(String step) throws Exception {
		if (step == "shipping") {
			return BrowserActions.getTextFromAttribute(driver, tabShipping, "class", "class name of shipping tab");
		} else if (step == "billing") {
			return BrowserActions.getTextFromAttribute(driver, tabBilling, "class", "class name of billing tab");
		} else if (step == "placeorder") {
			return BrowserActions.getTextFromAttribute(driver, tabPlaceOrder, "class", "class name of place order");
		} else {
			return null;
		}

	}

	/**
	 * To click the cancel button in the place order page
	 * 
	 * @throws Exception
	 */
	public void clickOnCancelButtonInPlaceOrder() throws Exception {

		if (Utils.getRunPlatForm() == "desktop") {
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElement(btnCancelInPlaceOrderDesktop, driver, "Cancel button in Place order page");
		} else if (Utils.getRunPlatForm() == "mobile") {
			Utils.waitForPageLoad(driver);
			BrowserActions.scrollToView(btnCancelInPlaceOrderMobile, driver);
			BrowserActions.clickOnElement(btnCancelInPlaceOrderMobile, driver, "Cancel button in Place order page");
		}

	}

	public boolean getTestShippingMethod() throws Exception {

		boolean status = false;

		String shippingMethod = BrowserActions.getText(driver, lblShippingMethod.get(0), "Shipping Method1");
		if (shippingMethod.contains("Test Shipment")) {
			status = true;
		}
		return status;

	}

	public boolean getStandardShippingMethod() throws Exception {

		boolean status = false;
		String shippingMethod = BrowserActions.getText(driver, lblShippingMethod.get(1), "Shipping Method2");
		if (shippingMethod.contains("Standard")) {
			status = true;
		}
		return status;

	}

	public boolean getExpressShippingMethod() throws Exception {
		boolean status = false;
		String shippingMethod = BrowserActions.getText(driver, lblShippingMethod.get(2), "Shipping Method3");
		if (shippingMethod.contains("Express")) {
			status = true;
		}
		return status;

	}

	public boolean getOvernightShippingMethod() throws Exception {
		boolean status = false;
		String shippingMethod = BrowserActions.getText(driver, lblShippingMethod.get(3), "Shipping Method4");
		if (shippingMethod.contains("Overnight")) {
			status = true;
		}
		return status;
	}

	public String getDefaultShippingAddress() throws Exception {
		String savedAddress = null;
		savedAddress = driver.findElement(By.cssSelector(".selected-option.selected")).getText();
		return savedAddress;
	}

	public String getShippingAddressOne() throws Exception {
		String shippingAddressOne = null;
		shippingAddressOne = BrowserActions.getText(driver, txtAddress_1InShipping, "Shipping Address1");
		return shippingAddressOne;
	}

	/*
	 * public String getShippingAddressCity() throws Exception{ String
	 * shippingAddressCity = null; shippingAddressCity =
	 * BrowserActions.getText(driver, txtCityInShipping, "City"); return
	 * shippingAddressCity; }
	 */

	public String getShippingAddressZipCode() throws Exception {

		return BrowserActions.getTextFromAttribute(driver, txtZipcodeInShipping, "value", "Zip code");
	}

	/*
	 * To get Billing Address Details throws Exception
	 */

	public String selectSavedBillingAddressByIndex(int index) throws Exception {
		String savedAddress = null;
		if (drpSavedBillingAddress.getText().contains("Saved")) {
			BrowserActions.clickOnElement(drpSavedBillingAddress, driver, "SortBy dropdown");
			BrowserActions.clickOnElement(lstSavedAddreses.get(index), driver, "Saved Address dropdown list");
		}
		savedAddress = drpSavedBillingAddress.getText();
		// savedAddress = savedAddress.split(" ")[2];
		return savedAddress;
	}

	public String getBillingAddressOne() throws Exception {
		String billingAddressOne = null;
		billingAddressOne = BrowserActions.getText(driver, txtAddress_1InBilling, "Billing Address1");
		return billingAddressOne;
	}

	public String getBillingAddressCity() throws Exception {
		String billingAddressCity = null;
		billingAddressCity = BrowserActions.getText(driver, txtCityInBilling, "City");
		return billingAddressCity;
	}

	public String getBillingAddressZipCode() throws Exception {
		String billingAddressZipCode = null;
		billingAddressZipCode = BrowserActions.getText(driver, txtZipcodeInBilling, "Zipcode");
		return billingAddressZipCode;
	}

	/**
	 * To Verify page navigated to place order page
	 * 
	 * @Return Place order Page
	 * @throws Exception
	 */

	public boolean VerifyPageNavigatedToPlaceOrder() throws Exception {
		if (btnPlaceOrderInPlaceOrderPage.isEnabled()) {
			return true;
		}
		return false;
	}

	/**
	 * To get Product Name
	 * 
	 * @Return Name
	 * @throws Exception
	 */

	public String PrdName() throws Exception {

		String Productname = BrowserActions.getText(driver, txtProductName, "Product name in shopping bag");
		return Productname;
	}

	/**
	 * To get Product Size
	 * 
	 * @Return Size
	 * @throws Exception
	 */
	public String PrdSize() throws Exception {

		String ProductSize = BrowserActions.getText(driver, txtSize, "Size in shopping bag");
		return ProductSize;
	}

	/**
	 * To get Product Color
	 * 
	 * @Return Color
	 * @throws Exception
	 */
	public String PrdColor() throws Exception {

		String ProductColor = BrowserActions.getText(driver, txtColor, "Product name in shopping bag");
		return ProductColor;
	}

	/**
	 * To get Product Subtotal
	 * 
	 * @Return Subtotal
	 * @throws Exception
	 */

	public String PrdSubtotal() throws Exception {

		String ProductSubtotal = BrowserActions.getText(driver, txtSubtotal, "Product name in shopping bag");
		return ProductSubtotal;
	}

	/**
	 * To Click on Edit billing address link
	 * 
	 * @Return Billing page
	 * @throws Exception
	 */

	public void EditBillingAddress() throws Exception {
		BrowserActions.clickOnElement(lnkEditBillAddress, driver, "Billing address edit link is clicked");
	}

	/**
	 * To Verify Page navigated to billing page
	 * 
	 * @Return Billing page
	 * @throws Exception
	 */

	public void VerifyPageNavigatedToBillingaddress() {

		if (ReturnToshipPage.isDisplayed()) {
			System.out.println("page is redirected to Billing Page");
			Log.event("page is redirected to Billing Page screen");
		}
	}

	/**
	 * To enter CVN number
	 * 
	 * @Return Card Details
	 * @throws Exception
	 */

	public LinkedHashMap<String, String> fillingCvn(String customerDetails) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardCVN = cutomercardDetails.split("\\|")[5];
		cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);
		BillingPageUtils.enterBillingDetails(cardDetails, driver);
		BrowserActions.nap(3);
		Log.event("Filled Card CVN Details", StopWatch.elapsedTime(startTime));
		return cardDetails;
	}

	/**
	 * To get the Billing phone number
	 * 
	 * @return Billphone
	 * 
	 */

	public String BillPhone() throws Exception {
		BrowserActions.nap(3);
		String BillPhone = BrowserActions.getText(driver, BillingPhone, "Billingname");
		return BillPhone;

	}

	/**
	 * To Verify page navigated to Billing Page
	 * 
	 * @Return Bill page
	 * @throws Exception
	 */

	public boolean VerifyBillPage() throws Exception {

		if (!BillingMethod.isEmpty()) {
			return true;
		}
		return false;
	}

	public boolean clickContinueInAddressValidationModal() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.clickOnElement(btnContinueInAddressValidationModal, driver,
					"Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);
			return true;
		}
		return false;
	}

	public void clickOnBillingHeader() throws Exception {
		BrowserActions.clickOnElement(tabBilling, driver, "Continue button in billingPage");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnAddressFieldOnBillingBag() throws Exception {
		BrowserActions.typeOnTextField(txtBillingAddressOne, "18305 sanjose ave", driver,
				"Continue button in billingPage");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnAddressFieldOnShippingBag() throws Exception {
		BrowserActions.typeOnTextField(txtAddressName, "XYZ", driver, "Continue button in billingPage");
		Utils.waitForPageLoad(driver);
	}

	public boolean VerifySaveToPaymentIsNotCheckedByDefault() throws Exception {
		boolean status = false;
		status = chkSaveCard.findElement(By.tagName("input")).isSelected();
		if (!status) {
			status = true;
		}
		return status;
	}

	public String getTextFromSelectedCard() throws Exception {
		return btnPaymentCardType.getText();
	}

	public void FillAddress2InShipping(String address2) throws Exception {
		txtAddress_2InShipping.clear();
		txtAddress_2InShipping.sendKeys(address2);
	}

	public String GetTextFromAddress2TextFiled() throws Exception {
		String addressTwo = txtAddress_2InShipping.getAttribute("value");
		return addressTwo;
	}

	public void mouseOverOnWhatIsThis() throws Exception {

		BrowserActions.mouseHover(driver, hoverOnwhatIsThis);
	}

	public void clickReturnToBilling() throws Exception {
		BrowserActions.clickOnElement(lnkReturnToShoppingBag, driver, "Return in BillingPage");
		Utils.waitForPageLoad(driver);
	}

	public void clickReturnToShipping() throws Exception {
		BrowserActions.clickOnElement(lnkReturnToShoppingBag, driver, "Return in BillingPage");
		Utils.waitForPageLoad(driver);
	}

	public ShoppingBagPage clickOnEditInOrderSummary() throws Exception {
		Utils.waitForElement(driver, lnkEditOrderSummary);
		BrowserActions.clickOnElement(lnkEditOrderSummary, driver, "Edit Button in Order Summary");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	public String getTextFromSectionHeaderBySectionName(String section) throws Exception {
		String sectionText = null;
		if (section == "ordersummary")
			sectionText = BrowserActions.getText(driver, lstSectionHeader.get(0), "Order summary").split(" Edit")[0];
		else if (section == "shippingaddress")
			sectionText = BrowserActions.getText(driver, lstSectionHeader.get(1), "Shipping address").split(" Edit")[0];
		else if (section == "billingaddress")
			sectionText = BrowserActions.getText(driver, lstSectionHeader.get(2), "Billing address").split(" Edit")[0];
		else if (section == "paymentmethod")
			sectionText = BrowserActions.getText(driver, lstSectionHeader.get(3), "Payment method").split(" Edit")[0];
		return sectionText;
	}

	/**
	 * To click on Edit shipping address
	 * 
	 * @Clicked on Edit SHipping address
	 * @throws Exception
	 */

	public void editShippingAddress() throws Exception {
		BrowserActions.clickOnElement(LnkEditShipAddress, driver, "Shipping address edit link is clicked");
	}

	/**
	 * To get the shipping phone number
	 * 
	 * @return Billphone
	 * 
	 */

	public String shipPhone() throws Exception {
		BrowserActions.nap(3);
		String BillPhone = BrowserActions.getText(driver, ShippiningPhone, "Shipping Phone number");
		return BillPhone;

	}

	public String getPaymentDetails() throws Exception {
		String paymentDetails = BrowserActions.getText(driver, txtPaymentDetails, "Payment Details");
		return paymentDetails;
	}

	/**
	 * 
	 * @return String - shippingDetails details
	 * @throws Exception
	 */
	public String getShippingDetailsInPlaceOrder() throws Exception {
		String shippingDetails = BrowserActions.getText(driver, shippingModule, "Payment Details");
		return shippingDetails;

	}

	/**
	 * To get the details of Billing in PlaceOrderPage .
	 * 
	 * @return List-Billing Details
	 * @throws Exception
	 */
	public ArrayList<String> gettingBillingDetailsInPlaceOrder() throws Exception {
		ArrayList<String> lstValues = new ArrayList<>();
		int i;

		for (i = 0; i < BillingDetailsInPlaceOrder.size(); i++) {
			lstValues.add(BrowserActions.getText(driver, BillingDetailsInPlaceOrder.get(i), "Billing details"));
		}
		return lstValues;

	}

	public String getshippingDetailsInPlaceOrder() throws Exception {
		String paymentDetails = BrowserActions.getText(driver, shippingModule, "Payment Details");
		return paymentDetails;

	}

	public String getTextFromBillingHeader() throws Exception {
		return BrowserActions.getText(driver, lnkReturnToShoppingBag, "Text From Billing Header");
	}

	/**
	 * To get the Firstname
	 * 
	 * @return Firstname in Billing address page from shipping address in Order
	 *         summary panel
	 * 
	 *         *
	 */

	public String getFirstnameOfShippingAddressInBillingAddressPage() throws Exception {
		BrowserActions.nap(3);
		String shippingFirstname = BrowserActions.getText(driver, billingPageShippingName, "shippingFirstname");
		shippingFirstname = shippingFirstname.split(" ")[0];
		return shippingFirstname;

	}

	/**
	 * To get the Lastname
	 * 
	 * @return Laststname in Billing address page from shipping address in Order
	 *         summary panel
	 * 
	 *         *
	 */

	public String getLastnameOfShippingAddressInBillingAddressPage() throws Exception {
		BrowserActions.nap(3);
		String shippingLasttname = BrowserActions.getText(driver, billingPageShippingName, "shippingLastname");
		shippingLasttname = shippingLasttname.split(" ")[1];
		return shippingLasttname;

	}

	/**
	 * To get the Shipping phone number In Billing Address Page
	 * 
	 * @return shippingphone
	 * 
	 */

	public String getPhoneNoInShippingAddressInBillingAddress() throws Exception {
		BrowserActions.nap(3);
		String shippingPhone = BrowserActions.getText(driver, billingPageShippingPhone, "shippingPhone");
		String ShippignPhoneupdate = shippingPhone.replaceAll("[-]", "");
		return ShippignPhoneupdate;

	}

	/**
	 * To get the Shipping Address1 In Billing Address
	 * 
	 * @return Address
	 * 
	 */

	public String getAddressInShippingAddressInBillingAddress() throws Exception {
		BrowserActions.nap(3);
		String shippingsAddress = BrowserActions.getText(driver, billingPageShippingAddress1, "shippingsAddress");
		return shippingsAddress;

	}

	/**
	 * To get the shipping City In Billing address page
	 * 
	 * @return City
	 * 
	 */

	public String getCityInShippingInBillingAddress() throws Exception {
		BrowserActions.nap(3);
		String shippingCity = BrowserActions.getText(driver, billingPageShippingCity, "billingPageShippingCity");
		shippingCity = shippingCity.split(",")[0];
		return shippingCity;

	}

	/**
	 * To get the shipping Zipcode In Billing address page
	 * 
	 * @return Zipcode
	 * 
	 */

	public String getZipcodeInShippingInBillingAddress() throws Exception {
		BrowserActions.nap(3);
		String shippingCity = BrowserActions.getText(driver, billingPageShippingCity, "billingPageShippingCity");
		shippingCity = shippingCity.split(",")[2];
		return shippingCity;

	}

	/***************************************************************************************************************
	 * Methods for getting shipping address details in Shipping Page
	 ***************************************************************************************************************/

	/**
	 * To get the Firstname
	 * 
	 * @return Firstname in Shipping address page
	 * 
	 */

	public String getFirstnameInShippingAddressPage() throws Exception {

		String shippingFirstname = BrowserActions.getTextFromAttribute(driver, txtFirstNameInShipping, "value",
				"shippingFirstname");
		return shippingFirstname;

	}

	/**
	 * To get the Lastname
	 * 
	 * @return Lastname in shipping address page
	 * 
	 */

	public String getLastnameInshippingAddress() throws Exception {

		String shippingLastname = BrowserActions.getTextFromAttribute(driver, txtLastNameInShipping, "value",
				"shippingLastname");
		return shippingLastname;

	}

	/**
	 * To get the City
	 * 
	 * @return City in shipping address page
	 * 
	 */

	public String getCityInshippingAddress() throws Exception {
		String shippingCity = BrowserActions.getTextFromAttribute(driver, txtCityInShipping, "value", "shippingCity");
		return shippingCity;

	}

	/**
	 * To get the Address *
	 * 
	 * @return Address in shipping address page
	 */

	public String getAddressInnshippingAddress() throws Exception {
		String shippingAddress = BrowserActions.getTextFromAttribute(driver, txtAddress_1InShipping, "value",
				"shippingAddress");
		return shippingAddress;

	}

	/**
	 * To get the shipping phone number In Shipping address page
	 * 
	 * @return shippingphone
	 * 
	 */

	public String getPhoneNoInshippingAddressPage() throws Exception {
		BrowserActions.nap(3);

		String shippingPhone = BrowserActions.getTextFromAttribute(driver, txtPhoneInShipping, "value",
				"Shipping Phonenumber");
		return shippingPhone;

	}

	/**
	 * To get the Shipping Zip code
	 * 
	 * @return Shipping phone in Shipping address page
	 * 
	 */

	public String getZipcodeInShippingAddress() throws Exception {
		String shippingZipcode = BrowserActions.getTextFromAttribute(driver, txtZipcodeInShipping, "value",
				"Shipping Zipcode");
		return shippingZipcode;

	}

	/**
	 * To Click on Edit Shipping address link In Billing Page
	 * 
	 * @Return Billing page
	 * @throws Exception
	 */

	public void clickEditShippingAddress() throws Exception {
		BrowserActions.clickOnElement(LnkEditShipAddress, driver, "Shipping address edit link is clicked");
	}

	public void ClickOnEmptyPlaceInBilling() throws Exception {
		BrowserActions.clickOnElement(btnEmptyspace, driver, "Clicked on Random element");

	}

	/**
	 * To get the Billing phone number
	 * 
	 * @return Billphone in billing address page
	 * 
	 */

	public String getPhoneNoInBillingAddress() throws Exception {
		String billingAddress = BrowserActions.getTextFromAttribute(driver, billingPhoneInBillingPage, "value", "");
		return billingAddress;

	}

	/**
	 * To get the Firstname
	 * 
	 * @return Firstname in billing address page
	 * 
	 */

	public String getFirstnameInBillingAddressPage() throws Exception {

		String billingFirstname = BrowserActions.getTextFromAttribute(driver, txtFirstNameInBilling, "value", "");
		return billingFirstname;

	}

	/**
	 * To get the Lastname
	 * 
	 * @return Lastname in billing address page
	 * 
	 */

	public String getLastnameInBillingAddress() throws Exception {

		String billingLastname = BrowserActions.getTextFromAttribute(driver, txtBillingLastname, "value", "");
		return billingLastname;

	}

	/**
	 * To get the City
	 * 
	 * @return City in billing address page
	 * 
	 */

	public String getCityInBillingAddress() throws Exception {
		String billingCity = BrowserActions.getTextFromAttribute(driver, txtBillingCity, "value", "");
		return billingCity;

	}

	/**
	 * To get the Address
	 * 
	 * @return Address in billing address page
	 * 
	 */

	public String getAddressInnBillingAddress() throws Exception {
		String billingAddress = BrowserActions.getTextFromAttribute(driver, txtBillingAddressOne, "value", "");
		return billingAddress;

	}

	/**
	 * To get the Billing phone number In Place order screen
	 * 
	 * @return Billphone
	 * 
	 */

	public String getPhoneNoInBillingAddressInPlaceOrderScrn() throws Exception {
		BrowserActions.nap(3);
		String BillPhone = BrowserActions.getText(driver, BillingPhone, "Billingname");
		String billPhoneupdate = BillPhone.replaceAll("[-]", "");
		return billPhoneupdate;

	}

	/**
	 * To get the Billing Firstname In Place orderscreen
	 * 
	 * @return Firstname
	 * 
	 */

	public String getFirstnameInBillingAddressInPlaceOrderScrn() throws Exception {
		BrowserActions.nap(3);
		String billingname = BrowserActions.getText(driver, txtBillingAddName, "Billingname");
		billingname = billingname.split(" ")[0];
		return billingname;

	}

	/**
	 * To get the Lastname In Place order screen
	 * 
	 * @return Lastname
	 * 
	 */

	public String getLastnameInBillingAddressInPlaceOrderScrn() throws Exception {
		BrowserActions.nap(3);
		String billingname = BrowserActions.getText(driver, txtBillingAddName, "Billingname");
		billingname = billingname.split(" ")[1];
		return billingname;
	}

	/**
	 * To get the Address In Place orderscreen
	 * 
	 * @return Address
	 * 
	 */

	public String getAddressInBillingAddressInPlaceOrderScrn() throws Exception {
		BrowserActions.nap(3);
		String billingAddress = BrowserActions.getText(driver, txtBillingAddOne, "Billing Address In Place Order");
		return billingAddress;

	}

	/**
	 * To get the City In Place order screen
	 * 
	 * @return City
	 * 
	 */

	public String getCityInBillingAddressInPlaceOrderScrn() throws Exception {
		BrowserActions.nap(3);
		String billingingCity = BrowserActions.getText(driver, txtBillingAddCity, "Billing Address In Place Order");
		billingingCity = billingingCity.split(",")[0];
		return billingingCity;

	}

	/**
	 * To Click on Edit billing address link
	 * 
	 * @Return Billing page
	 * @throws Exception
	 */

	public void clickEditBillingAddress() throws Exception {
		BrowserActions.clickOnElement(lnkEditBillAddress, driver, "Billing address edit link is clicked");
	}

	/**
	 * Filling the belk gift card details
	 * 
	 * @param giftCardDetail
	 * @return giftCardDetails
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingBelkGiftCardDetails(String giftCardDetail) throws Exception {
		LinkedHashMap<String, String> giftCardDetails = new LinkedHashMap<String, String>();
		String cutomercardDetails = checkoutProperty.getProperty(giftCardDetail);
		String giftCardNumber = cutomercardDetails.split("\\|")[0];
		String pinNumber = cutomercardDetails.split("\\|")[1];

		giftCardDetails.put("type_giftCardNumber_" + giftCardNumber, txtGiftCardNumber);
		giftCardDetails.put("type_pinNumber_" + pinNumber, txtGiftPinNumber);
		BillingPageUtils.enterBillingDetails(giftCardDetails, driver);
		// Log.event("Filled Belk Gift Card Details",
		// StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return giftCardDetails;
	}

	/**
	 * To fill the belk reward dollars
	 * 
	 * @param rewardDollarsDetails
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> fillingBelkRewardDollars(String rewardDollarsDetails) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> rewardDollarsDetail = new LinkedHashMap<String, String>();
		String cutomercardDetails = checkoutProperty.getProperty(rewardDollarsDetails);
		String certificateNumber = cutomercardDetails.split("\\|")[0];

		rewardDollarsDetail.put("type_certificateNumber_" + certificateNumber, txtCertificateNumber);
		BillingPageUtils.enterBillingDetails(rewardDollarsDetail, driver);
		Log.event("Filled Belk Reward Dollars Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return rewardDollarsDetail;
	}

	public LinkedHashMap<String, String> fillingBelkRewardDollars_e2e(String rewardDollarsDetails) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> rewardDollarsDetail = new LinkedHashMap<String, String>();
		String certificateNumber = rewardDollarsDetails;

		rewardDollarsDetail.put("type_certificateNumber_" + certificateNumber, txtCertificateNumber);
		BillingPageUtils.enterBillingDetails(rewardDollarsDetail, driver);
		Log.event("Filled Belk Reward Dollars Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return rewardDollarsDetail;
	}

	/**
	 * To click on the 'ApplyBelkRewardDollars' button
	 * 
	 * @throws Exception
	 */
	public void clickOnApplyBelkRewardDollars() throws Exception {
		BrowserActions.clickOnElement(btnApplyBelkRewardDollars, driver, "ApplyBelkRewardsDollars button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on the 'ApplyGiftCard' button
	 * 
	 * @throws Exception
	 */
	public void clickOnApplyGiftCard() throws Exception {
		BrowserActions.clickOnElement(btnAddGiftCard, driver, "Add Gift Card button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on the 'Check Gift card balance' button
	 * 
	 * @throws Exception
	 */
	public void clickOnCheckGiftCardBalance() throws Exception {
		BrowserActions.clickOnElement(btnCheckGiftCardBalance, driver, "Check gift card balance button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the billing address
	 * 
	 * @return billingDetails
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBillingAddress() throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		String billingCustomerName = BrowserActions.getText(driver, lblBillingName, "Billing name");
		String billingCustomerAddress1 = BrowserActions.getText(driver, lblBillingAddress1, "Billing Address1");
		String billingCustomerCity = BrowserActions.getText(driver, lblBillingCity, "Billing city");
		String billingCustomerPhoneNo = BrowserActions.getText(driver, lblBillingPhone, "Billing Phone No");

		billingDetails.put("FirstName", billingCustomerName.split(" ")[0]);
		billingDetails.put("LastName", billingCustomerName.split(" ")[1]);
		billingDetails.put("Address", billingCustomerAddress1);
		try {
			String billingCustomerAddress2 = BrowserActions.getText(driver, lblBillingAddress2, "Billing Address2");
			billingDetails.put("Address2", billingCustomerAddress2);
		} catch (Exception e) {
			Log.event("Address 2 is not displayed");
		}
		billingDetails.put("City", billingCustomerCity.split(",")[0]);
		billingDetails.put("State", billingCustomerCity.split(",")[1].trim().split(" ")[0]);
		billingDetails.put("Zipcode", billingCustomerCity.split(",")[1].trim().split(" ")[1]);
		billingDetails.put("Phone", billingCustomerPhoneNo.trim().replace("-", ""));
		return billingDetails;
	}

	/**
	 * To get the payment method pane text
	 * 
	 * @return paymentDetails
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getPaymentMethodText() throws Exception {
		LinkedHashMap<String, String> paymentDetails = new LinkedHashMap<String, String>();
		// try {
		String cardTypeInPaymentMethod = BrowserActions.getText(driver, txtCreditCardType, "Card Type");
		paymentDetails.put("cardType", cardTypeInPaymentMethod);
		String nameInPaymentMethod = BrowserActions.getText(driver, txtNameInCreditCard, "Name on Card");
		paymentDetails.put("userName", nameInPaymentMethod);
		String cardNumberInPaymentMethod = BrowserActions.getText(driver, txtCardNumberInPaymentMethod, "Card number");
		paymentDetails.put("cardNumber", cardNumberInPaymentMethod);
		// } /*catch (Exception e) {
		// String cardNumberInPaymentMethod = BrowserActions.getText(driver,
		// txtGiftCardNumberInPaymentMethod, "Gift Card number");
		// paymentDetails.put("cardNumber", cardNumberInPaymentMethod);
		// }*/

		String amountInPaymentMethod = BrowserActions.getText(driver, txtAmountInPaymentMethod, "Amount in payment");
		paymentDetails.put("amount", amountInPaymentMethod.replace("$", ""));
		return paymentDetails;
	}

	public LinkedHashMap<String, String> fillingBelkRewardCardDetails(String SaveToPaymentMethod,
			String customerDetails, String... iteration) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];
		String cardCVN = cutomercardDetails.split("\\|")[3];

		cardDetails.put("select_cardtype_" + cardType, btnCardType);
		cardDetails.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetails.put("type_cardno_" + cardNo, txtCardNo);
		cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);

		if (SaveToPaymentMethod.equals("YES")) {
			cardDetails.put("check_Save to Payment_YES", chkSaveToPaymentMethod);

		}
		BillingPageUtils.enterBillingDetails(cardDetails, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return cardDetails;
	}

	public void clearCVN() throws Exception {
		cvvOfCard.clear();
	}

	public LinkedHashMap<String, String> getShippingAddressDetails() throws Exception {
		BrowserActions.nap(5);
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

		// String firstName = BrowserActions.getText(driver, txtShippingName,
		// "First Name in Shipping Address");
		String addressOne = BrowserActions.getText(driver, txtAddressOne, "Address 1 in Shipping Address");

		String city = BrowserActions.getText(driver, txtShippingCity, "City in Shipping Address");
		String phoneNo = BrowserActions.getText(driver, txtShippingPhoneNo, "Phone No in Shipping Address");
		phoneNo = phoneNo.replaceAll("-", "");

		// shippingDetails.put("FirstName", firstName);
		shippingDetails.put("AddressOne", addressOne);
		shippingDetails.put("City", city);
		shippingDetails.put("PhoneNo", phoneNo);

		return shippingDetails;

	}

	/**
	 * getBillingAddressDetails
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBillingAddressDetails() throws Exception {
		BrowserActions.nap(5);
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();

		String firstName = BrowserActions.getText(driver, txtBillingFirstName, "First Name in Shipping Address");
		String addressOne = BrowserActions.getText(driver, txtBillingAddress, "Address 1 in Shipping Address");

		String city = BrowserActions.getText(driver, txtBillingAddCityInOS, "City in Shipping Address");
		String phoneNo = BrowserActions.getText(driver, txtBillinghoneNo, "Phone No in Shipping Address");
		phoneNo = phoneNo.replaceAll("-", "");

		billingDetails.put("FirstName", firstName);
		billingDetails.put("AddressOne", addressOne);
		billingDetails.put("City", city);
		billingDetails.put("PhoneNo", phoneNo);

		return billingDetails;

	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		BrowserActions.nap(2);
		Utils.waitUntilElementDisappear(driver, PDPspinner);
		BrowserActions.nap(2);
	}

	public String getSelectedRadioTextShippingMethod() throws Exception {
		String RadioValueOfShipmentMethod = "";

		if (BrowserActions.isRadioOrCheckBoxSelected(radioStandard)) {
			RadioValueOfShipmentMethod = radioStandard.getAttribute("value");
			System.out.println(RadioValueOfShipmentMethod);
		} else if (BrowserActions.isRadioOrCheckBoxSelected(radioExpress)) {
			RadioValueOfShipmentMethod = radioExpress.getAttribute("value");
			System.out.println(RadioValueOfShipmentMethod);
		} else if (BrowserActions.isRadioOrCheckBoxSelected(radioOverNight)) {
			RadioValueOfShipmentMethod = radioOverNight.getAttribute("value");
			System.out.println(RadioValueOfShipmentMethod);
		} else {
			Log.event("Radio button is not selected");
		}

		return RadioValueOfShipmentMethod;
	}

	public String shippingMethodInPlaceOrder() throws Exception {
		String ShipMethodInPlaceOrder = BrowserActions.getText(driver, Shipmethod,
				"Type of shipping method in place order screen");
		return ShipMethodInPlaceOrder;
	}

	public String shippingMethodInOrderRecipt() throws Exception {
		String ShipMethodInOrderReceipt = BrowserActions.getText(driver, ShipMethodInOrderRecipt,
				"Type of shipping method in Order Receipt screen");
		return ShipMethodInOrderReceipt;
	}

	public String shippingCostInPlaceOrder() throws Exception {
		String ShippingCostInPlaceOrder = BrowserActions.getText(driver, ShipCost,
				"Shipping Cost in place order screen");
		return ShippingCostInPlaceOrder;
	}

	public String shippingCostInOrderReceipt() throws Exception {
		String ShippingCost = BrowserActions.getText(driver, ShipCostInorderReceipt,
				"Shipping Cost As per Order summary is Extracted");
		return ShippingCost;
	}

	public String shippingCostInOrderSummary() throws Exception {
		String ShippingCost = BrowserActions.getText(driver, ShipCost,
				"Shipping Cost As per Order summary is Extracted");
		return ShippingCost;
	}

	/**
	 * To get product details (Product name, qty, color, size, price) in Order
	 * Summary section
	 * 
	 * @return LinkedHashMap<String, String>
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInOrderSummary() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		for (int i = 0; i < lstProductNamesInOrderSummary.size(); i++) {
			LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();
			// Writing product name in hashmap
			String pdtName = BrowserActions.getText(driver, lstProductNamesInOrderSummary.get(i), "Product Name");
			dataToBeReturned.put("ProductName", pdtName.trim());
			// Writing color in hashmap
			WebElement elementColor = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div[data-attribute='color']"));
			String colorName = BrowserActions.getText(driver, elementColor, "Color Name").split("\\:")[1];
			dataToBeReturned.put("Color", colorName.trim());
			// Writing size in hashmap
			WebElement elementSize = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div[data-attribute='size']"));
			String sizeValue = BrowserActions.getText(driver, elementSize, "Size Value").split("\\:")[1];
			dataToBeReturned.put("Size", sizeValue.trim());

			// Log.message(""+quantityValue.toString());

			try {
				// Writing Standard price value in hashmap
				WebElement stdPrice = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector(".line-item-price .standard-price"));
				String stdPriceValue = BrowserActions.getText(driver, stdPrice, "Standard Price Value");
				dataToBeReturned.put("Price", stdPriceValue.replace("$", ""));
			} catch (Exception e) {
				Log.event("Standard price value is not displayed");
			}
			try {
				// Writing Now price value in hashmap
				WebElement nowPrice = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector(".line-item-price .now-price"));
				String nowPriceValue = BrowserActions.getText(driver, nowPrice, "Now Price Value");
				dataToBeReturned.put("Price", nowPriceValue.replace("$", ""));
			} catch (Exception e) {
				Log.event("Now price value is not displayed");
			}
			WebElement quantityValue = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div>span[class*='qty-value']"));
			String qtyValue = BrowserActions.getText(driver, quantityValue, "quantity Value");
			dataToBeReturned.put("Quantity", qtyValue);
			// Writing quantity in hashmap

			productDetails.add(dataToBeReturned);
		}

		return productDetails;
	}

	/**
	 * to get the product details in order summary section
	 * 
	 * @return
	 * @throws Exception
	 * 
	 *             Last modified by : Dhanapal.K
	 * 
	 *             Last Modified date : 9/1/2017
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInOrderSummary1() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		int nosProduct = driver.findElements(By.cssSelector(".line-item.mini-cart-product")).size();
		for (int i = 0; i < nosProduct; i++) {
			LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();
			// Writing product name in hashmap
			String productName = driver.findElements(By.cssSelector(".checkout-order-totals .name > a")).get(i)
					.getAttribute("title").trim().replace("Go to Product:", "").trim();
			// String upc =
			// "";//driver.findElements(By.cssSelector("")).get(i).getText().trim();
			String color = driver
					.findElements(By.cssSelector(".checkout-order-totals div[data-attribute='color'] .value")).get(i)
					.getText().trim();
			String size = driver
					.findElements(By.cssSelector(".checkout-order-totals div[data-attribute='size'] .value")).get(i)
					.getText().trim();
			String qty = driver.findElements(By.cssSelector(".checkout-order-totals .sku-qty .value")).get(i).getText()
					.trim();
			String price = new String();

			WebElement lineItem = driver.findElements(By.cssSelector(".line-item-price")).get(i);
			BrowserActions.scrollToView(lineItem, driver);
			if (lineItem.getAttribute("innerHTML").contains("Clearance")) {
				price = lineItem.findElement(By.cssSelector(".now-price")).getText().replace(" ", "").trim()
						.split("\\$")[1].trim();
				// } else if
				// (lineItem.getAttribute("innerHTML").contains("Now")) {
			} //else if (Utils.waitForElement(driver, lineItem.findElement(By.cssSelector(".now-price")))) {
			else if (lineItem.getAttribute("innerHTML").contains("Now")){
				price = lineItem.findElement(By.cssSelector(".now-price")).getText().trim().split("\\$")[1].trim();
			} else if (Utils.waitForElement(driver, lineItem.findElement(By.cssSelector(".standard-price")))) {
				price = lineItem.findElement(By.cssSelector(".standard-price")).getText().trim().replace("$", "");
			}

			dataToBeReturned.put("ProductName", productName);
			// dataToBeReturned.put("Upc", upc);
			dataToBeReturned.put("Color", color);
			dataToBeReturned.put("Size", size);
			dataToBeReturned.put("Price", price);
			dataToBeReturned.put("Quantity", qty);

			productDetails.add(dataToBeReturned);
		}

		return productDetails;
	}

	/**
	 * To get cost details (Merchandise total, shipping cost, tax and order
	 * total cost) in Order Summary section
	 * 
	 * @return LinkedHashMap<String, String>
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getCostDetailsInOrderSummary() throws Exception {
		LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();
		String MerchandiseTotalCostValue = Utils.checkPriceWithDollar(
				BrowserActions.getText(driver, txtMerchandiseTotalCost, "Merchandise Total Cost Value"));
		// 1
		dataToBeReturned.put("MerchandiseTotal", MerchandiseTotalCostValue.replace("$", ""));

		try {
			String totalCouponSavings = BrowserActions.getText(driver, txtTotalCouponSavings, "Total Coupon Savings");
			// 2
			dataToBeReturned.put("CouponSavings", totalCouponSavings.replace("-$", ""));
		} catch (Exception e) {
			Log.event("Total Coupon Savings is not displayed");
		}

		if (pricingDetails.getText().contains("Associate Discount")) {
			String associateDiscountValue = BrowserActions.getText(driver, txtTotalCouponSavings,
					"Associate Discount amount");
			dataToBeReturned.put("AssociateDiscount", associateDiscountValue.replace("-$", ""));
		}

		if (pricingDetails.getText().contains("Gift Box")) {
			String giftBoxValue = BrowserActions.getText(driver, txtGiftBoxValue, "gift Box amount");
			// 3
			dataToBeReturned.put("Giftbox", giftBoxValue.replace("$", ""));
		}

		if (pricingDetails.getText().contains("Surcharge")) {
			String surChargeValue = Utils
					.checkPriceWithDollar(BrowserActions.getText(driver, txtSurchargeValue, "surcharge amount"));
			// 4
			dataToBeReturned.put("Surcharge", surChargeValue.replace("$", ""));
		}

		String shippingCostValue = BrowserActions.getText(driver, txtShippingCost, "Shipping Cost Value");
		// 5
		dataToBeReturned.put("EstimatedShipping", shippingCostValue.replace("$", ""));

		if (Utils.waitForElement(driver, shippingDiscountInOrdersummary)) {
			String shippingDiscount = shippingDiscountInOrdersummary.findElements(By.cssSelector("td")).get(1).getText()
					.replace("$", "").trim();
			// 4
			dataToBeReturned.put("ShippingDiscount", shippingDiscount.trim());
		}

		String estimatedSalesTaxValue = Utils.checkPriceWithDollar(
				BrowserActions.getText(driver, txtEstimatedSalesTaxCost, "Estimated Sales Tax Value"));
		// 6
		dataToBeReturned.put("SalesTax", estimatedSalesTaxValue.replace("$", ""));
		String orderTotalValue = Utils
				.checkPriceWithDollar(BrowserActions.getText(driver, txtTotalOrderValure, "Order Total Value"));
		// 7
		dataToBeReturned.put("EstimatedOrderTotal", orderTotalValue.replace("$", ""));

		return dataToBeReturned;
	}

	/**
	 * To verify whether sum of available costs (Merchandise total, shipping
	 * cost, tax) is equal to order total cost
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyOrderTotalInOrderSummary() throws Exception {
		boolean status = false;
		LinkedHashMap<String, String> dataForCostDetails = new LinkedHashMap<>();
		dataForCostDetails = getCostDetailsInOrderSummary();
		List<String> indexes = new ArrayList<String>(dataForCostDetails.keySet());
		double cost = 0;
		for (int i = 0; i < indexes.size() - 1; i++) {
			cost += Double.parseDouble(dataForCostDetails.get(indexes.get(i)).replace("$", ""));
			cost = Math.round(cost * 100D) / 100D;
		}
		double totalCost = Double.parseDouble(dataForCostDetails.get(indexes.get(indexes.size() - 1)).replace("$", ""));
		if (cost == totalCost)
			status = true;

		return status;
	}

	/**
	 * To get the shipping Address in Shipping Page
	 * 
	 * @return
	 * @throws Exception
	 *             Last modified By - Nandhini & Date modified - 1/3/2017
	 */
	public LinkedHashMap<String, String> getShippingAddressInShippingPage() throws Exception {
		LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();
		dataToBeReturned.put("FirstName", txtFirstNameInShipping.getAttribute("value"));
		dataToBeReturned.put("LastName", txtLastNameInShipping.getAttribute("value"));
		dataToBeReturned.put("Address", txtAddress_1InShipping.getAttribute("value"));
		dataToBeReturned.put("Address2", txtAddress_2InShipping.getAttribute("value"));
		dataToBeReturned.put("City", txtCityInShipping.getAttribute("value"));
		String state = drpStateInShipping.getText();
		dataToBeReturned.put("State", StateUtils.getStateCode(state));
		dataToBeReturned.put("Zipcode", txtZipcodeInShipping.getAttribute("value"));
		dataToBeReturned.put("Phone", txtPhoneInShipping.getAttribute("value").replace("-", ""));
		String shipMethod = slectedShippingType.findElement(By.xpath("..//..")).getText().split(":")[0];
		dataToBeReturned.put("ShippingMethod", shipMethod);
		return dataToBeReturned;
	}

	/**
	 * To get the shipping Address in Billing Page
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBillingAddressInBillingPage() throws Exception {
		LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();
		dataToBeReturned.put("FirstName", txtFirstNameInBilling.getAttribute("value"));
		dataToBeReturned.put("LastName", txtLastNameInBilling.getAttribute("value"));
		dataToBeReturned.put("Address", txtAddress_1InBilling.getAttribute("value"));
		dataToBeReturned.put("Address", txtAddress_1InBilling.getAttribute("value"));
		dataToBeReturned.put("Address2", txtAddress_2InBilling.getAttribute("value"));
		dataToBeReturned.put("City", txtCityInBilling.getAttribute("value"));
		String state = drpStateInShipping.getText();
		System.out.println(state);
		dataToBeReturned.put("State", StateUtils.getStateCode(state));
		dataToBeReturned.put("Zipcode", txtZipcodeInBilling.getAttribute("value"));
		dataToBeReturned.put("Phone", txtPhoneNoInBilling.getAttribute("value").replace("-", ""));
		return dataToBeReturned;
	}

	/**
	 * To get the product details from the place order page
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInPlaceOrder() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		BrowserActions.scrollToView(lstProductNamesInOrderSummary.get(0), driver);
		for (int i = 0; i < lstProductNamesInOrderSummary.size(); i++) {
			LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();

			// Writing product name in hashmap
			String pdtName = BrowserActions.getText(driver, lstProductNamesInOrderSummary.get(i), "Product Name");
			dataToBeReturned.put("ProductName", pdtName);
			// Writing product upc in hashmap
			WebElement elementUpc = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector(".product-UPC"));
			String upcValue = BrowserActions.getText(driver, elementUpc, "UPC value");
			dataToBeReturned.put("Upc", upcValue.trim());
			// Writing color in hashmap
			WebElement elementColor = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div[data-attribute='color']"));
			String colorName = BrowserActions.getText(driver, elementColor, "Color Name");
			dataToBeReturned.put("Color", colorName.split("\\:")[1].trim());
			// Writing size in hashmap
			WebElement elementSize = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div[data-attribute='size']"));
			String sizeValue = BrowserActions.getText(driver, elementSize, "Size Value");
			dataToBeReturned.put("Size", sizeValue.split("\\:")[1].trim());
			// Writing quantity in hashmap

			try {
				// Writing Standard price value in hashmap
				WebElement stdPrice = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector(".standard-price"));
				String stdPriceValue = BrowserActions.getText(driver, stdPrice, "Standard Price Value");
				// Log.message(stdPriceValue);
				dataToBeReturned.put("Price", stdPriceValue.split("\\$")[1].trim());
			} catch (Exception e) {
				Log.event("Standard price value is not displayed");
			}
			try {
				// Writing Now price value in hashmap
				WebElement nowPrice = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../../../.."))
						.findElement(By.cssSelector(".now-price"));
				String nowPriceValue = BrowserActions.getText(driver, nowPrice, "Now Price Value");
				Log.message(nowPriceValue);
				dataToBeReturned.put("Price", nowPriceValue.split("\\$")[1].trim());
			} catch (Exception e) {
				Log.event("Now price value is not displayed");
			}
			WebElement quantityValue = lstProductNamesInOrderSummary.get(i).findElement(By.xpath("../.."))
					.findElement(By.cssSelector("div>span[class*='qty-value']"));
			String qtyValue = BrowserActions.getText(driver, quantityValue, "Size Value");
			dataToBeReturned.put("Quantity", qtyValue);

			productDetails.add(dataToBeReturned);
		}

		return productDetails;
	}

	public LinkedList<LinkedHashMap<String, String>> getProductDetailsInPlaceOrder1() throws Exception {
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		BrowserActions.scrollToView(lstProductNamesInOrderSummary.get(0), driver);
		for (int i = 0; i < lstProductNamesInOrderSummary.size(); i++) {
			LinkedHashMap<String, String> dataToBeReturned = new LinkedHashMap<String, String>();

			String productName = driver.findElements(By.cssSelector(".product-list-item .name > a")).get(i)
					.getAttribute("title").trim().replace("Go to Product:", "").trim();
			String upc = driver.findElements(By.cssSelector(".product-UPC")).get(i).getText().trim();
			String color = driver.findElements(By.cssSelector(".product-list-item div[data-attribute='color'] .value"))
					.get(i).getText().trim();
			String size = driver.findElements(By.cssSelector(".product-list-item div[data-attribute='size'] .value"))
					.get(i).getText().trim();
			String qty = driver.findElements(By.cssSelector(".product-list-item .sku-qty .value")).get(i).getText()
					.trim();
			String price = driver.findElements(By.cssSelector(".subtotal-label.bold")).get(i).getText().trim()
					.split("\\$")[1].trim();// new String();

			/*
			 * WebElement lineItem =
			 * driver.findElements(By.cssSelector(".line-item-price")).get(i);
			 * BrowserActions.scrollToView(lineItem, driver);
			 * if(lineItem.getAttribute("innerHTML").contains("Clearance")){
			 * price =
			 * lineItem.findElement(By.cssSelector(".now-price")).getText().
			 * replace(" ", "").trim().split("\\$")[1].trim(); } else
			 * if(lineItem.getAttribute("innerHTML").contains("Now")){ price =
			 * lineItem.findElement(By.cssSelector(".price-sales")).getText().
			 * trim().split("\\$")[1].trim(); } else
			 * if(Utils.waitForElement(driver,
			 * lineItem.findElement(By.cssSelector(".standard-price")))){ price
			 * =
			 * lineItem.findElement(By.cssSelector(".standard-price")).getText()
			 * .trim().replace("$", ""); }
			 */

			dataToBeReturned.put("ProductName", productName);
			dataToBeReturned.put("Upc", upc);
			dataToBeReturned.put("Color", color);
			dataToBeReturned.put("Size", size);
			dataToBeReturned.put("Price", price);
			dataToBeReturned.put("Quantity", qty);

			productDetails.add(dataToBeReturned);
		}

		return productDetails;
	}

	public int getProductCountInBillingPage() {
		return ProductCountInBillingPage.size();
	}

	public void clikOnMiniCartToggle() throws Exception {
		Utils.waitForElement(driver, clikOnToggle);
		BrowserActions.clickOnElement(clikOnToggle, driver, "clik On Toogle");
	}

	public String getTextFromOrderSummaryinPlaceOrderPage() throws Exception {
		return BrowserActions.getText(driver, pricingDetails, "Text From order summary");
	}

	public String getBillingDetailsInPlaceOrder() throws Exception {
		String billingDetails = BrowserActions.getText(driver, billingModule, "Billing Details");
		return billingDetails;

	}

	public String getTextFromPaymentdetailsInExpressCheckOut() throws Exception {
		BrowserActions.nap(2);
		return BrowserActions.getText(driver, txtPaymentDetailsInExpressCheckout, "Text From Billing Header");
	}

	/**
	 * To get the Invalid Credit Card Error Message
	 * 
	 * @return txtInvalidCardNoError
	 * 
	 */

	public String getTextFromInvalidCreditCardErrorMsg() throws Exception {
		String txtInvalidCardNoError = BrowserActions.getText(driver, WrongCVN, "Error Message");
		System.out.println(txtInvalidCardNoError);
		return txtInvalidCardNoError;
	}

	/**
	 * To get the input text from Credit Card Number textfield
	 * 
	 * @return verifyCreditCardLastFourNo;
	 * 
	 */

	public String getTextFromCreditCardNumberLastFourNo() throws Exception {
		String CreditCardNumber = BrowserActions.getTextFromAttribute(driver, txtfieldCreditCard, "value",
				"Credit card number");
		System.out.println(CreditCardNumber);
		String verify[] = CreditCardNumber.split("\\*");
		String verifyCreditCardLastFourNo = verify[12];
		return verifyCreditCardLastFourNo;

	}

	/**
	 * To click on apply button in Express checkout
	 * 
	 * @Clicked on apply button
	 * @throws Exception
	 */
	public void clickOnApplyButton() throws Exception {
		BrowserActions.clickOnElement(btnApplyInPlaceOrderPage, driver, "Apply button in shipping");
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);
	}

	/**
	 * To get the error message
	 * 
	 * @return txterrormsg
	 * 
	 */

	public String getTextfromCvvErrorMsg() throws Exception {
		String txterrormsg = BrowserActions.getText(driver, CvvErrorMsg, "Error Message");
		return txterrormsg;
	}

	/**
	 * To verify cvv field is empty
	 * 
	 * @return true if cvv field is empty
	 * @throws Exception
	 */

	public boolean verifyCvvfieldIsEmpty() throws Exception {
		boolean status = false;

		if (clikOnCvv.getAttribute("value").equals("")) {
			status = true;
		}
		return status;
	}

	/**
	 * To enter a valid cvv in Cvv field
	 * 
	 * @throws Exception
	 *             Last Modified By - Gowri & Last Modified Date - 1/2/2017
	 */

	public void enterCVVinExpressCheckout(String customerDetails) throws Exception {
		BrowserActions.scrollToViewElement(clikOnCvv, driver);
		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String validcvv = cutomercardDetails.split("\\|")[5];
		BrowserActions.typeOnTextField(clikOnCvv, validcvv, driver, "CVV");
		Log.event("Entered the Valid Cvv " + validcvv);
	}

	public OrderConfirmationPage ClickOnPlaceOrderButton() throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop") {
			Utils.waitForElement(driver, btnPlaceOrderdesktop);
			BrowserActions.clickOnElement(btnPlaceOrderdesktop, driver, "Clicked Place Order Button");
		} else if (runPltfrm == "mobile") {
			Utils.waitForElement(driver, btnPlaceOrdermobile);
			BrowserActions.javascriptClick(btnPlaceOrdermobile, driver, "Clicked Place Order Button");
		}

		Utils.waitForPageLoad(driver);
		return new OrderConfirmationPage(driver).get();
	}

	/**
	 * return shippingmethods
	 * 
	 * @throws Exception
	 */
	public String getShippingMethods() throws Exception {
		BrowserActions.clickOnElement(drpDownShipppingMethods, driver, "Shipping method List");
		return BrowserActions.getText(driver, lstShipppingMethods, "Shipping Methods List");
	}

	public void clickOnWhyStandardShipping() throws Exception {
		BrowserActions.clickOnElement(lnkWhyStandardShipping, driver, "Why standard Shipping Only? link");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnCloseShippingModelPopup() throws Exception {
		BrowserActions.clickOnElement(closeShippingModelPopup, driver, "Why standard Shipping Only? link");
		Utils.waitForPageLoad(driver);

	}

	public ShoppingBagPage clickOnGoToShoppingBagInPopUp() throws Exception {
		BrowserActions.clickOnElement(lnkGoToShoppingBagINpopup, driver, "Go To ShoppingBag' link");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver);
	}

	public void clickOnContinueInStandardShippingPopup() throws Exception {
		BrowserActions.clickOnElement(btnContinueInStandardShippingInPopup, driver, "Why standard Shipping Only? link");
		Utils.waitForPageLoad(driver);
		clickOnContinueInShipping();
	}

	@FindBy(css = ".free-shipping")
	WebElement txtFreeShipping;

	@FindBy(css = ".shipping-promotion")
	WebElement msgShipmentResource;

	/**
	 * To get the Shipping promotion message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getTextFromShippingPromotionMsg() throws Exception {
		return BrowserActions.getText(driver, txtFreeShipping, "Free shipping");
	}

	/**
	 * To Fill the address one text field
	 * 
	 * @param address1
	 * @throws Exception
	 */
	public void FillAddressOneInShipping(String address1) throws Exception {
		txtAddress_1InShipping.clear();
		txtAddress_1InShipping.sendKeys(address1);
	}

	public boolean ContinueAddressValidationModalWithOriginalAddress() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.selectRadioOrCheckbox(chkOriginal, "YES");
			BrowserActions.clickOnElement(btnContinueInAddressValidationModal, driver,
					"Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);
			clickOnContinueInShipping();
			return true;
		}
		return false;
	}

	@FindBy(css = ".old-details:nth-child(2)")
	WebElement txtOriginalAddressInSuggestion;

	/**
	 * To get the zip code text from Original address
	 */
	public String returnZipCodeInOriginalAddress() throws Exception {
		String txtOriginalAddress = BrowserActions.getText(driver, txtOriginalAddressInSuggestion,
				"Txt suggested address");
		String zipCode[] = txtOriginalAddress.split(" ");
		return zipCode[2];

	}

	/**
	 * To get the text from payment method panel
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getTextFromPayMentMethodPanel() throws Exception {
		return BrowserActions.getText(driver, txtPaymentMethod, "Payment Method");
	}

	public String getTextFromCardNumber() throws Exception {
		return BrowserActions.getText(driver, txtCardNumber, "Card Number");
	}

	/**
	 * To check last four digit of card no as digit and all other is *
	 * 
	 * @param cardNo
	 * @return
	 * @throws Exception
	 */
	public boolean VerifyCardMask(String cardNo) throws Exception {
		boolean status1 = false;
		boolean status2 = false;

		String cardNoReverse = new StringBuffer(cardNo).reverse().toString();
		String[] cardNoSeparate = cardNoReverse.split("(?!^)");
		for (int i = 0; i < cardNoSeparate.length; i++) {
			if (i < 4) {
				if (cardNoSeparate[i].matches("\\d{1}")) {
					status1 = true;
				} else
					break;
			} else if (i >= 4 && i < cardNoSeparate.length) {
				if (!cardNoSeparate[i].matches("\\d{1}")) {
					status2 = true;
				} else
					break;
			} else
				break;

		}

		return status1 && status2;

	}

	/**
	 * To get the first name in text field
	 * 
	 * @return
	 * @throws Exception
	 *             Last modified By - Jayanthi & Date modified - 1/3/2017
	 */

	public String getTextFromFirstNameTextFieldInShipping() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtFldFirstName, "value", "");

	}

	/**
	 * To get the last name in text field Last modified By - Jayanthi & Date
	 * modified - 1/3/2017
	 */
	public String getTextFromLastNameTextFieldInShipping() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtFldLastName, "value", "");
	}

	/**
	 * To get the text from zip code text field in billing page
	 * 
	 */
	public String getTextFromZipCode() throws Exception {
		return BrowserActions.getText(driver, txtFldZipCode, "Zipcode");
	}

	public String getShippingAddressCity() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtCityInShipping, "value", "city");

	}

	public void clickBtnShippingPage() throws Exception {
		BrowserActions.clickOnElement(btnContinueNaviageteToBillingPage, driver, "");
		Utils.waitForPageLoad(driver);
	}

	public String GetOrderTotalValue() throws Exception {
		return BrowserActions.getText(driver, orderValue, "Order Total value");
	}

	public void clickOnAddInShipToMulipleAddress(int index) throws Exception {
		BrowserActions.clickOnElement(lnkAddInShipToMultipleAddress.get(index), driver,
				"Add Link in Product List in Ship to Multiple Address");
		Utils.waitForPageLoad(driver);
	}

	public boolean verifyAddressInShipToMultipleAddressDropDown(int index, LinkedHashMap<String, String> shipDetails)
			throws Exception {
		/*
		 * String adrs = adrsInMultiShipping.get(index).getText(); String name
		 * =adrs.split("//,")[0]; String fname = name.split("// ")[0]; String
		 * lname = name.split("// ")[1]; String address =adrs.split("//,")[1];
		 * String city =adrs.split("//,")[2]; String state
		 * =adrs.split("//,")[3]; String pin =adrs.split("//,")[4]; String
		 * shipfname = shipDetails.get(txtMultipleShippingFirstname); String
		 * shiplname = shipDetails.get(txtMultipleShippingLastname); String
		 * shipadrs = shipDetails.get(txtMultipleShippingAddress); String
		 * shipcity = shipDetails.get(txtMultipleShippingCity); String shipstate
		 * = shipDetails.get(btnMultipleShippingState); String shipzipcode =
		 * shipDetails.get(txtMultipleShippingZipcode);
		 * 
		 * if( (fname==shipfname)&& (lname==shiplname)&& (address==shipadrs)&&
		 * (state==shipstate)&& (pin==shipzipcode) ) return true; else
		 */
		return false;

	}

	public void clickAddressInShipToMultipleAddressDropdowm(int index) throws Exception {
		WebElement addressDropDown = driver.findElement(By.cssSelector(
				"table[class='item-list'] > tbody > tr:nth-child(1) .shippingaddress.required .selected-option"));
		BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown Button");
		Utils.waitForPageLoad(driver);
		List<WebElement> lstAddress = selectAddressInShipToMultipleAddress.get(index).findElement(By.xpath(".."))
				.findElements(By.cssSelector("ul>li"));
		BrowserActions.clickOnElement(lstAddress.get(index + 1), driver, "clicked address from the dropdown");

	}

	public void selectSavedCard(String customerDetails) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];
		String cardExpireMonth = cutomercardDetails.split("\\|")[3];
		String cardExpireYear = cutomercardDetails.split("\\|")[4];
		String cardCVN = cutomercardDetails.split("\\|")[5];

		cardDetails.put("select_cardtype_" + cardType, btnCardType);
		cardDetails.put("type_cardname_" + cardOwner, txtNameOnCard);
		cardDetails.put("type_cardno_" + cardNo, txtCardNo);
		cardDetails.put("select_expmonth_" + cardExpireMonth, btnCardExpMonth);
		cardDetails.put("select_expyr_" + cardExpireYear, btnCardExpYear);
		cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);

		PaymentUtils.clickCreditCardPayment(BrowserActions.checkLocator(driver, btnCardType), driver);
		BrowserActions.clickOnElement(BrowserActions.checkLocator(driver, btnCardType).findElement(By.xpath(".."))
				.findElement(By.cssSelector("ul>li:nth-child(2)")), driver, "select card from drop down");
		PaymentUtils.enterCVCNumber(BrowserActions.checkLocator(driver, txtCvnNo), cardCVN, driver);
		Log.event("Clicked Saved card", StopWatch.elapsedTime(startTime));
		Utils.waitForPageLoad(driver);
		BrowserActions.nap(2);

	}

	public void chooseSuggestedAddressInModal() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.selectRadioOrCheckbox(rdoSuggestedAddress, "YES");
			Utils.waitForPageLoad(driver);
		}
	}

	public String selectSavedShippingAddressByIndex(int index) throws Exception {
		String savedAddress = null;
		BrowserActions.clickOnElement(drpSavedAddressShipping, driver, "SortBy dropdown");
		BrowserActions.clickOnElement(lstSavedAddreses.get(index), driver, "Saved Address dropdown list");
		savedAddress = driver
				.findElement(By.cssSelector(
						"#dwfrm_singleshipping_shippingAddress> fieldset > div.form-row.select-address > div > div > div"))
				.getText();
		// savedAddress = savedAddress.split(" ")[2];
		return savedAddress;
	}

	public String getTextFromAddress() throws Exception {
		String address = null;
		address = BrowserActions
				.getText(driver, PincodeAddress, "Fetching the search text value  in the search result page")
				.replace("CA, CA ", "");

		return address;
	}

	public void addGiftCardValue(String text[]) throws Exception {
		BrowserActions.typeOnTextField(belkGiftCard, text[0], driver, "Add Card Number In Belk Gift Card");
		BrowserActions.typeOnTextField(belkGiftCardCvv, text[1], driver, "Add CVV Number In Belk Gift Cardr");
		BrowserActions.nap(5);
		clickOnContinueInBilling();

	}

	public void selectRandomAddressInAddressDropDown() throws Exception {
		List<WebElement> lstProductInShipToMultipleAddress = driver
				.findElements(By.cssSelector("table[class='item-list'] > tbody > tr"));
		int nosProduct = lstProductInShipToMultipleAddress.size();
		List<WebElement> lstAddressInDrp = driver.findElements(By.cssSelector(
				"table[class='item-list'] > tbody > tr:nth-child(1) .shippingaddress.required .custom-select >ul >li:not([class='selected'])"));
		int nosAddress = lstAddressInDrp.size();
		for (int i = 0, j = 0; i < nosProduct; i++, j++) {
			if (j == nosAddress)
				j = 0;
			WebElement addressDropDown = lstProductInShipToMultipleAddress.get(i)
					.findElement(By.cssSelector(".shippingaddress.required .selected-option"));

			BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown");
			Utils.waitForPageLoad(driver);
			List<WebElement> addressInDrp = lstProductInShipToMultipleAddress.get(i).findElements(
					By.cssSelector(".shippingaddress.required .custom-select >ul >li:not([class='selected'])"));
			;
			BrowserActions.clickOnElement(addressInDrp.get(j), driver, "First Address in Dropdown");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To select the address from dropdown and return the list of selected
	 * address as hashmap
	 * 
	 * @return
	 * @throws Exception
	 * 
	 *             Created By : Dhanapal. K Last Modified By : Dhanapal. K Last
	 *             Modified Date : 29/01/2017
	 */
	public LinkedList<LinkedHashMap<String, String>> selectAddrInAddressDropDownInMultiship() throws Exception {
		LinkedList<LinkedHashMap<String, String>> selectedAddressByProduct = new LinkedList<LinkedHashMap<String, String>>();
		List<WebElement> lstProductInShipToMultipleAddress = driver
				.findElements(By.cssSelector("table[class='item-list'] > tbody > tr.cart-row"));

		int nosProduct = lstProductInShipToMultipleAddress.size();
		List<WebElement> lstAddressInDrp = lstProductInShipToMultipleAddress.get(0).findElements(
				By.cssSelector(".shippingaddress.required .custom-select >ul >li:not([class='selected'])"));

		List<WebElement> lstDrp = driver.findElements(By.cssSelector(".shippingaddress .selected-option"));

		for (int j = 0; j < nosProduct; j++) {
			System.out.print(lstDrp.get(j).getText());
			if (!(lstDrp.get(j).getText().trim().equalsIgnoreCase("select"))) {
				BrowserActions.clickOnElement(lstDrp.get(j), driver, "Address Dropdown");
				Utils.waitForPageLoad(driver);
				WebElement element = driver
						.findElement(By.cssSelector(".shippingaddress .selection-list li[value='']"));
				BrowserActions.clickOnElement(element, driver, "Address Dropdown");
				Utils.waitForPageLoad(driver);
			}
		}

		int nosAddress = lstAddressInDrp.size();
		for (int i = 0, j = 0; i < nosProduct; i++, j++) {
			LinkedHashMap<String, String> address = new LinkedHashMap<String, String>();
			if (j == nosAddress)
				j = 0;
			WebElement addressDropDown = lstProductInShipToMultipleAddress.get(i)
					.findElement(By.cssSelector(".shippingaddress.required .selected-option"));
			BrowserActions.clickOnElement(addressDropDown, driver, "Address Dropdown");
			Utils.waitForPageLoad(driver);
			List<WebElement> addressInDrp = lstProductInShipToMultipleAddress.get(i).findElements(
					By.cssSelector(".shippingaddress.required .custom-select >ul >li:not([class='selected'])"));
			BrowserActions.clickOnElement(addressInDrp.get(j), driver, "First Address in Dropdown");
			Utils.waitForPageLoad(driver);
			String selectedAddress = lstProductInShipToMultipleAddress.get(i)
					.findElement(By.cssSelector(".shippingaddress.required .selected-option")).getText().trim();

			String firstName = selectedAddress.split(" ")[0].trim();
			String lastName = selectedAddress.split(" ")[1].replace(",", "").trim();
			String Address = selectedAddress.split(",")[1].trim();
			String address2 = new String();
			String city = new String();
			String state = new String();
			String zipCode = new String();
			if (selectedAddress.split("\\,").length > 5) {
				address2 = selectedAddress.split(",")[2].trim();
				city = selectedAddress.split(",")[2].trim();
				state = selectedAddress.split(",")[3].trim();
				zipCode = selectedAddress.split(",")[4].trim();
			}
			city = selectedAddress.split(",")[2].trim();
			state = selectedAddress.split(",")[3].trim();
			zipCode = selectedAddress.split(",")[4].trim();
			String phone = new String();

			address.put("FirstName", firstName);
			address.put("LastName", lastName);
			address.put("Address", Address);
			if (selectedAddress.split("\\,").length > 5)
				address.put("Address2", address2);
			address.put("City", city);
			address.put("State", state);
			address.put("Zipcode", zipCode);
			address.put("Phone", phone);
			// String productName =
			// lstProductInShipToMultipleAddress.get(i).findElement(By.cssSelector(".name
			// > a")).getAttribute("title").trim();
			selectedAddressByProduct.add(address);
		}

		return selectedAddressByProduct;
	}

	public LinkedHashMap<String, String> selectShippingMethodInMultiShippingByType() throws Exception {
		LinkedHashMap<String, String> selectedShippingMethods = new LinkedHashMap<String, String>();
		/*
		 * for(int i=0; i < lstShippingMethods.size(); i++){
		 * BrowserActions.clickOnElement(lstShippingMethods.get(i), driver, i +
		 * "th Shipping Method"); List<WebElement>
		 * lstShippingMethodsInMultiShipping =
		 * lstShippingMethods.get(i).findElement(By.xpath("..")).findElements(By
		 * .cssSelector("ul li"));
		 * 
		 * if(lstShippingMethodsInMultiShipping.size() != 0){ int rand =
		 * Utils.getRandom(0, lstShippingMethodsInMultiShipping.size());
		 * BrowserActions.clickOnElement(lstShippingMethodsInMultiShipping.get(
		 * rand), driver, (rand + 1) + "th Address in dropdown");
		 * Utils.waitForPageLoad(driver);
		 * selectedShippingMethods.put("ShippingMethod"+(i+1),
		 * lstShippingMethodsInMultiShipping.get(rand).getText().trim()); } else
		 * selectedShippingMethods.put("ShippingMethod"+(i+1),
		 * lstShippingMethods.get(i).getText().trim()); }
		 */

		return selectedShippingMethods;
	}

	public String selectShippingMethodInMultiShippingByRow(int methodIndex, String shippingType) throws Exception {
		String sType = new String();
		if (shippingType.equals("Standard"))
			sType = "Ground";
		else if (shippingType.equals("Express"))
			sType = "2DAY";
		else if (shippingType.equals("Overnight"))
			sType = "OVNT";
		else if (shippingType.equals("Test"))
			sType = "Test_Shipment";
		else if (shippingType.equals("Free"))
			sType = "Free";
		else
			sType = "Ground";

		String selectedShippingMethods = new String();
		BrowserActions.clickOnElement(lstShippingMethods.get(methodIndex - 1), driver,
				methodIndex + "th Shipping Method");
		if (sType.equals("Ground"))
			BrowserActions.clickOnElement(lstShippingMethods.get(methodIndex - 1), driver, sType + " in dropdown");
		else {
			WebElement element = lstShippingMethodDiv.get(methodIndex - 1)
					.findElement(By.cssSelector("li[value='" + sType + "']"));
			BrowserActions.scrollToViewElement(element, driver);
			BrowserActions.clickOnElement(element, driver, sType + " in dropdown");
		}
		Utils.waitForPageLoad(driver);
		// selectedShippingMethods =
		// lstShippingMethodsInMultiShipping.get(addressIndex -
		// 1).getText().trim();

		selectedShippingMethods = lstShippingMethods.get(methodIndex - 1).getText().trim();

		return selectedShippingMethods;
	}

	public int getNosShippingMethodInMultishipping() throws Exception {
		return lstShippingMethods.size();
	}

	public void selectShippingMethod(String shipType) throws Exception {
		String locator = new String();
		String description = new String();
		if (shipType.equalsIgnoreCase("Standard")) {
			locator = rdoStandard;
			description = "Standard Type";
		} else if (shipType.equalsIgnoreCase("Express")) {
			locator = rdoExpress;
			description = "Express Type";
		} else if (shipType.equalsIgnoreCase("Overnight")) {
			locator = rdoOvernight;
			description = "Overnight Type";
		} else {
			Log.failsoft("Unknown Shipping type entered. Continuing with Standard shipping type.");
			locator = rdoStandard;
		}
		BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)), "YES");
		Utils.waitForPageLoad(driver);
		waitForSpinner();
	}

	/**
	 * To get the payment method details
	 * 
	 * @return 'CardType','NameOnCard','CardNumber','CardExpiry','Amount'
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getPaymentMethodInPlaceOrder() throws Exception {
		LinkedHashMap<String, String> paymentMethod = new LinkedHashMap<String, String>();
		paymentMethod.put("CardType", lblCardType.getText());
		paymentMethod.put("NameOnCard", lblNameOnCard.getText());
		paymentMethod.put("CardNumber", txtCardNumberInPaymentMethod.getText());
		paymentMethod.put("Amount", lblAmount.getText().trim().replace("$", ""));
		return paymentMethod;
	}

	/**
	 * To get GiftCard Payment Details InPlaceOrder
	 * 
	 * @return 'Gift card number', 'Amount'
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getGiftCardPaymentDetailsInPlaceOrder() {
		LinkedHashMap<String, String> giftCardDetails = new LinkedHashMap<String, String>();
		giftCardDetails.put("GiftCardNumber", txtGiftCardNumberInPaymentMethod.getText().trim().split("\\:")[1].trim());
		if (lblAmount.getText().contains("$")) {
			giftCardDetails.put("Amount", lblAmount.getText().trim().replace("$", ""));
		} else {
			giftCardDetails.put("Amount", lblAmount.getText());
		}

		return giftCardDetails;
	}

	public int getProductCountInMultiShip() throws Exception {
		return lstProductInMultiShip.size();
	}

	/**
	 * getCheckoutPropertyValue
	 * 
	 * @param chkKey
	 * @return
	 * @throws Exception
	 */
	public String[] getCheckoutPropertyValue(String chkKey) throws Exception {

		String chkProperty = checkoutProperty.getProperty(chkKey);

		String[] chkValue = chkProperty.split("\\|");

		return chkValue;
	}

	/**
	 * getPromoMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getPromoMessage() throws Exception {
		return BrowserActions.getText(driver, lblPromo, "Promo message").trim();
	}

	public void clickPaymentEditLink() throws Exception {
		BrowserActions.clickOnElement(lnkPaymentEdit, driver, "Payment Edit link");
	}

	public String getCreditCardTypeFromEditPayment() throws Exception {
		String paymentType = BrowserActions.getText(driver, txtCreditCardType, "Payment Type");
		return paymentType;
	}

	public void clickOrderSummaryEditLink() throws Exception {
		BrowserActions.clickOnElement(lnkOrdersummaryEdit, driver, "Payment Edit link");
	}

	/**
	 * To click the continue button with original address in multiship
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean ContinueAddressValidationModalWithOriginalAddressInMultiShip() throws Exception {
		if (Utils.waitForElement(driver, addressValidationModal)) {
			BrowserActions.selectRadioOrCheckbox(chkOriginalInMultiShip, "YES");
			BrowserActions.clickOnElement(btnContinueInAddressValidationModal, driver,
					"Continue Button in Address Validation Modal");
			Utils.waitForPageLoad(driver);

			return true;
		}
		return false;
	}

	public void clickOnContinueInBilling() throws Exception {
		BrowserActions.scrollToViewElement(btnContinuePlaceOrder, driver);
		BrowserActions.clickOnElement(btnContinueToPlaceOrder, driver, "Continue button in billingPage");
		Utils.waitForPageLoad(driver);
	}

	public String GetShippingtype() throws Exception {
		return BrowserActions.getText(driver, shippingType, "Shipping type");
	}

	/**
	 * To Get GiftCard balance
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getGiftCardBalance() throws Exception {
		String giftCardBalance;
		Utils.waitForElement(driver, txtGiftCardMessage);
		BrowserActions.scrollToView(txtGiftCardMessage, driver);
		giftCardBalance = BrowserActions.getText(driver, txtGiftCardMessage, "GiftCard balance").split(":")[1].trim();
		giftCardBalance = giftCardBalance.replaceAll("\\$", "").trim();
		return giftCardBalance;
	}

	/**
	 * To Get GiftCard balance message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getGiftCardBalanceMessage() throws Exception {
		String giftCardBalance;
		Utils.waitForElement(driver, txtGiftCardMessage);
		BrowserActions.scrollToView(txtGiftCardMessage, driver);
		giftCardBalance = BrowserActions.getText(driver, txtGiftCardMessage, "GiftCard balance");

		return giftCardBalance;
	}

	/**
	 * To Get GiftCard Error message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getGiftCardErrormessage() throws Exception {
		String giftCardErrorMessage;
		Utils.waitForElement(driver, txtgiftcarderrormessage);
		BrowserActions.scrollToView(txtgiftcarderrormessage, driver);
		giftCardErrorMessage = BrowserActions.getText(driver, txtgiftcarderrormessage, "GiftCard balance").toString();
		return giftCardErrorMessage;
	}

	public String getTextFromShippingAddress1() throws Exception {
		String address = BrowserActions.getText(driver, shippngAddress1, "Shipping Address1");
		return address;
	}

	public String getTextFromShippingAddress2() throws Exception {
		String address = BrowserActions.getText(driver, shippngAddress2, "Shipping Address2");
		return address;
	}

	public void selectdropdownandAddress(int dropdownindex, int Addressindex) throws Exception {
		clickAddressDropown(dropdownindex);

		WebElement element = lstAdd.get(Addressindex - 1);

		BrowserActions.clickOnElement(element, driver, "Address");

		// Utils.waitUntilElementDisappear(driver, spinner);
	}

	public void clickAddressDropown(int dropdownindex) throws Exception {

		WebElement element = driver
				.findElement(By.xpath(".//*[@id='dwfrm_multishipping_addressSelection']/div/table/tbody/tr["
						+ dropdownindex + "]/td[3]/div[1]/div"));
		BrowserActions.clickOnElement(element, driver, "SelectDropDown");

	}

	public String getTextShippingMethodInMultiShipping() throws Exception {

		String dataToBeReturned = BrowserActions
				.getText(driver, shippingmethodInMultiShipping.get(0), "shipping method in multi shipping")
				.replace(":", "");
		return dataToBeReturned;
	}

	public boolean verifyGiftCardNoWhileApply() throws Exception {
		if ((lblCardNumberInApplyGiftCard.getText()).contains("************")) {
			return true;
		} else {
			return false;
		}

	}

	public LinkedHashMap<String, String> getMultiShipMethodsInSection() throws Exception {
		LinkedHashMap<String, String> shipMethod = new LinkedHashMap<String, String>();

		List<WebElement> lstShippingMethod = driver.findElements(By.cssSelector("span.bold"));
		int size = lstShippingMethod.size();
		for (int i = 0; i < size; i++) {
			shipMethod.put("ShippingMethod" + (i + 1),
					lstShippingMethod.get(i).findElement(By.cssSelector("+ span")).getText().trim());
		}

		return shipMethod;
	}

	/**
	 * getAssociateDiscountMessage
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getAssociateDiscountMessage() throws Exception {
		return BrowserActions.getText(driver, lblDiscountMessage, "Associate Discount");
	}

	/**
	 * to get the error message when applying invalid BRD
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getBrdErrMsg() throws Exception {
		if (errMsgBRD.getCssValue("display").contains("block"))
			return errMsgBRD.getText().trim();
		else
			return "No Error Message Shown!";
	}

	/**
	 * to get the error message when applying invalid gift card
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGiftCardErrMsg() throws Exception {
		if (Utils.waitForElement(driver, errMsgGiftCard))
			return errMsgGiftCard.getText().trim();
		else
			return "No Error Message Shown!";
	}

	/**
	 * to get the gift card number after filling into text input field
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGiftCardNumberFromInputField() throws Exception {
		return belkGiftCard.getAttribute("value").trim();
	}

	/**
	 * to get amount spent from gift card in mini payment section
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGiftCardAmountInSection() throws Exception {
		return lblAmount.getText().replaceAll("-", "").replaceAll("\\$", "").trim();
	}

	@FindBy(css = ".details div:not([class]) + div .minibillinginfo-amount")
	List<WebElement> lblAmount1;

	/**
	 * to get amount spent from gift card in mini payment section BY index
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGiftCardAmountInSection(int index) throws Exception {
		List<WebElement> lstEle = driver.findElements(By.cssSelector("div.details"));
		List<WebElement> lblAmount2 = new ArrayList<WebElement>();
		for (int i = 0; i < lstEle.size(); i++)
			if (lstEle.get(i).getText().contains("Gift Card"))
				lblAmount2.add(lstEle.get(i).findElement(By.cssSelector(".minibillinginfo-amount")));
		// WebElement element= lblAmount1.get(index);
		return lblAmount2.get(index).getText().trim().replace("$", "").trim();
	}

	/**
	 * to get the BRD Number from Input Field
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getBRDNumberFromInputField() throws Exception {
		return txtBRDNumberInInputField.getAttribute("value").trim();
	}

	@FindBy(css = ".brd-number")
	List<WebElement> txtBRDNumberInSection;

	@FindBy(css = ".brd-number")
	WebElement singleTxtBRDNumberInSection;

	public String getBRDNumberFromSection(int index) throws Exception {
		return txtBRDNumberInSection.get(index).getText().replace("Belk Reward Dollars:", "").replace("*", "").trim();
	}

	@FindBy(css = ".brd-amount-value")
	WebElement txtBRDAmountInSection;

	/**
	 * to get amount spent from BRD from mini payment section
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getBRDAmountInSection() throws Exception {
		return txtBRDAmountInSection.getText().trim();
	}

	public String getBRDAmountInSection(int index) throws Exception {
		List<WebElement> brdAmount = driver.findElements(By.cssSelector(".brd-amount-value"));
		return brdAmount.get(index).getText().replace("$", "").trim();
	}

	public int countOfMultiAddress() {

		return lstMultiShippingAddress.size();
	}

	public void selectShippingMethod(int index) throws Exception {
		if (selectList.getText().contains("Standard")) {
			BrowserActions.clickOnElement(selectList, driver, "Select Shipping method Dropdown");
			BrowserActions.javascriptClick(overNight, driver, "SortBy dropdown");
		} else if (selectList.getText().contains("Free Shipping")) {
			BrowserActions.clickOnElement(selectList, driver, "Select Shipping method Dropdown");
			BrowserActions.javascriptClick(express, driver, "SortBy dropdown");
		} else if (selectList.getText().contains("Express")) {
			BrowserActions.clickOnElement(selectList, driver, "Select Shipping method Dropdown");
			BrowserActions.javascriptClick(overNight, driver, "SortBy dropdown");
		} else if (selectList.getText().contains("Overnight")) {
			BrowserActions.clickOnElement(selectList, driver, "Select Shipping method Dropdown");
			BrowserActions.javascriptClick(express, driver, "SortBy dropdown");
		}
	}

	public String getShiipingCostBelowTotalCost() throws Exception {
		String dataToBeReturned = BrowserActions.getText(driver, shiipingCostBelowtotalCost,
				"shipping method in multi shipping");
		return dataToBeReturned;
	}

	public String getShiipingCostInBillingPage() throws Exception {
		String dataToBeReturned = BrowserActions.getText(driver, TypeShipping, "shipping method in multi shipping");
		return dataToBeReturned;
	}

	public String getShippingRateInOrderConfirmationPage() throws Exception {
		Utils.waitForElement(driver, getShippingRate);
		BrowserActions.scrollToViewElement(getShippingRate, driver);
		String rate = BrowserActions.getText(driver, getShippingRate, "get Shipping Rate");
		return rate;
	}

	/**
	 * To get the text from address one text field
	 * 
	 * @return
	 * @throws Exception
	 *             Last modified By - Jayanthi & Date modified - 1/3/2017
	 */
	public String GetTextFromAddressOneTextField() throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtAddress_1InShipping, "value", "Address1");

	}

	////

	public static final String START_SHIPPINGMETHOD = "div.checkoutmultishipping-methods > table";
	public static final String END_SHIPPINGMETHOD = ") > tbody > tr.cart-row.shippingInfo-row > td > div > ul li";

	/**
	 * To get Merchandise Value in Order Summary
	 * 
	 * @return Float Merchandise value
	 * @throws Exception
	 */

	public float getMerchandiseTotalValue() throws Exception {
		String mechandisevalue = BrowserActions.getText(driver, lblMerchandiseTotalValue, "Merchandise Total Value")
				.trim().replace("$", "");
		return Float.parseFloat(mechandisevalue);
	}

	/**
	 * To get Associate Discount Value in Order Summary
	 * 
	 * @return Float Associate Discount value
	 * @throws Exception
	 */

	public float getAssociateDiscountValue() throws Exception {
		String associateValue = BrowserActions.getText(driver, lblAssociateDiscountValue, "Merchandise Total Value")
				.trim().replace("$", "");
		return Float.parseFloat(associateValue);
	}

	/**
	 * To select shipping method in shippingmethod page
	 * 
	 * @param shippingmethod
	 *            e.g Ground,Free,2DAY,OVNT
	 * @return Float Associate Discount value
	 * @throws Exception
	 */
	public void selectShippingMethodByValue(String shippingmehod) throws Exception {

		List<WebElement> lstNoofItems = driver.findElements(By.cssSelector(START_SHIPPINGMETHOD));

		if (lstNoofItems.size() != 0) {

			for (int i = 0; i < lstNoofItems.size(); i++) {

				List<WebElement> lstshippingmethods = lstNoofItems.get(i)
						.findElements(By.cssSelector("tbody > tr.cart-row.shippingInfo-row > td > div > ul >li"));
				BrowserActions.clickOnElement(
						lstNoofItems.get(i)
								.findElement(By.cssSelector("tbody > tr.cart-row.shippingInfo-row > td > div")),
						driver, "Shipping methods");
				for (int j = 0; j < lstshippingmethods.size(); j++) {
					WebElement lstvalue = lstshippingmethods.get(i);
					if (lstvalue.getText().equalsIgnoreCase(shippingmehod)) {
						lstvalue.click();
						break;
					}

				}
			}
		}
	}

	public String getSelectedShippingmethodvalueByindex(int index) {
		return lstShippingMethods.get(index).getText().toString();
	}

	/**
	 * clickOnBelkLogo
	 * 
	 * @throws Exception
	 */
	public void clickOnBelkLogo() throws Exception {
		BrowserActions.clickOnElement(belkLogo, driver, "Belk Logo");
	}

	/**
	 * To verify system is in shipping page
	 * 
	 * @return boolean
	 * @throws Exception
	 *             Last Modified By Jayanthi.m --- 19/01/2017
	 */
	public boolean verifyShippingFieldActive() throws Exception {
		boolean status = false;
		if (txtheadingShippingOrder.findElement(By.xpath("../..")).getAttribute("class").contains("active")) {
			status = true;
		}
		return status;
	}

	public LinkedHashMap<String, String> getPaymentDetailsInFields() throws Exception {
		LinkedHashMap<String, String> paymentDetails = new LinkedHashMap<String, String>();

		String cardType = new String();
		String nameOnCard = new String();
		String cardNumber = new String();
		String expiryDate = new String();

		cardType = BrowserActions.getText(driver, btnCardType, "Card Type");
		nameOnCard = driver.findElement(By.cssSelector("#dwfrm_billing_paymentMethods_creditCard_owner"))
				.getAttribute("value").trim();
		cardNumber = driver.findElement(By.cssSelector("input[id*='dwfrm_billing_paymentMethods_creditCard_number']"))
				.getAttribute("value").trim();
		if (!cardType.equals("Belk Rewards Credit Card"))
			expiryDate = BrowserActions.getText(driver, btnCardExpMonth, "Card Type") + "|"
					+ BrowserActions.getText(driver, btnCardExpYear, "Card Type");

		if (cardType.equals("Belk Rewards Credit Card"))
			cardType = "Belk Reward Credit Card";
		else if (cardType.equals("American Express"))
			cardType = "Amex";

		cardNumber = cardNumber.substring(cardNumber.length() - 4);

		paymentDetails.put("CardType", cardType);
		paymentDetails.put("NameOnCard", nameOnCard);
		paymentDetails.put("CardNumber", cardNumber);
		if (!cardType.equals("Belk Rewards Credit Card"))
			paymentDetails.put("CardExpiry", expiryDate);

		return paymentDetails;

	}

	@FindBy(css = ".details div:not([class])")
	List<WebElement> lblGiftCardNumberFromSection;

	@FindBy(css = ".details div:not([class])")
	WebElement lblSingleGiftCardNumberFromSection;

	public LinkedHashMap<String, LinkedHashMap<String, String>> getPaymentDetailsInPlaceOrder(CheckoutPage checkoutPage)
			throws Exception {
		LinkedHashMap<String, LinkedHashMap<String, String>> paymentDetail = new LinkedHashMap<String, LinkedHashMap<String, String>>();

		// if(checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("lblAmount1"),
		// checkoutPage)){
		// if(Utils.waitForElement(driver, lblAmount1.get(0), 3)){
		if (Utils.waitForElement(driver, lblSingleGiftCardNumberFromSection, 3)) {
			for (int i = 0; i < lblAmount1.size(); i++) {
				LinkedHashMap<String, String> giftCardDetails = new LinkedHashMap<String, String>();
				String cardNumber = lblGiftCardNumberFromSection.get(i).getText().replace("Gift Card :", "")
						.replace("*", "").trim();
				giftCardDetails.put("GiftCardNumber", cardNumber);
				giftCardDetails.put("Amount", checkoutPage.getGiftCardAmountInSection(i));
				paymentDetail.put("GiftCard" + (i + 1), giftCardDetails);
			}
		}

		// if(checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("txtBRDNumberInSection"),
		// checkoutPage)){
		// if(Utils.waitForElement(driver, txtBRDNumberInSection.get(0), 3)){
		if (Utils.waitForElement(driver, singleTxtBRDNumberInSection, 3)) {
			for (int i = 0; i < txtBRDNumberInSection.size(); i++) {
				LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
				String cardNumber = checkoutPage.getBRDNumberFromSection(i);
				cardDetails.put("BRDNumber", cardNumber);
				cardDetails.put("Amount", checkoutPage.getBRDAmountInSection(i));
				paymentDetail.put("BRD" + (i + 1), cardDetails);
			}
		}

		// if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtCardNumber"),
		// checkoutPage)){
		if (Utils.waitForElement(driver, txtCardNumber, 3)) {
			LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
			String cardType = new String();
			String nameOnCard = new String();
			String cardNumber = new String();
			String amount = new String();

			cardType = driver.findElement(By.cssSelector(".cc-type")).getText().trim();
			nameOnCard = driver.findElement(By.cssSelector(".cc-owner")).getText().trim();
			cardNumber = driver.findElement(By.cssSelector(".cc-number")).getText().replace("*", "").trim();
			amount = driver
					.findElement(By.xpath("//div[@class='cc-type']/following-sibling::div[@class='credit-cart-name']"))
					.findElement(By.cssSelector("span")).getText().replace("$", "").trim();

			cardDetails.put("CardType", cardType);
			cardDetails.put("NameOnCard", nameOnCard);
			cardDetails.put("CardNumber", cardNumber);
			cardDetails.put("Amount", amount);

			paymentDetail.put("CreditCard", cardDetails);
		}
		return paymentDetail;
	}

	public void selectingAddressInMultiShipDropdown() throws Exception {

		List<WebElement> lstProduct = driver.findElements(By.cssSelector(".checkoutmultishipping>table>tbody>tr"));

		for (int i = 1; i <= lstProduct.size(); i = i + 2) {

			WebElement addressdrpdown = driver.findElement(By.cssSelector(
					".checkoutmultishipping>table>tbody>tr:nth-child(" + i + ")>td:nth-child(4)>div:nth-child(1)"));
			BrowserActions.clickOnElement(addressdrpdown, driver, "Clicked Address dropdown");
			Utils.waitForPageLoad(driver);
			WebElement addressList = driver
					.findElement(By.cssSelector(".checkoutmultishipping>table>tbody>tr:nth-child(" + i
							+ ")>td:nth-child(4)>div:nth-child(1)>ul>li:nth-child(2)"));

			BrowserActions.clickOnElement(addressList, driver, "Selected Address");
		}
	}

	public void clearGiftcardPIN() {
		driver.findElement(By.cssSelector(txtGiftPinNumber)).clear();

	}

	public void clickonRemoveGiftcard() throws Exception {
		Utils.waitForElement(driver, lnkRemoveGiftCard);
		BrowserActions.clickOnElement(lnkRemoveGiftCard, driver, "Remove Gift card ");
	}

	public PdpPage clickonProductthumbnailByIndex(int index) throws Exception {
		BrowserActions.clickOnElement(lstProductName.get(index), driver, "product thumbnail");
		return new PdpPage(driver).get();
	}

	public String getselectedShippingmethod() throws Exception {
		return BrowserActions.getText(driver, txtShippingMethods, "Selected Shipping Method");
	}

	/**
	 * To Return card number with Masked(not last 4 digits)
	 * 
	 * @param inputcardnumber
	 * @return maskedcardnumber
	 */
	public String getmaskedCardnumber(String inputcardnumber) {

		int cardlenth = inputcardnumber.length() - 1;
		String mask = inputcardnumber.substring(0, cardlenth - 3);
		String last4digit = inputcardnumber.substring(cardlenth - 3, cardlenth + 1);
		String masked = mask.replaceAll("[0-9]{1}", "*");
		String maskedcardnumber = masked + last4digit;
		return maskedcardnumber;
	}

	/**
	 * To read Giftcard Amount in Billing Page
	 * 
	 * @return Giftcard amount with $
	 */
	public String getGiftcardAmountInBillingPage() {
		return lblAmount.getText().trim();
	}

	/**
	 * To get Payment Details in Billing Page
	 * 
	 * @param customerDetails
	 * @param fieldvalue
	 *            CardName,CardNumber,CVNno,(CardExpYear,CardExpMonth) for Non
	 *            Belk Reward Cards
	 * @return return
	 * 
	 */
	public String getCardDetails(String customerDetails, String fieldvalue) throws Exception {
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
		String cardExpireMonth = "";
		String cardExpireYear = "";
		String cardCVN = "";

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardType = cutomercardDetails.split("\\|")[0];
		String cardOwner = cutomercardDetails.split("\\|")[1];
		String cardNo = cutomercardDetails.split("\\|")[2];

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardExpireMonth = cutomercardDetails.split("\\|")[3];
			cardExpireYear = cutomercardDetails.split("\\|")[4];
			cardCVN = cutomercardDetails.split("\\|")[5];
		}
		if (cardType.trim().equals("Belk Rewards Credit Card")) {
			cardCVN = cutomercardDetails.split("\\|")[3];
		}

		cardDetails.put("CardName", cardOwner);
		cardDetails.put("CardNumber", cardNo);

		if (!cardType.trim().equals("Belk Rewards Credit Card")) {
			cardDetails.put(cardExpireMonth, "CardExpMonth");
			cardDetails.put(cardExpireYear, "CardExpYear");
			cardDetails.put(cardCVN, "CVNno");
		} else {
			cardDetails.put(cardCVN, "CVNno");
		}
		System.out.println(cardDetails.get(fieldvalue).toString().trim());
		return cardDetails.get(fieldvalue).toString().trim();
	}

	public float getCalculateSubTotal() throws NumberFormatException, Exception {
		float subTotal = 0;
		String price = new String();
		float coupon = 0;
		if (lblPriceSectionInProduct.get(0).getText().contains("Clearance")) {
			price = lblPriceSectionInProduct.get(0).findElement(By.cssSelector(".now-price")).getText().trim()
					.split("\\$")[1].trim();
		} else if (lblPriceSectionInProduct.get(0).getText().contains("Now")) {
			price = lblPriceSectionInProduct.get(0).findElement(By.cssSelector(".now-price")).getText().trim()
					.split("\\$")[1].trim();
		} else if (Utils.waitForElement(driver,
				lblPriceSectionInProduct.get(0).findElement(By.cssSelector(".standard-price")))) {
			price = lblPriceSectionInProduct.get(0).findElement(By.cssSelector(".standard-price")).getText().trim()
					.replace("$", "");
		}

		String quantity = driver.findElement(By.cssSelector(".sku-qty .qty-value")).getText();

		if (lblPriceSectionInProduct.get(0).getText().contains("Coupon")) {
			coupon = Float.parseFloat(BrowserActions.getText(driver, txtCoupon, "Coupon value").split("\\$")[1]);
		}

		subTotal = Float.parseFloat(price) * Integer.parseInt(quantity) - coupon;

		return subTotal;
	}

	/**
	 * To select shipping method in shippingmethod page
	 * 
	 * @param shippingmethod
	 *            e.g Ground,Free,2DAY,OVNT
	 * @return Float Associate Discount value
	 * @throws Exception
	 */

	/*
	 * List<WebElement> lstNoofItems = driver.findElements(By
	 * .cssSelector(START_SHIPPINGMETHOD)); ||||||| .r1705 List<WebElement>
	 * lstNoofItems = driver.findElements(By
	 * .cssSelector(START_SHIPPINGMETHOD)); ======= List<WebElement>
	 * lstNoofItems = driver.findElements(By.cssSelector(START_SHIPPINGMETHOD));
	 * >>>>>>> .r1708
	 * 
	 * @FindBy(css = "table tr.cart-row.shippingInfo-row td>div>div")
	 * List<WebElement> lstshippingmethods;
	 */

	public String getCardNumber() {
		return lblCardNumberInBRD.getText().split(":")[1].replace("***********", "").toString();
	}

	public LinkedHashMap<String, String> getMultiShippingAddressByIndex(int index) throws Exception {

		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

		BrowserActions.scrollToView(selectAddressInShipToMultipleAddress.get(index), driver);
		String shippingCustomerName = selectAddressInShipToMultipleAddress.get(index).getText().split("\\,")[0];
		String shippingCustomerAddress = selectAddressInShipToMultipleAddress.get(index).getText().split("\\,")[1];
		String shippingCustomerCity = selectAddressInShipToMultipleAddress.get(index).getText().split("\\,")[2];
		String shippingCustomerState = selectAddressInShipToMultipleAddress.get(index).getText().split("\\,")[3];
		String shippingCustomerZipCode = selectAddressInShipToMultipleAddress.get(index).getText().split("\\,")[4];

		shippingDetails.put("FirstName", shippingCustomerName.split(" ")[0].trim());
		shippingDetails.put("LastName", shippingCustomerName.split(" ")[1].trim());
		shippingDetails.put("Address", shippingCustomerAddress);

		shippingDetails.put("City", shippingCustomerCity);
		shippingDetails.put("State", shippingCustomerState);
		shippingDetails.put("Zipcode", shippingCustomerZipCode);

		return shippingDetails;
	}

	public boolean verifyAddressDetails(LinkedHashMap<String, String> addressDetailsBefore,
			LinkedHashMap<String, String> addressDetailsAfter) {
		boolean status = true;

		Set<String> setBefore = addressDetailsBefore.keySet();
		Set<String> setAfter = addressDetailsAfter.keySet();

		Iterator<String> iteratorBefore = setBefore.iterator();
		Iterator<String> iteratorAfter = setAfter.iterator();

		while (iteratorBefore.hasNext()) {
			if (!addressDetailsBefore.get(iteratorBefore.next())
					.equalsIgnoreCase(addressDetailsAfter.get(iteratorAfter.next()))) {
				status = false;
				break;
			}

		}

		return status;
	}

	public void selectShipingAddressByValue(int iIndex) {

		Select selct = new Select(wAddress);
		selct.selectByIndex(iIndex);

	}

	public String GetShippingAddressInPopUp() throws Exception {
		String add = BrowserActions.getText(driver, suggestedAddress, "Suggested Address in PopUp ");
		String updateAdd = add.trim().replace("Farmington CT ", "");
		return updateAdd;

	}

	/**
	 * To click on the edit by section name
	 * 
	 * @param sectionName
	 *            - Ex. ordersummary, shippingaddress, billingaddress,
	 *            paymentmethod
	 * @throws Exception
	 */
	public boolean verifyEditLinkBySectionName(String sectionName) throws Exception {
		WebElement element = null;
		if (sectionName == "ordersummary") {
			BrowserActions.scrollToView(editOrderSummary.get(0), driver);
			element = editOrderSummary.get(0);
		} else if (sectionName == "shippingaddress") {
			BrowserActions.scrollToView(editOrderSummary.get(1), driver);
			element = editOrderSummary.get(1);
		} else if (sectionName == "billingaddress") {
			BrowserActions.scrollToView(editOrderSummary.get(2), driver);
			element = editOrderSummary.get(2);
		} else if (sectionName == "paymentmethod") {
			BrowserActions.scrollToView(editOrderSummary.get(3), driver);
			element = editOrderSummary.get(3);
		}
		return Utils.waitForElement(driver, element);
	}

	/**
	 * getShippingRegistryAddress
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getRegistryShippingAddress() throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.scrollToView(driver.findElement(By.cssSelector(registryAddress)), driver);
		String registryShipment = BrowserActions.getText(driver, registryAddress, "registry address");
		String shippingMethod = BrowserActions.getText(driver, lblShippmentMethod, "Shipping name");
		shippingDetails.put("registryShipment", registryShipment);
		shippingDetails.put("shippingMethod", shippingMethod);
		return shippingDetails;
	}

	public String GetBRDErrorMessage() throws Exception {
		String add = BrowserActions.getText(driver, brdErrorMessage, "BRD Error Message");
		return add;

	}

	public LinkedHashMap<String, String> fillingBelkGiftCardDetails_test(String giftCardNumber, String pinNumber)
			throws Exception {
		LinkedHashMap<String, String> giftCardDetails = new LinkedHashMap<String, String>();
		giftCardDetails.put("type_giftCardNumber_" + giftCardNumber, txtGiftCardNumber);
		giftCardDetails.put("type_pinNumber_" + pinNumber, txtGiftPinNumber);
		BillingPageUtils.enterBillingDetails(giftCardDetails, driver);
		// Log.event("Filled Belk Gift Card Details",
		// StopWatch.elapsedTime(startTime));
		BrowserActions.nap(2);
		return giftCardDetails;
	}

	@FindBy(css = "li.remove-pi>a")
	List<WebElement> listLnkRemoveGiftCard;

	public void clickonRemoveGiftcard(int index) throws Exception {
		Utils.waitForElement(driver, listLnkRemoveGiftCard.get(index));
		BrowserActions.clickOnElement(listLnkRemoveGiftCard.get(index), driver, "Remove Gift card ");
	}

	/**
	 * To click on ApplyBelkGiftLink in Express checkout
	 * 
	 * @Clicked on ApplyBelkGiftLink
	 * @throws Exception
	 */

	public void clickOnApplyBelkGiftLink() throws Exception {
		BrowserActions.clickOnElement(lnkApplyGiftCardInExpressCheckout, driver, "Clicked on Apply Gift Card Link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on ApplyBelkRewardLink in Express checkout
	 * 
	 * @Clicked on ApplyBelkRewardLink
	 * @throws Exception
	 */
	public void clickOnApplyBelkRewardLink() throws Exception {
		BrowserActions.clickOnElement(lnkApplyRewardCardInExpressCheckOut, driver, "Clicked on Apply Reward Card Link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on the shipping tab
	 * 
	 * @throws Exception
	 */
	public void clickOnShipping() throws Exception {
		BrowserActions.clickOnElement(tabShipping, driver, "shipping tab");

	}

	public void clickCheckBoxInShipping() throws Exception {

		BrowserActions.selectRadioOrCheckbox(Checkbox, "YES");
	}

	@FindBy(css = ".giftcardapplied.success")
	public WebElement lblGiftcardSuccessMsg;

	@FindBy(css = ".gift-cert-used.form-indent.info")
	public WebElement msgPaymentNotNeed;

	@FindBy(css = "div[class='form-row savedcardlist-row'] div[class='selected-option']")
	WebElement drpDownSavedPaymentMethods;

	@FindBy(css = "div[class='custom-select current_item']  ul li:not([class*='selected'])")
	List<WebElement> lstSavedPaymentMethods;

	/**
	 * To Get GiftCard Applied message
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getGiftCardAppliedMessage() throws Exception {
		String giftCardBalance;
		Utils.waitForElement(driver, lblGiftcardSuccessMsg);
		BrowserActions.scrollToView(lblGiftcardSuccessMsg, driver);
		giftCardBalance = BrowserActions.getText(driver, lblGiftcardSuccessMsg, "GiftCard balance");

		return giftCardBalance;
	}

	public String getMsgPaymentNotNeed() throws Exception {
		Utils.waitForElement(driver, msgPaymentNotNeed);
		BrowserActions.scrollToView(msgPaymentNotNeed, driver);
		String paymentDetails = BrowserActions.getText(driver, msgPaymentNotNeed, "Payment Details");
		return paymentDetails;
	}

	public String selectSavedPaymentMethodByIndex(int index) throws Exception {
		String savedAddress = null;
		BrowserActions.clickOnElement(drpDownSavedPaymentMethods, driver, "SortBy dropdown");
		BrowserActions.clickOnElement(lstSavedPaymentMethods.get(index), driver, "Saved Address dropdown list");
		savedAddress = driver
				.findElement(By.cssSelector(
						"div[class='form-row savedcardlist-row'] div[class='field-wrapper'] div[class='custom-select'] div[class*='selected']"))
				.getText();
		return savedAddress;
	}

	public String getPaymentDetailsInBillingPage() throws Exception {
		Utils.waitForPageLoad(driver);
		String paymentType = BrowserActions.getText(driver, btnCardType, "Payment Type");
		return paymentType;
	}

	/**
	 * To select the saved address randomly
	 * 
	 * @return shippingDetails
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> selectSavedAddressRandomly() throws Exception {

		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.clickOnElement(drpSelectedOptions.get(0), driver, "Saved Address");
		for (int i = 0; i < drpSavedAddress.findElements(By.cssSelector("options")).size(); i++) {
			if (!BrowserActions
					.getText(driver, drpSavedAddress.findElements(By.cssSelector("options")).get(i), "Saved address")
					.equals("selected")) {
				BrowserActions.clickOnElement(drpSavedAddress.findElements(By.cssSelector("options")).get(i), driver,
						"Select saved address");
				break;
			}
			Utils.waitForPageLoad(driver);

		}
		return shippingDetails;

	}

	/**
	 * To get SurchargeApplied Message in PLaceOrder Page return
	 * SurchargeAppliedMessage
	 * 
	 * @throws Exception
	 */
	public String getTextFromSurchargeAppliedMsg() throws Exception {
		String SurchargeAppliedMessage = BrowserActions.getText(driver, txtSurchargeAppliedMsg,
				"Surcharge Applied Message");
		return SurchargeAppliedMessage;

	} // TODO Auto-generated method stub

	/**
	 * To get the Instore Address from Shipping method page
	 * 
	 * @key ShippingMethod,StoreName,StoreAddress,PhoneNumber,State
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getInStoreAddress() throws Exception {
		LinkedHashMap<String, String> addressdetails = new LinkedHashMap<String, String>();

		String shippingmethod = BrowserActions.getText(driver, txtInStorePickupFree, "ShippingMethod");
		addressdetails.put("ShippingMethod", shippingmethod);
		String storeName = BrowserActions.getText(driver, txtInStoreName, "Store Name");
		addressdetails.put("StoreName", storeName);
		String storeaddress = BrowserActions.getText(driver, txtInStorePickupAddress, "Store Address");
		addressdetails.put("StoreAddress", storeaddress);
		String phoneNumber = BrowserActions.getText(driver, txtInStorePhone, "Phone Number");
		addressdetails.put("PhoneNumber", phoneNumber);
		String storeState = BrowserActions.getText(driver, txtInStoreState, "State");
		addressdetails.put("State", storeState);
		return addressdetails;
	}

	/**
	 * To get BelkRewardDollar Amount in the OrderSummary Panel return
	 * BelkRewardDollarAmount
	 * 
	 * @throws Exception
	 */

	public String getRewardDollarPrice() throws Exception {
		String BelkRewardDollarAmount = null;
		if (pricingDetails.getText().contains("Belk Reward Dollars")) {
			BelkRewardDollarAmount = (BrowserActions
					.getText(driver, txtBelkRewardDollarAmount, "Belk Reward Dollar Discount amount")
					.replace("-$", ""));
		}
		return BelkRewardDollarAmount;
	}

	public String[] getdiscountvalueFromOrderSummary() {
		String[] lstorderlabel = new String[lstorderdiscount.size()];
		WebElement element = lstorderdiscount.get(0);
		if (Utils.waitForElement(driver, element)) {
			for (int i = 1; i <= lstorderdiscount.size(); i++) {
				Log.message("discount text" + lstorderdiscount.get(i - 1).getText());
				lstorderlabel[i - 1] = lstorderdiscount.get(i - 1).getText();
			}
		} else {
			lstorderlabel[0] = "Discount coupons are not applied";
		}
		return lstorderlabel;

	}

	/**
	 * to get sales tax value
	 * 
	 * @return
	 * @throws Exception
	 */
	public String GetSalesTaxValue() throws Exception {
		return BrowserActions.getText(driver, txtEstimatedSalesTaxCost, "Sales Tax value");
	}

	/**
	 * To Read the Employee Discount message in Order COnfirmation Page
	 * 
	 * @return
	 */
	public String[] getdiscountmessage() {
		String[] employeedismessage = new String[txtEmployeediscount.size()];
		WebElement element = txtEmployeediscount.get(0);
		if (Utils.waitForElement(driver, element)) {
			for (int i = 1; i <= txtEmployeediscount.size(); i++) {
				employeedismessage[i - 1] = txtEmployeediscount.get(i - 1).getText();
			}
		} else {
			employeedismessage[0] = "Discount Message is not displayed in Place Order Page";
		}
		return employeedismessage;
	}

	@FindBy(css = ".instore-shipment-address>div:nth-child(1)")
	WebElement lblBopisShippingName;

	@FindBy(css = ".instore-shipment-address>div:nth-child(2)")
	WebElement lblBopisCustomerAddress;

	@FindBy(css = ".instore-shipment-address>div:nth-child(3)")
	WebElement lblBopisCustomerCity;

	@FindBy(css = ".instore-shipment-method")
	WebElement lblBopisShipType;

	/**
	 * getBopisShippingAddress - e2e
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBopisShippingAddress() throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.scrollToView(lblBopisShippingName, driver);
		shippingDetails.put("ShopName", lblBopisShippingName.getText().split(" ")[0]);
		shippingDetails.put("Address", lblBopisCustomerAddress.getText());
		shippingDetails.put("City", lblBopisCustomerCity.getText().split(",")[0]);
		shippingDetails.put("State", lblBopisCustomerCity.getText().split(",")[1].trim().split(" ")[0].trim());
		shippingDetails.put("Zipcode", lblBopisCustomerCity.getText().split(",")[1].trim().split(" ")[1].trim());
		shippingDetails.put("ShippingMethod", lblBopisShipType.getText().split("\\:")[1].trim().split("\\$")[0].trim());
		return shippingDetails;
	}

	/**
	 * getBopisShippingAddressInBillingPage - e2e
	 * 
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getBopisShippingAddressInBillingPage() throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		BrowserActions.scrollToView(driver.findElement(By.cssSelector(lblShippingName)), driver);
		String shippingCustomerName = BrowserActions.getText(driver, lblShippingName, "Shipping name");
		String shippingCustomerAddress1 = BrowserActions.getText(driver, lblShippingAddress1, "Shipping Address1");
		String shippingCustomerCity = BrowserActions.getText(driver, lblShippingCity, "Shipping city");
		String shippingType = BrowserActions.getText(driver, lblShippmentMethod, "Shipping Type");

		shippingDetails.put("ShopName", shippingCustomerName);
		shippingDetails.put("Address", shippingCustomerAddress1);
		shippingDetails.put("City", shippingCustomerCity.split(",")[0]);
		shippingDetails.put("State", shippingCustomerCity.split(",")[1].trim().split(" ")[0]);
		shippingDetails.put("Zipcode", shippingCustomerCity.split(",")[1].trim().split(" ")[1]);
		shippingDetails.put("ShippingMethod", shippingType);
		return shippingDetails;
	}

	/**
	 * to get Gift Box value
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGiftBoxValue() throws Exception {
		return BrowserActions.getText(driver, txtGiftBoxValue, "GiftBox value").replace("$", "");
	}

	public String getProductName() throws Exception {
		BrowserActions.scrollToView(productName, driver);
		String name = BrowserActions.getText(driver, productName, "Product name");
		return name;

	}

	/**
	 * To read the Order Level error Message
	 * 
	 * @return errorMessage
	 * @throws Exception
	 */
	public String getOrderErrorMessage() throws Exception {
		Utils.waitForElement(driver, errMsgCheckoutSummary);
		String errorMessage = BrowserActions.getText(driver, errMsgCheckoutSummary, "Order Level Error Message");
		return errorMessage;
	}

	/**
	 * to get text from error msg under belk reward dollars
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getErrorMsgOfBelkRewardDollars() throws Exception {
		return BrowserActions.getText(driver, msgErrorBelkReward, "Getting Error msg under belk reward dollars.");
	}

	/**
	 * to get text from error msg under invalid belk reward dollars
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getErrorMsgOfInvalidBelkRewardDollars() throws Exception {
		return BrowserActions.getText(driver, msgInvalidBelkRewardDollar,
				"Getting Error msg under belk reward dollars for invalid belk reward.");
	}

	public boolean verifyShippingAddressOrder() {
		String shipaddr1 = shippingAddresses.get(0).findElement(By.cssSelector(" h3")).getText();
		String shipaddr2 = shippingAddresses.get(1).findElement(By.cssSelector(" h3")).getText();
		String shipaddr3 = shippingAddresses.get(2).findElement(By.cssSelector(" span.gift-icon")).getText();
		if (shipaddr1.contains("In-Store Pickup") && shipaddr2.contains("Shipping Address #2")
				&& shipaddr3.contains("Registry Address"))
			return true;
		return false;

	}

	/**
	 * to get address from Instore Pickup
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getInStorePickupAddress() throws Exception {
		return BrowserActions.getText(driver, addrInStorePickUp, "Get address from Instore pickup");
	}

	/**
	 * to get method from Instore Pickup
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getInStorePickupMethod() throws Exception {
		return BrowserActions.getText(driver, shipMethdInStorePickUp, "Get shipMethod from Instore pickup");
	}

	/**
	 * to get text from r msg that Belk reward dollors applied successfully
	 * 
	 * @return
	 * @throws Exception
	 */

	public String getMsgBelkRewardDollarsApplied() throws Exception {
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, msgAppliedBelkRewardDollars,
				"Getting  msg that belk reward dollars applied successfully");
	}

	
	/**
	 * to Clickon RemoveLink for applied Belk Reward Dollors
	 * 
	 * @throws Exception
	 */
	public void clickonRemoveBelkRewardDollar() throws Exception {
		BrowserActions.scrollToView(btnRemoveInBRD, driver);
		BrowserActions.clickOnElement(btnRemoveInBRD, driver, " remove button");
		Utils.waitForPageLoad(driver);
	}
	
	

}// checkoutPage