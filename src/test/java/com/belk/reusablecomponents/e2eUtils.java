package com.belk.reusablecomponents;

import java.math.BigDecimal;
import java.text.Collator;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.belk.pages.CheckoutPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.ElementLayer;
import com.belk.pages.HomePage;
import com.belk.pages.PdpPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.headers.Headers;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.TestDataWritter;
import com.belk.support.Utils;

/**
 * Re-Usable methods of End-To-End Script Functionality for Retail Sites
 */
public class e2eUtils {
	public static ElementLayer elementLayer;
	private static String workbookName = "testdata\\data\\e2e_TestData.xls";
	private static String BRD_Sheet_Name = "BRD";
	private static String GC_Sheet_Name = "GiftCard";
	
	public static void doVerifications(
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP,				//1
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage,		//2
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping,		//3
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling,		//4
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder,	//5	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder,		//6	
			LinkedHashMap<String, String> costDetailsInCart,							//7
			LinkedHashMap<String, String> costDetailsInCHKShipping,						//8
			LinkedHashMap<String, String> costDetailsInCHKBilling,						//9	
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder,					//10
			LinkedHashMap<String, String> costDetailsInOrderConfirmation,				//11
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage,				//12
			LinkedHashMap<String, String> shippingDetailsInCHKBilling,					//13
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder,				//14
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation,				//15
			LinkedHashMap<String, String> billingAddressInCheckoutPage,					//16
			LinkedHashMap<String, String> billingAddressInPlaceOrder,					//17
			LinkedHashMap<String, String> billingAddrInOrderConfirmation,				//18
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInBilling,			//19
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInPlaceOrder,			//20
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInOrderConfirmation,	//21
			WebDriver driver)throws Exception{														//22
		//===========Sorting===================//
		productDetailsInPDP = Utils.sortLinkedListProduct(productDetailsInPDP);
		pdtDetailsInShoppingBagPage = Utils.sortLinkedListProduct(pdtDetailsInShoppingBagPage);
		pdtDetailsInCheckoutShipping = Utils.sortLinkedListProduct(pdtDetailsInCheckoutShipping);
		pdtDetailsInCheckoutBilling = Utils.sortLinkedListProduct(pdtDetailsInCheckoutBilling);
		pdtDetailsInCheckoutPlaceOrder = Utils.sortLinkedListProduct(pdtDetailsInCheckoutPlaceOrder);
		pdtDetailsInCheckoutOrder = Utils.sortLinkedListProduct(pdtDetailsInCheckoutOrder);
		List<String> keySet = new ArrayList<String>(ccCardDetailsInBilling.keySet());
		int keySetSize = keySet.size();
		if(keySet.toString().contains("GiftCard")){
			@SuppressWarnings("unchecked")
			HashMap<String, String> giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Balance");
			for(int p=0; p < keySetSize; p++){
				if(keySet.get(p).contains("GiftCard")){
					String balance = giftCardData.get(ccCardDetailsInBilling.get(keySet.get(p)).get("GiftCardNumber"));
					balance = Float.toString((Float.parseFloat(balance) - Float.parseFloat(ccCardDetailsInBilling.get(keySet.get(p)).get("Amount"))));
					if(Float.parseFloat(balance) == 0)
						TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", ccCardDetailsInBilling.get(keySet.get(p)).get("GiftCardNumber"), "Balance", "No Balance");
					else
						TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", ccCardDetailsInBilling.get(keySet.get(p)).get("GiftCardNumber"), "Balance", balance);
				}
			}
		}
		//=========Shopping Bag Verification==============//
		Log.message("<h2><b><u>Verification - Shopping Cart Page</u></b></h2>");
		Log.message("<br>");
		String[] ignoreUPC = {"Upc"};
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Cart' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Shopping Bag", productDetailsInPDP, pdtDetailsInShoppingBagPage, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//-----------------------------Print Order Summary in Shopping Cart---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Table</u></b>");
		Log.message("<b>Find the Order Summary details in Shopping Bag Page!</b>");
		Log.message("<br>");
		printTableHashMap("Order Summary", costDetailsInCart);
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCart), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//=========Shipping Page Verification===============//
		Log.message("<h2><b><u>Verification - Shipping Tab - Checkout Page</u></b></h2>");
		//---------------------------Product Details(PDP vs Shipping Tab)-----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Shipping)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Shipping)", productDetailsInPDP, pdtDetailsInCheckoutShipping, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Cost Details(Cart vs Shipping Tab)-----------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Shopping Bag and Checkout(Shipping) Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "CART", "Checkout(Shipping)", costDetailsInCart, costDetailsInCHKShipping), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKShipping), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//==========Billing Page Verification=================//
		Log.message("<h2><b><u>Verification - Billing Tab - Checkout Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Billing Tab)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Billing)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Billing)", productDetailsInPDP, pdtDetailsInCheckoutBilling, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Shipping Addr(Shipping Tab vs Billing Tab)-----------------------------//
		//LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
		Log.message("<br>");
		String[] ignoreAddress2 = {"Address2"};
		Log.message("<b>Expected Result : </b>Shipping Address in Checkout page(Shipping) and Checkout page(Billing) should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Shipping Address - Billing Tab", "Checkout(Shipping)", "Checkout(Billing)", shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Cost Details(Shipping vs Billing Tab)----------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Shipping) page and Checkout(Billing) Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Shipping)", "Checkout(Billing)", costDetailsInCHKShipping, costDetailsInCHKBilling), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKBilling), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//============Place Order Page Verification==============//
		Log.message("<h2><b><u>Verification - Place Order Tab - Checkout Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Place Order Tab)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Place Order)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Place Order)", productDetailsInPDP, pdtDetailsInCheckoutPlaceOrder, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//----------------------------Shipping Addr(Shipping Tab vs Place Order)----------------------------//
		//LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Shipping Address in Shipping Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Shipping Address - Place Order Tab", "Checkout(Shipping)", "Checkout(Place Order)", shippingDetailsInCheckoutPage, shippingDetailsInCHKPlaceOrder, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Billing Addr(Billing Tab vs Place Order)----------------------------//
		//LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
		Log.message("<br>");
		Log.message("<b><u>Billing Address Verification</u></b>");
		Log.message("<b>Expected Result : </b>Billing Address in Billing Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Billing Address - Place Order Tab", "Checkout(Billing)", "Checkout(Place Order)", billingAddressInCheckoutPage, billingAddressInPlaceOrder, ignoreAddress2), 
				"<b>Actual Result : </b>Billing Address details are same in Both pages", 
				"<b>Actual Result : </b>Billing Address detials are not same in pages");
		//----------------------------Payment Methods(Billing Tab vs Place Order)----------------------------//
		Log.message("<br>");
		String[] ignorePay = {"CardExpiry"};
		Log.message("<b><u>Payment Method Verification</u></b>");
		Log.message("<b>Expected Result : </b>Payment Method in Billing Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(sortCompartPrintPaymentDetails("Checkout(Billing)","Checkout(Place Order)",ccCardDetailsInBilling, ccCardDetailsInPlaceOrder,ignorePay), 
				"<b>Actual Result : </b>Payment Method are same in Both pages", 
				"<b>Actual Result : </b>Payment Method are not same in pages");
		//-----------------------------Cost Details(Billing vs Place Order Tab)---------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Shipping) page and Checkout(Place Order) page<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Billing)", "Checkout(Place Order)", costDetailsInCHKBilling, costDetailsInCHKPlaceOrder), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKPlaceOrder), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//===========Order Confirmation Page Verification=============//
		Log.message("<h2><b><u>Verification - Order Confirmation Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Order Confirmation)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Order Confirmation' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Place Order)", productDetailsInPDP, pdtDetailsInCheckoutOrder, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Shipping Addr(Shipping Tab vs Order Confirmation)-----------------------------//
		shippingDetailsInCheckoutPage.remove("Address2");
		shippingAddrInOrderConfirmation.remove("Address2");
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Shipping Address in Checkout page and Order confirmation page should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Shipping Address - Order Confirmation", "Checkout(Shipping)", "Order Confirmation", shippingDetailsInCheckoutPage, shippingAddrInOrderConfirmation, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Billing Addr(Billing Tab vs Order Confirmation)----------------------------//
		Log.message("<br>");
		Log.message("<b><u>Billing Address Verification</u></b>");
		Log.message("<b>Expected Result : </b>Billing Address in Checkout page and Order confirmation page should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Billing Address - Order Summary", "Checkout(Billing)", "Order Confirmation", billingAddressInCheckoutPage, billingAddrInOrderConfirmation, ignoreAddress2), 
				"<b>Actual Result : </b>Billing Address details are same in Both pages", 
				"<b>Actual Result : </b>Billing Address detials are not same in pages");
		//----------------------------Payment Methods(Place Order Tab vs Order Confirmation)----------------------------//
		Log.message("<br>");
		Log.message("<b><u>Payment Method Verification</u></b>");
		Log.message("<b>Expected Result : </b>Payment Method in Place Order Tab and Order Confirmation should be same.");
		Log.message("<br>");
		Log.softAssertThat(sortCompartPrintPaymentDetails("Checkout(Place Order)","Order Confirmation",ccCardDetailsInPlaceOrder, ccCardDetailsInOrderConfirmation,ignorePay), 
				"<b>Actual Result : </b>Payment Method are same in Both pages", 
				"<b>Actual Result : </b>Payment Method are not same in pages");
		//-----------------------------Cost Details(Place Order vs Order Confirmation)---------------------------//
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Place Order) page and Order Confirmation Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Place Order)", "Order Confirmation", costDetailsInCHKPlaceOrder, costDetailsInOrderConfirmation), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInOrderConfirmation), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//

		Log.message("<h2><b><u>OverAll Verification Table - Product Details</u></b></h2>");
		int prdIndex = productDetailsInPDP.size();
		for(int z = 0; z < prdIndex; z++){
			LinkedList<LinkedHashMap<String, String>> productMap = new LinkedList<LinkedHashMap<String, String>>();
			productMap.add(productDetailsInPDP.get(z));
			productMap.add(pdtDetailsInShoppingBagPage.get(z));
			productMap.add(pdtDetailsInCheckoutShipping.get(z));
			productMap.add(pdtDetailsInCheckoutBilling.get(z));
			productMap.add(pdtDetailsInCheckoutPlaceOrder.get(z));
			productMap.add(pdtDetailsInCheckoutOrder.get(z));
			String[] producttitle = {"PDP","Shopping Cart","Shipping","Billing","Place Order","Order Confirmation"};
			e2eUtils.printTableLinkedListHashMap(productMap,producttitle,"Product Details("+ (z+1) +")");
		}
		
		Log.message("<h2><b><u>OverAll Verification Table - Shipping Address</u></b></h2>");

		LinkedList<LinkedHashMap<String, String>> shippingaddressMap = new LinkedList<LinkedHashMap<String, String>>();
		shippingaddressMap.add(shippingDetailsInCheckoutPage);
		shippingaddressMap.add(shippingDetailsInCHKBilling);
		shippingaddressMap.add(shippingDetailsInCHKPlaceOrder);
		shippingaddressMap.add(shippingAddrInOrderConfirmation);
		String[] shippingtitle = {"Shipping","Billing","Place Order","Order Confirmation"};
		e2eUtils.printTableLinkedListHashMap(shippingaddressMap,shippingtitle,"Shipping Address Details");
		
		Log.message("<h2><b><u>OverAll Verification Table - Billing Address Address</u></b></h2>");
		LinkedList<LinkedHashMap<String, String>> billingaddressMap = new LinkedList<LinkedHashMap<String, String>>();
		billingaddressMap.add(billingAddressInCheckoutPage);
		billingaddressMap.add(billingAddressInPlaceOrder);
		billingaddressMap.add(billingAddrInOrderConfirmation);
		String[] billingTitle = {"Billing","Place Order","Order Confirmation"};
		e2eUtils.printTableLinkedListHashMap(billingaddressMap,billingTitle,"Shipping Address Details");		
		
		Log.message("<h2><b><u>OverAll Verification Table - Payment Methods</u></b></h2>");
		List<String> payIndex = new ArrayList<String>(ccCardDetailsInBilling.keySet());
		for(int x = 0 ;x < payIndex.size(); x++){
			LinkedList<LinkedHashMap<String, String>> paymentMap = new LinkedList<LinkedHashMap<String, String>>();
			if(payIndex.get(x).contains("GiftCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}else if(payIndex.get(x).contains("GiftCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}else if(payIndex.get(x).contains("CreditCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}
		}
		
		Log.message("<h2><b><u>OverAll Verification Table - Order Summary</u></b></h2>");
		LinkedList<LinkedHashMap<String, String>> costMap = new LinkedList<LinkedHashMap<String, String>>();
		costMap.add(costDetailsInCart);
		costMap.add(costDetailsInCHKShipping);
		costMap.add(costDetailsInCHKBilling);
		costMap.add(costDetailsInCHKPlaceOrder);
		costMap.add(costDetailsInOrderConfirmation);
		String[] orderSummaryTitle = {"CART","Shipping","Billing","Place Order","Order Confirmation"};
		e2eUtils.printTableLinkedListHashMap(costMap,orderSummaryTitle,"Cost Details");

	}
	
	public static void doVerifications(
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP,				//1
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage,		//2
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping,		//3
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling,		//4
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder,	//5	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder,		//6	
			LinkedHashMap<String, String> costDetailsInCart,							//7
			LinkedHashMap<String, String> costDetailsInCHKShipping,						//8
			LinkedHashMap<String, String> costDetailsInCHKBilling,						//9	
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder,					//10
			LinkedHashMap<String, String> costDetailsInOrderConfirmation,				//11
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage,	//12
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling,		//13
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder,	//14
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation,	//15
			LinkedHashMap<String, String> billingAddressInCheckoutPage,					//16
			LinkedHashMap<String, String> billingAddressInPlaceOrder,					//17
			LinkedHashMap<String, String> billingAddrInOrderConfirmation,				//18
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInBilling,			//19
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInPlaceOrder,			//20
			LinkedHashMap<String, LinkedHashMap<String, String>> ccCardDetailsInOrderConfirmation,	//21
			WebDriver driver)throws Exception{														//22
		//===========Sorting===================//
		productDetailsInPDP = Utils.sortLinkedListProduct(productDetailsInPDP);
		pdtDetailsInShoppingBagPage = Utils.sortLinkedListProduct(pdtDetailsInShoppingBagPage);
		pdtDetailsInCheckoutShipping = Utils.sortLinkedListProduct(pdtDetailsInCheckoutShipping);
		pdtDetailsInCheckoutBilling = Utils.sortLinkedListProduct(pdtDetailsInCheckoutBilling);
		pdtDetailsInCheckoutPlaceOrder = Utils.sortLinkedListProduct(pdtDetailsInCheckoutPlaceOrder);
		pdtDetailsInCheckoutOrder = Utils.sortLinkedListProduct(pdtDetailsInCheckoutOrder);
		
		shippingDetailsInCheckoutPage = Utils.sortLinkedListAddress(shippingDetailsInCheckoutPage);
		shippingDetailsInCHKBilling = Utils.sortLinkedListAddress(shippingDetailsInCHKBilling);
		shippingDetailsInCHKPlaceOrder = Utils.sortLinkedListAddress(shippingDetailsInCHKPlaceOrder);
		shippingAddrInOrderConfirmation = Utils.sortLinkedListAddress(shippingAddrInOrderConfirmation);
		
		List<String> keySet = new ArrayList<String>(ccCardDetailsInBilling.keySet());
		int keySetSize = keySet.size();
		if(keySet.toString().contains("GiftCard")){
			@SuppressWarnings("unchecked")
			HashMap<String, String> giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Balance");
			for(int p=0; p < keySetSize; p++){
				if(keySet.get(p).contains("GiftCard")){
					String balance = giftCardData.get(ccCardDetailsInBilling.get(p).get("GiftCardNumber"));
					balance = Float.toString((Float.parseFloat(balance) - Float.parseFloat(ccCardDetailsInBilling.get(ccCardDetailsInBilling).get("Amount"))));
					TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", ccCardDetailsInBilling.get("keySet.get(p)").get("GiftCardNumber"), "Balance", balance);
				}
			}
		}
		//=========Shopping Bag Verification==============//
		Log.message("<h2><b><u>Verification - Shopping Cart Page</u></b></h2>");
		Log.message("<br>");
		String[] ignoreUPC = {"Upc"};
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Cart' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Shopping Bag", productDetailsInPDP, pdtDetailsInShoppingBagPage, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCart), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//=========Shipping Page Verification===============//
		Log.message("<h2><b><u>Verification - Shipping Tab - Checkout Page</u></b></h2>");
		//---------------------------Product Details(PDP vs Shipping Tab)-----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Shipping)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Shipping)", productDetailsInPDP, pdtDetailsInCheckoutShipping, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Cost Details(Cart vs Shipping Tab)-----------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Shopping Bag and Checkout(Shipping) Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "CART", "Checkout(Shipping)", costDetailsInCart, costDetailsInCHKShipping), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKShipping), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//==========Billing Page Verification=================//
		Log.message("<h2><b><u>Verification - Billing Tab - Checkout Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Billing Tab)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Billing)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Billing)", productDetailsInPDP, pdtDetailsInCheckoutBilling, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Shipping Addr(Shipping Tab vs Billing Tab)-----------------------------//
		//LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
		Log.message("<br>");
		String[] ignoreAddress2 = {"Address2","Phone"};
		Log.message("<b>Expected Result : </b>Shipping Address in Checkout page(Shipping) and Checkout page(Billing) should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Shipping Address - Billing Tab", "Checkout(Shipping)", "Checkout(Billing)", shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Cost Details(Shipping vs Billing Tab)----------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Shipping) page and Checkout(Billing) Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Shipping)", "Checkout(Billing)", costDetailsInCHKShipping, costDetailsInCHKBilling), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKBilling), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//============Place Order Page Verification==============//
		Log.message("<h2><b><u>Verification - Place Order Tab - Checkout Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Place Order Tab)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Checkout(Place Order)' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Place Order)", productDetailsInPDP, pdtDetailsInCheckoutPlaceOrder, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//----------------------------Shipping Addr(Shipping Tab vs Place Order)----------------------------//
		//LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Shipping Address in Shipping Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Shipping Address - Place Order Tab", "Checkout(Shipping)", "Checkout(Place Order)", shippingDetailsInCheckoutPage, shippingDetailsInCHKPlaceOrder, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Billing Addr(Billing Tab vs Place Order)----------------------------//
		//LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
		Log.message("<br>");
		Log.message("<b><u>Billing Address Verification</u></b>");
		Log.message("<b>Expected Result : </b>Billing Address in Billing Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Billing Address - Place Order Tab", "Checkout(Billing)", "Checkout(Place Order)", billingAddressInCheckoutPage, billingAddressInPlaceOrder, ignoreAddress2), 
				"<b>Actual Result : </b>Billing Address details are same in Both pages", 
				"<b>Actual Result : </b>Billing Address detials are not same in pages");
		//----------------------------Payment Methods(Billing Tab vs Place Order)----------------------------//
		Log.message("<br>");
		String[] ignorePay = {"CardExpiry"};
		Log.message("<b><u>Payment Method Verification</u><b>");
		Log.message("<b>Expected Result : </b>Payment Method in Billing Tab and Place Order Tab should be same.");
		Log.message("<br>");
		Log.softAssertThat(sortCompartPrintPaymentDetails("Checkout(Billing)","Checkout(Place Order)",ccCardDetailsInBilling, ccCardDetailsInPlaceOrder,ignorePay), 
				"<b>Actual Result : </b>Payment Method are same in Both pages", 
				"<b>Actual Result : </b>Payment Method are not same in pages");
		//-----------------------------Cost Details(Billing vs Place Order Tab)---------------------------//
		//LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Shipping) page and Checkout(Place Order) page<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Billing)", "Checkout(Place Order)", costDetailsInCHKBilling, costDetailsInCHKPlaceOrder), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInCHKPlaceOrder), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//


		//===========Order Confirmation Page Verification=============//
		Log.message("<h2><b><u>Verification - Order Confirmation Page</u></b></h2>");
		//----------------------------Product Details(PDP vs Order Confirmation)----------------------------//
		//LinkedHashMap<String, String> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary().get(0);
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Verify Product details in 'PDP Page' vs 'Order Confirmation' - Product details should be equaled after adding product to bag<br>");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Product Details", "PDP", "Checkout(Place Order)", productDetailsInPDP, pdtDetailsInCheckoutOrder, ignoreUPC), 
				"<b>Actual Result : </b>Product Details are same in both Pages", 
				"<b>Actual Result : </b>Product Details are not same in both pages");
		//---------------------------Shipping Addr(Shipping Tab vs Order Confirmation)-----------------------------//
		shippingDetailsInCheckoutPage.remove("Address2");
		shippingAddrInOrderConfirmation.remove("Address2");
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Shipping Address in Checkout page and Order confirmation page should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableLinkedListHashMap("Shipping Address - Order Confirmation", "Checkout(Shipping)", "Order Confirmation", shippingDetailsInCheckoutPage, shippingAddrInOrderConfirmation, ignoreAddress2), 
				"<b>Actual Result : </b>Shipping Address details are same in Both pages", 
				"<b>Actual Result : </b>Shipping Address detials are not same in pages");
		//----------------------------Billing Addr(Billing Tab vs Order Confirmation)----------------------------//
		Log.message("<br>");
		Log.message("<b><u>Billing Address Verification</u></b>");
		Log.message("<b>Expected Result : </b>Billing Address in Checkout page and Order confirmation page should be same.");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Billing Address - Order Summary", "Checkout(Billing)", "Order Confirmation", billingAddressInCheckoutPage, billingAddrInOrderConfirmation, ignoreAddress2), 
				"<b>Actual Result : </b>Billing Address details are same in Both pages", 
				"<b>Actual Result : </b>Billing Address detials are not same in pages");
		//----------------------------Payment Methods(Place Order Tab vs Order Confirmation)----------------------------//
		Log.message("<br>");
		Log.message("<b><u>Payment Method Verification</u></b>");
		Log.message("<b>Expected Result : </b>Payment Method in Place Order Tab and Order Confirmation should be same.");
		Log.message("<br>");
		Log.softAssertThat(sortCompartPrintPaymentDetails("Checkout(Place Order)","Order Confirmation",ccCardDetailsInPlaceOrder, ccCardDetailsInOrderConfirmation,ignorePay), 
				"<b>Actual Result : </b>Payment Method are same in Both pages", 
				"<b>Actual Result : </b>Payment Method are not same in pages");
		//-----------------------------Cost Details(Place Order vs Order Confirmation)---------------------------//
		Log.message("<br>");
		Log.message("<b>Expected Result : </b>Both Order Summary Calculations should be same from Checkout(Place Order) page and Order Confirmation Page");
		Log.message("<br>");
		Log.softAssertThat(compareAndPrintTableHashMap("Cost Details - Order Summary Section", "Checkout(Place Order)", "Order Confirmation", costDetailsInCHKPlaceOrder, costDetailsInOrderConfirmation), 
				"<b>Actual Result : </b>Cost details are same in Both pages", 
				"<b>Actual Result : </b>Cost detials are not same in pages");
		//-----------------------------Order Summary Calculation Verification---------------------------------//
		Log.message("<br>");
		Log.message("<b><u>Order Summary - Calculation Verification</u></b>");
		Log.message("<b>Expected Result : </b>Order Total should be equal to Sum of All cost in Order Summary");
		Log.message("<br>");
		Log.softAssertThat(verifyOrderSummaryTotal(costDetailsInOrderConfirmation), 
				"<b>Actual Result : </b>Order Total equal to Sum of All cost in Order Summary", 
				"<b>Actual Result : </b>Order Total not equal to Sum of All cost in Order Summary");
		//--------------------------------------------------------//

		
		Log.message("<h2><b><u>OverAll Verification Table - Product Details</u></b></h2>");
		int prdIndex = productDetailsInPDP.size();
		for(int z = 0; z < prdIndex; z++){
			LinkedList<LinkedHashMap<String, String>> productMap = new LinkedList<LinkedHashMap<String, String>>();
			productMap.add(productDetailsInPDP.get(z));
			productMap.add(pdtDetailsInShoppingBagPage.get(z));
			productMap.add(pdtDetailsInCheckoutShipping.get(z));
			productMap.add(pdtDetailsInCheckoutBilling.get(z));
			productMap.add(pdtDetailsInCheckoutPlaceOrder.get(z));
			productMap.add(pdtDetailsInCheckoutOrder.get(z));
			String[] producttitle = {"PDP","Shopping Cart","Shipping","Billing","Place Order","Order Confirmation"};
			e2eUtils.printTableLinkedListHashMap(productMap,producttitle,"Product Details("+ (z+1) +")");
		}
		
		Log.message("<h2><b><u>OverAll Verification Table - Shipping Address</u></b></h2>");
		List<String> shipIndex = new ArrayList<String>(ccCardDetailsInBilling.keySet());
		for(int x = 0 ;x < shipIndex.size(); x++){
			LinkedList<LinkedHashMap<String, String>> shippingaddressMap = new LinkedList<LinkedHashMap<String, String>>();
			shippingaddressMap.add(shippingDetailsInCheckoutPage.get(x));
			shippingaddressMap.add(shippingDetailsInCHKBilling.get(x));
			shippingaddressMap.add(shippingDetailsInCHKPlaceOrder.get(x));
			shippingaddressMap.add(shippingAddrInOrderConfirmation.get(x));
			String[] shippingtitle = {"Shipping","Billing","Place Order","Order Confirmation"};
			e2eUtils.printTableLinkedListHashMap(shippingaddressMap,shippingtitle,"Shipping Address Details");
		}
		
		Log.message("<h2><b><u>OverAll Verification Table - Billing Address Address</u></b></h2>");
		LinkedList<LinkedHashMap<String, String>> billingaddressMap = new LinkedList<LinkedHashMap<String, String>>();
		billingaddressMap.add(billingAddressInCheckoutPage);
		billingaddressMap.add(billingAddressInPlaceOrder);
		billingaddressMap.add(billingAddrInOrderConfirmation);
		String[] billingTitle = {"Billing","Place Order","Order Confirmation"};
		e2eUtils.printTableLinkedListHashMap(billingaddressMap,billingTitle,"Shipping Address Details");		
		
		Log.message("<h2><b><u>OverAll Verification Table - Payment Methods</u></b></h2>");
		List<String> payIndex = new ArrayList<String>(ccCardDetailsInBilling.keySet());
		for(int x = 0 ;x < payIndex.size(); x++){
			LinkedList<LinkedHashMap<String, String>> paymentMap = new LinkedList<LinkedHashMap<String, String>>();
			if(payIndex.get(x).contains("GiftCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}else if(payIndex.get(x).contains("GiftCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}else if(payIndex.get(x).contains("CreditCard")){
				paymentMap.add(ccCardDetailsInBilling.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInPlaceOrder.get(payIndex.get(x)));
				paymentMap.add(ccCardDetailsInOrderConfirmation.get(payIndex.get(x)));
				String[] paymentTitle = {"Billing","Place Order","Order Confirmation"};
				e2eUtils.printTableLinkedListHashMap(paymentMap,paymentTitle,"Payment Method Details("+ payIndex.get(x) +")");
			}
		}
		
		Log.message("<h2><b><u>OverAll Verification Table - Order Summary</u></b></h2>");
		LinkedList<LinkedHashMap<String, String>> costMap = new LinkedList<LinkedHashMap<String, String>>();
		costMap.add(costDetailsInCart);
		costMap.add(costDetailsInCHKShipping);
		costMap.add(costDetailsInCHKBilling);
		costMap.add(costDetailsInCHKPlaceOrder);
		costMap.add(costDetailsInOrderConfirmation);
		String[] orderSummaryTitle = {"CART","Shipping","Billing","Place Order","Order Confirmation"};
		e2eUtils.printTableLinkedListHashMap(costMap,orderSummaryTitle,"Cost Details");

	}
	

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, LinkedHashMap<String, String>> fillPaymentDetails(HashMap<String, String> testData, CheckoutPage checkoutPage, WebDriver driver) throws Exception{
		int i = 1;
		boolean flag = false;
		LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfo = new LinkedHashMap<String, LinkedHashMap<String, String>>(); 

		if(!testData.get("GiftCardDetails").isEmpty()){
			String condition = new String();
			String amtValue = new String();
			int giftCardSize;
			HashMap<String, String> giftCardData = new HashMap<String, String>();
			if(testData.get("GiftCardDetails").contains("|")){
				condition = testData.get("GiftCardDetails").split("\\|")[1];
				amtValue = testData.get("GiftCardDetails").split("\\|")[2];
				giftCardSize = Integer.parseInt(testData.get("GiftCardDetails").split("\\|")[0]);
				giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Card_PIN", Arrays.asList("Balance", amtValue, condition ,"No Balance"));
			}else{
				giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Card_PIN");
				giftCardSize = Integer.parseInt(testData.get("GiftCardDetails"));
			}
			
			for(int j = 0,apply=-1 ; j < giftCardSize; j++ ){
				List<String> gcKey = new ArrayList<String>(giftCardData.keySet());
				LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
				String cardNumber = new String();
				int z = 0;
				do{
					checkoutPage.fillingBelkGiftCardDetails_test(gcKey.get(z), giftCardData.get(gcKey.get(z)));
					
					checkoutPage.clickOnApplyGiftCard();
					if(Utils.waitForElement(driver, checkoutPage.errMsgGiftCard, 3)){
						flag = false;
						giftCardData.remove(gcKey.get(z));
						//TestDataWritter.initBRDWrite(workbookName, BRD_Sheet_Name, brdKey.get(z), "notavail");
						TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", gcKey.get(z), "Balance", "No Balance");
					}
					else if(Utils.waitForElement(driver, checkoutPage.lblGiftcardSuccessMsg)){
						apply++;
						if(checkoutPage.getGiftCardAmountInSection(j).replace("-", "").trim().equals("0.00")){
							TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", gcKey.get(z), "Balance", "No Balance");
							
							checkoutPage.clickonRemoveGiftcard(apply);
							apply--;
							z++;
							flag = false;
							continue;
						}
						flag = true;
						cardNumber = gcKey.get(z);
						giftCardData.remove(gcKey.get(z));
						//TestDataWritter.initBRDWrite(workbookName, BRD_Sheet_Name, brdKey.get(z), "notavail");
						//TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", gcKey.get(z), "Balance", "notavail");
					}				
					z++;
				}while((flag==false) && (z < gcKey.size()));
				if(flag == false){
					Log.fail("Unable to Apply Giftcard..."); 
				}
				i++;
				
				cardDetails.put("GiftCardNumber", cardNumber);
				
				paymentInfo.put("GiftCard"+(j+1), cardDetails);
				Log.message("<span>    <span>--->>> Giftcard("+ (j+1) +") applied successfully!", driver);
				
			}
			for(int j = 0; j < giftCardSize; j++ ){
				paymentInfo.get("GiftCard"+(j+1)).put("Amount", checkoutPage.getGiftCardAmountInSection(j));
			}
			
		}

		if(!testData.get("DollarDetails").isEmpty()){

			//HashMap<String, String> brdData = TestDataExtractor.initBRDTestData(workbookName, BRD_Sheet_Name);
			HashMap<String, String> brdData = TestDataExtractor.initTestData(workbookName, BRD_Sheet_Name, "BRD_Number", "BRD_Status", Arrays.asList("BRD_Status", "avail", "equals" ,"notavail"));
			int dollarSize = Integer.parseInt(testData.get("DollarDetails").contains(".") ? 
					testData.get("DollarDetails").split(".")[0] : testData.get("DollarDetails"));
			
			for(int j = 0; j < dollarSize; j++ ){
				List<String> brdKey = new ArrayList<String>(brdData.keySet());
				LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
				String cardNumber = new String();
				int z = 0;
				do{
					checkoutPage.fillingBelkRewardDollars_e2e(brdKey.get(z));
					
					checkoutPage.clickOnApplyBelkRewardDollars();
					if(Utils.waitForElement(driver, checkoutPage.errMsgBRDdisplayed, 3)){
						flag = false;
						brdData.remove(brdKey.get(z));
						//TestDataWritter.initBRDWrite(workbookName, BRD_Sheet_Name, brdKey.get(z), "notavail");
						TestDataWritter.initWrite(workbookName, BRD_Sheet_Name, "BRD_Number", brdKey.get(z), "BRD_Status", "notavail");
					}
					else if(Utils.waitForElement(driver, checkoutPage.brdSuccessMsg)){
						flag = true;
						cardNumber = brdKey.get(z);
						brdData.remove(brdKey.get(z));
						//TestDataWritter.initBRDWrite(workbookName, BRD_Sheet_Name, brdKey.get(z), "notavail");
						TestDataWritter.initWrite(workbookName, BRD_Sheet_Name, "BRD_Number", brdKey.get(z), "BRD_Status", "notavail");
					}				
					z++;
				}while((flag==false) && (z < brdKey.size()));
				if(flag == false){
					Log.fail("Unable to Apply Belk Reward Dollars. Please contact BRD Administrator.", driver);
				}
				i++;
				
				cardDetails.put("BRDNumber", cardNumber.substring(cardNumber.length()-4));
				cardDetails.put("Amount", checkoutPage.getBRDAmountInSection(j));
				paymentInfo.put("BRD"+(j+1), cardDetails);
				Log.message("<span>    <span>--->>> Belk Reward Dollars("+ (j+1) +") applied successfully!", driver);
			}
		}

		if(!testData.get("CardDetails").isEmpty()){
			if(Utils.waitForElement(driver, checkoutPage.msgPaymentNotNeed, 3)){
				Log.message("<span>    <span>--->>> "+ checkoutPage.msgPaymentNotNeed.getText().trim(), driver);
			}else{
				checkoutPage.fillingCardDetails("No", testData.get("CardDetails"));
				Log.message("<span>    <span>--->>> Credit card details fillied successfully in appropriate fields!", driver);
				flag = true;i++;
				LinkedHashMap<String, String> cardDetails = checkoutPage.getPaymentDetailsInFields();
				paymentInfo.put("CreditCard", cardDetails);
			}
		}
		if(flag == false)
			Log.fail("No Payment methods recognized. Please check the Regression.xls", driver);

		return paymentInfo;
	}

	public static MyAccountPage signIn(HomePage homePage, String[]... usernamePasswd)throws Exception{
		MyAccountPage myAccountPage = null;
		if(usernamePasswd.length > 0){
			SignIn signIn = homePage.headers.navigateToSignIn();
			myAccountPage = signIn.signInToMyAccount(usernamePasswd[0][0], usernamePasswd[0][1]);

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(homePage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = homePage.clickOnMiniCart();
				shoppingBagPage.removeCouponsFromBag();
				shoppingBagPage.removeItemsFromBag();
				myAccountPage = shoppingBagPage.headers.navigateToMyAccount();
			}
		}else{
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("  ----> Navigated to 'SignIn' Page!");

			CreateAccountPage createAccount = signinPage.clickCreateAccount();
			Log.message("  ----> Navigated to Account Creation Page!");

			myAccountPage = (MyAccountPage) createAccount.CreateAccount(createAccount);
			Log.message("  ----> Account created successfully and Navigated to MyAccount Page!");
		}
		return myAccountPage;
	}

	public static ObjAndDataToReturn doOperation(String searchKey, WebDriver driver, String... giftBox)throws Exception{
		ObjAndDataToReturn dataToReturn = null;
		Headers headers = new Headers(driver);
		List<String> giftBoxProductNames = new ArrayList<String>();
		
		PdpPage pdpPage = headers.searchAndNavigateToPDP(searchKey.split("\\_")[0]);
		Log.message(" Navigated to PDP from search result page",driver);
		
		if(Utils.waitForElement(driver, pdpPage.regularProductPDP, 3)){
			
			LinkedHashMap<String, String> productDetailsInPDP = null;
			if(searchKey.contains("_"))
				productDetailsInPDP = pdpPage.setGetProductDetails(searchKey.split("\\_")[1]);
			else
				productDetailsInPDP = pdpPage.setGetProductDetails();

			if(giftBox[0].equals(searchKey))
				giftBoxProductNames.add(productDetailsInPDP.get("ProductName"));
			
			Log.message(" Selected Variation for Regular Product");
			printPrdDetails(productDetailsInPDP);

			pdpPage.clickAddToBag();
			Log.message(" Product added to Shopping cart!");
			if(giftBoxProductNames.size() > 0)
				dataToReturn = new ObjAndDataToReturn(pdpPage, productDetailsInPDP, (ArrayList<String>) giftBoxProductNames);
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetailsInPDP);
		} else if(Utils.waitForElement(driver, pdpPage.productSetPDP, 3)){
			LinkedList<LinkedHashMap<String, String>> productSetDetailsInPDP = new LinkedList<LinkedHashMap<String, String>>();

			// Selecting random Color for all products in product set
			pdpPage.selectColorRandomInProductSet();

			// Selecting Random Size for All Products in Product Set
			pdpPage.selectSizeRandomInProductSet();

			// Selecting random quantity for all products in product set
			if(searchKey.contains("_"))
				pdpPage.selectQuantityRandomInProductSet(searchKey.split("\\_")[1]);

			// Getting all details of child products in product set
			productSetDetailsInPDP = pdpPage.getProductDetailsInProductSet();
			int prdSetSize = productSetDetailsInPDP.size();
			Log.message(" Selected Variation for child products in Product Set");
			for (int k = 0; k < prdSetSize; k++) {
				Log.message("Child Product(" + (k + 1) + ")");
				printPrdDetails(productSetDetailsInPDP.get(k));
			}

			// Adding the product to bag
			pdpPage.clickAddToBag("productset");
			Log.message(" ProductSet Added to Cart successfully!");

			// Sorting product list
			productSetDetailsInPDP = Utils.sortLinkedListProduct(productSetDetailsInPDP);
			dataToReturn = new ObjAndDataToReturn(pdpPage, productSetDetailsInPDP);
		}
		return dataToReturn;
	}

	public static ObjAndDataToReturn doOperation(HashMap<String, String> testData, WebDriver driver, String[]... giftBox)throws Exception{
		ObjAndDataToReturn dataToReturn = null;
		Headers pageObject = new Headers(driver);
		PdpPage pdpPage = null;
		String[] searchKey = testData.get("SearchKey").split("\\|");
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		List<String> giftBoxProductNames = new ArrayList<String>();
		
		for(int x = 0; x < searchKey.length; x++){
			pdpPage = pageObject.searchAndNavigateToPDP(searchKey[x].split(":")[0].split("_")[0]);
			Log.message(" Navigated to PDP for Product ID:: " + searchKey[x].split(":")[0].split("_")[0],driver);
			
			//if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("regularProductPDP"), pdpPage)){
			if(Utils.waitForElement(driver, pdpPage.regularProductPDP)){
				LinkedHashMap<String, String> productDetailsInPDP = null;
				if(searchKey[x].contains("_"))
					productDetailsInPDP = pdpPage.setGetProductDetails(searchKey[x].split("_")[1]);
				else
					productDetailsInPDP = pdpPage.setGetProductDetails();

				if(giftBox.length > 0 && giftBox[0].equals(searchKey))
					giftBoxProductNames.add(productDetailsInPDP.get("ProductName"));
				
				if(searchKey[x].contains(":B")){
					pdpPage.selectStore(testData.get("Zipcode"));
					pdpPage.selectFreeInStore("YES");
				}
				
				/*if(searchKey[x].split(":")[0].contains("B"))
					pdpPage.selectStore(testData.get("Zipcode"));*/
				Log.message(" Selected Variation for Regular Product");
				printPrdDetails(productDetailsInPDP);

				pdpPage.clickAddToBag();
				Log.message(" Product added to Shopping cart!");
				productDetails.add(productDetailsInPDP);
			//} else if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("productSetPDP"), pdpPage)){
			} else if(Utils.waitForElement(driver, pdpPage.productSetPDP)){
				LinkedList<LinkedHashMap<String, String>> productSetDetailsInPDP = new LinkedList<LinkedHashMap<String, String>>();

				// Selecting Random Size for All Products in Product Set
				pdpPage.selectSizeRandomInProductSet();
				
				// Selecting random Color for all products in product set
				pdpPage.selectColorRandomInProductSet();

				// Selecting random quantity for all products in product set
				if(searchKey[x].contains("_"))
					pdpPage.selectQuantityRandomInProductSet(searchKey[x].split("\\_")[1]);

				// Getting all details of child products in product set
				productSetDetailsInPDP = pdpPage.getProductDetailsInProductSet();
				int prdSetSize = productSetDetailsInPDP.size();
				Log.message(" Selected Variation for child products in Product Set");
				for (int k = 0; k < prdSetSize; k++) {
					Log.message("Child Product(" + (k + 1) + ")");
					printPrdDetails(productSetDetailsInPDP.get(k));
				}

				for(int b = 0; b < prdSetSize; b++){
					productDetails.add(productSetDetailsInPDP.get(b));
				}
				
				// Adding the product to bag
				pdpPage.clickAddToBag("productset");
				Log.message(" ProductSet Added to Cart successfully!");

				// Sorting product list
				
			}
		}
		
		productDetails = Utils.sortLinkedListProduct(productDetails);
		if(giftBoxProductNames.size() > 0)
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails, (ArrayList<String>) giftBoxProductNames);
		else
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails);
		
		return dataToReturn;
	}

	public static boolean compareAndPrintTableHashMap(String title, String col1Head, String col2Head, LinkedHashMap<String, String> hashMap1, LinkedHashMap<String, String> hashMap2, String[]... noNeed)throws Exception{

		if(noNeed.length > 0){
			for(int i=0; i<noNeed.length;i++){
				System.out.println("Removed Key from <b>Expected:: " + noNeed[i][i]);
				hashMap1.remove(noNeed[i][i]);
				System.out.println("Removed Key from Actual  :: " + noNeed[i][i]);
				hashMap2.remove(noNeed[i][i]);
			}
		}

		Log.message("<br>");
		Log.message1("<table><tr bgcolor='green'><td colspan=3><font color='white'>" + title + "</font></td></tr>");
		Log.message1("<tr align='center' bgcolor='orange'><td>Contents</td><td>" + col1Head + "</td><td>" + col2Head + "</td></tr>");
		List<String> indexes1 = new ArrayList<String>(hashMap1.keySet());
		List<String> indexes2 = new ArrayList<String>(hashMap2.keySet());
		List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1 : indexes2;

		for(int i = 0; i < maxIndex.size(); i++){
			String value1 = hashMap1.containsKey(maxIndex.get(i))? hashMap1.get(maxIndex.get(i)):"No Value";
			String value2 = hashMap2.containsKey(maxIndex.get(i))? hashMap2.get(maxIndex.get(i)):"No Value";
			if(value1.equals(value2))
				Log.message1("<tr><td bgcolor='orange'>" + maxIndex.get(i) + "</td><td>"+ value1 +"</td><td>" + value2 + "</td></tr>");
			else
				Log.message1("<tr><td bgcolor='orange'>" + maxIndex.get(i) + "</td><td bgcolor='red'>"+ value1 +"</td><td>" + value2 + "</td></tr>");
		}
		Log.message1("</table>");

		return Utils.compareTwoHashMap(hashMap1, hashMap2);
	}
	
	public static void printTableHashMap(String title, LinkedHashMap<String, String> hashMap1)throws Exception{

		Log.message("<br>");
		Log.message1("<table><tr bgcolor='green'><td colspan=2><font color='white'>" + title + "</font></td></tr>");
		Log.message1("<tr align='center' bgcolor='orange'><td>Contents</td><td>Value</td></tr>");
		List<String> indexes1 = new ArrayList<String>(hashMap1.keySet());
		for(int i = 0; i < indexes1.size(); i++){
				Log.message1("<tr><td bgcolor='orange'>" + indexes1.get(i) + "</td><td>"+ hashMap1.get(indexes1.get(i)) +"</td></tr>");
		}
		Log.message1("</table>");
	}

	public static boolean compareAndPrintTableLinkedListHashMap(String title, String col1Head, String col2Head, LinkedList<LinkedHashMap<String, String>> hashMap1, LinkedList<LinkedHashMap<String, String>> hashMap2, String[]... noNeed)throws Exception{
		//int iteration1 = hashMap1.size();
		int iteration2 = hashMap2.size();
		boolean flag = true;
		for(int j=0; j<iteration2; j++){
			if(noNeed.length > 0){
				for(int i=0; i<noNeed.length;i++){
					System.out.println("Removed Key from <b>Expected:: " + noNeed[i][i]);
					hashMap1.get(j).remove(noNeed[i][i]);
					System.out.println("Removed Key from Actual  :: " + noNeed[i][i]);
					hashMap2.get(j).remove(noNeed[i][i]);
				}
			}

			Log.message("<br>");
			Log.message1("<table><tr bgcolor='green' align='center'><td colspan=3><font color='white'>" + title + "("+ (j+1) +")</font></td></tr>");
			Log.message1("<tr align='center' bgcolor='orange'><td>Contents</td><td>" + col1Head + "</td><td>" + col2Head + "</td></tr>");
			List<String> indexes1 = new ArrayList<String>(hashMap1.get(j).keySet());
			List<String> indexes2 = new ArrayList<String>(hashMap2.get(j).keySet());
			List<String> maxIndex = indexes1.size() > indexes2.size()? indexes1:indexes2;
			for(int i = 0; i < maxIndex.size(); i++){
				String value1 = hashMap1.get(j).containsKey(maxIndex.get(i)) ? hashMap1.get(j).get(maxIndex.get(i)) : "No Value";
				String value2 = hashMap2.get(j).containsKey(maxIndex.get(i)) ? hashMap2.get(j).get(maxIndex.get(i)) : "No Value";
				if(value1.equals(value2))
					Log.message1("<tr><td bgcolor='orange'>" + maxIndex.get(i) + "</td><td>"+ value1 +"</td><td>" + value2 + "</td></tr>");
				else
					Log.message1("<tr><td bgcolor='orange'>" + maxIndex.get(i) + "</td><td bgcolor='red'>"+ value1 +"</td><td>" + value2 + "</td></tr>");
			}
			Log.message1("</table>");

			flag = Utils.compareTwoHashMap(hashMap1.get(j), hashMap2.get(j));
		}

		return flag;
	}

	public static LinkedList<LinkedHashMap<String, String>> makeUnique(LinkedList<LinkedHashMap<String, String>> hashMap)throws Exception{
		int nosProduct = hashMap.size();
		for(int i = 0; i < nosProduct; i++){
			for(int j = i+1 ; j < nosProduct; j++){
				if(hashMap.get(i).get("ProductName").equals(hashMap.get(j).get("ProductName")))
					if(hashMap.get(i).get("Color").equals(hashMap.get(j).get("Color")))
						if(hashMap.get(i).get("Size").equals(hashMap.get(j).get("Size"))){
							int qty = Integer.parseInt(hashMap.get(i).get("Quantity")) + Integer.parseInt(hashMap.get(j).get("Quantity"));
							hashMap.get(i).put("Quantity", Integer.toString(qty));
							hashMap.remove(j);
							nosProduct = hashMap.size();
							j--;
						}
			}
		}

		return hashMap;
	}

	public LinkedList<LinkedHashMap<String, String>> sortLinkedList(LinkedList<LinkedHashMap<String, String>> actualList)throws Exception{
		LinkedList<LinkedHashMap<String, String>> listToReturn = new LinkedList<LinkedHashMap<String, String>>();
		List<String> indexes = new ArrayList<String>(actualList.get(0).keySet());
		actualList = makeUnique(actualList);

		LinkedList<String> list = new LinkedList<String>();
		int size = actualList.size();

		for(int x = 0; x < size; x++)
			list.add(actualList.get(x).get(indexes.get(0)));

		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});

		for(int i = 0; i < size; i++){
			for(int j = 0; j < size; j++){
				if(list.get(i).equals(actualList.get(j).get(indexes.get(0)))){
					listToReturn.add(actualList.get(j));
				}
			}
		}

		//printing sorted list
		/*System.out.println("-----------------------------------------");
		for(int y = 0 ;y < size; y++)
			System.out.println(list.get(y));
		System.out.println("-----------------------------------------");*/
		return listToReturn;
	}

	public static void printPrdDetails(LinkedHashMap<String, String> hashMap)throws Exception{
		Log.message("    ---> Selected Product  : " + hashMap.get("ProductName"));
		Log.message("    ---> selected Color    : " + hashMap.get("Color"));
		Log.message("    ---> selected size     : " + hashMap.get("Size"));
		Log.message("    ---> selected Quantity : " + hashMap.get("Quantity"));
	}
	
	public static boolean printTableLinkedListHashMap(LinkedList<LinkedHashMap<String, String>> linkedList, String[] title, String tableTitle)throws Exception{
		int size = linkedList.size();
		int iteration2;
		boolean flag = true;
		List<String> indexes = new ArrayList<String>(linkedList.get(0).keySet());
		List<String> maxIndex = indexes;
		for(int x=0; x < size; x++){
			if(maxIndex.size() < linkedList.get(x).keySet().size())
				maxIndex = new ArrayList<>(linkedList.get(x).keySet());
		}
		Log.message("<br>");
		Log.message1("<table><tr bgcolor='green'><td colspan="+ (size+1) +"><font color='white'>Verification - "+ tableTitle +"</font></td></tr>");
		Log.message1("<tr align='center' bgcolor='orange'><td>Contents</td>");
		for(int i = 0 ; i < size; i++)
		{
			Log.message1("<td bgcolor='orange'>"+ title[i] +"</td>");
		}
		Log.message1("</tr>");
		iteration2 = maxIndex.size();
		for(int j=0; j<iteration2; j++){
			
			Log.message1("<tr align='center'><td bgcolor='orange'>"+ maxIndex.get(j) +"</td>");
			for(int i = 0 ; i < size; i++)
			{
				String value = linkedList.get(i).containsKey(maxIndex.get(j))?linkedList.get(i).get(maxIndex.get(j)):"No Value";
				Log.message1("<td>"+ value +"</td>");
			}
			Log.message1("</tr>");
			
		}
		Log.message1("</table>");

		return flag;
	}
	
	public static boolean sortCompartPrintPaymentDetails(String colHead1, String colHead2, LinkedHashMap<String, LinkedHashMap<String, String>> paymentDetails1, LinkedHashMap<String, LinkedHashMap<String, String>> paymentDetails2, String[]... ignore)throws Exception{
		
		boolean flag = true;
		List<String> outterIndexOfFirst = new ArrayList<String>(paymentDetails1.keySet());
		List<String> outterIndexOfSecond = new ArrayList<String>(paymentDetails2.keySet());
		
		if(outterIndexOfFirst.size() != outterIndexOfSecond.size()){
			Log.message("Payment Methods Not fetched correctly : "+ colHead1 +"("+ outterIndexOfFirst +"), "+ colHead2 +"("+ outterIndexOfSecond +")");
			return false;
		}
		
		if(outterIndexOfFirst.toString().contains("GiftCard") && outterIndexOfSecond.toString().contains("GiftCard")){
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList<LinkedHashMap<String, String>>();
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList<LinkedHashMap<String, String>>();
			
			for(int i = 0; i < outterIndexOfFirst.size(); i++){
				if(outterIndexOfFirst.get(i).contains("GiftCard")){
					giftCardDetailsInFirst.add(paymentDetails1.get(outterIndexOfFirst.get(i)));
					
					if(giftCardDetailsInFirst.get(i).get("GiftCardNumber").length() > 4){
						String cardNumber = giftCardDetailsInFirst.get(i).get("GiftCardNumber").substring(giftCardDetailsInFirst.get(i).get("GiftCardNumber").length()-4);
						giftCardDetailsInFirst.get(i).put("GiftCardNumber", cardNumber);
					}
				}
				
				if(outterIndexOfSecond.get(i).contains("GiftCard")){
					giftCardDetailsInSecond.add(paymentDetails2.get(outterIndexOfSecond.get(i)));
					if(giftCardDetailsInSecond.get(i).get("GiftCardNumber").length() > 4){
						String cardNumber = giftCardDetailsInSecond.get(i).get("GiftCardNumber").substring(giftCardDetailsInSecond.get(i).get("GiftCardNumber").length()-4);
						giftCardDetailsInSecond.get(i).put("GiftCardNumber", cardNumber);
					}
				}
			}
			
			flag = compareAndPrintTableLinkedListHashMap("Payment Method(Gift Card) - Order Summary", colHead1, colHead2,giftCardDetailsInFirst, giftCardDetailsInSecond, ignore);
			
		}

		if(outterIndexOfFirst.toString().contains("BRD") && outterIndexOfSecond.toString().contains("BRD")){
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList<LinkedHashMap<String, String>>();
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList<LinkedHashMap<String, String>>();
			
			for(int i = 0; i < outterIndexOfFirst.size(); i++){
				if(outterIndexOfFirst.get(i).contains("BRD")){
					giftCardDetailsInFirst.add(paymentDetails1.get(outterIndexOfFirst.get(i)));
				}
				
				if(outterIndexOfSecond.get(i).contains("BRD")){
					giftCardDetailsInSecond.add(paymentDetails2.get(outterIndexOfSecond.get(i)));
				}
			}
			
			flag = compareAndPrintTableLinkedListHashMap("Payment Method(BRD) - Order Summary", colHead1, colHead2,giftCardDetailsInFirst, giftCardDetailsInSecond, ignore);
		}

		if(outterIndexOfFirst.toString().contains("CreditCard") && outterIndexOfSecond.toString().contains("CreditCard")){
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList<LinkedHashMap<String, String>>();
			LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList<LinkedHashMap<String, String>>();
			
			for(int i = 0; i < outterIndexOfFirst.size(); i++){
				if(outterIndexOfFirst.get(i).contains("CreditCard")){
					giftCardDetailsInFirst.add(paymentDetails1.get(outterIndexOfFirst.get(i)));
				}
				
				if(outterIndexOfSecond.get(i).contains("CreditCard")){
					giftCardDetailsInSecond.add(paymentDetails2.get(outterIndexOfSecond.get(i)));
				}
			}
			
			flag = compareAndPrintTableLinkedListHashMap("Payment Method(Credit Card) - Order Summary", colHead1, colHead2,giftCardDetailsInFirst, giftCardDetailsInSecond, ignore);
		}
		
		return flag;
	}

	public static boolean verifyOrderSummaryTotal(LinkedHashMap<String, String> ordersummaryTable)throws Exception{
		boolean Status=false;
		float costDetail = 0;
		float orderTotal = Float.parseFloat(ordersummaryTable.get("EstimatedOrderTotal").replace(",", ""));
		List<String> keyString = new ArrayList<String>(ordersummaryTable.keySet());
		for(int q = 0 ; q < ordersummaryTable.size()-1; q++){
			float value = 0;
			if(ordersummaryTable.get(keyString.get(q)).contains("-"))
				value -= Float.parseFloat(ordersummaryTable.get(keyString.get(q)).replace("-", "").replace(",", "").replace("$", ""));
			else
				value += Float.parseFloat(ordersummaryTable.get(keyString.get(q)).replace("-", "").replace(",", "").replace("$", ""));
			costDetail += value;
		}
		
		DecimalFormat df = new DecimalFormat("###.##");
		float costDetailAfterRounding=Float.parseFloat(df.format(costDetail));
		if(costDetailAfterRounding == orderTotal)
		{
			Status=true;
		}
		
		return Status;
	}
	
	public static void checkGiftBox(List<String> giftBoxProductNames, ShoppingBagPage shoppingBagPage)throws Exception{
		if(giftBoxProductNames.size() > 0){
			for(int x = 0; x < giftBoxProductNames.size(); x++){
				shoppingBagPage.checkAddGiftBoxByProductName(giftBoxProductNames.get(x));
				Log.message("    ---> Gift box added successfully for product("+ giftBoxProductNames.get(x) +") in Cart!");
			}
		}
	}
	/**
	 * 
	 * @param couponData
	 * @param shoppingBagPage
	 * @param driver
	 * @throws Exception
	 * Last modified Dhanapal 01/29/2017
	 */
	public static void applyCoupon(String[] couponData, ShoppingBagPage shoppingBagPage, WebDriver driver)throws Exception{
		
		for(int i = 0 ; i < couponData.length ; i++){

			shoppingBagPage.applyCouponInShoppingBag(couponData[i]);
			//shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtCouponSuccess","trowTotalCouponSavings"), shoppingBagPage)
			if(Utils.waitForElement(driver, shoppingBagPage.trowTotalCouponSavings) && 
					(Utils.waitForElement(driver, shoppingBagPage.txtCouponSuccess))){
				Log.message("    ---> Coupon applied successfully!");
			}else{
				Log.fail("    ---> Unable to Apply coupon. Err:: " + shoppingBagPage.getCouponErrMsg(), driver);
			}

		}
		
	}
	
	
	/**
	 * @Metho createRegistryAccount this method will return event type, registry
	 *        first name, registry last name
	 * @param homePage
	 * @param registryInfo
	 * @param shippingInfo
	 * @return
	 * @throws Exception
	 */
	public static RegistrySignedUserPage signInWithRegistryUser(HomePage homePage, String registryInfo, String shippingInfo)
			throws Exception {
		SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
		Log.message("  Navigate to resgistry page");

		CreateAccountPage createAccountPage = signIn.clickCreateAccount();
		Log.message("  Navigate to create Account page");

		GiftRegistryPage giftRegistryPage = (GiftRegistryPage) createAccountPage.CreateAccount(createAccountPage);
		Log.message(" Navigate to 'Gift Registry Page' After created Account");

		RegistrySignedUserPage registrySignedUserPage = giftRegistryPage.clickOnCreateRegistrybutton();
		Log.message(" Click on Create Registry button");

		registrySignedUserPage.fillingEventDetails(registryInfo);
		Log.message(" Filled Registry Details");
		
		registrySignedUserPage.clickContinueInCreateRegistry();
		Log.message(" Click on Continue Button in the create registry page");

		registrySignedUserPage.fillingPreEventShippingdetails(shippingInfo);
		Log.message(" Filled Event shipping Details");

		registrySignedUserPage.fillingPostEventShippingdetails(shippingInfo);
		Log.message(" Filled Event shipping Details");

		registrySignedUserPage.clickOnContinueButtonInEventShipping();
		Log.message(" Click on Continue Button in the 'Event Shipping' page");

		registrySignedUserPage.clickOnSubmitButton();
		Log.message(" Click on submit button in the Registry Page");
		return registrySignedUserPage;

	}
	
	public static ObjAndDataToReturn addProductToRegistry(HashMap<String, String> testData, WebDriver driver, String[]... giftBox)throws Exception{
		ObjAndDataToReturn dataToReturn = null;
		Headers pageObject = new Headers(driver);
		PdpPage pdpPage = null;
		String[] searchKey = testData.get("SearchKey").split("\\|");
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		List<String> giftBoxProductNames = new ArrayList<String>();
		
		for(int x = 0; x < searchKey.length; x++){
			pdpPage = pageObject.searchAndNavigateToPDP(searchKey[x].split(":")[0].split("_")[0]);
			Log.message(" Navigated to PDP for Product ID:: " + searchKey[x].split(":")[0].split("_")[0],driver);
			
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("regularProductPDP"), pdpPage)){
				LinkedHashMap<String, String> productDetailsInPDP = null;
				if(searchKey[x].contains("_"))
					productDetailsInPDP = pdpPage.setGetProductDetails(searchKey[x].split("_")[1]);
				else
					productDetailsInPDP = pdpPage.setGetProductDetails();

				if(giftBox.length > 0 && giftBox[0].equals(searchKey))
					giftBoxProductNames.add(productDetailsInPDP.get("ProductName"));
				
				Log.message(" Selected Variation for Regular Product");
				printPrdDetails(productDetailsInPDP);

				pdpPage.clickOnAddItemToRegistry();
				Log.message(" Product added to Registry!");
				productDetails.add(productDetailsInPDP);
			} 
		}
		
		productDetails = Utils.sortLinkedListProduct(productDetails);
		if(giftBoxProductNames.size() > 0)
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails, (ArrayList<String>) giftBoxProductNames);
		else
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails);
		
		return dataToReturn;
	}
	
	public static void addProductFromRegistryToBag(RegistrySignedUserPage registrySignedUserPage, String productName) throws Exception{
		 registrySignedUserPage.clickOnViewRegistrylnk();
		 Log.message(" ---> Clicked View button in the Registry page");
         registrySignedUserPage.addItemToBag(productName);
         Log.message(" ---> Product added to Bag");
	}
	
	
	/**
	 * @Metho createRegistryAccount this method will return event type, registry
	 *        first name, registry last name
	 * @param homePage
	 * @param registryInfo
	 * @param shippingInfo
	 * @return
	 * @throws Exception
	 */
	public static WishListPage createWishListAccount(HomePage homePage) throws Exception {
		SignIn signIn = homePage.headers.navigateToWishListAsGuest();
		Log.message(". Navigate to resgistry page");

		CreateAccountPage createAccountPage = signIn.clickCreateAccount();
		Log.message( ". Navigate to create Account page");

		WishListPage wishListPage = (WishListPage) createAccountPage.CreateAccount(createAccountPage);
		Log.message(" Navigate to 'Wish List Page' After created Account");

		return wishListPage;
	}
	
	/**
	 * addProductToList
	 * @param testData
	 * @param driver
	 * @param giftBox
	 * @return
	 * @throws Exception
	 */
	public static ObjAndDataToReturn addProductToList(HashMap<String, String> testData, WebDriver driver, String[]... giftBox)throws Exception{
		ObjAndDataToReturn dataToReturn = null;
		Headers pageObject = new Headers(driver);
		PdpPage pdpPage = null;
		String[] searchKey = testData.get("SearchKey").split("\\|");
		LinkedList<LinkedHashMap<String, String>> productDetails = new LinkedList<LinkedHashMap<String, String>>();
		List<String> giftBoxProductNames = new ArrayList<String>();
		
		for(int x = 0; x < searchKey.length; x++){
			pdpPage = pageObject.searchAndNavigateToPDP(searchKey[x].split(":")[0].split("_")[0]);
			Log.message(" Navigated to PDP for Product ID:: " + searchKey[x].split(":")[0].split("_")[0],driver);
			
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("regularProductPDP"), pdpPage)){
				LinkedHashMap<String, String> productDetailsInPDP = null;
				if(searchKey[x].contains("_"))
					productDetailsInPDP = pdpPage.setGetProductDetails(searchKey[x].split("_")[1]);
				else
					productDetailsInPDP = pdpPage.setGetProductDetails();

				if(giftBox.length > 0 && giftBox[0].equals(searchKey))
					giftBoxProductNames.add(productDetailsInPDP.get("ProductName"));
				
				Log.message(" Selected Variation for Regular Product");
				printPrdDetails(productDetailsInPDP);

				pdpPage.clickAddToWishListLink();
				Log.message(" Product added to Wish List!");
				productDetails.add(productDetailsInPDP);
			} 
		}
		
		productDetails = Utils.sortLinkedListProduct(productDetails);
		if(giftBoxProductNames.size() > 0)
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails, (ArrayList<String>) giftBoxProductNames);
		else
			dataToReturn = new ObjAndDataToReturn(pdpPage, productDetails);
		
		return dataToReturn;
	}
	
	public static void addProductFromListToBag(WishListPage wishListPage, String productName) throws Exception{
		wishListPage.addProductToBag(productName);
		Log.message(" Product added To Bag");
	}
	
	
	@SuppressWarnings("unchecked")
	public static void checkGiftCardBalance(CheckoutPage checkoutPage, WebDriver driver)throws Exception{
		//HashMap<String, String> giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Card_PIN");
		HashMap<String, String> giftCardData = TestDataExtractor.initTestData(workbookName, GC_Sheet_Name, "Card_Number", "Card_PIN", Arrays.asList("Balance","100","greaterThan","No Balance"));
		int size = giftCardData.size();
		List<String> giftCardNumber = new ArrayList<String>(giftCardData.keySet());
		for(int x = 0; x < size; x++){
			checkoutPage.fillingBelkGiftCardDetails_test(giftCardNumber.get(x),giftCardData.get(giftCardNumber.get(x)));
			checkoutPage.clickOnCheckGiftCardBalance();
			if(checkoutPage.txtGiftCardMessage.getText().contains("Gift Card Balance:")){
				String balance = checkoutPage.txtGiftCardMessage.getText().split(":")[1].trim();
				TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", giftCardNumber.get(x), "Balance", balance);
			}else{
				TestDataWritter.initWrite(workbookName, GC_Sheet_Name, "Card_Number", giftCardNumber.get(x), "Balance", "No balance");
			}
		}
		
	}
	
}// Billing_Page_Util