package com.belk.reusablecomponents;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * 
 * To define the state details and get the state code, state name
 *
 */
public class StateUtils {

	/**
	 * To set up the state details and returning as state details has hash map
	 * 
	 * @return stateDetails as a hash map
	 */
	public static HashMap<String, String> setState() {
		HashMap<String, String> stateDetails = new HashMap<String, String>();

		stateDetails.put("Armed Forces (AA)", "AA");
		stateDetails.put("Armed Forces (AE)", "AE");
		stateDetails.put("Armed Forces (AP)", "AP");
		stateDetails.put("Alabama", "AL");
		stateDetails.put("Alaska", "AK");
		stateDetails.put("Arizona", "AZ");
		stateDetails.put("Arkansas", "AR");
		stateDetails.put("California", "CA");
		stateDetails.put("Colorado", "CO");
		stateDetails.put("Connecticut", "CT");
		stateDetails.put("Delaware", "DE");
		stateDetails.put("Florida", "FL");
		stateDetails.put("Georgia", "GA");
		stateDetails.put("Hawaii", "HI");
		stateDetails.put("Idaho", "ID");
		stateDetails.put("Illinois", "IL");
		stateDetails.put("Indiana", "IN");
		stateDetails.put("Iowa", "IA");
		stateDetails.put("Kansas", "KS");
		stateDetails.put("Kentucky", "KY");
		stateDetails.put("Louisiana", "LA");
		stateDetails.put("Maine", "ME");
		stateDetails.put("Maryland", "MD");
		stateDetails.put("Massachusetts", "MA");
		stateDetails.put("Michigan", "MI");
		stateDetails.put("Minnesota", "MN");
		stateDetails.put("Mississippi", "MS");
		stateDetails.put("Missouri", "MO");
		stateDetails.put("Montana", "MT");
		stateDetails.put("Nebraska", "NE");
		stateDetails.put("Nevada", "NV");
		stateDetails.put("New Hampshire", "NH");
		stateDetails.put("New Jersey", "NJ");
		stateDetails.put("New Mexico", "NM");
		stateDetails.put("New York", "NY");
		stateDetails.put("North Carolina", "NC");
		stateDetails.put("North Dakota", "ND");
		stateDetails.put("Ohio", "OH");
		stateDetails.put("Oklahoma", "OK");
		stateDetails.put("Oregon", "OR");
		stateDetails.put("Pennsylvania", "PA");
		stateDetails.put("Rhode Island", "PI");
		stateDetails.put("South Carolina", "SC");
		stateDetails.put("South Dakota", "SD");
		stateDetails.put("Tennessee", "TN");
		stateDetails.put("Texas", "TX");
		stateDetails.put("Utah", "UT");
		stateDetails.put("Vermont", "VT");
		stateDetails.put("Virginia", "VA");
		stateDetails.put("Washington", "WA");
		stateDetails.put("Washington, D.C.", "DC");
		stateDetails.put("West Virginia", "WV");
		stateDetails.put("Wisconsin", "WI");
		stateDetails.put("Wyoming", "WY");

		return stateDetails;
	}

	/**
	 * To get the state code using the state name
	 * Ex: getStateCode("North Carolina");
	 * @param stateName
	 * @return stateCode
	 */
	public static String getStateCode(String stateName) {
		String stateCode = null;
		HashMap<String, String> getState = setState();
		stateCode = hasState(stateName) ? (String) getState.get(stateName) : null;
		return stateCode;
	}

	/**
	 * To get the state name using the state code
	 * Ex: getStateName("NC");
	 * @param stateCode
	 * @return stateName
	 */
	public static String getStateName(String stateCode) {
		String stateName = null;
		HashMap<String, String> getState = setState();
		for (Entry<String, String> entry : getState.entrySet()) {
			if (entry.getValue().equals(stateCode)) {
				stateName = entry.getKey();
				break;
			}
		}
		return stateName;
	}

	/**
	 * To check hash map contains given key(stateName)
	 * @param stateName
	 * @return true or false
	 */
	private static boolean hasState(String stateName) {
		HashMap<String, String> hasStateName = setState();
		return hasStateName.containsKey(stateName);
	}

}
