package com.belk.reusablecomponents;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import com.belk.pages.PdpPage;

public class ObjAndDataToReturn {
	    public PdpPage pdpPage;
	    public LinkedHashMap<String, String> prdDetails;
	    public LinkedList<LinkedHashMap<String, String>> prdDetailsList;
	    public List<String> giftBoxProductNames = new ArrayList<String>();
	    
	    public ObjAndDataToReturn(PdpPage pdppage, LinkedHashMap<String, String> prddetails) {
	        this.pdpPage = pdppage;
	        this.prdDetails = prddetails;
	        this.prdDetailsList = null;
	    }
	    
	    public ObjAndDataToReturn(PdpPage pdppage, LinkedHashMap<String, String> prddetails, ArrayList<String> giftbox) {
	        this.pdpPage = pdppage;
	        this.prdDetails = prddetails;
	        this.prdDetailsList = null;
	        this.giftBoxProductNames = giftbox;
	    }
	    
	    public ObjAndDataToReturn(PdpPage pdppage, LinkedList<LinkedHashMap<String, String>> prddetailslist) {
	        this.pdpPage = pdppage;
	        this.prdDetailsList = prddetailslist;
	        this.prdDetails = null;
	    }
	    
	    public ObjAndDataToReturn(PdpPage pdppage, LinkedList<LinkedHashMap<String, String>> prddetailslist, ArrayList<String> giftbox) {
	        this.pdpPage = pdppage;
	        this.prdDetailsList = prddetailslist;
	        this.prdDetails = null;
	        this.giftBoxProductNames = giftbox;
	    }
}
