package com.belk.testscripts.cssuite;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CustomerServiceHomePage;
import com.belk.pages.CustomerServiceLoginPage;
import com.belk.pages.HomePage;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class CSSuiteTestCases {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "CSSuite";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}
	
	@Test(groups = { "desktop" }, description = "Verify Left Navigation in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CSSUITE_001(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase 'Left Nav' is not applicable for mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String username = "vijay.radhakrishnan";
		String password = "Belk@123";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			CustomerServiceLoginPage customerServiceLoginPage = new CustomerServiceLoginPage(driver, webSite).get();
			CustomerServiceHomePage customerServiceHomePage = customerServiceLoginPage.logInToCSHomePage(username, password);
			// TODO
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_003
}