package com.belk.testscripts.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CheckoutPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.GiftCardsPage;
import com.belk.pages.HomePage;
import com.belk.pages.OrderConfirmationPage;
import com.belk.pages.PdpPage;
import com.belk.pages.QuickViewPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.AddressBookPage;
import com.belk.pages.account.EmailPreferencesPage;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.PaymentMethodsPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.CustomerServicePage;
import com.belk.pages.footers.PolicyGuideLinePage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistryInformationPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.DateTimeUtility;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class Registry {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "RegistryWishList";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System.getProperty("webSite")
				: context.getCurrentXmlTest().getParameter("webSite"));
	}// init

	/**
	 * @Metho createRegistryAccount this method will return event type, registry
	 *        first name, registry last name
	 * @param homePage
	 * @param registryInfo
	 * @param shippingInfo
	 * @return
	 * @throws Exception
	 */
	public String[] createRegistryAccount(HomePage homePage, String registryInfo, String shippingInfo, int i)
			throws Exception {
		SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
		Log.message(i++ + ". Navigate to resgistry page");

		CreateAccountPage createAccountPage = signIn.clickCreateAccount();
		Log.message(i++ + ". Navigate to create Account page");

		GiftRegistryPage giftRegistryPage = (GiftRegistryPage) createAccountPage.CreateAccount(createAccountPage);
		Log.message(i++ + ". Navigate to 'Gift Registry Page' After created Account");

		RegistrySignedUserPage registrySignedUserPage = giftRegistryPage.clickOnCreateRegistrybutton();
		Log.message(i++ + ". Click on Create Registry button");

		LinkedHashMap<String, String> registryDetails = registrySignedUserPage.fillingEventDetails(registryInfo);
		Log.message(i++ + ". Filled Registry Details");

		List<String> indexes = new ArrayList<String>(registryDetails.keySet());
		String[] eventDetails = new String[4];
		eventDetails[0] = indexes.get(0).replace("select_eventType_", "");
		eventDetails[1] = indexes.get(6).replace("type_reFirstName_", "");
		eventDetails[2] = indexes.get(7).replace("type_reLastName_", "");

		registrySignedUserPage.clickContinueInCreateRegistry();
		Log.message(i++ + ". Click on Continue Button in the create registry page");

		registrySignedUserPage.fillingPreEventShippingdetails(shippingInfo);
		Log.message(i++ + ". Filled Event shipping Details");

		registrySignedUserPage.fillingPostEventShippingdetails(shippingInfo);
		Log.message(i++ + ". Filled Event shipping Details");

		registrySignedUserPage.clickOnContinueButtonInEventShipping();
		Log.message(i++ + ". Click on Continue Button in the 'Event Shipping' page");

		registrySignedUserPage.clickOnSubmitButton();
		Log.message(i++ + ". Click on submit button in the Registry Page");
		eventDetails[3] = Integer.toString(i);
		return eventDetails;

	}

	/**
	 * @Metho createRegistryAccount this method will return event type, registry
	 *        first name, registry last name
	 * @param homePage
	 * @param registryInfo
	 * @param shippingInfo
	 * @return
	 * @throws Exception
	 */
	public String[] createWishListAccount(HomePage homePage, int i) throws Exception {
		SignIn signIn = homePage.headers.navigateToWishListAsGuest();
		Log.message(i++ + ". Navigate to resgistry page");

		CreateAccountPage createAccountPage = signIn.clickCreateAccount();
		Log.message(i++ + ". Navigate to create Account page");

		LinkedHashMap<String, String> accountDetails = createAccountPage.getAccountDetails();

		List<String> indexes = new ArrayList<String>(accountDetails.values());
		String[] accountInfo = new String[4];
		accountInfo[0] = indexes.get(0);
		accountInfo[1] = indexes.get(1);
		accountInfo[2] = indexes.get(2);
		accountInfo[3] = Integer.toString(i);

		return accountInfo;

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify items is removed from the list.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_083(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			signIn.signInToMyAccount(emailid, password);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("3. Searched with '" + searchKey + "' in the search text box");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("4. Navigated to 'Pdp' Page with randomly selected product");

			Log.message("5. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			String BrandName = pdpPage.getProductBrandName();
			String productName = pdpPage.getProductName();

			productName = BrandName.concat(" " + productName);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("6. Clicked on 'Add To Registry' link in PDP");

			// RegistrySignedUserPage registrySignedUserPage =
			// (RegistrySignedUserPage) myAccountPage
			// .navigateToSection("registry");

			pdpPage.headers.navigateToGiftRegistry();
			Log.message("7. Clicked on Registry link");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("8. Clicked on View link");

			Log.message("9. Clicked on Remove link");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Product should be removed when clicking on remove button");

			Log.assertThat((registrySignedUserPage.clickDeleteItem()),
					"<b>Actual Result 1:</b> Product is removed when clicking on remove button",
					"<b>Actual Result 1:</b> Product is not removed when clicking on remove button");

			/*
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 2:</b> 'You have no items in your gift registry' message should be displayed when clicking on remove button"
			 * );
			 * 
			 * Log.assertThat(registrySignedUserPage.
			 * getNoItemsInRegistryMessage( ).equals(
			 * "You have no items in your gift registry."),
			 * "<b>Actual Result 2:</b> 'You have no items in your gift registry' message is displayed when clicking on remove button"
			 * ,
			 * "<b>Actual Result 2:</b> 'You have no items in your gift registry' message is not displayed when clicking on remove button"
			 * );
			 */

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_083

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry Banner Content Slot in the Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_084(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Registry banner slot should be displayed");

			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryBannerSlot"),
							registrySignedUserPage),
							"<b>Actual Result :</b> Registry banner slot is displayed",
					"<b>Actual Result :</b> Registry banner slot is not displayed");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_084

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify My Account Links Content Asset is displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_085(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> My Account should be displayed on the left side");

			Log.assertThat(registrySignedUserPage.getMyAccountInfo().equals("My Account"),
					"<b>Actual Result 1:</b> My Account is displayed on the left side",
					"<b>Actual Result 1:</b> My Account is not displayed on the left side", driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Clicking on 'Profile' link in left navigation should be navigated to profile page");
			registrySignedUserPage.navigateToSection("profile");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("btnApplyChanges"),
							registrySignedUserPage),
							"<b>Actual Result 2:</b> Clicking on Profile link in left navigation is navigated to profile page",
							"<b>Actual Result 2:</b> Clicking on Profile link in left navigation is not navigated to profile page",
							driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Clicking on 'Order History' link in left navigation should be navigated to profile page");
			registrySignedUserPage.navigateToSection("orderhistory");
			Log.assertThat(driver.getCurrentUrl().contains("/Order-History"),
					"<b>Actual Result 2:</b> Clicking on 'Order History' link in left navigation is navigated to 'Order History' page",
					"<b>Actual Result 2:</b> Clicking on 'Order History' link in left navigation is not navigated to 'Order History' page",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result3:</b> Clicking on 'Address Book' link in left navigation should be navigated to Address Book page");
			AddressBookPage addressBook = (AddressBookPage) registrySignedUserPage.navigateToSection("addressbook");
			Log.assertThat(addressBook.elementLayer.verifyPageElements(Arrays.asList("btnAddNewAddress"), addressBook),
					"<b>Actual Result 3:</b> Clicking on 'Address Book' link in left navigation is navigated to 'Address Book' page",
					"<b>Actual Result 3:</b> Clicking on 'Address Book' link in left navigation is not navigated to 'Address Book' page",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> Clicking on 'Payment Method' link in left navigation should be navigated to Payment Method page");
			PaymentMethodsPage paymentMethodsPage = (PaymentMethodsPage) registrySignedUserPage
					.navigateToSection("paymentmethods");
			Log.assertThat(
					paymentMethodsPage.elementLayer.verifyPageElements(Arrays.asList("btnAddPaymentMethod"),
							paymentMethodsPage),
							"<b>Actual Result 4:</b> Clicking on 'Payment Method' link in left navigation is navigated to 'Payment Method' page",
							"<b>Actual Result 4:</b> Clicking on 'Payment Method' link in left navigation is not navigated to 'Payment Method' page",
							driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 5:</b> Clicking on 'Registry' link in left navigation should be navigated to Registry page");
			registrySignedUserPage.navigateToSection("registry");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("btnFindRegistry"),
							registrySignedUserPage),
							"<b>Actual Result 5:</b> Clicking on 'Registry' link in left navigation is navigated to 'Registry' page",
							"<b>Actual Result 5:</b> Clicking on 'Registry' link in left navigation is not navigated to 'Registry' page",
							driver);

			/*
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 6:</b> Clicking on 'FAQ' link in left navigation should be navigated to FAQ page"
			 * ); registrySignedUserPage.navigateToSection("faq"); /*
			 * Log.assertThat(registrySignedUserPage.elementLayer.
			 * verifyPageElements ( Arrays.asList("btnFindRegistry"),
			 * registrySignedUserPage),
			 * "<b>Actual Result 6:</b> Clicking on 'FAQ' link in left navigation is navigated to 'FAQ' page"
			 * ,
			 * "<b>Actual Result 6:</b> Clicking on 'FAQ' link in left navigation is not navigated to 'FAQ' page"
			 * , driver);
			 */

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 6:</b> Clicking on 'Wish List' link in left navigation should be navigated to Wish List page");
			WishListPage wishListPage = (WishListPage) registrySignedUserPage.navigateToSection("wishlist");
			Log.assertThat(wishListPage.elementLayer.verifyPageElements(Arrays.asList("btnFindWishList"), wishListPage),
					"<b>Actual Result 6:</b> Clicking on 'Wish List' link in left navigation is navigated to 'Wish List' page",
					"<b>Actual Result 6:</b> Clicking on 'Wish List' link in left navigation is not navigated to 'Wish List' page",
					driver);

			/*
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 8:</b> Clicking on 'Belk Rewards Credit Card ' link in left navigation should be navigated to Belk Rewards Credit Card page"
			 * ); BelkRewardsCreditCardPage belkRewardsCreditCardPage =
			 * (BelkRewardsCreditCardPage) registrySignedUserPage
			 * .navigateToSection("belkrewardcreditcard"); /*
			 * Log.assertThat(belkRewardsCreditCardPage.elementLayer.
			 * verifyPageElements( Arrays.asList("btnFindWishList"),
			 * belkRewardsCreditCardPage),
			 * "<b>Actual Result 8:</b> Clicking on 'Belk Rewards Credit Card' link in left navigation is navigated to 'Belk Rewards Credit Card' page"
			 * ,
			 * "<b>Actual Result 8:</b> Clicking on 'Belk Rewards Credit Card' link in left navigation is not navigated to 'Belk Rewards Credit Card' page"
			 * , driver);
			 */

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 7:</b> Clicking on 'Email Preferences' link in left navigation should be navigated to Email Preferences page");

			Log.assertThat(driver.getCurrentUrl().contains("/Account-EditProfile"),
					"<b>Actual Result 7:</b> Clicking on 'Email Preferences' link in left navigation is navigated to 'Email Preferences' page",
					"<b>Actual Result 7:</b> Clicking on 'Email Preferences' link in left navigation is not navigated to 'Email Preferences' page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_085

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Account Help Content Asset is displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_086(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Account Help Content Asset should be displayed");

			String runPltfrm = Utils.getRunPlatForm();

			if (runPltfrm == "mobile") {
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("bannerHelpContentMobile"),
								registrySignedUserPage),
								"<b>Actual Result:</b> Account Help Content Asset is displayed",
								"<b>Actual Result:</b> Account Help Content Asset is not displayed", driver);
			} else {
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("bannerHelpContent"),
								registrySignedUserPage),
								"<b>Actual Result:</b> Account Help Content Asset is displayed",
								"<b>Actual Result:</b> Account Help Content Asset is not displayed", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_086

	@Test(groups = {
	"desktop" }, description = "Verify system allows the customer to print the Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_087(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm == "mobile") {
			throw new SkipException(
					"Verify system allows the customer to print the Registry page - works only on DESKTOP");
		}

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			registrySignedUserPage.clickOnPrintIcon();
			Log.message("6. Clicked on 'Print' icon");

			Log.message("7. System pop up cannot be handled by selenium !!");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_087

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user add items to their Registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_088(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			registrySignedUserPage.clickOnAddItemsToRegistry();
			Log.message("6. Clicked on add items to registry");

			Log.assertThat(driver.getCurrentUrl().contains("/home/"),
					"7. Navigate to home Page when Clicking on 'Add Items to Registry' button",
					"7. Not navigate to home Page when Clicking on 'Add Items to Registry' button");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("8. Searched with '" + searchKey + "' in the search text box");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("9. Navigated to 'Pdp' Page with randomly selected product");

			Log.message("10. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			String BrandName = pdpPage.getProductBrandName();
			String productName = pdpPage.getProductName();

			productName = BrandName.concat(" " + productName);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Clicked on 'Add To Registry' link in PDP");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Product should be added to the registry and success message should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblsuccessMessage"), pdpPage),
					"<b>Actual Result:</b> Product is added to the registry and success message should be displayed",
					"<b>Actual Result:</b> Product is not added to the registry and success message should be displayed");

			pdpPage.headers.navigateToMyAccount();
			Log.message("12. Clicked on 'My Account' link from PDP");

			myAccountPage.navigateToSection("registry");
			Log.message("13. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("14. Clicked on View link");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Product is added in PDP and displayed in registry should be same");

			Log.assertThat(registrySignedUserPage.verifyProductAddedInRegistry(productName),
					"<b>Actual Result 2:</b> Product is added in PDP and displayed in registry are same",
					"<b>Actual Result 2:</b> Product is added in PDP and displayed in registry are not same");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_088

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Pick Your Gift details are properly displayed in Cart page screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_275(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : " + searchKey);

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("4. Selected Size : " + size);
			Log.message("5. Selected Color : " + color);
			Log.message("6. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			String size2 = pdpPage.selectSize("XLarge");
			String color2 = pdpPage.selectColor();
			String qty2 = pdpPage.selectQuantity("1");
			Log.message("8. Selected Size2 : " + size2);
			Log.message("9. Selected Color2 : " + color2);
			Log.message("10. Selected Quantity2 : " + qty2);

			pdpPage.clickAddToBag();
			Log.message("11. Second Product added to the bag");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Free Gift Product selection modal should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("giftProductModal"), pdpPage),
					"<b>Actual Result:</b> Free Gift Product selection modal is displayed",
					"<b>Actual Result:</b> Free Gift Product selection modal is not displayed", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_275

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify qualified message is displayed once item is selected from My Bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_276(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : " + searchKey);

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("4. Selected Size : " + size);
			Log.message("5. Selected Color : " + color);
			Log.message("6. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			String size2 = pdpPage.selectSize();
			String color2 = pdpPage.selectColor();
			String qty2 = pdpPage.selectQuantity();
			Log.message("8. Selected Size2 : " + size2);
			Log.message("9. Selected Color2 : " + color2);
			Log.message("10. Selected Quantity2 : " + qty2);

			pdpPage.clickAddToBag();
			Log.message("11. Second Product added to Shopping Bag!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> 'Congratulations, you've qualified for a free gift!' label should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblGiftProduct"), pdpPage),
					"<b>Actual Result 1:</b> 'Congratulations, you've qualified for a free gift!' label is displayed",
					"<b>Actual Result 1:</b> 'Congratulations, you've qualified for a free gift!' label is not displayed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_276

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user able select /unselect the added gift from gift", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_281(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : " + searchKey);

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize("Small");
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity("1");
			Log.message("4. Selected Size1 : " + size);
			Log.message("5. Selected Color1 : " + color);
			Log.message("6. Selected Quantity1 : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			String size2 = pdpPage.selectSize("XLarge");
			String color2 = pdpPage.selectColor();
			String qty2 = pdpPage.selectQuantity("1");
			Log.message("8. Selected Size2 : " + size2);
			Log.message("9. Selected Color2 : " + color2);
			Log.message("10. Selected Quantity2 : " + qty2);

			pdpPage.clickAddToBag();
			Log.message("11. Second product is added to the bag");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> 'Congratulations, you've qualified for a free gift!' label should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblGiftProduct"), pdpPage),
					"<b>Actual Result 1:</b> 'Congratulations, you've qualified for a free gift!' label is displayed",
					"<b>Actual Result 1:</b> 'Congratulations, you've qualified for a free gift!' label is not displayed",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Select button should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnGiftSelect"), pdpPage),
					"<b>Actual Result 2:</b> Select button is displayed",
					"<b>Actual Result 2:</b> Select button is not displayed", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> 'No, thanks. I'm not interested' should be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblNoThanks"), pdpPage),
					"<b>Actual Result 3:</b> 'No, thanks. I'm not interested' label is displayed",
					"<b>Actual Result 3:</b> 'No, thanks. I'm not interested' label is not displayed", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_281

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Selected gift is applied to the shopping Bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_282(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : " + searchKey);
			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize("Small");
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("4. Selected Size1 : " + size);
			Log.message("5. Selected Color1 : " + color);
			Log.message("6. Selected Quantity1 : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			String size2 = pdpPage.selectSize("XLarge");
			String color2 = pdpPage.selectColor();
			String qty2 = pdpPage.selectQuantity("1");
			Log.message("8. Selected Size2 : " + size2);
			Log.message("9. Selected Color2 : " + color2);
			Log.message("10. Selected Quantity2 : " + qty2);

			pdpPage.clickAddToBag();
			Log.message("11. Second Product added to the bag");

			pdpPage.clickSelectGiftProduct();
			Log.message("12. Selected Gift Product");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("13. Navigated to 'Mini Cart' !");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Free Gift Purchase message should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lblFreeGiftMessage"),
							shoppingBagPage),
							"<b>Actual Result 1:</b> Free Gift Purchase message is displayed",
							"<b>Actual Result 1:</b> Free Gift Purchase message is not displayed", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Free Gift Product price should not have billing price");

			Log.assertThat((shoppingBagPage.getSelectedGiftProductPrice().equals("$0.00")),
					"<b>Actual Result 2:</b> Free Gift Product price does not have billing price",
					"<b>Actual Result 2:</b> Free Gift Product price have billing price", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_282

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify gift card screen closed while clicking on Close button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_284(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> registry = Arrays.asList("freeGiftPopUp");
		List<String> registry1 = Arrays.asList("myBagHeader");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize("Small");
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity("2");
			Log.message("4. Selected Size : " + size);
			Log.message("5. Selected Color : " + color);
			Log.message("6. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			pdpPage.clickOnTocloseInPopUp();
			Log.message("7. Selected Gift Product");

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Verify 'gift card' screen closed while clicking on Close button");
			Log.assertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(registry, pdpPage),
					"<b>Actual Result:</b> 'Gift card screen is closed while clicking on Close button",
					"<b>Actual Result:</b> 'Gift card screen is not closed while clicking on Close button", driver);

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to 'Mini Cart' !");

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Verify 'gift card' screen closed while clicking on Close button");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(registry1, shoppingBagPage),
					"<b>Actual Result:</b> 'Gift card screen is closed while clicking on Close button",
					"<b>Actual Result:</b> 'Gift card screen is not closed while clicking on Close button", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_284

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify No Thanks message is shown in PYG screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_285(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> registry = Arrays.asList("freeGiftPopUp");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize("Small");
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity("2");
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("4. Product added to Shopping Bag!");
			pdpPage.clickOnNoThankMnotInterested();
			Log.message("5. Selected Gift Product");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result</b> No Thanks message should be shown use either option to dismiss the PYG selection");
			Log.assertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(registry, pdpPage),
					"<b>Actual Result:</b> No Thanks message dismiss the PYG selection",
					"<b>Actual Result:</b> No Thanks message do not dismiss the PYG selection", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_285

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message is shown for not qualified product", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_287(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			String selectedColor = pdpPage.selectColor();
			Log.message("3. Selected color: '" + selectedColor + "' from color swatch in the PDP page");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from size drop down in the PDP page");

			String promotionMessage = pdpPage.getPromotionMessage();
			Log.message("5. Get Prodmotion message: " + promotionMessage + "' in the pdp page");

			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button in the pdp Page");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigate to Bag Page ");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>" + promotionMessage
					+ " Promotion message should shown both 'PDP' Page and 'Shopping' page");

			Log.assertThat(shoppingBagPage.getPromotionMessage().contains(promotionMessage),
					"<b>Actual Result:</b> Shopping bag Promotion: '" + shoppingBagPage.getPromotionMessage()
					+ "' PDP page Promotion: '" + promotionMessage + "' are same",
					"<b>Actual Result:</b> Shopping bag Promotion: '" + shoppingBagPage.getPromotionMessage()
					+ "' PDP page Promotion: '" + promotionMessage + "' are not same",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_287

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Add Your Free Gift' message is shown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_288(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String promotion = "Add Your Free Gift";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			String selectedColor = pdpPage.selectColor();
			Log.message("3. Selected color: '" + selectedColor + "' from color swatch in the PDP page");

			String selectedSize = pdpPage.selectSize("XLarge");
			Log.message("4. Selected size: '" + selectedSize + "' from size drop down in the PDP page");

			String selectedQuantity = pdpPage.selectQuantity("2");
			Log.message("5. Selected Quantity: '" + selectedQuantity + "' from Quantity drop down in the PDP page");

			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button in the pdp Page");

			pdpPage.closeBonusOffer();
			Log.message("7. Clicked 'X' in the Bonus offer popup");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigate to Bag Page ");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> Add Your Free Gift text should shown in the Bag Page");

			Log.assertThat(shoppingBagPage.getPromotionPageLinl().contains(promotion),
					"<b>Actual Result:</b>" + shoppingBagPage.getPromotionMessage()
					+ " is shown in the Bag Page, when user added Qualifed product",
					"<b>Actual Result:</b>" + shoppingBagPage.getPromotionMessage()
					+ " is not shown in the Bag Page when user, is not added Qualifed product",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_288

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Add Your Free Gift' message is shown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_286(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			String selectedColor = pdpPage.selectColor();
			Log.message("3. Selected color: '" + selectedColor + "' from color swatch in the PDP page");

			String selectedSize = pdpPage.selectSize("XLarge");
			Log.message("4. Selected size: '" + selectedSize + "' from size drop down in the PDP page");

			String selectedQuantity = pdpPage.selectQuantity("2");
			Log.message("5. Selected Quantity: '" + selectedQuantity + "' from Quantity drop down in the PDP page");

			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button in the pdp Page");

			String bonusProductName = pdpPage.selectBonusProduct();
			Log.message("7. Selected Bonus ProductName: '" + bonusProductName);

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigate to Bag Page ");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> Selected Bonus offer product and Promotion message should shown in the 'Bag' Page");

			int size = shoppingBagPage.getPromotionProductDetails().size();
			for (int i = 0; i < size; i++) {
				Log.softAssertThat(
						bonusProductName.split(" ")[0]
								.contains(shoppingBagPage.getProductDetails().get(i).get("ProductName").split(" ")[0]),
								"<b>Actual Result:</b>Selected Promotion product name is shown in the Bag page",
								"<b>Actual Result:</b>Selected Promotion product name is not shown in the Bag page", driver);
				if (shoppingBagPage.getProductDetails().get(i).get("Subtotal").split("\\$")[1].trim()
						.contains("0.00")) {
					Log.assertThat(
							shoppingBagPage.getProductDetails().get(i).get("Subtotal").split("\\$")[1].contains("0.00"),
							"Selected  Gift item cart is displaying as $0.00.",
							"Selected  Gift item cart is not displaying as $0.00.", driver);
				}
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_286

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry Banner Content Slot displayedin 'Empty Registry'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_071(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Click on 'View link' in the Gift registry page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>Registry Banner Content Slot should displayed below 'Empty Registry' ");

			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("registryBanner"), giftRegistryPage),
					"<b>Actaul Result:</b> Registry Banner Content Slot is displayed below 'Empty Registry' ",
					"<b>Actaul Result:</b> Registry Banner Content Slot is displayed below 'Empty Registry'", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_071

	@Test(groups = {
	"desktop" }, description = "Verify Print Registry screen is opened while clicking on Print icon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_065(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message(i++ + ". Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			String upc = pdpPage.getUPCValue();
			Log.message(i++ + ".  Get the upc: '" + upc + "' in the pdp page");

			String price = pdpPage.getProductPrice();
			Log.message(i++ + ". Get the productPrice: '" + price + "' in the pdp page");

			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Sucessfully added product to Registry");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page");

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked view link button in the gift registry page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Print' button should shown in the Gift registry page");

			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("iconPrint"), giftRegistryPage),
					"<b>Actual Result: </b>'Print' is display in the 'Gift Registry' Page",
					"<b>Actual Result: </b>'Print' is not display in the 'Gift Registry' Page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_065

	@Test(groups = {
	"desktop" }, description = "Verify “Add Gift Cards” link direct the user to the Gift Card PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_069(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked 'View link' in the Gift registry page");

			PdpPage pdpPage = giftRegistryPage.clickOnAddToGiftCardButton();
			Log.message(i++ + ". Clicked 'Add to gift Card' button in the Gift registry page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>By clicking 'Add to giftcard button', It should redirect to 'gift card PDP' Page ");

			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "belk-gift-card") && pdpPage != null,
					"<b>Actaul Result:</b> Navigate to correct URL: " + driver.getCurrentUrl(),
					"<b>Actaul Result:</b> Navigated to wrong URL: " + driver.getCurrentUrl(), driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_069

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify “Add Items to Registry” link direct the user to the Homepage", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_070(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked 'View link' in the Gift registry page");

			homePage = giftRegistryPage.clickOnAddItemToRegistryButton();
			Log.message(i++ + ". Clicked 'Add to item Registry' button in the Gift registry page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>By clicking 'Add to Item Registry button', It should redirect to 'Home' Page ");

			Log.assertThat(driver.getCurrentUrl().contains("home"),
					"<b>Actaul Result:</b> Navigate to correct URL: " + driver.getCurrentUrl(),
					"<b>Actaul Result:</b> Navigated to wrong URL: " + driver.getCurrentUrl(), driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_070

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify scroll down displays under the Saved Address drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_140(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.clickOnSavedAddress();
			Log.message("8. Clicked on saved Address");

			Log.message("<b>Expected Result :</b> Scroll Down Should be present for saved address");
			Log.assertThat(regSignedUser.verifyDropdown(),
					"<b>Actual Result :</b> Scroll Down is present for saved address",
					"<b>Actual Result :</b> Scroll Down is not present for saved address");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	} // TC_BELK_Registry_140

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify tool tip displays for the APO FPO and phone", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_141(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");

			Log.message("<b>Expected Result 1:</b> Tool tip with content for APO FPO should be opened");
			Log.assertThat(regSignedUser.MouseoverAPO(),
					"<b>Actual Result 1:</b> Tool tip with content for APO FPO is opened",
					"<Actual Result 1:</b> Tool tip with content for APO FPO is not opened");

			Log.message("<b>Expected Result 2:</b> Tool tip with content for Phone Number should be opened");
			Log.assertThat(regSignedUser.MouseoverPhoneNumber(),
					"<b>Actual Result 2:</b> Tool tip with content for Phone Number is opened",
					"<b>Actual Result 2:</b> Tool tip with content for Phone Number is not opened");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	} // TC_BELK_Registry_141

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Post event Shipping heading displays", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_142(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");

			Log.message(
					"<b>Expected Result :</b> Post Event heading should be displayed in step 2 while creating new registry");
			Log.assertThat(regSignedUser.verifyPostEvent(),
					"<b>Actual Result :</b> Post Event heading is displayed in step 2 while creating new registry",
					"<b>Actual Result :</b> Post Event heading is not displayed in step 2 while creating new registry");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_Registry_142

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify resource message displays under the heading of Post-Event shipping.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_143(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");

			Log.message(
					"<b>Expected Result :</b> This address is where your registry gifts will be sent after your event Message should be displayed below Post event shipping address ");
			Log.assertThat(regSignedUser.verifyPostEventMessage(),
					"<b>Actual Result :</b> This address is where your registry gifts will be sent after your event Message is displayed below Post event shipping address ",
					"<b>Actual Result :</b> This address is where your registry gifts will be sent after your event Message is not displayed below Post event shipping address ");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_Registry_143

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Previous and Cancel button in the Step 2 Shipping Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_144(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			LinkedHashMap<String, String> Values = regSignedUser.getValuesInShipping();
			regSignedUser.clickOnPrevious();
			regSignedUser.clickContinueInCreateRegistry();
			Log.message(
					"<b>Expected Result 1:</b> The details enetered in Step 2 should not be retained when we click on previous");
			Log.assertThat(!(regSignedUser.getValuesInShipping()).equals(Values),
					"<b>Actual Result 1:</b> The details enetered in Step 2 is not retained when we click on previous",
					"<b>Actual Result 1:</b> The details enetered in Step 2 is retained when we click on previous");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("9. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message(
					"<b>Expected Result 2:</b> The Page should be navigated to Create Registry  Step 3 Event Details.");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("step3CreateRegistry"), regSignedUser),
					"<b>Actual Result 2:</b> The Page is navigated to Create Registry  Step 3 Event Details.",
					"<b>Actual Result 2:</b> The Page is not navigated to Create Registry  Step 3 Event Details.");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_144

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify customer navigates to Create Registry  Step 3 Event Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_145(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");

			Log.message("<b>Expected Result :</b> The Page should be navigated to Create Registry - Step 3 of 3");

			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("step3CreateRegistry"), regSignedUser),
					"<b>Actual Result :</b> The Page is navigated to Create Registry - Step 3 of 3",
					"<b>Actual Result :</b> The Page is not navigated to Create Registry - Step 3 of 3");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_145

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays the resource message", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_146(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");

			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("step3CreateRegistry"), regSignedUser),
					"10. The Page is navigated to Create Registry - Step 3 of 3",
					"10. The Page is not navigated to Create Registry - Step 3 of 3");

			Log.message(
					"<b>Expected Result :</b> To complete the process of creating your registry, please review the information below and click Submit-Resource Message should be displayed");

			Log.assertThat(regSignedUser.verifyMessage(),
					"<b>Actual Result :</b> To complete the process of creating your registry, please review the information below and click Submit-Resource Message is displayed",
					"<b>Actual Result :</b> To complete the process of creating your registry, please review the information below and click Submit-Resource Message is not displayed");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_146

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Terms and Condition check box in the Event details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_148(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String phoneNumber= testData.get("EditData");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(username,
					passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers
					.navigateToGiftRegistry();
			Log.message("4.Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage
					.clickOnCreateRegistrybutton();
			Log.message("5.Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6.Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7.clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8.Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9.Clicked on Continue in step 2");
			Log.message("<b>Expected Result 1:The Terms and Conditions Checkbox should be available");
			Log.assertThat(
					regSignedUser.verifycheckTermsAndConditionlabel(),
					"<b>Actual Result 1:</b>The Terms and Conditions Checkbox is available",
					"<b>Actual Result 1:</b>The Terms and Conditions Checkbox is not available");

			Log.message("<b>Expected Result 2:The Terms and Conditions Checkbox should not be selected by default");
			Log.assertThat(
					regSignedUser.verifycheckTermsAndConditionCheckbox(),
					"<b>Actual Result 2:</b>The Terms and Conditions Checkbox is not selected by default",
					"<b>Actual Result 2:</b>The Terms and Conditions Checkbox is selected by default");
			regSignedUser.clickOnSubmitButtonwithoutTerms();

			Log.message("<b>Expected Result 3:The error message should be displayed as You must agree to the terms and conditions before creating a registry. Please check the box above to continue.");
			Log.assertThat(
					regSignedUser.verifyWarningMessageForNoTermsAndCondition(),
					"<b>Actual Result 3:</b>The error message is displayed as You must agree to the terms and conditions before creating a registry. Please check the box above to continue.",
					"<b>Actual Result 3:</b>The error message is not displayed as You must agree to the terms and conditions before creating a registry. Please check the box above to continue.");
			myAccountPage.navigateToSection("emailpreferences");
			EmailPreferencesPage emailPreference = new EmailPreferencesPage(driver).get();
			emailPreference.EditPhoneInEmailPreference(phoneNumber);
			Log.message("10.Entered the new phone Number");
			emailPreference.enterEmailandPwd(username, passwd);

			Log.message("<b>Expected Result 4:The New Deatils should be updated through Email Preference");
			Log.assertThat(
					myAccountPage.elementLayer.verifyPageElements(
							Arrays.asList("divMyAccount"),
							myAccountPage),
							"<b>Actual Result 4:</b>The New Deatils are updated through Email Preference",
							"<b>Actual Result 4:</b>The New Deatils is not updated through Email Preference",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_REGISTRY_148


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify error message displays for Terms and Conditions checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_149(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");

			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("step3CreateRegistry"), regSignedUser),
					"10. The Page is navigated to Create Registry - Step 3 of 3",
					"10. The Page is not navigated to Create Registry - Step 3 of 3");
			regSignedUser.ClickonSubmitToverifyErrorMsgInStep3();

			Log.message(
					"<b>Expected Result :</b> You must agree to the terms and conditions before creating a registry. Please check the box above to continue Error Message should be displayed");

			Log.assertThat(regSignedUser.verifyErrorMessage(),
					"<b>Actual Result :</b> You must agree to the terms and conditions before creating a registry. Please check the box above to continue Error Message is displayed",
					"<b>Actual Result :</b> You must agree to the terms and conditions before creating a registry. Please check the box above to continue Error Message is not displayed");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_149

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify customer navigates to 'Create Registry: Step 2 Shipping' page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_127(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page

			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			registrySignedUserPage.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			Log.message("5. Filled the Registry Event Details");
			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Continue button on the Event Page");

			String createRegistryHeadingStep2 = registrySignedUserPage.preShippingCreateRegistryHeading();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: <b> Create Registry page should be displayed with the heading 'Create Registry- Step 2 of 3'.");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtRegistryStep2"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> Create Registry Step 2 On the Pre Shipping page is displayed and the heading is   :"
									+ createRegistryHeadingStep2,
									"<b> Actual Result 1: </b> The Create Registry Heading is not displayed on the Pre Shipping shipping page.",
									driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_127

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify resource message displays under the heading of 'Pre-Event shipping'.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_128(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page

			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			registrySignedUserPage.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			Log.message("5. Filled the Registry Event Details");
			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Continue button on the Event Page");
			String resourceMessage = registrySignedUserPage.preShippingText();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: <b>  Resources message 'This address is where your registry gifts will be sent before your event. If your shipping address will be changing post-event, please provide that address as wel' should be displayed under the heading of 'Pre-Event Shipping'");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtResourceMessageOnPreShipping"), registrySignedUserPage),
							"<b> Actual Result 1: </b> The Resource Message is displayed and the message is  :"
									+ resourceMessage,
									"<b> Actual Result 1: </b> The Resource message is not displayed on the Pre Event Shipping.",
									driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_128

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify customer navigates to Create Registry page..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_121(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			registrySignedUserPage.txtregistryCreateHeading();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> Create Registry page should be displayed with the heading 'Create Registry-Step 1 of 3 '");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtCreateNewRegistry"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> The Heading is displayed and the heading is :" + "registryHeading",
					"<b> Actual Result 1: </b> The Heading is not displayed ");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_121

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify success messge display above the Add to Registry link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_115(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign in Page");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigated to PdpPage using the search key : " + searchKey);

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("5. Selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("6. Selected size :" + size);

			pdpPage.clickOnRegistrySign();
			Log.message("7. Clicked on Add to Registry link");
			String successTxtMessageofProduct = pdpPage.registryLoginAddSuccessMessage();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1: </b> The item should be added to the Registry and success message should be displayed above the Add to Registry link, if the customer is logged in and has a single registry.</h1>");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSuccessMessage"), pdpPage),
					"<b> Actual Result 1:</b> Success message is: " + successTxtMessageofProduct
					+ " if the customer is logged ",
					"<b> Actual Result 1:</b> Success message is not displayed above the Add to Registry link, if the customer is not logged",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_115

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system navigate to Create Registry page for authorised user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_114(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign in Page");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigated to PdpPage using the search key : " + searchKey);

			// Load the PDP Page with search keyword

			String color = pdpPage.selectColor();
			Log.message("5. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("6. selected size :" + size);

			RegistrySignedUserPage registersign = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("7. Clicked on Registry link");

			String registryPageUrl = driver.getCurrentUrl();
			Log.message("8. Navigate to the Create Account 'Registry' Page the URL is: " + registryPageUrl);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> System should direct to Create Registry page, If the customer is logged in, but they do not have an activeregistry");
			Log.assertThat(
					registersign.elementLayer.verifyPageElements(Arrays.asList("txtCreateRegistryStep1"), registersign),
					"<b> Acual Result 1: </b> Navigated to the Create Registry page as a login User",
					"<b> Actual Result 1: </b> Not able to Navigate to the Create Registry Page as a login User",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_114

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system navigates to Login page for unauthenticated user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_113(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("3. Select product from search result page!");

			String color = pdpPage.selectColor();
			Log.message("4. Selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("5. Selected size :" + size);
			pdpPage.navigateToRegistryAsGuest();
			Log.message("6. Clicked on Registry link");

			String registryPageUrl = driver.getCurrentUrl();
			Log.message("7. The Registry Page URL is: " + registryPageUrl);

			if (Utils.getRunPlatForm() == "desktop") {

				Log.message("<br>");
				Log.message(
						"<b> Expected Result 1: </b> Registry Login page should be displayed if the customer is not logged");

				Log.assertThat(registryPageUrl.contains("giftregistry"),
						"<b> Acual Result 1: </b> Registry Login Page is displayed after clicked on the Add To registry link",
						"<b> Actual Result 1: </b> Registry Login Page is not displayed after clicked on the Add To registry link",
						driver);
			}
			if (Utils.getRunPlatForm() == "mobile") {

				Log.message("<br>");
				Log.message(
						"<b> Expected Result 1: </b> Registry Login page should be displayed if the customer is not logged");
				Log.assertThat(registryPageUrl.contains("giftregistry"),
						"<b> Acual Result 1: </b> Registry Login Page is displayed after clicked on the Add To registry link",
						"<b> Actual Result 1: </b> Registry Login Page is not  displayed after clicked on the Add To registry link",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_113

	@Test(groups = {
	"desktop" }, description = "Verify Add To Shopping Bag Button for an out of stock product in the Wishlist Result public view.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_329(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String firstnamewishlist = testData.get("FirstName");
		String enterLastName = testData.get("LastName");
		String emailid = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			WishListPage wishlistLogin = signIn.detailsOfWishlistForm(firstnamewishlist, enterLastName, emailid);
			Log.message("3. Login to the Sign in Wishlist");
			Log.message("<br>");
			Log.message("<b> Expected Result 1: Print button should be present for Desktop");

			Log.assertThat(
					wishlistLogin.elementLayer.verifyPageElements(Arrays.asList("lnkPrintOption"), wishlistLogin),
					"<b> Actual Result 1: </b> Print button is present in the Wishlist Page",
					"<b> Actual Result 2: </b>Print button is not present in the Wishlist Page", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_329

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the functionality of Contiue button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_125(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Login to the Sign in registry");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the Create New Registry");
			registrySignedUserPage.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			Log.message("5. Fill the Registry Form to create the Profile ", driver);
			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Continue in the registry as a login user");
			String preEventShippingURl = driver.getCurrentUrl();
			Log.message("7. The Pre Event Shipping URL is:" + preEventShippingURl);
			String preEventHeading = registrySignedUserPage.preEventShippingText();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> The event information should be saved to the registry and the customer should be directed to Create Registry: Step 2 Shipping.");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtPreEventShippingPage"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> Filling the Event information and Navigated to the Step: "
									+ preEventHeading,
									"<b> Actual Result 1: </b> Event information form not filled  and  not Navigated to the Step 2 ",
									driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_125

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the functionality of Cancel button..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_124(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			driver.getCurrentUrl();
			registrySignedUserPage.clickCancelInCreateRegistry();
			Log.message("5. Click on the cancel button in the Create New Registry");
			driver.getCurrentUrl();
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> Customer should be redirected to the My Registry page.");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtMyRegistry"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> Customer Navigated to the My Registry Page",
							"<b> Actual Result 1: </b> Customer not Navigated to the My Registry Page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_124

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify details under Event information heading.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_122(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			Log.message("5. Event information heading should be displayed with the following details");
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> Event Type heading should be displayed");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtEventTypeOnEventForm"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> Event type heading is displayed in the Event heading form ",
							"<b> Actual Result 1: </b> Event type heading is not displayed in the Event heading form", driver);

			registrySignedUserPage.drpGetTxtListValue();

			String labelRegistryValue = registrySignedUserPage.getAlltheDropDownValues();
			Log.message("<br>");
			Log.message("<b> Expected Result 2: </b> Drop down should include All Registry type.");
			Log.message("<b> Actual Result 2: </b> The Registry Options are: <br>" + labelRegistryValue, driver);
			registrySignedUserPage.drpGetTxtListValue();
			Log.message("<br>");
			Log.message("<b> Expected Result 3: </b> Event Name and text box should be displayed in the Event Form");
			Log.assertThat(
					registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("txtEventName", "txtEventDate"), registrySignedUserPage),
					"<b> Actual Result 3: </b> Event Name ,Event Textbox and Event Date heading is displayed in the Event Information Form. ",
					"<b> Actual Result 3: </b> Event Name, Event Textbox and Event Date heading  is not displayed in the Event Information Form.",
					driver);
			Log.message("<br>");
			Log.message("<b> Expected Result 4: </b> City and text box should be dispalyed in the Event Form");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(
					Arrays.asList("txtEventCity", "txtBoxEventCity", "txtHeadingState"), registrySignedUserPage),
					"<b> Actual Result 4: </b> City  heading and textbox is displayed in the Event Information Form. ",
					"<b> Actual Result 4: </b> City  heading and textbox heading  is not displayed in the Event Information Form.",
					driver);
			registrySignedUserPage.drpStateBox();
			String labelRegistrySateValue = registrySignedUserPage.getAlltheDropDownStateValues();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 5: </b> State, Drop down should be displayed with all state and APO/FPO values");
			Log.message(
					"<b> Actual Result 5: </b> State, Drop down should be displayed with all state and APO/FPO values and the values are"
							+ labelRegistrySateValue,
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
		driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_122

	@Test(groups = {
	"desktop" }, description = "Verify Account help text is displayed below unregistered Account Navigation content assest", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_006(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm == "mobile") {
			throw new SkipException("This testcase is not applicable for '" + runPltfrm + "'");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			// Verifying the account help text panel in registry page
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Account Help text panel should be displayed below unregistered account navigation content asset should be displayed in Registry screen.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("txtAccountNavUnregpanel"), registryPage)
					&& registryPage.elementLayer.verifyPageElements(Arrays.asList("txtAccounthelpcontent"),
							registryPage),
							"<b>Actual Result 1:</b> Account Help text and unregistered account navigation content asset are displayed in Registry screen.",
							"<b>Actual Result 1:</b> Account Help text and unregistered account navigation content asset are not displayed in Registry screen.",
							driver);
			Log.message("<br>");
			// clicking 'Signin' Button and Navigating to SignIn Page
			SignIn signinpage = homePage.headers.navigateToSignIn();
			Log.message("3. Clicked on 'SignIn' button on header and Navigated to 'SignIn' Page!");

			// Verifying the account help text panel in 'Sign' page
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Account Help text panel should be displayed below unregistered account navigation content asset should be displayed in Sign In screen.");
			Log.assertThat(
					signinpage.elementLayer.verifyPageElements(Arrays.asList("txtAccountNavUnregpanel"),
							signinpage)
							&& signinpage.elementLayer.verifyPageElements(Arrays.asList("txtAccounthelpcontent"), signinpage),
							"<b>Actual Result 2:</b> Account Help text and unregistered account navigation content asset are displayed in Sign In screen.",
							"<b>Actual Result 2:</b> Account Help text and unregistered account navigation content asset are not displayed in Sign In screen.",
							driver);
			Log.message("<br>");
			// clicking 'WishList' Button and Navigating to WishList Page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("4. Clicked on 'WishList' button on header and Navigated to 'WishList' Page!");

			// Verifying the account help text panel in WishList page
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Account Help text panel should be displayed below unregistered account navigation content asset should be displayed in WishList screen.");
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("txtAccountNavUnregpanel"), wishlistpage)
					&& wishlistpage.elementLayer.verifyPageElements(Arrays.asList("txtAccounthelpcontent"),
							wishlistpage),
							"<b>Actual Result 3:</b> Account Help text and unregistered account navigation content asset are displayed in WishList screen.",
							"<b>Actual Result 3:</b> Account Help text and unregistered account navigation content asset are not displayed in WishList screen.",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_006

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Email validation Error message is shown when user tries to Login with invalid email id", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_009(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String EmailInvalidErrorMsg = "We're sorry, but this email address is not in the proper format: (EXAMPLE: email_user@email.com). Please re-enter your email address.";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering Invalid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered invalid EmailId: '" + emailid + "' in the Email Field.");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered Password: '" + password + "' in the Password Field.");
			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on 'SignIn' Button.");
			// Verifying Email Validation Error Message
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Email validation error message should be displayed below the email field, when user tries to login with invalid email id.");
			Log.assertThat(registryPage.getEmailErrorMsg().contains(EmailInvalidErrorMsg),
					"<b>Actual Result 1:</b> Email validation error message is displayed below the email field, when user tries to login with invalid email id.",
					"<b>Actual Result 1:</b> Email validation error message is not displayed below the email field, when user tries to login with invalid email id.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_009

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user can view their Registry by logging to the Belk site using valid credentials", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_007(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid EmailId: '" + emailid + "' in the Email Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered Password: '" + password + "' in the Password Field");
			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on SignIn Button");
			// Verifying Page Navigated To My registry Page
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> The page should be navigated to the  My Registry View-> My Registries page.");
			Log.assertThat((driver.getCurrentUrl().contains("GiftRegistry-Start")),
					"<b>Actual Result 1:</b> The page is navigated to the  My Registry View-> My Registries page.",
					"<b><b>Actual Result 1:</b> The page is not navigated to the My Registry View-> My Registries page.",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> 'Find Someone's Gift Registry' section should be displayed in the Regsitry Page.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("titleFindSomeoneregistry"),
							registryPage)
							&& registryPage.elementLayer.verifyPageElements(Arrays.asList("fldFindSomeoneregistry"),
									registryPage),
									"<b>Actual Result 2:</b> 'Find Someone's Gift Registry section' is  displayed in the Regsitry Page.",
									"<b>Actual Result 2:</b> 'Find Someone's Gift Registry section' is not displayed in the Regsitry Page.",
									driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> If there is any added registry, then it should display the section 'Your Registries' in the Registry Page.");
			Log.assertThat(registryPage.elementLayer.verifyPageElements(Arrays.asList("fldYourRegistry"), registryPage),
					"<b>Actual Result 3:</b> 'Your Registries' section is displayed in the Regsitry Page.",
					"<b>Actual Result 3:</b> 'Your Registries' section is not in the Regsitry Page.", driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> 'Create a New Registry' section should be displayed in the Registry Page.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("btnCreateNewRegistry"), registryPage),
					"<b>Actual Result 4:</b> 'Create a New Registry' section is displayed in the Registry Page.",
					"<b>Actual Result 4:</b> 'Create a New Registry' section is not displayed in the Registry Page.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_007

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Page redirected to Create Account page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_012(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Clicking Create NEW Account button
			CreateAccountPage createAccountPage = registryPage.clickCreateAccount();
			Log.message(
					"3. Clicked on 'Create New Account' button on header and Navigated to 'Create New Account' Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> The page should be navigated to the Create Account Page.");
			Log.assertThat((driver.getCurrentUrl().contains("Account-StartRegister")),
					"<b>Actual Result 1:</b> The page is navigated to the Create Account Page.",
					"<b>Actual Result 1:</b> The page is not navigated to the Create Account Page.", driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> 'First Name', 'Last Name', 'Phone Number', 'Email', 'Confirm Email', 'Password(6 - 20 characters)' and 'Confirm Password' fields sholud be displayed in the Create Account Page.");
			Log.assertThat(
					createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtFirstName"), createAccountPage)
					&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtLastName"),
							createAccountPage)
							&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtPhoneNumber"),
									createAccountPage)
									&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtEmail"), createAccountPage)
									&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtEmailConfirmation"),
											createAccountPage)
											&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtPassword"),
													createAccountPage)
													&& createAccountPage.elementLayer.verifyPageElements(Arrays.asList("txtConfirmPassword"),
															createAccountPage),
															"<b>Actual Result 2:</b> 'First Name', 'Last Name', 'Phone Number', 'Email', 'Confirm Email', 'Password(6 - 20 characters)' and 'Confirm Password' fields is displayed in the Create Account Page.",
															"<b>Actual Result 2:</b> 'First Name', 'Last Name', 'Phone Number', 'Email', 'Confirm Email', 'Password(6 - 20 characters)' and 'Confirm Password' fields is not displayed in the Create Account Page.",
															driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_012

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry Benefits Content Asset displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_015(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String giftRegistryContentAsset = "Create Gift Registry Benefits Content";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Registry Benefits Content Asset should be displayed below New Customer Pane in the Registry Page.");
			Log.assertThat(
					registryPage.getTextFromNewCustomer().contains(giftRegistryContentAsset)
					&& registryPage.elementLayer
					.verifyPageElements(Arrays.asList("txtRegistryBenefitContentAsset"), registryPage),
					"<b>Actual Result :</b> Registry Benefits Content Asset is displayed in the Registry Page.",
					"<b>Actual Result :</b> Registry Benefits Content Asset is not displayed in the Registry Page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_015

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user is allowed to search with Advanced Search link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_014(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Verifying Advanced Search Link is displayed
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Advanced Search link should be displayed below the Event Type field.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("lnkAdvancedSearchInFindRegistry"),
							registryPage),
							"<b>Actual Result 1:</b> Advanced Search link is displayed below the Event Type field.",
							"<b>Actual Result 1:</b> Advanced Search link is not displayed below the Event Type field.",
							driver);
			// clicking Advanced search Link
			registryPage.clickOnAdvancedSearchLnk();
			Log.message("3. Clicked on 'Advanced Search' link in the Registry Page.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After clicking Advanced Search link, the following hidden nonrequired search fields 'City', 'State', 'Event Date', 'Registry ID' should be displayed.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("fldCityInFindRegistry"), registryPage)
					&& registryPage.elementLayer.verifyPageElements(Arrays.asList("selectStateInFindRegistry"),
							registryPage)
							&& registryPage.elementLayer.verifyPageElements(Arrays.asList("fldEventDateinAdvFindRegistry"),
									registryPage)
									&& registryPage.elementLayer.verifyPageElements(Arrays.asList("fldRegistryIdinAdvFindRegistry"),
											registryPage),
											"<b>Actual Result 2:</b> After clicking Advanced Search link, the following hidden nonrequired search fields 'City', 'State', 'Event Date', 'Registry ID' is displayed.",
											"<b>Actual Result 2:</b> After clicking Advanced Search link, the following hidden nonrequired search fields 'City', 'State', 'Event Date', 'Registry ID' is not displayed.",
											driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Basic Search link should be displayed below Registry ID(optional)field.");
			Log.assertThat(registryPage.elementLayer.verifyPageElements(Arrays.asList("lnkSimpleSearchInFindRegistry"),
					registryPage),

					"<b>Actual Result 3:</b> Basic Search link is displayed below Registry ID(optional)field.",
					"<b>Actual Result 3:</b> Basic Search link is not displayed below Registry ID(optional)field.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_014

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Reset Password window is prompted when clicking on Forgot Password link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_010(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String txtresetpasswordSuccessmsg = "Reset Password Email Sent";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// clicking forgot password link
			registryPage.clikForgotPwd();
			Log.message("3. Clicked on 'Forgot Password' Link.");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Password recovery window should be opened.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("passwordRecoveryPage"), registryPage),
					"<b>Actual Result 1:</b> Password recovery window is opened.",
					"<b>Actual Result 1:</b> Password recovery window is not opened.", driver);
			Log.message("<br>");
			registryPage.clickCancel();
			Log.message("4. Clicked on 'Cancel' Button in Password Recovery window.");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After clicking 'Cancel' button email should not be sent and reset password screen should be closed.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("passwordRecoveryPage"),
							registryPage),
							"<b>Actual Result 2:</b> After clicking 'Cancel' button, email is not sent and reset password screen is closed.",
							"<b>Actual Result 2:</b> After clicking 'Cancel' button, email is sent and reset password screen is not closed.",
							driver);
			Log.message("<br>");
			// Entering Valid Email ID
			registryPage.enterEmailID(emailid);
			Log.message("5. Entered valid EmailId: '" + emailid + "' in the Email Field.");
			// Clicking forgot password Link
			registryPage.clikForgotPwd();
			Log.message("6. Clicked on 'Forgot Password' Link.");
			// entering emailid in password recovery page
			registryPage.enterEmailIDInPasswordRecoveryPage(emailid);
			Log.message("7. Entered valid EmailId: '" + emailid + "' in Password Recovery window.");
			// clicking send button
			registryPage.clickSendButton();
			Log.message("8. Clicked on 'Send' Button In Password Recovery window.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> After clicking 'Send' button with valid email id, Reset Password Email Sent information message should be displayed.");
			Log.assertThat(
					registryPage.getTextFromPasswordResetSuccessPage().trim().contains(txtresetpasswordSuccessmsg),
					"<b>Actual Result 3:</b> After clicking 'Send' button with valid email id, Reset Password Email Sent information message is displayed.",
					"<b>Actual Result 3:</b> After clicking 'Send' button with valid email id, Reset Password Email Sent information message is not displayed.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_010

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user can search the Added Registry from Find Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_013(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String registryresults = "registryresults";
		String Firstname = "registrybelk";
		String Lastname = "registrybelk";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver);
			registrysignedUserPage.findRegistry(Firstname, Lastname, "Housewarming");
			String Eventname1 = registrysignedUserPage.gettextFromSelectedEventinDropDown();
			Log.message("3. Entered FirstName: '" + Firstname + "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type1 " + Eventname1);

			registrysignedUserPage.findRegistry(Firstname, Lastname, "Dorm");
			String Eventname2 = registrysignedUserPage.gettextFromSelectedEventinDropDown();
			Log.message("4. Entered FirstName: '" + Firstname + "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type2  " + Eventname2);

			registrysignedUserPage.findRegistry(Firstname, Lastname, "Wedding Registry");
			String Eventname3 = registrysignedUserPage.gettextFromSelectedEventinDropDown();
			Log.message("5. Entered FirstName: '" + Firstname + "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type3 " + Eventname3);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> User should be allow to search with different Event type example: Wedding Registry, House warming etc.");
			Log.assertThat(
					Eventname1.contains("Housewarming") && Eventname2.contains("Dorm")
					&& Eventname3.contains("Wedding Registry"),
					"<b>Actual Result 1:</b> User is allowed to search with different Event type example: Wedding Registry, House warming etc. ",
					"<b>Actual Result 1:</b> User is not allowed to search with different Event type example: Wedding Registry, House warming etc.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Registry details should be displayed in Registry screen.");
			Log.assertThat(registryPage.elementLayer.verifyPageElements(Arrays.asList(registryresults), registryPage),

					"<b>Actual Result 3:</b> Registry details is displayed in Registry screen.",
					"<b>Actual Result 3:</b> Registry details is not displayed in Registry screen.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_013

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Help Content assest displays below Left Navigation Pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_205(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String txtAccountHelpContent = "account-help content";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// entering emailid and password
			registryPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered the Valid Credentials (EmailId: '" + emailid + "', Password: '" + password
					+ "') and Clicked on 'Login' Button in 'Registry' Page.");
			String txtAccountpanel = registryPage.getTextFromleftAccountPanel();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Help Content assest should be displayed below left navigation pane in Registry screen.");
			Log.assertThat(
					txtAccountpanel.contains(txtAccountHelpContent) && registryPage.elementLayer
					.verifyPageElements(Arrays.asList("txtAccounthelpcontent"), registryPage),
					"<b>Actual Result 1:</b> Help Content assest is displayed below left navigation pane in Registry screen.",
					"<b>Actual Result 1:</b> Help Content assest is not displayed below left navigation pane in Registry screen.",
					driver);
			Log.message("<br>");

			SignIn signinpage = homePage.headers.navigateToSignIn();
			Log.message("4. Clicked on 'SignIn' button on header and Navigated to 'SignIn' Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Help Content assest should be displayed below left navigation pane in Signin screen.");
			Log.assertThat(
					txtAccountpanel.contains(txtAccountHelpContent) && signinpage.elementLayer
					.verifyPageElements(Arrays.asList("txtAccounthelpcontent"), signinpage),
					"<b>Actual Result 2:</b> Help Content assest is displayed below left navigation pane in Signin screen.",
					"<b>Actual Result 2:</b> Help Content assest is not displayed below left navigation pane in Signin screen.",
					driver);
			Log.message("<br>");
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("5. Clicked on 'WishList' button on header and Navigated to 'WishList' Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Help Content assest should be displayed below left navigation pane in Wishlist screen.");
			Log.assertThat(
					txtAccountpanel.contains(txtAccountHelpContent) && wishlistpage.elementLayer
					.verifyPageElements(Arrays.asList("txtAccounthelpcontent"), wishlistpage),
					"<b>Actual Result 3:</b> Help Content assest is displayed below left navigation pane in Wishlist screen.",
					"<b>Actual Result 3:</b> Help Content assest is not displayed below left navigation pane in Wishlist screen.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_205

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Wish List guest customer content in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_312(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Verifying Wish list Guest Customer Content
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The content for the Wish List guest customer should be displayed below the Create Account button.");
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("WishlistcontentforGuestCustomer"),
							wishlistpage),
							"<b>Actual Result :</b> The content for the Wish List guest customer is displayed below the Create Account button.",
							"<b>Actual Result :</b> The content for the Wish List guest customer is not displayed below the Create Account button.",
							driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_312

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify clicking on View link under Wish List Search Results", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_318(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String FirstName = "wishlist";
		String LastName = "wishlist";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// entering first name
			wishlistpage.enterFindWishListFirstName(FirstName);
			Log.message("3. Entered First Name: '" + FirstName + "' in the FirstName Text Field.");
			// entering last name
			wishlistpage.enterFindWishListLastName(LastName);
			Log.message("4. Entered Last Name: '" + LastName + "' in the LastName Text Field.");
			// entering valid emailid
			wishlistpage.enterFindWishListEmailId(emailid);
			Log.message("5. Entered valid Email Id: '" + emailid + "' in the Email Text Field.");
			// clicking find wishlist button
			wishlistpage.clickFindWishList();
			Log.message("6. Clicked on 'Find The WishList' Button.");
			// clicking 'view' link in wish list result page
			wishlistpage.clickOnViewLink();
			Log.message("7. Clicked on 'View Link' in Wish Search Result Page.");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> On Click, the wish list details should be displayed in public view.");
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("wishListResultContent"), wishlistpage),
					"<b>Actual Result :</b> On Click, the wish list details are displayed in public view.",
					"<b>Actual Result :</b> On Click, the wish list details are not displayed in public view.", driver);
			Log.message("<br>");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_318

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Wish List Search Results", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_317(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String FirstName = "wishlist";
		String LastName = "wishlist";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering first Name
			wishlistpage.enterFindWishListFirstName(FirstName);
			Log.message("3. Entered First Name: '" + FirstName + "' in the FirstName Text Field.");
			// Entering Last Name
			wishlistpage.enterFindWishListLastName(LastName);
			Log.message("4. Entered Last Name: '" + LastName + "' in the LastName Text Field.");
			// Entering Valid EmailId
			wishlistpage.enterFindWishListEmailId(emailid);
			Log.message("5. Entered valid Email Id: '" + emailid + "' in the Email Text Field.");
			// clicking find wishlist button
			wishlistpage.clickFindWishList();
			Log.message("6. Clicked on 'Find The WishList' Button");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> System should display First Name, Last Name, and Location details of the public wish list results.");
			Log.assertThat(
					wishlistpage.getTextFromWishListResults().contains(FirstName)
					&& wishlistpage.getTextFromWishListResults().contains(LastName)
					&& wishlistpage.getTextFromWishListResults().contains("Location"),
					"<b>Actual Result :</b> System displayed First Name, Last Name, and Location details of the public wish list results.",
					"<b>Actual Result :</b> System not displayed First Name, Last Name, and Location details of the public wish list results.",
					driver);
			Log.message("<br>");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_317

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify page redirected and fields are displayed Event Info header.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_156(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> elements = Arrays.asList("selectEventType", "txtEventname", "txtEventCity", "selectState",
				"eventDate");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("5. Clicked on event Info Tab!");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message(
					"Page should be redirected to Event Info tab and fields names are displayed below Registrant Information.And below details should be displayed:");
			Log.message("Event Type");
			Log.message("Event Name");
			Log.message("Event Date (mm/dd/yyy)");
			Log.message("Event Location (City & State)");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(registryInformationPage.elementLayer.verifyPageElements(elements, registryInformationPage),
					"Page redirected to Event Info tab and fields names are displayed below Registrant Information , And below details displayed as: Event Type , Event Name ,Event Date (mm/dd/yyy) ,Event Location (City & State)",
					"Page not redirected to Event Info tab and fields names are displayed below Registrant Information , And not shown below details as: Event Type ,Event Name ,Event Date (mm/dd/yyy) ,Event Location (City & State)",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_156

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the fields in empty registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_179(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String txtMsg = "Please enter the information below to find a registry.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			GiftRegistryPage giftRegistryPage = myaccount.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to gift registry", driver);
			Log.message("<b>Expected Result :</b> Registry header should be displayed properly");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("lblRegistry"), giftRegistryPage),
					"<b>Actual Result :</b> Registry page header is present properly",
					"<b>Actual Result :</b> Registry page header is not present properly");
			Log.message("<b>Expected Result :</b> Find a registry Subheading should be displayed");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("lblFindRegistry"),
							giftRegistryPage),
							"<b>Actual Result :</b> Find Registry header is present properly",
					"<b>Actual Result :</b> Find Registry header is not present properly");
			Log.message(
					"<b>Expected Result :</b> Please enter the information below to find a registry message should be displayed");
			Log.assertThat(giftRegistryPage.getTextFromGiftRegistry().equals(txtMsg),
					"<b>Actual Result :</b> Resource message is properly displayed",
					"<b>Actual Result :</b> Resource message is not properly displayed");
			Log.message(
					"<b>Expected Result :</b> First Name and last name and event date should be displayed properly");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("lblFirstName"), giftRegistryPage),
					"<b>Actual Result :</b> First Name is displayed properly",
					"<b>Actual Result :</b> First Name is not displayed properly");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("lblLastName"), giftRegistryPage),
					"<b>Actual Result :</b> Last name is displayed properly",
					"<b>Actual Result :</b> Last name is not displayed properly");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("lblEventType"), giftRegistryPage),
					"<b>Actual Result :</b> Event type is displayed properly",
					"<b>Actual Result :</b> Event type is not displayed properly");
			giftRegistryPage.clickOnAdvanceSearch();
			Log.message("5. Clicked Advance search button");
			Log.message("<b>Expected Result :</b> Additional fields should properly displyed");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageListElements(Arrays.asList("fldAdvanceSearch"),
							giftRegistryPage),
							"<b>Actual Result :</b> Additional fields are displayed",
					"<b>Actual Result :</b> Additional fields are not displayed");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_179

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the fields in empty registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_341(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			Log.message("4. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("5. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page", driver);

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("6. Select product from search result page!", driver);
			// Select Size, Color, Qty
			String color = pdpPage.selectColor();
			Log.message("7. color is selected and the selected color is " + color);
			String quantity = pdpPage.selectQuantity();
			Log.message("8. Quantity is selected and the quantity is" + quantity);
			// add product to shopping bag
			pdpPage.clickAddToWishListLink();
			Log.message("9. Added to wish list!");
			WishListPage wishListPage = pdpPage.headers.navigateToWishList();

			Log.assertThat(wishListPage.elementLayer.verifyPageElements(Arrays.asList("fldItemAdded"), wishListPage),
					"Added item is properly displayed", "Added item is not properly displayed", driver);
			wishListPage.clickOnRemoveLink();
			Log.message("10. Remove link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The customer clicks the Remove link, the items should be removed from the Wish List");
			Log.assertThat(
					wishListPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("fldItemAdded"), wishListPage),
					"<b>Actual Result :</b> Item is removed properly in wishlist",
					"<b>Actual Result :</b> Item is not removed properly in wishlist", driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_341

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the fields in empty registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_180(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "jog";
		String lstName = "la";
		String event = "Wedding Registry";
		String txtNoRegistryFound = "No registry has been found for " + firstName + " " + lstName
				+ " wedding, please try again.";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to gift registry", driver);
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set " + firstName);
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set " + lstName);
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			String noRegistryMsg = registrySignedUser.getTextFromRegistry();
			Log.message("9. Got the text from registry user " + noRegistryMsg);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Empty Registry screen should be displayed with the resources message No registry has been found for First name,Last name, please try again.with the heading Registry Results");
			Log.assertThat(noRegistryMsg.equals(txtNoRegistryFound),
					"<b>Actual Result :</b> No registry has been found for First name,Last name, please try again message is displayed properly",
					"<b>Actual Result :</b> No registry has been found for First name,Last name, please try again message is not displayed properly",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_180

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify items are added while clicking on Add to shopping button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_198(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link in multiple registry is clicked");
			String upcInRegistryPage = registryInformationPage.getUPCInRegistryPage(1);
			String quantityInRegistry = registryInformationPage.getQunatityOfProduct(1);
			registryInformationPage.clickOnAddtoBag();
			Log.message("11. Add to bag button is clicked");
			registryInformationPage.clickOnMiniCart();

			String qtyInBagPage = shoppingBagPage.getNoOfQtyInShoppingBagPage();
			ArrayList<String> upcInBagPage = shoppingBagPage.getUPCInMinicartPage();
			String strUpcInBagPage = upcInBagPage.get(0).replace("[", "").replace("]", "");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Add to shopping bag link should be add the selected qty of the item to the customer’s cart.");
			Log.assertThat(quantityInRegistry.equals(qtyInBagPage),
					"<b>Actual Result :</b> Quantity for the item is same in both bag page and registry page",
					"<b>Actual Result :</b> Quantity for the item is not same in both bag page and registry page",
					driver);
			Log.assertThat(upcInRegistryPage.equals(strUpcInBagPage),
					"<b>Actual Result :</b> Upc in bag page is same for the product added in registry",
					"<b>Actual Result :</b> Upc in bag page is different for the product added in registry");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_198

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system allows the customer to increase the quantity.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_199(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set " + firstName);
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set " + lstName);
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			String quantityInRegistry = registryInformationPage.getQunatityOfProduct(1);
			Log.message("11. Quantity for the product in registry is" + quantityInRegistry);
			registryInformationPage.clickOnAddtoBag();
			Log.message("12. Add to bag button is clicked");
			registryInformationPage.clickOnMiniCart();
			Log.message("13. mini cart link is clicked");
			String quantityInBagAfterChange = shoppingBagPage.selectQtyByIndex(3);
			Log.message(
					"14. quantity is selected using index and the selected quantity is " + quantityInBagAfterChange);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Customer should be allowed to add more than the qty requested,  may not allowed to exceed the Max_Buy_Qty");
			Log.assertThat(!quantityInRegistry.equals(quantityInBagAfterChange),
					"<b>Actual Result :</b> Customer is allowed to add more than the qty requested may not allowed to exceed the Max_Buy_Qty ",
					"<b>Actual Result :</b> Customer is not allowed to add more than the qty requested may not allowed to exceed the Max_Buy_Qty");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_199

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Edit link is not shown in the product page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_196(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry", driver);
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Edit link should not be displayed in Product tile.");
			Log.assertThat(
					registryInformationPage.elementLayer
					.verifyPageListElementsDoNotExist(Arrays.asList("lnkEditDetails"), registryInformationPage),
					"<b>Actual Result :</b> Edit link is  displayed in Product tile.",
					"<b>Actual Result :</b> Edit link is not be displayed in Product tile.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_196

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Update/Delete QTY link is not shown in shoppers view screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_197(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry", driver);
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Verify Update/Delete QTY link is not shown in shoppers view screen");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElementsDoNotExist(Arrays.asList("lnkUpdate"),
							registryInformationPage),
							"<b>Actual Result :</b> Update link is  not displayed in Product tile.",
							"<b>Actual Result :</b> Update link is  displayed in Product tile.", driver);
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElementsDoNotExist(Arrays.asList("lnkRemove"),
							registryInformationPage),
							"<b>Actual Result :</b> Remove link is not displayed in  Product tile.",
							"<b>Actual Result :</b> Remove link is displayed in properly", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_197

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify gifter able to view the Still needs (read only count)", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_194(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set " + firstName);
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set " + lstName);
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message(
					"<b>Expected Result :</b> Gifter should be allowed to view the Still Needs quantity (read only count)");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lblStillNeeds"),
							registryInformationPage),
							"<b>Actual Result :</b> Still Needs quantity (read only count) label is present properly",
					"<b>Actual Result :</b> Still Needs quantity (read only count) label is not present properly");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_194

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify search using the First name, Last name and Event type", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_019(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String PartialNonCaseSensitiveRegistrantfirstname = "REGISTRYBEL";
		String PartialNonCaseSensitiveRegistrantlastname = "REGISTRYBEL";
		String eventype = "Wedding Registry";
		String PartialNonCaseSensitiveCo_Registrantfirstname = "TEST1";
		String PartialNonCaseSensitiveCo_Registrantlastname = "TEST";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid EmailId: '" + emailid + "' in the EmailId Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered Password: '" + password + "' in the Password Field");
			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on 'SignIn' Button");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver).get();
			// Entered first name,last name and event type in the 'find
			// Registry' field
			Log.message("<br>");
			Log.message("*  Verifying Search using the First name, Last name and Event type of 'Registrant'");
			registrysignedUserPage.setFirstNameInFindRegistry(PartialNonCaseSensitiveRegistrantfirstname);
			Log.message("6. Entered First name: '" + PartialNonCaseSensitiveRegistrantfirstname
					+ "' in the First Name Field");
			registrysignedUserPage.setLastNameInFindRegistry(PartialNonCaseSensitiveRegistrantlastname);
			Log.message(
					"7. Entered Last name: '" + PartialNonCaseSensitiveRegistrantlastname + "' in the Last Name Field");
			registrysignedUserPage.selectEventTypeInFindRegistry(eventype);
			Log.message("8. Event type: '" + eventype + "' is selected");
			registrysignedUserPage.clickOnFindRegistry();
			Log.message("9. Clicked on 'Find Registry' Button");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> User should be able to search the Registry Results using 'Partial and NonCase Sensitive' FirstName and LastName of Registrant.");

			Log.assertThat(registryPage.elementLayer.verifyPageElements(Arrays.asList("registryresults"), registryPage),

					"<b>Actual Result 1:</b> User is able to search the Registry Results using 'Partial and NonCase Sensitive Inputs' : FirstName and LastName of Registrant.",
					"<b>Actual Result 1:</b> User is not able to search the Registry Results using 'Partial and NonCase Sensitive Inputs' : FirstName and LastName of Registrant.",
					driver);
			Log.message("<br>");
			Log.message("* Verifying search using the First name, Last name and Event type of 'Co-Registrant'");
			registrysignedUserPage.setFirstNameInFindRegistry(PartialNonCaseSensitiveCo_Registrantfirstname);
			Log.message("10. Entered First name: '" + PartialNonCaseSensitiveCo_Registrantfirstname
					+ "' in the First Name Field");
			registrysignedUserPage.setLastNameInFindRegistry(PartialNonCaseSensitiveCo_Registrantlastname);
			Log.message("11. Entered Last name: '" + PartialNonCaseSensitiveCo_Registrantlastname
					+ "' in the Last Name Field");
			registrysignedUserPage.clickOnFindRegistry();
			Log.message("13. Clicked on 'Find Registry' Button");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> User should be able to search the Registry Results using 'Partial and NonCase Sensitive' FirstName and LastName of Co_Registrant.");

			Log.assertThat(registryPage.elementLayer.verifyPageElements(Arrays.asList("registryresults"), registryPage),
					"<b>Actual Result 2:</b> User is able to search the Registry Results using 'Partial and NonCase Sensitive Inputs' : FirstName and LastName of Co_Registrant.",
					"<b>Actual Result 2:</b> User is not able to search the Registry Results using 'Partial and NonCase Sensitive Inputs' : FirstName and LastName of Co_Registrant.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_019//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify advance search in Find a Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_020(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			registrySignedUserPage.setFirstNameInFindRegistry("aspire");
			Log.message("2. Enterd First name in the 'Find  a Registry' p/age");
			registrySignedUserPage.setLastNameInFindRegistry("qa");
			Log.message("3. Entered Last Name in the 'Find  a Registry' page");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("4. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnAdvancedSearch();
			Log.message("5. clicked on advaced search");
			registrySignedUserPage.setCityInFindRegistry("Stamford");
			Log.message("6. Entered City in the 'Find  a Registry' page");
			registrySignedUserPage.selectStateInFindRegistry("Connecticut");
			Log.message("7. Selected 'Connecticut' from the State drop down in the 'Find  a Registry' page");
			registrySignedUserPage.selectMonthInFindRegistry("December");
			Log.message("8. Selected 'December' from the Event Month drop down in the 'Find  a Registry' page");
			registrySignedUserPage.selectYearInFindRegistry("2016");
			Log.message("9. Selected 'December' from the Event Year drop down in the 'Find  a Registry' page");
			registrySignedUserPage.setRegIdInFindRegistry("200000305");
			Log.message("10. Entered Registry Id in the 'Find a Registry' page");
			RegistryInformationPage registryInformationPage = registrySignedUserPage
					.clickOnFindRegistry("Simple Search");
			Log.message("11. clicked on 'Find a Registry', It navigated to Registry Information Page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The matched search registries should be displayed  in the Registry page");
			Log.assertThat(registryInformationPage.getRegId(0).contains("200000305"),
					"<b>Actual Result:</b> The matched search registries is displayed  in the Registry page ",
					"<b>Actual Result:</b> The matched search registries is not displayed  in the Registry page ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_020

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify advance search field displays in 'Find in Registry' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_021(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			Log.message("2. Navigated to Registry Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> The Basic Fields such as 'FirstName' , 'LastName' ,'EventType' should be displayed in the  'Find a Registry' page  ");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtFirstNameInFindRegistry", "txtLastNameInFindRegistry"),
							registrySignedUserPage)) && (registrySignedUserPage.elementLayer
									.verifyPageElementsDoNotExist(
											Arrays.asList("txtCityInFindRegistry", "txtStateInFindRegistry",
													"selectEventDateMonthInFindRegistry",
													"selectEventDateYearInFindRegistry", "txtRegistryIDInFindRegistry"),
													registrySignedUserPage)
													&& (registrySignedUserPage.elementLayer.verifyPageListElements(
															Arrays.asList("selectEventTypeInFindRegistry"), registrySignedUserPage)))
															&& (registrySignedUserPage.elementLayer.verifyPageListElements(
																	Arrays.asList("selectEventTypeInFindRegistry"), registrySignedUserPage))),
																	"<b>Actual Result1:</b> The Basic Fields such as 'FirstName' , 'LastName' ,'EventType' is displayed in the  'Find a Registry' page",
																	"<b>Actual Result1:</b> The Basic Fields such as 'FirstName' , 'LastName' ,'EventType' is not displayed in the  'Find a Registry' page",
																	driver);

			registrySignedUserPage.clickOnAdvancedSearch();
			Log.message("3. clicked on advaced search");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> By clicking on 'Advanced Search' the advanced search fields should be displayed in the 'Find a Registry' page  ");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtCityInFindRegistry",
							"txtStateInFindRegistry", "selectEventDateMonthInFindRegistry",
							"selectEventDateYearInFindRegistry", "txtRegistryIDInFindRegistry"),
							registrySignedUserPage))
							&& (registrySignedUserPage.verifyFirstNameLastNameInFindRegistry("Simple Search"))),
							"<b>Actual Result2:</b> By clicking on 'Advanced Search' the advanced search fields is displayed in the 'Find a Registry' page ",
							"<b>Actual Result2:</b> By clicking on 'Advanced Search' the advanced search fields is not displayed in the 'Find a Registry' page ",
							driver);

			registrySignedUserPage.clickOnSimpleSearch();
			Log.message("4. clicked on Simple search");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result3:</b> By clicking on 'Simple Search' 'FirstName' , 'LastName' ,'EventType' should be displayed in the  'Find a Registry' page  ");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtFirstNameInFindRegistry", "txtLastNameInFindRegistry"),
							registrySignedUserPage)) && (registrySignedUserPage.elementLayer
									.verifyPageElementsDoNotExist(
											Arrays.asList("txtCityInFindRegistry", "txtStateInFindRegistry",
													"selectEventDateMonthInFindRegistry",
													"selectEventDateYearInFindRegistry", "txtRegistryIDInFindRegistry"),
													registrySignedUserPage))),
													"<b>Actual Result3:</b> By clicking on 'Simple Search' 'FirstName' , 'LastName' ,'EventType' should be displayed in the  'Find a Registry' page ",
													"<b>Actual Result3:</b> By clicking on 'Simple Search' 'FirstName' , 'LastName' ,'EventType' should be displayed in the  'Find a Registry' page ",
													driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_021

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Find Registry button in Find in Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_022(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.setFirstNameInFindRegistry("aspire");
			Log.message("2. Enterd First name");
			registrySignedUserPage.setLastNameInFindRegistry("qa");
			Log.message("3. Entered Last Name");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("4. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();
			Log.message("5. clicked on 'Find a Registry', It navigated to Registry Information Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The list of the registries should be displayed  in the Registry page");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("YourRegistries"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> The list of the registries is displayed  in the Registry page ",
							"<b>Actual Result:</b> The list of the registries is not displayed  in the Registry page ", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_022

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry result in 'Find in Registry' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_023(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("2. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();
			Log.message("3. clicked on 'Find a Registry', It navigated to Registry Information Page");

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Registry results  should displays as paginated in the RegistryInformation Page");
			Log.assertThat(
					((registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lstPageNos"),
							registryInformationPage))),
							"<b>Actual Result:</b> Registry results is displayed as pagination in the RegistryInformation Page",
							"<b>Actual Result:</b> Registry results is not displayed as pagination in the RegistryInformation Page",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_023

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry Banner Content Slot displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_025(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Registry Banner Content Slot should be displayed on the top of the 'My Registry' Page");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryContentSlot"),
							registrySignedUserPage))
							&& (registrySignedUserPage.getBreadCrumbLastValue().contains("Your Registries"))),
							"<b>Actual Result1:</b> Registry Banner Content Slot is displayed on the top of the 'My Registry' Page",
							"<b>Actual Result1:</b> Registry Banner Content Slot is not displayed on the top of the 'My Registry' Page",
							driver);

			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Clicked on 'CreateNewRegistry' button");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> Registry Banner Content Slot should be displayed on the top of the 'Create New Regitry' Page");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("registryContentSlotInCreteRegistry"), registrySignedUserPage))),
							"<b>Actual Result2:</b> Registry Banner Content Slot is displayed on the top of the 'Create New Registry' Page",
							"<b>Actual Result2:</b> Registry Banner Content Slot is not displayed on the top of the 'Create New Registry' Page",
							driver);

			homePage.headers.navigateToGiftRegistry();
			registrySignedUserPage.setFirstNameInFindRegistry("aspire");
			Log.message("5. Enterd First name");
			registrySignedUserPage.setLastNameInFindRegistry("qa");
			Log.message("6. Entered Last Name");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("7. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();
			Log.message("8. clicked on 'Find a Registry', It navigated to Registry Information Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result3:</b> Registry Banner Content Slot should be displayed on the top of the 'Find Someone's Regitry' Page");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryContentSlot"),
							registrySignedUserPage))
							&& (registrySignedUserPage.getBreadCrumbLastValue().contains("Find Someone's Registry"))),
							"<b>Actual Result3:</b> Registry Banner Content Slot is displayed on the top of the 'Find Someone's Registry' Page",
							"<b>Actual Result3:</b> Registry Banner Content Slot is not displayed on the top of the 'Find Someone's Registry' Page",
							driver);

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);
			registryInformationPage.clickOnViewInRegistry(1);

			Log.message("9. Clicked on 'View' link to view someone's Registry Details");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result4:</b> Registry Banner Content Slot should be displayed on the top of the 'View Other’s Registries' Page");
			Log.assertThat(
					((registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("registryContentSlot"),
							registryInformationPage))),
							"<b>Actual Result4:</b> Registry Banner Content Slot is displayed on the top of the 'View Other’s Registries' Page",
							"<b>Actual Result4:</b> Registry Banner Content Slot is not displayed on the top of the 'View Other’s Registries' Page",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_025

	@Test(groups = { "mobile",
	"tablet" }, description = "Verify registry result is displayed is table format for Tablet/Mobile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_026(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		if(!(Utils.getRunPlatForm()=="mobile")){
			throw new SkipException("This testcase is only applicable for mobile");
		}
		else{
			try {
		
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.setFirstNameInFindRegistry("aspire");
			Log.message("2. Enterd First name");
			registrySignedUserPage.setLastNameInFindRegistry("qa");
			Log.message("3. Entered Last Name");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("4. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);
			Log.message("5. clicked on 'Find a Registry', It navigated to Registry Information Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Registry results should be displayed in 2 columns in the Registry page");
			Log.assertThat(
					((registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("regDetailsInMobile"),
							registryInformationPage))),
							"<b>Actual Result:</b> Registry results is displayed in 2 columns in the Registry page ",
							"<b>Actual Result:</b> Registry results is not displayed in 2 columns in the Registry page ",
							driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
		    driver.quit();
		} // finally
		}
	}// TC_BELK_REGISTRY_026

	@Test(groups = { "mobile" }, description = "Verify the Help Content Assets is displayed in Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_027(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if(!(Utils.getRunPlatForm()=="mobile"))
		{
			throw new SkipException("This testcase is applicable only for mobile");
		}
		else{
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			Log.message("4. Navigated to Registry Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Help Content Assests should be displayed in the Registry Page  ");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("HelpContentSlotMobile"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> Help Content Assests should be displayed in the Registry Page",
							"<b>Actual Result:</b> Help Content Assests should be displayed in the Registry Page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
		}
	}// TC_BELK_REGISTRY_027

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Create a New Registry' button is displayed below Find Registry button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_028(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			Log.message("4. Navigated to Registry Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> 'Create New Registry' button should be displayed in the 'Your Registries' page ");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> 'Create New Registry' button is displayed in the 'Your Registries' page",
							"<b>Actual Result:</b> 'Create New Registry' button is not displayed in the 'Your Registries' page",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_028

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Create Registry' screen is open when clicking on 'Create New Registry' button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_029(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Clicked on 'CreateNewRegistry' button , It Navigated to createNewRegistry form page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Event Information , Registration Information fields should be displayed in the 'CreateNewRegistry' form page");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("EventInfo"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> Event Information , Registration Information fields is displayed in the 'CreateNewRegistry' form page",
							"<b>Actual Result:</b> Event Information , Registration Information fields is not displayed in the 'CreateNewRegistry' form page",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_029

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify page redirected to Create Registry - Step 3 of 3 screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_030(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 2;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message(i++ + ". Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message(i++ + ". Navigate to create Account page");

			GiftRegistryPage giftRegistryPage = (GiftRegistryPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message(i++ + ". Navigate to 'Gift Registry Page' After created Account");

			RegistrySignedUserPage registrySignedUserPage = giftRegistryPage.clickOnCreateRegistrybutton();
			Log.message(i++ + ". Clicked on Create Registry button");

			LinkedHashMap<String, String> registryDetails = registrySignedUserPage.fillingEventDetails("weddingRegistry");
			Log.message(i++ + ". Filled Registry Details");

			List<String> indexes = new ArrayList<String>(registryDetails.keySet());
			String[] eventDetails = new String[4];
			eventDetails[0] = indexes.get(0).replace("select_eventType_", "");
			eventDetails[1] = indexes.get(6).replace("type_reFirstName_", "");
			eventDetails[2] = indexes.get(7).replace("type_reLastName_", "");

			registrySignedUserPage.clickContinueInCreateRegistry();
			Log.message(i++ + ". Click on Continue Button in the create registry page");

			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_Updateddetails");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.clickOnContinueButtonInEventShipping();
			Log.message(i++ + ". Click on Continue Button in the 'Event Shipping' page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Page should be redirected to Create Registry - Step 3 of 3 screen While submitting Created Registry");

			Log.assertThat(registrySignedUserPage.gettxtStep3InCreateRegistry().contains("Step 3 of 3"),
					"<b>Actual Result1:</b> Page is redirected to Create Registry - Step 3 of 3 screen While submitting Created Registry",
					"<b>Actual Result1:</b> Page is not redirected to Create Registry - Step 3 of 3 screen While submitting Created Registry",
					driver);

			registrySignedUserPage.clickOnSubmitButton();
			Log.message(i++ + ". Click on submit button in the Registry Page");
			eventDetails[3] = Integer.toString(i);

			i = Integer.parseInt(eventDetails[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			homePage.headers.navigateToGiftRegistry();
			Log.message("<br>");
			Log.message("<b>Expected Result2:</b> The newly added Registry should be displeyed in the 'Registry' Page");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("YourRegistries"),
							registrySignedUserPage))),
							"<b>Actual Result2:</b> The newly added Registry is displeyed in the 'Registry' Page",
							"<b>Actual Result2:</b> The newly added Registry is not displeyed in the 'Registry' Page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_030

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify bar code is generated for the newly added registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_031(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 2;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Bar code' should be generated for the newly added Registry");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryBarCode"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> 'Bar code' is generated for the newly added Registry",
							"<b>Actual Result:</b> 'Bar code' is not generated for the newly added Registry", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_031

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays the Global belk Footer", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_032(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 2;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message(i++ + ". Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message(i++ + ". Navigate to create Account page");

			GiftRegistryPage giftRegistryPage = (GiftRegistryPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message(i++ + ". Navigate to 'Gift Registry Page' After created Account");

			RegistrySignedUserPage registrySignedUserPage = giftRegistryPage.clickOnCreateRegistrybutton();
			Log.message(i++ + ". Click on Create Registry button");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Global Belk footer should be displayed in step 1 of 'CreateRegistry' page");

			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("footerPanel"),
							registrySignedUserPage),
							"<b>Actual Result1:</b> Global Belk footer is displayed in step 1 of 'CreateRegistry' page",
							"<b>Actual Result1:</b> Global Belk footer is not displayed in step 1 of 'CreateRegistry' page",
							driver);

			LinkedHashMap<String, String> registryDetails = registrySignedUserPage.fillingEventDetails("weddingRegistry");
			Log.message(i++ + ". Filled Registry Details");

			List<String> indexes = new ArrayList<String>(registryDetails.keySet());
			String[] eventDetails = new String[4];
			eventDetails[0] = indexes.get(0).replace("select_eventType_", "");
			eventDetails[1] = indexes.get(6).replace("type_reFirstName_", "");
			eventDetails[2] = indexes.get(7).replace("type_reLastName_", "");

			registrySignedUserPage.clickContinueInCreateRegistry();
			Log.message(i++ + ". Click on Continue Button in the create registry page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> Global Belk footer should be displayed in step 2 of 'CreateRegistry' page");

			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("footerPanel"),
							registrySignedUserPage),
							"<b>Actual Result2:</b> Global Belk footer is displayed in step 2 of 'CreateRegistry' page",
							"<b>Actual Result2:</b> Global Belk footer is not displayed in step 2 of 'CreateRegistry' page",
							driver);

			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_Updateddetails");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.clickOnContinueButtonInEventShipping();
			Log.message(i++ + ". Click on Continue Button in the 'Event Shipping' page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result3:</b> Global Belk footer should be displayed in step 3 of 'CreateRegistry' page");

			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("footerPanel"),
							registrySignedUserPage),
							"<b>Actual Result3:</b> Global Belk footer is displayed in step 3 of 'CreateRegistry' page",
							"<b>Actual Result3:</b> Global Belk footer is not displayed in step 3 of 'CreateRegistry' page",
							driver);

			registrySignedUserPage.clickOnSubmitButton();
			Log.message(i++ + ". Click on submit button in the Registry Page");
			eventDetails[3] = Integer.toString(i);

			i = Integer.parseInt(eventDetails[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			homePage.headers.navigateToGiftRegistry();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result4:</b> Global Belk footer should be displayed after creating Registry in the Registry Page");

			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("footerPanel"),
							registrySignedUserPage),
							"<b>Actual Result4:</b> Global Belk footer is displayed after creating Registry in the Registry Page",
							"<b>Actual Result4:</b> Global Belk footer is not displayed after creating Registry in the Registry Page",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_032

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify validation message displayed no registry results returned,", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_033(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.clickOnFindRegistry();
			Log.message("2. clicked on FindRegistry");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Finding registry without entering the values in the fields, the Validation failure message should be displayed in the 'Find Registry' page  ");
			Log.assertThat(
					registrySignedUserPage.getNoRegistryFoundMsg()
					.contains("No registry has been found for your search, please try again"),
					"<b>Actual Result:</b> Finding registry without entering the values in the fields, the Validation failure message is displayed  in the 'Find Registry' page ",
					"<b>Actual Result:</b> Finding registry without entering the values in the fields, the Validation failure message is not displayed in the 'Find Registry' page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_033

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify resource message is displayed when no registry results returned,", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_034(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			registrySignedUserPage.setFirstNameInFindRegistry("asalfh");

			Log.message("2. Enterd First name");
			registrySignedUserPage.setLastNameInFindRegistry("laskdu");

			Log.message("3. Entered LastName");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");

			Log.message("4. Selected 'Edding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();

			Log.message("5. clicked on FindRegistry");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> When we Enter Invalid 'FirstName' and 'LastName' , the No Registries found message should be displayed in the 'Find Registry' page");
			Log.assertThat(registrySignedUserPage.getNoRegistryFoundMsg().contains("No registry has been found"),
					"<b>Actual Result:</b> When we Enter Invalid 'FirstName' and 'LastName' , the No Registries found message is displayed in the 'Find Registry' page",
					"<b>Actual Result:</b> When we Enter Invalid 'FirstName' and 'LastName' , the No Registries found message is not displayed in the 'Find Registry' page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_034

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify advance search field displays in 'Find in Registry' page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_035(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.clickOnAdvancedSearch();
			Log.message("4. clicked on advaced search");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> By clicking on 'Advanced Search' the advanced search fields should be displayed in the 'Find a Registry' page  ");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtCityInFindRegistry",
							"txtStateInFindRegistry", "selectEventDateMonthInFindRegistry",
							"selectEventDateYearInFindRegistry", "txtRegistryIDInFindRegistry"),
							registrySignedUserPage))
							&& (registrySignedUserPage.verifyFirstNameLastNameInFindRegistry("Simple Search"))),
							"<b>Actual Result:</b> By clicking on 'Advanced Search' the advanced search fields is displayed in the 'Find a Registry' page ",
							"<b>Actual Result:</b> By clicking on 'Advanced Search' the advanced search fields is not displayed in the 'Find a Registry' page ",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_035

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify added registry are displayed in Your Registries pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_036(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			Log.message("4. Navigated to Registry Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The list of the registries should be displayed under the “Your Registries” in the Registry page");
			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("YourRegistries"),
							registrySignedUserPage))),
							"<b>Actual Result:</b> The list of the registries is displayed under the “Your Registries” in the Registry page ",
							"<b>Actual Result:</b> The list of the registries is not displayed under the “Your Registries” in the Registry page ",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_036

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user can view the added registry from My Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_037(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 2;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message(i++ + ". Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message(i++ + ". Navigate to create Account page");

			GiftRegistryPage giftRegistryPage = (GiftRegistryPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message(i++ + ". Navigate to 'Gift Registry Page' After created Account");

			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> 'Empty Registry' list should be displayed if there are no registries in the 'Registry' Page");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("YourRegistries"),
							registrySignedUserPage))),
							"<b>Actual Result1:</b> 'Empty Registry' list is displayed if there are no registries in the 'Registry' Page",
							"<b>Actual Result1:</b> 'Empty Registry' list is not displayed if there are no registries in the 'Registry' Page",
							driver);

			giftRegistryPage.clickOnCreateRegistrybutton();
			Log.message(i++ + ". Click on Create Registry button");

			LinkedHashMap<String, String> registryDetails = registrySignedUserPage.fillingEventDetails("weddingRegistry");
			Log.message(i++ + ". Filled Registry Details");

			List<String> indexes = new ArrayList<String>(registryDetails.keySet());
			String[] eventDetails = new String[4];
			eventDetails[0] = indexes.get(0).replace("select_eventType_", "");
			eventDetails[1] = indexes.get(6).replace("type_reFirstName_", "");
			eventDetails[2] = indexes.get(7).replace("type_reLastName_", "");

			registrySignedUserPage.clickContinueInCreateRegistry();
			Log.message(i++ + ". Click on Continue Button in the create registry page");

			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_Updateddetails");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message(i++ + ". Filled Event shipping Details");

			registrySignedUserPage.clickOnContinueButtonInEventShipping();
			Log.message(i++ + ". Click on Continue Button in the 'Event Shipping' page");

			registrySignedUserPage.clickOnSubmitButton();
			Log.message(i++ + ". Click on submit button in the Registry Page");
			eventDetails[3] = Integer.toString(i);

			i = Integer.parseInt(eventDetails[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			homePage.headers.navigateToGiftRegistry();
			Log.message("<br>");
			Log.message("<b>Expected Result2:</b> The Registries list should be displayed  in the 'Registry' page");

			Log.assertThat(
					((registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("YourRegistries"),
							registrySignedUserPage))),
							"<b>Actual Result2:</b> The Registries list is displayed  in the 'Registry' page",
							"<b>Actual Result2:</b> The Registries list is not displayed  in the 'Registry' page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_037

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Need Help section in Wish List page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_299(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			WishListPage wishListpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigated to WishListPage");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Help Content Assests should be displayed in the 'WishList' Page  ");

			if(Utils.getRunPlatForm()=="desktop")
			{
			Log.assertThat(
					((wishListpage.elementLayer.verifyPageElements(Arrays.asList("HelpContentSlotDesktop"), wishListpage))),
					"<b>Actual Result:</b> Help Content Assests should be displayed in the 'WishList' Page",
					"<b>Actual Result:</b> Help Content Assests should be displayed in the 'WishList' Page", driver);
			}
			if(Utils.getRunPlatForm()=="mobile")
			{
			Log.assertThat(
					((wishListpage.elementLayer.verifyPageElements(Arrays.asList("HelpContentSlotMobile"), wishListpage))),
					"<b>Actual Result:</b> Help Content Assests should be displayed in the 'WishList' Page",
					"<b>Actual Result:</b> Help Content Assests should be displayed in the 'WishList' Page", driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_299

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Need Help section in Wish List page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_300(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			WishListPage wishListpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigated to WishListPage");

			String bclastValue = wishListpage.getBreadCrumbLastValue();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Breadcrumb Last value should be displayed as 'Wish List' in  the WishList Page.");

			Log.assertThat(bclastValue.contains("Wish List"),
					"<b>Actual Result:</b> The Breadcrumb Last value is displayed as 'Wish List' in  the WishList Page.",
					"<b>Actual Result:</b> The Breadcrumb Last value is not displayed as 'Wish List' in  the WishList Page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_300

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify page redirected and fields are displayed below Registrant Information header. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_157(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> elements = Arrays.asList("selectRegistrantRole", "txtRegistrantFirstName", "txtRegistrantLastName",
				"txtRegistrantEmail");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("5. Clicked on event Info Tab!");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message(
					"Page should be redirected to Event Info tab and fields names are displayed below Registrant Information.And below details should be displayed:");
			Log.message("Role");
			Log.message("First Name");
			Log.message("Last Name");
			Log.message("Email");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(registryInformationPage.elementLayer.verifyPageElements(elements, registryInformationPage),
					"Page redirected to Event Info tab and fields names are displayed below Registrant Information , And below details displayed as: First Name , Last Name ,Role ,Email",
					"Page not redirected to Event Info tab and fields names are displayed below Registrant Information , And not shown below details as:  First Name , Last Name ,Role ,Email",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_157

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify page redirected and fields are displayed below CoRegistrant Information header.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_158(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> elements = Arrays.asList("selectCoRegistrantRole", "txtCoRegistrantFirstName",
				"txtCoRegistrantLastName", "txtCoRegistrantEmail");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("5. Clicked on event Info Tab!");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message(
					"Page should be redirected to Event Info tab and fields names are displayed below CoRegistrant Information.And below details should be displayed:");
			Log.message("Role");
			Log.message("First Name");
			Log.message("Last Name");
			Log.message("Email");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(registryInformationPage.elementLayer.verifyPageElements(elements, registryInformationPage),
					"Page redirected to Event Info tab and fields names are displayed below CoRegistrant Information , And below details displayed as: First Name , Last Name ,Role ,Email",
					"Page not redirected to Event Info tab and fields names are displayed below CoRegistrant Information , And not shown below details as:  First Name , Last Name ,Role ,Email",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_158

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify form fields validate the entered values ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_159(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> errorMsg = Arrays.asList("lblMandatory");
		List<String> disableButton = Arrays.asList("btnContinue");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page !");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account !");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("5. Clicked on event Info Tab!");
			// Filling event details page
			registrySignedUserPage.fillingEventDetails("EventDetails");
			Log.message("6. filled event Info page !");
			Log.message("<br>");
			Log.message("<b>Expected Result 1</b>");
			Log.message("Error message should be displayed below the blank fields !");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(errorMsg, registrySignedUserPage),
					"On leaving any mandatory field it is showing 'This field is required ' !",
					"On leaving any mandatory field it is not showing 'This field is required ' !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2</b>");
			Log.message("'Apply' button should be disabled !");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElementsDisabled(disableButton,
							registryInformationPage),
							"'Apply' button is disabled when we leave any mandatory field !",
							"'Apply' button is not disabled when we leave any mandatory field !", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_159

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify edited registry details are updated in Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_160(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String previousEventType;
		String UpdatedEventType;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// Getting event type before editing
			previousEventType = registrySignedUserPage.getEventTypeInMyRegistry();
			Log.message("4. Before editing event type is '" + previousEventType + "' !", driver);
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("6. Clicked on event Info Tab!");
			// Select EventType
			registryInformationPage.selectEventTypeNonSelected(previousEventType);
			Log.message("7. Selected the event type !");
			// click on Apply Button
			registryInformationPage.clickCancelInEventInfo();
			Log.message("8. clicked on Apply button !");
			// clicking on registry tab on headers
			homePage.headers.navigateToGiftRegistry();
			// Getting event type after editing
			UpdatedEventType = registrySignedUserPage.getEventTypeInMyRegistry();
			Log.message("9. After editing and cancel the event type is '" + UpdatedEventType + "' !");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message(
					"On click of Cancel button should not save any entered information, and revert back to the previous values entered.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat((previousEventType.equals(UpdatedEventType)),
					"On click of Cancel button System is not saving any entered information, and revert back to the previous values entered !",
					"On click of Cancel button System is saving the entered information, and revert back to the previous values entered !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_160

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify edited registry details are updated in Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_161(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String previousEventType;
		String UpdatedEventType;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// Getting event type before editing
			previousEventType = registrySignedUserPage.getEventTypeInMyRegistry();
			Log.message("4. Before editing event type is '" + previousEventType + "' !", driver);
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnEventInfoTab();
			Log.message("6. Clicked on event Info Tab!");
			// Select EventType
			registryInformationPage.selectEventTypeNonSelected(previousEventType);
			Log.message("7. Selected the event type !");
			// click on Apply Button
			registryInformationPage.clickContinue();
			Log.message("8. clicked on Apply button !");
			// clicking on registry tab on headers
			homePage.headers.navigateToGiftRegistry();
			// Getting event type after editing
			UpdatedEventType = registrySignedUserPage.getEventTypeInMyRegistry();
			Log.message("9. After editing event type is '" + UpdatedEventType + "' !");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message("Edited Registry details should be updated in Registry screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(!(previousEventType.equals(UpdatedEventType)),
					"After editing in EventForm updated event is showing in Active Registry !",
					"After editing in EventForm updated event is not showing in Active Registry !", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_161

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Shipping info tab displayed in Edit Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_162(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> ShippingDetails = Arrays.asList("preEventShipping", "postEventShipping");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message("5. Clicked on Shipping Info Tab!");
			LinkedHashMap<String, String> PreShippingInfo = registrySignedUserPage.getPreEventShippingDetails();
			LinkedHashMap<String, String> preShipping = new LinkedHashMap<>();
			preShipping.put("AddressHome", "San Fransisco");
			preShipping.put("FirstName", "qa");
			preShipping.put("LastName", "qa");
			preShipping.put("Address", "654 market stre");
			preShipping.put("City", "San Fransisco");
			preShipping.put("State", "California");
			preShipping.put("PhoneNo", "4157460140");
			preShipping.put("Zipcode", "94105");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Shipping Information should be displayed in Shipping info tab");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(PreShippingInfo.equals(preShipping),
					"pre-shipping information is displaying in pre-shipping tab ! ",
					"pre-shipping information is not displaying in pre-shipping tab ! ", driver);
			LinkedHashMap<String, String> PostShippingInfo = registrySignedUserPage.getPostEventShippingDetails();
			LinkedHashMap<String, String> postShipping = new LinkedHashMap<>();
			postShipping.put("AddressHome", "San Fransisco-1");
			postShipping.put("FirstName", "qa");
			postShipping.put("LastName", "qa");
			postShipping.put("Address", "654 market street dk");
			postShipping.put("City", "San Fransisco");
			postShipping.put("State", "California");
			postShipping.put("PhoneNo", "4157460140");
			postShipping.put("Zipcode", "94105");
			Log.assertThat(PostShippingInfo.equals(postShipping),
					"post-shipping information is displaying in post-shipping tab ! ",
					"post-shipping information is not displaying in post-shipping tab ! ", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("PreEvent Shipping Information and Post event Shipping information should be displayed");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(ShippingDetails, registryInformationPage),
					"PreEvent Shipping Information and Post event Shipping information is displayed in shippingInfo Tab !",
					"PreEvent Shipping Information and Post event Shipping information is not displayed in shippingInfo Tab !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_162

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the information under PreEvent shipping", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_163(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> preShippingDetails = Arrays.asList("txtFirstNameInPreEventShipping",
				"txtLastNameInPreEventShipping", "txtAddress1InPreEventShipping", "txtPhoneInPreEventShipping");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message("5. Clicked on Shipping Info Tab!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"PreEvent Shipping information should be displayed with the following: frist Name, Last Name ,Address, Phone ");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(preShippingDetails,
							registryInformationPage),
							"PreEvent Shipping information displayed with the following: frist Name, Last Name ,Address, Phone ",
							"PreEvent Shipping information not displayed with the following: frist Name, Last Name ,Address, Phone ",
							driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_163

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the heading as 'Select or Add an Address' .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_164(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> SelectOrAddAddress = Arrays.asList("SelectOrAddAnAddress");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			// click on eventInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message("5. Clicked on Shipping Info Tab!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("'Select or Add an Address' should be displayed under the Preevent Shipping information .");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(SelectOrAddAddress,
							registryInformationPage),
							"'Select or Add an Address' displayed under the Preevent Shipping information .",
							"'Select or Add an Address' not displayed under the Preevent Shipping information .", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_164

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system allows the user to add address under the Pre-Event Shipping address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_165(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		Log.testCaseInfo(testData);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// ceate registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage !");
			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage !");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			// click on shippingInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". Clicked on Shipping Info Tab!");
			// filling pre-Event shipping Details
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message(i++ + ". Filled pre-shipping details !");
			// Get the pre-Event shipping info before saving
			LinkedHashMap<String, String> PreShippingInfoBeforeSaving = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". Successfully got the pre-Event Shipping info before saving !");
			// filling Post-Event shipping Details
			registrySignedUserPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message(i++ + ". Filled post-shipping details !");
			// Get the post-Event shipping info before saving
			LinkedHashMap<String, String> PostShippingInfoBeforeSaving = registrySignedUserPage
					.getPostEventShippingDetails();
			Log.message(i++ + ". Successfully got the pre-Event Shipping info before saving !");
			// click on Apply Button
			registryInformationPage.clickOnApplyButtonInShippingInfo();
			Log.message(i++ + ". Clicked on Apply Button !");
			// click on shippingInfo tab
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". Clicked on shippingInfo Tab !");
			// Get the pre-Event shipping info after saving
			LinkedHashMap<String, String> PreShippingInfoAfterSaving = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". Successfully got the pre-Event Shipping info after saving !");
			// Get the post-Event shipping info after saving
			LinkedHashMap<String, String> PostShippingInfoAfterSaving = registrySignedUserPage
					.getPostEventShippingDetails();
			Log.message(i++ + ". Successfully got the post-Event Shipping info after saving !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Customer should be allowed to add addresses to the gift registry address fields - Pre Event and Post Event.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(PreShippingInfoBeforeSaving.equals(PreShippingInfoAfterSaving),
					"Customers are allowed to add addresses to the gift registry address fields - Pre Event !",
					"Customers are not allowed to add addresses to the gift registry address fields - Pre Event !",
					driver);
			Log.assertThat(PostShippingInfoBeforeSaving.equals(PostShippingInfoAfterSaving),
					"Customers are allowed to add addresses to the gift registry address fields - Post Event !",
					"Customers are not allowed to add addresses to the gift registry address fields - Post Event !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_165

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can able to select the saved from PostEvent Shipping Information .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_166(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("dropdownForPreShipping");
		Log.testCaseInfo(testData);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// ceate registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage !");
			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage !");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			// click on shippingInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". Clicked on Shipping Info Tab!");
			// filling pre-Event shipping Details
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message(i++ + ". Filled pre-shipping details !");
			// Get the pre-Event shipping info before saving
			LinkedHashMap<String, String> PreShippingInfo = registrySignedUserPage.getPreEventShippingDetails();
			Log.message(i++ + ". Successfully got the pre-Event Shipping info before saving !");
			// click on UsePreEventAddress button
			registrySignedUserPage.clickOnUsePreEventAddress();
			Log.message(i++ + ". clicked on UsePreEventAddress button !");
			// Get the post-Event shipping info after clicking on
			// UsePreEventAddress button
			LinkedHashMap<String, String> AfterClikingUsePreEventAddress = registrySignedUserPage
					.getPostEventShippingDetails();
			Log.message(i++
					+ ". Successfully got the post-Event Shipping info after clicking on UsePreEventAddress button !");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("A dropdown with 'Select Saved Address' should be displayed to the user .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(element, registrySignedUserPage),
					"A dropdown with 'Select Saved Address' is displayed to the user .",
					"A dropdown with 'Select Saved Address' is not displayed to the user .", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"If the customer clicks the 'Use Pre Event Address' button then the Pre-Event address should be filled in Post Event Address.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(PreShippingInfo.equals(AfterClikingUsePreEventAddress),
					"After cliking on 'Use Pre Event Address' button then the Pre-Event address filled in Post Event Address.",
					"After cliking on 'Use Pre Event Address' button then the Pre-Event address not filled in Post Event Address.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_166

	@Test(groups = {
	"desktop" }, description = "Verify APO/FPO and Phone Assest tool tip is shown in Shipping Information", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_167(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {
			// ** Loading the test data from excel using the test case id */
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			String searchKey = testData.get("SearchKey");
			List<String> ApoFpoPreEvent = Arrays.asList("ApoFpoToolTipForPreEvent");
			List<String> PhonePreEvent = Arrays.asList("PhoneToolTipForPreEvent");
			List<String> ApoFpoPostEvent = Arrays.asList("ApoFpoToolTipForPostEvent");
			List<String> PhonePostEvent = Arrays.asList("PhoneToolTipForPostEvent");
			int i = 1;
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				// ceate registry account
				String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
				i = Integer.parseInt(details[3]);
				// Search the product with keyword
				SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
				Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage !");
				// select the product
				PdpPage pdpPage = searchResultPage.selectProduct();
				Log.message(i++ + ". Selected the product and Navigated to pdpPage !");
				pdpPage.selectColor();
				pdpPage.selectSize();
				pdpPage.clickOnRegistrySign();
				Log.message(i++ + ". Product added to my registry !");
				homePage.headers.navigateToGiftRegistry();
				RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
				// click on viewRegistry
				RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
				Log.message(i++ + ". Clicked on view Active Registry !");
				// click on shippingInfo
				registryInformationPage.clickOnShippingInfoTab();
				Log.message(i++ + ". Clicked on Shipping Info Tab!");
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b>");
				Log.message("APO/FPO  tool tip should be displayed in Pre-Event Shipping address.");
				Log.message("<br>");
				Log.message("<b>Actual Result 1:</b>");
				// Hover on ApoFpolnk In PreEvent shipping
				registrySignedUserPage.hoverOnApoFpolnkInPreEvent();
				Log.message(i++ + ". MouseHovered on ApoFpolnk In PreEvent shipping !");
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(ApoFpoPreEvent, registrySignedUserPage),
						"APO/FPO  tool tip  displayed in Pre-Event Shipping address .",
						"APO/FPO  tool tip not displayed in Pre-Event Shipping address .", driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b>");
				Log.message("PhoneAssest tool tip should be displayed in Pre-Event Shipping address.");
				Log.message("<br>");
				Log.message("<b>Actual Result 2:</b>");
				// Hover on phoneAssest In PreEvent shipping
				registrySignedUserPage.hoverOnPhoneAssestlnkInPreEvent();
				Log.message(i++ + ". MouseHovered on phoneAssestlnk In PreEvent shipping !");
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(PhonePreEvent, registrySignedUserPage),
						"PhoneAssest tool tip displayed in Pre-Event Shipping address .",
						"PhoneAssest tool tip not displayed in Pre-Event Shipping address .", driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 3:</b>");
				Log.message("APO/FPO  tool tip should be displayed in Post-Event Shipping address.");
				Log.message("<br>");
				Log.message("<b>Actual Result 3:</b>");
				// Hover on ApoFpolnk In PostEvent shipping
				registrySignedUserPage.hoverOnApoFpolnkInPostEvent();
				Log.message(i++ + ". MouseHovered on ApoFpolnk In PostEvent shipping !");
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(ApoFpoPostEvent, registrySignedUserPage),
						"APO/FPO  tool tip  displayed in Post-Event Shipping address .",
						"APO/FPO  tool tip not displayed in Post-Event Shipping address .", driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 4:</b>");
				Log.message("PhoneAssest tool tip should be displayed in Pre-Event Shipping address.");
				Log.message("<br>");
				Log.message("<b>Actual Result 4:</b>");
				// Hover on phoneAssest In PostEvent shipping
				registrySignedUserPage.hoverOnPhoneAssestlnkInPostEvent();
				Log.message(i++ + ". MouseHovered on phoneAssestlnk In PostEvent shipping !");
				Log.assertThat(
						registrySignedUserPage.elementLayer.verifyPageElements(PhonePostEvent, registrySignedUserPage),
						"PhoneAssest tool tip displayed in Pre-Event Shipping address .",
						"PhoneAssest tool tip not displayed in Pre-Event Shipping address .", driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			}
		} // finally
	}// TC_BELK_REGISTRY_167

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify shipping info added for the registry .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_168(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// ceate registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage !");
			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage !");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			// click on shippingInfo
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". Clicked on Shipping Info Tab!");
			// filling pre-event details
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message(i++ + ". filled pre-event shipping details !");
			// Get the pre-event shipping info before saving
			LinkedHashMap<String, String> PreShippingInfoBeforeSaving = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". sucessfully got the pre event shipping info before saving it !");
			// click on Apply button
			registryInformationPage.clickOnApplyButtonInShippingInfo();
			Log.message(i++ + ". clicked on Apply Button !");
			// click on shipping info tab
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". clicked on shipping info tab !");
			// Get the pre-event shipping info after modified
			LinkedHashMap<String, String> PreShippingInfoAfterModify = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". sucessfully got the pre event shipping info after modified it !");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("Added shipping address should be displayed in registry .");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(PreShippingInfoBeforeSaving.equals(PreShippingInfoAfterModify),
					"Added shipping address displayed in registry .",
					"Added shipping address not displayed in registry.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_168

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify page reverted to registry screen .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_169(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// ceate registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage !");
			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage !");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			// click on ShippingInfoTab
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". Clicked on Shipping Info Tab!");
			// Get the pre-event shipping info before saving
			LinkedHashMap<String, String> PreShippingInfoBeforeModifying = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". sucessfully got the pre event shipping info before modifying it !");
			// filling pre-event details
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message(i++ + ". filled pre-event shipping details !");
			// click on cancel button
			registryInformationPage.clickCancelInShippingInfo();
			Log.message(i++ + ". clicked on cancel Button !");
			// click on viewRegistry
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			// click on shipping info tab
			registryInformationPage.clickOnShippingInfoTab();
			Log.message(i++ + ". clicked on shipping info tab !");
			LinkedHashMap<String, String> PreShippingInfoAfterCancel = registrySignedUserPage
					.getPreEventShippingDetails();
			Log.message(i++ + ". sucessfully got the pre-event shipping info after modifiedand cancel it !");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message(
					"On click of Cancel button should not save any entered information, and revert back to the previous values entered.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(PreShippingInfoBeforeModifying.equals(PreShippingInfoAfterCancel),
					"On click of Cancel button sysstem is not saving any entered information, and revert back to the previous values entered.",
					"On click of Cancel button sysstem is saving entered information, and revert back with values entered.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_169

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Maximum Quantity displays in the Would Love drop down. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_093(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectSizeByIndex(4);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");

			String qtyinwouldlove = registrySignedUserPage.getWouldLoveSelectedvalue();

			Log.message("<b>Expected Result:</b>  Default display in the drop down box should be 1.");
			Log.assertThat(qtyinwouldlove.equals("1"),
					"<b>Actual Result1:</b>  Default display in the drop down box is 1.",
					"<b>Actual Result1:</b>  Default display in the drop down box is not 1", driver);
			int wouldlovecount = registrySignedUserPage.getWouldLoveCount();
			Log.message("<b>Expected Result:</b>  If inventory < 24, the system should display the range from 1 - ["
					+ wouldlovecount + "].");

			Log.assertThat(wouldlovecount < 24,
					"<b>Actual Result1:</b>  If inventory < 24, the system displays the range from 1 - ["
							+ wouldlovecount + "].",
							"<b>Actual Result1:</b>  If inventory < 24, the system  did not display the range from 1 - [inventory available].",
							driver);
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_093

	@Test(groups = { "desktop",	"mobile" }, description = "Verify system allows the customer to enter the quantity manually in the Would Love drop down. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_094(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Regiis");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("6. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("7. Color swatch Selected");
			pdpPage.selectSizeByIndex(2);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			String valuebeforeseelction = registrySignedUserPage.getWouldLoveSelectedvalue();
			registrySignedUserPage.selectWouldLoveIndex(4);
			Log.message("13. Changed quantity of Would Love dropdown");
			String valueafterseelction = registrySignedUserPage.getWouldLoveSelectedvalue();
			Log.message(
					"<b>Expected Result:</b> System should allow the customer to enter the quantity in the drop down");
			Log.softAssertThat(!valuebeforeseelction.equals(valueafterseelction),
					"<b>Actual Result1:</b> System allowed the customer to enter the quantity in the drop down.",
					"<b>Actual Result1:</b> System did not allow the customer to enter the quantity in the drop down.",
					driver);
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_094

	@Test(groups = { "desktop",	"mobile" }, description = "Verify registry displays the price of the product ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_098(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			String originalsalepriceinpdppage = pdpPage.originalPriceTxt().trim() + pdpPage.salePriceTxt().trim();
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			Log.message("<b>Expected Result:</b> The price should be displayed as, Org. $xx.xx Now $xx.xx.");
			String originalsalepriceinregistrypage = registryInformationPage.originalPriceTxt().trim()
					+ registryInformationPage.SalePriceTxt().trim();
			Log.softAssertThat(originalsalepriceinpdppage.equals(originalsalepriceinregistrypage),
					"<b>Actual Result1:</b> The price is displayed as[" + originalsalepriceinregistrypage + "]",
					"<b>Actual Result1:</b>The price is not displayed as Org. $xx.xx Now $xx.xx.", driver);
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// // TC_BELK_REGISTRY_098


@Test(groups = { "desktop",	"mobile" }, description = "Verify Priority drop displays in the Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_101(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		ArrayList<String> expected_Prioritylist = new ArrayList<String>();
		ArrayList<String> mylist = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(2);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");

			Log.message(
					"<b>Expected Result:</b> Priority drop down should be displayed with the following options None, Highest, High, Medium, Low, Lowest");

			expected_Prioritylist.add("Highest");
			expected_Prioritylist.add("High");
			expected_Prioritylist.add("Medium");
			expected_Prioritylist.add("Low");
			expected_Prioritylist.add("Lowest");

			mylist = registrySignedUserPage.getPriorityList();

			for (String priorityvalue : mylist) {

				Log.softAssertThat(expected_Prioritylist.contains(priorityvalue),

						"<b>Actual Result:</b> Priority drop down have '" + priorityvalue + "'",
						"<b>Actual Result:</b>" + priorityvalue + " is not displayed in the 'Priority' drop down",
						driver);
			}
			Log.message(
					"<b>Expected Result:</b> Products should receive a default value of none unless otherwise overwritten");
			String defaultvalueforpriority = registrySignedUserPage.getPrioritySelectedvalue();
			Log.softAssertThat(defaultvalueforpriority.equals("None"),
					"<b>Actual Result1:</b> Products default value is none unless otherwise overwritten",
					"<b>Actual Result1:</b> Products default value is not none unless otherwise overwritten", driver);
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_101

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify update and Remove links are displayed in Registry With Products .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_102(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(2);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
	
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			Log.message("<b>Expected Result:</b> Update and remove links should be available.");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("linkUpdate", "linkRemove"),
							registryInformationPage),
							"<b>Actual Result1:</b> Update and remove links is available.",
							"<b>Actual Result1:</b>  Update and remove links is not available.", driver);
	
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}
	
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_102

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Low Stock Message in Registry With Products .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_103(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			String pdpstockmessage = pdpPage.StockTxt();
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			String stockmsg = registryInformationPage.StockTxt();
			Log.message("<b>Expected Result:</b> Low Stock Message should be displayed");
			Log.assertThat(stockmsg.equals(pdpstockmessage), "<b>Actual Result1:</b> Low Stock Message is displayed.",
					"<b>Actual Result1:</b>  Low Stock Message is not displayed.");

			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_103

	// TC_BELK_REGISTRY_104
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Add to Shopping bag button in Registry with product page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_104(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[0]);
			Log.message("7. Navigated to PDP Page from Search Results Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQuantity();
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickOnRegistrySign();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			registrySignedUserPage.clickAddItemsToRegistry();
			homePage.headers.searchAndNavigateToPDP(searchKey[1]);
			Log.message("13. Navigated to PDP Page from Search Results Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			Log.message("14.  Size is Selected from size dropdown");
			pdpPage.selectQuantity();
			Log.message("15. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickOnRegistrySign();
			Log.message("16. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("17. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}			
			Log.message("18. Clicked View from the Active Registry");
			
			String Productname1 = registrySignedUserPage.getProductName();
			registrySignedUserPage.addItemToBag(Productname1);
			ShoppingBagPage shoppingBagPage = registrySignedUserPage.clickMiniCart();
			
			Log.message("<br>");
			
			Log.message("<b> Expected Result :</b> Product shoud be added to shopping bag from registry ");
			
			Log.assertThat((shoppingBagPage.getProductName(Productname1)), 
					"<b> Actual Result :</b> Product is added to shopping bag", 
					"<b> Actual Result :</b> Product is not added to shopping bag");
			
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_104

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Out Of Stock Items message is shown .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_105(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			SearchResultPage searchResultPagehomePage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPagehomePage.selectProductByIndex(1);
			Log.message("8. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("13. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("14. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();

			Log.message(
					"<b>Expected Result:</b> Out Of Stock Items should be displayed in the Registry with product page.");
			Log.message("No Test Data to Proceed");

			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_105

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify edit registry modal is displays for the customer .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_106(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6.Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Selected color");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();

			registryInformationPage.clickProductName(1);

			Log.message("14. Clicking on Product Name");
			Log.message(
					"<b>Expected Result:</b> Edit registry modal should be opened when a user clicks/taps the Product Name");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("btnUpdateRegistry"),
							registryInformationPage),
							"<b>Actual Result:</b> Edit registry modal is opened when a user clicks/taps the Product Name",
							"<b>Actual Result:</b> Edit registry modal is not opened when a user clicks/taps the Product Name ",
							driver);
			registryInformationPage.clickUpdateRegistry();
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_106

	// TC_BELK_REGISTRY_108
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify size chart modal displays on the screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_108(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			SearchResultPage searchResultPagehomePage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPagehomePage.selectProductByIndex(1);
			Log.message("8. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSize();
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQuantity();
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickProductName(1);
			Log.message("14. Clicking on Product Name");
			registryInformationPage.selectColorByIndex(3);
			Log.message("15. Changed Sku Color");
			String exceptedcolor = registryInformationPage.ColorTxtInEditPopup();
			registryInformationPage.clickUpdateRegistry();
			Log.message("16. Clicking Update registry button");
			/*
			 * String actualcolor = registryInformationPage.ColorTxt();
			 * Log.message(
			 * "<b>Expected Result:</b> Choosed changes should be updated in the Registry"
			 * ); Log.assertThat(exceptedcolor.equals(actualcolor),
			 * "<b>Actual Result:</b> Color Sku got updated",
			 * "<b>Actual Result:</b>  Color Sku did not get updated", driver);
			 */
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_108

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Update Registry button in the Edit modal of the Registry.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_109(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickProductName(1);
			Log.message("14. Clicking on Product Name");
			registryInformationPage.selectColorByIndex(3);
			Log.message("15. Changed Sku Color");
			String exceptedcolor = registryInformationPage.ColorTxtInEditPopup();
			registryInformationPage.clickUpdateRegistry();
			Log.message("16. Clicking Update registry button");
			String actualcolor = registryInformationPage.ColorTxt();
			Log.message("<b>Expected Result:</b> Choosed changes should be updated in the Registry");
			Log.assertThat(exceptedcolor.equals(actualcolor), "<b>Actual Result:</b> Color Sku got updated",
					"<b>Actual Result:</b>  Color Sku did not get updated", driver);
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_109

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify page redirected to Registry login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Navigated to Gift Registry Page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Page should be redirected to login registry page");
			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "giftregistry")
					&& giftRegistryPage != null,
					"<b>Actual Result:</b> Page is redirected to login registry page",
					"<b>Actual Result:</b> Page is not redirected to login registry page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_001

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify bread crumb is displayed in Registry login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Navigated to Gift Registry Page");
			List<String> breadCrumbText = giftRegistryPage.getTextInBreadcrumb();
			Log.message("3. Breadcrumb text is obtained");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Bread crumb should be displayed in login registry page as Home > Registry Login");
			Log.assertThat(giftRegistryPage.verifyBreadCrumb(breadCrumbText),
					"<b>Actual Result:</b> Bread Crumb of Registry Login is displayed in Registry Login Page",
					"<b>Actual Result:</b> Bread Crumb of Registry Login is not displayed in Registry Login Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_002

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system navigate to login page after clicking 'Add to Registry button'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// GiftRegistryPage giftRegistryPage =
			// homePage.navigateToRegistryPage();
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP page");
			String color = pdpPage.selectColor();
			Log.message("4. selected " + color + " color");
			String size = pdpPage.selectSize();
			Log.message("5. selected " + size + " size");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b>  The non-authenicated should be allowed to select 'Add to Registry'");
			Log.assertThat(pdpPage.verifyAddToRegistryLinkEnabled(),
					"<b>Actual Result 1:</b> The non-authenicated user are allowed to select 'Add to Registry'",
					"<b>Actual Result 1:</b> The non-authenicated user are not allowed to select 'Add to Registry'",
					driver);

			RegistryGuestUserPage registryGuestUserPage = pdpPage.navigateToRegistryAsGuest();
			Log.message("6. Clicked on 'Add To Registry' Link");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b>  After clicking Add To Registry, system should navigate to Login page");
			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "giftregistry")
					&& registryGuestUserPage != null,
					"<b>Actual Result 2:</b> After clicking Add To Registry, system navigated to Login page",
					"<b>Actual Result 2:</b> After clicking Add To Registry, system is not navigated to Login page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_004

	@Test(groups = {
	"desktop" }, description = "Verify system navigate to login page after clicking 'Add to Registry button' in Quick view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_005(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase is applicable for 'Desktop' only");
		} else {

			// ** Loading the test data from excel using the test case id */
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				// GiftRegistryPage giftRegistryPage =
				// homePage.navigateToRegistryPage();
				SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
				Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
				QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
				Log.message("3. Navigated to Quick View Page");
				String color = quickViewPage.selectColor();
				Log.message("4. selected " + color + "color");
				String size = quickViewPage.selectSize();
				Log.message("5. selected " + size + "size");
				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b>  The non-authenicated should be allowed to select 'Add to Registry'");
				Log.assertThat(quickViewPage.verifyAddToRegistryLinkEnabled(),
						"<b>Actual Result:</b> The non-authenicated user are allowed to select 'Add to Registry'",
						"<b>Actual Result:</b> The non-authenicated user are not allowed to select 'Add to Registry'",
						driver);

				RegistryGuestUserPage registryGuestUserPage = quickViewPage.navigateToRegistryAsGuest();
				Log.message("6. Clicked on 'Add To Registry' Link");

				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b>  After clicking Add To Registry, system should navigate to Login page");
				Log.assertThat(
						Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "giftregistry")
						&& registryGuestUserPage != null,
						"<b>Actual Result:</b> After clicking Add To Registry, system navigated to Login page",
						"<b>Actual Result:</b> After clicking Add To Registry, system is not navigated to Login page",
						driver);

				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}

	}// TC_BELK_GLOBALNAV_005

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify that the user is able to Login to see Wish List with valid Email and Password value.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_302(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn wishListSignInPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Navigated to wish list sign in page");

			WishListPage wishListPage = wishListSignInPage.signInToWishList(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should be navigated to their Wish List page, when valid details are provided.");
			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "Wishlist") && wishListPage != null,
					"<b>Actual Result:</b> User navigated to their Wish List page, provided the valid details",
					"<b>Actual Result:</b> User are not navigated to their Wish List page, provided the valid details",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_302

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify that the user is not able to Login to see Wish List with invalid Email and Password value", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_303(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn wishListSignInPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Navigated to wish list sign in page");
			WishListPage wishListPage = wishListSignInPage.signInToWishList(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Appropriate error message should be displayed when invalid Email Address or Password is given in Wish List Login fields.");
			Log.assertThat(
					(wishListPage.verifyWishListInvalidSignIn()
							.equals("Sorry, this does not match our records. Check your spelling and try again.")),
							"<b>Actual Result:</b> Error message is displayed, with invalid email address or password is given",
							"<b>Actual Result:</b> Error message is not displayed, with invalid email address or password is given",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_303

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Wish List Name in Wish List Results public view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_319(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String name[] = testData.get("SearchKey").split("\\|");
		String firstName = name[0];
		String lastName = name[1];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Navigated to wish list sign in page");
			WishListPage wishListPage = new WishListPage(driver).get();
			LinkedHashMap<String, String> findWishListDetails = wishListPage.fillFindWishList(firstName, lastName,
					emailid);
			Log.message("3. Entered Find Wish List Details");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The [firstName] [lastName] Wish List of the wish list, the User is viewing should be displayed in Wish List Results public view");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(wishListPage.verifyNameInFindWishList(findWishListDetails),
					"<b>Actual Result:</b> The User is viewing is displayed firstName and lastName in Wish List Results public view",
					"<b>Actual Result:</b> The User is viewing is not displayed firstName and lastName in Wish List Results public view",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_319

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Add Gift Cards' link direct the user to the Gift Card PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_081(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn myAccount = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SignIn Page");
			MyAccountPage myAccountPage = myAccount.signInToMyAccount(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			RegistrySignedUserPage registryPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
			Log.message("4. Navigated to Registry Signed User Page");
			RegistryInformationPage registryInformationPage = registryPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked view Link and Navigated to Registry Information Page");
			GiftCardsPage giftCardPage = registryInformationPage.clickOnAddGiftCard();
			Log.message("6. Clicked Add Gift card button and Navigated to Gift card Page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> 'Add Gift Cards' link should be direct the user to the Gift Card PDP.");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "belk-gift-card")
					&& giftCardPage != null,
					"'Add Gift Cards' link direct the user to the Gift Card PDP.",
					"'Add Gift Cards' link is not direct the user to the Gift Card PDP", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_081

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the deletion modal in the Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_039(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> elements = Arrays.asList("modalDeleteRegistry");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// / Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Navigate to My Registry Page
			registrySignedUserPage.clickOnDelete();
			Log.message("4. Clicked Delete On Registry Link!");
			// BrowserActions.nap(3);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Delete registry modal should be displayed with a resource message stating whether the customer wants to confirm deletion of the address for their account.");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(elements, registrySignedUserPage),
					"<b>Actual Result :</b> Deletion modal in the Registry screen is visible!",
					"<b>Actual Result :</b> Deletion modal in the Registry screen is not Visible!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_39

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify action buttons in the delete modal of the registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_040(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			// Navigate to My Registry Page
			registrySignedUserPage.clickOnDelete();
			Log.message("4. Clicked on delete link!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Added Registry Modal should be displayed in My registry screen with Yes and Cancel button.");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("modalDeleteRegistry"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b>  Cancel → closes the modal, maintains the registry in the Customer’s account and returns the customer to the My Registries page.!",
							"<b>Actual Result 1:</b>  Cancel → not closes the modal, maintains the registry in the Customer’s account and not returns the customer to the My Registries page.!",
							driver);
			Log.message("<br>");
			// Navigate to My Registry Page
			registrySignedUserPage.clickOnCancel();
			Log.message("5. Clicked on Cancel Button in Pop Up!");

			// Navigate to My Registry Page
			registrySignedUserPage.clickOnDelete();
			Log.message("6. Clicked on delete link!");

			// Navigate to My Registry Page
			registrySignedUserPage.clickOnYesInPopUp();
			Log.message("7. Clicked on Yes Button in Pop Up!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Added Registry Modal should be displayed in My registry screen with Yes and Cancel button.");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Yes → successfully deletes the registry from the Customer’s account, closes the modal and returns the customer to the My Registries page.!",
							"<b>Actual Result 1:</b> Yes → unsuccessfully not deletes the registry from the Customer’s account, closes the modal and not returns the customer to the My Registries page.!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_40

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Create New Registry button displaysin MY Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_041(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Added Registry Modal should be displayed in My registry screen with Yes and No button!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Create New Registry button is displayed in MY Registry screen!",
							"<b>Actual Result 1:</b> Create New Registry button is not  displayed in MY Registry screen!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_41


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Account Help Content Asset is displayed My Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_044(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Account Help Content Asset should be displayed in My Registry screen!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("accountContent"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' Account Help Content Asset is not displayed!",
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' Account Help Content Asset is not displayed!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_44

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify social sharing links are displayed in My register screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_057(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();

			giftRegistryPage.clickOnRegistryPanel();
			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked 'View link' in the Gift registry page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Social sharing lnks should be displayed in My register screen!");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("facebookTab", "twitterTab",
							"googleplusTab", "pinternetTab", "emailTab", "linkTab"), giftRegistryPage),
							"<b>Actual Result 1: </b> Navigated to 'My Registry screen' and Social sharing links are displayed!",
							"<b>Actual Result 1: </b> Navigated to 'My Registry screen' and Social sharing links are not displayed!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_57

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify auto generated bar code is displayed in My Register screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_056(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			// Clicked On View Link Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked On View Link Button!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Auto generated bar code should be displayed in My Register screen!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryBarCode"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated bar code is displayed!",
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated bar code is not displayed!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_56

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify auto generated Registry Id is displayed in My Register screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_055(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			// Clicked On View Link Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked On View Link Button!");

			// BrowserActions.nap(5);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Auto generated Registry Id should be displayed in My Register screen!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryId"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated Registry Id is displayed!",
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated Registry Id not  displayed!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_55

	
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify “Add Items to Registry” link direct the user to the Homepage", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_320(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createWishListAccount(homePage, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Wish List' user");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with keyword '" + searchKey + "' and Navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message(i++ + ". Navigated to 'Pdp' Page with randomly selected product");

			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			String upc = pdpPage.getUPCValue();
			Log.message(i++ + ". Get the Upc value in the PDP page");

			String price = pdpPage.getProductPrice();
			Log.message(i++ + ". Get the productPrice: '" + price + "' in the pdp page");

			pdpPage.clickAddToWishListLink();
			Log.message(i++ + ". Sucessfully added product to wishlist");

			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message(i++ + ". Navigated to 'WishList' Page");

			wishListPage.findWishList(details[0], details[1], details[2]);
			Log.message(i++ + ". Finded the Wishlist Based on the combination FirstName: '" + details[0]
					+ "' Last Name: '" + details[1] + "' emailId: '" + details[2]);

			wishListPage.clickOnViewLink();
			Log.message(i++ + ". Clicked 'View button' in the Find a registry page");

			int size = wishListPage.getProductDetailsInWishList().size();
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Product details(product name, upc, size, color, quantity, price) in wish list should be same as in PDP Page");
			Log.message("<b>Actual Result:</b>");
			for (int j = 0; j < size; j++) {
				LinkedHashMap<String, String> productDetails = wishListPage.getProductDetailsInWishList().get(j);
				Log.assertThat(productDetails.get("ProductName").contains(productName),
						"1. Product details - product name " + productDetails.get("ProductName")
						+ "' in Wish List is displayed same as '" + productName + "' in PDP Page",
						"Product details - product name " + productDetails.get("ProductName")
						+ "' in Wish List is not displayed same as '" + productName + "' in PDP Page",
						driver);
				Log.assertThat(productDetails.get("Upc").contains(upc),
						"2. Product details - product upc " + productDetails.get("Upc")
						+ "' in Wish List is displayed same as '" + upc + "' in PDP Page",
						"Product details - product upc " + productDetails.get("Upc")
						+ "' in Wish List is not displayed same as '" + upc + "' in PDP Page",
						driver);
				Log.assertThat(productDetails.get("Color").contains(selectedColor),
						"3. Product details - product color " + productDetails.get("Color")
						+ "' in Wish List is displayed same as '" + selectedColor + "' in PDP Page",
						"Product details - product color " + productDetails.get("Color")
						+ "' in Wish List is not displayed same as '" + selectedColor + "' in PDP Page",
						driver);
				Log.assertThat(productDetails.get("Size").contains(selectedSize),
						"4. Product details - product size " + productDetails.get("Size")
						+ "' in Wish List is displayed same as '" + selectedSize + "' in PDP Page",
						"Product details - product size " + productDetails.get("Size")
						+ "' in Wish List is not displayed same as '" + selectedSize + "' in PDP Page",
						driver);
				Log.assertThat(productDetails.get("Price").contains(price),
						"5. Product details - product Price " + productDetails.get("Price")
						+ "' in Wish List is displayed same as '" + price + "' in PDP Page",
						"Product details - product Price " + productDetails.get("Price")
						+ "' in Wish List is not displayed same as '" + price + "' in PDP Page",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_320

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Forgot password link in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_304(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.headers.navigateToWishList();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);
			SignIn signinPage = new SignIn(driver).get();
			signinPage.clikForgotPwd();
			Log.assertThat(signinPage.verifyforgotpasswordmodalwindow(),
					"<b> Actual Result:</b> Forgot Password Popup window is displayed",
					"<b> Actual Result:</b> Forgot Password Popup window is not displayed");
			Log.message("3. Clicked on ForgotPassword link");
			Log.message("4. ForgotPassword modal window is displayed");

			// Click on Cancel button in Forgot password modal window
			signinPage.clickOnCancelForgotPassword();
			Log.message("4. Click on Cancel button in Forgot password modal window");
			Log.assertThat(signinPage.verifySigninPage(), "<b> Forgot password modal window is closed</b>",
					"<b> Forgot Password modal window is not closed</b>");

			// Click on Forgot password Link
			signinPage.clikForgotPwd();
			signinPage.enterForgotPasswordEmail(emailid);
			signinPage.clickOnSendForgotPassword();
			Log.assertThat(signinPage.verifyPasswordResetSuccessModal(),
					"<b>Actual Result: Password has been reset successfully</b>",
					"<b> Actual Result: Password is not reset Failure</b>", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Remember Me checkbox in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_305(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			// Select the Remember Me check box
			signinPage.clickOnRememberMecheckBox();
			Log.message("3. Remember Me Check has been selected");

			// Signin to the application with valid User name and Password
			signinPage.signInToWishList(emailid, password);
			Log.message("3. Entered Valid credentials( Email ID:" + emailid + "/ Password:" + password
					+ ") in 'Sign In' page", driver);

			((JavascriptExecutor) driver).executeScript("window.open();");
			driver.close();
			ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(newTab.get(0));
			Log.message("6. Closed the browser and opening a new window on same browser", driver);
			driver.get(webSite);
			homePage.headers.clickMobileHamburgerMenu();
			Log.assertThat(Utils.waitForElement(driver, homePage.headers.lnkLoginLogoutMobileSignInUser),
					"User is not logged out since while login to the application Remember Me check is selected",
					"User is not logged in");

			Utils.waitForPageLoad(driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

		// Need to confirm with manual Team regarding session time out
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Login button in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_306(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			// Signin to the application with valid User name and Password
			signinPage.signInToWishList(emailid, password);

			Log.message("3. Entered Valid credentials(Email ID:" + emailid + "/Password:" + password
					+ ") in 'WishList' page", driver);
			Log.message("4. WishList Page is displayed.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Find Wish List text in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_307(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked WishList link on header and navigated to 'WishList' Page!", driver);
			Log.assertThat((wishlistpage.gettxtFindWishList().equalsIgnoreCase("Find A Wish List")),
					"<b>Actual Result: 'Find a Wish List' text is displayed</b>",
					"<b> Actual Result: 'Find a Wish List' text is not displayed in the WishList page");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the fields in Find Wish List section in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_308(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked WishList link on header and navigated to 'WishList' Page!", driver);

			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("txtWishListSearchFirstName"),
							wishlistpage),
							"<b>Actual Result : </b>\"First Name text Field is displayed!",
							"<b>Actual Result : </b>\"First Name text Field is not displayed.", driver);

			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("txtWishListSearchLastName"),
							wishlistpage),
							"<b>Actual Result : </b>\"Last Name text Field is displayed!",
							"<b>Actual Result : </b>\"Last Name text Field is not displayed.", driver);

			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("txtWishListSearchEmail"), wishlistpage),
					"<b>Actual Result : </b>\"WishList Search Email button is displayed!",
					"<b>Actual Result : </b>\"WishList Search Email  button is not displayed.", driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify clicking on Find Wish List button with valid details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_309(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigate to WishList Page");

			// Search the wishlist based on Email Address
			wishlistpage.wishListSearch(emailid);
			Log.message("3. Search the wishlist based on Email Address:" + emailid);

			// Click on View link in the Wishlist search
			wishlistpage.navigateToWishListResults();
			Log.message("4. Click on View link in the Wishlist search");

			// Verify Search Results is displayed or not
			wishlistpage.validateWishListSearch();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify New Customer text in Wish List login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_310(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigate to WishList Page");

			Log.assertThat(wishlistpage.CREATENEWTEXT.equals(wishlistpage.gettextCreateNewAccount()),
					"Create New Account information text is displayed in wishlist page",
					"Create New Account informaiton text is not displayed in wishlist page");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify clicking on Create Account button in Wish List login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_311(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to WishList page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigate to WishList Page");

			// Navigate to Create Account Page
			CreateAccountPage createaccount = wishlistpage.clickOnCreateAccount();
			Log.assertThat(createaccount.elementLayer.verifyPageElements(Arrays.asList("txtFirstName"), createaccount),
					"<b>Actual ResultL:</b> Create Account Page is loaded successfully",
					"<b>Actual Result:</b> Create Account Page is not loaded successfully");
			Log.message("3. Navigate to Create Account Page");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed for create registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_229(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
			SignIn signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Sign in to 'Registry' with user id and password
			RegistrySignedUserPage registersignIn = signinPage.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Clicked on 'Create New Registry' button
			registersignIn.clickOnCreateNewRegistry();
			Log.message("4. Clicked on 'Create New Registry' button");

			String breadcrumbTextActual[] = registersignIn.getTextFromBreadCrumb().trim().split("\\n");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>' Verify registry bread crumbs is displayed for create registry");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;
					Log.message("" + breadCrumbData[i] + "---" + breadcrumbTextActual[i]);
				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_229

	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed when Find a Registry: Unauthenticated", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_230(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
			SignIn signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Finding Someone else registry as a 'Guest' user
			signinPage.registrySignedUserPage.findRegistry("belk", "registry7", "Wedding Registry");
			Log.message("3. Entered credentials to 'Find Someone's else' registry as 'Guest' user");

			String breadcrumbTextActual[] = signinPage.registrySignedUserPage.getTextFromBreadCrumb().trim()
					.split("\\n");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify registry bread crumbs is displayed when Find a Registry: Unauthenticated");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;
					Log.message("" + breadCrumbData[i] + "---" + breadcrumbTextActual[i]);
				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_230

	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed when Find a Registry: No Results", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_231(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
			SignIn signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Sign in to 'Registry' with user id and password
			RegistrySignedUserPage registersignIn = signinPage.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Finding Someone else registry as a 'Guest' user
			signinPage.registrySignedUserPage.findRegistry("belk", "registry58", "Wedding Registry");
			Log.message("4. Entered credentials to 'Find Someone's else' registry as 'SignedIn' user");

			String[] breadcrumbTextActual = registersignIn.getTextFromBreadCrumb().trim().split("\\n");
			Log.message("" + breadcrumbTextActual);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>' Verify registry bread crumbs is displayed when Find a Registry");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;
					Log.message("" + breadCrumbData[i] + "---" + breadcrumbTextActual[i]);
				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_231

	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed when My Registry - View My Registries", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_232(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
			SignIn signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Sign in to 'Registry' with user id and password
			RegistrySignedUserPage registrySignedUserPage = signinPage.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Clicked on 'View' registry link
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on 'View' link in the 'Registry' page!");

			String[] breadcrumbTextActual = signinPage.registrySignedUserPage.getTextFromBreadCrumb().trim()
					.split("\\n");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify registry bread crumbs is displayed when My Registry - View My Registries");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;
					Log.message("" + breadCrumbData[i] + "---" + breadcrumbTextActual[i]);
				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_232

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Select a Registry modal displays after the click of Add to Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_116(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign in Page");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigated to PdpPage using the search key : " + searchKey);

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("5. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("6. selected size :" + size);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("7. Clicked on Registry link ");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 2: </b> Select a Registry modal should be displayed, if the customer is logged in and has a multiple registry");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("popUpRegistryBox"), pdpPage),
					"<b> Actual Result 2: Registry Modal are displayed when the customer logged in with multiple registry",
					"<b> Actual Result 2: </b> Registry Modal are not displayed when the customer logged in with multiple registry",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_116

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Apply and Cancel button is displayed Add registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_120(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to sign in Page");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);
			// Load the SearchResult Page with search keyword

			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigated to PdpPage using the search key : " + searchKey);
			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("5. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("6. selected size :" + size);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("7. Clicked on Registry link");

			
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> Selected registry should be added with the items while clicking on Apply button");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnApplyButton"), pdpPage),
					"<b> Acual Result 1: </b> The Apply button is displayed and the product Add to the registry",
							
							"<b> Actual Result 1: </b>  Registry Modal is not displayed and the Apply button is not displayed ",
							driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 2: </b> Selected registry should not be added with the items while clicking on Cancel button");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnCancelOnRegistryModal"), pdpPage),
					"<b> Acual Result 2: </b> The Cancel button is displayed and click on the cancel button product not added to the Registry ",
					"<b> Actual Result 2: </b>  Registry Modal is not displayed and the Cancel button is not displayed ",
					driver);

			Log.message("<br>");
			pdpPage.clickCancelButton();
			Log.message("9. Click on the cancel button on the registry modal ");
			String titleOfPage = driver.getTitle();
			Log.message("10. Navigated to the PDP page after clicked on the Cancel button and the titiel is :"
					+ titleOfPage, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_120

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify addresses saves to the Address Book.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_133(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Login to the Sign in registry");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the Create New Registry");
			registrySignedUserPage.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			Log.message("5. Fill the Registry Form to create the Profile ", driver);
			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Continue in the registry as a login user");
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message("7. Add the Pre Event shipping address");
			registrySignedUserPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message("8. Add the Post Event shipping address");
			registrySignedUserPage.clickOnContinueButtonInEventShipping();
			Log.message("9. Click on the Continue Button In the Event Shipping");

			if (runPltfrm == "desktop") {
				registrySignedUserPage.clickOnAddressBookLeftNav();
				Log.message("9. Click on the Address book on the Left Nav in ''Step 3-'");
				String txtShippingAddres = registrySignedUserPage.txtSavedAdddress();
				Log.message("10. Click on the Left Nav for the Address book");
				String getAddressBookURL = driver.getCurrentUrl();

				Log.message("11. Navigated to the Address book Page and the URL is:" + getAddressBookURL);
				Log.message("<br>");
				Log.message(
						"<b> Expected Result 1: </b> Address added in the gift registry should be saved to the address book.");
				Log.message("<b> Actual Result 1: </b> The Saved address in the Address book is :" + txtShippingAddres,
						driver);
			}

			if (runPltfrm == "mobile") {
				registrySignedUserPage.clickMobileHamburgerMenu();
				Log.message("9. Click on the Address book on the Left Nav in ''Step 3-'");
				registrySignedUserPage.clickMyAccount();
				Log.message("10. Click on the My account page from the Hmburger icon");
				String txtShippingAddres = registrySignedUserPage.txtSavedAddressMobile();
				Log.message("10. Click on the Address mobile fromHamburger");
				String getAddressBookURL = driver.getCurrentUrl();
				Log.message("11. Navigated to the Address book Page and the URL is:" + getAddressBookURL);
				Log.message("<br>");
				Log.message(
						"<b> Expected Result 1: </b> Address added in the gift registry should be saved to the address book.");
				Log.message("<b> Actual Result 1: </b> The Saved address in the Address book is :" + txtShippingAddres,
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_133

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Title and Sub-title displays in the Select Registry modal..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_117(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign in Page");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigated to PdpPage using the search key : " + searchKey);

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("5. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("6. selected size :" + size);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("7. Clicked on Registry link", driver);

			String registrySelect = pdpPage.registrySelectOnSelectModal();
			Log.message("The Multiple registry message is :" + registrySelect);
			String registrySubtitleSelect = pdpPage.registrySubtitleSelectOnSelectModal();
			Log.message("The Multiple registry subtitle message is :" + registrySubtitleSelect);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> 'Select a Registry' title should be displayed on the Select Registry modal..");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSelectRegistry"), pdpPage),
					"<b> Acual Result 1: </b> Registry Modal is displayed and the heading is: " + registrySelect,
					"<b> Actual Result 1: </b>  Registry Modal is not displayed as a logged in user", driver);
			Log.message("<br>");

			Log.message(
					"<b> Expected Result 2: </b> 'Select a registry you would like to add item' sub title should be displayed in the Select Registry modal.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSubtitleSelectRegistry"), pdpPage),
					"<b> Acual Result 2: </b> Registry Modal is displayed and the Subheading heading is: "
							+ registrySubtitleSelect,
							"<b> Actual Result 2: </b>  Registry Modal is not displayed as a logged in user", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_117

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify details under Registrant Information heading.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_123(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			Log.message("5. Event information heading should be displayed with the following details");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> Create Registrant heading should be displayed with the form values for Registrant and CoRegistrant.");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtRegistrantHeading", "txtCoRegistrantHeading"), registrySignedUserPage),
							"<b> Actual Result 1: </b> 'Registrant and Co-Registrant' headings are displayed in the Create registry Registrant form",
							"<b> Actual Result 1: </b> 'Registrant and Co-Registrant' headings are not  displayed in the Create registry Registrant form",
							driver);
			Log.message("<br>");
			String getDropDownValues = registrySignedUserPage.getDropDownBoxRole();
			Log.message("<br>");
			Log.message("<b> Expected Result 2: <b> Role, Drop down should be included with Bride, Groom");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtRole"),
							registrySignedUserPage),
							"<b> Actual Result 2: </b> Event Role  is displayed and drop down values are : "
									+ getDropDownValues,
									"<b> Actual Result 2: </b> Event Type is not visible on the form", driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 3: </b>  The Last Name, Text box  are visible on the create registrant heading");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtLastNameRegistry", "txtBoxLastName"), registrySignedUserPage),
							"<b> Actual Result 3: </b> Event Last Name text and Event Last  Name Texybox is displayed ",
							"<b> Actual Result 3: </b> Event Last Name text and Event Last Name Texybox is not displayed",
							driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 4: </b>  The Last Name, Text box  are visible on the create registrant heading");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("txtEmailInForm", "txtBoxEmailInForm"), registrySignedUserPage),
							"<b> Actual Result 4: </b> Email text field and Email textbox field is displayed ",
							"<b> Actual Result 4: </b> Email text field and Email textbox field are not  displayed", driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_123

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify error message displays in Create Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_126(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			registrySignedUserPage.clickOnContinueButton();
			Log.message("4. Click on the Continue button on the Event Page");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b>  The form field should be highlighted in red with an error stating that 'The field is required field' in the Event Information Form");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("errorMessageEventtype", "errorMessageEventFirstName",
									"errorMessageEventLastName", "errorMessageEventCity", "errorMessageState"),
									registrySignedUserPage),
									"<b> Actual Result 1: </b> 'The Required Field is visible in the Event Form if the form is blank ",
									"<b> Actual Result 1: </b> The Required Field is not visible in the Event Form.", driver);
			Log.message("<br>");

			Log.message(
					"<b> Expected Result 2: <b>  global message 'To complete the process of creating your registry, please review the information below click Submit'.");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtGlobalMessage"),
							registrySignedUserPage),
							"<b> Actual Result 2: </b> Global message is visible in the Event Registrant Form if the form is blank ",
							"<b> Actual Result 2: </b> The Global message is not visible in the Event Registrant Form.",
							driver);
			Log.message("<br>");
			Boolean result = registrySignedUserPage.verifyGlobalErrorMessageColour();
			Log.message("5. Global message is in red color and the result is: " + result, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_126

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system allows user to Email My Registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchkey = testData.get("SearchKey");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message(i++ + ". Navigated to 'Pdp' Page with productID");

			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			String upc = pdpPage.getUPCValue();
			Log.message(i++ + ".  Get the upc: '" + upc + "' in the pdp page");

			String price = pdpPage.getProductPrice();
			Log.message(i++ + ". Get the productPrice: '" + price + "' in the pdp page");

			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Sucessfully added product to Registry");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked view link button in the gift registry page");

			giftRegistryPage.clickOnMakeThisList();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify user can select private and public list from My Register screen");

			Log.assertThat(giftRegistryPage.makeThisListButtonType(),
					"<b>Actual Result:</b> 'Make This List Private' button displayed after clicking on button",
					"<b>Actual Result:</b> 'Make This List Private' button not displayed after clicking on button",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_060

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system hides the Private Gift Registries.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_062(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchkey = testData.get("SearchKey");
		List<String> registry = Arrays.asList("lblEventName", "lblRegistryId", "lblBarCode");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			homePage = giftRegistryPage.clickOnAddItemToRegistryButton();
			Log.message(i++ + ". Clicked on 'Add item to registry button' in the gift registry page");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message(i++ + ". Navigated to 'Pdp' Page with productID");

			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			String upc = pdpPage.getUPCValue();
			Log.message(i++ + ".  Get the upc: '" + upc + "' in the pdp page");

			String price = pdpPage.getProductPrice();
			Log.message(i++ + ". Get the productPrice: '" + price + "' in the pdp page");

			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Sucessfully added product to Registry");

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked view link button in the gift registry page");

			giftRegistryPage.clickOnMakeThisList();
			Log.message(i++ + ". Clicked 'Make this Private' button in the Gift Registry Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result</b> The event name, event ID, and barcode should be displayed, If a customer attempts to access a private registry.");

			Log.assertThat(giftRegistryPage.elementLayer.verifyPageElements(registry, giftRegistryPage),
					"<b>Actual Result:</b> 'registryInfo' is displayed on the 'Registry' page",
					"<b>Actual Result:</b>'registryInfo' displayed on the 'Registry' page", driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_062

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user allows to select Private Gift Registries.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_061(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchkey = testData.get("SearchKey");
		// List<String> registry = Arrays.asList("lblEventName",
		// "lblRegistryId", "lblBarCode");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			homePage = giftRegistryPage.clickOnAddItemToRegistryButton();
			Log.message(i++ + ". Clicked on 'Add item to registry button' in the gift registry page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchkey);
			Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to PDP Page");
			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Sucessfully added product to Registry");

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked view link button in the gift registry page");

			giftRegistryPage.clickOnMakeThisList();
			Log.message(i++ + ". Clicked 'Make this Private' button in the Gift Registry Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The social share icons should not displayed and the button text should appear as “Make This List Public” after clicking on the button. ");

			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("facebookTab",
							"twitterTab", "googleplusTab", "pininterestTab", "emailTab", "linkTab"), giftRegistryPage),
							"<b>Actual Result :</b> 'social share icons'  not displayed on the 'Registry' page",
							"<b>Actual Result :</b>'social share icons' is displayed on the 'Registry' page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_061


	@Test(groups = {
	"desktop" }, description = "Verify user can print registry from My register screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_063(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase is applicable for 'Desktop' only");
		} else {
			// ** Loading the test data from excel using the test case id */
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

			String registryInfo = testData.get("RegistryInfo");
			String shippingInfo = testData.get("ShippingInfo");
			String searchkey = testData.get("SearchKey");

			int i = 1;
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message(i++ + ". Navigated to 'Belk' Home Page!");

				String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
				i = Integer.parseInt(details[3]);
				Log.message(i++ + ". Successfully created 'Registry' user");

				GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

				homePage = giftRegistryPage.clickOnAddItemToRegistryButton();
				Log.message(i++ + ". Clicked on 'Add item to registry button' in the gift registry page");

				PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchkey);
				Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to PDP Page");

				String productName = pdpPage.getProductName();
				Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

				String selectedColor = pdpPage.selectColor();
				Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

				String selectedSize = pdpPage.selectSize();
				Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

				pdpPage.clickOnRegistrySign();
				Log.message(i++ + ". Sucessfully added product to Registry");

				giftRegistryPage = homePage.headers.navigateToGiftRegistry();
				Log.message(i++ + ". Navigate to Registry page", driver);

				giftRegistryPage.clickOnViewRegistrylnk();
				Log.message(i++ + ". Clicked view link button in the gift registry page");

				Log.message("<br>");
				Log.message("<b>Expected Result :</b> Verify user can print registry from My register screen.");
				Log.assertThat(
						giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("iconPrint"), giftRegistryPage),
						"<b>Actual Result :</b> 'Print icons' is displayed on the 'Registry' page!",
						"<b>Actual Result :</b> 'Print icons' not displayed on the 'Registry' page!", driver);

				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_REGISTRY_063

	@Test(groups = {
	"mobile" }, description = "Print icon should not be displayed in mobile.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_064(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "desktop") {
			throw new SkipException("Verify for mobile only");
		}
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchkey = testData.get("SearchKey");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			homePage = giftRegistryPage.clickOnAddItemToRegistryButton();
			Log.message(i++ + ". Clicked on 'Add item to registry button' in the gift registry page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchkey);
			Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to PDP Page");

			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Get the productName: '" + productName + "' in the pdp page");

			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected color: '" + selectedColor + "' from color swatched in the pdp page");

			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Selected size: '" + selectedSize + "' from size drop down in the pdp page");

			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Sucessfully added product to Registry");

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked view link button in the gift registry page");

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Print icon should not be displayed in mobile.");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("iconPrint"),
							giftRegistryPage),
							"<b>Actual Result:</b> 'Print icons' not displayed on the 'Registry' page",
							"<b>Actual Result:</b> 'Print icons' displayed on the 'Registry' page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_064

	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed when My Registry - Empty Registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_233(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
			SignIn signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Sign in to 'Registry' with user id and password
			RegistrySignedUserPage registrySignedUserPage = signinPage.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered valid user Credentials:" + emailid + "and" + password);

			// Finding Someone else registry as a 'Guest' user
			signinPage.registrySignedUserPage.findRegistry("Belk", "Registry59", "Wedding Registry");
			Log.message("4. Entered credentials to 'Find Someone's else' registry as 'SignedIn' user");

			// Clicked on 'View' registry link
			registrySignedUserPage.clickOnViewlinkFindRegistry();
			Log.message("5. Clicked on 'View' link in the 'Registry' page!");

			String[] breadcrumbTextActual = signinPage.registrySignedUserPage.getTextFromBreadCrumb().trim()
					.split("\\n");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify registry bread crumbs is displayed when My Registry - Empty Registry");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;

				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_233

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify product details are displayed in purchases tab", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_172(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("productDetailsInPurchaseTab");
		int i = 1;
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message(i++ + ". Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			// click on registry from header
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Clicked on registry link from header !");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			registryInformationPage.clickOnAddtoBag();
			Log.message(i++ + ". Product added to Shopping Bag!");
			// click on Minicart
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". clicked on minicart Button!");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message(i++ + ". clicked on checkout Button!");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address4");
			Log.message(i++ + ". filled shipping details !");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". clicked on continue Button !");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_AmericanExpress");
			Log.message(i++ + ". filled card detais !");
			checkOutPage.clickOnContinueInBilling();
			Log.message(i++ + ". clicked on continue Button !");
			// click on placeOrder Button
			checkOutPage.ClickOnPlaceOrderButton();
			Log.message(i++ + ". clicked on placeOrder Button !");
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". clicked on registry from header !");
			// click on viewRegistry
			registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			registryInformationPage.clickOnPurchasesTab();
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message("Below product details should be displayed in Purchases tab: ");
			Log.message("Product image should reflect variation of the registry item purchased");
			Log.message("Product name should leverage the {brand} {name} values and UPC .");
			Log.message("Color should be displayed as color name");
			Log.message("Size and variation .");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElements(element, registryInformationPage),
					"Below product details displayed in Purchases tab: product name , product color , UPC ,Size and Variation !",
					"Below product details not displayed in Purchases tab: product name , product color , UPC ,Size and Variation !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_172

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Purchase details are displayed in purchase tab", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_173(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// create registry account
			String[] details = createRegistryAccount(homePage,

					"weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			// Search the product with keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message(i++ + ". Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry !");
			// click on registry from header
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". clicked on registry from header !");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			registryInformationPage.clickOnAddtoBag();
			Log.message(i++ + ". Product added to Shopping Bag!");
			// click on Minicart
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". clicked on minicart Button!");
			int QtyInBagInBag = Integer.parseInt(shoppingBagPage.getNoOfQtyInShoppingBagPage());
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message(i++ + ". clicked on checkout Button!");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address4");
			Log.message(i++ + ". filled shipping details !");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". clicked on continue Button !");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_AmericanExpress");
			Log.message(i++ + ". filled card detais !");
			checkOutPage.clickOnContinueInBilling();
			Log.message(i++ + ". clicked on continue Button !");
			// click on placeOrder Button
			checkOutPage.ClickOnPlaceOrderButton();
			Log.message(i++ + ". clicked on placeOrder Button !");
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". clicked on registry from header !");
			// click on viewRegistry
			registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on view Active Registry !");
			registryInformationPage.clickOnPurchasesTab();
			Log.message(i++ + ". Clicked on purchase Tab !");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message("Below product details should be displayed in Purchases tab: ");
			Log.message("Qty should reflect the no of items purchased by the customer. ");
			Log.message(
					"Date Purchased should display the date in which the item was purchased in the format mm/dd/yyyy");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			int QtyInBagInPurchasesTab = registryInformationPage.getQtyInPurchaseTab();
			String DateInPurchasesTab = registryInformationPage.getDateFormatInPurchaseTab();
			DateInPurchasesTab = DateInPurchasesTab.trim();
			Log.assertThat(QtyInBagInBag == QtyInBagInPurchasesTab,
					"Qty reflects the no of items purchased by the customer. ",
					"Qty reflects the no of items purchased by the customer. ", driver);
			Log.assertThat(DateTimeUtility.verifyDateFormat(DateInPurchasesTab, "MM/dd/yy"),
					"Date Purchased displed the date in which the item was purchased in the format mm/dd/yy .",
					"Date Purchased not displed the date in which the item was purchased in the format mm/dd/yy .",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_173

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify heading and field names displays in Empty Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_176(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> findRegistry = Arrays.asList("lblfindRegistry");
		List<String> elements = Arrays.asList("txtFistNameRegistry", "txtLastNameRegistry", "eventTypeInRegistry");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			signIn.scrollToAdvanceSearch();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("'Find a Registry' heading should be displayed on the page.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElements(findRegistry, signIn),
					"'Find a Registry' link heading displayed on the page.",
					"'Find a Registry' link heading displayed on the page.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Below field should display by default :");
			Log.message("First Name");
			Log.message("Last Name");
			Log.message("Event Type");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElements(elements, signIn),
					"By default 'First Name' , 'Last Name' and 'Event Type' displayed in Registry SignIn page. ",
					"By default 'First Name' , 'Last Name' and 'Event Type' not displayed in Registry SignIn page. ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_176

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify additional fields displays on the Empty Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_177(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> element = Arrays.asList("cityInAdvancedRegistry", "stateInAdvancedRegistry",
				"eventDateInAdvancedRegistry", "regIdInAdvancedRegistry");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			signIn.scrollToAdvanceSearch();
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message(
					"Before clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' should not be displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElementsDoNotExist(element, signIn),
					"Before clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' not displayed.",
					"Before clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' displayed.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"After clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' should be displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			signIn.clickOnAdvanceSearchBtn();
			Log.message("3. Clicked on 'Advance Search' Button !");

			Log.assertThat(signIn.elementLayer.verifyPageElements(element, signIn),
					"After clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' displayed.",
					"Before clciking 'Advance Search Button' the text field 'City' , 'State' ,'EventDate' ,'RegId' not displayed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_177

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Add to Shopping Bag button in Wish List Results public view.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_325(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailId = testData.get("EmailAddress");
		List<String> element = Arrays.asList("btnAddToBag");
		List<String> SuccessMsg = Arrays.asList("btnCheckoutInOrderSummary");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			signIn.enterWishlistEmailid(emailId);
			Log.message("3. Entered EmailId as: '" + emailId + "'!");
			signIn.clickOnFindWishListBtn();
			Log.message("4. clicked on find wishlist button!");
			RegistryInformationPage registryInformationPage = signIn.clickOnViewBtnInWishList();
			Log.message("5. clicked on view button!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("'AddToShoppingBag' button should displayed in wishListPge .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElements(element, registryInformationPage),
					"'AddToShoppingBag' button displayed in wishListPage .",
					"'AddToShoppingBag' button not displayed in wishListPage .", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"When the customer clicks the Add to Cart button, the active product should be added to their cart and the mini­cart should open with the added product.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			registryInformationPage.clickOnAddtoBag();
			Log.message("6. Clicked on 'AddToShoppingBag' Button and Navigated to ShoppingBagPage!");

			Log.assertThat(registryInformationPage.elementLayer.verifyPageElements(SuccessMsg, registryInformationPage),
					"When the customer clicks the Add to Cart button, the active product added to their cart and the mini­cart open with the added product.",
					"When the customer clicks the Add to Cart button, the active product added to their cart and the mini­cart not open with the added product.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_325

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify clicking on Cancel button in Edit Wish List Item modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_347(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		List<String> registry = Arrays.asList("PopUp");
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] details = createWishListAccount(homePage, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Wish List' user");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message(i++ + ". Searched with keyword '" + searchKey + "' and navigated to PDP Page");

			pdpPage.clickAddToWishListLink();
			Log.message(i++ + ". Sucessfully added product to wishlist");

			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message(i++ + ". Navigated to 'WishList' Page");

			wishListPage.findWishList(details[0], details[1], details[2]);
			Log.message(i++ + ". Finded the Wishlist Based on the combination FirstName: '" + details[0]
					+ "' Last Name: '" + details[1] + "' emailId: '" + details[2]);

			wishListPage.clickOnViewLink();
			Log.message(i++ + ". Clicked 'View button' in the Find a registry page");

			wishListPage.ClickOnEditLink();
			Log.message(i++ + ". Clicked 'Edit button' in the Find a registry page");

			wishListPage.ClickOnCancelInPopUp();
			Log.message(i++ + ". Clicked 'Cancel button' in the Find a registry page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> The Edit Wish List Item modal should get closed and returned to the Wish List page.");
			Log.assertThat(wishListPage.elementLayer.verifyPageElementsDoNotExist(registry, wishListPage),
					"<b>Actual Result 1:</b> Edit Wish List Item modal is closed and returned to the Wish List page.",
					"<b>Actual Result 1:</b> Edit Wish List Item modal is not closed and returned to the Wish List page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_347

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system auto fills the Post Event Shipping address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_131(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the Create New Registry button");

			registrySignedUserPage.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			Log.message("5. Filled the event details as a 'wedding registry'.");

			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Contine button ,navigate to the Shipping details page.");

			registrySignedUserPage.fillingPreEventShippingdetails("valid_shipping_address6");
			Log.message("7. Fill the Pre event details,driver");

			LinkedHashMap<String, String> PreShippingInfo = registrySignedUserPage.getPreEventShippingDetails();
			Log.message("8. Pre-Shipping details are: " + PreShippingInfo);

			registrySignedUserPage.clickOnUsePreEventAddress();
			Log.message("9. Click on the 'Use Pre Event Address' button.", driver);

			LinkedHashMap<String, String> PostShippingInfo = registrySignedUserPage.getPostEventShippingDetails();
			Log.message("10. Post-Shipping deatils are : " + PostShippingInfo);
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> Verify system auto fills the Post Event Shipping address.");
			Log.assertThat(PreShippingInfo.equals(PostShippingInfo),
					"<b> Actual Result 1: </b> post-shipping information is displaying and same as pre-shipping! ",
					"<b> Actual Result 1: </b> post-shipping information is not displayed! ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_131

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify added registry are displayed in Add Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_118(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("3. Navigated to PDP Page !");
			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("6. Clicked on Registry link", driver);

			String registrySelect = pdpPage.registrySelectOnSelectModal();
			Log.message("7. The Multiple registry message is :" + registrySelect);
			Log.message("<br>");
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.message(
						"<b> Expected Result 1: </b> 'Select a Registry modal' should display the Added Registry with the following 'EventName, EventType,EventDate'");
				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSelectModalEventName",
								"txtSelectModalEventType", "txtSelectModalEventDate"), pdpPage),
								"<b> Actual Result 1: </b>  Select Modal display  the following  'Event-Name,Event-Type,Event-Date'",
								"<b> Actual Result 1: </b>  Select Modal  not display  the following  'Event-Name,Event-Type,Event-Date'",
								driver);

			}

			if (runPltfrm == "mobile") {
				Log.message(
						"<b> Expected Result 1: </b> 'Select a Registry modal' should display the Added Registry with the following 'EventName, EventType,EventDate'");
				Log.assertThat(pdpPage.elementLayer.verifyPageElements(
						Arrays.asList("lblMobileEventType", "lblMobileEventDate", "lblMobileEventName"), pdpPage),
						"<b> Actual Result 1: </b>  Select Modal display  the following  'Event-Name,Event-Type,Event-Date'",
						"<b> Actual Result 1: </b>  Select Modal  not display  the following  'Event-Name,Event-Type,Event-Date'",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_118

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify clicking on Find Wish List button with valid details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_316(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String FirstName = "wishlist2";
		String LastName = "wishlist2";
		String txtNoWishListResultErrorMsg = "We are sorry. No wish list could be found for , wishlistwishlist_316@ymail.com. Please try again.";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering First Name
			wishlistpage.enterFindWishListFirstName(FirstName);
			Log.message("3. Entered First Name: '" + FirstName + "' in the FirstName Text Field.");
			// Entering last name
			wishlistpage.enterFindWishListLastName(LastName);
			Log.message("4. Entered Last Name: '" + LastName + "' in the LastName Text Field.");
			// clicking find wish list button
			wishlistpage.clickFindWishList();
			Log.message("5. Clicked on 'Find The WishList' Button");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> If results are found, the results should be displayed below the Wish List Results section.");
			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(Arrays.asList("wishListTable"), wishlistpage),
					"<b>Actual Result 1:</b> Results are displayed below the Wish List Results section",
					"<b>Actual Result 1:</b> Results are not displayed below the Wish List Results section", driver);
			Log.message("<br>");
			// entering email id
			wishlistpage.enterFindWishListEmailId(emailid);
			Log.message("6. Entered valid Email Id: '" + emailid + "' in the Email Text Field.");
			// clicking find wish list button
			wishlistpage.clickFindWishList();
			Log.message("7. Clicked on 'Find The WishList' Button");
			Log.message("<br>");
			String NoResultErrorMsgInWishListResult = wishlistpage.getTextFromNOWishListErrorMsg().trim();
			Log.message("<b>Expected Result 2:</b>  If no results are found, Appropriate message should be displayed.");
			Log.assertThat(NoResultErrorMsgInWishListResult.contains(txtNoWishListResultErrorMsg),
					"<b>Actual Result 2:</b> Appropriate Error message is displayed, After Entering an 'Email Id' which doesn't contain any items added to the Wishlist.",
					"<b>Actual Result 2:</b> Appropriate Error message is not displayed, After Entering an 'Email Id' which doesn't contain any items added to the Wishlist.",
					driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_WISHLIST_316

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Wish List Search fields", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_315(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> wishlistdetails = null;
		wishlistdetails = Arrays.asList("txtWishListSearchFirstName", "txtWishListSearchLastName",
				"txtWishListSearchEmail", "btnFindWishList");
		String emailid = testData.get("EmailAddress");
		String FirstName = "wishlist2";
		String LastName = "wishlist2";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Wish List Search fields should include First Name, Last Name, Email, Find wishlist button.");

			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(wishlistdetails, wishlistpage),
					"<b>Actual Result 1:</b> Following fields 'First Name', 'Last Name', 'Email', 'Find wishlist button' are included in the Wish List Search fields.",
					"<b>Actual Result 1:</b> Following fields 'First Name', 'Last Name', 'Email', 'Find wishlist button' are not included in the Wish List Search fields.",
					driver);
			Log.message("<br>");

			// Entering first name
			wishlistpage.enterFindWishListFirstName(FirstName);
			Log.message("3. Entered First Name: '" + FirstName + "' in the FirstName Text Field.");
			// entering last name
			wishlistpage.enterFindWishListLastName(LastName);
			Log.message("4. Entered Last Name: '" + LastName + "' in the LastName Text Field.");

			// clicking find wishlist button
			wishlistpage.clickFindWishList();
			Log.message("5. Clicked on 'Find The WishList' Button");
			// clicking view kink in wiwsh result page
			wishlistpage.clickOnViewLink();
			Log.message("6. Clicked on 'View Link' in Wish List Page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Users should be able to search a wish list using first name and last name,The customer should see the lists which have been set to public.");
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("wishListResultContent"), wishlistpage),
					"<b>Actual Result 2:</b> Users  are able to search a wish list using 'first name' and 'last name' and customer is able to see the lists which have been set to public ",
					"<b>Actual Result 2:</b> Users are not able to search a wish list using 'first name' and 'last name' and customer is not able to see the lists which have been set to public",
					driver);
			Log.message("<br>");
			// Entering Email Id ,to verify whether the user is able to search a
			// wish list using 'email id' alone//

			wishlistpage.enterFindWishListEmailId(emailid);
			Log.message("7. Entered valid Email Id: '" + emailid + "' in the Email Text Field.");
			// clicking find wishlist button
			wishlistpage.clickFindWishList();
			Log.message("8. Clicked on 'Find The WishList' Button");
			// clicking view link in wiwsh result page
			wishlistpage.clickOnViewLink();
			Log.message("9. Clicked on 'View Link' in Wish List Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Users should be able to search a wish list using 'email id',The customer should see the lists which have been set to public.");
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("wishListResultContent"), wishlistpage),
					"<b>Actual Result 3:</b> Users are able to search a wish list using 'email id' and customer is able to see the lists which have been set to public ",
					"<b>Actual Result 3:</b> Users are not able to search a wish list using 'email id' and customer is not able to see the lists which have been set to public",
					driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_WISHLIST_315

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Find Wish List Description in Search Results page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_314(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String WishListDescriptioncontent = "Please provide the information requested below to find a wish list.";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			String txtWishListDescription = wishlistpage.getTextFromWishListDescription();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The Wish List Description should get displayed below the Find Wish List header in Search Results page.");

			Log.assertThat(txtWishListDescription.contains(WishListDescriptioncontent),
					"<b>Actual Result :</b> The Wish List Description is displayed below the Find Wish List header in Search Results page.",
					"<b>Actual Result :</b> The Wish List Description not displayed below the Find Wish List header in Search Results page.",
					driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_WISHLIST_314

	@Test(groups = {
	"mobile" }, description = "Verify print icon is not displayed in Mobile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_203(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException("This testcase is only applicable for mobile");
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstname = "registrybelk";
		String lastname = "registrybelk";
		String eventype = "Wedding Registry";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid EmailId in the EmailId Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered Password in the Password Field");
			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on 'SignIn' Button");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver).get();
			// Entered first name,last name and event type in the 'find the wish
			// list Registry'
			registrysignedUserPage.findRegistry(firstname, lastname, eventype);
			Log.message("6. Entered First Name: '" + firstname + "', Last Name: '" + lastname + "' and Event Type: '"
					+ eventype + "' in 'Find the Registry' Field");
			// Clicking ViewLink
			registryPage.clickViewLink();
			Log.message("7. Clicked on View Link");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Print icon should not be displayed in mobile.");
			Log.assertThat(
					registrysignedUserPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("PrintIcon"),
							registrysignedUserPage),
							"<b>Actual Result :</b> Print icon is not displayed in mobile.",
							"<b>Actual Result :</b> Print icon is displayed in mobile.", driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_203//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays the Registry icon and Eventname near the product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_200(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstname = "registrybelk";
		String lastname = "registrybelk";
		String eventype = "Wedding Registry";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid EmailId: '" + emailid + "' in the EmailId Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered Password: '" + password + "' in the Password Field");
			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on SignIn Button");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver).get();
			// Entered first name,last name and event type in the 'find the wish
			// list' field
			registrysignedUserPage.findRegistry(firstname, lastname, eventype);
			Log.message("6. Entered First Name: '" + firstname + "', Last Name: '" + lastname + "' and Event Type: '"
					+ eventype + "' in 'Find the Registry' Field");

			// Clicking View Link
			registryPage.clickViewLink();
			Log.message("7. Clicked on View Link");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);
			registryInformationPage.clickOnAddtoBag();
			Log.message("8. Clicked on 'Add to bag' button");
			registryInformationPage.clickOnMiniCart();
			Log.message("9. Clicked on 'Minicart' Icon");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The cart should be displayed with an icon and the eventName next to the cart item to indicate that item was purchased from a belk.com registry.");

			Log.assertThat(registryInformationPage.VerifyCartRegistryIconAndEventName("wedding's Registry"),
					"<b>Actual Result:</b> The cart is displayed with an 'icon' and the 'eventName' next to the cart item to indicate that item was purchased from a belk.com registry.",
					"<b>Actual Result:</b> The cart is not displayed with an 'icon' and the 'eventName' next to the cart item to indicate that item was purchased from a belk.com registry.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_200//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify item remains on the Registry item page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_201(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String firstname = "sprint7registry";
		String lastname = "sprint7registry";
		String eventype = "Wedding Registry";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homepage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdpPage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page
			Log.message("7. Selected size:'" + pdpPage.selectSize() + "' from Size dropdown in the Pdp!");

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("8. Clicked on 'Add To Registry' link");

			GiftRegistryPage registryPage = homepage.headers.navigateToGiftRegistry();
			Log.message("9. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver).get();
			// Entered first name,last name and event type in the 'find the wish
			// list' field
			registrysignedUserPage.findRegistry(firstname, lastname, eventype);
			Log.message("10. Entered First Name: '" + firstname + "', Last Name: '" + lastname + "' and Event Type: '"
					+ eventype + "' in 'Find the Registry' Field");
			// Clicking View Link
			registryPage.clickViewLink();
			Log.message("11. Clicked on 'View' Link");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);
			int StillneedsCountBeforePlacingOrder = registryInformationPage.getStillNeedsQtyCount();
			Log.message(
					"12. Before Placing an Order ,The 'Still Needs' Quantity is " + StillneedsCountBeforePlacingOrder);
			int RegistryItemCountBeforePlacingOrder = registryInformationPage
					.getCountOfTotalRegistryItemInRegistryPage();
			Log.message("13. Before Placing an Order ,The Total Registry Items available In Registry Page is "
					+ RegistryItemCountBeforePlacingOrder);
			registryInformationPage.clickOnAddtoBag();
			Log.message("14. Clicked on 'Add to bag' button");
			registryInformationPage.clickOnMiniCart();
			Log.message("15. Clicked on 'Minicart' Icon");
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver);
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("16. Clicked on 'Check out' Button In Shopping Bag Page!");
			checkoutPage.enterCVVinExpressCheckout("card_Valid_Discover");
			Log.message("17. Entered the Valid CVV in CVV Text Field!");
			checkoutPage.clickOnApplyButton();
			Log.message("18. Clicked on 'Apply' Button In Place Order Page!");
			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on 'Place Order' button and the Order is Placed Successfully");
			homepage.headers.navigateToGiftRegistry();
			Log.message(
					"20. Again Clicked on 'Registry' button on header and Navigated to 'Registry' Page to Verify whether or not the Registry qty decremented upon successful order submission.");
			registrysignedUserPage.findRegistry(firstname, lastname, eventype);
			Log.message("21. Entered First Name: '" + firstname + "', Last Name: '" + lastname + "' and Event Type: '"
					+ eventype + "' in 'Find the Registry' Field");
			registryPage.clickViewLink();
			Log.message("22. Clicked on 'View' Link");
			int StillneedsCountAfterPlacingOrder = registryInformationPage.getStillNeedsQtyCount();
			Log.message("23. After Placing an Order ,The 'Still Needs' Quantity is Reduced to "
					+ StillneedsCountAfterPlacingOrder);
			int RegistryItemCountAfterPlacingOrder = registryInformationPage
					.getCountOfTotalRegistryItemInRegistryPage();
			Log.message("24. After Placing an Order ,The Total Registry Items in Registry Page remained as "
					+ RegistryItemCountAfterPlacingOrder);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Selected item should remain on the Registry Items page with the Needs qty decremented upon successful order submission.");
			Log.assertThat(
					(RegistryItemCountBeforePlacingOrder == RegistryItemCountAfterPlacingOrder)
					&& (StillneedsCountBeforePlacingOrder > StillneedsCountAfterPlacingOrder),
					"<b>Actual Result :</b> Selected item remained on the Registry Items page with the Needs qty decremented upon successful order submission.",
					"<b>Actual Result :</b> Selected item not remained on the Registry Items page with the Needs qty decremented upon successful order submission.",
					driver);
			homepage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);
			registrySignedUserPage.clickOnViewRegistrylnk();
			registrySignedUserPage.clickDeleteItem();
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_201

	@Test(groups = {
	"desktop" }, description = "Verify details are while clicking print button from shopper View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_202(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstname = "registrybelk";
		String lastname = "registrybelk";
		String eventype = "Wedding Registry";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid 'EmailId: " + emailid + "' in the EmailId Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered 'Password: " + password + "' in the Password Field");

			// Clicking Sign In Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on SignIn Button");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(driver).get();

			registrysignedUserPage.findRegistry(firstname, lastname, eventype);
			Log.message("6. Entered First Name: '" + firstname + "', Last Name: '" + lastname + "' and Event Type: '"
					+ eventype + "' in 'Find the Registry' Field");

			// Clicking View Link
			registryPage.clickViewLink();
			Log.message("7. Clicked on View Link");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Verify 'Print' icon should  be displayed in desktop.");
			Log.assertThat(
					registrysignedUserPage.elementLayer.verifyPageElements(Arrays.asList("PrintIcon"),
							registrysignedUserPage),
							"<b>Actual Result :</b> 'Print' icon is displayed in desktop.",
							"<b>Actual Result :</b> 'Print' icon is not displayed in desktop.", driver);
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_202//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message is shown when user is logged in with invalid credential", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_008(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String InvalidCredentialsErrorMsg = "Sorry, this does not match our records. Check your spelling and try again.";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");
			// Entering valid Email id
			registryPage.enterEmailID(emailid);
			Log.message("3. Entered Valid 'EmailId' in the EmailId Field");
			// Entering Password
			registryPage.enterPassword(password);
			Log.message("4. Entered 'Password' in the Password Field");
			// Clicking 'Sign In' Button
			registryPage.clickBtnSignIn();
			Log.message("5. Clicked on 'SignIn' Button");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Invalid login error message should be displayed below the Registry Login when clicking login button with invalid credentials");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(Arrays.asList("InvalidLoginErrormsg"),
							registryPage)
							&& registryPage.getTextFromInvalidCredentialsLoginErrorMsg().contains(InvalidCredentialsErrorMsg),
							"<b>Actual Result 1:</b> Invalid login error message is displayed below the Registry Login when clicking login button with invalid credentials",
							"<b>Actual Result 1:</b> Invalid login error message is not displayed below the Registry Login when clicking login button with invalid credentials",
							driver);
			Log.message("<br>");

			Log.message("<b>Expected Result 2:</b> The page should not be navigated to the My Registries page");
			Log.assertThat((!driver.getCurrentUrl().contains("GiftRegistry-Start")),
					"<b>Actual Result 2:</b> The page is navigated to the My Registries page",
					"<b>Actual Result 2:</b> The page is not navigated to the My Registries page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_008//

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify clicking on the Update link after making changes in Priority and Qty fields", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_340(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			Log.message("4. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("5. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("6. Select product from search result page!");
			// Select Size, Color, Qty
			String color = pdpPage.selectColor();
			Log.message("7. color is selected and the selected color is " + color);
			String quantity = pdpPage.selectQuantity();
			Log.message("8. Quantity is selected and the quantity is" + quantity);
			// add product to shopping bag
			pdpPage.clickAddToWishListLink();
			Log.message("9. Added to wish list!");
			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message("Navigated to wish list");
			String quantityinWishlist = wishListPage.selectQunatity();
			Log.message("10. Quantity is selected" + quantityinWishlist);
			String priorityinWishlist = wishListPage.selectPriority();
			Log.message("11. Priority is selected and the selected priority is " + priorityinWishlist);
			wishListPage.clickOnUpdate();
			Log.message("12. Update link is clicked");
			String qtyAfterUpdate = wishListPage.getselectedOptionFromQuantity();
			Log.message("13. Got the quantity from updated drop down");
			String priorityAfterUpdate = wishListPage.getSelectedOptionFromPriority();
			Log.message("14. Got the priority from updated drop down");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> When the customer clicks the Update link, the requested changes should be updated to Priority and Quantity fields");
			Log.assertThat(qtyAfterUpdate.equals(quantityinWishlist),
					"<b>Actual Result :</b> Quantity is updated properly",
					"<b>Actual Result :</b> Quantity is not updated properly", driver);
			Log.assertThat(priorityAfterUpdate.equals(priorityinWishlist),
					"<b>Actual Result :</b> Priority is updated properly",
					"<b>Actual Result :</b> Priority is updated properly");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_340

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Registry Barcode is generated based on the registry ID", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_182(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching home page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(1);
			Log.message("10. view link is clicked");
			String registryId = registryInformationPage.getTextFromRegistryId();
			registryId = registryId.split(":")[1];
			Log.message("11. Got the registry id is " + registryId);
			String barcodeId = registryInformationPage.getTextFromBarCode();
			Log.message("12. Got the barcode id " + barcodeId);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Registry Id and Barcode Id should be generated and Barcode IDs should be 12 digits, therefore the barcode will prepend a 49 to the registry ID and end with a known value (not a 0)");
			Log.assertThat(registryInformationPage.Verifybarcode(registryId),
					"<b>Actual Result :</b> Registry Id and Barcode Id is generated and Barcode IDs is 12 digits, therefore the barcode is prepend a 49 to the registry ID and end with a known value (not a 0)",
					"<b>Actual Result :</b> Registry Id and Barcode Id is not generated and Barcode IDs is not 12 digits, therefore the barcode is prepend a 49 to the registry ID and end with a known value (not a 0)",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_182

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Account Help Content Asset is displayed Empty Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_185(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver);
			Log.message("4. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("5. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("5. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("6. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("7. Find Registry is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b>  Account Help Content Asset should be displayed.");
			Log.assertThat(
					registrySignedUser.elementLayer.verifyPageElements(Arrays.asList("fldHelpContentAsset"),
							registrySignedUser),
							"<b>Actual Result :</b> Account Help Content Asset is Displayed properly",
							"<b>Actual Result :</b> Account Help Content Asset is not Displayed properly", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_185

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Print icon on top of Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_337(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			Log.message("4. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			WishListPage wishListPage = myaccount.headers.navigateToWishList();
			wishListPage.clickOnRemoveLink();
			Log.message("5.Remove link is clicked and Items are removed from bag");
			Log.message("<br>");
			if (Utils.getRunPlatForm() == "desktop") {
				Log.message(
						"<b>Expected Result :</b> Desktop: On Click, the browser print function should be displayed");
				Log.assertThat(wishListPage.elementLayer.verifyPageElements(Arrays.asList("lnkPrinter"), wishListPage),
						"<b>Actual Result :</b> print icon is displayed properly in wishlist",
						"<b>Actual Result :</b> print icon is not displayed properly removed properly in wishlist",
						driver);
				wishListPage.clickOnPrintIcon();
				Log.message("6. Print icon is clicked");
				Log.message("System windows (Print page) could not automatable using selenium");

			} else if (Utils.getRunPlatForm() == "mobile") {
				Log.message("<b>Expected Result :</b> Mobile:  print function should not be displayed");
				Log.assertThat(
						wishListPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkPrinter"),
								wishListPage),
								"<b>Actual Result :</b> print icon is not displayed in wishlist",
								"<b>Actual Result :</b> print icon is displyed  in wishlist", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_337

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Wish List Top Section in Wish List with items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_338(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			Log.message("4. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("5. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("6. Select product from search result page!");
			// Select Size, Color, Qty
			String color = pdpPage.selectColor();
			Log.message("7. color is selected and the selected color is " + color);
			String quantity = pdpPage.selectQuantity();
			Log.message("8. Quantity is selected and the quantity is " + quantity);
			// add product to shopping bag
			pdpPage.clickAddToWishListLink();
			Log.message("9. Added to wish list!");
			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The following section should be displayed at the top of the Wish List page has the below content slots:Wish List 1) Global Content Slot, 2) Find Wish List Description Copy");
			Log.assertThat(
					wishListPage.elementLayer.verifyPageElements(Arrays.asList("fldWishListSearchDescription"),
							wishListPage),
							"<b>Actual Result :</b> Find Wish List Description is properly displayed in wishlist",
							"<b>Actual Result :</b> Find Wish List Description is properly displayed in wishlist", driver);
			Log.assertThat(
					wishListPage.elementLayer.verifyPageElements(Arrays.asList("fldGlobalContentSlot"), wishListPage),
					"<b>Actual Result :</b> Global content slot is properly displayed in wishlist",
					"<b>Actual Result :</b> Global content slot is not properly displayed in wishlist");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_338

	@Test(groups = {
	"mobile" }, description = "Print icon is not displayed above Registry Banner Content Slot for mobile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_188(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException("This testcase is only applicable for mobile");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching home page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message(
					"<b>Expected Result :</b> Print icon should not be displayed above the Registry Banner Content Slot");
			Log.assertThat(!registrySignedUser.verifyPrintIconExistAboveRegistryBanner(),
					"<b>Actual Result :</b> Print icon is not displayed above the Registry Banner Content Slot in mobile",
					"<b>Actual Result :</b> Print icon is  displayed above the Registry Banner Content Slot in mobile",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_188

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify no item message displays on the Empty Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_183(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "sachin";
		String lstName = "tendulkar";
		String event = "Wedding Registry";
		String emptyRegistryMsg = "This registry currently has no items available. Please check back later.";
		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<br>");
			Log.message("<b>Expexted Result :</b> No item message resource message should be displayed.");
			Log.assertThat(registryInformationPage.getTextFromEmptyRegistry().equals(emptyRegistryMsg),
					"<b>Actual Result :</b> Empty registry message is properly shown",
					"<b>Actual Result :</b> Empty registry message is not properly shown", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_183

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify gifter able to view the Would love quantity (read only count)", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_193(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Gifter should be allowed to view the Would love quantity (read only count)");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lblWouldLove"),
							registryInformationPage),
							"<b>Actual Result :</b> Would love quantity (read only count) label is present properly",
							"<b>Actual Result :</b> Would love quantity (read only count) label is present properly", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_193

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Registry Banner Content Slot is displayed above the Registry header", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_186(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";
		List<String> registryBanner = Arrays.asList("fldRegistryBanner");

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver);
			Log.message("4. Navigated to gift registry", driver);
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("5. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("5. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("6. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("7. Find Registry is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Registry banner content slot should displayed properly");
			Log.assertThat(registrySignedUser.elementLayer.verifyPageElements(registryBanner, registrySignedUser),
					"<b>Actual Result :</b> Registry content slot Banner is Displayed properly",
					"<b>Actual Result :</b> Registry content slot Banner is not Displayed properly", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_186

	@Test(groups = {
	"desktop" }, description = "Verify Print button is displayed above Registry Banner Content Slot", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_187(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Print icon should not be displayed above the Registry Banner Content Slot");
			Log.assertThat(registrySignedUser.verifyPrintIconExistAboveRegistryBanner(),
					"<b>Actual Result :</b> Print icon is  displayed above the Registry Banner Content Slot",
					"<b>Actual Result :</b> Print icon is not displayed above the Registry Banner Content Slot",
					driver);
			registrySignedUser.clickOnPrintIcon();
			Log.message("11. Print Icon is clicked");
			Log.message("System windows(Print page) cannot be automated using selenium");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_187

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify product link disables below registry event details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_195(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";

		// Get the web driver instance

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
			Log.message("5. Navigated to gift registry");
			registrySignedUser.setFirstNameInFindRegistry(firstName);
			Log.message("6. First name is set ");
			registrySignedUser.setLastNameInFindRegistry(lstName);
			Log.message("7. Last name is set");
			registrySignedUser.selectEventTypeInFindRegistry(event);
			Log.message("8. Event type is selected " + event);
			registrySignedUser.clickOnFindRegistry();
			Log.message("9. Find Registry is clicked");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.clickOnViewInRegistry(2);
			Log.message("10. View link is clicked");
			Log.message("<b>Expected Result :</b> Product link should be displayed below registry event details");
			Log.assertThat(
					registrySignedUser.elementLayer.verifyPageElements(Arrays.asList("lblProductNme"),
							registrySignedUser),
							"<b>Actual Result :</b> Product link is  displayed below registry event details",
							"<b>Actual Result :</b> Product link is not displayed below registry event details", driver);
			String urlBeforeClickProductName = driver.getCurrentUrl();
			registrySignedUser.clickOnProductName();
			String urlAfterClickProductName = driver.getCurrentUrl();
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> The product name hyperlink should be disabled");
			Log.assertThat(urlBeforeClickProductName.equals(urlAfterClickProductName),
					"<b>Actual Result :</b> The product name hyperlink is disabled",
					"<b>Actual Result :</b> The product name hyperlink is not disabled");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_195

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Registry Item Attributes ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_092(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("11. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("12. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("13. Clicked View from the Active Registry");

			Log.message("<b>Expected Result:</b>  Date Added, should display as mm/dd/yyy.");
			String dateadded = registrySignedUserPage.DateAddedTxt();
			Log.softAssertThat(registrySignedUserPage.isValidFormatregex(dateadded),
					"<b>Actual Result1:</b> Date Added is displayed as mm/dd/yyy.",
					"<b>Actual Result1:</b> Date Added is not displayed as mm/dd/yyy.", driver);
			Log.message("<b>Expected Result:</b> Would Love, should display as drop down list.");

			Log.softAssertThat(registrySignedUserPage.isWouldLoveSelectbox(),
					"<b>Actual Result1:</b> Date Added is displayed as mm/dd/yyy.",
					"<b>Actual Result1:</b> Date Added is not displayed as mm/dd/yyy.", driver);
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_092
	
	// TC_BELK_REGISTRY_104
	@Test(groups = { "desktop",
	"mobile" }, description = "Add to Wishlist | Add to Registry Links are not shown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_111(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[0]);
			Log.message("7. Navigated to PDP Page from Search Results Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQuantity();
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickOnRegistrySign();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			registrySignedUserPage.clickAddItemsToRegistry();
			homePage.headers.searchAndNavigateToPDP(searchKey[1]);
			Log.message("13. Navigated to PDP Page from Search Results Page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			Log.message("14.  Size is Selected from size dropdown");
			pdpPage.selectQuantity();
			Log.message("15. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickOnRegistrySign();
			Log.message("16. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("17. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}			
			Log.message("18. Clicked View from the Active Registry");
			
			String Productname1 = registrySignedUserPage.getProductName();
			registrySignedUserPage.addItemToBag(Productname1);
			ShoppingBagPage shoppingBagPage = registrySignedUserPage.clickMiniCart();
			
			Log.message("<br>");
			
			Log.message("<b> Expected Result :</b> Product shoud be added to shopping bag from registry ");
			
			Log.assertThat((shoppingBagPage.getProductName(Productname1)), 
					"<b> Actual Result :</b> Product is added to shopping bag", 
					"<b> Actual Result :</b> Product is not added to shopping bag");
			
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_111

	
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Update Registry button in the Edit modal of the Registry.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_112(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectSizeByIndex(4);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("13. Clicked on AddItemsToRegistry button");
			homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("14. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			pdpPage.selectSizeByIndex(2);
			Log.message("15.  Size is Selected from size dropdown");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("16. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("17. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("18. Clicked View from the Active Registry");

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();

			int xLocationOfFirstProduct = registryInformationPage.getCurrentLocationOfPdtByIndex(1);
			int xLocationOfSecondProduct = registryInformationPage.getCurrentLocationOfPdtByIndex(2);
			Log.message("<b>Expected Result:</b> Items should be added as separate line items to the Registry");
			Log.assertThat(xLocationOfFirstProduct != xLocationOfSecondProduct,
					"<b>Actual Result:</b> Items is added as separate line items to the Registry",
					"<b>Actual Result:</b>  Items is not added as separate line items to the Registry", driver);
			// registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_112

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system redirects the customer to Find a Registry:Unauthenticated or Find a Registry: No Results", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_178(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(
					"<b>Expected Result:</b>  The page should be redirected to Registry login page and Find Registry Pane should be shown below Login pane");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("findRegistryPanel"),
							giftRegistryPage),
							"<b>Actual Result1:</b>The page redirected to Registry login page and Find Registry Pane is shown below Login pane",
							"<b>Actual Result1:</b>  Update and remove links is not available.", driver);
			giftRegistryPage.setFirstNameInFindRegistry("test");
			Log.message("2. First Name Entered in Find Registry form");
			giftRegistryPage.setLastNameInFindRegistry("test213");
			Log.message("3. Last Name Entered in Find Registry form");
			giftRegistryPage.selectEventTypeInFindRegistry(1);
			Log.message("4. Event Type selected in Find Registry form");
			giftRegistryPage.clickOnFindRegistry();
			Log.message("5. Find registry Button is Clicked");

			Log.message(
					"<b>Expected Result:</b>  System Should redirect to No Search results Page If it could not find the Result for the entered criteria");

			String nosearchtxt = giftRegistryPage.getNoSearchTxt();

			Log.softAssertThat(
					nosearchtxt
					.equalsIgnoreCase("No registry has been found for test test213 wedding, please try again."),
					"<b>Actual Result:</b> System redirected to No Search results Page",
					"<b>Actual Result:</b>  System did not redirect to No Search results Page", driver);
			giftRegistryPage.setFirstNameInFindRegistry("test");
			Log.message("6.First Name Entered in Find Registry form");
			giftRegistryPage.setLastNameInFindRegistry("test");
			Log.message("7.Last Name Entered in Find Registry form");
			giftRegistryPage.clickOnFindRegistry();
			Log.message("8.Find registry Button is Clicked");
			Log.message(
					"<b>Expected Result:</b>  System should redirect the customer to Find a Registry:Unauthenticated screen, If it found the result for the entered criteria. ");
			if (Utils.getRunPlatForm() != "mobile") {

				Log.assertThat(
						giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("linkView"), giftRegistryPage),
						"<b>Actual Result:</b> System redirected the customer to Find a Registry:Unauthenticated screen.",
						"<b>Actual Result:</b>  System did not redirect the customer to Find a Registry:Unauthenticated screen .",
						driver);
			} else {
				Log.assertThat(
						giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("linkViewMobile"),
								giftRegistryPage),
								"<b>Actual Result:</b> System redirected the customer to Find a Registry:Unauthenticated screen.",
								"<b>Actual Result:</b>  System did not redirect the customer to Find a Registry:Unauthenticated screen .",
								driver);
			}
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_178

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Warning message is shown for invalid email address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_246(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SighIn Page from Home Page");
			signIn.clikForgotPwd();
			Log.message("3. Forgot Password Link is Clicked");
			signIn.enterEmailInResetPasswordPopup("weer");
			Log.message("4. Email Text box is entered with invalid Email address");
			signIn.clickBtnSend();
			Log.message("5. Send Button is clicked");
			String warningmessageInResetPasswordPopup = signIn.warningmesaageInResetPasswordPopup();
			Log.message(
					"<b>Expected Result:</b>  Warning message should be shown when clicking on Reset Password screen");
			Log.assertThat(
					warningmessageInResetPasswordPopup
					.equals("We're sorry, but this email address is not in the proper format: (EXAMPLE: email_user@email.com). Please re-enter your email address."),
					"<b>Actual Result:</b> Warning message is shown when clicking on Reset Password screen",
					"<b>Actual Result:</b> Warning message is not shown when clicking on Reset Password screen  .",
					driver);
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_246

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'View Registry' Link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_024(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver);

			registrySignedUserPage.setFirstNameInFindRegistry("aspire");
			Log.message("2. Enterd First name");
			registrySignedUserPage.setLastNameInFindRegistry("qa");
			Log.message("3. Entered Last Name");
			registrySignedUserPage.selectEventTypeInFindRegistry("Wedding Registry");
			Log.message("4. Selected 'Wedding Registry' from the event drop down in the 'Find  a Registry' page");
			registrySignedUserPage.clickOnFindRegistry();
			Log.message("5. Clicked on 'Find a Registry', It navigated to Registry Information Page");

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver);
			registryInformationPage.clickOnViewInRegistry(1);

			Log.message("6. Clicked on 'View' link to view someone'e Registry Details");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Products should be displayed in View Registy  with the 'Event Name- Event Date', 'Registry ID', 'Registrant' , ' Co-Registrant' , 'Event Type' , 'LocationRegistry' 'Bar code' in the 'RegistryInformation' Page");
			Log.assertThat(
					((registryInformationPage.getEventInfoFromFindRegistry().contains("Registry ID"))
							&& (registryInformationPage.getEventInfoFromFindRegistry().contains("Registrant"))
							&& (registryInformationPage.getEventInfoFromFindRegistry().contains("Event Type"))
							&& (registryInformationPage.getEventInfoFromFindRegistry().contains("Location"))
							&& (registryInformationPage.elementLayer.verifyPageElements(
									Arrays.asList("EventTitleInFinDRegistry", "BarCodeInViewRegistry"),
									registryInformationPage)))
									&& (registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lstItemsInRegistry"),
											registryInformationPage)),
											"<b>Actual Result:</b> Products is displayed in View Registy  with the 'Event Name- Event Date', 'Registry ID', 'Registrant' , ' Co-Registrant' , 'Event Type' , 'LocationRegistry' 'Bar code' in the 'RegistryInformation' Page",
											"<b>Actual Result:</b> Products is not displayed in View Registy  with the 'Event Name- Event Date', 'Registry ID', 'Registrant' , ' Co-Registrant' , 'Event Type' , 'LocationRegistry' 'Bar code' in the 'RegistryInformation' Page",
											driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_024

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify Need Help section in Wish List page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_298(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {

			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
			String[] Left_Nav = testData.get("LeftNavigation").split(",");

			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				// Load HomePage
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				WishListPage wishListpage = homePage.headers.navigateToWishList();
				Log.message("2. Navigated to WishListPage");

				wishListpage.ClickonCreateAccount();
				Log.message("3. Navigated to 'Create Account' Page");

				Log.message("<br>");
				Log.message(
						"<b>Expected Result1:</b> By Clicking on 'CreateAccount' link in the LeftNavigation, It should navigate to corresponding 'CreateAccount' page");

				Log.softAssertThat(Left_Nav[0].replaceAll(" ", "").contains("Account"),
						"<b>Actual Result1:</b> By Clicking on 'CreateAccount' link in the LeftNavigation, It is navigated to corresponding 'CreateAccount' page",
						"<b>Actual Result1:</b> By Clicking on 'CreateAccount' link in the LeftNavigation, It is not navigated to corresponding 'CreateAccount' page",
						driver);
				BrowserActions.navigateToBack(driver);

				wishListpage.ClickonPrivacypolicy();
				Log.message("4. Navigated to 'Privacy-Policy' Page");

				Log.message("<br>");
				Log.message(
						"<b>Expected Result2:</b> By Clicking on 'PrivacyPolicy' link in the LeftNavigation, It should navigate to corresponding 'PrivacyPolicy' page");

				Log.softAssertThat(Left_Nav[1].replaceAll(" ", "").contains("privacy"),
						"<b>Actual Result2:</b> By Clicking on 'PrivacyPolicy' link in the LeftNavigation, It is navigates to corresponding 'PrivacyPolicy' page",
						"<b>Actual Result2:</b> By Clicking on 'PrivacyPolicy' link in the LeftNavigation, It is not navigated to corresponding 'PrivacyPolicy' page",
						driver);
				BrowserActions.navigateToBack(driver);

				wishListpage.ClickonSecureShopping();
				Log.message("5. Navigated to 'Secure Shopping' page");

				Log.message("<br>");
				Log.message(
						"<b>Expected Result3:</b> By Clicking on 'SecureShopping' link in the LeftNavigation, It should navigate to corresponding 'SecureShopping' page");

				Log.softAssertThat(Left_Nav[2].replaceAll(" ", "").contains("security"),
						"<b>Actual Result3:</b> By Clicking on 'SecureShopping' link in the LeftNavigation, It is navigates to corresponding 'SecureShopping' page",
						"<b>Actual Result3:</b> By Clicking on 'SecureShopping' link in the LeftNavigation, It is not navigated to corresponding 'SecureShopping' page",
						driver);
				BrowserActions.navigateToBack(driver);

				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_REGISTRY_298

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Returing Customer Text in Wish List Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_301(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			WishListPage wishListpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigated to WishListPage");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The 'Returing Customer Description' should be displayed below the Wish List Login header in  the 'WishList' Page.");

			Log.assertThat(
					(wishListpage.elementLayer.verifyPageElements(Arrays.asList("txtReturningCustomers"),
							wishListpage)),
							"<b>Actual Result:</b> The 'Returing Customer Description' is displayed below the Wish List Login header in  the 'WishList' Page.",
							"<b>Actual Result:</b> The 'Returing Customer Description' is not displayed below the Wish List Login header in  the 'WishList' Page.",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_301

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Quantity Requested field in Wish List Results public view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_322(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			// entering first name
			wishlistpage.enterFindWishListFirstName("asqa");
			Log.message("3. Entered 'First Name' in Find the Wish List Field");

			// entering last name
			wishlistpage.enterFindWishListLastName("asqa");
			Log.message("4. Entered 'Last Name' in Find the Wish List Field");

			// clicking find Wish List button
			wishlistpage.clickFindWishList();
			Log.message("6. Clicked on 'Find The WishList' Button");

			// clicking 'view' link in wish list result page
			wishlistpage.clickOnViewLink();
			Log.message("7. Clicked on 'View Link' in Wish List Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The quantity of the products should get displayed in Wish List Results public view page");

			Log.assertThat(
					(wishlistpage.elementLayer.verifyPageElements(Arrays.asList("SelectedQuantity"), wishlistpage)),
					"<b>Actual Result:</b> The quantity of the products is displayed in Wish List Results public view page",
					"<b>Actual Result:</b> The quantity od the products is not displayed in Wish List Results public view page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_322

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Register for TheKnot.com check box in the Event details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_150(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingRegistryDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("step3CreateRegistry"), regSignedUser),
					"10. The Page is navigated to Create Registry - Step 3 of 3",
					"10. The Page is not navigated to Create Registry - Step 3 of 3");
			regSignedUser.clickOnCheckboxInStep3();

			regSignedUser.clickOnSubmitButton();

			Log.message(
					"<b>Expected Result :</b> Customers should be choosed to send their wedding registry to theknot.com and the registry id should be generated");

			String runPltfrm = Utils.getRunPlatForm();

			if (runPltfrm == "mobile") {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("btnAddToRegistry"), regSignedUser),
						"<b>Actual Result 1:</b> Customers should be choosed to send their wedding registry to theknot.com and the registry id is generated",
						"<b>Actual Result 1:</b> Customers should be choosed to send their wedding registry to theknot.com and the registry id is not generated");

			} else {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("lblRegistryConfirmation"),
								regSignedUser),
								"<b>Actual Result :</b> Customers should be choosed to send their wedding registry to theknot.com and the registry id is generated",
						"<b>Actual Result :</b> Customers should be choosed to send their wedding registry to theknot.com and the registry id is not generated");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_150

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify page redirected to appropriate page while clicking on tab", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_155(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			RegistrySignedUserPage regSignedUser = registryPage.clickonViewRegister();
			Log.message("5. Clicked On View Registry");

			if (Utils.getRunPlatForm() == "desktop") {
				regSignedUser.clickOnMyRegistry();
				Log.message("<b>Expected Result 1:</b> My Registry Tab should be enabled");
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("lblRegistryConfirmation"),
								regSignedUser),
								"<b>Actual Result 1:</b> My Registry Tab is enabled",
						"<b>Actual Result 1:</b> My Registry Tab is not enabled");
			} else {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("lblRegistryConfirmation"),
								regSignedUser),
								"<b>Actual Result 1:</b> My Registry Tab is enabled",
						"<b>Actual Result 1:</b> My Registry Tab is not enabled");
			}

			regSignedUser.clickOnEventInfo();
			Log.message("<b>Expected Result 2:</b> Event info Tab should be enabled");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("lblRegistrationInfoInEventInfo"),
							regSignedUser),
							"<b>Actual Result 2:</b> Event info Tab is  enabled",
					"<b>Actual Result 2:</b> Event info Tab is not enabled");

			regSignedUser.clickOnShippingInfo();
			Log.message("<b>Expected Result 3:</b> Shipping info Tab should be enabled");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("lblPreEventShipping"), regSignedUser),
					"<b>Actual Result 3:</b> Shipping info Tab is enabled",
					"<b>Actual Result 3:</b> Shipping info Tab is not enabled");

			regSignedUser.clickOnPurchases();
			Log.message("<b>Expected Result 4:</b> Purchases Tab should be enabled");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElements(Arrays.asList("warningMsgInPurchase"), regSignedUser),
					"<b>Actual Result 4:</b> Purchases Tab is enabled",
					"<b>Actual Result 4:</b> Purchases Tab is not enabled");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_155

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry id get displays for the created registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_154(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");
			regSignedUser.clickOnCheckboxInStep3();
			regSignedUser.clickOnSubmitButton();
			Log.message("10. Entered all details and clicked on submit button");

			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "mobile") {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("btnAddToRegistry"), regSignedUser),
						"11.Registry Id is generated", "11.Registry Id is not generated");

			} else {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("btnMakeThisListPrivate"),
								regSignedUser),
								"11. My Registry Tab is be enabled", "11. My Registry Tab is not enabled");
			}
			String RegisterId = regSignedUser.getRegisterId();
			Log.message("The Register Id is :" + RegisterId);

			Log.message("<b>Expected Result :</b> Unique 9 digit registry ID should be generated");
			Log.assertThat(regSignedUser.verifyRegisterIdFormat(RegisterId),
					"<b>Actual Result :</b> Unique 9 digit registry ID is generated",
					"<b>Actual Result :</b> Unique 9 digit registry ID is not generated");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_154

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Previous and Cancel button in the Step 2 Event Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_153(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");
			regSignedUser.clickOnCheckboxInStep3();
			Log.message("10. Clicked on the knot checkbox");
			regSignedUser.clickOnPreviousInStep3();
			Log.message("11. Clicked on Previous button");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("12. Clicked on Continue in step 2");
			Log.message("<b>Expected Result 1:</b> The information should not be saved in step 3");
			Log.assertThat(regSignedUser.verifyTheKnotCheckbox(),
					"<b>Actual Result 1:</b> The information is not saved in step 3",
					"<b>Actual Result 1:</b> The information is saved in step 3");
			regSignedUser.clickOnCheckboxInStep3();
			regSignedUser.clickOnSubmitButton();
			Log.message("13. Entered all details and clicked on submit button");
			String runPltfrm = Utils.getRunPlatForm();

			if (runPltfrm == "mobile") {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(Arrays.asList("btnAddToRegistry"), regSignedUser),
						"14.Registry Id is generated", "14.Registry Id is not generated");

			} else {
				Log.assertThat(regSignedUser.elementLayer.verifyPageElements(Arrays.asList("btnMakeThisListPrivate"),
						regSignedUser), "14. Registry Id is generated", "14. Registry Id is not generated");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_153

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify error message displays for Sales Associate checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_152(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");
			regSignedUser.clickOnCheckboxInStep3();
			Log.message("10. Clicked on the knot checkbox");
			regSignedUser.ClickOnSalesCheckbox();
			regSignedUser.clickOnSubmitButton();
			Log.message("11. Clicked on submit button");
			Log.message(
					"<b>Expected Result 1:</b> This field is required error message should be displayed when we do not enter employee id and store id");
			Log.assertThat(regSignedUser.VerifyWarningMsgForNoEmpidStoreId(),
					"<b>Actual Result 1:</b> This field is required error message is displayed when we do not enter employee id and store id",
					"<b>Actual Result 1:</b> This field is required error message is not displayed when we do not enter employee id and store id");
			regSignedUser.enterWrongValuesForEmpStoreId("WrongValueForEmpStoreId");
			regSignedUser.clickOnSubmitButton();
			Log.message(
					"<b>Expected Result 2:</b> Please enter at least 6 characters error message should be displayed when we enter less than 6 digit employee id and Please enter at least 4 character when we enter less than 4 digit store id");
			regSignedUser.VerifyWarningMsgForSizeEmpidStoreId();
			Log.assertThat(regSignedUser.VerifyWarningMsgForSizeEmpidStoreId(),
					"<b>Actual Result 2:</b> Please enter at least 6 characters error message is displayed when we enter less than 6 digit employee id and Please enter at least 4 character error message is displayed when we enter less than 4 digit store id",
					"<b>Actual Result 2:</b> Please enter at least 6 characters error message is not displayed when we enter less than 6 digit employee id and Please enter at least 4 character error message is not displayed when we enter less than 4 digit store id");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_152

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Sales Associate check box in the Event details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_151(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			// RegistrySignedUserPage registryPage;
			RegistrySignedUserPage regSignedUser = registryPage.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");
			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");
			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");
			regSignedUser.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");
			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");
			regSignedUser.clickOnCheckboxInStep3();
			Log.message("10. Clicked on the knot checkbox");

			Log.message(
					"<b>Expected Result 1:</b> The employee Id Text Box should not be present before selecting the check box of sales associate");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtEmpId"), regSignedUser),
					"<b>Actual Result 1:</b> The employee Id  Text Box is not  present before selecting the check box of sales associate",
					"<b>Actual Result 1:</b> The employee Id Text Box is present before selecting the check box of sales associate");

			Log.message(
					"<b>Expected Result 2:</b> The Store Id Text Box should not be present before selecting the check box of sales associate");
			Log.assertThat(
					regSignedUser.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtStoreId"), regSignedUser),
					"<b>Actual Result 2:</b> The Store Id Text Box is not present before selecting the check box of sales associate",
					"<b>Actual Result 2:</b> The Store Id Text Box is present before selecting the check box of sales associate");

			regSignedUser.ClickOnSalesCheckbox();

			Log.message(
					"<b>Expected Result 3:</b> The employee Id Text Box should be be present before selecting the check box of sales associate");
			Log.assertThat(regSignedUser.elementLayer.verifyPageElements(Arrays.asList("txtEmpId"), regSignedUser),
					"<b>Actual Result 3:</b> The employee Id  Text Box is present before selecting the check box of sales associate",
					"<b>Actual Result 3:</b> The employee Id Text Box is not present before selecting the check box of sales associate");

			Log.message(
					"<b>Expected Result 4:</b> The Store Id Text Box should  be present before selecting the check box of sales associate");
			Log.assertThat(regSignedUser.elementLayer.verifyPageElements(Arrays.asList("txtStoreId"), regSignedUser),
					"<b>Actual Result 4:</b> The Store Id Text Box is present before selecting the check box of sales associate",
					"<b>Actual Result 4:</b> The Store Id Text Box is not present before selecting the check box of sales associate");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_REGISTRY_151

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify products are displayed in Shoppers view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_189(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lastName = "thick";
		String eventType = "Wedding Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			registrySignedUserPage.findRegistry(firstName, lastName, eventType);
			Log.message("4. Entered the credentials(" + firstName + "/" + lastName + ") and selected event type ("
					+ eventType + "");
			Log.message("<br>");
			Log.message("<b>Expected Result-1 :</b> Verify user having  registry or Not!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("lstYourRegistriesView"),
							registrySignedUserPage),
							"<b>Actual Result-1 :</b> View Record  Available !",
							"<b>Actual Result-1 :</b> User do not have any registry Please check input data  !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> Registry details should displayed after clicking on View!");
			Log.message("5. Clicked On View Link Button!");

			registrySignedUserPage.clickOnViewlinkFindRegistry();
			Log.assertThat(
					registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("registryDetailsOnFindRegistry"), registrySignedUserPage),
					"<b>Actual Result -2:</b> Registry details  displayed after clicking on View!",
					"<b>Actual Result -2:</b> Registry details did not display after clicking on View Link!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_189

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify tabs are removed in Shoppers view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_190(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lastName = "thick";
		String eventType = "Wedding Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			registrySignedUserPage.findRegistry(firstName, lastName, eventType);
			Log.message("4. Entered the credentials(" + firstName + "/" + lastName + ") and selected event type ("
					+ eventType + "");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Verify user have registry or Not!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("lstYourRegistriesView"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> View Record  Available !",
							"<b>Actual Result 1:</b> User do not have any registry Please check input data  !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Registry details should displayed after clicking on View!");
			registrySignedUserPage.clickOnViewlinkFindRegistry();
			Log.message("5. Clicked On View Link Button!");
			Log.assertThat(
					registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("productDetailsOnFindRegistry"), registrySignedUserPage),
					"<b>Actual Result 2 :</b>   This registry currently has no items available. Please check back later.!",
					"<b>Actual Result 2 :</b>   Product details  displayed after clicking on View Please check the input data!",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_190

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Actions are disabled when viewing the product as some one's registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_191(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lastName = "thick";
		String eventType = "Wedding Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '" + password
					+ "' and signedIn to myRegistry Account!");
			registrySignedUserPage.findRegistry(firstName, lastName, eventType);
			Log.message("4. Entered the credentials(" + firstName + "/" + lastName + ") and selected event type ("
					+ eventType + "");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> Verify user have registry or Not!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("lstYourRegistriesView"),
							registrySignedUserPage),
							"<b>Actual Result-1 :</b> User have Registry with 'View Record Available' !",
							"<b>Actual Result-1 :</b> User do not have any registry!", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> Registry details should displayed after clicking on View!");
			registrySignedUserPage.clickOnViewlinkFindRegistry();
			Log.message("5. Clicked On View Link Button!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnMakeListPublic", "btnMakeListPrivate", "addItemToRegistry",
									"addGiftCards", "facebookLinkOnFindRegistry", "twitterLinkOnFindRegistry"),
									registrySignedUserPage),
									"<b>Actual Result -2 :</b> Registry details  displayed after clicking on View!",
									"<b>Actual Result -2 :</b> Registry details not displayed after clicking on View!", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_191

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Priority field in Wish List Results public view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_323(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistPage = homePage.headers.navigateToWishList();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			// Search the wishlist based on Email Address
			wishlistPage.wishListSearch(emailid);

			Log.message("3. Search the wishlist based on Email Address:" + emailid);

			wishlistPage.navigateToWishListResults();
			wishlistPage.gettextWishListPriorityByIndex(1);
			Log.message("...................................................");
			// Get Priority value from Wishlist
			Log.assertThat(wishlistPage.gettextWishListPriorityByIndex(1).equals("Highest"),
					"Priority Value" + wishlistPage.gettextWishListPriorityByIndex(1) + " is displayed correctly ",
					"Priority value is displayed as " + wishlistPage.gettextWishListPriorityByIndex(1)
					+ "but Actual value is : Highest");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Social networking icons in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_332(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			WishListPage wishlistpage = signinPage.signInToWishList(emailid, password);
			Log.message("3. Entered Valid credentials(Email ID:" + emailid + "/Password:" + password
					+ ") in 'WishListPage' page", driver);

			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(

					Arrays.asList("lnkShareFacebook"), wishlistpage),
					"<b>Actual Result : </b>\"Share Facebook icon is displayed!",
					"<b>Actual Result : </b>\"Share Facebook icon is not displayed.", driver);
			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(Arrays.asList("lnkShareTwitter"), wishlistpage),
					"<b>Actual Result : </b>\"Share Twitter icon is displayed!",
					"<b>Actual Result : </b>\"Share Twitter icon is not displayed.", driver);

			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("lnkShareGooglePlus"), wishlistpage),
					"<b>Actual Result : </b>\"Share Google Plus icon is displayed!",
					"<b>Actual Result : </b>\"Share Google Plus icon is not displayed.", driver);
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(Arrays.asList("lnkSharePinterNet"), wishlistpage),
					"<b>Actual Result : </b>\"Share PinterNet icon is displayed!",
					"<b>Actual Result : </b>\"Share PinterNet icon is not displayed.", driver);

			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(Arrays.asList("lnkShareEmail"), wishlistpage),
					"<b>Actual Result : </b>\"Share Through Email icon is displayed!",
					"<b>Actual Result : </b>\"Share Through Email icon is not displayed.", driver);

			Log.assertThat(wishlistpage.elementLayer.verifyPageElements(Arrays.asList("lnkShareLink"), wishlistpage),
					"<b>Actual Result : </b>\"Share link icon is displayed!",
					"<b>Actual Result : </b>\"Share link icon is not displayed.", driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}


	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Description icon in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_334(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			homePage.headers.navigateToWishListAsGuest();
			Log.message("2.Clicked On WishList");

			SignIn signInPage = new SignIn(driver).get();
			Log.message("3. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signInPage.signInToAccount(emailid, password);
			Log.message("4. Entered the credentials(" + emailid + "/"
					+ password + ") and navigating to WishList Page");

			WishListPage wishlist = new WishListPage(driver).get();
			String errorMsg = wishlist.getTextFrmWishlistMsg();
			Log.message("<b> Expected Result: </b> You currently have no items in your wish list. Please click below to start adding items-Message should be displayed");
			Log.assertThat(
					errorMsg.contains("no items"),
					"<b> Actual Result: </b> You currently have no items in your wish list. Please click below to start adding items-Message is displayed",
					"<b> Actual Result: </b> You currently have no items in your wish list. Please click below to start adding items- Message is not displayed",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_WISHLIST_334

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Description icon in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_335(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			homePage.headers.navigateToWishListAsGuest();
			Log.message("2.Clicked On WishList");

			SignIn signInPage = new SignIn(driver).get();
			Log.message("3. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signInPage.signInToAccount(emailid, password);
			Log.message("4. Entered the credentials(" + emailid + "/"
					+ password + ") and navigating to WishList Page");

			WishListPage wishlist = new WishListPage(driver).get();
			wishlist.ClickAddGiftCards();
			Log.message("5.Clicked on Add Gift Cards button");
			Log.message("<b> Expected Result: </b> The User should be navigated to Gift Cards");
			Log.assertThat(
					wishlist.getTextFromBreadcrumb().contains("Gift"),
					"<b> Actual Result: </b> The User is navigated to Gift Cards",
					"<b> Actual Result: </b> The User is not navigated to Gift Cards",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_WISHLIST_335

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Description icon in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_336(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			homePage.headers.navigateToWishListAsGuest();
			Log.message("2.Clicked On WishList");

			SignIn signInPage = new SignIn(driver).get();
			Log.message("3. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signInPage.signInToAccount(emailid, password);
			Log.message("4. Entered the credentials(" + emailid + "/"
					+ password + ") and navigating to WishList Page");

			WishListPage wishlist = new WishListPage(driver).get();
			wishlist.ClickOnAddItemsToWishList();	
			Log.message("5.Clicked on Add items to WishList");

			Log.message("<b>Expected Result 1:</b> The page should be redirected to Homepage");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("homePageSlotData"),
							homePage),
							"<b>Actual Result 1:</b> Page is redirected to HomePage page",
							"<b>Actual Result 1:</b> Page is not redirected to Home page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_WISHLIST_336



	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Edit Wish List Item modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_343(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinpage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			WishListPage wishlistPage = signinpage.signInToWishList(emailid, password);
			Log.message("3. Entered Valid credentials(" + emailid + "/" + password + ") in 'Sign In' page", driver);

			// Get Priority value from Wishlist
			wishlistPage.clickOnWishListEditDetailsByIndex(1);
			Log.message("4. EditDetails link is clicked");
			Log.assertThat(wishlistPage.verifyEditDetailsModal(),
					"<b> Actual Result: Edit Details Modal window is displayed.",
					"<b> Actual Result: Edit Details Modal window is not displayed.");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Links in Edit Wish List Item modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_344(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!", driver);

		// Navigate to Signin page
		SignIn signinpage = homePage.headers.navigateToWishListAsGuest();
		Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

		WishListPage wishlistPage = signinpage.signInToWishList(emailid, password);
		Log.message("3. Entered Valid credentials(Email ID:" + emailid + "/Password" + password + ") in 'Sign In' page",
				driver);

		// Get Priority value from Wishlist
		wishlistPage.clickOnWishListEditDetailsByIndex(1);
		Log.assertThat(wishlistPage.verifyEditDetailsModal(),
				"<b> Actual Result</b>: Edit Details Modal window is displayed.",
				"<b> Actual Result</b>: Edit Details Modal window is not displayed.");

		Log.assertThat(
				wishlistPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("lnkProductName"), wishlistPage),
				"<b>Actual Result</b>: Product Name link is disabled",
				"<b>Actual Result</b>: Product Name link is not disabled");

		// Two more component need to implemented based on the Test cases
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Update Wish List button in Edit Wish List Item modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_345(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinpage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			WishListPage wishlistPage = signinpage.signInToWishList(emailid, password);
			Log.message(
					"3. Entered Valid credentials(Email ID:" + emailid + "/Password" + password + ") in 'Sign In' page",
					driver);

			// Get Priority value from Wishlist
			wishlistPage.clickOnWishListEditDetailsByIndex(1);

			Log.assertThat(wishlistPage.verifyEditDetailsModal(), "4. Edit Details Modal window is displayed.",
					"4. Edit Details Modal window is not displayed.");

			String quantityInBagAfterChange = wishlistPage.selectQuantity("3");
			wishlistPage.clickOnUpdate();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Quantity selected in the modal should be updated in the wishlist page");

			Log.assertThat((quantityInBagAfterChange.equals(wishlistPage.getSelectedQuantity())),
					"<b>Actual Result: </b> Quantity selected in the modal is updated in the wishlist page",
					"<b>Actual Result: </b> Quantity selected in the modal is not updated in the wishlist page");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system ability to log out of their registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_016(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Click on Registry Link in Headers menu
			SignIn sign = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Clicked on Registry Link");

			// Login with valid user credentials
			RegistrySignedUserPage registrysignuserpage = sign
					.signInFromRegistryPage(emailid, password);
			Log.message("3. User Loged in with following user Credientials: Email Id:"
					+ emailid + "/Password:" + password);
			Log.assertThat(registrysignuserpage.verifyRegistrySigninElement(),
					"user is navigate to Reigstry SignIn Page",
					"User not able to login");

			// Click on Signout link
			RegistryGuestUserPage registrygustuser = registrysignuserpage.headers
					.clickSignOutFromRegistry();
			Log.assertThat(registrygustuser.verifyGustRegistryElement(),
					"<b>Actual Result: </b> User Signout successfully",
					"<b>Actual Result: </b> User not able to signout");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Footer in the Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_017(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> footers = null;
		List<String> footers1 = null;
		List<String> footers2 = null;
		List<String> footers3 = null;

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop") {
			footers = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation",
					"lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards", "lnkAboutBelk",
					"lnkViewOurAdsOriginal", "lnkListOfBrands",
					"lnkCareersAtBelkOriginal", "lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal", "lnkdownloadApp",
					"lnkIOs", "lnkAndriod", "lnkSocialMedia", "lnkStayConnect",
					"lnkfacebook", "lnkTwitter", "lnkPrintrest",
					"lnkInstagram", "signUP", "txtEmail", "btnJoin",
					"legalMessage", "phoneNo", "lnkTerms", "lnkPrivacy",
					"btnPageFeedBack", "btnSiteMap", "btnOrderByCall");
		} else if (runPltfrm == "mobile") {
			footers = Arrays.asList("lnkdownloadApp", "lnkIOs", "lnkAndriod",
					"lnkSocialMedia", "lnkStayConnect", "lnkfacebook",
					"lnkTwitter", "lnkPrintrest", "lnkInstagram", "signUP",
					"txtEmail", "btnJoin", "legalMessage", "phoneNo",
					"lnkTerms", "lnkPrivacy", "btnPageFeedBack", "btnSiteMap",
					"btnOrderByCall");
			footers1 = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation");
			footers2 = Arrays.asList("lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards");
			footers3 = Arrays.asList("lnkAboutBelk", "lnkViewOurAdsOriginal",
					"lnkListOfBrands", "lnkCareersAtBelkOriginal",
					"lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal");
		}

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Click on Registry Link in Headers menu
			SignIn sign = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Clicked on Registry Link");

			// Login with valid user credentials
			RegistrySignedUserPage registrysignuserpage = sign
					.signInFromRegistryPage(emailid, password);
			Log.message("3. User Loged in with following user Credientials: Email Id:"
					+ emailid + "/Password:" + password);
			Log.assertThat(registrysignuserpage.verifyRegistrySigninElement(),
					"4. user is navigate to Reigstry SignIn Page",
					"User not able to login");

			Log.message(
					"<br><b>Expected Result: </b>The footers in the SignIn User Registry Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						registrysignuserpage.elementLayer.verifyPageElements(
								footers, registrysignuserpage.footers),
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				registrysignuserpage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						registrysignuserpage.elementLayer.verifyPageElements(
								footers1, registrysignuserpage.footers),
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				registrysignuserpage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						registrysignuserpage.elementLayer.verifyPageElements(
								footers2, registrysignuserpage.footers),
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				registrysignuserpage.footers.clickAboutBelk();

				Log.softAssertThat(
						registrysignuserpage.elementLayer.verifyPageElements(
								footers3, registrysignuserpage.footers),
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						registrysignuserpage.elementLayer.verifyPageElements(
								footers, registrysignuserpage.footers),
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the SignIn User Registry Page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);

			}Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
	}


	@Test(enabled = false, groups = { "desktop", "mobile", "tablet" }, description = "Verify registry displays the product image", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_096(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchkey = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] productname = new String[searchkey.length];
		;
		String registeredProductColor;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to Sign In Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			// Sign In to the application with valid User credentials
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrysigninuser = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigate to Gift Registry page");

			// Click on View link in Respective Registry Item
			registrysigninuser.clickOnViewlink();
			Log.message("5. Navigate to Respective Registry item details page");

			// Click on Add items to Registry button
			registrysigninuser.clickOnAddItemToRegistry();
			Log.message("6. Click on Add itemsto Registry button");

			for (int i = 0; i <= searchkey.length; i++) {
				SearchResultPage searchResultPage = homePage
						.searchProductKeyword(searchkey[i]);
				Log.message("7. Searched with keyword '" + searchkey[i]
						+ "' and Navigated to search result Page");

				PdpPage pdpPage = searchResultPage.navigateToPDP();
				Log.message("8. Navigated to 'Pdp' Page with randomly selected product");

				pdpPage.clickOnRegistrySign();

				productname[i] = pdpPage.getProductName();

			}

					registeredProductColor = registrysigninuser
						.getRegisteredProductColor(productname[1]);
				Log.assertThat(
						(registeredProductColor.equals(registrysigninuser
								.getRegisteredProductColorFrommodal())),
						"<b>Actual Result: </b> Product :"
								+ productname[1]
								+ " Color in Registry section and QuickView windows is same",
						"<b>Actual Result: </b> Product :"
								+ productname[1]
								+ " Color in Registry section and QuickView windows is not  same");				

			

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			int i = 0;
			int size = productname.length;
			while (i < size) {
				registryInformationPage.removeItemsFromRegigistry();
				i = i + 1;
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(enabled = false, groups = { "desktop", "mobile", "tablet" }, description = "Verify registry displays the edit link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_097(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchkey = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		// Load the HomePage
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to Sign In Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			// Sign In to the application with valid User credentials
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			myaccount.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrysigninuser = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigate to Gift Registry page");

			// Click on View link in Respective Registry Item
			registrysigninuser.clickOnViewlink();
			Log.message("5. Navigate to Respective Registry item details page");

			// Click on Add items to Registry button
			registrysigninuser.clickOnAddItemToRegistry();
			Log.message("6. Click on Add itemsto Registry button");
			String[] productname = new String[searchkey.length];
			// Add Product from pdp page to Registry(3 products)
			for (int i = 0; i <= searchkey.length; i++) {
				SearchResultPage searchResultPage = homePage
						.searchProductKeyword(searchkey[i]);
				Log.message("7. Searched with keyword '" + searchkey[i]
						+ "' and Navigated to search result Page");

				PdpPage pdpPage = searchResultPage.navigateToPDP();
				Log.message("8. Navigated to 'Pdp' Page with randomly selected product");
				productname[i] = pdpPage.getProductName();
				pdpPage.clickOnRegistrySign();
			}
			Log.message("<b>Expected Result:</b> Product details modal window should be displayed");

			// Click on Edit Details link in each product on Registry
				registrysigninuser
						.clickRegistedProductEditDetails(productname[1]);
				Log.assertThat(registrysigninuser
						.isRegisteredProducteditDetailsvalidation(),
						"<b>Actual Result: </b> Product " + productname[1]
								+ " Modal window is displayed",
						"<b>Actual Result: </b> Product " + productname[1]
								+ " Modal window is not displayed");

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			int i = 0;
			int size = productname.length;
			while (i < size) {
				registryInformationPage.removeItemsFromRegigistry();
				i = i + 1;
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify 'Add Items to Registry' link direct the user to the Homepage", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_082(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn myAccount = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SignIn Page");
			MyAccountPage myAccountPage = myAccount.signInToMyAccount(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			RegistrySignedUserPage registryPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
			Log.message("4. Navigated to Registry Signed User Page");
			registryPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked view Link");
			registryPage.clickOnAddItemsToRegistry();
			Log.message("6. Clicked Add Items to Registry button and Navigated to Home Page");
			// boolean status = Utils.verifyPageURLContains(driver,
			// webSite.split("/s/")[0], "home")&&
			// homePagefromRegistry != null;
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> 'Add Items to Registry' link should be direct the user to the Homepage");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(driver.getCurrentUrl().contains("home"),
					"'Add Items to Registry' link direct the user to the Homepage",
					"'Add Items to Registry' link not direct the user to the Homepage", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_082

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Registry action in my Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_079(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn myAccount = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SignIn Page");
			MyAccountPage myAccountPage = myAccount.signInToMyAccount(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			RegistrySignedUserPage registryPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
			Log.message("4. Navigated to Registry Signed User Page");
			RegistryInformationPage registryInformationPage = registryPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked view Link and Navigated to Registry Information Page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The following Registry Actions should be displayed in the Registry page Registry Tabs, Registry Name & Event Date, Registry ID, Registry Barcode, Social Sharing, Email My Registry, Make List Public / Private, Print Registry");
			Log.message("<b>Actual Result:</b>");
			if (Utils.getRunPlatForm() == "desktop") {
				Log.softAssertThat(
						registryInformationPage.elementLayer.verifyPageElements(
								Arrays.asList("tabMyRegistry", "tabEventInfo", "tabShippingInfo", "tabPurchases"),
								registryInformationPage),
								"Registry tabs are displayed in the Registry page",
								"Registry tabs are not displayed in the Registry page", driver);
			}

			else {
				Log.softAssertThat(
						registryInformationPage.elementLayer
						.verifyPageElements(
								Arrays.asList("tabMyRegistryMobile", "tabEventInfoMobile",
										"tabShippingInfoMobile", "tabPurchasesMobile"),
										registryInformationPage),
										"Registry tabs are displayed in the Registry page",
										"Registry tabs are not displayed in the Registry page", driver);
			}

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("btnMakeListPrivate"),
							registryInformationPage),
							"Make List Private/public Button is displayed in the registry page",
							"Make List Private/public Button is not displayed in the registry page", driver);

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lnkSocialSharing"),
							registryInformationPage),
							"Social sharing Link and Email Sharing Link is displayed in the registry page",
							"Social sharing Link and Email Sharing Link is not displayed in the registry page", driver);

			Log.softAssertThat(
					registryInformationPage.elementLayer
					.verifyPageElements(Arrays.asList("txtRegistryNameandEventDate"), registryInformationPage),
					"Registry Name and Event Date is displayed in the registry page",
					"Registry Name and Event Date is not displayed in the registry page", driver);

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("txtRegistryId"),
							registryInformationPage),
							"Registry Id is displayed in the registry page",
							"Registry Id is not displayed in the registry page", driver);

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("barcode"),
							registryInformationPage),
							"Bar Code is displayed in the registry page", "Bar Code is not displayed in the registry page",
							driver);

			if (Utils.getRunPlatForm() == "desktop") {

				Log.softAssertThat(
						registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("lnkPrintRegistry"),
								registryInformationPage),
								"Print Registry Link is displayed in the registry page",
								"Print Registry Link is not displayed in the registry page", driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_079

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the search in the Empty Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_075(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		int i = 1;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();

			giftRegistryPage.clickOnViewLink();
			Log.message(i++ + ". Clicked 'View link' in the Gift registry page");
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>System should display the Search result, when customer process using search option");

			Log.assertThat(driver.getCurrentUrl().contains("/search/") && searchResultPage != null,
					"<b>Actaul Result:</b> Customer process using search option and the system displays the Search result",
					"<b>Actaul Result:</b> Customer process using search option and the system does not display the Search result",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_075

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify addresses saves the modified address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_134(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String shippingaddress = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		LinkedHashMap<String, String> savedAddress;// .get("FirstName").toString();

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Click on Registry Link in Headers menu
			SignIn sign = homePage.headers.navigateToSignIn();
			Log.message("2. Navigate to Signin Page");

			// Login with valid user credientials
			RegistrySignedUserPage registrySignIn = sign.signInFromRegistryPage(emailid, password);
			Log.message("3. Sign In with valid user Credientials Email ID:" + emailid + "/Password:" + password);

			homePage.headers.navigateToGiftRegistry();

			// Click on Create New Registry button
			registrySignIn.clickOnCreateNewRegistry();
			Log.message("4. Click on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filling the valid event details");

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("6. Click on Continue button in Event Registry page");

			// Select Saved Address For Pre Event
			registrySignIn.selectSavedPrePostEventAddressByIndex(1, "PreEvent");
			Log.message("7. Select the saved address for Pre Event");

			// Update the PreEvent Address and Fill Post Event address
			registrySignIn.fillingEventShippingdetails(shippingaddress);
			Log.message("8. Update the Pre Event address and Fill Post Event address");
			LinkedHashMap<String, String> filledaddress = registrySignIn.getPreEventShippingDetails();

			// Click on Continue button in Shipping address section
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("9. Click on Continue button in shipping address");

			// Click on Submit button
			registrySignIn.clickOnSubmitButton();
			Log.message("10. Click on Subtmit button in Post Event address");

			// Navigate to Addressbook section
			MyAccountPage myaccount = homePage.headers.navigateToMyAccount();
			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			// Get Total number of address
			int totalsavedaddress = addressbook.getSavedAddress().size();

			Log.message("Reading saved address");
			System.out.println(+totalsavedaddress);

			for (int i = 0; i < totalsavedaddress; i++) {

				savedAddress = addressbook.getSavedAddress().get(i);
				savedAddress.get("FirstName").toString();
				filledaddress.get("FirstName").toString();
				savedAddress.remove("Defaultshipping");
				savedAddress.remove("Defaultbilling");
				savedAddress.remove("Title");
				savedAddress.remove("Address2");
				filledaddress.remove("AddressHome");
				filledaddress.remove("Title");
				filledaddress.remove("Address2");
				if (filledaddress.get("FirstName").equalsIgnoreCase(savedAddress.get("FirstName"))) {
					Log.assertThat((Utils.compareTwoHashMap(filledaddress, savedAddress)),
							"Address modified in the gift registry is saved to the address book.",
							"Address modified in the gift registry is not saved to the address book.");
					break;
				}

			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify addresses saves the modified address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_136(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String shippingaddress = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		boolean status = false;

		LinkedHashMap<String, String> savedAddress;// .get("FirstName").toString();

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message("3. Navigate to create Account page");

			createAccountPage.CreateAccount(createAccountPage);

			GiftRegistryPage giftregistry = homePage.headers.navigateToGiftRegistry();

			// Click on Create New Registry button
			RegistrySignedUserPage registrySignIn = giftregistry.clickOnCreateRegistrybutton();
			Log.message("4. Click on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filling the valid event details");

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("6. Click on Continue button in Event Registry page");

			// Update the PreEvent Address and Fill Post Event address
			registrySignIn.fillingEventShippingdetails(shippingaddress);
			Log.message("7. Enter the Pre Event and Post Event address");

			registrySignIn.clickOnUsePreEventAddress();
			Log.message("8. Update the Pre Event address and Fill Post Event address");
			LinkedHashMap<String, String> filledaddress = registrySignIn.getPreEventShippingDetails();

			// Click on Continue button in Shipping address section
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("9. Click on Continue button in shipping address");

			// Click on Submit button
			registrySignIn.clickOnSubmitButton();
			Log.message("10. Click on Subtmit button in Post Event address");

			// Navigate to Addressbook section
			MyAccountPage myaccount = homePage.headers.navigateToMyAccount();
			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			// Get Total number of address
			int totalsavedaddress = addressbook.getSavedAddress().size();

			Log.message("Reading saved address");
			System.out.println(+totalsavedaddress);
			for (int i = 0; i < totalsavedaddress; i++) {

				savedAddress = addressbook.getSavedAddress().get(i);
				savedAddress.get("FirstName").toString();
				filledaddress.get("FirstName").toString();

				if ((savedAddress.get("Defaultshipping").equalsIgnoreCase("YES"))
						&& (savedAddress.get("Defaultshipping").equalsIgnoreCase("YES"))) {
					status = true;
				}
				savedAddress.remove("Defaultshipping");
				savedAddress.remove("Defaultbilling");
				savedAddress.remove("Title");
				savedAddress.remove("Address2");
				filledaddress.remove("AddressHome");
				filledaddress.remove("Title");
				filledaddress.remove("Address2");

				if ((filledaddress.get("FirstName").equalsIgnoreCase(savedAddress.get("FirstName")))
						&& (status == true)) {
					Log.assertThat((Utils.compareTwoHashMap(filledaddress, savedAddress)),
							"Address is added as default address.", "Address is not added as default address.");
					break;
				}

			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify addresses saves the modified address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_137(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String shippingaddress = testData.get("ShippingInfo");
		String address = testData.get("Address");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		String savedaddressFirstname = null;
		String filledaddresshome = null;
		LinkedHashMap<String, String> savedAddress;// .get("FirstName").toString();
		LinkedHashMap<String, String> selectedAddressbookAddress;

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message("3. Navigate to create Account page");

			createAccountPage.CreateAccount(createAccountPage);

			// Navigate to Addressbook section
			MyAccountPage myaccount = homePage.headers.navigateToMyAccount();
			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			addressbook.clickAddNewAddress();
			addressbook.fillingAddNewAddressDetails(address, "YES", "YES");
			addressbook.ClickOnSave();

			if (addressbook.isSuggestedAddressModalwindowDisplayed()) {
				addressbook.clickContinueAddressSuggestion();
			} else {
				addressbook.clickOnContinueInNoAddressFoundPopup();
			}

			selectedAddressbookAddress = addressbook.getSavedAddress().get(0);

			GiftRegistryPage giftregistry = homePage.headers.navigateToGiftRegistry();

			// Click on Create New Registry button
			RegistrySignedUserPage registrySignIn = giftregistry.clickOnCreateRegistrybutton();
			Log.message("4. Click on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filling the valid event details");

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("6. Click on Continue button in Event Registry page");

			// Select Saved Address For Pre Event
			registrySignIn.selectSavedPrePostEventAddressByIndex(0, "PreEvent");
			Log.message("7. Select the saved address for Pre Event");

			// Update the PreEvent Address and Fill Post Event address
			registrySignIn.fillingEventShippingdetails(shippingaddress);
			registrySignIn.clickOnUsePreEventAddress();
			LinkedHashMap<String, String> modifiedAddress = registrySignIn.getPreEventShippingDetails();

			Log.message("8. Update the Pre Event address and Fill Post Event address");

			// Click on Continue button in Shipping address section
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("9. Click on Continue button in shipping address");

			// Click on Submit button
			registrySignIn.clickOnSubmitButton();
			Log.message("10. Click on Subtmit button in Post Event address");

			// Navigate to Addressbook section
			homePage.headers.navigateToMyAccount();
			AddressBookPage addressbook1 = myaccount.navigateToAddressBook();

			// Get Total number of address
			int totalsavedaddress = addressbook1.getSavedAddress().size();

			Log.message("Reading saved address");

			for (int i = 0; i < totalsavedaddress; i++) {

				savedAddress = addressbook1.getSavedAddress().get(i);
				savedaddressFirstname = savedAddress.get("FirstName").toString();
				filledaddresshome = modifiedAddress.get("FirstName").toString();

				savedAddress.remove("Defaultshipping");
				savedAddress.remove("Defaultbilling");
				savedAddress.remove("Title");
				savedAddress.remove("Address2");
				modifiedAddress.remove("AddressHome");
				selectedAddressbookAddress.remove("Defaultshipping");
				selectedAddressbookAddress.remove("Title");
				selectedAddressbookAddress.remove("Address2");
				selectedAddressbookAddress.remove("Defaultbilling");

				if (selectedAddressbookAddress.get("FirstName").equalsIgnoreCase(savedAddress.get("FirstName"))) {
					Log.assertThat((Utils.compareTwoHashMap(selectedAddressbookAddress, savedAddress)),
							"Selected address is not affected due to Registry creation using this address",
							"Selected address is modified in the address book");
				}

				if (savedaddressFirstname.equalsIgnoreCase(filledaddresshome)) {

					Log.assertThat((Utils.compareTwoHashMap(modifiedAddress, savedAddress)),
							"Modified Address is added into the addressBook as new Address",
							"Modified Address is not added into the addressBook as new Address");
					break;
				}
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify addresses saves the modified address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_138(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String shippingaddress = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		String savedaddressFirstname = null;
		String filledaddress = null;
		LinkedHashMap<String, String> savedAddress;// .get("FirstName").toString();
		LinkedHashMap<String, String> modifiedAddress;

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigate to resgistry page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message("3. Navigate to create Account page");

			createAccountPage.CreateAccount(createAccountPage);

			GiftRegistryPage giftregistry = homePage.headers.navigateToGiftRegistry();

			// Click on Create New Registry button
			RegistrySignedUserPage registrySignIn = giftregistry.clickOnCreateRegistrybutton();
			Log.message("4. Click on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filling the valid event details");

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("6. Click on Continue button in Event Registry page");

			// Update the PreEvent Address and Fill Post Event address
			registrySignIn.fillingEventShippingdetails(shippingaddress);
			Log.message("7. Entering Pre-Event address");

			registrySignIn.clickOnUsePreEventAddress();
			Log.message("8. Clicking on Use Pre-Event address");

			modifiedAddress = registrySignIn.getPreEventShippingDetails();
			Log.message("9. Get Modified Address");

			// Click on Continue button in Shipping address section
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("10. Click on Continue button in shipping address");

			// Click on Submit button
			registrySignIn.clickOnSubmitButton();
			Log.message("11. Click on Subtmit button in Post Event address");

			// Navigate to Addressbook section
			MyAccountPage myaccount = homePage.headers.navigateToMyAccount();
			AddressBookPage addressbook1 = myaccount.navigateToAddressBook();

			// Get Total number of address
			int totalsavedaddress = addressbook1.getSavedAddress().size();

			Log.message("12. Reading saved address");

			for (int i = 0; i < totalsavedaddress; i++) {

				savedAddress = addressbook1.getSavedAddress().get(i);
				savedaddressFirstname = savedAddress.get("FirstName").toString();

				Log.message("Before Editing First Name:" + savedaddressFirstname);

				filledaddress = modifiedAddress.get("FirstName").toString();

				if (savedaddressFirstname.equalsIgnoreCase(filledaddress)) {

					addressbook1.clickOnEditAddressByIndex(i + 1);
					// Modifiying address
					addressbook1.setFirstName("Firstname");
					addressbook1.setLastName("Lastname");
					addressbook1.clickOnSaveInAddNewAddressModalAfterEdit();

					if (addressbook1.isSuggestedAddressModalwindowDisplayed()) {
						addressbook1.selectUseOriginalAddress();
						addressbook1.clickContinueAddressSuggestion();
						savedAddress.clear();
						savedAddress = addressbook1.getSavedAddress().get(i);
						savedaddressFirstname = savedAddress.get("FirstName").toString();
					}

					homePage.headers.navigateToGiftRegistry();
					// registrySignIn.clickOnRegistrylink();
					Log.message("13. Navigate to Registry page");

					registrySignIn.clickOnViewRegistrylnk();
					Log.message("14. Clicking on View Link in My Registry Section");

					// Navigate to ShippingInfo Tab
					registrySignIn.clickOnShippingInfo();
					Log.message("15. Navigate to Shipping Info tab");

					// Reading Shipping info Shipping address
					modifiedAddress = registrySignIn.getPreEventShippingDetails();
					filledaddress = modifiedAddress.get("FirstName").toString();

					Log.message("16. Get Saved Registry address after modified from the addressbook");

					// Removing Non Comparable components
					savedAddress.remove("Defaultshipping");
					savedAddress.remove("Defaultbilling");
					savedAddress.remove("Title");
					savedAddress.remove("Address2");

					modifiedAddress.remove("AddressHome");
					modifiedAddress.remove("Defaultshipping");
					modifiedAddress.remove("Address2");
					modifiedAddress.remove("Defaultbilling");

					// Comparing Modified address with ShippingInfo Address
					if (savedaddressFirstname.equalsIgnoreCase(filledaddress)) {
						Log.message("Modified Field and Values FirstName:" + savedaddressFirstname, driver);
						Log.assertThat((Utils.compareTwoHashMap(modifiedAddress, savedAddress)),
								"Modified Address is updated in ShippingInfo Page as well",
								"Modified Address is not updated in ShippingInfo Page");
						break;
					} else {
						Log.message("No address is matching to Edit");
					}
				}
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify details under 'Pre-Event and Post Event Shipping' heading.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_129(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page

			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			registrySignedUserPage.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filled the Registry Event Details");
			registrySignedUserPage.clickOnContinueButton();
			Log.message("6. Click on the Continue button on the Event Page");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: <b>  'Pre event and Post Event Shipping' heading should be displayed with the form values for 'Select or Add an Address'");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("postEvent",
							"txtSelectAddAddressPreEvent", "lblPreEventShipping", "txtSelectAddAddressPostEvent"),
							registrySignedUserPage),
							"<b> Actual Result 1: </b> 'Navigate to Create Registry Step 2 - 'Pre event and Post event' Shipping heading is displayed with 'Select  or Add an Address ",
							"<b> Actual Result 1: </b> 'Navigate to Create Registry Step 2 - 'Pre event and Post event 'Shipping heading is not displayed",
							driver);
			Log.message("<br>");
			Log.message("<b> Expected Result 2: <b>  'Select Saved Address, Drop down should be displayed");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("selectSavedAddressInPreEventShipping",
									"selectSavedAddressInPostEventShipping", "txtSelectSavedAddress"),
									registrySignedUserPage),
									"<b> Actual Result 2: </b> 'Navigate to Create Registry Step 2 - ' Select Saved Address Heading with saved Address Drop down box is displayed",
									"<b> Actual Result 2: </b> 'Navigate to Create Registry Step 2 - 'Select Saved Addres Heading and drop down box  is not displayed",
									driver);
			Log.message("<br>");
			Log.message("<b> Expected Result 3: <b>  Form Label should be displayed in the Pre Event Shipping Form");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("lblFirstNamePreEvent",
							"lblLastNamePreEvent", "lblAddressPreEvent", "lblCityPreEvent"), registrySignedUserPage),
							"<b> Actual Result 3: </b> The Label are displayed in the 'Pre Event Shipping Form ' are -Label First Name, Label Last Name,Label Address ,Label City ",
							"<b> Actual Result 3: </b> Labels are not dispalyed in the Pre Event shipping form", driver);
			Log.message("<br>");
			Log.message("<b> Expected Result 4: <b>  Form Label should be displayed in the Pre Event Shipping Form");
			Log.softAssertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("lblStatePreEvent", "lblPostalPreEvent", "lblPhoneNoPreEvent"),
							registrySignedUserPage),
							"<b> Actual Result 4: </b> The Label are displayed in the 'Pre Event Shipping Form ' are -Label State, Label Zip code,Label Phone no. ",
							"<b> Actual Result 4: </b> Labels are not dispalyed in the Pre Event shipping form", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_129

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system allows the customer to add Pre Shipping address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_130(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page

			RegistrySignedUserPage registrySignIn = signIn.signInToMyRegistryAccount(emailid, password);

			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			registrySignIn.clickOnCreateNewRegistry();
			Log.message("4. Click on the create new Registry", driver);
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message("5. Filled the Registry Event Details");
			registrySignIn.clickOnContinueButton();

			Log.message("6. Click on Continue button in Event Registry page");
			registrySignIn.selectSavedAddressbyIndex(2);
			Log.message("7. Select the saved address for Pre Event");
			LinkedHashMap<String, String> PreShippingInfo = registrySignIn.getPreEventShippingDetails();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b>  Address fields should be auto filled once the customer choose the address from the 'Saved Address' drop down.");
			Log.message(
					"<b> Actual Result 1: </b> Pre Event form get filled once the customer select the Saved address from the drop down and the pre event details are :  "
							+ PreShippingInfo,
							driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 2: </b>  First name, Last name, Address 1, Address 2, City, State, Zip code and phone fields should be active");
			Log.message(
					"<b> Actual Result 2: </b> All the Pre Event form field are active ,form is filled and the Pre Event shipping deatils are :  "
							+ PreShippingInfo,
							driver);
			Log.message("<br>");
			LinkedHashMap<String, String> EditPreShippingInfo = registrySignIn
					.fillingPreEventShippingdetails("Shipping_PreEvent_Updateddetails");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 3: </b>  First name, Last name, Address 1, Address 2, City, State, Zip code and phone fields should be editable");
			Log.assertThat(!(PreShippingInfo.equals(EditPreShippingInfo)),
					"<b> Actual Result 3: </b> As PreShipping Info and Edit PreShipping Info are not equal so that the  Pre Event shipping details are editibale and Updated ",
					"<b> Actual Result 3: </b> As PreShipping Info and Edit PreShipping Info are equal so the Pre Event shipping details are not editibale ",
					driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_130

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Make This List Private/Public button in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_331(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			WishListPage wishlistpage = signinPage.signInToWishList(emailid, password);
			Log.message("3. Entered Valid credentials(Email ID:" + emailid + "/Password:" + password
					+ ") in 'WishListPage' page", driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> The button text should toggle Make This List Public to Make This List Private upon clicking. A Wish List should be private by default.");

			Log.softAssertThat(
					wishlistpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lnkShareFacebook", "lnkShareGooglePlus", "lnkShareTwitter"), wishlistpage),
							"<b> Actual Result 1: </b> Wislist is private by default and social icons are not visible when the wishlist is private. ",
							"<b> Actual Result 1: </b> Wislist is not private by default and social icons are visible when the wishlist is private.",
							driver);
			Log.message("<br>");
			wishlistpage.clickOnMakeThisList();
			Log.message("4. Click on the 'Make this List Pivate or Public'");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 2: </b> When the Wish List owner clicks Make This List Public button, their Wish List should be searchable by all customers.");
			Log.softAssertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("lnkShareFacebook", "lnkShareGooglePlus", "lnkShareTwitter"), wishlistpage),
							"<b> Actual Result 2: </b> Clicked on the Wishlist button ,make it public then the social icons are displayed and wishlist is searchable by all the customers. ",
							"<b> Actual Result 2: </b> Clicked on the Wishlist button ,make it public then the social icons are not displayed and wishlist is not  searchable by all the customers.",
							driver);

			Log.message("<br>");
			wishlistpage.clickOnMakeThisList();
			Log.message("5. Click on the 'Make this List Pivate or Public'");
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 3: </b> When the Wish List owner clicks Make This List Private button, other customers should no longer search for or view this customer’s Wish List.");
			Log.softAssertThat(
					wishlistpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lnkShareFacebook", "lnkShareGooglePlus", "lnkShareTwitter"), wishlistpage),
							"<b> Actual Result 3: </b> Clicked on the Wishlist button ,make it private then the social icons are not displayed and wishlist is not searchable by all the customers. ",
							"<b> Actual Result 3: </b> Clicked on the Wishlist button ,make it private then the social icons are displayed and wishlist is searchable by all the customers.",
							driver);
			Log.message("<b>");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_331

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Wish List Shipping Address dropdown in Wish List with no items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_330(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("2. Clicked WishList button on header and navigated to 'WishList' Page!", driver);

			WishListPage wishlistpage = signinPage.signInToWishList(emailid, password);
			Log.message("3. Entered Valid credentials(Email ID:" + emailid + "/Password:" + password
					+ ") in 'WishListPage' page", driver);
			Log.message("<br>");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_WISHLIST_330


	
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user allow edit the pre or post shipping from Shipping Info screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_052(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();

			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);

			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage!");

			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage!");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry!");

			// Navigated To Registry
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			// Click on View Registry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");

			// Click On Purchase Header Tab
			registrySignedUserPage.clickOnShippingInfo();
			Log.message(i++ + "Clicked On Purcahse Header Tab!");

			// Filling Shipping Details
			LinkedHashMap<String, String> fillingEventShippingdetails = registrySignedUserPage
					.fillingPostEventShippingdetails("Shipping_PreEvent_details");
			List<String> indexes = new ArrayList<String>(fillingEventShippingdetails.keySet());
			String value = indexes.get(6).replace("type_prezipCode_", "");
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");

			// Click On Purchase Header Tab
			registrySignedUserPage.clickOnContinueButtonInEventShipping();
			Log.message(i++ + "Clicked On Apply Button!");
			BrowserActions.nap(5);

			// Click On Purchase Header Tab
			registrySignedUserPage.clickOnShippingInfo();
			Log.message(i++ + "Clicked On Purcahse Header Tab!");

			LinkedHashMap<String, String> fillingEventShippingdetails1 = registrySignedUserPage
					.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			List<String> indexes1 = new ArrayList<String>(fillingEventShippingdetails1.keySet());
			String value1 = indexes1.get(6).replace("type_prezipCode_", "");
			registrySignedUserPage.fillingPreEventShippingdetails("Shipping_PostEvent_details");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be allow to edit the pre or post shipping Info screen");
			Log.assertThat(!(value.equals(value1)),
					"<b>Actual Result :</b> User is allowed to edit the pre or post shipping Info screen",
					"<b>Actual Result :</b> User is not allow to edit the pre or post shipping Info screen", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_052


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can add/edit/remove the item from My Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_048(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);

			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage!");

			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage!");
			pdpPage.selectColor();
			pdpPage.selectSize();
			String ProductUpcInPdpPage = pdpPage.getUPCValue();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry!");
			Log.message(ProductUpcInPdpPage);

			// Navigated To Registry
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			// Click on View Registry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");
			String ProductUpcInMyRegistry = registryInformationPage.getUPCValue();
			Log.message(ProductUpcInMyRegistry);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should be allowed to add item from My Registry screen");
			Log.assertThat(ProductUpcInPdpPage.equals(ProductUpcInMyRegistry),
					"<b>Actual Result 1:</b> Item added to my registry",
					"<b>Actual Result 1:</b> Item is not added to my registry", driver);

			// Click on Remove button
			registryInformationPage.removeItemsFromRegigistry();
			Log.message(i++ + ". Clicked on Remove Button!");

			String Message = registryInformationPage.getText();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> User should be allowed to Remove item from My Registry screen");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("messageNoitem"),
							registryInformationPage),
							"<b>Actual Result 2:</b> Item is removed from my registry" + Message,
							"<b>Actual Result 2:</b> Item is not removed from my registry", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_048

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user allow to change Qty or priority from My Register screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);

			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage!");

			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage!");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry!");

			// Navigated To Registry
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			// Click on View Registry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");

			// Clicking On Prority Drop down
			String prority = registryInformationPage.getQunatityOfPrority(1);
			registryInformationPage.SelectProrityByIndex(1);
			String prority1 = registryInformationPage.getQunatityOfPrority(1);
			Log.message(i++ + ". Clicked on priority Drop Down!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should be allow to change priority from My Register screen");
			Log.assertThat(!(prority.equals(prority1)),
					"<b>Actual Result 1:</b> User is able to change priority from My Register screen ",
					"<b>Actual Result 1:</b> User is not able to change priority from My Register screen");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_049

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the order in the Purchase tab.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_053(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey");
		int i = 1;

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();

			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);

			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);

			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage!");

			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage!");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry!");

			// Navigated To Registry
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			// Click on View Registry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");

			// Click on Add To Bag
			registryInformationPage.clickOnAddtoBag();
			Log.message(i++ + ". Product added to Shopping Bag!");

			// Click on Mini Cart
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + "Clicked On Mini Cart!");

			// Click on CheckOut Button
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message(i++ + "Clicked On CheckOut Button!");

			// Filling Shipping Details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address4");
			Log.message(i++ + "Shipping Details Filled successfully!");

			// Click on Continue Button
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + "Clicked On Continue Button!");

			// Filling Billing Details
			checkOutPage.fillingBillingDetailsAsSignedInUser("No", "No", "valid_address4");
			Log.message(i++ + "Billing Details Filled successfully!");

			// Filling Card Details
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + "Card Details Filled successfully!");

			// Click on Continue Button
			checkOutPage.clickOnContinueInBilling();
			Log.message(i++ + "Clicked On Continue Button!");

			// Click on Continue Button
			checkOutPage.ClickOnPlaceOrderButton();
			OrderConfirmationPage orderconfirmationpage = new OrderConfirmationPage(driver).get();
			Log.message(i++ + "Clicked On Continue Button!");

			// Navigated To Registry
			String Productname = orderconfirmationpage.getProductName();
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");

			// Click on View Registry
			registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");

			// Click on Purchase Tab
			registryInformationPage.clickOnPurchasesTab();
			String Productname1 = registrySignedUserPage.getProductName();
			Log.message(i++ + ". Clicked on Purchase Tab!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be allow to view the purchases from registry screen");
			Log.assertThat(Productname.equals(Productname1),
					"<b>Actual Result :</b>  User is allowed to view the purchases from registry screen",
					"<b>Actual Result :</b>  User is not allowed to view the purchases from registry screen", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_053

	@Test(groups = { "desktop", "mobile" }, description = "Verify for mobile Breadcrumbs will start with a single left arrow.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_235(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "mobile") {
			throw new SkipException(
					"This test case is applicable only for mobile");
		} else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			String emailid = testData.get("EmailAddress");
			String password = testData.get("Password");
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SignIn signIn = homePage.headers.navigateToSignIn();
				Log.message("2. Navigated to SighIn Page from Home Page");
				// Navigate to My Account Page
				MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
						password);
				Log.message("3. Navigated to My Account Page!");
				myAccountPage.headers.navigateToGiftRegistry();
				RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
						driver).get();
				Log.message("4. Navigated to giftregistry page on Clicking Registry");
				String actualbreadcrumbtxt = registrySignedUserPage
						.getTextFromBreadCrumbMobile();
				Log.message("<b>Expected Result:</b>  Bread crumb should be displayed as < Back to Women");
				Log.assertThat(
						actualbreadcrumbtxt.equals("Back to Home"),
						"<b>Actual Result:</b> Bread crumb is displayed as < Back to Women",
						"<b>Actual Result:</b> Bread crumb is not displayed as < Back to Women",
						driver);
				Log.testCaseResult();
			}

			catch (Exception e) {
				Log.exception(e, driver);
			}// catch
			finally {
				Log.endTestCase();
				driver.quit();
			}// finally
		}
	}// TC_BELK_REGISTRY_235

	@Test(groups = { "desktop", "mobile" }, description = "Verify for mobile Breadcrumbs will start with a single left arrow.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_WISHLIST_346(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SighIn Page from Home Page");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			WishListPage wishListPage = myAccountPage.headers
					.navigateToWishList();
			Log.message("4. Navigated to WishList Page!");
			wishListPage.clickOnProductName("IZOD Big & Tall Self Collar");
			Log.message("5. Clicked on Product Name and Edit Wish List Item modal opened");
			Log.message("<b>Expected Result:</b>  The 'Add to Wish List' and 'Add to Registry' links should be removed from Edit Wish List Item modal.");
			Log.assertThat(
					wishListPage.VerifyLinkNotPresent("Add to Wish List")
					& wishListPage
					.VerifyLinkNotPresent("Add to Registry"),
					"<b>Actual Result:</b> The 'Add to Wish List' and 'Add to Registry' links is removed from Edit Wish List Item modal.",
					"<b>Actual Result:</b> The 'Add to Wish List' and 'Add to Registry' links is not removed from Edit Wish List Item modal.",
					driver);
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			// driver.quit();
		}// finally

	}// TC_BELK_REGISTRY_346

	@Test(groups = { "desktop", "mobile" }, description = "Verify links are disabled to Product details page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_107(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			SearchResultPage
			searchResultPagehomePage=homePage.headers.searchProductKeyword
			(searchKey); 
			Log.message("7. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page"); 
			PdpPage pdpPage=searchResultPagehomePage.selectProductByIndex(1);
			Log.message("8. Navigated to PDP Page from Search Results Page");
			Log.message("9. Searched with keyword '" + searchKey + "' and navigated to PDP Page");
			pdpPage.selectColor();
			pdpPage.selectSizeByIndex(1);
			Log.message("10.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("11. Quantity drowdown is selected with Quantity 2");
			registrySignedUserPage=pdpPage.navigateToRegistryAsSignedUser();
			Log.message("12. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("13. Navigated to Registry Page"); if
			(Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1); } else {
					registrySignedUserPage.viewRegistryByIndexMobile(1); }
			Log.message("14. Clicked View from the Active Registry");

			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			String productnametxt = registryInformationPage
					.getProductNametxt(1);
				registryInformationPage.clickProductName(1);
			Log.message("15. Clicking on Product Name and Registry Item Model opened");
			Log.message("<b>Expected Result:</b> View Details link should be removed");
			Log.assertThat(registryInformationPage
					.VerifyLinkNotPresent("View Details"),
					"<b>Actual Result:</b> View Details link is removed",
					"<b>Actual Result:</b>  View Details link is not removed",
					driver);

			Log.message("<b>Expected Result:</b> Product Name and Star review should be disabled");
			Log.assertThat(
					registryInformationPage
					.IsProductNameDisabled(productnametxt)
					& registryInformationPage
					.VerifyLinkNotPresent("reviews"),
					"<b>Actual Result:</b> Product name and Star review is disabled",
					"<b>Actual Result:</b> Product name and Star review is not disabled",
					driver);
			registryInformationPage.clickCancel();
			registryInformationPage.removeItemsFromRegigistry();
			Log.message("16. Items removed from Registry");
		}

		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			 driver.quit();
		}// finally
	}// TC_BELK_REGISTRY_107

	@Test(groups = { "desktop", "mobile" }, description = "Verify conversion of lower case and white spaces in the Url.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_242(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : "
					+ searchKey);
			String Productnamefrombreadcrumb = pdpPage
					.getProductNameFromBreadCrumb();
			String url = driver.getCurrentUrl();
			String[] productnamefromurl = url.split("\\/p/")[1].split("\\/");
			Log.message("<b>Expected Result:</b> Url should be converted and display in lower cases.");
			Log.assertThat(
					pdpPage.isURLInLowercase(productnamefromurl[0]),
					"<b>Actual Result:</b> Url is converted and displayed in lower cases.",
					"<b>Actual Result:</b> Url is not converted and displayed in lower cases.",
					driver);

			Log.message("<b>Expected Result:</b> White spaces between the products name should be displayed with '-'");
			Log.assertThat(
					pdpPage.verifyspacesconvertedtohyphen(
							Productnamefrombreadcrumb, productnamefromurl[0]),
							"<b>Actual Result:</b> White spaces between the products name is displayed with '-'.",
							"<b>Actual Result:</b> White spaces between the products name is not displayed with '-'",
							driver);
			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_REGISTRY_242

	@Test(groups = { "desktop", "mobile" }, description = "Verify Still Needs value displays in the Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_099(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			SearchResultPage searchResultPagehomePage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("7. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPagehomePage.selectProductByIndex(2);
			Log.message("8. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(4);
			Log.message("9.  Size is Selected from size dropdown");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			Log.message("<b>Expected Result:</b> Still Needs value should be a calculated value which should be the difference of the Would Love and Purchased values.");
			registrySignedUserPage.selectWouldLoveIndex(2);
			registryInformationPage.clickupdate();
			int wouldcount = Integer.parseInt(registrySignedUserPage
					.getWouldLoveSelectedvalue());
			int stillneed = Integer.parseInt(registryInformationPage
					.stillNeedsTxt());
			int qtypurshased = Integer.parseInt(registrySignedUserPage
					.getQtySelectedvalue());
			registryInformationPage.clickOnAddtoBag();
			Log.message("13. Purchase [" + qtypurshased + "]"
					+ "of the product");
			ShoppingBagPage shoppingBagPage = registryInformationPage
					.clickOnMiniCart();
			Log.message("14. Navigated to shopping Cart Page");
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			checkoutPage.clickBtnShippingPage();
			Log.message("15. Clicked Continue button");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("16. Payment details Entered");
			checkoutPage.clickBtnShippingPage();
			Log.message("17. Clicked Continue button");
			Log.message("18. Order Placed with [" + qtypurshased + "]");
			OrderConfirmationPage orderConfirmationPage =  checkoutPage.placeOrder();
			GiftRegistryPage giftRegistryPage = orderConfirmationPage.headers
					.navigateToGiftRegistry();
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("19. Clicked View from the Active Registry");
			registryInformationPage = new RegistryInformationPage(driver).get();
			int stillneeds = Integer.parseInt(registryInformationPage
					.stillNeedsTxt());

			Log.softAssertThat(
					stillneeds == wouldcount - qtypurshased,
					"<b>Actual Result1:</b> Still Needs value is the difference of the Would Love and Purchase values",
					"<b>Actual Result1:</b> Still Needs value is not the difference of the Would Love and Purchase values",
					driver);
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// // TC_BELK_REGISTRY_099

	@Test(groups = { "desktop", "mobile" }, description = "Verify product attributes is is Registry screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_090(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("7. Navigated to PdpPage using the search key : "
					+ searchKey);
			LinkedHashMap<String, String> productdetailsfrompdp = pdpPage
					.setGetProductDetails();
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("8. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("9. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("10. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			LinkedHashMap<String, String> productdeatilsfromregistry = registryInformationPage
					.GetProductDetails(1);
			Log.message("<b>Expected Result:</b> Product Name should be displayed. ");
			Log.assertThat(
					productdeatilsfromregistry
					.get("ProductName")
					.toString()
					.contains(
							productdetailsfrompdp.get("ProductName")
							.toString()),
							"<b>Actual Result:</b> Product Name is displayed",
							"<b>Actual Result:</b> Product Name is not displayed",
							driver);
			Log.message("<b>Expected Result:</b> UPC value for the product should be displayed. ");
			Log.assertThat(
					productdeatilsfromregistry
					.get("Upc")
					.toString()
					.equals(productdetailsfrompdp.get("Upc").toString()),
					"<b>Actual Result:</b> UPC value for the product is displayed",
					"<b>Actual Result:</b> UPC value for the product is not displayed",
					driver);
			Log.message("<b>Expected Result:</b> Color of the product should be displayed. ");
			Log.assertThat(
					productdeatilsfromregistry
					.get("Color")
					.toString()
					.equals(productdetailsfrompdp.get("Color")
							.toString()),
							"<b>Actual Result:</b> Color of the product is displayed",
							"<b>Actual Result:</b> Color of the product is not displayed",
							driver);
			Log.message("<b>Expected Result:</b> Size of the product should be displayed. ");
			Log.assertThat(
					productdeatilsfromregistry
					.get("Size")
					.toString()
					.equals(productdetailsfrompdp.get("Size")
							.toString()),
							"<b>Actual Result:</b> Size of the product is displayed",
							"<b>Actual Result:</b> Size of the product is not displayed",
							driver);
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_090

	@Test(groups = { "desktop","mobile" }, description = "Verify Content Asset HTML field displays in registry content pages.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_206(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);


		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");


			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Registry SignIn page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message("3. Navigated to create Account page");

			createAccountPage.CreateAccount(createAccountPage) ;
			Log.message("4. Created a new Account with valid credantials");	

			RegistrySignedUserPage registrySignIn = homePage.headers.navigateToASingedUserRegistry();
			Log.message("5. Navigated to Registry Page");
			// Click on Create New Registry button

			registrySignIn.findRegistry("belk", "registry233", "Wedding Registry");



			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Verify Content Asset HTML field displays in registry content pages.");
			Log.assertThat(
					registrySignIn.elementLayer.verifyPageElements(
							Arrays.asList("registryHtmlContent"),
							registrySignIn),
							"<b>Actual Result 1:</b> Content Asset HTML field displays in registry content pages.",
							"<b>Actual Result 1:</b> Content Asset HTML field not displays in registry content pages.",
							driver);
			Log.message("<br>");

			MyAccountPage myAccount = homePage.headers.navigateToMyAccount();
			Log.message("6. Clicked on 'MyAccount' button on header and Navigated to 'MyAccount' Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Verify Content Asset HTML field displays in registry content pages.");
			Log.assertThat(
					myAccount.elementLayer.verifyPageElements(
							Arrays.asList("divMyAccount"),
							myAccount),
							"<b>Actual Result 2:</b> Content Asset HTML field displays in registry content pages.",
							"<b>Actual Result 2:</b> Content Asset HTML field not displays in registry content pages.",
							driver);
			Log.message("<br>");
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("7. Clicked on 'WishList' button on header and Navigated to 'WishList' Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Verify Content Asset HTML field displays in registry content pages.");
			Log.assertThat(

					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("wishListHtmlContent"),
							wishlistpage),
							"<b>Actual Result 3:</b> Content Asset HTML field displays in registry content pages.",
							"<b>Actual Result 3:</b> Content Asset HTML field not displays in registry content pages.",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_206

	

	@Test(groups = { "desktop", "mobile" }, description = "Verify details of My Registry, Event info, Shipping info and Purchase", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_047(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// / Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn
					.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Click on View Registry
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> The following options should be available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!");
			Log.assertThat(registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("prioritylbl","Qtylbl","btnadd","btnEdit","btnRemove"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> All options are available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!",
					"<b>Actual Result 1:</b> All options are not available available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!");


			// Click on Event Info 
			registrySignedUserPage.clickOnEventInfo();
			Log.message("5. Clicked on Event Info!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> The following options should be available to the user: Event Type, Event Name, Event Date, Event Location (City, StateEdit Registrant and CoRegistrant Information (Role, First & Last Name and Email!");
			Log.assertThat(registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("txtBoxFirstName","txtLastNameRegistry","txtEmailInForm","drpRoleType","selectStateInCreateNewRegistry"),
							registrySignedUserPage),
							"<b>Actual Result 2:</b> All options are available to the user: Event Type, Event Name, Event Date, Event Location (City, StateEdit Registrant and CoRegistrant Information 'Role, First & Last Name and Email'!",
							"<b>Actual Result 2:</b> All options are not available: Event Type, Event Name, Event Date, Event Location (City, StateEdit Registrant and CoRegistrant Information 'Role, First & Last Name and Email'!",driver);

			// Click on Shipping Info 
			registrySignedUserPage.clickOnShippingInfo();
			Log.message("6. Clicked on Shipping Info!");

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> The following options should be available to the user: Pre & Post shipping locations!");
			Log.assertThat(registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("preAddressInShippingInfo","preCityInShippingInfo","postCityInShippingInfo","postaddressInShippingInfo"),
							registrySignedUserPage),
							"<b>Actual Result 3:</b> All options are available to the user: Pre & Post shipping locations!",
					"<b>Actual Result 3:</b> All options are not available available to the user: Pre & Post shipping locations!");

			// Click on Purchase Info 
			registrySignedUserPage.clickOnPurchases();
			Log.message("7. Clicked on Purchases Info!");

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> The following options should be available to the user: View purchases made from selected registry!");
			Log.assertThat(registrySignedUserPage.elementLayer
					.verifyPageElements(Arrays.asList("purchaseBag"),
							registrySignedUserPage),
							"<b>Actual Result 4:</b> All options are available to the user: View purchases made from selected registry!",
					"<b>Actual Result 4:</b> All options are not available available to the user: View purchases made from selected registry!");
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_047

	


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user allowed to make item public or private or Share Registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_050(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		String searchKey = testData.get("SearchKey");
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			// create registry account
			String[] details = createRegistryAccount(homePage, "weddingRegistry", "valid_shipping_address6", i);
			i = Integer.parseInt(details[3]);

			// Search the product with keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with '" + searchKey + "' product and Navigated to searchResultPage!");

			// select the product
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message(i++ + ". Selected the product and Navigated to pdpPage!");
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Product added to my registry!");

			// Navigated To Registry
			homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + "Navigated To Registry!");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			// Clicked on view Registry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked on View Registry!");

			String ItemTypeAfter = registryInformationPage.getBtnItemname();
			registryInformationPage.clickOnMakeThisItem();
			String ItemTypeBefore = registryInformationPage.getBtnItemname();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should be allowed to make public or private!");
			Log.assertThat(!(ItemTypeAfter.equals(ItemTypeBefore)),
					"<b>Actual Result 1:</b> User is able to make item Public/Private from My Register screen!",
					"<b>Actual Result 1:</b> User is able to make item Public/Private from My Register screen!");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_050


	@Test(enabled=false,groups = { "desktop", "mobile", "tablet" }, description = "Verify Order Summary details in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_289(String browser) throws Exception 
	{
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchkey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);


		try {
			//Navigate to Belk Home Page
			HomePage homepage= new HomePage(driver,webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");	

			//Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchkey);
			Log.message("2. Search "+ searchkey	+ " in the home page and  Navigated to 'Search Result'  Page");

			//Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("3. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.selectSize();		
			pdpPage.selectQuantity("3");
			pdpPage.clickAddToBag();
			String productname=pdpPage.getProductName();
			Log.message("4. "+pdpPage.getProductName()+"Product is added into Shopping bag");	

			if(pdpPage.isselectFreeGiftModal()==true)
			{				
				Log.message("<b>Actual Result-1:</b> Free Gift Modal window is displayed" , "<b>Actual Result-1:</b> Free Gift Modal window is not displayed");
				pdpPage.clickOnNoThanks();			
			}
			// Click on Mini cart(View bag)
			ShoppingBagPage shoppingbag=homepage.clickOnMiniCart();

			Log.message("5. Clicking on Minicart(View bag) button");

			//shoppingbag.
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The mini­cart should automatically open when the User adds a product to shopping cart from the PDP");
			Log.assertThat(	pdpPage.elementLayer.verifyPageElements(
					Arrays.asList("addedToMyBagPopUp"), pdpPage),
					"<b>Actual Result-1:</b> The mini­cart is automatically open when the User adds a product to shopping cart from the PDP",
					"<b>Actual Result-1:</b> The mini­cart is notautomatically open when the User adds a product to shopping cart from the PDP",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	
	}
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the order in the Purchase tab.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_171(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to MyRegisty Sign In Page!");
			// Navigate to MyRegistryAccount Page
			RegistrySignedUserPage registrySignedUserPage = signIn
					.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered EmailId '" + emailid + "' and password '"
					+ password + "' and signedIn to myRegistry Account!");
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage
					.clickOnViewRegistrylnk();
			Log.message("4. Clicked on View Registry!");
			registryInformationPage.clickOnPurchasesTab();
			Log.message("5. Clicked on purchase Tab !");
			ArrayList<String> DateInPurchasesTab = registryInformationPage
					.getFormatOfDateInPurchaseForMultipleProduct();
			Log.message("6. Got the date format from purchase Tab !");
			Log.message("<br>");
			Log.message("<b>Expected Result</b>");
			Log.message("Purchases should be ordered by Date Purchased Newest - Oldest.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			ArrayList<String> sortedDate = new ArrayList<String>(
					DateInPurchasesTab);
			Collections.sort(sortedDate);
			Log.assertThat(DateInPurchasesTab.equals(sortedDate),
					"Purchases ordered by Date Purchased Newest - Oldest.",
					"Purchases not ordered by Date Purchased Newest - Oldest.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_171
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify registry event details are displayed in Event Name and Event Date", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_181(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = "kar";
		String lstName = "thick";
		String event = "Wedding Registry";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
			try {
				// Launching home page
				
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page");
				SignIn signinPage = homePage.headers.navigateToSignIn();
				Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
				MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
						password);
				Log.message("3. Entered the credentials(" + emailid + "/"
						+ password + ") and navigating to 'My Account' page");
				ShoppingBagPage shoppingBagPage = null;
				if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
					shoppingBagPage = myaccount.clickOnMiniCart();
					shoppingBagPage.removeItemsFromBag();
				} 
				Log.message("4. Products removed from Cart");
				 myaccount.headers.navigateToGiftRegistry();
				RegistrySignedUserPage registrySignedUser = new RegistrySignedUserPage(driver).get();
				Log.message("5. Navigated to gift registry");
				registrySignedUser.setFirstNameInFindRegistry(firstName);
				Log.message("6. First name is set ");
				registrySignedUser.setLastNameInFindRegistry(lstName);
				Log.message("7. Last name is set");
				registrySignedUser.selectEventTypeInFindRegistry(event);
				Log.message("8. Event type is selected " + event);
				registrySignedUser.clickOnFindRegistry();
				Log.message("9. Find Registry is clicked");
				RegistryInformationPage registryInformationPage = new RegistryInformationPage(driver).get();
				registryInformationPage.clickOnViewInRegistry(1);
				Log.message("10. view link is clicked",driver);
				Log.message("<br>");
				Log.message("<b>Expected Result :</b> Registry ID, Registrant, Event Type, Location fields and corresponding values should display properly");
			    Log.assertThat(registryInformationPage.elementLayer.verifyPageListElements(Arrays.asList("lstRegistryDesc"), registryInformationPage), "<b>Actual Result :</b> Registry ID,Registrant both first name and last name of registrant and co registrant,Event Type,Location fields and values are displayed properly", "<b>Actual Result :</b> Registry ID,Registrant (both first name and last name of registrant and co registrant) ,Event Type,Location fields and values are not displayed properly");
			    Log.message("<br>");
			    Log.message("<b>Expected Result :</b> Event name and event date should display properly");
			    Log.assertThat(registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("txtEventNameAndDate"), registryInformationPage), "<b>Actual Result :</b> Event date and Event name is displayed properly", "<b>Actual Result :</b> Event date and Event name is not displayed properly");
			    Log.message("<br>");
			    String cityAndState=registryInformationPage.getTextFromCityAndState();
			    Log.message("11. Got the text from city and state :"+cityAndState);
			    Log.message("<br>");
			    Log.message("<b>Expected Result :</b> eventCity and eventState fields should separated by comma");
			    Log.assertThat(cityAndState.contains(","), "<b>Actual Result :</b> eventCity and eventState fields is separated by comma", "<b>Actual Result :</b> eventCity and eventState fields is not separated by comma.");
			    String date= registryInformationPage.getTextFromEventNameAndDate().split("\\-")[1].trim();
			    Log.message("<br>");
			    date=registryInformationPage.formateDayAndMonthIntoDoubleDigit(date);
			    Log.message("<b>Expected Result :</b> Event date should be displayed as mm/dd/yyyyy");
			    Log.assertThat(DateTimeUtility.verifyDateFormat(date,"MM/dd/yy"),"<b>Actual Result :</b> Event date is displayed as mm/dd/yyyyy","<b>Actual Result :</b> Event date is not displayed as mm/dd/yyyyy",driver);
			    Log.testCaseResult();

			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_181
	

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify registrant have the ability to view the product tile displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_077(String browser) throws Exception {


		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> lstProductNames = Arrays.asList("SEDONIA POPPY F/Q QLT SET","Multi Color Stripe Shorts","Beach House Plus Size Solid Skirted Bottom");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn myAccount = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SignIn Page");
			MyAccountPage myAccountPage = myAccount.signInToMyAccount(emailid, password);
			Log.message("3. Entered user credentials email '" + emailid + "' and password '" + password);
			RegistrySignedUserPage registryPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
			Log.message("4. Navigated to Registry Signed User Page");
			RegistryInformationPage registryInformationPage = registryPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked view Link and Navigated to Registry Information Page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Product tile should be displayed");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					registryInformationPage.verifyItemAddedToRegistry(lstProductNames),
					"product added to registry is listed",
					"product added to registry is not listed",
					driver);



			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_077

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify tabs in the Registry page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_076(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn myAccount = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to SignIn Page");
			MyAccountPage myAccountPage = myAccount.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered user credentials email '" + emailid
					+ "' and password '" + password);
			RegistrySignedUserPage registryPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Navigated to Registry Signed User Page");
			registryPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked view Link and Navigated to Registry Information Page");
			registryPage.clickOnEventInfoInEditRegistry();
			registryPage.fillingEventDetails("eventInfo");
			registryPage.pressTab();
			registryPage.clickOnApplyInEditRegistry();
			registryPage.clickOnShippingInfoInEditRegistry();

			registryPage.fillingPreEventShippingdetails("preEvent");
			registryPage.fillingPostEventShippingdetails("postEvent");
			registryPage.clickOnApplyInEditRegistry();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The following Registry Actions should be displayed in the Registry page Registry Tabs, Registry Name & Event Date, Registry ID, Registry Barcode, Social Sharing, Email My Registry, Make List Public / Private, Print Registry");
			Log.message("<b>Actual Result:</b>");

			if(Utils.getRunPlatForm()=="desktop"){

				Log.softAssertThat(registryPage.elementLayer
						.verifyPageElements(Arrays.asList("tabMyRegistryDesktop",
								"tabEventInfoDesktop", "tabShippingInfoDesktop",
								"tabPurchasesDesktop"), registryPage),
						"Registry tabs are displayed in the Registry page",
						"Registry tabs are not displayed in the Registry page",
						driver);
			}

			else {
				Log.softAssertThat(registryPage.elementLayer
						.verifyPageElements(Arrays.asList(
								"tabMyRegistryMobile", "tabEventInfoMobile",
								"tabShippingInfoMobile", "tabPurchasesMobile"),
								registryPage),
						"Registry tabs are displayed in the Registry page",
						"Registry tabs are not displayed in the Registry page",
						driver);
			}

			registryPage.clickOnEventInfoInEditRegistry();
			Log.message("6. Clicked Event Info Tab");
			BrowserActions.nap(5);
			LinkedHashMap<String, String> registryDetailsBefore = registryPage.getEventRegistryDetails();
			Log.message("7. Got the event registry Details before edit");
			registryPage.fillingEventDetails("weddingRegistry");
			Log.message("8. Filled Registry Details");

			registryPage.pressTab();

			registryPage.clickOnApplyInEditRegistry();
			Log.message("9. Clicked On Apply Button");

			registryPage.clickOnShippingInfoInEditRegistry();
			Log.message("10. Clicked Shipping Info Tab");
			LinkedHashMap<String, String> preEventShippingdetailsBefore = registryPage.getPreEventShippingDetails();
			Log.message("11. Got the Pre Event shipping Details before edit");
			registryPage.fillingPreEventShippingdetails("Shipping_PreEvent_details");
			Log.message("12. Filled Pre Event shipping Details");
			LinkedHashMap<String, String> postEventShippingdetailsBefore = registryPage.getPostEventShippingDetails();
			Log.message("13. Got the Post Event shipping Details before edit");
			registryPage.fillingPostEventShippingdetails("Shipping_PostEvent_details");
			Log.message("14. Filled Post Event shipping Details");

			registryPage.clickOnApplyInEditRegistry();
			Log.message("15. Clicked on Apply Button");

			registryPage.clickOnEventInfoInEditRegistry();
			Log.message("16. Clicked Event Info Tab");
			LinkedHashMap<String, String> registryDetailsAfter = registryPage.getEventRegistryDetails();
			Log.message("17. Got the event registry Details after edit");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be edit the event details");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(registryPage.verifyEventDetails(registryDetailsBefore, registryDetailsAfter), 
					"User edit the event details", 
					"User can't edit the event details", 
					driver);
			registryPage.clickOnShippingInfoInEditRegistry();
			Log.message("18. Clicked Shipping Info Tab");
			LinkedHashMap<String, String> preEventShippingdetailsAfter = registryPage.getPreEventShippingDetails();
			Log.message("19. Got the Pre Event shipping Details after edit");
			LinkedHashMap<String, String> postEventShippingdetailsAfter = registryPage.getPostEventShippingDetails();
			Log.message("20. Got the Post Event shipping Details after edit");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be edit the event shipping details");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(registryPage.verifyEventDetails(preEventShippingdetailsBefore, preEventShippingdetailsAfter), 
					"User edit the pre event shipping details", 
					"User can't edit the pre event shipping details", 
					driver);
			Log.softAssertThat(registryPage.verifyEventDetails(postEventShippingdetailsBefore, postEventShippingdetailsAfter), 
					"User edit the post event shipping details", 
					"User can't edit the post event shipping details", 
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_076

	@Test(groups = { "desktop", "mobile" }, description = "Verify system directs to Create Registry: Step 1 Event / Registrant Information", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_042(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> step1_Elements = Arrays.asList(
				"txtEventnameInCreateNewRegistry",
				"txtRegistrantFirstNameInCreateNewRegistry",
				"txtCoRegistrantEmailInCreateNewRegistry",
				"txtRegistrantLastNameInCreateNewRegistry");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// / Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn
					.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Navigate to My Registry Page
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Clicked On Create New Registry Button!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Create Registry: Step 1 Event / Registrant Information should be opened on the click of 'Create New Registry' button.");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							step1_Elements, registrySignedUserPage),
							"<b>Actual Result 1:</b> Step 1 Event/ Registrant Information is opened on the click of 'Create New Registry' button!",
							"<b>Actual Result 1:</b> Step 1 Event/ Registrant Information is not opened on the click of 'Create New Registry' button!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_042

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Registry Banner Content Slot is displayed in My Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_045(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String exceptedCreateRegistry = "Create Registry";
		String exceptedFindRegistry = "Registry";
		String exceptedYourRegistry = "Registry";
		String exceptedMyRegistry = "Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");
			String Actual = registrySignedUserPage.getTextFromMyRegistryHeader();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Verify Registry Banner Content Slot is displayed in My Registry screen");
			Log.assertThat(exceptedMyRegistry.equals(Actual),
					"<b>Actual Result 1:</b> The Registry banner in a global content slot is Displayed!.",
					"<b>Actual Result 1:</b> The Registry banner in a global content slot is not Displayed!");
			Log.message("<br>");
			// Clicked On Create New Registry Button
			registrySignedUserPage.clickOnCreateNewRegistry();
			Log.message("4. Clicked On Create New Registry Button!");
			String Actual1 = registrySignedUserPage.getTextFromGlobalRegistryHeader();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Verify Registry Banner Content Slot is displayed in My Registry screen");
			Log.assertThat(exceptedCreateRegistry.equals(Actual1),
					"<b>Actual Result 2:</b> The Registry banner in a global content slot is Displayed!",
					"<b>Actual Result 2:</b> The Registry banner in a global content slot is not Displayed!", driver);
			Log.message("<br>");
			// Navigate to My Registry Page
			registrySignedUserPage.headers.navigateToGiftRegistry();
			Log.message("5. Navigated to My Registry Page!");

			// Clicked On View Link Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("6. Clicked On View Link Button!");
			String Actual2 = registrySignedUserPage.getTextFromYourRegistryHeader().trim();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Verify Registry Banner Content Slot is displayed in My Registry screen");
			Log.assertThat(exceptedYourRegistry.equals(Actual2),
					"<b>Actual Result 3:</b> The Registry banner in a global content slot is Displayed!",
					"<b>Actual Result 3:</b> The Registry banner in a global content slot is not Displayed!", driver);
			Log.message("<br>");
			
			// Navigate to My Registry Page
			registrySignedUserPage.headers.navigateToGiftRegistry();
			Log.message("7. Clicked On View Link Button!");

			// Navigate to My Registry Page
			registrySignedUserPage.findRegistry("belk", "registry", "Wedding Registry");
			Log.message("8. Clicked On Find Registry!");
			String Actual3 = registrySignedUserPage.getTextFromMyRegistryHeader();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> Verify Registry Banner Content Slot is displayed in My Registry screen");
			Log.assertThat(exceptedFindRegistry.equals(Actual3),
					"<b>Actual Result 4:</b> The Registry banner in a global content slot is Displayed!",
					"<b>Actual Result 4:</b> The Registry banner in a global content slot is not Displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_045

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify tabs in the My Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_046(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			// Navigate to GiftRegistry Page
			registrySignedUserPage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Gift Registry Page");
			
			// Clicked On View Link Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked On View Link Button!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The following tabs should be displayed in the Registry page,'My Registry','Event Info','Shipping Info','Purchase'!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageListElements(Arrays.asList("lstOfTab"),
							registrySignedUserPage),
							"<b>Actual Result :</b> Tabs are displayed in the Registry page,'My Registry','Event Info','Shipping Info','Purchase'!",
							"<b>Actual Result :</b> Tabs are not displayed in the Registry page,'My Registry','Event Info','Shipping Info','Purchase'!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_046
	
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user allow edit the event date, name,type,location from My register scree", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_051(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Click On View Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("4. Clicked On View Button!");

			// Click On Event Info Tab
			registrySignedUserPage.clickOnEventInfo();
			Log.message("5. Clicked On Event Info Tab!");

			// Filling Registry Details
			LinkedHashMap<String, String> shippingDetails = registrySignedUserPage
					.fillingRegistryDetailsWithOneRegistrant("weddingRegistry");
			List<String> indexes = new ArrayList<String>(shippingDetails.keySet());
			String eventType = indexes.get(0).replace("select_eventType_", "");
			String eventdate = indexes.get(2).replace("pickdate_eventDate_", "");
			String city = indexes.get(3).replace("type_city_", "");
			String state = indexes.get(4).replace("select_state_", "");
			Log.message("6. Filling Registry Details!");

			// Click On Continue Button
			registrySignedUserPage.clickOnContinueButton();
			Log.message("7. Clicked On Continue Button!");

			// Click On Event Info Tab
			registrySignedUserPage.clickOnEventInfo();
			Log.message("8. Clicked On Event Info Tab!");

			LinkedHashMap<String, String> shippingDetailsUpdated = registrySignedUserPage
					.fillingRegistryDetailsWithOneRegistrant("DormRegistry");
			List<String> indexesUpdated = new ArrayList<String>(shippingDetailsUpdated.keySet());
			String eventTypeUpdated = indexesUpdated.get(0).replace("select_eventType_", "");
			String eventdateUpdated = indexesUpdated.get(2).replace("pickdate_eventDate_", "");
			String cityUpdated = indexesUpdated.get(3).replace("type_city_", "");
			String stateUpdated = indexesUpdated.get(4).replace("select_state_", "");
			Log.message("9. Filling Registry Details Updated!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should be allow to edit the event Type!");
			Log.assertThat(!(eventType.equals(eventTypeUpdated)),
					"<b>Actual Result 1:</b> User is able to edit event type!",
					"<b>Actual Result 1:</b> User is not able to edit event type!", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> User should be allow to edit the event Location!");
			Log.assertThat(!(cityUpdated.equals(city)), "<b>Actual Result 2:</b> User is able to edit event Location!",
					"<b>Actual Result 2:</b> User is not able to edit event Location!", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> User should be allow to edit the event Location!");
			Log.assertThat(!(stateUpdated.equals(state)),
					"<b>Actual Result 3:</b> User is able to edit event Location!",
					"<b>Actual Result 3:</b> User is not able to edit event Location!", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> User should be allow to edit the event date!");
			Log.assertThat(!(eventdateUpdated.equals(eventdate)),
					"<b>Actual Result 4:</b> User is able to edit event date!",
					"<b>Actual Result 4:</b> User is not able to edit event date!", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_051
	
	@Test(groups = { "desktop","mobile" }, description = "Verify Registry name and event date is displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_054(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Entered Email id '" + emailid + "' and Password '" + password + "'");

			registrySignedUserPage.headers.navigateToGiftRegistry();
			Log.message("4. Navigated to Gift Registry Page");
			
			// Clicked On View Link Button
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked On View Link Button!");
			
			registrySignedUserPage.clickOnAddItemToRegistry();
			Log.message("6. Clicked On Add Item To Registry Button!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("7. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProductByIndex(2);
			Log.message("8. Navigated to PDP Page !");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("9. selected the '" + size + "' size !");

			// Navigate to My Registry Page
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");

			//Click On View Link
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("12. Clicked On View Link Button!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Auto generated Registry Id code should be displayed in My Register screen!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryId"),
							registrySignedUserPage),
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated Registry Id is displayed!",
							"<b>Actual Result 1:</b> Navigated to 'My Registry screen' and Auto generated Registry Id is not displayed!",
							driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Auto generated bar code should be displayed in My Register screen!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("registryBarCode"),
							registrySignedUserPage),
							"<b>Actual Result 2:</b> Auto generated bar code is displayed!!",
							"<b>Actual Result 2:</b> Auto generated bar code is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_054
	@Test(groups = { "desktop","mobile" }, description = "Verify My Account Links Content Asset is displayed.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_072(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");


			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Registry SignIn page");

			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			Log.message("3. Navigated to create Account page");

			createAccountPage.CreateAccount(createAccountPage) ;
			Log.message("4. Created a new Account with valid credantials");	

			GiftRegistryPage giftregistry = homePage.headers.navigateToGiftRegistry();
			Log.message("5. Navigated to Registry Page");
			// Click on Create New Registry button
			RegistrySignedUserPage registrySignIn= giftregistry.clickOnCreateRegistrybutton();
			Log.message("6. Clicked on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingEventDetails(registryInfo);

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("7. Filled the valid Registry details and Clicked on Continue button in Create Registry page");

			// Update the PreEvent Address and Fill Post Event address in Step 2
			registrySignIn.fillingPostEventShippingdetails(shippingInfo);
			registrySignIn.fillingPreEventShippingdetails(shippingInfo);
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("8. Filled the valid Event details and Clicked on Continue button in Event Registry page");

			registrySignIn.clickOnSubmitButton();	
			Log.message("9. Clicked on submit button in the Registry Page");

			registrySignIn.headers.navigateToGiftRegistry();
			Log.message("10. Navigated to Registry Page after creating a valid registry account");
			// click on viewRegistry

			registrySignIn.clickOnViewRegistrylnk();
			Log.message("11. Clicked on View link in active Registry");

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Verify My Account Links Content Asset is displayed.");

			if(Utils.getRunPlatForm()=="desktop"){
				Log.assertThat(registrySignIn.elementLayer.verifyPageElements(Arrays.asList("lnkMyAccount","lnkProfile","lnkOrderHistory","lnkAddressBook","lnkPaymentMethods","lnkRegistry","lnkFAQ","lnkWishList","lnkBelkRewardsCreditCard","lnkEmailPreferences"), registrySignIn), 
						"<b>Actual Result:</b> My Account Links Content Asset displayed on the MyRegistry Page ",
						"<b>Actual Result:</b> My Account Links Content Asset not displayed on the MyRegistry Page ",
						driver);
			}

			if(Utils.getRunPlatForm()=="mobile"){
				registrySignIn.clickMobileHamburgerMenu();
				registrySignIn.clickMyAccountLink();
				Log.assertThat(registrySignIn.elementLayer.verifyPageElements(Arrays.asList("lnkProfileMobile","lnkOrderHistoryMobile","lnkAddressBookMobile","lnkPaymentMethodsMobile","lnkRegistryMobile","lnkWishListMobile"), registrySignIn), 
						"<b>Actual Result:</b> My Account Links Content Asset displayed on the MyRegistry Page ",
						"<b>Actual Result:</b> My Account Links Content Asset not displayed on the MyRegistry Page ",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_072
	@Test(groups = { "desktop",
	"mobile" }, description = " Verify sytem allows to share Registry link via social sharing.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_058(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		String searchkey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			// Search product with Id
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchkey);
			Log.message(i++ + ". Searched with keyword '" + searchkey + "' and navigated to PDP Page");
			// Selecting 'colour'
			String color = pdpPage.selectColor();
			Log.message(i++ + ". selected color" + color);

			// Selecting 'size'
			String size = pdpPage.selectSize();
			Log.message(i++ + ". selected color" + size);

			// Again,Clicked on 'Registry'
			pdpPage.clickOnRegistrySign();
			Log.message(i++ + ". Clicked 'Add to Registry' button in 'PDP' page");

			GiftRegistryPage giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ + ". Navigate to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ + ". Clicked 'View link' in the Gift registry page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> Social sharing lnks should be displayed in My register screen!");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("facebookTab", "twitterTab",
							"googleplusTab", "pininterestTab", "emailTab", "linkTab"), giftRegistryPage),
							"<b>Actual Result 1: </b> Social sharing lnks are displayed in My register screen!",
							"<b>Actual Result 1: </b> Social sharing lnks are not displayed in My register screen!", driver);

			Log.message("<br>");
			giftRegistryPage.navigateToPinterestPage();
			Log.message(i++ + ". Navigated to 'Pinterest page!");

			Utils.switchToNewWindow(driver);
			String link = driver.getCurrentUrl();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2: </b> On click/tap, the link should open the social network in a new tab with the shareable Registry Link.");

			Log.message("<b>Actual Result 2: </b> After click/tap on the social link,navigated social URL is:" + link,
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_058
	@Test(groups = {
	"desktop" }, description = "Verify system access the native functionality to print.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_066(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("Verify for desktop only");
		}
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;


			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ +". Navigated to Registry page", driver);

			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ +". Clicked view link button in the gift registry page");

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Verify system access the native functionality to print.");
			Log.assertThat(
					giftRegistryPage.elementLayer.verifyPageElements(Arrays.asList("iconPrint"), giftRegistryPage),
					"<b>Actual Result:</b> 'Print icons' displayed on the 'Registry' page",
					"<b>Actual Result:</b> 'Print icons' not displayed on the 'Registry' page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_066
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify tabs at the top of the empty registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_067(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ +". Navigated to Registry page", driver);

			RegistryInformationPage registryInformationPage = giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ +". Clicked view link button in the gift registry page");;
			// click on viewRegistry





			Log.message("<br>");
			Log.message(
					"<b>Expected Result</b> Verify Registry tabs(My Registry, Event Info, Shipping Info, Purchases) should be displayed.");
			Log.message("<b>Actual Result:</b>");
			if (Utils.getRunPlatForm() == "desktop") {
				Log.softAssertThat(
						registryInformationPage.elementLayer.verifyPageElements(
								Arrays.asList("tabMyRegistry", "tabEventInfo", "tabShippingInfo", "tabPurchases"),
								registryInformationPage),
								"Registry tabs are displayed in the Registry page",
								"Registry tabs are not displayed in the Registry page", driver);
			}

			else if (Utils.getRunPlatForm() == "mobile") {

				Log.softAssertThat(
						registryInformationPage.elementLayer
						.verifyPageElements(
								Arrays.asList("tabMyRegistryMobile", "tabEventInfoForMobile",
										"tabShippingInfoForMobile", "tabPurchasesForMobile"),
										registryInformationPage),
										"Registry tabs are displayed in the Registry page",
										"Registry tabs are not displayed in the Registry page", driver);
			}

			Log.message("<br>");
			Log.message("<b>Expected Result</b> Verify User should be able to navigate through all the tabs.");

			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("txtRegistryId"),
							registryInformationPage),
							"Navigated to MyRegistry Tab in the Registry page and the Registry ID:"
									+ registryInformationPage.getTextOfRegistryId(),
									"Unable to Navigated to MyRegistry Tab in the Registry page", driver);

			registryInformationPage.clickOnEventInfoTab();

			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("headerEventInformation"),
							registryInformationPage),
							"Navigated to EventInfo Tab in the Registry page and the EventInformation form opens with heading:"
									+ registryInformationPage.getTextOfEventInfomationHeaderForm(),
									"Unable to Navigated to EventInfo Tab in the Registry page", driver);

			registryInformationPage.clickOnShippingInfoTab();

			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("headerPreEventShipping"),
							registryInformationPage),
							"Navigated to ShippingInfo Tab in the Registry page and the ShippingInformation form opens with heading:"
									+ registryInformationPage.getTextOfShippingHeaderForm(),
									"Unable to Navigated to ShippingInfo Tab in the Registry page", driver);

			registryInformationPage.clickOnPurchasesTab();

			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(
					registryInformationPage.elementLayer.verifyPageElements(Arrays.asList("msgPurchase"),
							registryInformationPage),
							"Navigated to Purchases Tab in the Registry page and the Purchase message displays as:"
									+ registryInformationPage.getTextOfPurchaseMsg(),
									"Unable to Navigated to Purchases Tab in the Registry page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_067

	@Test(groups = { "desktop","mobile" }, description = "Verify 'No Registry Items Message' displayed.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_068(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!");

			String[] details = createRegistryAccount(homePage, registryInfo, shippingInfo, i);
			i = Integer.parseInt(details[3]);
			Log.message(i++ + ". Successfully created 'Registry' user");

			GiftRegistryPage giftRegistryPage = new GiftRegistryPage(driver);

			giftRegistryPage = homePage.headers.navigateToGiftRegistry();
			Log.message(i++ +". Navigated to Registry page", driver);

			RegistryInformationPage registryInformationPage = giftRegistryPage.clickOnViewRegistrylnk();
			Log.message(i++ +". Clicked view link button in the gift registry page");;
			// click on viewRegistry

			Log.message("<br>");
			Log.message("<b>Expected Result</b> 'No Registry Items Message' should be displayed below the registry Id.");

			Log.assertThat(registryInformationPage.elementLayer
					.verifyPageElements(Arrays.asList("msgNoItemsInRegistry"), registryInformationPage),
					"<b>Actual Result:</b> 'Message' displayed on the MyRegistry Page below registry Id as: "+registryInformationPage.getTextOfMyRegistryMsg(),
					"<b>Actual Result:</b> 'Message' not displayed on the MyRegistry Page below registry Id",
					driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_068
	
	@Test(groups = {"desktop" }, description = "Verify Breadcrumb in Policies & Guidelines page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_363(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			CustomerServicePage customerServicePage = homePage.footers.navigateToCustomerService();
			Log.message("2. Navigated to 'Customer Service Link' from the footers");
			
			PolicyGuideLinePage policyGuideLinePage = customerServicePage.navigateToPolicyGuidelinePage();
			Log.message("3. Navigated to 'Policy & Guidelines Page!'");

	        String[] breadcrumbTextActual = policyGuideLinePage.getTextFromBreadCrumbInDesktop().trim().split("\\n");
	      
	        boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;
					Log.message("" + breadCrumbData[i] + "---" + breadcrumbTextActual[i]);
				} else
					flag = false;
			}

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify The Breadcrumb should be displayed as Home > Customer Service > Policies & Guidelines.");

			
			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on PolicyGuideLine page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on PolicyGuideLine page", driver);
		
			
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_363
	
	@Test(groups = {"desktop","mobile" }, description = "Verify clicking on Read more link under each content section in Policies & Guidelines page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_365(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

	

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			CustomerServicePage customerServicePage = homePage.footers.navigateToCustomerService();
			Log.message("2. Navigated to 'Customer Service Link' from the footers");
			
			PolicyGuideLinePage policyGuideLinePage = customerServicePage.navigateToPolicyGuidelinePage();
			Log.message("3. Navigated to 'Policy & Guidelines Page!'");

			policyGuideLinePage.clickOnReadMoreLink(0);
			Log.message("4. Navigate to Easy Returns");
	       
			
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b>' On Click, the customer should be redirected to the appropriate content page.");

			Log.assertThat(driver.getCurrentUrl().contains("easy-returns"),
					"<b>Actual Result 1a:</b> 'Navigated to appropriate content page from PolicyGuideLine page and the heading is displayed as '"+ policyGuideLinePage.getTextOfHeading() + "' .",
					"<b>Actual Result 1a:</b> 'Unable to Navigate to appropriate content page from PolicyGuideLine page", driver);
			
			Log.message("<br>");
			policyGuideLinePage.clickOnBackToResults();
			
			policyGuideLinePage.clickOnReadMoreLink(1);
			Log.message("5. Navigate to Privacy & Security");
			
			Log.message("<br>");
			
			Log.assertThat(driver.getCurrentUrl().contains("privacy-security"),
					"<b>Actual Result 1b:</b> 'Navigated to appropriate content page from PolicyGuideLine page and the heading is displayed as '"+ policyGuideLinePage.getTextOfHeading() + "' .",
					"<b>Actual Result 1b:</b> 'Unable to Navigate to appropriate content page from PolicyGuideLine page", driver);
			
			Log.message("<br>");
			policyGuideLinePage.clickOnBackToResults();
			
			policyGuideLinePage.clickOnReadMoreLink(2);
			Log.message("6. Navigate to Privacy Policy");

			Log.message("<br>");
			
			Log.assertThat(driver.getCurrentUrl().contains("pricing-policy"),
					"<b>Actual Result 1c:</b> 'Navigated to appropriate content page from PolicyGuideLine page and the heading is displayed as '"+ policyGuideLinePage.getTextOfHeading() + "' .",
					"<b>Actual Result 1c:</b> 'Unable to Navigate to appropriate content page from PolicyGuideLine page", driver);
			
			
			Log.message("<br>");
			policyGuideLinePage.clickOnBackToResults();
			
			policyGuideLinePage.clickOnReadMoreLink(3);
			Log.message("7. Navigate to Terms of Use");
			Log.message("<br>");
			
			Log.assertThat(driver.getCurrentUrl().contains("terms-of-use"),
					"<b>Actual Result 1d:</b> 'Navigated to appropriate content page from PolicyGuideLine page and the heading is displayed as '"+ policyGuideLinePage.getTextOfHeading() + "' .",
					"<b>Actual Result 1d:</b> 'Unable to Navigate to appropriate content page from PolicyGuideLine page", driver);
			
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_365
	
	@Test(groups = {
	"desktop" }, description = "Verify registry bread crumbs is displayed when My Registry - Registry With Products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_234(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String breadCrumbData[] = testData.get("EditData").trim().split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to 'Registry' login page
	        SignIn signinPage  = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to 'Registry Login' Page!");

			// Finding Someone else registry as a 'Guest' user
			signinPage.registrySignedUserPage.findRegistry("sprint7", "tc234", "Wedding Registry");
			Log.message("3. Entered credentials to 'Find Someone's else' registry as 'SignedIn' user");

String[] breadcrumbTextActual = signinPage.registrySignedUserPage.getTextFromBreadCrumb().trim()
					.split("\\n");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>' Verify registry bread crumbs is displayed when My Registry - Registry With Products");

			boolean flag = true;
			for (int i = 0; i < breadCrumbData.length; i++) {
				if (breadCrumbData[i].equals(breadcrumbTextActual[i])) {
					flag = true;

				} else
					flag = false;
			}

			Log.assertThat((breadCrumbData.length == breadcrumbTextActual.length) && flag,
					"<b>Actual Result:</b> 'Breadcrumbs displayed properly on Registry page",
					"<b>Actual Result:</b> 'Breadcrumbs not displayed properly on Registry page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_234
	
	
	public void TC_BELK_REGISTRY_292(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").replace("S_", "").split("\\|")[0];
		String couponCode = testData.get("SearchKey").replace("S_", "").split("\\|")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();

			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				
			}
			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			
			Log.message((i++) + ". Search " + searchKey + "  and  Navigated to 'PDP'  Page");

			// Navigating to product with productID
			
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");																		
			// click on viewRegistry
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("<br>");
			Log.message("<b>Expected Result</b> Coupon id and discounting coupon value should be properly displayed in My Bag screen");

			Log.assertThat(shoppingBagPage.elementLayer
					.verifyPageElements(Arrays.asList("txtAppliedCoupon","txtCouponDiscountValue"), shoppingBagPage),
					"<b>Actual Result:</b> Coupon id and discounting coupon value displayed in My Bag screen, " + shoppingBagPage.getAppliedCouponCode() + " : " + shoppingBagPage.getCouponDiscountValue(),
					"<b>Actual Result:</b> Coupon id and discounting coupon value not displayed in My Bag screen",
					driver);
			CheckoutPage checkOutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +". Navigated to Place Order Page!");

			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>  Order should be placesd successfully with discount coupon code applied");
			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && OrderconfirmationPage.elementLayer
					.verifyPageElements(Arrays.asList("txtTotalCouponSavings"), OrderconfirmationPage),
					"<b>Actual Result :</b> Order placed successfully with discount coupon code applied",
					"<b>Actual Result :</b> Order not placed successfully with discount coupon code applied", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_292

	@Test(groups = { "desktop","mobile" }, description = "Verify coupon id is displayed once coupon applied to the item", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_293(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").replace("S_", "").split("\\|")[0];
		String couponCode = testData.get("SearchKey").replace("S_", "").split("\\|")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();

			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				
			}
			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			
			Log.message((i++) + ". Search " + searchKey + "  and  Navigated to 'PDP'  Page");

			// Navigating to product with productID
			
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");																		
			// click on viewRegistry
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("<br>");
			Log.message("<b>Expected Result</b> Coupon id should be properly displayed in My Bag screen");

			Log.assertThat(shoppingBagPage.elementLayer
					.verifyPageElements(Arrays.asList("txtAppliedCoupon"), shoppingBagPage),
					"<b>Actual Result:</b> Coupon id displayed in My Bag screen, " + shoppingBagPage.getAppliedCouponCode(),
					"<b>Actual Result:</b> Coupon id not displayed in My Bag screen",
					driver);
			CheckoutPage checkOutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +". Navigated to Place Order Page!");

			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>  Order should be placesd successfully with discount coupon code applied");
			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && OrderconfirmationPage.elementLayer
					.verifyPageElements(Arrays.asList("txtTotalCouponSavings"), OrderconfirmationPage),
					"<b>Actual Result :</b> Order placed successfully with discount coupon code applied",
					"<b>Actual Result :</b> Order not placed successfully with discount coupon code applied", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_293

	@Test(groups = { "desktop","mobile" }, description = "Verify N/A is displayed for non qualified coupon code", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_294(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").replace("S_", "").split("\\|")[0];
		String couponCode = testData.get("SearchKey").replace("S_", "").split("\\|")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();

			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				
			}
			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			
			Log.message((i++) + ". Search " + searchKey + "  and  Navigated to 'PDP'  Page");

			// Navigating to product with productID
			
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");																		
			// click on viewRegistry
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("<br>");
			Log.message("<b>Expected Result</b> For non qualified coupon code N/A should be displayed");

			Log.assertThat(shoppingBagPage.getTextCouponApplied().equals("N/A"),
					"<b>Actual Result:</b> For non qualified coupon code N/A displayed, " + shoppingBagPage.getTextCouponApplied(),
					"<b>Actual Result:</b> For non qualified coupon code N/A is not displayed",
					driver);
			CheckoutPage checkOutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +". Navigated to Place Order Page!");

			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>  Order should be placesd successfully and the coupon should not be displayed");
			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && 
					OrderconfirmationPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtTotalCouponSavings"), OrderconfirmationPage),
					"<b>Actual Result :</b> Order placed successfully and the coupon is not be displayed",
					"<b>Actual Result :</b> Order not placed successfully for non qualified coupon", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_294
	
	@Test(groups = { "desktop","mobile" }, description = "Verify by qualifying existing coupon code.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_REGISTRY_295(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").replace("S_", "").split("\\|")[0];
		String couponCode = testData.get("SearchKey").replace("S_", "").split("\\|")[1];
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			
			Log.message((i++) + ". Search " + searchKey + "  and  Navigated to 'PDP'  Page");

			// Navigating to product with productID
			
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");																		
			// click on viewRegistry
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("<br>");
			Log.message("<b>Expected Result</b> When line item added to the cart with the qualified coupon got exists, the coupon section and the cart line items should be updated.");

			Log.assertThat(shoppingBagPage.elementLayer
					.verifyPageElements(Arrays.asList("txtAppliedCoupon","txtCouponDiscountValue","lblOrderCouponSavings"), shoppingBagPage),
					"<b>Actual Result:</b>  The coupon section and the cart line items updated.",
					"<b>Actual Result:</b>  The coupon section and the cart line items not updated.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_REGISTRY_295

}// REGISTRY
