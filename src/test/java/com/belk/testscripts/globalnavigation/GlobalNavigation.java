package com.belk.testscripts.globalnavigation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CategoryLandingPage;
import com.belk.pages.CouponsPage;
import com.belk.pages.FindInStorePage;
import com.belk.pages.GiftCardsPage;
import com.belk.pages.HomePage;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.AboutBelkPage;
import com.belk.pages.footers.ApoFpoPage;
import com.belk.pages.footers.CustomerServicePage;
import com.belk.pages.footers.EmailSignupPage;
import com.belk.pages.footers.FaqPage;
import com.belk.pages.footers.GoogleStorePage;
import com.belk.pages.footers.ListOfBrandPage;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.footers.SiteMapPage;
import com.belk.pages.footers.StayConnectedPage;
import com.belk.pages.footers.StoreLocationPage;
import com.belk.pages.footers.TermsAndConditionsPage;
import com.belk.pages.footers.UnsubscribeEmailpage;
import com.belk.pages.footers.ViewMyBelkRewardPage;
import com.belk.pages.footers.WhyIsRequiredPage;
import com.belk.pages.headers.BelkRewardCardPage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class GlobalNavigation {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "GlobalNavigation";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section of the site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> headers = null;
		if (runPltfrm == "desktop") {
			headers = Arrays.asList("headerPanel", "headerTop",
					"headerTopMain", "headerNav",
					"headerBannerBottomPromotion", "headerBannerSiteWideCopy");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerPanel", "headerTopMain",
					"headerNav", "headerBannerBottomPromotionMobile",
					"headerBannerSiteWideCopy");
		}

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"2. Headers section displayed with header promotion, header top, header top main, header nav, header banner bottom promotion and header banner ",
					"Headers not displayed as expected, plz check the event log",
					driver);
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("3. Navigated to " + l1category + " >>" + l2category
					+ " >>" + l3category);

			Log.message("<b>Expected Result:</b> Global Header section should be displayed in Home Page and PLP");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"<b>Actual Result:</b> Header section is displayed on Belk Home page and PLP",
					"<b>Actual Result:</b> Headers not displayed as expected, plz check the event log",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_001

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section for Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// https://development-web-belk.demandware.net/on/demandware.store/Sites-Belk-Site/default/COShipping-Start
		String checkoutURL = webSite.replace("/s/Belk", "")
				+ "/on/demandware.store/Sites-Belk-Site/default/COShipping-Start";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> headers = null;
		if (runPltfrm == "desktop") {
			headers = Arrays
					.asList("headerPanel", "headerTop", "headerTopMain");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerPanel", "headerTopMain");
		}
		List<String> Checkoutheaders = Arrays.asList("headerPanel");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"2. Headers displayed on Belk page other than checkout page",
					"Headers not displayed as expected, plz check the event log",
					driver);
			Log.event("checkoutURL:: " + checkoutURL);

			PdpPage pdppage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("3. Search with keyword '" + searchKey
					+ "' and navigated to PDP!");

			pdppage.addProductToBag();
			Log.message("5. Add a product to bag in PDP!");

			BrowserActions.nap(3);
			driver.get(checkoutURL);
			Utils.waitForPageLoad(driver);
			Log.message("6. Navigating to checkout page, by loading the URL - "
					+ checkoutURL);

			Log.message("<b>Expected Result:</b> Global Header section should Not be displayed in Checkout Page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Checkoutheaders, homePage.headers),
					"<b>Actual Result:</b> Global Header section is not displayed on Checkout page",
					"<b>Actual Result:</b> Global Header section is displayed, plz check the event log and screenshot",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_002

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Header Banner - Content slot", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> headersLeftContent = null;
		List<String> headersRightContent = null;
		if (runPltfrm == "desktop") {
			headersLeftContent = Arrays.asList("headerLeftContentAsset",
					"lnkGiftCards", "lnkCoupons");
			headersRightContent = Arrays.asList("headerRightContentAsset",
					"lnkCoupons", "lnkWishList", "lnkRegistry");
		} else if (runPltfrm == "mobile") {
			headersLeftContent = Arrays.asList("headerTopMain",
					"lnkGiftCardsMobileHambrg", "lnkCouponsMobileHambrg",
					"lnkWishListMobileHambrg", "lnkRegistryMobileHambrg");

			/*
			 * headersRightContent = Arrays.asList("lnkCouponsMobileHambrg",
			 * "lnkWishListMobileHambrg", "lnkRegistryMobileHambrg",
			 * "lnkLoginLogoutMobileHambrg");
			 */
		}

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			if ((runPltfrm == "mobile")) {
				homePage.headers.clickMobileHamburgerMenu();

				Log.message("<b>Expected Result:</b> Gift Cards , Coupons, Wish List, Registry & Signin links should be present in Left side of Global Header section");
				Log.assertThat(
						homePage.elementLayer.verifyPageElements(
								headersLeftContent, homePage.headers),
						"<b>Actual Result:</b> Gift Cards, Coupons, Wish List, Registry & Sign in links are displayed in the Mobile Header Menu",
						"<b>Actual Result:</b> Some of the link missing in Mobile header menu( Gift Cards, Belk Rewards Card, Coupons, Wish List, Registry & Sign in), plz check the event log",
						driver);
			} else {

				Log.message("<b>Expected Result:</b> Gift Cards & Belk Rewerd Card links should be present in Left side of Global Header section");
				Log.assertThat(
						homePage.elementLayer.verifyPageElements(
								headersLeftContent, homePage.headers),
						"<b>Actual Result:</b> Gift Cards & Belk Rewerd Card links are present in Left side of Global Header section",
						"<b>Actual Result:</b> Gift Cards & Belk Rewerd Card links are Not present in Left side of Global Header section, plz check the event log",
						driver);
				Log.message("<b>Expected Result:</b> Coupons, Wish List, Registry & Signin links should be present in Right side of Global Header section");
				Log.assertThat(
						homePage.elementLayer.verifyPageElements(
								headersRightContent, homePage.headers),
						"<b>Actual Result:</b> Coupons, Wish List, Registry & Signin links are present in Right side of Global Header section",
						"<b>Actual Result:</b> Coupons, Wish List, Registry & Signin links are Not present in Right side of Global Header section");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_003

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_004(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			GiftCardsPage giftCardsPage = homePage.headers
					.navigateToGiftCards();
			Log.message("2. Clicked 'Gift Cards' link on header");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Clicking on 'Gift Card' link should be navigated to Gift card page");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "gift")
							&& giftCardsPage != null,
					"1. Navigate to correct URL: " + driver.getCurrentUrl(),
					"1. Navigated to wrong URL: " + driver.getCurrentUrl(),
					driver);

			ArrayList<String> txtInBreadCrumb = giftCardsPage
					.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).contains("Gift Cards"),
						"2. The breadcrumb is displayed as expected - 'Home > Gift Cards'",
						"2. The breadcrumb is not displayed as expected - 'Home > Gift Cards' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_004

	@Test(groups = { "desktop", "mobile", "tablet", "desktop_bat", "mobile_bat" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey").split("\\|")[0];
		String productID = testData.get("SearchKey").split("\\|")[1];
		String l1_Category = testData.get("L1_category");
		List<String> headers = null;
		List<String> headers1 = null;
		List<String> headers2 = null;
		List<String> footers = null;
		List<String> footers1 = null;
		List<String> footers2 = null;
		List<String> footers3 = null;
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop") {
			headers = Arrays.asList("lnkGiftCards", "lnkCoupons",
					"headerTopPromotionMsg", "lnkWishList", "lnkRegistry",
					"belkLogo", "lnkFindInStore",
					"txtSearch", "iconMiniCart", "lnkViewBag", "lnkWomens",
					"lnkMens", "lnkJuniors", "lnkKidsAndBaby", "lnkBeauty",
					"lnkShoes", "lnkHandbagsAndAccessories", "lnkJewelry",
					"lnkBedAndBath", "lnkForTheHome", "lnkClearance",
					"headerBannerBottomPromotion", "headerBannerSiteWideCopy");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerTopPromotionMsgMobile", "belkLogo",
					"txtSearch", "iconMiniCart", "lnkViewBag",
					"headerBannerBottomPromotionMobile", "headerBannerSiteWideCopy");
			headers1 = Arrays.asList("lnkWomens", "lnkMens", "lnkJuniors",
					"lnkKidsAndBaby", "lnkBeauty", "lnkShoes",
					"lnkHandbagsAndAccessories", "lnkJewelry", "lnkBedAndBath",
					"lnkForTheHome", "lnkClearance");
			headers2 = Arrays.asList("lnkCouponsMobileHambrg", "lnkFindInStoreMobileHambrg",
					"lnkGiftCardsMobileHambrg", "lnkWishListMobileHambrg", "lnkRegistryMobileHambrg");
		}

		if (runPltfrm == "desktop") {
			footers = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation",
					"lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards", "lnkAboutBelk",
					"lnkViewOurAdsOriginal", "lnkListOfBrands",
					"lnkCareersAtBelkOriginal", "lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal", "lnkdownloadApp",
					"lnkIOs", "lnkAndriod", "lnkSocialMedia", "lnkStayConnect",
					"lnkfacebook", "lnkTwitter", "lnkPrintrest",
					"lnkInstagram", "signUP", "txtEmail", "btnJoin",
					"legalMessage", "phoneNo", "lnkTermsOriginal", "lnkPrivacyOriginal",
					"btnPageFeedBack", "btnSiteMap", "btnOrderByCall");
		} else if (runPltfrm == "mobile") {
			footers = Arrays.asList("lnkdownloadApp", "lnkIOs", "lnkAndriod",
					"lnkSocialMedia", "lnkStayConnect", "lnkfacebook",
					"lnkTwitter", "lnkPrintrest", "lnkInstagram", "signUP",
					"txtEmail", "btnJoin", "legalMessage", "phoneNo",
					"lnkTermsOriginal", "lnkPrivacyOriginal", "btnPageFeedBack", "btnSiteMap",
					"btnOrderByCall");
			footers1 = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation");
			footers2 = Arrays.asList("lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards");
			footers3 = Arrays.asList("lnkAboutBelk", "lnkViewOurAdsOriginal",
					"lnkListOfBrands", "lnkCareersAtBelkOriginal",
					"lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal");
		}

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<br><u>Verifying header and footer in Search Result Page</u>");
			SearchResultPage searchResultPage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to Search Result page");

			Log.message(
					"<br><b>Expected Result: </b>The headers in the Search Result Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				searchResultPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers1, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers2, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store");
				searchResultPage.headers.closeMobileHamburgerMenu();
			}

			Log.message(
					"<br><b>Expected Result: </b>The footers in the Search Result Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				searchResultPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers1, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				searchResultPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers2, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				searchResultPage.footers.clickAboutBelk();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers3, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in Category Landing Page</u>");
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();

			Log.message("<br>3. Navigated to Category Landing Page of '"
					+ l1_Category + "'");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the Category Landing Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				categoryLandingPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers1, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers2, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				searchResultPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the Category Landing Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				categoryLandingPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers1, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				categoryLandingPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers2, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				categoryLandingPage.footers.clickAboutBelk();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers3, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in PDP</u>");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(productID);
			Log.message("3. Search with keyword '" + productID
					+ "' and navigated to PDP!");

			Log.message("<br>4. Navigated to PDP of product with product ID: '"
					+ productID + "'");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the PDP is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(headers,
								pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				pdpPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers1, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers2, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				pdpPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the PDP is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				pdpPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers1, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				pdpPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers2, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				pdpPage.footers.clickAboutBelk();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers3, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in shopping bag page</u>");
			pdpPage.addProductToBag();
			Log.message("<br>5. Added a product to bag");
			ShoppingBagPage shoppingBagPage = pdpPage.navigateToBagPage();
			Log.message("6. Navigated to shopping bag page");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the shopping bag page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				shoppingBagPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers1, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers2, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				pdpPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the shopping bag page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				shoppingBagPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers1, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				shoppingBagPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers2, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				shoppingBagPage.footers.clickAboutBelk();

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers3, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_005

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_005a(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey").split("\\|")[0];
		String productID = testData.get("SearchKey").split("\\|")[1];
		String l1_Category = testData.get("L1_category");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> headers = null;
		List<String> headers1 = null;
		List<String> headers2 = null;
		List<String> footers = null;
		List<String> footers1 = null;
		List<String> footers2 = null;
		List<String> footers3 = null;
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop") {
			headers = Arrays.asList("lnkGiftCards", "lnkCoupons",
					"headerTopPromotionMsg", "lnkWishList", "lnkRegistry",
					"belkLogo", "lnkFindInStore",
					"txtSearch", "iconMiniCart", "lnkViewBag", "lnkWomens",
					"lnkMens", "lnkJuniors", "lnkKidsAndBaby", "lnkBeauty",
					"lnkShoes", "lnkHandbagsAndAccessories", "lnkJewelry",
					"lnkBedAndBath", "lnkForTheHome", "lnkClearance",
					"headerBannerBottomPromotion", "headerBannerSiteWideCopy");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerTopPromotionMsgMobile", "belkLogo",
					"txtSearch", "iconMiniCart", "lnkViewBag",
					"headerBannerBottomPromotionMobile", "headerBannerSiteWideCopy");
			headers1 = Arrays.asList("lnkWomens", "lnkMens", "lnkJuniors",
					"lnkKidsAndBaby", "lnkBeauty", "lnkShoes",
					"lnkHandbagsAndAccessories", "lnkJewelry", "lnkBedAndBath",
					"lnkForTheHome", "lnkClearance");
			headers2 = Arrays.asList("lnkCouponsMobileHambrg", "lnkFindInStoreMobileHambrg",
					"lnkGiftCardsMobileHambrg", "lnkWishListMobileHambrg", "lnkRegistryMobileHambrg");
		}

		if (runPltfrm == "desktop") {
			footers = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation",
					"lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards", "lnkAboutBelk",
					"lnkViewOurAdsOriginal", "lnkListOfBrands",
					"lnkCareersAtBelkOriginal", "lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal", "lnkdownloadApp",
					"lnkIOs", "lnkAndriod", "lnkSocialMedia", "lnkStayConnect",
					"lnkfacebook", "lnkTwitter", "lnkPrintrest",
					"lnkInstagram", "signUP", "txtEmail", "btnJoin",
					"legalMessage", "phoneNo", "lnkTermsOriginal", "lnkPrivacyOriginal",
					"btnPageFeedBack", "btnSiteMap", "btnOrderByCall");
		} else if (runPltfrm == "mobile") {
			footers = Arrays.asList("lnkdownloadApp", "lnkIOs", "lnkAndriod",
					"lnkSocialMedia", "lnkStayConnect", "lnkfacebook",
					"lnkTwitter", "lnkPrintrest", "lnkInstagram", "signUP",
					"txtEmail", "btnJoin", "legalMessage", "phoneNo",
					"lnkTermsOriginal", "lnkPrivacyOriginal", "btnPageFeedBack", "btnSiteMap",
					"btnOrderByCall");
			footers1 = Arrays.asList("lnkCustomerService",
					"lnkTrackYourOrderOriginal",
					"lnkShippingInFormationOriginal", "lnkPolicyGuideLines",
					"lnkFQAS", "lnkContactOriginal", "lnkStoreLocation");
			footers2 = Arrays.asList("lnkBlekRewardCard", "lnkApplyCreditCard",
					"lnkPayYourBill", "lnkCheckAvailableBalance",
					"lnkVisitBelkRewards");
			footers3 = Arrays.asList("lnkAboutBelk", "lnkViewOurAdsOriginal",
					"lnkListOfBrands", "lnkCareersAtBelkOriginal",
					"lnkVendorInformationOriginal",
					"lnkCorporateInformationOriginal");
		}
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			ShoppingBagPage shoppingBagPage = null;
			SearchResultPage searchResultPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				searchResultPage = shoppingBagPage.headers
						.searchProductKeyword(searchKey);
			} else {
				searchResultPage = myaccount.headers
						.searchProductKeyword(searchKey);
			}
			Log.message("<br><u>Verifying header and footer in Search Result Page</u>");
			Log.message("4. Searched with keyword '" + searchKey
					+ "' and navigated to Search Result page");

			Log.message(
					"<br><b>Expected Result: </b>The headers in the Search Result Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				searchResultPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers1, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								headers2, searchResultPage.headers),
						"<b>Actual Result: </b>The headers in the Search Result Page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the Search Result Page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				searchResultPage.headers.closeMobileHamburgerMenu();
			}

			Log.message(
					"<br><b>Expected Result: </b>The footers in the Search Result Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				searchResultPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers1, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				searchResultPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers2, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				searchResultPage.footers.clickAboutBelk();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers3, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers, searchResultPage.footers),
						"<b>Actual Result: </b>The footers in the Search Result Page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Search Result Page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in Category Landing Page</u>");
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();

			Log.message("<br>5. Navigated to Category Landing Page of '"
					+ l1_Category + "'");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the Category Landing Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				categoryLandingPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers1, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								headers2, categoryLandingPage.headers),
						"<b>Actual Result: </b>The headers in the Category Landing Page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the Category Landing Page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				searchResultPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the Category Landing Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				categoryLandingPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers1, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				categoryLandingPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers2, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				categoryLandingPage.footers.clickAboutBelk();

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers3, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								footers, categoryLandingPage.footers),
						"<b>Actual Result: </b>The footers in the Category Landing Page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the Category Landing Page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in PDP</u>");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(productID);
			Log.message("5. Search with keyword '" + productID
					+ "' and navigated to PDP!");

			Log.message("<br>6. Navigated to PDP of product with product ID: '"
					+ productID + "'");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the PDP is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(headers,
								pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				pdpPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers1, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								headers2, pdpPage.headers),
						"<b>Actual Result: </b>The headers in the PDP is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the PDP is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				pdpPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the PDP is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				pdpPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers1, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				pdpPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers2, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				pdpPage.footers.clickAboutBelk();

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers3, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						pdpPage.elementLayer.verifyPageElements(
								footers, pdpPage.footers),
						"<b>Actual Result: </b>The footers in the PDP is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the PDP is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.message("<br><u>Verifying header and footer in shopping bag page</u>");
			pdpPage.addProductToBag();
			Log.message("<br>7. Added a product to bag");
			shoppingBagPage = pdpPage.navigateToBagPage();
			Log.message("8. Navigated to shopping bag page");
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, 0)");
			Log.message(
					"<br><b>Expected Result: </b>The headers in the shopping bag page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
					driver);
			if (runPltfrm == "desktop") {
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links Gift Cards, Coupons, Promotion msg, Wish List, Registry, Sign In, Belk logo, Find In Store, Search text box, Mini cart icon, Shopping bag link, L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Promotion bottom, SiteWideCopy",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - Belk logo, Promotion msg, Search text box, Mini cart icon, Shopping bag link,  Promotion bottom, SiteWideCopy",
						driver);
				shoppingBagPage.headers.clickMobileHamburgerMenu();
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers1, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - L1Category[Women, Men, Junior, KidsAndBaby, Beauty, Shoes, HandbagsAndAccessories, Jewelry, BedAndBath, ForTheHome], Clearance, Sign In",
						driver);
				((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								headers2, shoppingBagPage.headers),
						"<b>Actual Result: </b>The headers in the shopping bag page is displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						"<b>Actual Result: </b>The headers in the shopping bag page is not displayed with links - Wish List, Gift Cards, Coupons, Registry,  Find In Store",
						driver);
				pdpPage.headers.closeMobileHamburgerMenu();
			}
			Log.message(
					"<br><b>Expected Result: </b>The footers in the shopping bag page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
					driver);
			if (runPltfrm == "desktop") {
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links Customer Service, TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location, Belk Reward Card, Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards, About Belk, View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information, download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, YouTube, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			} else if (runPltfrm == "mobile") {
				shoppingBagPage.footers.clickCustomerServiceLink();

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers1, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under Customer Service - TrackYourOrder, Shipping InFormation, PolicyG uideLines, FQAS, Contact Us, Store Location",
						driver);
				shoppingBagPage.footers.clickBelkRewardCards();

				Log.softAssertThat(
						searchResultPage.elementLayer.verifyPageElements(
								footers2, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under Belk Reward Cards - Apply Credit Card, Pay Your Bill, Check Available Balance, Visit Belk Rewards",
						driver);

				shoppingBagPage.footers.clickAboutBelk();

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers3, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links under About Belk - View Our Ads, List Of Brands, Careers At Belk, Vendor Information, Corporate Information",
						driver);
				((JavascriptExecutor) driver)
						.executeScript("window.scrollTo(0, document.body.scrollHeight)");

				Log.softAssertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								footers, shoppingBagPage.footers),
						"<b>Actual Result: </b>The footers in the shopping bag page is displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						"<b>Actual Result: </b>The footers in the shopping bag page is not displayed with links - download App, IOS, Andriod, Social Media, Stay Connect, facebook, Twitter, Printrest, Instagram, signUP, Email text box, Join button, legal Message, phone No, Terms, Privacy, Page FeedBack, Site Map, Order By Call",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_005a

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		CouponsPage couponPage = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Coupons' link should be navigated to Coupons page");
			couponPage = homePage.headers.navigateToCoupons();
			Log.message("2. Clicked on Coupons link");
			Log.message("<br><b>Actual Result:</b>");
			Log.softAssertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "coupons")
							&& couponPage != null,
					"1. Clicking on 'Coupons' link is navigated to Coupons page with correct url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Coupons' link is Not navigated to Coupons page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = couponPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals("Coupons"),
						"2. The breadcrumb is displayed as expected - 'Home > Coupons'",
						"2. The breadcrumb is not displayed as expected - 'Home > Coupons' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_006

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrom = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		SignIn signinPage = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Wish List' link should be navigated to Wish List Login page");
			signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message("<br><b>Actual Result:</b> ");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"1. Clicking on 'Wish List' link is navigated to Wish List Login page "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Wish List' link is Not navigated to Wish List Login page",
					driver);

			List<String> txtInBreadCrumb = signinPage
					.getTextInBreadcrumb();
			if(runPltfrom=="desktop") {
			Log.assertThat(
					txtInBreadCrumb.get(0).equals("My Account")
							&& txtInBreadCrumb.get(1).equals("Wish List"),
					"2. The breadcrumb is displayed as expected - 'My Account > Wish List'",
					"2. The breadcrumb is not displayed as expected - 'My Account > Wish List' and the actual is : '"
							+ txtInBreadCrumb.get(0)
							+ " > "
							+ txtInBreadCrumb.get(1) + "'");
			} else if(runPltfrom=="mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to My Account"),
						"2. The breadcrumb is displayed as expected - 'Back to My Account'",
						"2. The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_007

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_008(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Wish List' link as Member account should be navigated to Wish List page");
			WishListPage wishlist = myaccount.headers.navigateToWishList();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Wishlist-Show")
							&& wishlist != null,
					"1. Clicking on 'Wish List' link as Member account is navigated to Wish List page with expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Wish List' link as member account is Not navigated to Wish List page and the actual url is: "
							+ driver.getCurrentUrl(), driver);

			List<String> txtInBreadCrumb = signinPage
					.getTextInBreadcrumb();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("My Account")
								&& txtInBreadCrumb.get(1).equals("Wish List"),
						"2. The breadcrumb is displayed as expected - 'My Account > Wish List'",
						"2. The breadcrumb is not displayed as expected - 'My Account > Wish List' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to My Account"),
						"2. The breadcrumb is displayed as expected - 'Back to My Account'",
						"2. The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_008

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		SignIn signinPage = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Registry' link as Guest should be navigated to Registry Login page");
			signinPage = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"1. Clicking on 'Registry' link as Guset is navigated to login page with expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Registry' link as guest is Not navigated to login page and the actual url is : "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = signinPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals(
										"Registry Login"),
						"2. The breadcrumb is displayed as expected - 'Home > Registry Login'",
						"2. The breadcrumb is not displayed as expected - 'Home > Registry Login' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_009

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Registy' link as member account should be navigated to My Registry page");
			GiftRegistryPage registry = myaccount.headers
					.navigateToGiftRegistry();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "GiftRegistry")
							&& registry != null,
					"1. Clicking on 'Registy' link as Member account is navigated to My Registry page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Registy' link as Member account is Not navigated to My Registry page and the actual url is: "
							+ driver.getCurrentUrl(), driver);

			List<String> txtInBreadCrumb = registry.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals("Registry")
								&& txtInBreadCrumb.get(2).equals("Your Registries"),
						"2. The breadcrumb is displayed as expected - 'Home > Registry > Your Registries'",
						"2. The breadcrumb is not displayed as expected - 'Home > My Registry' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_010

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_011(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		SignIn signinPage = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Sign In' link should be navigated to My Account Login page");
			signinPage = homePage.headers.navigateToSignIn();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"<b>Actual Result:</b> Clicking on 'Sign In' link is navigated to My Account Login page with the expected url:  "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Clicking on 'Sign In' link is Not navigated to My Account Login page and the actula url is: "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = signinPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals(
										"Sign In/Register"),
						"2. The breadcrumb is displayed as expected - 'Home > Sign In/Register'",
						"2. The breadcrumb is not displayed as expected - 'Home > Sign In/Register' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_011

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_012(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			homePage = myaccount.headers.navigateToHome();
			Log.message("4. Clicked Belk's logo and navigated to Home page",
					driver);

			myaccount = homePage.headers.navigateToMyAccount();
			Log.message("5. Clicked on My Account link");

			Log.message("<b>Expected Result:</b> Clicking on 'My Account' link as Member should be navigated to My Account page");
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Account-Show")
							&& myaccount != null,
					"1. Clicking on 'My Account' link as Member is navigated to My Account page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'My Account' link as Member is Not navigated to My Account page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = myaccount.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals("My Account"),
						"2. The breadcrumb is displayed as expected - 'Home > My Account'",
						"2. The breadcrumb is not displayed as expected - 'Home > My Account' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_012

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_013(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);
			myaccount.headers.clickSignOut();
			Log.message("4. Clicked on SignOut link");
			Log.message("<b>Expected Result:</b> Clicking on 'Sign Out' link as Member should be navigated to My Account Login page");
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"1. Clicking on 'Sign out' link as Member is navigated to My Account Login page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Sign out' link as Member is Not navigated to My Account Login page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = signinPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals(
										"Sign In/Register"),
						"2. The breadcrumb is displayed as expected - 'Home > Sign In/Register'",
						"2. The breadcrumb is not displayed as expected - 'Home > Sign In/Register' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_013

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_014(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> lnkMyAccount = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);
			((JavascriptExecutor) driver).executeScript("window.open();");
			driver.close();
			ArrayList<String> newTab = new ArrayList<String>(
					driver.getWindowHandles());
			driver.switchTo().window(newTab.get(0));
			Log.message(
					"4. Closed the browser and opening a new window on same browser",
					driver);
			driver.get(webSite);
			Utils.waitForPageLoad(driver);

			Log.message("<b>Expected Result:</b> Previously Logged in User section should be retained");
			if (runPltfrm == "desktop") {
				lnkMyAccount = Arrays.asList("lnkMyAccount");
				Log.assertThat(
						homePage.elementLayer.verifyPageElements(lnkMyAccount,
								homePage.headers),
						"<b>Actual Result:</b> Previously Logged in User section is retained Successfully",
						"<b>Actual Result:</b> Previously Logged in User section is Not retained",
						driver);
			} else if (runPltfrm == "mobile") {
				lnkMyAccount = Arrays.asList("lnkMyAccountMobileHambrg");
				homePage.headers.clickMobileHamburgerMenu();
				Log.assertThat(
						homePage.elementLayer.verifyPageElements(lnkMyAccount,
								homePage.headers),
						"<b>Actual Result:</b> Previously Logged in User section is retained Successfully",
						"<b>Actual Result:</b> Previously Logged in User section is Not retained",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_014

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_015(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated to " + l1category + " >>" + l2category
					+ " >>" + l3category, driver);

			homePage = homePage.headers.navigateToHome();
			Log.message("3. Clicked on 'Belk's' logo");
			Log.message("<b>Expected Result:</b> Clicking on 'Belk's' logo should navigate to Home page");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "home")
							&& homePage != null,
					"<b>Actual Result:</b> Clicked on 'Belk's' logo and navigated to Home page with the expected url: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Clicked on 'Belk's' logo and not navigated to Home page and the actual url is : "
							+ driver.getCurrentUrl());

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_015

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_016(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> headers = null;
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerTopPromotionMsgMobile");
		} else if (runPltfrm == "desktop") {
			headers = Arrays.asList("headerTopPromotionMsg");
		}

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> Promotion message content should be displayed");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"<b>Actual Result:</b> 'FREE SHIPPING ON ORDERS OVER $99' Promotion message is displayed ",
					"<b>Actual Result:</b> 'FREE SHIPPING ON ORDERS OVER $99' Promotion message is Not displayed ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_016

	@Test(groups = { "desktop", "mobile", "tablet", "desktop_bat", "mobile_bat" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_017(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String level1 = testData.get("L1_category");

		List<String> errormsg = Arrays.asList("searchErrorMsg");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.assertThat(
					homePage.headers.verifyGlobalNavigationL1Color(),
					"2. Global navigation(L1) categories list is displayed with black color and 'Clearance' is displayed with Red color",
					"Global navigation(L1) categories  list is not displayed with black color and 'Clearance' is not displayed with Red color",
					driver);

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.closeMobileHamburgerMenu();
			}
			homePage.headers.navigateTo(level1);
			Log.message("<b>Expected Result:</b> Clicking on 'Clearance' Category link  navigates to Clearance page and Products should be listed");
			Log.assertThat(homePage.elementLayer.verifyPageElementsDoNotExist(
					errormsg, homePage.headers), "<b>Actual Result:</b> "
					+ level1 + " have products listed in the landing page",
					"<b>Actual Result:</b>" + level1
							+ "' doesn't have any Products listed", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_017

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_018(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> headers = null;
		if (runPltfrm == "desktop") {
			headers = Arrays.asList("headerBannerBottomPromotion");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerBannerBottomPromotionMobile");
		}
		List<String> headers1 = Arrays.asList("headerBannerSiteWideCopy");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			Log.message("<b>Expected Result:</b> Header Banner 'Bottom Promotion' should be displayed in Home page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"<b>Actual Result:</b> Header Banner 'Bottom Promotion' is displayed in Home page ",
					"<b>Actual Result:</b> Header Banner 'Bottom Promotion' is Not displayed in Home page",
					driver);
			Log.message("<b>Expected Result:</b> Header Banner 'Site Wide Copy' should be displayed in Home page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers1,
							homePage.headers),
					"<b>Actual Result:</b> Header Banner 'Site Wide Copy' is displayed in Home page ",
					"<b>Actual Result:</b> Header Banner 'Site Wide Copy' is Not displayed in Home page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_018

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String zipcode = testData.get("L1_category");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			FindInStorePage findInStorePage = homePage.headers.navigateToFindInStore();
			Log.message("2. Successfully navigated to Find In Store page",
					driver);
			findInStorePage.typeZipCode(zipcode);
			List<String> txtInBreadCrumb = findInStorePage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if(runPltfrm=="desktop")
			Log.assertThat(txtInBreadCrumb.get(0).equals("Home") && txtInBreadCrumb.get(1).equals("Store Locator"), "Store Location behaviour is working as expected", "Store Location behaviour is not working as expected");
			else
				Log.assertThat(txtInBreadCrumb.get(0).equals("Back to Home"), "Store Location behaviour is working as expected", "Store Location behaviour is not working as expected");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_GLOBALNAV_019

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Search Box behavior - Search Box Verification & Search Icon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_020(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> headerSearch = null;
		String searchTextBox = null;

//		if (runPltfrm == "desktop") {
			searchTextBox = HomePage.SEARCH_KEYWORD_STRING;
//		} else if (runPltfrm == "mobile") {
//			searchTextBox = HomePage.SEARCH_KEYWORD_STRING_MOBILE;
//		}
		headerSearch = Arrays.asList("fldSearchBox");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.assertThat(homePage.elementLayer.verifyPageElements(
					headerSearch, homePage.headers),
					"2. Search Box is under Find Store Link",
					"Search Box is not under Find Store Link");

			Log.assertThat(
					homePage.getTextFromSearchTextBox().contains(searchTextBox),
					"3.'" + searchTextBox + "' is display in the search box",
					searchTextBox + " is not display in the search box", driver);

			homePage.clickSearchTextBox();
			Log.softAssertThat(
					!homePage.getSearchTextBoxClassAttribute()
							.contains("focus"),
					"4. Search instruction message present until first character typed",
					"Search instruction message is not displayed before entering first character",
					driver);

			homePage.enterSearchTextBox("te");
			Log.message("<b>Expected Result:</b> Search Instruction message should be cleared after entering a charecter in Search field");
			Log.softAssertThat(
					homePage.getSearchTextBoxClassAttribute().contains("focus"),
					"<b>Actual Result:</b> Search Instruction message is cleared after entering a charecter in Search field ",
					"<b>Actual Result:</b> Search Instruction message is Not cleared after entering a charecter in Search field",
					driver);

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword: " + searchKey);
			Log.message("<b>Expected Result:</b> Search keyword should be cleared from the Search field after the Search results been displayed");
			Log.assertThat(
					searchResultPage.getTextFromSearchTextBox().contains(
							searchTextBox),
					"<b>Actual Result:</b> Search keyword is cleared from the Search field after the Search results been displayed ",
					"<b>Actual Result:</b> Search keyword is Not cleared from the Search field after the Search results been displayed",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_20

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify - Search result page landing - Enter Key", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_021(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		String searchTextBox = "Your search results for \"" + searchKey + "\"";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			Log.message("<b>Expected Result:</b> Products related to "
					+ searchTextBox
					+ " should be displayed in search result page ");
			Log.assertThat(
					searchResultPage.getTextFromSearchResult().contains(
							searchTextBox),
					"<b>Actual Result:</b> Products related to "
							+ searchTextBox
							+ " is displayed in search result page ",
					"<b>Actual Result:</b> Products related to "
							+ searchTextBox
							+ " is Not displayed in search result page ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_21

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Search Box behavior - Auto Suggestion - Valid and Invalid Input", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_022(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> homePageElement = Arrays.asList("autoSuggestPopup");

		String searchKey = testData.get("SearchKey");
		String invalidSearchKey = testData.get("InvalidSearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> Auto Suggestion pop-up should not be displayed for Invalid Charater Search key");
			homePage.enterSearchTextBox(invalidSearchKey);
			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							homePageElement, homePage.get()),
					"<b>Actual Result:</b> Auto Suggestion pop-up is not displayed for Invalid Charater Search key : "
							+ invalidSearchKey,
					"<b>Actual Result:</b> Auto Suggestion pop-up is displayed for Invalid Charater Search key : "
							+ invalidSearchKey, driver);

			SearchResultPage searchResultPage = homePage
					.searchProductWithAutoSuggestion(searchKey);
			Log.message("3. searched with valid keyword: " + searchKey);
			Log.message("<b>Expected Result:</b> Clicking on a link in Auto suggestion list should navigate to Search results");

			Log.assertThat(
					searchResultPage != null,
					"<b>Actual Result:</b> Clicking on a link in Auto suggestion list is navigate to Search results",
					"<b>Actual Result:</b> Clicking on a link in Auto suggestion list is not navigated to Search results",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_22

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Search Box behavior - Auto Suggestion - Valid Input to PDP Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_023(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String selectedProduct = homePage
					.searchProductWithAutoSuggestionsToPDP(searchKey);
			PdpPage pdpPage = new PdpPage(driver).get();
			Log.message("2. Typed keyword : " + searchKey
					+ " and clicked the link '" + selectedProduct
					+ "' from auto suggestion popup");
			Log.message("<b>Expected Result:</b> Clicking on Auto suggestion link of a product should navigate to PDP");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], selectedProduct
									.toLowerCase().replaceAll("& ", "").replaceAll(" ", "-")
									.replaceAll("®", "reg"))
							&& pdpPage != null,
					"<b>Actual Result:</b> Clicking on Auto suggestion link of a product '"
							+ selectedProduct
							+ "' is navigated to PDP with the expected url: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Clicking on Auto suggestion link of a product is not navigated to PDP and the actual url is: "
							+ driver.getCurrentUrl(), driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_23

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Cart with 0 item", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_024(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String itemcount = "0";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> homePageElement = Arrays.asList("bagOverlay");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.message("<b>Expected Result:</b> Items count should be displayed in Bag");
			Log.assertThat(
					homePage.getItemCountFromMiniCart().contains(itemcount),
					"<b>Actual Result:</b>" + itemcount
							+ " Items is displayed in the bag",
					"<b>Actual Result:</b>" + itemcount
							+ " Items count is Not displayed in the bag",
					driver);
			Log.message("<b>Expected Result:</b> Bag over should not be displayed");
			Log.assertThat(homePage.elementLayer.verifyPageElementsDoNotExist(
					homePageElement, homePage),
					"<b>Actual Result:</b>  Bag over is not be displayed",
					"<b>Actual Result:</b>  Bag over is displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_024

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Cart with 1 item", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_025(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String shippingMessage = "Added to Shopping Bag";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey
					+ "' and navigated to search result page");

			PdpPage pdppage = searchresultpage.navigateToPDP();
			Log.message("3. Select product from search result page!");
			pdppage.selectColor();
			pdppage.selectSize();
			pdppage.clickAddToBagButton();
			Log.message("4. Add product to bag from pdpPage!");

			// if (Utils.getRunPlatForm() != "mobile")
			// pdppage.mouseOverMiniCart();

			Log.message("<br><b>Expected Result:</b> "
					+ shippingMessage
					+ " Message should be displayed in Mini cart after adding the product to the bag");
			Log.assertThat(
					pdppage.minicart.getSuccessMsgFromMiniCard().contains(
							shippingMessage),
					"<b>Actual Result:</b> "
							+ shippingMessage
							+ " Message is displayed in Mini cart after adding the product to the bag ",
					"<b>Actual Result:</b> "
							+ shippingMessage
							+ " Message is Not displayed in Mini cart after adding the product to the bag",
					driver);

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("<br>5. Mini Cart icon is clicked");
			Log.message("<br><b>Expected Result:</b> Clicking on Mini cart icon should navigate to the bag page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Cart-Show")
							&& shoppingBagPage != null,
					"Clicking on Mini cart icon is navigated to the bag page with the expected url: "
							+ driver.getCurrentUrl(),
					"Clicking on Mini cart icon is not navigated to the bag page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_025

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Cart Icon - Mini-Cart - PDP Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_026(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to '" + searchKey
					+ "' search result Page!");

			PdpPage pdppage = searchresultpage.navigateToPDP();
			Log.message("3. Select random product from search result page!");

			String breadCrumb = pdppage.getProductNameFromBreadCrumb();
			Log.message("4. Navigated to PDP page and product name '"
					+ breadCrumb + "' is getting displayed in breadcrumbs");

			pdppage.addProductToBag();
			Log.message("5. Add product to bag from pdpPage!");

			homePage = pdppage.headers.navigateToHome();
			Log.message("6. Navigate to Home Page from pdppage!");

			pdppage = homePage.minicart.clickProductNameOnMiniCart();
			Log.message("7. Navigate to PDP Page after clicking the productname in the bag overly!");

			Log.message("<b>Expected Result:</b> Correct Product name should be displayed in the Breadcrumb of PDP");
			Log.assertThat(
					pdppage.getProductNameFromBreadCrumb().contains(breadCrumb),
					"<b>Actual Result:</b>"
							+ breadCrumb
							+ " - Correct Product name is displayed in the Breadcrumb of PDP ",
					"<b>Actual Result:</b>"
							+ breadCrumb
							+ " - Correct Product name is Not displayed in the Breadcrumb of PDP ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_026

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Cart Icon - Mini-Cart - PDP Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_027(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultpage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			PdpPage pdppage = searchresultpage.navigateToPDP();
			Log.message("3. Select product from search result page!");

			pdppage.addProductToBag();
			Utils.waitForPageLoad(driver);
			Log.message("4. Add product to bag from pdpPage!");

			String item_count = pdppage.minicart.getCartProductCount();
			Log.message("5. " + item_count + " count is display in the cart ");

			((JavascriptExecutor) driver).executeScript("window.open();");
			driver.close();
			ArrayList<String> newTab = new ArrayList<String>(
					driver.getWindowHandles());
			driver.switchTo().window(newTab.get(0));
			Log.message(
					"6. Closed the browser and opening a new window on same browser",
					driver);
			driver.get(webSite);
			Utils.waitForPageLoad(driver);

			Log.message("<b>Expected Result:</b> Previously added Product should be retained in the Bag");
			Log.assertThat(
					homePage.getItemCountFromMiniCart().contains(item_count),
					"<b>Actual Result:</b>"
							+ item_count
							+ " Previously added Product is retained in the Bag ",
					"<b>Actual Result:</b>"
							+ item_count
							+ " Previously added Product is Not retained in the Bag ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_027

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_028(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("4. Navigated to " + searchKey + " search result Page!");

			PdpPage pdppage = searchresultpage.navigateToPDP();
			Log.message("5. Select product from search result page!");

			pdppage.addProductToBag();
			Log.message("6.Product added to bag from pdpPage!");

			String item_count_before = pdppage.minicart.getCartProductCount();
			Log.message("7. " + item_count_before
					+ " count is display in the cart ");

			Utils.waitForPageLoad(driver);

			homePage.headers.clickSignOut();
			Log.message("8.Clicked SignOut button on header");

			signinPage = pdppage.headers.navigateToSignIn();
			signinPage.signInToMyAccount(emailid, password);
			Log.message("9. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			String item_count_after = pdppage.minicart.getCartProductCount();
			Log.message("10. " + item_count_after
					+ " count is display in the cart ");

			Log.message("<b>Expected Result:</b> Member account - Previously added Product should be retained in the Bag");
			Log.assertThat(
					item_count_after.equals(item_count_before),
					"<b>Actual Result:</b>Member account - Previously added Product count is retained in the Bag ",
					"<b>Actual Result:</b>Member account - Previously added Product count is retained in the Bag ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_HEADERS_028

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_029(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String level1 = testData.get("L1_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			Log.message("<b>Expected Result:</b> Sub Category should be displayed in Blue color");
			Log.assertThat(
					homePage.headers.verifyL2CategoryColor(level1),
					"<b>Actual Result:</b>  Sub Category is displayed in Blue color for category -  "
							+ level1,
					"<b>Actual Result:</b>  Sub Category is Not displayed in Blue color for category -  "
							+ level1, driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_029

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_030(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");
		String productLandingURL = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.headers.navigateTo(l1category, l2category, l3category);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("<b>Expected Result:</b> PLP should be displayed");
			Log.message("<br><b>Actual Result:</b> ");
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], productLandingURL)
							&& plpPage != null,
					"1. Navigated to PLP of " + l1category + " >>" + l2category
							+ " >>" + l3category + " with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Not Navigated to PLP of " + l1category + " >>"
							+ l2category + " >>" + l3category
							+ " and the actual url is:"
							+ driver.getCurrentUrl(), driver);

			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1category)
										&& (breadcrumbList.get(2)
												.equals(l2category)) && (breadcrumbList
											.get(3).equals(l3category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1category + " > " + l2category + " > "
								+ l3category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1category + " > " + l2category + " > "
								+ l3category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_033

	// Mobile_Done
	@Test(groups = { "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_031(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop") {
			throw new SkipException(
					"L2 category content slot is not applicable for MOBILE");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> Content slot shoould be displayed on Mouse over on Category name");
			Log.assertThat(
					homePage.headers.verifyCategory1ContentSlot(l1category),
					"<b>Actual Result:</b> Content Slot is Present when hovered over "
							+ l1category + " category.",
					"<b>Actual Result:</b> Content Slot is Not Present when hovered over "
							+ l1category + " category ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_031

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_032(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> footer = Arrays.asList("lnkCustomerService",
				"lnkBlekRewardCard", "lnkAboutBelk", "lnkdownloadApp");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");
			Log.message("<b>Expected Result:</b> Footer pane should be available with the links");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(footer,
							homePage.footers),
					"<b>Actual Result:</b> Footer pane is available with the links",
					"<b>Actual Result:</b> Footer pane is Not available with the links",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_032

	@Test(groups = { "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_033(String browser) throws Exception {

		// String runPltfrm = Utils.getRunPlatForm();
		// if (runPltfrm != "mobile") {
		// throw new SkipException(
		// "Verify the footer's link(Customer Service, Belk Rewards Card & About
		// Belk) functionality as Collapse&Expand - it is only work on MOBILE");
		// }

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			homePage.footers.clickCustomerServiceLink();
			Log.message("3. Expanding customer servies link in the Home page ",
					driver);

			Log.assertThat(
					Utils.waitForElement(driver,
							homePage.footers.expandCustomerService),
					"4. Custermer services link has been expanded in the Home Page",
					"Customer services link is not expanded in the Home Page");

			homePage.footers.clickCustomerServiceLink();
			Log.message("5. closed customer servies link in the Home page ");

			Log.assertThat(Utils.waitForElement(driver,
					homePage.footers.closedCustomerService),
					"6. Custermer services link is  closed in the Home Page",
					"Customer services link is not closed in the Home Page");

			homePage.footers.clickBelkRewardCards();
			Log.message("7. Expanding Belk reward card link in the Home page ",
					driver);

			Log.assertThat(Utils.waitForElement(driver,
					homePage.footers.expandBelkRewardCard),
					"8. Belk reward card is  expanded in the Home Page",
					"Customer services link is not expanded in the Home Page");

			homePage.footers.clickBelkRewardCards();
			Log.message("9. closed Belk reward card link in the Home page ");

			Log.assertThat(Utils.waitForElement(driver,
					homePage.footers.closedBelkRewardCard),
					"10. Belk reward card is closed in the Home Page",
					"Belk reward card is not closed in the Home Page");

			homePage.footers.clickAboutBelk();
			Log.message("10. Expanding About in the Home page ", driver);

			Log.assertThat(Utils.waitForElement(driver,
					homePage.footers.expandAboutBelk),
					"11. About Belk  is expanded in the Home Page",
					"About Belk not is expanded in the Home Page");

			homePage.footers.clickAboutBelk();
			Log.message("12. closed About in the Home page ");

			Log.message("<b>Expected Result:</b> Footer pane should be available with the links 'Customer Service', 'Belk Rewards Card' & 'About Belk' ");
			Log.assertThat(Utils.waitForElement(driver,
					homePage.footers.closedAboutBelk),
					"13. About belk is closed in the Home Page",
					"About Belk  is not closed in the Home Page");
			Log.message(
					"<b>Actual Result:</b> Footer pane is available with the links 'Customer Service', 'Belk Rewards Card' & 'About Belk' ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_033

	@Test(groups = { "desktop", "tablet" }, description = "Verify the links in the links section of the footer - 'Customer Service'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_034(String browser) throws Exception {
//		String runPltfrm = Utils.getRunPlatForm();
//		if (runPltfrm != "desktop") {
//			throw new SkipException(
//					"The footer 'Customer Service' link not navigate to customer service page in mobile, hence skipping this test case on Mobile");
//		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			CustomerServicePage customerSerivcePage = homePage.footers
					.navigateToCustomerService();
			Log.message("3. Navigated to 'Belk' Customer Service!",
					driver.getCurrentUrl());
			Log.message("<b>Expected Result:</b> Clicking on 'Customer service' link should navigate to Customer serviice page");
			ArrayList<String> txtInBreadrcumb = customerSerivcePage.getTextInBreadcrumb();
			Log.assertThat(
					txtInBreadrcumb.get(0).equals("Home") && txtInBreadrcumb.get(1).equals("Customer Service") && Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "customer-service"),
					"<b>Actual Result:</b> Clicking on 'Customer service' link is navigated to Customer serviice page",
					"<b>Actual Result:</b> Clicking on 'Customer service' link is Not navigated to Customer serviice page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_034

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_035(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> footerTrackOrder = Arrays.asList("lnkTrackYourOrder");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Track Your Order' link should navigate to Order Tracking page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(footerTrackOrder,
							homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Track Your Order' link is navigated to Order Tracking page",
					"<b>Actual Result:</b> Clicking on 'Track Your Order' link is Not navigated to Order Tracking page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_035

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_036(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> footerShippingInFormation = Arrays
				.asList("lnkShippingInFormation");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Shipping information' link should navigate to Shipping information page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							footerShippingInFormation, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Shipping information' link is navigated to Shipping information page",
					"<b>Actual Result:</b> Clicking on 'Shipping information' link is Not navigated to Shipping information page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_036

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_037(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		// String lblTerms = "Terms & Conditions of Sale";

		String lblTerms = TermsAndConditionsPage.TERMS_CONDITIONS_CONTENT_HEADER;
		String runPltfrm = Utils.getRunPlatForm();
		List<String> termsAndConditionContent = Arrays.asList("primaryContent");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			TermsAndConditionsPage termsandconditions = homePage.footers
					.navigateToPolicyGuideLine();
			Log.message("3. Navigated to Policy Guide Line Page!",
					driver.getCurrentUrl());

			Log.assertThat(
					termsandconditions
							.getContentHeader()
							.contains(
									TermsAndConditionsPage.TERMS_CONDITIONS_CONTENT_HEADER),
					"4. " + lblTerms
							+ " should be displayed in the policy page",
					lblTerms + " is not display in the policy page");

			Log.message("<b>Expected Result:</b> Clicking on 'Policy & Guidelines' link should navigate to Policy & Guidelines page");
			Log.assertThat(
					termsandconditions.elementLayer.verifyPageElements(
							termsAndConditionContent, termsandconditions),
					"<b>Actual Result:</b> Clicking on 'Policy & Guidelines' link is navigated to Policy & Guidelines page",
					"<b>Actual Result:</b> Clicking on 'Policy & Guidelines' link is Not navigated to Policy & Guidelines page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_037

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify link 'FAQS'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_038(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String lblTFaqs = FaqPage.FAQS_CONTENT_NAME;
		String runPltfrm = Utils.getRunPlatForm();
		List<String> FAQContent = Arrays.asList("fqaPrimaryContent");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			FaqPage faqPage = homePage.footers.navigateToFAQ();
			Log.message("3. Navigated to FAQ Page!", driver.getCurrentUrl());

			Log.assertThat(faqPage.getTextFromFAQSPage().contains(lblTFaqs),
					"4. " + lblTFaqs + " should be displayed in the FAQS page",
					lblTFaqs + " is not displayed in the FAQS page");

			Log.message("<b>Expected Result:</b> Clicking on 'FAQS' link should navigate to FAQS page");
			Log.assertThat(
					faqPage.elementLayer
							.verifyPageElements(FAQContent, faqPage),
					"<b>Actual Result:</b> Clicking on 'FAQS' link is navigated to FAQS page",
					"<b>Actual Result:</b> Clicking on 'FAQS' link is Not navigated to FAQS page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_038

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_039(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> footerContact = Arrays.asList("lnkContact");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}
			homePage.footers.clickContactUsLink();
			Log.message("3. Clicked on 'Contact us' link ");
			Log.message("<b>Expected Result:</b> Clicking on 'Contact us' link should navigate to Contact Us page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(footerContact,
							homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Contact us' link is navigated to Contact Us page",
					"<b>Actual Result:</b> Clicking on 'Contact us' link is Not navigated to Contact Us page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_039

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_040(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String lblStore = StoreLocationPage.STORES_CONTENT_STRING;
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			StoreLocationPage storeLocationPage = homePage.footers
					.navigateToStore();
			Log.message("3. Navigated to store Page!");

			Log.assertThat(storeLocationPage.verifyBreadcrumb(), "4. "
					+ lblStore + " should be displayed in the FAQS page",
					lblStore + " is not display in the FAQS page");

			Log.message("<b>Expected Result:</b> Clicking on 'Store Locations' link should navigate to Store Locations page");
			Log.assertThat(
					storeLocationPage != null,
					"<b>Actual Result:</b> Clicking on 'Store Locations' link is navigated to Store Locations page",
					"<b>Actual Result:</b> Clicking on 'Store Locations' link is Not navigated to Store Locations page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_040

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_041(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		String belkRewardURL = "https://www.belkcredit.com/consumergen2/login.do?subActionId=1000&clientId=belk&langId=en&accountType=plcc";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if (runPltfrm.equals("mobile")) {
				Log.message("<b>Expected Result:</b> Clicking on 'Belk Reward card' link should navigate to Belk Reward card page");
				homePage.footers.clickBelkRewardCards();
				Log.message(
						"<b>Actual Result:</b> Clicking on 'Belk Reward card' link is navigated to Belk Reward card page",
						driver);
			}
			if (runPltfrm.equals("desktop")) {
				Log.message("<b>Expected Result:</b> Clicking on 'Belk Reward card' link should navigate to Belk Reward card page");
				homePage.footers.navigateToBelkRewardCard();
				Log.assertThat(
						driver.getCurrentUrl().equalsIgnoreCase(belkRewardURL),
						"<b>Actual Result:</b> Clicking on 'Belk Rewards Cards' link is navigated to Belk Reward card page - "
								+ driver.getCurrentUrl(),
						"<b>Actual Result:</b> Clicking on 'Belk Rewards Cards' link is Not navigated to Belk Reward card page",
						driver);

				Set<String> windowHandles = driver.getWindowHandles();
				Iterator<String> it = windowHandles.iterator();
				String parentBrowser = it.next();
				driver.switchTo().window(parentBrowser);
				Log.message("4. Navigated to Home Page!", driver);

				Log.message("<b>Expected Result:</b> Clicking on 'Pay Your Bill Online' link should navigate to Belk Reward card page");
				homePage.footers.navigateToBelkPayBillOnline();
				Log.assertThat(
						driver.getCurrentUrl().equalsIgnoreCase(belkRewardURL),
						"<b>Actual Result:</b> Clicking on 'Pay Your Bill Online' link is navigated to Belk Reward card page"
								+ driver.getCurrentUrl(),
						"<b>Actual Result:</b> Clicking on 'Pay Your Bill Online' link is Not navigated to Belk Reward card page",
						driver);

			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_FOOTERS_041

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		String applyBelkCardURL = "";
		if (runPltfrm.equals("desktop")) {
			applyBelkCardURL = "https://www.onlinecreditcenter2.com/eapplygen2/load.do?cHash=1315641838&subActionId=1000&langId=en";
		} else {
			applyBelkCardURL = "https://www.onlinecreditcenter2.com/mapplygen4.1/load.do?cHash=1315641838&subActionId=1000";
		}
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickBelkRewardCards();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Belk Credit card' link should navigate to Belk Reward card Apply page");
			homePage.footers.navigateToApplyCreditCard();
			Log.assertThat(
					driver.getCurrentUrl().equalsIgnoreCase(applyBelkCardURL),
					"<b>Actual Result:</b> Clicking on 'Belk Rewards Cards' link is navigated to Belk Reward card Apply page - "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Clicking on 'Belk Rewards Cards' link is Not navigated to Belk Reward card Apply page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SPRINT1_042

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_043(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String lblRewardBenefits = ViewMyBelkRewardPage.BELK_REWARDS_BENEFITS_CONTENT_STRING;
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickBelkRewardCards();
			}

			ViewMyBelkRewardPage viewMyBelkRewardPage = homePage.footers
					.navigateToViewMyRewardCard();
			Log.message("3. Navigated to view my reward card Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Visit My Belk Rewards' link should navigate to Belk Reward card Benefits page");
			Log.assertThat(
					viewMyBelkRewardPage.getTextFromBelkRewardsPage().contains(
							lblRewardBenefits),
					"<b>Actual Result:</b> Clicking on 'Visit My Belk Rewards' link is navigated to Belk Reward card Benefits page",
					"<b>Actual Result:</b> Clicking on 'Visit My Belk Rewards' link is Not navigated to Belk Reward card Benefits page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_043

	@Test(groups = { "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_044(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			AboutBelkPage aboutBelkPage = homePage.footers
					.navigateToAboutBelk();
			Log.message("3. Clicked on 'About Belk' link");

			Log.message("<b>Expected Result:</b> Clicking on 'About Us' link should navigate to About Us page");
			Log.assertThat(
					aboutBelkPage.getTextFromAboutPage().equals("About Us"),
					"<b>Actual Result:</b> Clicking on 'About Us' link is navigated to About Us page",
					"<b>Actual Result:</b> Clicking on 'About Us' link is Not navigated to About Us page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_044

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_045(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		List<String> footerViewOurAds = Arrays.asList("lnkViewOurAds");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickAboutBelk();
			}
			Log.message("<b>Expected Result:</b> Clicking on 'View Our Ads' link should navigate to Belk Ads page");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(footerViewOurAds,
							homePage.footers),
					"<b>Actual Result:</b> Clicking on 'View Our Ads' link is navigated to Belk Ads page",
					"<b>Actual Result:</b> Clicking on 'View Our Ads' link is Not navigated to Belk Ads page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_045

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_046(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		List<String> brandContent = Arrays.asList("brandPrimaryContent");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			// if (runPltfrm.equals("mobile")) {
			// homePage.footers.clickAboutBelk();
			// }

			ListOfBrandPage listBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("3. Naviagte to List Brand Page",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'List of Brands' link should navigate to Brands list page");
			Log.assertThat(
					listBrandPage.elementLayer.verifyPageElements(brandContent,
							listBrandPage),
					"<b>Actual Result:</b> Clicking on 'List of Brands' link is navigated to Brands list page",
					"<b>Actual Result:</b> Clicking on 'List of Brands' link is Not navigated to Brands list page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_047

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_047(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		List<String> footerCareerAtBelk = Arrays.asList("lnkCareersAtBelk");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickAboutBelk();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Careers at Belk' link should navigate to Belk Careers page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							footerCareerAtBelk, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Careers at Belk' link is navigated to Belk Careers page",
					"<b>Actual Result:</b> Clicking on 'Careers at Belk' link is Not navigated to Belk Careers page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_047

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_048(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		List<String> footerVendorInformation = Arrays
				.asList("lnkVendorInformation");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickAboutBelk();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Vendor Information' link should navigate to Vendor Information page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							footerVendorInformation, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Vendor Information' link is navigated to Vendor Information page",
					"<b>Actual Result:</b> Clicking on 'Vendor Information' link is Not navigated to Vendor Information page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_048

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();

		List<String> footerCorporateInformation = Arrays
				.asList("lnkCorporateInformation");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			if (runPltfrm.equals("mobile")) {
				homePage.footers.clickAboutBelk();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Corporate Information' link should navigate to Corporate Information page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							footerCorporateInformation, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Corporate Information' link is navigated to Corporate Information page",
					"<b>Actual Result:</b> Clicking on 'Corporate Information' link is Not navigated to Corporate Information page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_049

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_050(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> getConnectedContent = Arrays.asList("primaryContent");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			StayConnectedPage stayConnectedPage = homePage.footers
					.navigateToStayConnected();
			Log.message("3. Navigate to Download App Page",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'Download app page' link should navigate to Download app page");
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					stayConnectedPage.getTextFromstayConnectedPage().equals(
							"Get Connected")
							&& Utils.verifyPageURLContains(driver,
									webSite.split("/s/")[0], "get-connected")
							&& stayConnectedPage.elementLayer
									.verifyPageElements(getConnectedContent,
											stayConnectedPage),
					"1. Clicking on 'Download app page' link is navigated to Download app page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Download app page' link is Not navigated to Download app page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			ArrayList<String> txtInBreadcrumb = stayConnectedPage
					.getTextFromstayConnectedPage();
			if(runPltfrm =="desktop") {
			Log.assertThat(
					txtInBreadcrumb.get(0).equals("Home") &&
					txtInBreadcrumb.get(1).equals("customer service") &&
					txtInBreadcrumb.get(2).equals("Get Connected"),
					"2.  The breadcrumb value is as expected: 'Home > customer service > Get Connected'",
					"2.  The breadcrumb value is not as expected: 'Home > customer service > Get Connected' and the actual is: '"
							+ txtInBreadcrumb + "'");
			} else if(runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadcrumb.get(0).equals("Back to customer service"),
						"2.  The breadcrumb value is as expected: 'Back to customer service'",
						"2.  The breadcrumb value is not as expected: 'Back to customer service' and the actual is: '"
								+ txtInBreadcrumb + "'");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_050

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_051(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");
			GoogleStorePage googleStorePage = homePage.footers
					.navigateToGoogleStore();
			Log.message("3. Clicked on 'Download app Andriod' link");
			Log.message("<b>Expected Result:</b> Clicking on 'Download app Andriod' link should navigate to Google play store page");

			Log.assertThat(
					googleStorePage != null
							&& Utils.verifyPageURLContains(driver,
									GoogleStorePage.URL.split("&")[0],
									"com.belk.android"),
					"<b>Actual Result:</b> Clicking on 'Download app Andriod' link is navigated to Google play store page with the expected URL: "
							+ driver.getCurrentUrl(),
					"Clicking on 'Download app Andriod' link is not navigated to Google play store page and the actual URL: "
							+ driver.getCurrentUrl(), driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_051

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_052(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("Step 1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				homePage.footers.navigateToAppleStore();
				Log.message("3. Navigated to Apple Store Page!", driver);
			} else if (runPltfrm == "mobile") {
				Log.message("<b>Expected Result:</b> Clicking on 'Download app iOS' link should navigate to Apple Store page");
				Log.assertThat(
						homePage.footers
								.getIOSLinkURL()
								.contains(
										"https://itunes.apple.com/us/artist/belkinc./id575718365"),
						"<b>Actual Result:</b> Clicking on 'Download app iOS' link is navigated to Apple store page mapped with - https://itunes.apple.com/us/artist/belkinc./id575718365",
						"<b>Actual Result:</b> Clicking on 'Download app iOS' link is navigated to Apple store page is Not mapped with correct URL - https://itunes.apple.com/us/artist/belkinc./id575718365",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_052

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the links in the links section of the footer - 'Stay Connected'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_053(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> getConnectedContent = Arrays.asList("primaryContent");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			StayConnectedPage stayConnectedPage = homePage.footers
					.navigateToStayConnected();
			Log.message("2. Navigated to 'Stay connected!");

			Log.message("<b>Expected Result:</b> Clicking on 'Stay Connected' link Should navigate to Get Connected page");
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "get-connected")
							&& stayConnectedPage.elementLayer
									.verifyPageElements(getConnectedContent,
											stayConnectedPage),
					"1. Clicking on 'Stay Connected' link is navigated to Get Connected page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Stay Connected' link is Not navigated to Get Connected page and the actual url: "
							+ driver.getCurrentUrl(), driver);
			ArrayList<String> txtInBreadcrumb = stayConnectedPage
					.getTextFromstayConnectedPage();
			if(runPltfrm =="desktop") {
			Log.assertThat(
					txtInBreadcrumb.get(0).equals("Home") &&
					txtInBreadcrumb.get(1).equals("customer service") &&
					txtInBreadcrumb.get(2).equals("Get Connected"),
					"2.  The breadcrumb value is as expected: 'Home > customer service > Get Connected'",
					"2.  The breadcrumb value is not as expected: 'Home > customer service > Get Connected' and the actual is: '"
							+ txtInBreadcrumb + "'");
			} else if(runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadcrumb.get(0).equals("Back to customer service"),
						"2.  The breadcrumb value is as expected: 'Back to customer service'",
						"2.  The breadcrumb value is not as expected: 'Back to customer service' and the actual is: '"
								+ txtInBreadcrumb + "'");
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_053

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_054(String browser) throws Exception {

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] socialLinks = testData.get("SocailMediaLinks").split(",");

		Log.testCaseInfo(testData);

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			// Load Facebook Page
			homePage.footers.navigateToFacebookPage();
			Log.softAssertThat(
					socialLinks[0].replaceAll(" ", "").contains("facebook"),
					"2. Facebook Page Navigated Successfully!",
					"2. Cannot Navigate to Facebook Page!", driver);
			BrowserActions.navigateToBack(driver);
			// Load Twitter Page
			homePage.footers.navigateToTwitterPage();
			Log.softAssertThat(
					socialLinks[1].replaceAll(" ", "").contains("twitter"),
					"3. Twitter Page Navigated Successfully!",
					"3. Cannot Navigate to Twitter Page!", driver);
			BrowserActions.navigateToBack(driver);
			// Load Pinterest Page
			homePage.footers.navigateToPinterestPage();
			Log.softAssertThat(
					socialLinks[2].replaceAll(" ", "").contains("pinterest"),
					"4. Pinterest Page Navigated Successfully!",
					"4. Cannot Navigate to Pinterest Page!", driver);
			BrowserActions.navigateToBack(driver);
			// Load Instagram Page
			homePage.footers.navigateToInstagramPage();
			Log.softAssertThat(
					socialLinks[3].replaceAll(" ", "").contains("instagram"),
					"5. Instagram Page Navigated Successfully!",
					"5. Cannot Navigate to Instagram Page!", driver);
			BrowserActions.navigateToBack(driver);
			// Load YouTube Page
			homePage.footers.navigateToYoutubePage();
			Log.softAssertThat(
					socialLinks[4].replaceAll(" ", "").contains("youtube"),
					"6. Youtube Page Navigated Successfully!",
					"6. Cannot Navigate to Youtube Page!", driver);
			BrowserActions.navigateToBack(driver);
			// Load Google Plus Page
			homePage.footers.navigateToGooglePlusPage();
			Log.softAssertThat(
					socialLinks[5].replaceAll(" ", "").contains("google"),
					"7. Google Plus Page Navigated Successfully!",
					"7. Cannot Navigate to GooglePlus Page!", driver);
			Log.message("<b>Expected Result:</b> Clicking on 'facebook', 'twitter', 'pinterest','instagram','youtube', 'google' links Should navigate to corresponding page");
			BrowserActions.navigateToBack(driver);
			Log.message(
					"<b>Actual Result:</b> Clicking on 'facebook', 'twitter', 'pinterest','instagram','youtube', 'google' links is navigated to corresponding page",
					driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_054

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_055(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		List<String> footer = Arrays.asList("signUP", "txtEmail", "btnJoin");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the Home Page");

			Log.message("<b>Expected Result:</b> 'Sign up & Save' section should be available in the Footer section of page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(footer,
							homePage.footers),
					"<b>Actual Result:</b> 'Sign up & Save' section is available in the Footer section of page",
					"<b>Actual Result:</b> 'Sign up & Save' section is Not available in the Footer section of page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_055

	@Test(groups = { "desktop", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_056(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String email_id = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message("2. Typed email address in the footer");

			Log.message("<b>Expected Result:</b> Enrolled Email id should be available in the Header frame of Email signup page");
			Log.assertThat(
					emailsignuppage.getTextFromemailSignupPage().contains(
							email_id),
					"<b>Actual Result:</b> Enrolled Email id  "
							+ email_id
							+ "is available in the Header frame of Email signup page",
					"<b>Actual Result:</b> Enrolled Email id   "
							+ email_id
							+ "is Not available in the Header frame of Email signup page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_056

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_057(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String error_Message = HomePage.EMAIL_SIGNUP_ERROR_MSG;
		String email_id = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Error message should be displayed for Invalid Email Id");
			Log.assertThat(homePage.footers.getTextFromEmailMessage(email_id)
					.contains(error_Message),
					"<b>Actual Result:</b> Error message  - '" + error_Message
							+ "' is displayed for Invalid Email Id  - "
							+ email_id,
					"<b>Actual Result:</b> Error message - '" + error_Message
							+ " ' is Not displayed for Invalid Email Id   "
							+ email_id, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_057

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_058(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String legal_Message = "Placeholder for legal messaging.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> legal_Message should be displayed in the footer section of the page");
			Log.assertThat(
					homePage.footers.getTextFromLegalMessage().contains(
							legal_Message),
					"<b>Actual Result:</b> legal_Message " + legal_Message
							+ " is displayed in the footer section of the page",
					"<b>Actual Result:</b> legal_Message "
							+ legal_Message
							+ " is Not displayed in the footer section of the page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_058

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_059(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Page Feedback' link should display Feedback Pop-up screen");
			homePage.footers.clickOnPageFeedBack();
			Log.message("2. clicking on page feedback link");

			throw new SkipException(
					"Document is been downloaded instead of displayinng the Feedback pop-up screen, so we are skiping this test case as it requires Maintenance");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_059

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.clickOnOrderByCall();
			Log.message("2. Clicking on order by call link", driver);
			throw new SkipException(
					"Call window is appeared after clicking on order by call"
							+ "In Automation we can't handle this so we are skiping this test case");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_060

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Email Signup page is shown.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_061(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		Arrays.asList("signUp", "txtEmail", "btnJoin");

		String email_id = testData.get("EmailAddress");
		List<String> emailContent = Arrays.asList("emailContent");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailPage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message("2. Navigated to Email sign up page after filling the email address!");

			Log.message("<b>Expected Result:</b> Email Signup Page should display the Signup content");
			Log.assertThat(
					emailPage.elementLayer.verifyPageElements(emailContent,
							emailPage),
					"<b>Actual Result:</b> Email Signup Page is displayed with the Signup content",
					"<b>Actual Result:</b> Email Signup Page is Not display with the Signup content",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_061

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_062(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String Text_email_sign = EmailSignupPage.EMAIL_SIGNUP_CONTENT_MSG;
		String button_email = EmailSignupPage.EMAIL_SIGNUP_SAVE_PREF_CONTENT_MSG;

		String email_id = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message(
					"2. Navigated to Email sign up page after filling the email address!",
					driver);

			Log.assertThat(
					emailsignuppage.getTextFromemail_up().contains(
							Text_email_sign),
					"3. '" + Text_email_sign
							+ "' should be displayed in the Email_Sign_up page",
					Text_email_sign
							+ " is not display in the Email_sign_up page");

			Log.message("<b>Expected Result:</b> Save Preference button should be display in Email Signup Page");
			Log.assertThat(
					emailsignuppage.verifysavebutton().contains(button_email),
					"<b>Actual Result:</b> Save Preference button is displayed in Email Signup Page",
					"<b>Actual Result:</b> Save Preference button is Not display in Email Signup Page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_062

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_063(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String email_id = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> emailSignUpFields = Arrays.asList("txtEmail",
				"txtFirstName", "txtLastName", "txtAddress1", "txtAddress2",
				"txtCity", "lblState", "txtZipCode", "lblGender",
				"divBirthDate", "txtPhone", "btnCancel", "btnSavePreference");
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message("2. Enter valid email id- '" + email_id
					+ "' and navigated to email sign up page");

			Log.message("<b>Expected Result:</b> Email Signup Page should contain all the field 'Email', 'First Name', 'Last Name', 'Address 1', 'Address 2', 'City', 'State', 'Zip Code', 'Gender', 'Birthday', 'Phone', 'Cancel', 'Save Preferences'");
			Log.assertThat(
					emailsignuppage.elementLayer.verifyPageElements(
							emailSignUpFields, emailsignuppage),
					"<b>Actual Result:</b> Email Signup Page contains all the field 'Email', 'First Name', 'Last Name', 'Address 1', 'Address 2', 'City', 'State', 'Zip Code', 'Gender', 'Birthday', 'Phone', 'Cancel', 'Save Preferences'",
					"<b>Actual Result:</b> Email Signup Page doesn't have all the field 'Email', 'First Name', 'Last Name', 'Address 1', 'Address 2', 'City', 'State', 'Zip Code', 'Gender', 'Birthday', 'Phone', 'Cancel', 'Save Preferences'",
					driver);
			// Log.message("4. Navigated to Email sign up page after filling the
			// email address!", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_063

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_064(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");

		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page", driver);

			Log.message("<b>Expected Result:</b> Clicking on 'APO/FPO' should display APO/FPO conditions ");
			ApoFpoPage apoFpoPage = emailSignupPage.navigateToApoFpoPage();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					apoFpoPage != null
							&& Utils.verifyPageURLContains(driver,
									webSite.split("/s/")[0], "apo-fpo"),
					"1. Clicked on 'APO/FPO' displays APO/FPO conditions with the expected URL: "
							+ driver.getCurrentUrl(),
					"1. Clicked on 'APO/FPO' displays APO/FPO conditions and the actual URL: "
							+ driver.getCurrentUrl());
			ArrayList<String> breadcrumbList = apoFpoPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop")
				Log.assertThat(
						breadcrumbList.get(0).equals("Home")
								&& breadcrumbList.get(1).equals(
										"Customer Service")
								&& breadcrumbList.get(2).equals("apo-fpo"),
						"2. The breadcrumb is displayed as expected: 'Home > Customer Service > apo-fpo'",
						"2. The breadcrumb is not displayed as expected: 'Home > Customer Service > apo-fpo' and actual is: '"
								+ breadcrumbList.get(0)
								+ " > "
								+ breadcrumbList.get(1)
								+ " > "
								+ breadcrumbList.get(2) + "'", driver);
			else if (runPltfrm == "mobile")
				Log.assertThat(
						breadcrumbList.get(0)
								.equals("Back to Home"),
						"2. The breadcrumb is displayed as expected: '> Back to Home'",
						"2. The breadcrumb is not displayed as expected: '> Back to Home' and actual is: '> "
								+ breadcrumbList.get(0) + "'", driver);
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_064

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_065(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");
		List<String> requiredContent = Arrays.asList("requiredPrimaryContent");

		Log.testCaseInfo(testData);
		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Filling Email ID and Click Save button
			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page", driver);

			WhyIsRequiredPage whyisrequiredPage = emailSignupPage
					.navigateToWhyIsRequiredPage();
			Log.message("3. Navigate to why is required page");

			Log.message("<b>Expected Result:</b> Clicking on 'why is required' should display APO/FPO conditions ");
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "help-telephone")
							&& whyisrequiredPage.elementLayer
									.verifyPageElements(requiredContent,
											whyisrequiredPage),
					"1. Clicking on 'why is required' displays why is required conditions with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'why is required' doesn't display why is required conditions and the actual url: "
							+ driver.getCurrentUrl(), driver);
			ArrayList<String> breadcrumbList = whyisrequiredPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop")
				Log.assertThat(
						breadcrumbList.get(0).equals("Home")
								&& breadcrumbList.get(1).equals(
										"Customer Service")
								&& breadcrumbList.get(2).equals("help-telephone"),
						"2. The breadcrumb is displayed as expected: 'Home > Customer Service > help-telephone'",
						"2. The breadcrumb is not displayed as expected: 'Home > Customer Service > help-telephone' and actual is: '"
								+ breadcrumbList.get(0)
								+ " > "
								+ breadcrumbList.get(1)
								+ " > "
								+ breadcrumbList.get(2) + "'", driver);
			else if (runPltfrm == "mobile")
				Log.assertThat(
						breadcrumbList.get(0)
								.equals("Back to Home"),
						"2. The breadcrumb is displayed as expected: '> Back to Home'",
						"2. The breadcrumb is not displayed as expected: '> Back to Home' and actual is: '> "
								+ breadcrumbList.get(0) + "'", driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_065

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_066(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");

		Log.testCaseInfo(testData);

		List<String> homePageFields = Arrays.asList("homePageSlotData");

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page");

			emailSignupPage.clickCancelButton();
			Log.message("3. Click on cancel button", driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'Cancel' button in Email signup page should navigate back to Previous page ");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(homePageFields,
							homePage),
					"<b>Actual Result:</b> Clicking on 'Cancel' button in Email signup page is navigated back to Previous page",
					"<b>Actual Result:</b> Clicking on 'Cancel' button in Email signup page is Not navigated back to Previous page",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_066

	@Test(groups = { "desktop", "mobile", "tablet", "desktop_bat", "mobile_bat"  }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_067(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");
		String runPltfrm = Utils.getRunPlatForm();
		Log.testCaseInfo(testData);

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page", driver);

			emailSignupPage.fillingPreferencesDetails("valid_address1", "Male", "September-21-1992");
			Log.message(
					"3. Clicked 'Save Preference' button on Email Signin page",
					driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Save Preference' button in Email signup page should navigate to Confirmation page ");
			ArrayList<String> txtInBreadcrumb = emailSignupPage.getTextInBreadcrumb();
			if (runPltfrm == "desktop")
				Log.assertThat(
						txtInBreadcrumb.get(0).equals("Home")
								&& txtInBreadcrumb.get(1).equals("Email"),
						"<b>Actual Result:</b> Clicking on 'Save Preference' button in Email signup page is navigated to Confirmation page",
						"<b>Actual Result:</b> Clicking on 'Save Preference' button in Email signup page is not navigated to Confirmation page", driver);
			else if (runPltfrm == "mobile")
				Log.assertThat(
						txtInBreadcrumb.get(0).equals("Back to Home"),
						"<b>Actual Result:</b> Clicking on 'Save Preference' button in Email signup page is navigated to Confirmation page",
						"<b>Actual Result:</b> Clicking on 'Save Preference' button in Email signup page is not navigated to Confirmation page", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_067

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_068(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");

		Log.testCaseInfo(testData);

		List<String> emailFooter = Arrays.asList("btnPrivacyPolicy",
				"btnEmailUnsubscribe", "btnTermsAndUse");

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page", driver);

			Log.message("<b>Expected Result:</b> Email Signup page should display the footer section");
			Log.assertThat(
					emailSignupPage.elementLayer.verifyPageElements(
							emailFooter, emailSignupPage),
					"<b>Actual Result:</b> Email Signup page displays the footer section",
					"<b>Actual Result:</b> Email Signup page doesn't display the footer section as expected",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_068

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_069(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");
		String runPltfrm = Utils.getRunPlatForm();
		Log.testCaseInfo(testData);

		try {

			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page",
					driver.getCurrentUrl());

			PrivacyAndPolicyPage privacyPolicyPage = emailSignupPage
					.NavigateToPrivacyPolicy();
			Log.message("1. Navigated to 'Privacy Policy'  Page!",
					driver.getCurrentUrl());
			ArrayList<String> txtFromBreadcrumb = privacyPolicyPage.getTextFromBreadcrumb();
			Log.message("<b>Expected Result:</b> Email Signup page should display the Privacy content section");
			
			if(runPltfrm =="desktop") {
				Log.assertThat(
						txtFromBreadcrumb.get(0).equals("Home") &&
						txtFromBreadcrumb.get(1).equals("customer service") &&
						txtFromBreadcrumb.get(2).equals("Privacy Policy"),
						"<b>Actual Result:</b> Email Signup page displays the Privacy content section",
						"<b>Actual Result:</b> Email Signup page doesn't display the Privacy content section as expected", driver);
				} else if(runPltfrm == "mobile") {
					Log.assertThat(
							txtFromBreadcrumb.get(0).equals("Back to customer service"),
							"<b>Actual Result:</b> Email Signup page displays the Privacy content section",
							"<b>Actual Result:</b> Email Signup page doesn't display the Privacy content section as expected",
							driver);
				}
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_069

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_070(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");

		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page",
					driver.getCurrentUrl());
			emailSignupPage.clickMobileMessage();
			Log.message("<b>Expected Result:</b> Email Signup page should display the Message link section");
			Log.assertThat(
					emailSignupPage.verifyMobileMessageLink(),
					"<b>Actual Result:</b> Email Signup page displays the Message link section",
					"<b>Actual Result:</b> Email Signup page doesn't display the Message link section as expected",
					driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_070

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_071(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'UnSubScribe Email' option in Email Signup page should navigate to  Email Unsubscribe page");
			UnsubscribeEmailpage subScribeEmailpage = emailSignupPage
					.navigateToUnSubscribeLink();
			Log.message("3. Navigated to 'UnScbScribe Email'  Page!", driver);

			Log.assertThat(
					subScribeEmailpage.getTextFromUnScribeEmail().equals(
							"Email Unsubscribe"),
					"<b>Actual Result:</b> Clicking on 'UnScbScribe Email' option in Email Signup page is navigated to  Email Unsubscribe page",
					"<b>Actual Result:</b> Clicking on 'UnScbScribe Email' option in Email Signup page is Not Navigated to  Email Unsubscribe page",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_071

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_072(String browser) throws Exception {
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailId = testData.get("EmailAddress");
		List<String> termsAndConditionContent = Arrays.asList("primaryContent");

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailSignupPage = homePage.footers
					.fillingEmailAddress(emailId);
			Log.message("2. Enter valid email id- '" + emailId
					+ "' and navigated to email sign up page",
					driver.getCurrentUrl());

			TermsAndConditionsPage termsAndConditionPage = emailSignupPage
					.NavigateToTermsAndCondition();
			Log.message("3. Navigated to ' Terms And Condition'  Page!",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'Terms And Condition' option in Email Signup page should navigate to Terms And Condition page");
			Log.assertThat(
					termsAndConditionPage.elementLayer.verifyPageElements(
							termsAndConditionContent, termsAndConditionPage),
					"<b>Actual Result:</b> Clicking on ' Terms And Condition' option in Email Signup page is navigated to Terms And Condition page",
					"<b>Actual Result:</b> Clicking on ' Terms And Condition' option in Email Signup page is Not Navigated to Terms And Condition page",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_GLOBALNAV_072

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_073(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> Home page should contain more than 15 Content slots");
			Log.assertThat(
					homePage.verifyContentSlotCount(),
					"<b>Actual Result:</b> Home page contain's more than 15 Content slots",
					"<b>Actual Result:</b> Home page doesn't contain more than 15 Content slots",
					driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_073

	@Test(groups = { "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_075(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop") {
			throw new SkipException(
					"Site Map page didn't have left side navigator in mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> siteMapContent = Arrays.asList("siteContent");
		List<String> leftNavigation = Arrays.asList("btnCustomerService",
				"btnNeedHelp");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SiteMapPage siteMapPage = homePage.footers.navigateToSiteMap();
			Log.message("2. Clicked 'SiteMap on footer and navigated to SiteMap page");

			Log.assertThat(
					siteMapPage.elementLayer.verifyPageElements(siteMapContent,
							siteMapPage),
					"3. Site Map content is displayed as expected",
					"Site Map content is not displayed as expected, plz check the event log",
					driver);

			Log.message("<b>Expected Result:</b> Site Map page should display Left navigation list");
			Log.assertThat(
					siteMapPage.elementLayer.verifyPageElements(leftNavigation,
							siteMapPage),
					"<b>Actual Result:</b> Site Map page displays all the Left navigation list",
					"<b>Actual Result:</b> Site Map page doesn't display all the Left navigation list",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_075

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_076(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String level1 = testData.get("L1_category");
		List<String> headerNavL2;
		List<String> siteMapL2;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			headerNavL2 = homePage.headers.getL2Category(level1);
			Log.message("2. Header " + level1 + "sub-categories are :: "
					+ headerNavL2);

			SiteMapPage siteMapPage = homePage.footers.navigateToSiteMap();
			Log.message("3. Clicked 'SiteMap on footer and navigated to SiteMap page");

			siteMapL2 = siteMapPage.getL2Category(level1);
			Log.message("4. Site map's " + level1 + "sub-categories are :: "
					+ siteMapL2);

			Log.message("<b>Expected Result:</b> Site Map page should display all L1 & L2 Category list");
			Log.assertThat(
					Utils.compareTwoList(headerNavL2, siteMapL2),
					"Header "
							+ level1
							+ " <b>Actual Result:</b> sub-categories(L2) are displayed as same in sitemap page's sub-category(L2):: "
							+ siteMapL2,
					"<b>Actual Result:</b> Site map's "
							+ level1
							+ " sub-categories(L2) are not displayed as header's "
							+ level1 + " sub-categories(L2):: " + siteMapL2
							+ " , but expected is: " + headerNavL2, driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_076

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_077(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		String contentHeader = TermsAndConditionsPage.TERMS_CONDITIONS_CONTENT_HEADER;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			TermsAndConditionsPage termAndCondition = homePage.footers
					.navigateToTermsAndConditions();
			Log.message(
					"2. Clicked 'Terms' link on footer and navigated to Terms & Conditions page",
					driver);

			Log.message("<b>Expected Result:</b> Clicking on 'Terms' link in footer page should navigate to Terms of Use page");
			Log.assertThat(
					termAndCondition.getContentHeader().contains(contentHeader),
					"<b>Actual Result:</b> Clicking on 'Terms' link in footer page should navigate to Terms of Use page "
							+ contentHeader + "'",
					"<b>Actual Result:</b> Clicking on 'Terms' link in footer page should navigate to Terms of Use page "
							+ contentHeader, driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_077

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_078(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		String contentHeader = PrivacyAndPolicyPage.PRIVACY_POLICY_CONTENT_HEADER;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PrivacyAndPolicyPage privacyandpolicy = homePage.footers
					.navigateToPrivacyAndPolicy();
			Log.message(
					"2. Clicked 'Privacy Policy' link on footer and navigated to Privacy & Policy page",
					driver);

			Log.message("<b>Expected Result:</b> Privacy & Policy page should display the content");
			Log.assertThat(
					privacyandpolicy.getContentHeader().contains(contentHeader),
					"<b>Actual Result:</b> Privacy & Policy page displays the content"
							+ contentHeader + "'",
					"<b>Actual Result:</b> Privacy & Policy page displays the content "
							+ contentHeader, driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_078

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_079(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String email_id = testData.get("EmailAddress");
		List<String> tooltip = Arrays.asList("toolTipApoFpoOverlay");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message(
					"2. Navigated to Email sign up page after filling the email address!",
					driver.getCurrentUrl());

			emailsignuppage.mouseOverAPOFPO();
			Log.message("3. Mouse overing the APOFPO tooltip", driver);

			Log.message("<b>Expected Result:</b> Tool tip should be displayed in the Email Signup page");
			Log.assertThat(
					emailsignuppage.elementLayer.verifyPageElements(tooltip,
							emailsignuppage),
					"<b>Actual Result:</b> Tool tip is displayed in the Email Signup page",
					"<b>Actual Result:</b> Tool tip is Not displayed in the Email Signup page ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_079

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_080(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm != "desktop") {
			throw new SkipException(
					"Verify that a tooltip is hidden on mouse click/ESC key - only work on DESKTOP");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String email_id = testData.get("EmailAddress");
		List<String> tooltip = Arrays.asList("toolTipApoFpoOverlay");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message(
					"2. Navigated to Email sign up page after filling the email address!",
					driver.getCurrentUrl());

			emailsignuppage.mouseOverAPOFPO();
			Log.message("3. Mouse overing the APOFPO tooltip", driver);

			Log.assertThat(
					emailsignuppage.elementLayer.verifyPageElements(tooltip,
							emailsignuppage),
					"4. APOFPO Tool Tip is display in the Email Sign in page as expected",
					"APOFPO Tool Tip is not displayed in the Email Sign in page");
			// emailsignuppage.toolTipApoFpoOverlayClickESC();
			// Log.message("5. Clicked the ESC key in the keyboard on APOFPO tooltip overlay");
			//
			// Log.message("<b>Expected Result:</b> Tool Tip should be Hidden on clicking 'Esc' key");
			// Log.assertThat(
			// !emailsignuppage.elementLayer.verifyPageElements(tooltip,
			// emailsignuppage),
			// "<b>Actual Result:</b> Tool Tip is Hidden on clicking 'Esc' key",
			// "<b>Actual Result:</b> Tool Tip is Not Hidden on clicking 'Esc' key ",
			// driver);

			Log.message("<b>Expected Result:</b> Tool Tip should be Hidden by mouse moving out from APOFPO tooltip");
			emailsignuppage.mouseOutFromApoFpo();
			Log.assertThat(
					emailsignuppage.elementLayer.verifyPageElementsDoNotExist(
							tooltip, emailsignuppage),
					"<b>Actual Result:</b> Tool Tip is hidden by mouse moving out from APOFPO tooltip",
					"<b>Actual Result:</b> Tool Tip is not hidden by mouse moving out from APOFPO tooltip");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_080

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_081(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String email_id = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			EmailSignupPage emailsignuppage = homePage.footers
					.fillingEmailAddress(email_id);
			Log.message(
					"2. Navigated to Email sign up page after filling the email address!",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'ApoFpo Tooptip' link should navigate to 'apo-fpo' page");
			ApoFpoPage apoFpoPage = emailsignuppage.navigateToApoFpoPage();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					apoFpoPage != null
							&& Utils.verifyPageURLContains(driver,
									webSite.split("/s/")[0], "apo-fpo"),
					"1. Clicked on 'APO/FPO' displays APO/FPO conditions with the expected URL: "
							+ driver.getCurrentUrl(),
					"1. Clicked on 'APO/FPO' displays APO/FPO conditions and the actual URL: "
							+ driver.getCurrentUrl());
			ArrayList<String> breadcrumbList = apoFpoPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop")
				Log.assertThat(
						breadcrumbList.get(0).equals("Home")
								&& breadcrumbList.get(1).equals(
										"Customer Service")
								&& breadcrumbList.get(2).equals("apo-fpo"),
						"2. The breadcrumb is displayed as expected: 'Home > Customer Service > apo-fpo'",
						"2. The breadcrumb is not displayed as expected: 'Home > Customer Service > apo-fpo' and actual is: '"
								+ breadcrumbList.get(0)
								+ " > "
								+ breadcrumbList.get(1)
								+ " > "
								+ breadcrumbList.get(2) + "'", driver);
			else if (runPltfrm == "mobile")
				Log.assertThat(
						breadcrumbList.get(0)
								.equals("Back to Home"),
						"2. The breadcrumb is displayed as expected: '> Back to Home'",
						"2. The breadcrumb is not displayed as expected: '> Back to Home' and actual is: '> "
								+ breadcrumbList.get(0) + "'", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_081

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_082(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.enterEmailID(emailid);
			Log.message("3. Entered credentials(" + emailid
					+ ") in 'Sign in ' page");

			signinPage.enterPassword(password);
			Log.message("4. Entered credentials(" + password
					+ ") in 'Sign in' page");

			signinPage.clickBtnSignIn();
			Log.message("5. Clicked 'Sign in' button");

			Log.message("<b>Expected Result:</b> Error message should be displayed on clicking Sign In Button with Invalid email details");
			Log.assertThat(
					signinPage.getSignInFormErrorMsg() != null,
					"<b>Actual Result:</b> Error message is displayed on clicking Sign In Button with Invalid email details"
							+ signinPage.getSignInFormErrorMsg(),
					"<b>Actual Result:</b> Error message is Not displayed on clicking Sign In Button with Invalid email details",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_082

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_083(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String txtErrorMessage = SignIn.INLINE_USERNAME_ERROR;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.clickBtnSignIn();
			Log.message("3. Click on sign in button'Sign in' page");

			Log.message("<b>Expected Result:</b> Inline Error message should be displayed on clicking Sign In Button without entering email details");
			Log.assertThat(
					signinPage.getUserNameInlineErrorMsg().contains(
							txtErrorMessage),
					"<b>Actual Result:</b> Inline Error message ('"
							+ txtErrorMessage
							+ "') is displayed on clicking Sign In Button without entering email details",
					"<b>Actual Result:</b> Inline Error message is Not displayed on clicking Sign In Button without entering email details",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_083

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_085(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + "'",
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_085

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_086(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();

			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_086

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_087(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String runPltfrm = Utils.getRunPlatForm();
		String stringMatches = testData.get("CategoryLandingURL");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_087

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_088(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String runPltfrm = Utils.getRunPlatForm();
		String stringMatches = testData.get("CategoryLandingURL");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_088

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_089(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String runPltfrm = Utils.getRunPlatForm();
		String stringMatches = testData.get("CategoryLandingURL");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_089

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_090(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_090

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_091(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_091

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_092(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_092

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_093(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_093

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_094(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category);
			homePage.headers.navigateTo(l1_Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (categoryLandingPage != null),
					"1. User is navigated to Category Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Category Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat((breadcrumbList.get(0).equals("Home"))
						&& (breadcrumbList.get(1).equals(l1_Category)),
						"2. Breadcrumb is displayed as expected- Home > "
								+ l1_Category,
						"2. Breadcrumb is not displayed as expected- Home > "
								+ l1_Category + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_094

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_095(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_094

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_096(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_096

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_097(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_097

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_098(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_098

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_099(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_099

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_100(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_100

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_101(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_101

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_102(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_102

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_103(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_103

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_104(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(
										l1_Category) && (breadcrumbList.get(2)
										.equals(l2_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category
								+ "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_104

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_105

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_106(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_106

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_107(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_107

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_108(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_108

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_109(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_109

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_110(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_110

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_111(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_111

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_112(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_112

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_113(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equals(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_113

	// Mobile_Done
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_114(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1_Category = testData.get("L1_category");
		String l2_Category = testData.get("L2_category");
		String l3_Category = testData.get("L3_category");
		String stringMatches = testData.get("CategoryLandingURL");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		// CategoryLandingPage categoryLandingPage = null;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Home"))
								&& (breadcrumbList.get(1).equalsIgnoreCase(l1_Category)
										&& (breadcrumbList.get(2)
												.equals(l2_Category)) && (breadcrumbList
											.get(3).equals(l3_Category))),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ l1_Category + " > " + l2_Category + " > "
								+ l3_Category + "' and the actual is: '"
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3) + "'", driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equalsIgnoreCase("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_114

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_GLOBALNAV_115(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		String belkRewardURL = "https://www.belkcredit.com/consumergen2/login.do?subActionId=1000&clientId=belk&langId=en&accountType=plcc";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.footers.scrollToFooter();
			Log.message("2. Scroll to footer in the homepage");

			if (runPltfrm.equals("mobile")) {
				Log.message("<b>Expected Result:</b> Clicking on 'Check Available Balance' link should navigate to Belk Reward card page");
				homePage.footers.clickBelkRewardCardBalance();
				Log.message(
						"<b>Actual Result:</b> Clicking on 'Check Available Balance' link is navigated to Belk Reward card page",
						driver);
			}
			if (runPltfrm.equals("desktop")) {
				Log.message("<b>Expected Result:</b> Clicking on 'Check Available Balance' link should navigate to Belk Reward card page");
				homePage.footers.navigateToBelkRewardCardBalance();
				Log.assertThat(
						driver.getCurrentUrl().equalsIgnoreCase(belkRewardURL),
						"<b>Actual Result:</b> Clicking on 'Check Available Balance' link is navigated to Belk Reward card page - "
								+ driver.getCurrentUrl(),
						"<b>Actual Result:</b> Clicking on 'Check Available Balance' link is Not navigated to Belk Reward card page",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_FOOTERS_115

}// Headers