package com.belk.testscripts.checkout;

import java.util.*;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;

import com.belk.pages.*;
import com.belk.pages.account.*;
import com.belk.pages.registry.RegistryInformationPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.StateUtils;
import com.belk.support.*;

@Listeners(EmailReport.class)
public class Checkout {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "Checkout";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System.getProperty("webSite")
				: context.getCurrentXmlTest().getParameter("webSite"));
	}// init

	public void cleanup(WebDriver driver, String username, String password) throws Exception {
		HomePage homePage = new HomePage(driver, webSite).get();
		SignIn signInPage = homePage.headers.navigateToSignIn();
		MyAccountPage myAcc = signInPage.signInToMyAccount(username, password);
		ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
		bagPage.removeItemsFromBag();
	}

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system displays the cart merge message.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String size = testData.get("size");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Select Size, Color, Qty
			pdpPage.selectSize(size);
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigate to SignIn Page
			SignIn signin = homepage.headers.navigateToSignIn().get();
			Log.message("5. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			ShoppingBagPage shoppingbag = signin.signInFromShoppingBag(emailid, password);
			Log.message("6. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Verifying Global and Item leve cart merging alert message

			Log.message("7. Cart merge validation message is displayed and validated", driver);

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>system should displays the cart merge message");
			Log.assertThat((shoppingbag.getBagmergeLineitemlevelalert() == shoppingbag.getBagmergeLineitemlevelalert()),
					"<b>Actual Result :</b> 'Global and Line item level alert message is displayed'",
					"<b>Actual Result :</b> 'Global and Line item level alert message not is displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_001

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system displays the cart merge message when user logins in during checkout - Bag Merge", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String size = testData.get("size");
		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Select Size, Color, Qty
			pdpPage.selectSize(size);
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Click on Mini cart(View bag)
			ShoppingBagPage shoppingbag = pdpPage.clickOnMiniCart();

			Log.message("5. Clicking on Minicart(View bag) button");

			// Click on Checkout button in Shopping bag page
			SignIn sign = (SignIn) shoppingbag.clickOnCheckoutInOrderSummary(shoppingbag);
			Log.message("6. Click on Checkout button in Shopping bag page");

			// Login with Valid User creditinals
			shoppingbag = sign.signInFromShoppingBag(emailid, password);
			Log.message("7. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Verifying Global and Item leve cart merging alert message
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>Verify system displays the cart merge message when user logins in during checkout - Bag Merge");
			Log.assertThat((shoppingbag.getBagmergeGloballevelalert() == shoppingbag.getBagmergeLineitemlevelalert()),
					"<b>Actual Result :</b>Global and Line item level alert message is displayed",
					"<b>Actual Result :</b>Global and Line item level alert message is displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_006

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the item updated cart message - Surcharge", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");
			String surcharhehgettxt = shoppingBagPage.surchargeTxtDisplay();
			Log.message("9. The Shipping Surcharge is: " + surcharhehgettxt);

			Log.message("<br>");

			Log.message("<b> Expected Result 1: </b> This item will incur a shipping surcharge.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtSurchargeOnCartPage"),
							shoppingBagPage),
							"<b> Actual Result 1: </b> \"Shipping Surcharge\" is displayed in the Shopping Bag page and the text is: "+surcharhehgettxt,
							"<b> Actual Result 1: </b> \"Shipping Surcharge\" is not displayed in the Shopping Bag page.",
							driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_003

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify that no duplicate(s) of a single line item is shown during bag/basket merge.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_016(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String color;
		int QtyInBagIn2ndTry;
		int QtyInBagIn1stTry;
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = myaccount.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			pdpPage.selectSizeByIndex(1);
			Log.message("5. selected the size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			QtyInBagIn1stTry = Integer.parseInt(shoppingBagPage.getNoOfQtyInShoppingBagPage());
			shoppingBagPage.headers.clickSignOut();
			pdpPage = shoppingBagPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with '" + searchKey + "'");

			Log.message("8. Navigated to PDP");

			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message("9. selected the '" + color + "' Color !");
			// Selecting the Size
			pdpPage.selectSizeByIndex(1);
			Log.message("10. selected the size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("11. Product added to Bag !");
			// Navigating to shopping bag
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("12. Navigated to shopping bag page");
			// Navigating to signIn page
			signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			signIn.enterEmailID(emailid);
			signIn.enterPassword(password);
			signIn.clickBtnSignIn();
			BrowserActions.nap(5);
			QtyInBagIn2ndTry = Integer.parseInt(shoppingBagPage.getNoOfQtyInShoppingBagPage());
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("User should see that only the Quantity of product should be increased in the Cart.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat((QtyInBagIn1stTry + 1) == QtyInBagIn2ndTry,
					"No of Quantity is increased by 1 in shoppingBagPage .",
					"No of Quantity is not increased in shoppingBagPage .");
			String GlobalMessage = shoppingBagPage.getGlobalMessage();
			String LineMessage = shoppingBagPage.getLineMessage();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Global Message should be: Items from your other shopping bag have been added to your current shopping bag.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(GlobalMessage.equals(shoppingBagPage.GLOBAL_MESSAGE),
					"Global Message :'Items from your other shopping bag have been added to your current shopping bag' displayed.",
					"Global Message :'Items from your other shopping bag have been added to your current shopping bag' not displayed.");
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message("Line Item message should be:This item in your bag has been changed.");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.assertThat(LineMessage.equals(shoppingBagPage.LINE_MESSAGE),
					"Line Message :'This item in your bag has been changed.' displayed.",
					"Line Message :'This item in your bag has been changed.' not displayed.");
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_016


	@Test(groups = { "desktop",
	"tablet" }, description = "Verify that the respective Cart Merging message is displayed by adding same product as guest & authorised user ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_017(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String msgCartMerging = "Items from your other shopping bag have been added to your current shopping bag.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");


			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Getting ProductName from the BreadCrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("7. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// Selecting the random colour from the colour swatch
			pdpPage.selectColorByIndex(1);
			Log.message("8. Selected Color from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("9. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQuantity();
			Log.message("10. Selected Quantity: '1' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("11.Product added to Shopping Bag!");

			homePage.headers.clickSignOut();
			Log.message("12. Logged out from the Account");

			// Navigating to product with productId
			pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("13. Searched with '" + searchKey + "'");
			Log.message("14. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			pdpPage.selectColorByIndex(1);
			Log.message("15. Selected Color from the color swatches in the PDP Page");

			pdpPage.selectSize(selectedSize);
			Log.message("16. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQuantity();
			Log.message("17. Selected Quantity: '1' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("18. By Clicking on 'AddToShopping' Bag , the Product added to 'Shopping Bag'");

			homePage.headers.navigateToSignIn();
			Log.message("19. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToAccount(emailid, password);
			Log.message("20. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			ShoppingBagPage shopingBagPage = new ShoppingBagPage(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> The Quantity of the  product should be increased in the 'shoppingBag' Page ");

			int productIndex = shopingBagPage.getIndexByProductName(productName);
			String updatedQtyInShoppingBag = shopingBagPage.getQuantityValue(productIndex);

			Log.softAssertThat(updatedQtyInShoppingBag.equals("2"),
					"<b>Actual Result1:</b> The Quantity of the product is increased in the 'shoppingBag' Page",
					"<b>Actual Result1:</b> The Quantity of the product is not increased in the 'shoppingBag' Page",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> The Message 'Items from your other shopping bag have been added to your current shopping bag' should be displayed in the Shopping Bag page ");

			Log.softAssertThat(((shopingBagPage.getmsgCartMerging()).contains(msgCartMerging)),
					"<b>Actual Result2:</b> The Message 'Items from your other shopping bag have been added to your current shopping bag' is displayed in Shopping Bag page",
					"<b>Actual Result2:</b> The Message 'Items from your other shopping bag have been added to your current shopping bag' is not displayed in Shopping Bag page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CHECKOUT_017


	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the 'Product Tile' in 'Order Summary' section in '2. Billing' page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		String color;
		String size;
		List<String> element = Arrays.asList("scrollBarInBillingPage");
		List<String> element1 = Arrays.asList("orderSummeryPane");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = null;
			for (int i = 0; i <= 2; i++) {
				// Load the SearchResult Page with search keyword
				SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey[i]);
				Log.message(
						"2." + i + " Searched with keyword '" + searchKey[i] + "' and navigated to search result Page");

				// Load the PDP Page with search keyword
				pdpPage = searchResultpage.selectProduct();
				Log.message("3." + i + " Navigated to PDP Page !");
				// Selecting the Color
				color = pdpPage.selectColor();
				Log.message("4." + i + " selected the '" + color + "' Color !");
				// Selecting the Size
				size = pdpPage.selectSize();
				Log.message("5." + i + " selected the '" + size + "' size !");
				// Adding item to the shopping bag
				pdpPage.clickAddToBag();
				Log.message("6." + i + " Product added to Bag !");
				BrowserActions.nap(5);
			}
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. Clicked checkoutAsGuest and navigated to checkout page");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("10. shipping details filled successfully");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. clicked continue button");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Scroll bar should be displayed if there are more than 2 items in the Cart.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(checkOutPage.elementLayer.verifyPageElements(element, checkOutPage),
					"If productCount is more than 2 then scroll bar is visible in billing page !",
					"if productCount is more than 2 then scroll bar is not visible in billing page !", driver);
			Log.message("<br>");
			// Returning to shipping page
			checkOutPage.clickReturnToShipping();
			Log.message("12. Returned to shippingPage");
			// Returning to shoppingBag page
			checkOutPage.clickReturnToShoppingBag();
			Log.message("13. Returned to shoppingBagPage");
			// Removing item from Bag
			shoppingBagPage.removeItemsByIntex(1);
			Log.message("14. 1 item removed from shoppingBag");
			// Navigating to signIn page
			signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("15. Navigated to SignIn page");
			// clicking Checkout As Guest
			checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("16. Clicked checkoutAsGuest and navigated to checkout page");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("17. shipping details filled successfully");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. clicked continue button");
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Only 2 items should be visible to the customer be default.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(checkOutPage.getProductCountInBillingPage() == 2,
					"By default Only 2 items is visible to the customer.",
					"By default Only 2 items is not visible to the customer.", driver);
			Log.assertThat(checkOutPage.elementLayer.verifyPageElementsDoNotExist(element, checkOutPage),
					"if product is 2 then scroll bar is not there in billing page !",
					"if product is 2 then scroll bar is still there in billing page !", driver);

			// clicking Toggle
			checkOutPage.clikOnMiniCartToggle();
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message(
					"The following product details should be shown when user clicks on arrow (>) :Product Thumbnail , Product Name ,Variations ,QuantityPrice");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.message("19. clicked On Toggle");
			Log.assertThat(checkOutPage.elementLayer.verifyPageElements(element1, checkOutPage),
					"After clicking  [arrow (>)] Product Thumbnail , Product Name ,Variations ,Quantity ,Price displayed .",
					"After clicking  [arrow (>)] Product Thumbnail , Product Name ,Variations ,Quantity ,Price not displayed .",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_019

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system displays Price on the Order Summary section in '2. Billing' page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_020(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String color;
		String size;

		List<String> elements = Arrays.asList("OrgPriceInBillingPageOnOrderSummary",
				"NowPriceInBillingPageOnOrderSummary");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("3. Navigated to PDP Page !");
			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page !");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page !");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. clicked on Checkout As Guest Button !");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("10. shipping details filled successfully !");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. clicked on continue button !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Item without Promotions/Clearance /Sale: Only Price ,Original Price and Now Price should be displayed .");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(checkOutPage.elementLayer.verifyPageElements(elements, checkOutPage),
					"Item without Promotions/Clearance /Sale: then it is displaying only Original Price and Now Price  .",
					"Item without Promotions/Clearance /Sale: then it is not displaying only Original Price and Now Price  .",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_020

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the Shipping Address section Ship to Home", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_025(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String txtAddressShipping = "shippingaddress";
		String txtFree = "FREE";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("3. Searched with '" + searchKey + "'");

			Log.message("4. Navigated to PDP Page !");

			String color = pdpPage.selectColor();
			Log.message("5. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);

			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. CheckoutAsGuest button is clicked");

			LinkedHashMap<String, String> shippingDetails = checkOutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Express");
			List<String> indexes = new ArrayList<String>(shippingDetails.keySet());
			String firstNameInShippingPage = indexes.get(0).replace("type_firstname_", "");
			String lastNameInShippinPage = indexes.get(1).replace("type_lastname_", "");
			String address1InShippinPage = indexes.get(2).replace("type_address_", "");
			String cityInShippinPage = indexes.get(3).replace("type_city_", "");
			String zipCodeInShippingPage = indexes.get(5).replace("type_zipcode_", "");
			Log.message("10. Shipping details filled ");
			checkOutPage.clickOnContinueInShipping();
			Log.message("11. Continue shipping field is clicked");
			checkOutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("12. Continue button is clicked in suggestion popup modal", driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:<b> In Shipping Address Home Delivery panel below fields should be properly shown. Customer Name,Customer Address,Customer Phone Number,Shipping Method should be displayed");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippingPanelName"), checkOutPage),
					"<b>Actual Result:<b> Customer name is displayed properly",
					"<b>Actual Result:<b> Customer name is not displayed properly");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShipingPanelAddress"), checkOutPage),
					"<b>Actual Result:<b> Customer address is displayed properly",
					"<b>Actual Result:<b> Customer address is not displayed properly");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippingPanelCity"), checkOutPage),
					"<b>Actual Result:<b> Customer Phone Number is displayed properly",
					"<b>Actual Result:<b> Customer Phone Number is not displayed properly");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShipmentMethod"), checkOutPage),
					"<b>Actual Result:<b>Shippment Method is displayed properly",
					"<b>Actual Result:<b>Shippment Method is not displayed properly");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippmenPanelPhone"), checkOutPage),
					"<b>Actual Result:<b>Phone no is displayed properly",
					"<b>Actual Result:<b>Phone no is not displayed properly");
			Log.message("<br>");
			String txtShipPromoMsg = checkOutPage.getTextFromShippingPromotionMsg();
			Log.message("13. Got the text from shipping promotion and the message is: " + txtShipPromoMsg);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> customer qualified free shipping display FREE and a resource message should be displayed");
			Log.assertThat(txtFree.equals(txtShipPromoMsg),
					"<b>Actual Result :</b> Text FREE is displayed in shipmentmethod",
					"<b>Actual Result :</b> Text FREE is not displayed in shipment method");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("msgShipmentResource"), checkOutPage),
					"<b>Actual Result :</b> Shipment Resource message is displayed properly",
					"<b>Actual Result :</b> Shipment Resource message is not displayed properly");
			checkOutPage.clickOnEditBySectionName(txtAddressShipping);
			Log.message("14. Edit link is clicked in shipping Address Home Delivery panel");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:<b> Verify user can edit the Shipping Address Home Delivery details from Billing Addresspanel and When clicked on Edit link, it should navigate to the Checkout: Step 1. Shipping page");
			Log.assertThat(checkOutPage.verifyShippingFieldActive(),
					"<b>Actual Result:<b>After clicking Edit link user is able to edit the shipping field,Navigated to shipping page for editing",
					"<b>Actual Result:<b>After clicking Edit link user is not able to edit the shipping field,Not navigated to shipping page for editing",
					driver);
			Log.message(
					"<b>Expected Result :</b> Shipping page, displaying all the pre-filled information. Already saved addreses should be prefilled in the drop down list");
			String firstNameInTextField = checkOutPage.getTextFromFirstNameTextFieldInShipping();
			Log.assertThat(firstNameInTextField.equals(firstNameInShippingPage),
					"<b>Actual Result :</b> First name textfield is prefiled with previously stored address",
					"<b>Actual Result :</b> First name textfield is not prefiled with previously stored address");
			String lastNameInTextField = checkOutPage.getTextFromLastNameTextFieldInShipping();
			Log.assertThat(lastNameInTextField.equals(lastNameInShippinPage),
					"<b>Actual Result :</b> Last name textfield is prefiled with previously stored Name",
					"<b>Actual Result :</b> Last name textfield is not prefiled with previously stored Name");
			String addressOneInTextField = checkOutPage.GetTextFromAddressOneTextField();
			Log.message("15. Got the text from Address one field and the address is" + addressOneInTextField);
			Log.assertThat(addressOneInTextField.equals(address1InShippinPage),
					"<b>Actual Result:<b> Address one textfield is prefiled with previously stored address",
					"<b>Actual Result:<b> Address one textfield is not prefiled with previously stored address");
			String cityInTxtfld = checkOutPage.getShippingAddressCity();
			Log.message("16. Got the text from text field " + cityInTxtfld);
			Log.assertThat(cityInTxtfld.equals(cityInShippinPage),
					"<b>Actual Result:<b>city textfield is prefiled with previously stored address",
					"<b>Actual Result:<b> city textfield is not prefiled with previously stored address");
			String zipcodeInTxtfld = checkOutPage.getShippingAddressZipCode();
			Log.message("17. Got the text from text field " + cityInTxtfld);
			Log.assertThat(zipcodeInTxtfld.equals(zipCodeInShippingPage),
					"<b>Actual Result:<b>zipcode textfield is prefiled with previously stored address",
					"<b>Actual Result:<b> zipcode textfield is not prefiled with previously stored address");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_025

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the Edit link in Payment Method Section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_028(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("6. Navigated to 'Mini Cart' !");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("7. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message("9. Entered Shipping address as guest user");
			checkOutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button in Shipping address Page");
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("13. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("14. Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on 'Continue' Button in Billing Page");

			checkOutPage.clickPaymentEditLink();
			Log.message("16. Navigated to place order page and clicked on edit link");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1:</b> Page should be redirected to Billing address page when clicking on Edit in payment section in Place order page");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("btnCheckGiftCard"), checkOutPage),
					"<b>Actual Result 1:</b> Page is redirected to Billing Address page",
					"<b>Actual Result 1:</b> Page is not redirected to Billing Address page", driver);

			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Edited the card details entered already");

			Log.message("<br>");
			Log.message("<b> Expected Result 2:</b> User should be able to edit payment details");

			Log.assertThat(
					!(checkOutPage.getPaymentDetailsInBillingPage()
							.equals(checkOutPage.getCreditCardTypeFromEditPayment())),
							"<b>Actual Result 2:</b> Payment details are edited",
					"<b>Actual Result 3:</b> Payment details are not edited");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_028



	// TC_BELK_CHECKOUT_033
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message displayed as credit card not met the expected limit", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_033(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			/*
			 * SearchResultPage searchResultpage = myAccountPage.headers
			 * .searchProductKeyword(searchKey); Log.message(
			 * "4. Navigated to Search Result Page : " + searchKey);
			 * 
			 * PdpPage pdpPage = searchResultpage.navigateToPDPWithProductId();
			 * Log.message("5. Navigated to PDP from Search results page !");
			 */

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkRewardCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("12. Entered card details");
			checkoutPage.clickOnContinueInBilling();
			Log.message("13. Clicked on continue button  in Billing address Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_033

	// TC_BELK_CHECKOUT_034
	
	// TC_BELK_CHECKOUT_035
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Order is properly placed with Belk  reward credit card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_035(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");
			Log.message("10. Entered Shipping Address in Shipping address Page");
			checkoutPage.clickOnContinueInShipping();
			Log.message("11. Clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("12. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkRewardCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("13. Entered card details");
			checkoutPage.clickOnContinueInBilling();

			Log.message("14. Clicked on Continue button in Billing address Page");

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> Page should be redirected to Place order page");

			if (Utils.getRunPlatForm().equals("mobile"))

				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutPage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);
			else
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutPage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();

			Log.message("15. Clicked on 'Place order' button in Place order page");

			Log.message("</br>");
			Log.message("<b>Expected Result 2:</b> Page should be redirected to Order receipt page");

			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("btnReturnToShopping"),
							orderConfirmationPage),
							"<b>Actual Result 2:</b> Page is redirected to Order receipt page",
							"<b>Actual Result 2:</b> Page is not redirected to Order receipt page", driver);

			Log.message("</br>");
			Log.message("<b>Expected Result 3:</b> Order should be placed with Belk Reward Credit Card");

			Log.assertThat((orderConfirmationPage.getPaymentMethodType()).equals("Belk Reward Credit Card"),
					"<b>Actual Result 3:</b> Order is placed with Belk Reward Credit Card",
					"<b>Actual Result 3:</b> Order is not placed with Belk Reward Credit Card", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_035


	// TC_BELK_CHECKOUT_037
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify validation failure message is shown for  invalid Belk Gift card number with invalid CVV", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_037(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			SearchResultPage srchpage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			PdpPage pdpPage=srchpage.selectProduct();
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCardInvalid");
			Log.message("12. Entered gift card details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("13. Clicked on 'Apply Gift card' button");
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. clicked on continue button in Billing address Page");
			Log.message("<b> Expected Result :</b> Invalid Belk Gift Card Number warning message  should be displayed near Card number.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_037

	// TC_BELK_CHECKOUT_038
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify card locked message is shown for  Belk Gift card number", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_038(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("12. Entered gift card details");

			checkoutPage.clickOnApplyGiftCard();
			Log.message("13. Clicked on 'Apply Gift card' button");

			// checkoutPage.clickOnContinueInBilling();
			// Log.message("12. clicked on continue button in Billing address
			// Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_038

	// TC_BELK_CHECKOUT_039
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message shown for Belk gift card not met the expected limit", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_039(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("12. Entered gift card details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("13. Clicked on 'Apply Gift card' button");

			// checkoutPage.clickOnContinueInBilling();
			// Log.message("12. clicked on continue button in Billing address
			// Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_039

	

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify system hides the '2-Days' shipping method for the excluded products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_051(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			String selectedColor = pdpPage.selectColor();
			Log.message("7. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("8. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Selected Quantity: '2' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("10. Product added to Shopping Bag!'");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			pdpPage.clickOnMiniCart();

			Log.message("11. Clicking on Mini cart icon, It navigated to the ShoppingBag page");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("12. Navigated to Checkout as SignedUser!");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("13. Clicking on 'Yes' button for multi shipping in the 'ShippingDetails' Page");

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();

			shipToMultipleAddressPage.clickaddLink();
			Log.message("14. Clicked on 'Add' Link to add the New Address In the 'ShipToMultipleAddress' Page");

			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("NO", "valid_address1");
			Log.message("15. Entered Address Details");

			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Added Address is Saved");

			// Select only first address from the dropdown
			shipToMultipleAddressPage.selectFirstAddressInAddressDropDown();
			Log.message("17. Selected the saved address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(
					"18. By Clicking on 'Continue' in the MultipleShippingAddress Page, It Navigated to Shipping method page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> In shipping method page ,'2-Days' Shipping method should be hidden from the drop down  for the Excluded products.");

			Log.assertThat(
					(checkoutPage.getShippingMethods().contains("Standard"))
					&& (!(checkoutPage.getShippingMethods().contains("Test Shipment"))
							&& (!(checkoutPage.getShippingMethods().contains("Express")))
							&& (!(checkoutPage.getShippingMethods().contains("Overnight")))),
							"<b>Actual Result:</b> In shipping method page ,'2-Days' Shipping method is hidden from the drop down  for the Excluded products.",
					"<b>Actual Result:</b> In shipping method page ,'2-Days' Shipping method is not hidden from the drop down  for the Excluded products.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_051

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify system displays the 'Why is Standard Shipping my only option' link for the excluded products. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_052(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			String selectedColor = pdpPage.selectColor();
			Log.message("7. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("8. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Selected Quantity: '2' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("10. Product added to Shopping Bag!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			pdpPage.clickOnMiniCart();

			Log.message("11. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("12. Navigated to Checkout as SignedUser!");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("13. Clicking on 'Yes' button for multi shipping");

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();

			shipToMultipleAddressPage.clickaddLink();
			Log.message("14. Clicked on 'Add' Link to add the New Address In the 'ShipToMultipleAddress' Page");

			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("NO", "valid_address1");
			Log.message("15. Entered Address Details");

			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Added Address is Saved");

			// Select only first address from the dropdown
			shipToMultipleAddressPage.selectFirstAddressInAddressDropDown();
			Log.message("17. Selected the saved address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(
					"18. By Clicking on 'Continue' in the MultipleShippingAddress Page and Navigated to Shipping method page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> When the excluded product is present in the shopping bag , then 'Why is Standard Shipping my only option' link should be displayed in the 'Shipping method' page");

			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkWhyStandardShipping"), checkoutPage),
					"<b>Actual Result1:</b> When the excluded product is present in the shopping bag , then 'Why is Standard Shipping my only option' link is displayed in the 'Shipping method' page",
					"<b>Actual Result1:</b> When the excluded product is present in the shopping bag , then 'Why is Standard Shipping my only option' link is not displayed in the 'Shipping method' page",
					driver);

			Log.message("<br>");
			checkoutPage.clickOnWhyStandardShipping();
			Log.message("19. Clicked on link 'Why is Standard Shipping my only option'");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Standard ShippingModel PopUp should be displayed in the 'shippingMethod' Page");

			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("standardShippingModelPopUp"),
							checkoutPage),
							"<b>Actual Result1:</b> Standard ShippingModel PopUp is displayed in the 'shippingMethod' Page",
							"<b>Actual Result1:</b> Standard ShippingModel PopUp is not displayed in the 'shippingMethod' Page",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_052

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the contents in Standard shipping only modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_053(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();

			PdpPage pdppage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			pdppage.selectSizeByIndex(1);
			Log.message("7.  Size is Selected from size dropdown");
			pdppage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdppage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			String cartQty = shoppingBagPage.getQuantityValue(0);

			String cartproductname = shoppingBagPage.PrdName();
			String cartprice = shoppingBagPage.PrdSubtotal();

			String cartproductimgurl = shoppingBagPage.getProductImageSrc();

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked Checkout button");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"12. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("13. Clicked on Add Link to add Address");
			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes", address);
			Log.message("14. Shipping details filled in the shipping address form");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("15. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("16. Selected Use original address as entered Radio button");
			shipToMultipleAddressPage.ClickContinueBtn();
			Log.message("17. Clicked on 'Continue' Button on Address Validation popup");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("18. Clicked on 'Save' button on Multi Shipping Address page");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("19. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 4);
			Log.message("20. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();

			Log.message("21. Navigated to Shipping Method Page by Clicking Continue button from Multi Shipping Page");
			shippingMethodPage.ClickWhyIsStandardShippingMyOnlyOption();
			Log.message("22. Clicked on 'Why is Standard shipping my only option?");

			Log.message(
					"<b>Expected Result:</b> Expedited Shipping should be displayed with the following details,Product Thumbnail ,Name, Variation, QuantityPrice");
			String QtyFromWhyIsStandardShippingMyOnlyOptionModel = shippingMethodPage
					.getQtyFromWhyIsStandardShippingMyOnlyOptionModel();

			String productnameFromWhyIsStandardShippingMyOnlyOptionModel = shippingMethodPage
					.getProductNameFromWhyIsStandardShippingMyOnlyOptionModel();
			String priceFromWhyIsStandardShippingMyOnlyOptionModel = shippingMethodPage
					.getPriceFromWhyIsStandardShippingMyOnlyOptionModel();

			String producturlFromWhyIsStandardShippingMyOnlyOptionModel = shippingMethodPage
					.getProductUrlFromWhyIsStandardShippingMyOnlyOptionModel();
			Log.softAssertThat(QtyFromWhyIsStandardShippingMyOnlyOptionModel.trim().contains(cartQty.trim()),
					"<b>Actual Result:</b> Quantity is displyed in the Shipping Restriction model.",
					"<b>Actual Result:</b> Quantity is not displyed in the Shipping Restriction model.", driver);

			Log.softAssertThat(productnameFromWhyIsStandardShippingMyOnlyOptionModel.contains(cartproductname),
					"<b>Actual Result:</b> Product Name is displyed in the Shipping Restriction model.",
					"<b>Actual Result:</b> Product Name is not displyed in the Shipping Restriction model.", driver);

			Log.softAssertThat(cartprice.trim().contains(priceFromWhyIsStandardShippingMyOnlyOptionModel.trim()),

					"<b>Actual Result:</b> Price is displyed in the Shipping Restriction model.",
					"<b>Actual Result:</b> Price is not displyed in the Shipping Restriction model.", driver);
			Log.softAssertThat(producturlFromWhyIsStandardShippingMyOnlyOptionModel.equals(cartproductimgurl),

					"<b>Actual Result:</b> Product Thumbnail is displyed in the Shipping Restriction model.",
					"<b>Actual Result:</b>  Product Thumbnail is not displyed in the Shipping Restriction model.",
					driver);
			shippingMethodPage.ClickGoToShoppingBagInWhyIsStandardShippingMyOnlyOption();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("23. Removed items from Cart");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_053

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify the 'Go to Shopping Bag' link in Standard shipping only modal ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_054(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			String selectedColor = pdpPage.selectColor();
			Log.message("7. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("8. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Selected Quantity: '2' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("10.Product added to Shopping Bag!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			pdpPage.clickOnMiniCart();

			Log.message("11. Clicking on Mini cart icon, It navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("12. Navigated to Checkout as SignedUser!");

			// Clicking on 'Yes' button for multishipping

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("13. Clicking on 'Yes' button for multi shipping");

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();

			shipToMultipleAddressPage.clickaddLink();
			Log.message("14. Clicked on 'Add' Link to add the New Address In the 'ShipToMultipleAddress' Page");

			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("NO", "valid_address1");
			Log.message("15. Entered Address Details");

			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Added Address is Saved");

			// Select only first address from the dropdown
			shipToMultipleAddressPage.selectFirstAddressInAddressDropDown();
			Log.message("17. Selected the saved address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(
					"18. By Clicking on 'Continue' in the MultipleShippingAddress Page and Navigated to Shipping method page");

			checkoutPage.clickOnWhyStandardShipping();
			Log.message("19. Clicked on link 'Why is Standard Shipping my only option' in the 'ShippingMethod' page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> 'Go to Shopping Bag' link should be displayed in the 'Standard shipping only modal' in the 'ShippingMethod' page");

			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkGoToShoppingBagINpopup"),
							checkoutPage),
							"<b>Actual Result1:</b> 'Go to Shopping Bag' link is displayed in the 'Standard shipping only modal' in the 'ShippingMethod' page",
							"<b>Actual Result1:</b> 'Go to Shopping Bag' link is not displayed in the 'Standard shipping only modal' in the 'ShippingMethod' page",
							driver);

			Log.message("<br>");
			checkoutPage.clickOnGoToShoppingBagInPopUp();
			Log.message("20. Clicked on link' GoToShoppingBag'");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> By clicking on 'Go to Shopping Bag' link, It should be re-directed to 'ShoppingBag' page");

			Log.softAssertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnCheckoutInOrderSummary"),
							shoppingBagPage),
							"<b>Actual Result2:</b> By clicking on 'Go to Shopping Bag' link, It is re-directed to 'ShoppingBag' page",
							"<b>Actual Result2:</b> By clicking on 'Go to Shopping Bag' link, It is not re-directed to 'ShoppingBag' page",
							driver);

			Log.testCaseResult();

			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_054

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify the 'Continue' button in 'Standard shipping only modal'. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_055(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			// Searching product in my account page with a productId
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			String selectedColor = pdpPage.selectColor();
			Log.message("7. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("8. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Selected Quantity: '2' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("10. Product added to Shopping Bag!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			pdpPage.clickOnMiniCart();

			Log.message("11. Clicking on Mini cart icon, It navigated to the 'ShoppingBag' page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("12. Navigated to Checkout as SignedUser!");

			// Clicking on 'Yes' button for multishipping

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("13. Clicking on 'Yes' button for multi shipping");

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();

			shipToMultipleAddressPage.clickaddLink();
			Log.message("14. Clicked on 'Add' Link to add the New Address In the 'ShipToMultipleAddress' Page");

			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("NO", "valid_address1");
			Log.message("15. Entered Address Details");

			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Added Address is Saved");

			// Select only first address from the dropdown
			shipToMultipleAddressPage.selectFirstAddressInAddressDropDown();
			Log.message("17. Selected the saved address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(
					"18. By Clicking on 'Continue' in the MultipleShippingAddress Page and Navigated to Shipping method page");

			checkoutPage.clickOnWhyStandardShipping();
			Log.message("19. Clicked on link 'Why is Standard Shipping my only option'");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> 'Continue' button should be displayed in the 'Standard shipping only modal' in the ShippingMethod' Page");

			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnContinueInStandardShippingInPopup"),
							checkoutPage),
							"<b>Actual Result1:</b> 'Continue' button is displayed in the 'Standard shipping only modal' in the ShippingMethod' Page",
							"<b>Actual Result1:</b> 'Continue' button is not displayed in the 'Standard shipping only modal' in the ShippingMethod' Page",
							driver);

			Log.message("<br>");
			checkoutPage.clickOnContinueInStandardShippingPopup();
			Log.message("20. Clicked on link 'Continue' in Standard shippingmethod popup");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> By clicking on 'Continue' button , It  should be re-directed to 'Multi Ship Step-3 Billing' page");

			Log.softAssertThat(checkoutPage.getTextFromBillingHeader().contains("Return to Shipping Methods"),
					"<b>Actual Result2:</b> By clicking on 'Continue' button , It  is re-directed to 'Multi Ship Step-3 Billing' page",
					"<b>Actual Result2:</b> By clicking on 'Continue' button , It  is not re-directed to 'Multi Ship Step-3 Billing' page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_055

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the details in the Order Summary section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_056(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked Checkout button");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"12. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("13. Clicked on Add Link to add Address");
			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes", address);
			Log.message("14. Shipping details filled in the shipping address form");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("15. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("16. Selected Use original address as entered Radio button");
			shipToMultipleAddressPage.ClickContinueBtn();
			Log.message("17. Clicked on 'Continue' Button on Address Validation popup");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("18. Clicked on 'Save' button on Multi Shipping Address page");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("19. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 4);
			Log.message("20. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();

			Log.message("21. Navigated to Shipping Method Page by Clicking Continue button from Multi Shipping Page");
			Log.message(
					"<b>Expected Result:</b> Order Summary section should be displayed with the following details ");

			Log.message("No test Data to proceed");
			shippingMethodPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("22. Items Removed from Cart");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_056

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the Edit link in Order Summary section of 2. Shipping Method page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_057(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		List<String> ShoppingCartPagelist = Arrays.asList("divCart");
		List<String> ShippingMethodPagelist = Arrays.asList("lnkEdit");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			pdpPage.selectSizeByIndex(2);
			Log.message("6.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("7. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("8. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to Cart page");
			String cartproductname = shoppingBagPage.PrdName();

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Clicked Checkout button");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on Add Link to add Address");
			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes", address);
			Log.message("13. Shipping details filled in the shipping address form");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on 'Save' Button on Multi Shipping Address page");
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("15. Selected Use original address as entered Radio button");
			// Clicking on 'Continue' button
			shipToMultipleAddressPage.ClickContinueBtn();
			Log.message("16. Clicked on 'Continue' Button on Address Validation popup");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicked on 'Save' button on Multi Shipping Address page");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("18. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 4);
			Log.message("19. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();

			Log.message("20. Navigated to Shipping Method Page by Clicking Continue button from Multi Shipping Page");
			Log.message(
					"<b>Expected Result:</b> In Shipping Method page, the 'Edit' link should be displayed in Order Summary section - header");
			Log.softAssertThat(
					shoppingBagPage.elementLayer.verifyPageElements(ShippingMethodPagelist, shippingMethodPage),
					"<b>Actual Result:</b> In Shipping Method page, the 'Edit' link is displayed in Order Summary section - header .",
					"<b>Actual Result:</b> In Shipping Method page, the 'Edit' link is not displayed in Order Summary section - header .");
			shoppingBagPage = shippingMethodPage.ClickEditLinkFromOrderSummary();
			Log.message(
					"<b>Expected Result:</b>  Clicking on the Edit link from Order Summary in Shipping Method page should be navigated to Cart Page.");
			Log.softAssertThat(shoppingBagPage.elementLayer.verifyPageElements(ShoppingCartPagelist, shoppingBagPage),
					"<b>Actual Result:</b> Clicking on the Edit link from Order Summary in Shipping Method Page successfully navigated to Cart Page.",
					"<b>Actual Result:</b> Clicking on the Edit link from Order Summary in Billing page failed to navigate to Cart Page.",
					driver);
			String productname = shoppingBagPage.PrdName();
			Log.message("productname" + productname);

			Log.message("<b>Expected Result:</b>  Previous selections and data should get remembered in the cart page");
			Log.softAssertThat(shoppingBagPage.PrdName().equals(cartproductname),
					"<b>Actual Result:</b> Previous selections and data is remembered in the cart page.",
					"<b>Actual Result:</b> Previous selections and data is not remembered in the cart page", driver);

			Log.message("<b>Expected Result:</b> User should be allowed to make the changes");

			shoppingBagPage.ChangeQuantity(1);
			Log.softAssertThat(shoppingBagPage.getQuantityValue(0).equals("1"),
					"<b>Actual Result:</b> User is allowed to update the quantity.",
					"<b>Actual Result:</b> User is not allowed to update the quantity.", driver);
			checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message(
					"<b>Expected Result:</b>  User should be able to checkout and the previous selection and data should be shown");
			Log.message("checkoutPage.getValueFromQuantityInOrderSummary():"
					+ checkoutPage.getValueFromQuantityInOrderSummary());
			Log.softAssertThat(checkoutPage.getValueFromQuantityInOrderSummary() == 1,
					"<b>Actual Result:</b> User is able to checkout and the previous selection and data is shown",
					"<b>Actual Result:</b> User is not able to checkout and the previous selection and data is not shown",
					driver);
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("21. Removed items from Cart");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_057

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the Shipping Address section below order Summary section ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_058(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");

			Log.message("6. Navigated to PDP");

			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked Checkout button");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"12. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("13. Clicked on Add Link to add Address");
			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes", address);
			Log.message("14. Shipping details filled in the shipping address form");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("15. Clicked on 'Save' Button on Multi Shipping Address page");
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("16. Selected Use original address as entered Radio button");
			// Clicking on 'Continue' button
			shipToMultipleAddressPage.ClickContinueBtn();
			Log.message("17. Clicked on 'Continue' Button on Address Validation popup");
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("18. Clicked on 'Save' button on Multi Shipping Address page");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("19. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 4);
			Log.message("20. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();

			Log.message("21. Navigated to Shipping Method Page by Clicking Continue button from Multi Shipping Page");
			Log.message(
					"<b>Expected Result:</b> Shipping address will be displayed in this order 1.In Store Pickup (BOPIS),2.Ship to Home,3.Registry");
			Log.message("No test data for In Store Pickup (BOPIS),2.Ship to Home,3.Registry addresses");
			shippingMethodPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("22. Items Removed from Cart");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_058

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system navigates to 'Place order' page, when authorized user enter from Mini cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_069(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");

			Log.message("5. Navigated to PDP");

			String selectedColor = pdpPage.selectColor();
			Log.message("6. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("7. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			String selectedQuantity = pdpPage.selectQuantity();
			Log.message("8. Selected Quantity:'" + selectedQuantity + "' from the size drop down  in the PDP Page");

			pdpPage.clickAddToBag();
			Log.message("9. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to 'Mini Cart' !");

			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Navigated to Checkout as SignedUser!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It should Navigated to 'Express Checkout' page");

			Log.softAssertThat(checkOutPage.getTextFromBillingHeader().contains("Return to Billing"),
					"<b>Actual Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It is Navigated to 'Express Checkout' page",
					"<b>Actual Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It is not Navigated to ''Express Checkout' page",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> Default Shipping Address, Billing Address , Payment Details should be displayed in 'Express Checkout' page");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPaymentDetailsInExpressCheckout", "billingModule", "shippingModule"),
							checkOutPage),
							"<b>Actual Result2:</b> Default Shipping Address, Billing Address , Payment Details is displayed in 'Express Checkout' page",
							"<b>Actual Result2:</b> Default Shipping Address, Billing Address , Payment Details is not displayed in 'Express Checkout' page",
							driver);

			Log.testCaseResult();

			shoppingBagPage = checkOutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_069

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system navigate to 'Place order' page, when user login via Checkout Login", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_070(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");

		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			homePage.headers.clickSignOut();
			Log.message("5. SignOut from the account");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("6. Searched with '" + searchKey + "'");
			Log.message("7. Navigated to PDP");

			String selectedColor = pdpPage.selectColor();
			Log.message("8. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("9. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			String selectedQuantity = pdpPage.selectQuantity();
			Log.message("10. Selected Quantity:'" + selectedQuantity + "' from the size drop down  in the PDP Page");

			pdpPage.clickAddToBag();
			Log.message("11. Product added to Shopping Bag!");

			pdpPage.clickOnMiniCart();
			Log.message("12. Navigated to 'Mini Cart' !");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("13. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.signInToExpressCheckout(emailid, password);
			Log.message("14. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'Express CheckOut' page", driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It should Navigated to 'Express Checkout' page");

			Log.softAssertThat(checkOutPage.getTextFromBillingHeader().contains("Return to Billing"),
					"<b>Actual Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It is Navigated to 'Express Checkout' page",
					"<b>Actual Result1:</b> By Clicking On 'CheckOut' in 'minicart' page, It is not Navigated to ''Express Checkout' page",
					driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> Default Shipping Address, Billing Address , Payment Details should be displayed in 'Express Checkout' page");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPaymentDetailsInExpressCheckout", "billingModule", "shippingModule"),
							checkOutPage),
							"<b>Actual Result2:</b> Default Shipping Address, Billing Address , Payment Details is displayed in 'Express Checkout' page",
							"<b>Actual Result2:</b> Default Shipping Address, Billing Address , Payment Details is not displayed in 'Express Checkout' page",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_070





	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can purchase order with 'Belk Gift Card' from Billing Address screen ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_119(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			pdppage.selectSizeByIndex(1);
			Log.message("4.  Size is Selected from size dropdown !");
			pdppage.clickAddToBag();
			Log.message("5. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("6. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. clicked on checkoutAsGuest Button !");
			checkoutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("9. Clicked on continue Button in shipping page !");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_Hawaii_address");
			Log.message("10. Billing details filled as address 'alaska' !");
			// filling card details
			checkoutPage.fillingBelkGiftCardDetails("GiftCard100");
			Log.message("11. GiftCard details filled with $100 !");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("12. clicked on ApplyGiftCard in Billing page !");
			checkoutPage.clickOnContinueInBilling();
			Log.message("13. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("14. clicked PlaceOrderButton in placeOrder page !");
			String ActualPaymentType = orderConfirmationPage.getPaymentTypeInPlaceOrderPage();
			String ExpectedPaymentType = "Gift Card";
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address !",
					"Order didn't placed Successfully on added shipping address !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("In Order Receipt payment should be displayed as gift card");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(ActualPaymentType.equals(ExpectedPaymentType),
					"In Order Receipt payment displayed as gift card",
					"In Order Receipt payment didn't displayed as gift card", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_119

	// TC_BELK_CHECKOUT_121
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user can check balance for Belk Gift Card from Billing Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_121(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String giftCard = testData.get("Payments");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);

			Log.message("   Selected emailid : " + emailid);
			Log.message("   Selected password : " + password);

			Log.message("3. Navigated to My Account Page!");
			SearchResultPage searchResultpage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Navigated to Search Result Page : " + searchKey);

			PdpPage pdpPage = searchResultpage.navigateToPDPWithProductId();
			Log.message("5. Navigated to PDP from Search results page !");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			} else {

				checkoutPage.ContinueAddressValidationModalWithDefaults();
			}

			checkoutPage.fillingBelkGiftCardDetails(giftCard);
			Log.message("11. Entered gift card details");
			checkoutPage.clickOnCheckGiftCardBalance();
			Log.message("12. Clicked on Check Gift card balnce button");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Belk Gift card balance should be displayed");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtGiftCardMessage"), checkoutPage),
					"<b>Actual Result 1:</b> Belk Gift card balance is displayed and Balance is: $"
							+ checkoutPage.getGiftCardBalance(),
							"<b>Actual Result 1:</b> Belk Gift card balance is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_121

	// TC_BELK_CHECKOUT_122
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message is shown when applying invalid Belk gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_122(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String giftCard = testData.get("Payments");

		String errorMessage = "Invalid Gift Card Number or PIN";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);

			Log.message("3. Navigated to My Account Page!");
			SearchResultPage searchResultpage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Navigated to Search Result Page : " + searchKey);

			PdpPage pdpPage = searchResultpage.navigateToPDPWithProductId();
			Log.message("5. Navigated to PDP from Search results page !");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails(giftCard);
			Log.message("11. Entered gift card details");

			checkoutPage.clickOnApplyGiftCard();

			Log.message("12. Clicked on Apply Gift Card");

			Log.assertThat(checkoutPage.getGiftCardErrormessage() == errorMessage,
					"Error message is displayed correctly :'" + checkoutPage.getGiftCardErrormessage() + "'",
					"Error message is not displayed as '" + checkoutPage.getGiftCardErrormessage()
					+ "' but expected is 'Invalid Gift Card Number or PIN'");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_122

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can able to purchase order with suggested address from Shipping address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_125(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String colorName = pdpPage.selectColor();
			Log.message("4. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize = pdpPage.selectSize();
			Log.message("5. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			pdpPage.selectQuantity();
			Log.message("6. Select the Quantity");

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsGuest("valid_address3", "Standard");
			Log.message("11. Filling shipping details as guest");

			checkoutpage.clickBtnShippingPage();
			Log.message("12. Click on the Shipping button");
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b>  Address validation screen should be opened ");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("addressValidationModal"), checkoutpage),
					"<b>Actual Result 1: </b> Address Validation Pop_up Should get open after filling the shipping details as Guest User",
					"<b>Actual Result 1: </b> Address Validation Pop_up Should get not get open after filling the shipping details as Guest User",
					driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 2: 'Continue' and 'Cancel' button should be shown on the Address Validation Pop_up");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(
							Arrays.asList("btnCancelAddressValidationpop_up", "btnContinueAddressValidationpop_up"),
							checkoutpage),
							"<b>Actual Result 2: </b> Continue' and 'Cancel' button is displayed after filling the shipping details as Guest User in Address validation pop up",
							"<b>Actual Result 2: </b>  Continue' and 'Cancel' button is not displayed after filling the shipping details as Guest User in Address validation pop up",
							driver);

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 3: 'Use Suggested Address with radio button' should be properly shown  on the Address Validation Pop_up");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblSuggestedAddressLabel"),
							checkoutpage),
							"<b>Actual Result 3: </b> 'Use Suggested Address with radio button' is displayed after filling the shipping details as Guest User in Address validation pop up",
							"<b>Actual Result 3: </b>  'Use Suggested Address with radio ' is not displayed after filling the shipping details as Guest User in Address validation pop up",
							driver);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 4: 'Use Original Address with radio button' should be properly shown  on the Address Validation Pop_up");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblOriginalAddressLabel"),
							checkoutpage),
							"<b>Actual Result 4: </b> 'Use  Original with radio button' is displayed after filling the shipping details as Guest User in Address validation pop up",
							"<b>Actual Result 4: </b> 'Use Original Address with radio ' is not displayed after filling the shipping details as Guest User in Address validation pop up",
							driver);

			Log.message("<br>");
			checkoutpage.clickContinueInAddressValidationModal();
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_125

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can able to add original address as entered in Shipping address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_127(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);

			String color = pdpPage.selectColor();
			Log.message("3. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("4. selected size :" + size);

			pdpPage.clickAddToBag();
			Log.message("5. Click on Add to bag button");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. CheckoutAsGuest button is clicked");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("9. Shipping details filled,", driver);
			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Continue button is clicked");
			String zipCodeInOriginalAddress = checkOutPage.returnZipCodeInOriginalAddress();
			Log.message("11. Got the original address zipcode in suggestion panel: " + zipCodeInOriginalAddress);
			checkOutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("12. Navigated to Billing page from shipping page!", driver);
			Log.message("13. Continue button in suggestion panel is clicked");
			String zipcodeInShipping = checkOutPage.returnShippingZipCodeInBillingAddressPanel();
			Log.message("14. Got the zip code in Shipping home panel from billing page and the zip code is "
					+ zipcodeInShipping);
			Log.message("<br>");
			Log.message("<b>Expected Result : </b> original address should be displayed in shipping address screen ");
			Log.assertThat(zipCodeInOriginalAddress.equals(zipcodeInShipping),
					"<b>Actual Result : </b> Original Address in shipping panel is properly displayed in Billing Address page",
					"<b>Actual Result : </b> Original Address in shipping panel is not properly displayed in Billing Address page");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			// Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_127

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify by confirm order with payment method 'Discover' and by adding different multiple address example FPO,Hawaii ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_128(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			Log.message("4.  Size is Selected from size dropdown !");
			pdppage.selectQtyByIndex(2);
			Log.message("5. Quantity drowdown is selected with Quantity '2' !");
			pdppage.clickAddToBag();
			Log.message("6. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. clicked on checkoutAsGuest Button !");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("10. clicked on yes Button for MultiShipping !");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on Add Link to add Address");
			LinkedHashMap<String, String> shippingDetails1 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_APOFPO_address");
			Log.message("13. Shipping details filled in the shipping address form");
			List<String> indexes = new ArrayList<String>(shippingDetails1.keySet());
			String StateInAddress1 = indexes.get(5).replace("select_state_", "");
			String stateCode1 = StateUtils.getStateCode(StateInAddress1);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("15. Clicked on 'Continue' Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.clickaddLink();
			LinkedHashMap<String, String> shippingDetails2 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_Hawaii_address");
			Log.message("16. Shipping details filled in the shipping address form");
			List<String> index = new ArrayList<String>(shippingDetails2.keySet());
			String StateInAddress2 = index.get(5).replace("select_state_", "");
			String stateCode2 = StateUtils.getStateCode(StateInAddress2);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicked on 'Save' Button on Multi Shipping Address page !");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("18. Clicked on 'Continue' Button on Address Validation popup !");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("19. Clicked on 'Save' Button on Multi Shipping Address page !");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("20. First Item's address dropdown is selected !");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message("21. Second Item's address dropdown is selected !");
			shipToMultipleAddressPage.clickContinue();
			Log.message("22. Clicked on continue Button !");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue Button in shipping page !");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_alaska");
			Log.message("24. Billing details filled as address 'alaska' !");
			// filling card details
			LinkedHashMap<String, String> cardType = checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("25. Card details filled as cardType 'Discover' !");
			List<String> card = new ArrayList<String>(cardType.keySet());
			String cardTypeInBilling = card.get(0).replace("select_cardtype_", "");
			checkoutPage.clickOnContinueInBilling();
			Log.message("26. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("27. clicked PlaceOrderButton in placeOrder page !");
			String stateCode1InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(0).get("State");
			String stateCode2InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(1).get("State");
			String cardTypeInOrder = orderConfirmationPage.getPaymentMethodInOrderSummary().get("CardType");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address !",
					"Order didn't placed Successfully on added shipping address !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Added multiple shipping address APO/FPO and Hawaii should be properly displayed in Order confirmation screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(stateCode1.equals(stateCode1InOrder),
					"Added multiple shipping address APO/FPO properly displayed in Order confirmation screen.",
					"Added multiple shipping address APO/FPO properly not displayed in Order confirmation screen.",
					driver);
			Log.assertThat(stateCode2.equals(stateCode2InOrder),
					"Added multiple shipping address Hawaii properly displayed in Order confirmation screen.",
					"Added multiple shipping address Hawaii properly not displayed in Order confirmation screen.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message(
					"Added multiple shipping address with selected payment method 'Discover' should be properly displayed in Order Receipt screen");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.assertThat(cardTypeInBilling.equals(cardTypeInOrder),
					"Added multiple shipping address with selected payment method 'Discover' properly displayed in Order Receipt screen !",
					"Added multiple shipping address with selected payment method 'Discover' not displayed in Order Receipt screen !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_128

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify by confirm order with payment method 'Visa' and by adding different multiple address example Alaska, California", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_129(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			pdppage.selectSize();
			Log.message("4.  Size is Selected from size dropdown !");
			pdppage.selectQtyByIndex(2);
			Log.message("5. Quantity drowdown is selected with Quantity '2' !");
			pdppage.clickAddToBag();
			Log.message("6. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. clicked on checkoutAsGuest Button !");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("10. clicked on yes Button for MultiShipping !");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on Add Link to add Address");
			LinkedHashMap<String, String> shippingDetails1 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_alaska");
			Log.message("13. Shipping details filled in the shipping address form");
			List<String> indexes = new ArrayList<String>(shippingDetails1.keySet());
			String StateInAddress1 = indexes.get(5).replace("select_state_", "");
			String stateCode1 = StateUtils.getStateCode(StateInAddress1);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("15. Clicked on 'Continue' Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.clickaddLink();
			LinkedHashMap<String, String> shippingDetails2 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_california_address");
			Log.message("16. Shipping details filled in the shipping address form");
			List<String> index = new ArrayList<String>(shippingDetails2.keySet());
			String StateInAddress2 = index.get(5).replace("select_state_", "");
			String stateCode2 = StateUtils.getStateCode(StateInAddress2);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicked on 'Save' Button on Multi Shipping Address page !");
			// Clicking on 'Continue' button
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("18. Clicked on 'Continue' Button on Address Validation popup !");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("19. Clicked on 'Save' Button on Multi Shipping Address page !");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("20. First Item's address dropdown is selected !");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message("21. Second Item's address dropdown is selected !");
			shipToMultipleAddressPage.clickContinue();
			Log.message("22. Clicked on continue Button !");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue Button in shipping page !");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_APOFPO_address");
			Log.message("24. Billing details filled as address 'alaska' !");
			// filling card details
			LinkedHashMap<String, String> cardType = checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("25. Card details filled as cardType 'Visa' !");
			List<String> card = new ArrayList<String>(cardType.keySet());
			String cardTypeInBilling = card.get(0).replace("select_cardtype_", "");
			checkoutPage.clickOnContinueInBilling();
			Log.message("26. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("27. clicked PlaceOrderButton in placeOrder page !");
			String stateCode1InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(0).get("State");
			String stateCode2InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(1).get("State");
			String cardTypeInOrder = orderConfirmationPage.getPaymentMethodInOrderSummary().get("CardType");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address !",
					"Order didn't placed Successfully on added shipping address !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Added multiple shipping address APO/FPO and Hawaii should be properly displayed in Order confirmation screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(stateCode1.equals(stateCode1InOrder),
					"Added multiple shipping address  Alaska properly displayed in Order confirmation screen.",
					"Added multiple shipping address Alaska properly not displayed in Order confirmation screen.",
					driver);
			Log.assertThat(stateCode2.equals(stateCode2InOrder),
					"Added multiple shipping address  California properly displayed in Order confirmation screen.",
					"Added multiple shipping address California properly not displayed in Order confirmation screen.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message(
					"Added multiple shipping address with selected payment method 'Visa' should be properly displayed in Order Receipt screen");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.assertThat(cardTypeInBilling.equals(cardTypeInOrder),
					"Added multiple shipping address with selected payment method 'Visa' properly displayed in Order Receipt screen !",
					"Added multiple shipping address with selected payment method 'Visa' not displayed in Order Receipt screen !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_129

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify by confirm order with payment method 'Belk Reward Credit Card' and by adding different multiple address example New Jersey and New Mexico ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_131(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			pdppage.selectSize();
			Log.message("4.  Size is Selected from size dropdown !");
			pdppage.selectQtyByIndex(2);
			Log.message("5. Quantity drowdown is selected with Quantity '2' !");
			pdppage.clickAddToBag();
			Log.message("6. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. clicked on checkoutAsGuest Button !");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("10. clicked on yes Button for MultiShipping !");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on Add Link to add Address");
			LinkedHashMap<String, String> shippingDetails1 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_NewJersey_address");
			Log.message("13. Shipping details filled in the shipping address form");
			List<String> indexes = new ArrayList<String>(shippingDetails1.keySet());
			String StateInAddress1 = indexes.get(5).replace("select_state_", "");
			String stateCode1 = StateUtils.getStateCode(StateInAddress1);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("15. Clicked on 'Continue' Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.clickaddLink();
			LinkedHashMap<String, String> shippingDetails2 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_NewMexico_address");
			Log.message("16. Shipping details filled in the shipping address form");
			List<String> index = new ArrayList<String>(shippingDetails2.keySet());
			String StateInAddress2 = index.get(5).replace("select_state_", "");
			String stateCode2 = StateUtils.getStateCode(StateInAddress2);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicked on 'Save' Button on Multi Shipping Address page !");
			// Clicking on 'Continue' button
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("18. Clicked on 'Continue' Button on Address Validation popup !");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("19. Clicked on 'Save' Button on Multi Shipping Address page !");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("20. First Item's address dropdown is selected !");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message("21. Second Item's address dropdown is selected !");
			shipToMultipleAddressPage.clickContinue();
			Log.message("22. Clicked on continue Button !");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue Button in shipping page !");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_Hawaii_address");
			Log.message("24. Billing details filled as address 'alaska' !");
			// filling card details
			LinkedHashMap<String, String> cardType = checkoutPage.fillingCardDetails("NO",
					"card_BelkRewardsCreditCard");
			Log.message("25. Card details filled as cardType 'Visa' !");
			List<String> card = new ArrayList<String>(cardType.keySet());
			String cardTypeInBilling = card.get(0).replace("select_cardtype_", "").replace("s", "");
			checkoutPage.clickOnContinueInBilling();
			Log.message("26. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("27. clicked PlaceOrderButton in placeOrder page !");
			String stateCode1InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(0).get("State");
			String stateCode2InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(1).get("State");
			String cardTypeInOrder = orderConfirmationPage.getPaymentMethodInOrderSummary().get("CardType");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address !",
					"Order didn't placed Successfully on added shipping address !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Added multiple shipping address New Jersey and New Mexico should be properly displayed in Order confirmation screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(stateCode1.equals(stateCode1InOrder),
					"Added multiple shipping address New Jersey properly displayed in Order confirmation screen.",
					"Added multiple shipping address New Jersey properly not displayed in Order confirmation screen.",
					driver);
			Log.assertThat(stateCode2.equals(stateCode2InOrder),
					"Added multiple shipping address New Mexico properly displayed in Order confirmation screen.",
					"Added multiple shipping address New Mexico properly not displayed in Order confirmation screen.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message(
					"Added multiple shipping address with selected payment method 'Belk Reward Credit Card' should be properly displayed in Order Receipt screen");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.assertThat(cardTypeInBilling.equals(cardTypeInOrder),
					"Added multiple shipping address with selected payment method 'Belk Reward Credit Card' properly displayed in Order Receipt screen !",
					"Added multiple shipping address with selected payment method 'Belk Reward Credit Card' not displayed in Order Receipt screen !",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_131

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Order summary total is properly reduced when removing the added item from My bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_136(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// String searchKey[] = testData.get("SearchKey").split("\\|");

		String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 2 different products
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {
				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message("2. Searched with '" + searchKey + "'");
				Log.message("3. Navigated to PDP");
				// To add product for desktop
				if (Utils.getRunPlatForm() == "desktop") {
					pdpPage.addProductToBag();

				}
				// To add product for mobile
				if (Utils.getRunPlatForm() == "mobile") {

					pdpPage.addProductToBag();
					// To click on the mini cart pop_up to add another product
					// to the bag.
					pdpPage.clickOnMiniCart();
				}

			}
			// Navigate to shopping bag page after clicking on minicart
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("4. Navigated to Shopping bag page!");

			// Navigate to siginin page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("5. Clicked on Checkout in Order summary Page");

			// Clicking on checkout as guest
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("6. Clicked on Checkout As Guest User in Sign In Page");

			// Filling shipping details as guest user
			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("7. Entered Shipping Address Details as Guest User");

			// clicking on continue in shipping address page
			checkoutpage.clickOnContinueInShipping();
			checkoutpage.clickContinueInAddressValidationModal();
			Log.message("8. Clicked on Continue In Shipping Address Page as Guest User");

			// clicked on edit link in the order summary panel
			checkoutpage.clickOnEditBySectionName("ordersummary");
			Log.message("9. Clicked on Edit Link In Order Summary Panel.");

			// Getting the Estimated value before deleting the product from the
			// cart
			Float estimatedval1 = shoppingbagpage.getEstimatedValue();
			Log.message("10. The Estimated value is before deleting product: " + estimatedval1);

			// To remove the added product from the shopping bag page by index
			// value.
			shoppingbagpage.removeItemsByIntex(1);
			Log.message("11. Remove an added item in the shopping bag page.");
			// Getting the Estimated value after deleting the product from the
			// cart
			Float estimatedval2 = shoppingbagpage.getEstimatedValue();
			Log.message("12. The Estimated value is after deleting product: " + estimatedval2);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b> Estimated order total should be properly reduced in Order Summary panel when removing the item from shopping bag screen.");
			Log.softAssertThat(!estimatedval1.equals(estimatedval2),
					"<b> Actual Result: </b> Estimated order total is updated properly after removing the item from shopping bag screen. ",
					"<b> Actual Result: </b> Estimated order total is not updated properly after removing the item from shopping bag screen.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_136


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify order successfully placed with Same billing & shipping address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_144(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		double shipValue;
		String txtFree = "FREE";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);

			String color = pdpPage.selectColor();
			Log.message("3. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("4. selected size :" + size);

			pdpPage.clickAddToBag();
			Log.message("5. Click on Add to bag button");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. CheckoutAsGuest button is clicked");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address_virgenia", "Standard");
			Log.message("9. Shipping details filled ");

			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Continue shipping field is clicked");
			checkOutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("11. Continue button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address_virgenia");
			Log.message("12. Billing details filled ");
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("13. Card details filled");

			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Continue in billing field is clicked");
			BrowserActions.nap(2);
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message("15. Place order button is clicked");
			Log.message(
					"<b>Expected Result : </b> Order should be properly placed with same billing and shipping address.");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldEstimatedTotal"),
							orderconfirmationpage),
							"<b>Actual Result : </b> Order is properly placed with same billing and shipping address.",
							"<b>Actual Result : </b> Order is not properly placed with same billing and shipping address.",
							driver);
			String merchandiseTotal = orderconfirmationpage.getTextFromOrderSummary().replaceAll("\\$", "").trim();
			float merchandiseValue = Float.parseFloat(merchandiseTotal);
			Log.message("17.Got the text from merchandise Value and the value is :" + merchandiseValue);
			String shipment = orderconfirmationpage.getTextFromShipping();
			shipment = shipment.replaceAll("\\$", "");
			if (shipment.contains(txtFree)) {
				shipValue = 0.0;
			} else {
				shipValue = Double.parseDouble(shipment);
			}
			Log.message("18.Got the text from shipping Value and the value is:" + shipValue);
			String salesTax = orderconfirmationpage.getTextFromSalesTax();
			salesTax = salesTax.replaceAll("\\$", "");
			double salesTaxValue = Double.parseDouble(salesTax);
			Log.message("19. Got the text From Sales Tax and the value is :" + salesTax);
			String shipSurCharge = orderconfirmationpage.getTextFromShipSurCharge();
			shipSurCharge = shipSurCharge.replaceAll("\\$", "");
			double shipSurChargeValue = Double.parseDouble(shipSurCharge);
			Log.message("20. Got the text From shipSurCharge and the value is :" + shipSurChargeValue);
			String orderTotal = orderconfirmationpage.getTextFromOrderTotal().replaceAll("\\$", "");
			double orderTotalValue = Double.parseDouble(orderTotal);
			Log.message("21. Got the text From OrderTotal and the value is" + orderTotalValue);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> Estimated order total should properly calculated for the different billing and shipping address");
			Log.assertThat(orderTotalValue == merchandiseValue + salesTaxValue + shipValue + shipSurChargeValue,
					"<b>Actual Result : </b> Estimated order total is calculated properly",
					"<b>Actual Result : </b> Estimated order total is not calculated properly", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_144

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify order successfully placed with different billing & shipping address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_145(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		double shipValue;
		String txtFree = "FREE";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);

			String color = pdpPage.selectColor();
			Log.message("3. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("4. selected size :" + size);

			pdpPage.clickAddToBag();
			Log.message("5. Click on Add to bag button");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. CheckoutAsGuest button is clicked");

			// CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutasReg();
			checkOutPage.fillingShippingDetailsAsGuest("valid_address_virgenia", "Standard");
			Log.message("9. Shipping details filled ");

			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Continue shipping field is clicked");
			checkOutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("11. Continue button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_alaska");
			Log.message("12. Billing details filled ");
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("13. Card details filled");
			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Continue in billing field is clicked");
			BrowserActions.nap(2);
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message("15. Place order button is clicked");
			Log.message(
					"<b>Expected Result : </b> Order should be properly placed with different billing and shipping address.");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldEstimatedTotal"),
							orderconfirmationpage),
							"<b>Actual Result : </b> Order is properly placed with different billing and shipping address.",
					"<b>Actual Result : </b> Order is not properly placed with different billing and shipping address.");
			String merchandiseTotal = orderconfirmationpage.getTextFromOrderSummary().replaceAll("\\$", "").trim();
			double merchandiseValue = Double.parseDouble(merchandiseTotal);
			Log.message("16.Got the text from merchandise Value and the value is :" + merchandiseValue);
			String shipment = orderconfirmationpage.getTextFromShipping();
			shipment = shipment.replaceAll("\\$", "");
			if (shipment.contains(txtFree)) {
				shipValue = 0.0;
			} else {
				shipValue = Double.parseDouble(shipment);
			}
			Log.message("17.Got the text from shipping Value and the value is:" + shipValue);
			String salesTax = orderconfirmationpage.getTextFromSalesTax();
			salesTax = salesTax.replaceAll("\\$", "");
			double salesTaxValue = Double.parseDouble(salesTax);
			Log.message("18. Got the text From Sales Tax and the value is :" + salesTax);
			String shipSurCharge = orderconfirmationpage.getTextFromShipSurCharge();
			shipSurCharge = shipSurCharge.replaceAll("\\$", "");
			double shipSurChargeValue = Double.parseDouble(shipSurCharge);
			Log.message("19. Got the text From shipSurCharge and the value is :" + shipSurChargeValue);
			String orderTotal = orderconfirmationpage.getTextFromOrderTotal().replaceAll("\\$", "");
			double orderTotalValue = Double.parseDouble(orderTotal);
			Log.message("20. Got the text From OrderTotal and the value is" + orderTotalValue);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> Estimated order total should properly calculated for the different billing and shipping address");
			Log.assertThat(orderTotalValue == merchandiseValue + salesTaxValue + shipValue + shipSurChargeValue,
					"<b>Actual Result : </b> Estimated order total is calculated properly for the different billing and shipping address",
					"<b>Actual Result : </b> Estimated order total is not calculated properly for the different billing and shipping address",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_145

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Sale (Now/Clearance) Price display in Mini cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_150(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");
			pdpPage.mouseOverMiniCart();
			MiniCartPage miniCartPage = new MiniCartPage(driver).get();
			Log.message("7. Naviagte to Shopping bag", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> Order subtotal should be displayed properly");
			Log.assertThat(miniCartPage.elementLayer.verifyPageElements(Arrays.asList("ordertotal"), miniCartPage),
					"<b>Actual Result: </b> Order total is properly displyed",
					"<b>Actual Result: </b> Order total is not properly displyed");
			Log.testCaseResult();

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // checkout

	}// TC_BELK_CHECKOUT_150

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Order subtotal is displayed in Mini cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_151(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");
			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");

			if (Utils.getRunPlatForm() == "mobile") {
				pdpPage.clickOnMiniCartIcon();
			} else if (Utils.getRunPlatForm() == "desktop") {
				pdpPage.mouseOverMiniCart();
			}
			Log.message("7. Navigated to Mini cart");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> Order total should properly displyed");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("ordertotalinMiniCart"), pdpPage),
					"<b>Actual Result: </b> Order total is properly displyed",
					"<b>Actual Result: </b> Order total is not properly displyed");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_151

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Sale Product Discount Savings value is displayed above View Bag button in Mini Cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_152(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");
			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");

			if (Utils.getRunPlatForm() == "mobile") {
				pdpPage.clickOnMiniCartIcon();
			} else if (Utils.getRunPlatForm() == "desktop") {
				pdpPage.mouseOverMiniCart();
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // checkout

	}// TC_BELK_CHECKOUT_152

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether the product discount savings value does not take into consideration any coupons or order level discounts applied to the cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_153(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");
			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");

			if (Utils.getRunPlatForm() == "mobile") {
				pdpPage.clickOnMiniCartIcon();
			} else if (Utils.getRunPlatForm() == "desktop") {
				pdpPage.mouseOverMiniCart();
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_CHECKOUT_153

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether a regular and sale price, an Original and Now or Clearance price are displayed for a product if the product has a price record in one or more active pricebooks.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_157(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page of selected product");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");
			pdpPage.selectQuantity();
			Log.message("6. Select the Quantity");
			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");
			Log.message("<br>");

			Log.message("<b> Expected Result 1a: </b> Order Sub Total should be properly displayed in Cart screen.");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("", ""), shoppingBagPage),
					"<b>Actual Result 1a: </b>\"Estimated Sub Total\" is displayed in the Shopping Bag page.",
					"<b>Actual Result 1a: </b>\"Estimated Sub Total\" is not displayed in the Shopping Bag page.",
					driver);
			Log.message("<br>");
			shoppingBagPage.verifyProductPrice();
			Log.message("<br>");
			Log.message("<b> Expected Result 1b: </b>  Product pricing should be highlighted in red color.");
			Log.message("<b> Actual Result 1b: </b> Product Pricing should be highlighted with the red colors.",
					driver);
			Log.message("<br>");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PRICE_157

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify product pricing is properly displayed in Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_160(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");
			pdpPage.selectQuantity();
			Log.message("6. Select the Quantity");
			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");
			String shoppingCartPrice = shoppingBagPage.productPricingShoopingCart();
			Log.message("9. The Red ColoredShopping Price is: " + shoppingCartPrice);
			Log.message("<br>");

			Log.message("<b> Expected Result 1a: </b> Product pricing should be properly displayed in Cart page");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("productPricing"), shoppingBagPage),
					"<b>Actual Result 1a: </b>\"Product Pricing\" is displayed in the shopping cart.",
					"<b>Actual Result 1a: </b>\"Product Pricing\" is not displayed in the shopping cart.", driver);
			Log.message("<br>");
			shoppingBagPage.verifyProductPrice();
			Log.message("<br>");
			Log.message("<b> Expected Result 1b: </b>	 Product pricing should be highlighted in red color.");
			Log.message("<b> Actual Result 1b: </b> Product Pricing should be highlighted with the red colors.",
					driver);
			Log.message("<br>");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PRICE_160

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether the aggregate subtotal of the items in the cart (item price + adjustments) x qty is displayed near 'Subtotal' text per line item.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_163(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] searchKey = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = null;
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				// Searching a product with product keyword
				SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey[searchIndex]);
				Log.message("2. Searched with keyword  and navigated to search result Page");
				pdpPage = searchResultPage.navigateToPDP();
				Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

				// Select the size,color & qty.
				// To add product for desktop
				if (Utils.getRunPlatForm() == "desktop") {
					String selectedSize = pdpPage.selectSize();
					Log.message("4. Selected size: '" + selectedSize + "' from the PDP page");

					String colorName = pdpPage.selectColor();
					Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

					pdpPage.selectQuantity();
					Log.message("6. Select the Quantity");

					pdpPage.clickAddToBag();
				}
				// To add product for mobile
				if (Utils.getRunPlatForm() == "mobile") {

					String selectedSize = pdpPage.selectSize();
					Log.message("4. Selected size: '" + selectedSize + "' from the PDP page");

					String colorName = pdpPage.selectColor();
					Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");
					pdpPage.selectQuantity();

					Log.message("6. Select the Quantity");
					pdpPage.clickAddToBag();
					// To click on the mini cart pop_up to add another product
					// to the bag.
					pdpPage.clickOnMiniCart();
				}

			}
			// Navigating to 'ShoppingBag' Page

			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("7. Click on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			LinkedHashMap<String, String> orderSummaryDetails = shoppingbagpage.getOrderSummaryDetails();
			Log.message("8. The aggregate subtotal of the items is: " + orderSummaryDetails);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> The aggregate subtotal of the items in the cart (item price + adjustments) x qty should be displayed near 'Subtotal' text per line item.");
			Log.assertThat(
					shoppingbagpage.elementLayer.verifyPageElements(
							Arrays.asList("lblOrderMerchandiseTotal", "lblOrderShippingAmount"), shoppingbagpage),
							"<b>Actual Result 1: </b>\"Estimated Sub Total\" is displayed in the Shopping Bag page of the items in the cart (item price only, without tax or shipping) and the ammount is "
									+ orderSummaryDetails,
									"<b>Actual Result 1: </b>\"Estimated Sub Total\" is not displayed in the Shopping Bag page.",
									driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_163.

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether the Subtotal per line item does not include tax or shipping.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_164(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String currentPdpURL = driver.getCurrentUrl();
			Log.message("4. Navigated to the 'PDP' and the URL of the PDP is :" + currentPdpURL);
			String standardPDPPrice = pdpPage.getStandardPrice();
			Log.message("5. The standard Product price is:" + standardPDPPrice);
			String selectedSize = pdpPage.selectSize();
			Log.message("6. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("7. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdpPage.clickAddToBag();
			Log.message("8. Add the Product to the bag");
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("9. Naviagte to Shopping bag");
			String currentShoppingCartPageURL = driver.getCurrentUrl();
			Log.message("10. Navigated to the 'Shopping Bag Page' and the URL of the Shopping Bag Page is :"
					+ currentShoppingCartPageURL);
			String estimatedShoppingBagPrice = shoppingBagPage.getStandardSubtotalPrice();
			Log.message("11. The estimated Product price is: " + estimatedShoppingBagPrice);
			Log.message("<br>");

			Log.message("<b> Expected Result 1: </b> The Subtotal per line item should not include tax or shipping.");
			Log.assertThat(standardPDPPrice.equals(estimatedShoppingBagPrice),
					"<b> Actual Result 1: <b> The 'Subtotal' per line item should not include 'tax or shipping'.",
					"<b> Actual Result 1: <b> The 'Subtotal' per line item should include 'tax or shipping'.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_164


	
	@Test(groups = { "desktop","mobile" }, description = "Verify Ability to display messaging at an item level, if item is added but not claimed for GWP/PYG /BOGO. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_210(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			//Select the Product

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			Log.message("3. Navigated to PDP Page ");
			pdpPage.selectSize();
			Log.message("4.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(3);
			Log.message("5. Quantity drowdown is selected with Quantity 3");
			pdpPage.clickAddToBag();
			Log.message("6. Add to cart button is Clicked");
			pdpPage.clickOnTocloseInPopUp();
			Log.message("7. Close the free gift item");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to Cart page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Add Your free Gift message should be displayed in Cart page");
			
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddFreeGiftToBag"), shoppingBagPage),
							"<b>Actual Result 1: </b> Navigated to Shopping Bag Page and Add Your Free Gift  message is displayed if Free gift is not added on the PDP page  "
								,"<b>Actual Result 1: </b> Navigated to Shopping Bag Page and Add Your Free Gift message is not displayed .",
									driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_210

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for GWP/PYG/BOGO. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_211(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			Log.message("6. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSize();
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(4);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			
			
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b> Free Gift With Purchase Added message should be displayed in cart page");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_211



	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level if item is added but not claimed for PWP. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_213(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("6. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message("<b>Expected Result:</b> Get a $10 Mascara with an Beauty purchase over $35 View Details");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_213

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_214(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("6. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message("<b>Expected Result:</b> Bonus Offer Pricing should be Reflected in Cart page");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_214

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_215(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			pdpPage.selectSizeByIndex(2);
			Log.message("6. Navigated to PDP Page from Search Results Page");
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b> Get a $10 Mascara with an Beauty purchaseover $35 View Details message should be displayed in Cart Page");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_215

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_216(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("6. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message("<b>Expected Result:</b> Get a $10 Mascara with an Beauty purchase over $35 View Details");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_216

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_217(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("6. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(2);
			Log.message("7.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("8. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("9. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b> Get a $10 Mascara with an Beauty purchase over $35 View Details message should be displayed in Cart Page");

			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("11. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_217

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_218(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			Log.message("6. Navigated to Search Results Page");

			Log.message("<b>Expected Result:</b> Buy Amount and item and in Promo phrase GET <M> <ITM> FREE in PLP");

			Log.message("No test Data to proceed with the validation");

			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("7. Navigated to PDP Page from Cart Page");
			Log.message("<b>Expected Result:</b> Buy Amount and item and in Promo phrase GET <M> <ITM> FREE in PDP");
			Log.message("No test Data to proceed with the validation");
			pdpPage.selectSizeByIndex(2);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("10. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Navigated to Cart page");
			Log.message("<b>Expected Result:</b> Buy Amount and item and in Promo phrase GET <M> <ITM> FREE in PLP");
			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("12. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_218

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_219(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			Log.message("6. Navigated to Search Results Page");

			Log.message(
					"<b>Expected Result:</b> BUY <AMT> <ITM> , GET <M> <ITM> FREE, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get 1 item Free");

			Log.message("No test Data to proceed with the validation");

			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("7. Navigated to PDP Page from Cart Page");
			Log.message(
					"<b>Expected Result:</b> BUY <AMT> <ITM> , GET <M> <ITM> FREE, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get 1 item Free");
			Log.message("No test Data to proceed with the validation");
			pdpPage.selectSizeByIndex(2);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("10. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b> BUY <AMT> <ITM> , GET <M> <ITM> FREE, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get 1 item Free");
			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("12. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_219

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify possible promo and quantifier phrases from PMF", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_220(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// myAccountPage.miniCartPage.removeAddedCartItems();
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PlpPage plp = new PlpPage(driver);
			Log.message("6. Navigated to Search Results Page");
			plp.getTextPromoMessage();
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("7. Navigated to PDP Page from Cart Page");


			pdpPage.selectColor();
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("10. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b>  BUY <AMT> <ITM>,GET <DSC>%OFF <M> <ITM>, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get % of off on the item");
			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("12. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_220

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify ability to display messaging at an item level, if item is added and claimed for PWP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_221(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			Log.message("6. Navigated to Search Results Page");

			Log.message(
					"<b>Expected Result:</b>  BUY <AMT> <ITM>,GET <DSC>%OFF <M> <ITM>, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get % of off on the item");
			Log.message("No test Data to proceed with the validation");
			PdpPage pdpPage = searchResultPage.selectProductByIndex(1);
			Log.message("7. Navigated to PDP Page from PLP Page");
			Log.message(
					"<b>Expected Result:</b>  BUY <AMT> <ITM>,GET <DSC>%OFF <M> <ITM>, Buy 1 Under Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, get % of off on the item");

			Log.message("No test Data to proceed with the validation");
			pdpPage.selectSizeByIndex(2);
			Log.message("8.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Quantity drowdown is selected with Quantity 2");
			pdpPage.clickAddToBag();
			Log.message("10. Add to cart button is Clicked");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Navigated to Cart page");
			Log.message(
					"<b>Expected Result:</b>  SPEND $<AMT> ON <ITM> GET <M> <ITM> FREE, Spend $ Amount on Armour® Undeniable Stars & Stripes Athletic Crew Socks Single Pair, Get an item FREE");
			Log.message("No test Data to proceed with the validation");
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.message("10. Items in cart removed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_221

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays 'Coupon message' in the Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_015(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String couponCode = testData.get("CouponCode");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Navigated to 'Shopping bag !");

			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("7. Entered Coupon Code and clicked on 'Apply coupon' button");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>  Success message should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("couponMessage"), shoppingBagPage),
					"<b> Actual Result 1: </b> Coupon success message is displayed",
					"<b> Actual Result 1: </b> Coupon success message is not displayed.", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>  Tick Mark should be displayed before the success message");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("couponTickMark"), shoppingBagPage),
					"<b> Actual Result 2: </b> Tick Mark is displayed before the success message",
					"<b> Actual Result 2: </b> Tick Mark is not displayed before the success message", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>  Tick Mark should be Green color");

			Log.assertThat((shoppingBagPage.verifyGlobalErrorMessageColour()),
					"<b> Actual Result 3: </b> Tick Mark is in Green color",
					"<b> Actual Result 3: </b> Tick Mark is not Green color", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b>  Coupon applied section should have ':' symbol");

			Log.assertThat((shoppingBagPage.getCouponText().contains(":")),
					"<b> Actual Result 3: </b> Coupon applied section contains ':' symbol",
					"<b> Actual Result 3: </b> Coupon applied section does not contains ':' symbol", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_015

	// TC_BELK_CHECKOUT_022
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the Edit link in Payment Method Section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_022(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			;
			Log.message("6. Navigated to 'Mini Cart' !");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("7. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message("9. Entered Shipping address as guest user");
			checkOutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button in Shipping address Page");
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("13. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("14. Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on 'Continue' Button in Billing Page");

			Log.message("<br>");
			Log.message("<b> Expected Result 1:</b> Edit link should be displayed in order summary section");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lnkOrdersummaryEdit"), checkOutPage),
					"<b>Actual Result 1:</b> Edit link is displayed in order summary section",
					"<b>Actual Result 1:</b> Edit link is not displayed in order summary section", driver);

			checkOutPage.clickOrderSummaryEditLink();
			Log.message("16. Navigated to place order page and clicked on edit link");

			Log.message("<br>");
			Log.message("<b> Expected Result 1:</b> Page should be redirected to Shopping bag page");

			Log.assertThat(checkOutPage.elementLayer.verifyPageElements(Arrays.asList("drpQuantity"), checkOutPage),
					"<b>Actual Result 1:</b> Page is redirected to Shopping bag page",
					"<b>Actual Result 1:</b> Page is not redirected to Shopping bag page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_022

	
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the cart message for Free Shipping.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_013(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty + " from the PDP page");

			String txtMessageFreeShipping = pdpPage.getTextFreeShipping();
			Log.message("7. The Free Shipping message on the PDP is: " + txtMessageFreeShipping);

			pdpPage.clickAddToBag();
			Log.message("8. Click on the 'Add To Bag' button and the Product add to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("9. Navigate to the Shopping bag");

			String verifyTheFreeShippingOnMyBag = shoppingBagPage.verifyFreeShippingMessage();
			Log.message(
					"10. Navigate to Shopping bag and the Free Shipping message is : " + verifyTheFreeShippingOnMyBag);

			Log.message("<br>");

			// To run in Mobile Or Desktop Platform
			String runPltfrm = Utils.getRunPlatForm();

			if (runPltfrm == "mobile") {
				Log.message("<b> Expected Result 1: </b> The Free shipping Cart message should be displayed.");
				Log.assertThat(
						shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("freeShippingMessageBagMobile"),
								shoppingBagPage),
								"<b>Actual Result 1: </b>\"Free shipping Cart\" message is displayed in the Shopping Bag page and the message is:"
										+ verifyTheFreeShippingOnMyBag,
										"<b>Actual Result 1: </b>\"Free shipping Cart\" message is not displayed in the Shopping Bag page.",
										driver);
			}
			if (runPltfrm == "desktop") {
				Log.message("<b> Expected Result 1: </b> The Free shipping Cart message should be displayed.");
				Log.assertThat(
						shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("freeShippingMessageMyBag"),
								shoppingBagPage),
								"<b>Actual Result 1: </b>\"Free shipping Cart\" message is displayed in the Shopping Bag page and the message is:"
										+ verifyTheFreeShippingOnMyBag,
										"<b>Actual Result 1: </b>\"Free shipping Cart\" message is not displayed in the Shopping Bag page.",
										driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_013

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether Subtotal for Free gifts displays as 0.00.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_166(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. Select the Quantity from the PDP page");

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			pdpPage.selectFreeGiftItem();
			Log.message("8. Select the free gift from the Pop Up");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page.");

			String freegiftAdded = shoppingBagPage.freeGiftAddedMessage();
			Log.message("10. The free gift added product message is: " + freegiftAdded);
			String standardprice = shoppingBagPage.getStandardSubtotalPriceFreeAddedGift();
			Log.message("11. The Standard Price are  " + standardprice);

			Log.message("<br>");

			Log.message("<b> Expected Result 1: </b> Subtotal for Free gifts should display as $0.00.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtSubtotalspanOfFreeGift"),
							shoppingBagPage),
							"<b>Actual Result 1: </b>\" SubTotal\" is displayed in the Shopping Bag page for the free gift and the amount is "
									+ standardprice,
									"<b>Actual Result 1: </b>\" SubTotal\" is not displayed in the Shopping Bag page.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PRICE_166

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether Free gifts does not display a Original or Sale price at the line item.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_165(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. Select the Quantity from the PDP page");

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			pdpPage.selectFreeGiftItem();
			Log.message("8. Select the free gift from the Pop Up");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page.");

			String freegiftAdded = shoppingBagPage.freeGiftAddedMessage();
			Log.message("10. The free gift added product message is: " + freegiftAdded);

			Log.message("<br>");

			Log.message(
					"<b> Expected Result 1: </b> Verify whether Free gifts does not display a Original or Sale price at the line item.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lblFreeGift"), shoppingBagPage),
					"<b>Actual Result 1: </b>\"Free gifts\" is displayed, but original and sale price is not displayed in the Shopping Bag page ",
					"<b>Actual Result 1: </b>\"Free gifts\" is not displayed,but original and sale price is displayed in the Shopping Bag page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_165

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify user can able select saved address from billing or shipping address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_133(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered valid credentiand navigated to 'My Account' page");

			ShoppingBagPage bagPage = myaccount.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			bagPage.clickOnMyAccountLink();

			// Navigated to My Account Page
			myaccount.navigateToMyAddressBook();
			Log.message("4. Navigated to My Account Page!");

			// Navigated to Address Book Link
			myaccount.clickOnAddAddress();
			Log.message("5. Navigated to Add Address In My Account Page!");

			// Filling Address Details
			AddressBookPage AddressBookPage = new AddressBookPage(driver).get();
			LinkedHashMap<String, String> shippingDetails = AddressBookPage.fillingAddNewAddressDetails(address, "No",
					"No");
			List<String> indexes = new ArrayList<String>(shippingDetails.keySet());
			String valueInMyAddressBook = indexes.get(5).replace("type_city_", "");
			Log.message("6. Address Fiiled Successfully!");

			// Clicking On Save Button to Save address
			AddressBookPage.ClickOnSave();
			AddressBookPage.ClickOnContinueInValidationPopUp();
			Log.message("7. Clicked On Save Address Button!");

			// Navigated to Search Result Page
			SearchResultPage searchResultpage = AddressBookPage.searchProductKeyword(searchKey);
			Log.message("9. Navigated to Search Result Page!");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("10. Navigated to PDP Page !");

			// Select Size, Color, Qty
			String color = pdpPage.selectColor();
			String size = pdpPage.selectSize();
			Log.message("11. Selected Size,Colour and Quantity!");

			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("12. Product added to Bag!");

			// Navigating to shopping bag
			MiniCartPage miniCartPage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingBagPage = miniCartPage.navigateToBag();
			Log.message("13. Navigated to shopping bag page!");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("14. Navigate to checkout page!");

			// Selecting Saved Address From Drop Down
			LinkedHashMap<String, String> shippingDetails1 = checkoutPage.fillingShippingDetailsAsSignedInUser("NO",
					"No", "Standard", "valid_address5");
			List<String> indexes1 = new ArrayList<String>(shippingDetails1.keySet());
			String valueInShippingPage = indexes1.get(4).replace("type_city_", "");
			Log.message("15. Shipping Address Filled Successfully!");

			// Clicking Continue Button in Shiiping Page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Clicked on Continue in the Shipping Page!");

			// Selecting Saved Address From Drop Down
			LinkedHashMap<String, String> shippingDetails2 = checkoutPage.fillingBillingDetailsAsSignedInUser("NO",
					"NO", "valid_address5");
			List<String> indexes2 = new ArrayList<String>(shippingDetails2.keySet());
			String valueInBillingPage = indexes2.get(4).replace("type_city_", "");
			Log.message("17. Billing Address Filled Successfully!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("18. Card details filled in the billing form!");

			// Filling all the details in the billing page
			checkoutPage.clickOnContinueInBilling();
			Log.message("19. Clicked on Continue in the Shipping Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1: </b> Saved address should be properly displayed in Shipping Address screen ");
			Log.assertThat(valueInMyAddressBook.equalsIgnoreCase(valueInShippingPage),
					"<b> Actual Result 1: </b> Saved addresss in Address book is displayed Same in Shipping Adress Screen",
					"<b> Actual Result 1: </b> Saved addresss in Address book is not displayed Same in Shipping Adress Screen",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2: </b> Saved address should be properly displayed in Billing Address screen ");
			Log.assertThat(valueInMyAddressBook.equalsIgnoreCase(valueInBillingPage),
					"<b> Actual Result 2: </b> Saved addresss in Address book is displayed Same in Billing Address Screen ",
					"<b> Actual Result 2: </b> Saved addresss in Address book is not displayed Same in Billing Adress Screen",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_133


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify free shipping message is properly displayed above the 'Add to Shopping Bag' button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_203(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to searchresultpage from homepage
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Select 'Colour'
			String selectedColor = pdpPage.selectColor();
			Log.message("4. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Select 'Size'
			String selectedSize = pdpPage.selectSize();
			Log.message("5. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Select 'Quantity'
			String selectedQuantity = pdpPage.selectQuantity();
			Log.message("6. Selected Quantity:'" + selectedQuantity + "' from the size drop down  in the PDP Page");

			// clicked on 'Add to Bag'
			pdpPage.clickAddToBag();
			Log.message("7. By Clicking on 'AddToShopping' Bag , the Product added to 'Shopping Bag'");

			String Text = pdpPage.getTextFreeShipping();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Above the Add to Shopping Bag button 'Free Shipping message' should be displayed in PDP screen.");

			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtFreeShipping"), pdpPage),
					"<b> Actual Result : </b> 'Free Shipping message' displayed in PDP screen" + Text,
					"<b> Actual Result : </b> 'Free Shipping message' not displayed in PDP screen ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_203

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Order summary total is properly reduced when removing the added item from My bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_126(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to searchresultpage from homepage
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			// Select 'Colour'
			String selectedColor = pdpPage.selectColor();
			Log.message("4. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Select 'Size'
			String selectedSize = pdpPage.selectSize();
			Log.message("5. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Select 'Quantity'
			String selectedQuantity = pdpPage.selectQuantity();
			Log.message("6. Selected Quantity:'" + selectedQuantity + "' from the size drop down  in the PDP Page");

			// clicked on 'Add to Bag'
			pdpPage.clickAddToBag();
			Log.message("7. By Clicking on 'AddToShopping' Bag , the Product added to 'Shopping Bag'");

			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("9. Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");

			checkoutPage.fillingShippingDetailsAsGuest("inValid_address2", "Standard");
			Log.message("11. Entered Shipping Address Details as Guest User with invalid ZipCode");

			// clicked on 'Continue' in shipping
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseSuggestedAddressInModal();
			Log.message("12. Selected 'Suggested' address.");

			// getting suggested address 'ZipCode' from popup
			String shippingAddress1 = checkoutPage.GetShippingAddressInPopUp();
			Log.message("  Suggested Address :" + shippingAddress1);

			// Clicked on 'Continue' in address modal
			checkoutPage.clickContinueInAddressValidationModal();
			// Clicked on 'Continue' in shipping
			checkoutPage.clickOnContinueInShipping();

			// filled billing details as Guest
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("13. Billing details filled as a Guest User.");

			// filled card details
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details filled in the Billing page.");
			checkoutPage.clickOnContinueInBilling();

			// Navigate to 'Order Summary' page
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();

			// getting shipping address from order confirmation page
			String shippingAddress2 = orderconfirmationpage.GetShippingAddress();
			Log.message("  Suggested Shipping Addess :" + shippingAddress2);
			Log.message("<br>");

			Log.message(
					"<b>Expected Result :</b> The Shipping Address selected in the QAS Modal should be shown in Order Receipt.");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && (shippingAddress1.equals(shippingAddress2)),
					"<b> Actual Result :</b> Suggested Shipping Address displayed in Order Corfirmation page.",
					"<b> Actual Result :</b> Suggested Shipping Address not displayed in Order Corfirmation page. ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_126

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify estimated order total should be properly displayed in Place order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_137(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// String searchKey[] = testData.get("SearchKey").split("\\|");

		String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 2 different products like jeans and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {
				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message("2. Searched with '" + searchKey + "'");
				Log.message("3. Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message("4. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message("6. Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message("7. Add the Product to the bag");

			}
			// Navigated to shoppingbag page after clicking on minicart icon in
			// pdp page
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping bag' page. ");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("9. Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");

			// Filling the shipping details as a GuestUser
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Entered Shipping Address Details as Guest");

			// clicking on continue in shipping page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.clickContinueInAddressValidationModal();
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue in Shipping Address Details as Guest");

			// Filling billing address details as a guest user
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("13. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details are fillied in the billing page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue in Billing page! ");

			// Getting Estimated order value from the place order page.
			String orderval1 = checkoutPage.GetOrderTotalValue();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Estimated order total amount should be properly displayed in place order screen");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabPlaceOrder"), checkoutPage)
					&& (checkoutPage.elementLayer.verifyPageElements(Arrays.asList("orderValue"), checkoutPage)),
					"<b> Actual Result 1:</b> Estimated order total is displayed properly in place order screen "
							+ orderval1,
					"<b> Actual Result 1:</b> Estimated order total is not displayed properly in place order screen ");
			Log.message("<br>");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Order should be placed when clicking place order button from place order screen");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderconfirmationpage.GetOrderNumber();

			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderconfirmationpage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"), orderconfirmationpage),
					"<b> Actual Result 2: </b> Order placed after clicking on place order button and Order number is generated: "
							+ ordernumb,
							"<b> Actual Result 2: </b> Order is not placed after clicking on place order button. ", driver);
			Log.message("<br>");

			Log.message("<br>");
			String orderval2 = orderconfirmationpage.GetOrderTotalValue();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Estimated order total should be properly displayed in Order confirmation screen.");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("orderValue"),
							orderconfirmationpage),
							"<b> Actual Result 3:</b> Estimated order total is displayed properly in Order confirmation screen "
									+ orderval2,
					"<b> Actual Result 3:</b> Estimated order total is not displayed properly in Order confirmation screen");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_137



	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the Order Summary details in Cart page(Shopping Bag with Products) for a Signed-In User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_026(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// ** Loading the test data from excel using the test case id */

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("5. Navigated to 'Pdp' Page with randomly selected product");

			String color = pdpPage.selectColor();
			Log.message("6. Selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("7. Selected size :" + size);

			pdpPage.clickOnRegistrySign();
			Log.message("8. Clicked on 'Add to wishlist' button");
			homePage.headers.navigateToGiftRegistry();
			Log.message("9. Clicked on registry link from header !");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			// click on viewRegistry
			RegistryInformationPage registryInformationPage = registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("10. Clicked on view Active Registry !");
			registryInformationPage.clickOnAddtoBag();
			pdpPage.clickOnMiniCart();
			Log.message("11. Minicart icon is clicked and navigated to Shopping bag page");
			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("12. Clicked 'Checkout' button and naviagte to checkout page");
			checkoutpage.fillingShippingDetailsAsSignedInUser("YES", "NO", "Standard", "valid_address2");
			checkoutpage.clickOnContinueInShipping();
			Log.message("13. 'Continue button' in shipping address page is clicked");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			LinkedHashMap<String, String> shipAddressBeforeEdit = checkoutpage.getShippingAddress();
			Log.message("14. Continue button is clicked in popup modal and navigated to billing page");
			BrowserActions.nap(2);
			Log.message("<br>");
			Log.message("<b>Expected Result-1a :</b> Registry Name should displayed properly");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblRegistryName"), checkoutpage),
					"<b>Actual Result-1a :</b> Registry name is displayed properly",
					"<b>Actual Result-1a :</b> Registry name is not displayed properly");
			Log.message("<br>");
			Log.message("<b>Expected Result-1b :</b> Shipping Method | Shipping Cost should displayed properly");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblShipmentMethod"), checkoutpage),
					"<b>Actual Result-1b :</b> Shipment method and cost is displayed properly",
					"<b>Actual Result-1b :</b> Shipment method and cost is not displayed properly");
			Log.message("<br>");
			Log.message("<b>Expected Result-2 :</b> Edit link should be displayed");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageListElements(Arrays.asList("editOrderSummary"), checkoutpage),
					"<b>Actual Result-2 :</b> Edit Link in shipping page is displayed properly",
					"<b>Actual Result-2 :</b> Edit Link in shipping page is not displayed properly");
			checkoutpage.clickOnEditBySectionName("shippingaddress");
			Log.message("<br>");
			Log.message("15. Edit link is clicked");
			checkoutpage.fillingShippingDetailsAsSignedInUser("YES", "NO", "Standard", "valid_address3");
			Log.message("16. Edited shipping method");
			checkoutpage.clickOnContinueInShipping();
			Log.message("17. 'Continue button' in shipping address page is clicked");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			LinkedHashMap<String, String> shipAddressAfterEdit = checkoutpage.getShippingAddress();
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> user should be able to edit the Shipping address details");
			Log.assertThat(!shipAddressAfterEdit.equals(shipAddressBeforeEdit),
					"<b>Actual Result-3 :</b> Address is edited properly",
					"<b>Actual Result-3 :</b> Address is not edited properly");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_026



	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether the price range is displayed for a product in cart if the product is a variation master that has SKUs with multiple price values", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_156(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String PriceFirstProduct = pdpPage.getProductPriceFromPdp();
			Log.message("4. Navigate to the Product and Price of the product before variation "+PriceFirstProduct);
			pdpPage.selectSizeByIndex(3);
			Log.message("5. Selected size from the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("6. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdpPage.selectQuantity();
			Log.message("7. Select the Quantity");

			String colorName1 = pdpPage.selectColor();
			Log.message("8. Selected color: '" + colorName1 + "' from color swatch in the PDP page");

			Log.message("9. Selected size from the PDP page");
			String PriceSecondProduct = pdpPage.getProductPriceFromPdp();
			Log.message("10. Navigate to the Product and Price of the product after variation "+PriceSecondProduct);
			pdpPage.clickAddToBag();
			Log.message("11. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("12. Naviagte to Shopping bag");
			String FinalValue = shoppingBagPage.getProductPriceFromshoppingBagpage();

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1a: </b> The price for that specific UPC should be displayed after the variation is selected in PDP Page!");
			Log.assertThat(!(PriceFirstProduct.equals(PriceSecondProduct)),
					"<b>Actual Result 1a: The price of the product is not equal after the variation is selected",
					"<b>Actual Result 1a: The price of the product is equal after the variation is selected", driver);

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1b: </b> The price for that specific UPC should be displayed and price should be same in PDP and Shopping cart page!");
			Log.assertThat(FinalValue.equals(PriceSecondProduct),
					"<b>Actual Result 1b: The Price in Pdp and Shopping bag page is equal",
					"<b>Actual Result 1b: The Price in Pdp and Shopping bag page is not equal", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PRICE_156

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether Each charge / credit line is considering the quantity and display the total amount added or subtracted from the product’s price.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_162(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category(" + category[0] + ") -> L2-Category("
					+ category[1] + ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

			String color = pdpPage.selectColor();
			Log.message("4. Selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("5. Selected size :" + size);

			pdpPage.selectQuantity();
			Log.message("6. Select the Quantity");
			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_162






	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify shipping method Standard: 0.00 is calculated with added item in My Bag screen, when estimated total is 99 or greater - Registered User", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_080(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");
			// Get the Shipping Cost in Shipping Address Page
			String ShippingCostFrmOrderSmmary = checkoutpage.shippingCostInOrderSummary();
			Log.message("13.Shipping Cost in Order Summary in Shipping Address Page :" + ShippingCostFrmOrderSmmary);

			Log.message(
					"<b>Expected Result1: </b> Shipping Cost in Order Summary of Shipping address page for standard shipment method should  be '$0.00'");
			Log.assertThat((ShippingCostFrmOrderSmmary.equals("$0.00")),
					"<b>Actual Result1: </b> Shipping Cost in Order Summary of Shipping address page for standard shipment method is '$0.00'",
					"<b>Actual Result1: </b> Shipping Cost in Order Summary of Shipping address page for standard shipment method is not '$0.00'");

			Log.message(
					"<b>Expected Result2: </b> Shipping Method in Shipping address page should be selected as standard shipment method");
			Log.assertThat((checkoutpage.getSelectedRadioTextShippingMethod().equals("Ground")),
					"<b>Actual Result2: </b> Shipping Method in Shipping address page is selected as standard shipment method",
					"<b>Actual Result2: </b> Shipping Method in Shipping address page is not selected as standard shipment method");

			// Click on Continue in Shipping
			checkoutpage.clickOnContinueInShipping();

			checkoutpage.chooseOriginalAddressInModal();
			Log.message("15.Selected Original address in Modal");

			// Validate continue button in Shipping address page.
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("16.Entered Billing Address Details as Registered User");
			// Enter Card Details
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("17.Entered Card Details ");
			// Click on Continue in Billing
			checkoutpage.clickOnContinueInBilling();
			Log.message("18.Clicked on Continue Billing Page");

			Log.message("<b>Expected Result3: </b> Page should be redirected to Place order page");

			if (Utils.getRunPlatForm().equals("mobile"))

				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutpage),
								"<b>Actual Result3: </b> Page is redirected to Place order page",
								"<b>Actual Result3: </b>  Page is not redirected to Place order page", driver);
			else
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutpage),
								"<b>Actual Result3: </b> Page is redirected to Place order page",
								"<b>Actual Result3: </b> Page is not redirected to Place order page", driver);

			// Get the Shipping Method in Place Order Screen
			String shippingMethodInPlaceOrder = checkoutpage.shippingMethodInPlaceOrder();

			// Get shipping cost in place order screen
			String shippingCostInPlaceOrder = checkoutpage.shippingCostInPlaceOrder();
			// Click on Place Order
			OrderConfirmationPage orderConfirmationPage;

			if (Utils.getRunPlatForm().equals("desktop")) {

				orderConfirmationPage = checkoutpage.placeOrder();

				Log.message("19.Clicked on place Order Page");

				Log.message("<b>Expected Result4: </b>Page should be redirected to Order receipt page");

				Log.assertThat(
						orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("btnReturnToShopping"),
								orderConfirmationPage),
								"<b>Actual Result4: </b> Page is redirected to Order receipt page",
								"<b>Actual Result4: </b> Page is not redirected to Order receipt page", driver);

				Log.message(
						"<b>Expected Result5:</b>Shipping Method in Order Receipt should be standard Shipment Method");
				Log.assertThat((shippingMethodInPlaceOrder.equals(checkoutpage.shippingMethodInOrderRecipt())),
						"<b>Actual Result5: </b> Shipping Method in Order Receipt is standard Shipment Method",
						"<b>Actual Result5: </b> Shipping Method in Order Receipt is not standard Shipment Method");

				Log.message(
						"<b>Expected Result6</b> :Shipping Cost in Order Receipt for product cost greater than $98.99 in standard Shipment Method should be selected as $0.00");
				Log.assertThat((shippingCostInPlaceOrder.equals(checkoutpage.shippingCostInOrderReceipt())),
						"<b>Actual Result6: </b> Shipping Cost in Order Receipt for product cost greater than $98.99 in standard shipment method selected is $0.00",
						"<b>Actual Result6: </b>  Shipping Cost in Order Receipt for product cost greater than $98.99 in standard shipment method selected is not $0.00");
			} else {
				Log.message("page not found displayed in mobile site when clicked on Place order");
			}
			Log.testCaseInfo(Comments);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_080

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify shipping method Express: $24.95 is calculated with added item in My Bag screen - Registered User", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_081(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Express", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");
			// Get the Shipping Cost in Shipping Address Page
			String ShippingCostFrmOrderSmmary = checkoutpage.shippingCostInOrderSummary();
			Log.message("13.Shipping Cost in Order Summary in Shipping Address Page :" + ShippingCostFrmOrderSmmary);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b>Shipping Cost is Order Summary of Shipping address page for Express shipment method should  be '$24.95'");
			Log.assertThat((ShippingCostFrmOrderSmmary.equals("$24.95")),
					"<b>Actual Result 1:</b>Shipping Cost is Order Summary of Shipping address page for Express shipment method is '$24.95'",
					"<b>Actual Result 1:</b>Shipping Cost is Order Summary of Shipping address page for Express shipment method is not '$24.95'");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2:</b>Shipping Method in Shipping address page should be selected as Express shipment method");
			Log.assertThat((checkoutpage.getSelectedRadioTextShippingMethod().equals("2DAY")),
					"<b>Actual Result 2:</b>Shipping Method in Shipping address page is selected as Express shipment method",
					"<b>Actual Result 2:</b>Shipping Method in Shipping address page is not selected as Express shipment method");
			// Clicked on Contiue in Shipping Page
			checkoutpage.clickOnContinueInShipping();
			Log.message("14.Clicked on Continue Shipping Address Details as Registered User");
			checkoutpage.chooseOriginalAddressInModal();
			Log.message("15.Selected Original address in Modal");
			// Validate the Address in Shipping page
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter billing Details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("16.Entered Billing Address Details as Registered User");
			// Enter Card Details
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("17.Entered Card Details ");
			// Clicked on Continue in shipping page.
			checkoutpage.clickOnContinueInBilling();
			Log.message("18.Clicked on Continue Billing Page");

			Log.message("</br>");
			Log.message("<b>Expected Result 3:</b> Page should be redirected to Place order page");

			if (Utils.getRunPlatForm().equals("mobile"))

				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutpage),
								"<b>Actual Result 3:</b> Page is redirected to Place order page",
								"<b>Actual Result 3:</b> Page is not redirected to Place order page", driver);
			else
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutpage),
								"<b>Actual Result 3:</b> Page is redirected to Place order page",
								"<b>Actual Result 3:</b> Page is not redirected to Place order page", driver);
			// Get the shipment method in place order
			String shippingMethodInPlaceOrder = checkoutpage.shippingMethodInPlaceOrder();

			// Get shipping cost in place order screen
			String shippingCostInPlaceOrder = checkoutpage.shippingCostInPlaceOrder();

			// Click on Place Order
			OrderConfirmationPage orderConfirmationPage;

			if (Utils.getRunPlatForm().equals("desktop")) {

				orderConfirmationPage = checkoutpage.placeOrder();
				Log.message("19.Clicked on place Order Page");

				Log.message("</br>");
				Log.message("<b>Expected Result 4:</b> Page should be redirected to Order receipt page");

				Log.assertThat(
						orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("btnReturnToShopping"),
								orderConfirmationPage),
								"<b>Actual Result 4:</b> Page is redirected to Order receipt page",
								"<b>Actual Result 4:</b> Page is not redirected to Order receipt page", driver);

				Log.message("</br>");
				Log.message(
						"<b>Expected Result 5:</b>Shipping Method in Order Receipt should be Express Shipment Method");
				Log.assertThat((shippingMethodInPlaceOrder.equals(checkoutpage.shippingMethodInOrderRecipt())),
						"<b>Actual Result 5:</b>Shipping Method in Order Receipt is Express Shipment Method",
						"<b>Actual Result 5:</b>Shipping Method in Order Receipt is not Express Shipment Method");

				Log.message("</br>");
				Log.message(
						"<b>Expected Result 6:</b>Shipping Cost in Order Receipt for Express shipment method should be selected as $24.95");
				Log.assertThat((shippingCostInPlaceOrder.equals(checkoutpage.shippingCostInOrderReceipt())),
						"<b>Actual Result 6:</b>Shipping Cost in Order Receipt for Express shipment method selected is $24.95",
						"<b>Actual Result 6:</b>Shipping Cost in Order Receipt for Express shipment method selected is not $24.95");

			} else {
				Log.message("page not found displayed in mobile site when clicked on Place order");
			}
			Log.testCaseInfo(Comments);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_081

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify shipping method Overnight: $29.95 is calculated with added item in My Bag screen - Registered User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_082(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Overnight", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");
			// Get the Shipping Cost in Shipping Address Page
			String ShippingCostFrmOrderSmmary = checkoutpage.shippingCostInOrderSummary();
			Log.message("13.Shipping Cost in Order Summary in Shipping Address Page :" + ShippingCostFrmOrderSmmary);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>Shipping Cost is Order Summary of Shipping address page for Overnight shipment method should  be '$29.95'");
			Log.assertThat((ShippingCostFrmOrderSmmary.equals("$29.95")),
					"<b>Actual Result :</b>Shipping Cost is Order Summary of Shipping address page for Overnight shipment method is '$29.95'",
					"<b>Actual Result :</b>Shipping Cost is Order Summary of Shipping address page for Overnight shipment method is not '$29.95'");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>Shipping Method in Shipping address page should be selected as Overnight shipment method");
			Log.assertThat((checkoutpage.getSelectedRadioTextShippingMethod().equals("OVNT")),
					"<b>Actual Result :</b>Shipping Method in Shipping address page is selected as Overnight shipment method",
					"<b>Actual Result :</b>Shipping Method in Shipping address page is not selected as Overnight shipment method");
			// Click on Continue button in shipping page
			checkoutpage.clickOnContinueInShipping();
			Log.message("14.Clicked on Continue Shipping Address Details as Registered User");
			checkoutpage.chooseOriginalAddressInModal();
			Log.message("15.Selected Original address in Modal");
			// Validate the address by clicking on continue button
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("16.Entered Billing Address Details as Registered User");
			// Enter card details
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("17.Entered Card Details");
			// Click on Conitnue in billing page
			checkoutpage.clickOnContinueInBilling();
			Log.message("18.Clicked on Continue Billing Page");

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> Page should be redirected to Place order page");

			if (Utils.getRunPlatForm().equals("mobile"))

				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutpage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);
			else
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutpage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);
			// Get the shipping method in place order
			String shippingMethodInPlaceOrder = checkoutpage.shippingMethodInPlaceOrder();
			// Get shipping cost in place order screen
			String shippingCostInPlaceOrder = checkoutpage.shippingCostInPlaceOrder();
			// Click on Place Order
			OrderConfirmationPage orderConfirmationPage;

			if (Utils.getRunPlatForm().equals("desktop")) {

				orderConfirmationPage = checkoutpage.placeOrder();
				Log.message("19.Clicked on place Order Page");

				Log.message("</br>");
				Log.message("<b>Expected Result 2:</b> Page should be redirected to Order receipt page");

				Log.assertThat(
						orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("btnReturnToShopping"),
								orderConfirmationPage),
								"<b>Actual Result 2:</b> Page is redirected to Order receipt page",
								"<b>Actual Result 2:</b> Page is not redirected to Order receipt page", driver);

				Log.message("</br>");
				Log.message(
						"<b>Expected Result :</b>Shipping Method in Order Receipt should be Overnight Shipment Method");
				Log.assertThat((shippingMethodInPlaceOrder.equals(checkoutpage.shippingMethodInOrderRecipt())),
						"<b>Actual Result :</b>Shipping Method in Order Receipt is Overnight Shipment Method",
						"<b>Actual Result :</b>Shipping Method in Order Receipt is not Overnight Shipment Method");
				Log.message("</br>");
				Log.message(
						"<b>Expected Result 6:</b>Shipping Cost in Order Receipt for Overnight shipment method should be selected as $29.95");
				Log.assertThat((shippingCostInPlaceOrder.equals(checkoutpage.shippingCostInOrderReceipt())),
						"<b>Actual Result :</b>Shipping Cost in Order Receipt for Overnight shipment method selected is $29.95",
						"<b>Actual Result :</b>Shipping Cost in Order Receipt for Overnight shipment method selected is not $29.95");
			} else {
				Log.message("page not found displayed in mobile site when clicked on Place order");
			}
			Log.testCaseInfo(Comments);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_082

	
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify estimated order total is properly calculated when applying the gift card from billing address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_084(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Overnight", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");

			// Click on Continue button in shipping page
			checkoutpage.clickOnContinueInShipping();
			Log.message("13.Clicked on Continue Shipping Address Details as Registered User");

			checkoutpage.chooseOriginalAddressInModal();
			Log.message("14.Selected Original address in Modal");

			// Validate the address by clicking on continue button
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("15.Entered Billing Address Details as Registered User");
			// Enter card details
			checkoutpage.fillingBelkGiftCardDetails("card_GiftCard");
			checkoutpage.clickOnApplyGiftCard();
			Log.message(
					"<Expected Result: </b> Last four digits of the Card Number should be seen and the other numbers should be marked with asterick");
			Log.assertThat(checkoutpage.verifyGiftCardNoWhileApply(),
					"<b>Actual Result: </b> Last four digits of the Card Number is shown and the other numbers are marked with asterick",
					"<b>Actual Result:</b> Last four digits of the Card Number is not displayed and the other numbers are not marked with asterick");

			Log.message("<Expected Result: </b> Remove button should be displayed to remove gift card");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnRemoveGiftCard"), checkoutpage),
					"<b>Actual Result: </b> Remove Button is available to remove gift card",
					"<b>Actual Result: </b> Remove Button is not available to remove gift card", driver);
			Log.message("<Expected Result: </b> Transaction amount should be displayed for gift card");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblTransAmtForGiftCard"), checkoutpage),
					"<b>Actual Result: </b> Transaction amount is displayed for gift card",
					"<b>Actual Result: </b> Transaction amount is not displayed for gift card", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_084

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify estimated order total is properly calculated when applying the gift card from billing address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_085(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Overnight", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");

			// Click on Continue button in shipping page
			checkoutpage.clickOnContinueInShipping();
			Log.message("13.Clicked on Continue Shipping Address Details as Registered User");
			checkoutpage.chooseOriginalAddressInModal();
			Log.message("14.Selected Original address in Modal");
			// Validate the address by clicking on continue button
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("15.Entered Billing Address Details as Registered User");
			// Enter card details
			checkoutpage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("16.Entered Gift Card details");
			checkoutpage.clickOnApplyGiftCard();
			Log.message("17.Clicked on apply gift card", driver);
			Log.message(
					"<Expected Result: </b> Last four digits of the Card Number should be seen and the other numbers should be marked with asterick");
			Log.assertThat(checkoutpage.verifyGiftCardNoWhileApply(),
					"<b>Actual Result: </b> Last four digits of the Card Number is shown and the other numbers are marked with asterick",
					"<b>Actual Result:</b> Last four digits of the Card Number is not displayed and the other numbers are not marked with asterick");

			Log.message("<Expected Result: </b> Remove button should be displayed to remove gift card");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnRemoveGiftCard"), checkoutpage),
					"<b>Actual Result: </b> Remove Button is available to remove gift card",
					"<b>Actual Result: </b> Remove Button is not available to remove gift card", driver);
			Log.message("<Expected Result: </b> Transaction amount should be displayed for gift card");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblTransAmtForGiftCard"), checkoutpage),
					"<b>Actual Result: </b> Transaction amount is displayed for gift card",
					"<b>Actual Result: </b> Transaction amount is not displayed for gift card", driver);

			Log.message("<Expected Result: </b>  'Credit Card' field should be disabled.");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblTransAmtForGiftCard"), checkoutpage),
					"<b>Actual Result: </b>  'Credit Card' field is disabled.",
					"<b>Actual Result: </b>  'Credit Card' field is not disabled.", driver);

			// Log.testCaseInfo(Comments);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_085

	
	


	
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the details for Non-registry shipping address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_046(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page!");

			// Navigate to Mini Cart
			ShoppingBagPage shoppingBagPage = myAccountPage.clickOnMiniCart();
			Log.message("4. Navigated to Shopping Bag Page!");

			// Navigate to Checkout Page
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("5. Navigated to Checkout Page!");

			// Clicked 'Yes' on Multi Shipping Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("6. Clicked 'Yes'  On Multi Shipping Button!");

			// Clicking Add button to add the address
			checkoutPage.clickOnAddInShipToMulipleAddressByRow(2);
			Log.message("7. Clicked Add button To add address Detail For 2nd product!");

			// Filling Shipping Details
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address1");
			checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			Log.message("8. Shipping Address filled Successfully!");

			// Clicked Continue In Address Detail
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("9. Clicked on 'Continue' in multiship address Page!");

			// Selecting Shipping Method
			String shippingType = checkoutPage.getTextShippingMethodInMultiShipping();
			checkoutPage.selectShippingMethod(1);
			String shippingTypeChanged = checkoutPage.getTextShippingMethodInMultiShipping();
			Log.message("10. Shipping Method Seleceted SuccessFully!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1 :</b> User should be able to select the appropriate Shipment method from the Shipment drop down!");
			Log.assertThat(!(shippingTypeChanged.equals(shippingType)),
					"<b>Actual Result1 :</b> User is able to select appropriate Shipment method from the Shipment drop down!",
					"<b>Actual Result1 :</b> User is able to select appropriate Shipment method from the Shipment drop down!");

			// Clicked Continue In Shipping Detail
			checkoutPage.clickOnContinueInShipping();
			Log.message("11. Clicked on 'Continue' in multiship method Page!");
			String ShippingCost1 = checkoutPage.getShiipingCostInBillingPage();
			String ShippingCost2 = checkoutPage.getShiipingCostBelowTotalCost();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2 :</b>  The shipment total should be adjusted to match the updated shipping method!");
			Log.assertThat(ShippingCost1.equals(ShippingCost2),
					"<b>Actual Result2 :</b> Shipment Cost is Equal to Shiping Method Choosed!",
					"<b>Actual Result2 :</b> Shipment Cost is not Equal to Shiping Method Choosed!");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> The following customer address details should be displayed: First name,Last name,Address,Phone Number!");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("firstNameInBilling",
							"lastNameInBilling", "addressInBilling", "phn_numberInBilling"), checkoutPage),
							"<b> Actual Result 1: </b> All Elements Are Visible To User: First name,Last name,Address,Phone Number!",
							"<b> Actual Result 1: </b> All Elements Are not Visible To User: First name,Last name,Address,Phone Number!",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_046



	@Test(groups = { "desktop",
	"mobile" }, description = "Verify simple pricing promotions", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_167(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Navigated to the shopping bag page");
			double nowPrice = shoppingBagPage.getNowPriceMsg();
			String nowP = String.valueOf(nowPrice);
			String msg = shoppingBagPage.getStandardPriceMsg();
			Log.message("  Standard Price: " + msg);

			Log.message("<br>");
			Log.message("<b> Expected Result 1:</b> Verify the promotional message in the Shopping Bag Page");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtStandardPrice"), shoppingBagPage),
					"<b>Actual Result 1:</b> Msg display" + msg, "<b>Actual Result 1:</b> Msg Not display", driver);

			Log.message("Price of product after % discount :" + nowPrice);

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(". Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(". Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message(". Entered Shipping address as guest user");
			checkOutPage.clickOnContinueInShipping();
			Log.message(". clicked on continue Button in Shipping address Page");
			checkOutPage.chooseOriginalAddressInModal();
			Log.message(". Selected Original address in the modal");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(". Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message(". Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(". Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message(". Clicked on 'Continue' Button in Billing Page");

			OrderConfirmationPage orderConfirmationPage = checkOutPage.ClickOnPlaceOrderButton();
			Log.message(" Navigated to Order Confirmation Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Order should be properly placed with % off .");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderConfirmationPage.GetOrderNumber();

			Log.message("<br>");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"), orderConfirmationPage),
					"<b> Actual Result 2: </b> Order placed with after clicking on place order button and Order number is generated: "
							+ ordernumb,
							"<b> Actual Result 2: </b> Order is not placed after clicking on place order button. ", driver);

			String paymentType = orderConfirmationPage.GetCardType();
			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();
			String merTotal = String.valueOf(merchandiseTotal);

			Log.message("<br>");
			Log.message(
					"<b> Expected Result :</b> % off should be properly placed when confirming the order with payment method Visa.");

			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("CrdType"),
							orderConfirmationPage) && (nowP.equals(merTotal)),
							"<b>Actual Result :</b> Order is placed with the Payment method:" + paymentType
							+ " with % off on the product ",
							"<b>Actual Result :</b> Order is not placed with the mentioned Payment method ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_167

	

	@Test(groups = { "desktop",	"mobile" }, description = "Verify system displays the item updated cart message.- Product Shipping Restrictions", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdppage= homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page

			Log.message("3. Navigated to PDP!");
			String selectedSize=pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdppage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdppage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			String txtRestrictionMessage = shoppingBagPage.getTextRestrictionShippingMessage();
			Log.message(
					"<b> Expected Result: </b> Verify system displays the item updated cart message.- Product Shipping Restrictions");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtLineItemRestriction"),
							shoppingBagPage),
							"<b>Actual Result: </b>  'Navigated to shopping bag page ,line text for Product resticted shipping is displayed and the text is : "
									+txtRestrictionMessage,
									"<b>Actual Result: </b>  'Navigated to shopping bag page but line text for Product resticted shipping is not displayed.",
									driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//	driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_002

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Employee Discount in the Order Summary in the Place Order step of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_206(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Search " + searchKey + " in the home page and  Navigated to 'PDP'  Page");

			pdpPage.clickAddToBag();
			Log.message("3. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("6. Filled Shipping details");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("7. Click continue Shipping button shipping method page!");

			checkoutPage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
			Log.message("8. Filled billing details");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("9. Card details filled in the billing form!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("10. Click continue button in Billing address page!");

			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message("11. User able to confirm the order with payment method as visa for free shipping product",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Shipping promotion should be added when confirming the order with payment method Visa and Order should be successfully placed");

			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("txtResourcemsg"),
							orderconfirmationpage),
							"Shipping promotion is added when confirming the order with payment method Visa and Order is successfully placed",
							"Shipping promotion is not added when confirming the order with payment method Visa and Order is not successfully placed",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_206


	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Employee Discount in the Order Summary in the Place Order step of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_232(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to SignIn Page
			SignIn signinPage = homepage.headers.navigateToSignIn().get();
			Log.message("2. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("6. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Express", shippingaddress);
			Log.message("9. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("10. Click continue Shipping button shipping method page!");

			/*
			 * //Filling the CardDetails in the billing page
			 * checkoutPage.fillingCardDetails("YES",creditcard); Log.message(
			 * "11. Card details filled in the billing form!");
			 */
			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page
			float merchendisevalue = checkoutPage.getMerchandiseTotalValue();
			float assoicatediscount = checkoutPage.getAssociateDiscountValue();

			Log.message("<b>Expected Result</b> Associate Discount value should be calcualted correctly", driver);
			Log.assertThat((merchendisevalue * 30 / 100) == assoicatediscount,
					"<b>Actual Result :</b> 'Assoicate Discount value is calculated correctly",
					"<b>Actual Result :</b> 'Assoicate Discount value is not calculated correctly");

			checkoutPage.placeOrder();
			Log.message(
					"13. User able to confirm the order with multiple payment option(Belk Gift card with other Credit cards)",
					driver);

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("PlaceOrderInHeaderEnabledInMultiShipping"), checkoutPage),
							"<b>Actual Result :</b> 'Enabled to go to the previous page",
					"<b>Actual Result :</b> 'Not Enabled to go to the previous page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify whether the price adjustments (shipping surcharge if applied, and order or product specific coupons) are displayed per line item in the cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_161(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PdpPage using the search key : " + searchKey);
			Log.message("3. Navigate to the PDP ");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			String txtSurchargeMessage = shoppingBagPage.getTextSurchargeMessage();
			Log.message("<b>");
			Log.message(
					"<b> Expected Result 1: </b> The price adjustments should be displayed per line item in the cart.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtSurchargeOnShoppingCartPage"),
							shoppingBagPage),
							"<b>Actual Result 1: </b>  'Navigated to shopping bag page ,line text for Product  shipping surcharge is displayed and the text is : "
									+ txtSurchargeMessage,
									"<b>Actual Result 1: </b>  'Navigated to shopping bag page but line text for Product shipping surcharge is not displayed.",
									driver);

			Log.message("<b>");
			String txtSubtotalPrice = shoppingBagPage.txtSubtotalLineItemPrice();
			Log.message("9. The Text subtotal is : " + txtSubtotalPrice);
			String price = shoppingBagPage.getStandardPriceMsg();
			Log.message("10. The Standard Price is: " + price);

			String txtSurcharge = shoppingBagPage.txtShippingSurchargeAmmount();
			Log.message("11. The Text Shiping Surcharge  is : " + txtSurcharge);
			Log.message("<b>");
			Log.message(
					"<b> Expected Result 2: Verify the the line item should be display as: 'Orig Price, Surcharge,Sale");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("txtSubtotalPriceLineItem", "txtStandardPrice", "txtShippingSurcharge"),
							shoppingBagPage),
							"<b>Actual Result 2: </b>  'Navigated to shopping bag page ,line text for Product  shipping surcharge ,original price is displayed",
							"<b>Actual Result 2: </b>  'Navigated to shopping bag page but line text for Product Origianl price, surcharge is not displayed.",
							driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_161




	@Test(groups = { "desktop",	"mobile" }, description = "Verify system displays the item updated cart message - Shipping Restriction", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresult = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresult.navigateToPDP();
			Log.message("3. Navigated to PDP!");
			String selectedSize=pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdppage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdppage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			String txtRestrictionStatetMessage = shoppingBagPage.getTextRestrictionStateShippingMessage();
			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b> Verify system displays the item updated cart message - Shipping Restriction");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtLineItemStateRestriction"),
							shoppingBagPage),
							"<b>Actual Result: </b>  'Navigated to shopping bag page ,line text for Product resticted shipping state is displayed and the text is : "
									+ txtRestrictionStatetMessage,
									"<b>Actual Result: </b>  'Navigated to shopping bag page but line text for Product resticted shipping state is not displayed.",
									driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_004

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Bonus Offers GWP Modal in PDP page for products eligible for a Choice Bonus Product Discount", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_226(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instances
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with Keyword '" + searchKey + "' and Navigated to Search Result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP");
			pdpPage.clickOnGwpModalLink();
			Log.message("4. Clicked on 'GWP modal' Link");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result : </b> The 'Free Gift with Purchase' message and Bonus Offers GWP Modal should get displayed in PDP page.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("GWPmodalPopUp"), pdpPage),
					"<b> Actual Result : </b> The 'Free Gift with Purchase' message and Bonus Offers GWP Modal is displayed in PDP page.",
					"<b> Actual Result : </b> The 'Free Gift with Purchase' message and Bonus Offers GWP Modal is not displayed in PDP page.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PROMOTIONS_226

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Bonus Offers GWP Modal in PDP page for products eligible for a Choice Bonus Product Discount", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_228(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instances
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with Keyword '" + searchKey + "' and Navigated to Search Result Page");
			
			Log.message("3. Navigated to PDP");

			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdpPage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("5. Selected size:'" + pdpPage.selectSize() + "' from Size dropdown in the Pdp!");
			pdpPage.selectQtyByIndex(3);
			Log.message("6. Select the Quantity" );
			// Adding product to bag
			pdpPage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button");
			pdpPage.clickOnTocloseInPopUp();
			Log.message("7. Close the free gift item");
			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result : </b> The Select your Free Gift GWP Modal should be dispalyed on the Cart Page.");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("selectFreeGiftModal"), shoppingBagPage),
					"<b> Actual Result : </b> The 'Free Gift with Purchase' message and Bonus Offers GWP Modal is displayed in PDP page.",
					"<b> Actual Result : </b> The 'Free Gift with Purchase' message and Bonus Offers GWP Modal is not displayed in PDP page.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PROMOTIONS_228

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Bonus Offers GWP Modal in PDP page for products eligible for a Choice Bonus Product Discount", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_229(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instances
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with Keyword '" + searchKey + "' and Navigated to Search Result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP");

			pdpPage.clickOnGwpModalLink();
			Log.message("4. Clicked on 'GWP modal' Link");
			String txtFreeOffers= pdpPage.getFreeOfferTxt();
			Log.message("<br>");
			Log.message("<b> Expected Result </b> A list of eligible product tiles that have been assigned as bonus items within a promotion should get displayed within in the GWP modal.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtBonusDescription","imgPopUPFreeOffer"), pdpPage),
					"<b> Actual Result : </b> Free offer bonus product description is displayed and Bonus Offers GWP Modal is displayed in PDP page.",
					"<b> Actual Result : </b> Bonus Offers GWP Modal is not displayed in PDP page.",
					driver);
			Log.testCaseResult();

		} // try 
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PROMOTIONS_229

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the '3.Billing' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_062(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress(0);
			Log.message("5. Clicked Add button on multiship step1 page for 1st product");
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");

			checkoutPage
			.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			checkoutPage.clickOnSaveInAddEditAddressModal();
			Log.message("6. filled address for 1st product and clicked save button");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("7. Click on Contine button in Shipping Address page");			
			checkoutPage.clickOnContinueInShipping();

			Log.message("<b>Expected Result:</b>");
			Log.message("<br>");
			Log.message("Billing Address section and Payment section should be enabled");
			Log.assertThat(
					checkoutPage.elementLayer
					.verifyPageElements(
							Arrays.asList("txtBillingAddressname", "txtBillingFirstname", "txtBillingLastname",
									"txtBillingAddressOne", "txtBillingCity", "btnBillingState",
									"txtBillingZipcode", "txtBillingPhoneNo", "txtBillingEmail",
									"chkBillingEmail", "chkSaveToBillingAddressBook", "btnCardType"),
									checkoutPage),
									"Billing Address section and Payment section should be enabled",
					"Billing Address section and Payment section is not enabled");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the Product details displayed in Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_048(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress(0);
			Log.message("5. Clicked Add button on multiship step1 page for 1st product");
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");

			checkoutPage
			.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			checkoutPage.clickOnSaveInAddEditAddressModal();
			Log.message("6. filled address for 1st product and clicked save button");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("7. Click on Contine button in Shipping Address page");

			Log.message("8. filled address for 2nd product and clicked save button");

			checkoutPage.getSelectedShippingmethodvalueByindex(0);
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageListElements(Arrays
							.asList("lstProductName", "lstProductthumbnail",
									"lstProductthumbnail", "lstProductcolor",
									"lstProductSize", "lstProductQty"),
									checkoutPage),
									"<b>Actual Result:</b> All the Product Details are displayed",
					"<b>Actual Result:</b> Product details are not displayed as Expected");
			PdpPage pdpPage = checkoutPage.clickonProductthumbnailByIndex(0);
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("imgPrimary"), pdpPage),
							"<b>Actual Result:</b> PDP Page is displayed",
					"<b>Actual Result:</b> PDP Page is not displayed");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the details for Registry shipping address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_047(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");

			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress(0);
			Log.message("5. Clicked Add button on multiship step1 page for 1st product");
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");

			checkoutPage
			.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			checkoutPage.clickOnSaveInAddEditAddressModal();
			Log.message("6. filled address for 1st product and clicked save button");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("7. Click on Contine button in Shipping Address page");

			System.out.println(checkoutPage
					.getNosShippingMethodInMultishipping());
			String drpshippingmethodvalue = checkoutPage
					.getselectedShippingmethod();

			String selectedShippingMethod = checkoutPage
					.shippingMethodInPlaceOrder();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Default Shipping Method should be selected as Standard");
			Log.message("<br>");

			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					drpshippingmethodvalue.contains("Standard"),
					"<b>Actual Result:</b>Default Shipping Method would be selected as "
							+ drpshippingmethodvalue,
							"<b>Actual Result:</b>Default Shipping Method is not selected as Standard and Selected shipping method is"
									+ drpshippingmethodvalue, driver);

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstshippingmethods"), checkoutPage),
							"<b>Actual Result:</b>Shipment method is selectable",
					"<b>Actual Result:</b>Shipment method is not selectable");
			checkoutPage.selectShippingMethodByValue("2DAY");
			checkoutPage.clickOnContinueInShipping();
			String selectedShippingMethod2 = checkoutPage
					.shippingMethodInPlaceOrder();

			Log.softAssertThat(
					(!selectedShippingMethod.contains(selectedShippingMethod2)),
					"<b>Actual Result:</b> Shipping method is updated",
					"<b>Actual Result:</b> Shipping method is not udpated",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Pacesetter Discount in the Order Summary in the Place Order step of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_233(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments");
		String couponcode = testData.get("CouponCode");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to SignIn Page
			SignIn signinPage = homepage.headers.navigateToSignIn().get();
			Log.message("2. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. User Entered Credientials in User Name:" + emailid
					+ "/Password:" + password + ".", driver);

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = myaccount.headers
					.searchProductKeyword(searchKey);
			Log.message("4. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("6. " + pdpPage.getProductName()
					+ "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag(couponcode);
			float couponsavings = Integer.parseInt(shoppingBagPage
					.getOrderSummaryDetails().get("CouponSavings"));
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES",
					"Express", shippingaddress);
			Log.message("9. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("10. Click continue Shipping button shipping method page!");

			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page
			float merchendisevalue = checkoutPage.getMerchandiseTotalValue();
			float assoicatediscount = checkoutPage.getAssociateDiscountValue();

			Log.message(
					"<b>Expected Result</b> Associate Discount value should be calcualted correctly",
					driver);
			Log.assertThat(
					((merchendisevalue - couponsavings) * 20 / 100) == assoicatediscount,
					"<b>Actual Result :</b> 'Assoicate Discount value is calculated correctly",
					"<b>Actual Result :</b> 'Assoicate Discount value is not calculated correctly");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}


	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays a Discount message.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to Sign in page");
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Credentials email: "+emailid+" and password: "+password+" and navigated to my account page");
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
			}

			myAccountPage.headers.navigateTo(searchKey[0],searchKey[1],searchKey[2]);
			PlpPage plpPage = new PlpPage(driver).get();
			pdpPage = plpPage.navigateToPDP();
			Log.message((i++) + ". Searched with '" + searchKey   + "'");

			Log.message((i++) + ". Navigated to PDP");

			// Selecting the Color
			String color1 = pdpPage.selectColor();
			Log.message((i++) + ". selected the '" + color1 + "' Color !");
			// Selecting the Size
			String size1 = pdpPage.selectSize();
			Log.message((i++) + ". selected the '" + size1 + "' size !");
			String qty = pdpPage.selectQuantity("2");
			Log.message((i++) + ". selected the '" + qty + "' size !");
			pdpPage.clickAddToBag();
			Log.message((i++) + ". product added to bag");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to checkout page");
			// Navigating to checkout page as user

			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message((i++) + ". Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");
			// checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}
			Log.message((i++) + ". Filled shipping Address for 1st Product and clicked save button");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message((i++) + ". Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address6");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}
			Log.message((i++) + ". filled address for 2nd product and clicked save button");
			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message((i++) + ". chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message((i++) + ". chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on continue button in multiaddress ");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Shipping discount message should be displayed");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					checkoutPage.getPromoMessage().contains("FREE"),
					"Shipping discount message is displayed",
					"Shipping discount message is not displayed",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_049

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the Shipping Address section - Ship to Home", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		boolean addressvalidation;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to Sign in page");
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Credentials email: "+emailid+" and password: "+password+" and navigated to my account page");
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
			}


			pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey   + "'");

			Log.message((i++) + ". Navigated to PDP");

			// Selecting the Color
			String color1 = pdpPage.selectColor();
			Log.message((i++) + ". selected the '" + color1 + "' Color !");
			// Selecting the Size
			String size1 = pdpPage.selectSize();
			Log.message((i++) + ". selected the '" + size1 + "' size !");
			String qty = pdpPage.selectQuantity("2");
			Log.message((i++) + ". selected the '" + qty + "' size !");
			pdpPage.clickAddToBag();
			Log.message((i++) + ". product added to bag");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to checkout page");
			// Navigating to checkout page as user

			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message((i++) + ". Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");
			// checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}
			Log.message((i++) + ". Filled shipping Address for 1st Product and clicked save button");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message((i++) + ". Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address6");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}
			Log.message((i++) + ". filled address for 2nd product and clicked save button");
			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message((i++) + ". chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message((i++) + ". chosen the address in dropdown for 2nd product");

			LinkedHashMap<String, String> firstAddressBefore = checkoutPage.getMultiShippingAddressByIndex(0);
			LinkedHashMap<String, String> secondAddressBefore = checkoutPage.getMultiShippingAddressByIndex(1);
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on continue button in multiaddress ");
			LinkedHashMap<String, String> shippingAddressDetails = checkoutPage.getShippingAddress();
			Set<String> addressKeys = shippingAddressDetails.keySet();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("verify shipping address section");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(addressKeys.contains("FirstName") && addressKeys.contains("LastName"),
					"Customer name is displayed",
					"Customer name is not displayed",
					driver);

			Log.softAssertThat(addressKeys.contains("Address") && addressKeys.contains("City") && addressKeys.contains("State"),
					"Customer address is displayed",
					"Customer address is not displayed",
					driver);
			Log.softAssertThat(addressKeys.contains("Phone"),
					"Customer phone number is displayed",
					"Customer phone number is not displayed",
					driver);
			Log.message("<b>Expected Result:</b>");
			Log.message("The Edit link should be displayed in Shipping Address section - header");					
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("LnkEditShipAddress"), checkoutPage),
					"<b>Actual Result:</b>edit link is displayed in Shipping address",
					"<b>Actual Result:</b>edit link is not displayed in Shipping address",
					driver);
			checkoutPage.editShippingAddress();
			Log.message("<b>Expected Result:</b>");
			Log.message(" When clicked on Edit link, it should navigate to the Checkout: Step 1. Shipping page,");
			Log.assertThat(driver.getCurrentUrl().contains("COShippingMultiple-Start"),
					"Navigated to step1 Shipping page",
					"DOes not navigated to step1 Shipping page",
					driver);

			LinkedHashMap<String, String> firstAddressAfter = checkoutPage.getMultiShippingAddressByIndex(0);
			LinkedHashMap<String, String> secondAddressAfter = checkoutPage.getMultiShippingAddressByIndex(1);
			Log.message("<b>Expected Result:</b>");
			Log.message("All the pre-filled information should be displayed");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(
					checkoutPage.verifyAddressDetails(firstAddressBefore, firstAddressAfter),
					"pre - filled information is displayed for product 1",
					"pre - filled information is not displayed for product 1",
					driver);
			Log.softAssertThat(
					checkoutPage.verifyAddressDetails(secondAddressBefore, secondAddressAfter),
					"pre - filled information is displayed for product 2",
					"pre - filled information is not displayed for product 2",
					driver);
			Log.message("<b>Expected Result:</b>");
			Log.message("Already saved addresses should be displayed in the drop down list");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(
					checkoutPage.verifyAddressInShipToMultipleAddress(),
					"User see that the address entered for the product is retained respectively in the page in a dropdown",
					"User see that the address entered for the product is not retained respectively in the page in a dropdown",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_060

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify promotional call out message is properly shown in PDP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_194(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey   + "' and navigated to PDP");
			Log.message("</br>");
			Log.message("<b>Expected Result :</b> call out message should be displayed");
			Log.assertThat(
					pdpPage.getPromotionalCalloutMessage().contains("callout"),
					"<b>Actual Result :</b> call out message displayed '"+pdpPage.getPromotionalCalloutMessage()+"'",
					"<b>Actual Result :</b> call out message is not displayed");
			pdpPage.clickOnViewDetails();
			Log.message("<b>Expected Result :</b> Click on View Details link nearer to promotional call out details should be redirected to Deal Based Offer Offers & Rebates Messaging");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("offersandRebatesTab"), pdpPage),
					"<b>Actual Result :</b> Redirected to Offers & Rebates Messaging",
					"<b>Actual Result :</b> Not redirected to Offers & Rebates Messaging");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}//TC_BELK_CHECKOUT_194


	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify user can purchase order with Belk reward dollors and payment method from Billing Address screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_123(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message((i++) + ". Select product from search result page!");
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn Page to Checkout as Sign in user!");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsUser(emailid, password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");
			Log.message((i++) + ". Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();

			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}
			Log.message((i++) + ". clicked on continue button");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address6");
			Log.message((i++) + ". filled billing address");
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard1");
			Log.message((i++) + ". filled card details");
			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar1");
			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) + ". filled belk reward dollars details and click on Apply");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". clicked on continue button");
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message((i++) + ". Clicked place order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Order should be placed successfully when user place order with Belk Credit card number and Belk Reward Dollars");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage!=null,
					"Order placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					"Order is not placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"In Order Receipt payment should be displayed as Belk reward dollar");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("fldRewardDollar"), orderConfirmationPage),
					"Belk Reward Dollar is displayed in Order Reciept",
					"Belk Reward Dollar is not displayed in Order Reciept",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}





	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify discount amount is properly calculated in Order summary panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_134(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		//	String emailid = testData.get("EmailAddress");
		//	String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("BelkGiftCard");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey   + "' and navigated to PDP");

			// Select Size, Color, Qty
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("5. " + pdpPage.getProductName()
					+ "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = checkoutSignInPage.clickOnCheckoutAsGuest();
			Log.message("7. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsGuest(shippingaddress, "Standard");
			Log.message("8. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("9. Click continue Shipping button shipping method page!");

			// Filling the CardDetails in the billing page

			checkoutPage.fillingBillingDetailsAsGuest(shippingaddress);
			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> Payment Method pane should be disabled in Billing Address screen");
			Log.assertThat(
					!checkoutPage.elementLayer
					.verifyPageElements(Arrays.asList("lblPaymentMethod"), checkoutPage),
					"<b>Actual Result 1:</b> 'payment method disabled",
					"<b>Actual Result 1:</b> 'payment method is not disabled");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			LinkedHashMap<String, String> giftCardDetails = orderConfirmationPage.getGiftCardPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> paymentDetails = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			Log.message(
					"<b>  Expected result 2: </b> User able to confirm the order with multiple payment option(Belk Gift card with other Credit cards");
			Log.assertThat(giftCardDetails.get("Amount").equals(paymentDetails.get("EstimatedOrderTotal")), 
					"<b> Actual Result 2: </b> Added amount is properly calculated with the gift card", 
					"<b> Actual Result 2: </b> Added amount is not properly calculated with the gift card", 
					driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}//TC_BELK_CHECKOUT_134

	@Test(groups = { "desktop","mobile" }, description = "Verify whether the best price is displayed on top of the original price with the word 'Now' when the product SKU has an active price record that has any of the following pricebooks,Regular,Best Price", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_159(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");
			pdpPage.selectQuantity();
			Log.message("6. Select the Quantity");
			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");
			String txtOfNowPrice=shoppingBagPage.getTextNowPrice();
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> The best price should be displayed below the original price with the word 'Now' ");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtNowPrice"),
							shoppingBagPage),
							"<b> Actual Result: </b> Navigate to the Shopping Bag page and and Best Price is displayed with the word 'Now' and the text is: " + txtOfNowPrice,
							"<b> Actual Result: </b> Navigate to the Shopping Bag page and Best price is not displayed ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PRICE_159

	@Test(groups = { "desktop","mobile" }, description = "Verify PRODUCT, buy X and Y/ get Z promotional  message is displayed in Cart page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_169(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			// Load PDP Page for Search Keyword
			PdpPage pdppage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			// Navigating to Pdp Page
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			//CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			//Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_169


	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify the '3.Billing' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_061(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress(0);
			Log.message("5. Clicked Add button on multiship step1 page for 1st product");
			checkoutPage
			.fillingAddressDetailsInMultipleShipping("valid_address7");

			checkoutPage
			.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			checkoutPage.clickOnSaveInAddEditAddressModal();
			Log.message("6. filled address for 1st product and clicked save button");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("7. Click on Contine button in Shipping Address page");			
			checkoutPage.clickOnContinueInShipping();

			Log.message("<b>Expected Result:</b>");
			Log.message("<br>");
			Log.message("Billing Address section and Payment section should be enabled");
			Log.assertThat(
					checkoutPage.elementLayer
					.verifyPageElements(
							Arrays.asList("txtBillingAddressname", "txtBillingFirstname", "txtBillingLastname",
									"txtBillingAddressOne", "txtBillingCity", "btnBillingState",
									"txtBillingZipcode", "txtBillingPhoneNo", "txtBillingEmail",
									"chkBillingEmail", "chkSaveToBillingAddressBook", "btnCardType","belkGiftCard","txtCertificateNumber"),
									checkoutPage),
									"Billing Address section and Payment section should be enabled",
					"Billing Address section and Payment section is not enabled");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify PRODUCT, buy X/ get to buy Y promotional message displayed for Purchase with Purchase", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_172(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address =testData.get("Address");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
		
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");


			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			pdppage.selectQtyByIndex(2);
			Log.message("8. The Quantity is selected ");
			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("9. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("10. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			String txtBuyOneText= shoppingBagPage.getTextPrmotionalMessage();
			Log.message("11. The Promotional message for the product Buy 1 get one Product Free is:" + txtBuyOneText);

			Log.message("<b> Expected Result 1: </b> View the pre-specified item is added in Cart page and verify the promotional message ");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("promoFinalTxt"),
							shoppingBagPage),
							"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Promotional message is displayed ",
							"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Promotional message is not displayed ", driver);

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("12. Clicked on 'Check out' Button In Shopping Bag Page!");
			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
			Log.message("13. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("14. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("15. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("16. Filled the Billing details in Billing form!");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("17. Entered card details");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("19. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("20. Clicked on the Place Order Button");
			Log.message("<b> Expected Result 2: </b>  Order should be properly purchased with purchased item");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}
	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify Bonus OffersGift With Purchase is properly displayed in My Bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_174(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address =testData.get("Address");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			Log.message("3. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdppage.selectQtyByIndex(2);
			Log.message("6. The Quantity is selected ");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("6. Clicked on 'Add to bag' button");

			pdppage.selectFreeGiftItem();
			Log.message("7. Select the Free Gift Item");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			String freegiftAdded=shoppingBagPage.freeGiftAddedMessage();
			Log.message("<b> Expected Result 1: </b>  View the pre-specified item is added in Cart page and verify the promotional message");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lblFreeGiftMessage"),
							shoppingBagPage),
							"<b> Actual Result 1: </b> Navigate to the Shopping Bag page, the Free Gift message is displayed and the message is:  "+freegiftAdded,
							"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Free Gift  message is not displayed ", driver);
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("10. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message("12. Entered Shipping address as guest user");
			checkOutPage.clickOnContinueInShipping();
			Log.message("13. clicked on continue Button in Shipping address Page");
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("14. Selected Original address in the modal");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("15. Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("16. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("17. Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on 'Continue' Button in Billing Page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("19. Clicked on 'Continue' Button in Validation Modal");
			checkOutPage.ClickOnPlaceOrderButton();
			Log.message("20. Clicked on the Place Order Button");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_174

	@Test(groups = { "desktop","mobile" }, description = "Verify system displays the cart message for Free Shipping.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_014(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty + " from the PDP page");

			String getTxtOfRebate = pdpPage.txtRebateMessageOnPDP();
			Log.message("7. The Rebate message on the PDP is: " + getTxtOfRebate);

			pdpPage.clickAddToBag();
			Log.message("8. Click on the 'Add To Bag' button and the Product add to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("9. Navigate to the Shopping bag");
			String rebateText=shoppingBagPage.getTextRebateMessage();

			Log.message("<b> Expected Result : The 'Rebate' message should be displayed in Cart Page.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtcartRebate"),
							shoppingBagPage),
							"<b> Actual Result: </b> Navigate to the Shopping Bag page, the Rebate message is displayed and the message is:  "+rebateText ,
							"<b> Actual Result: </b> Navigate to the Shopping Bag page and the Rebate message is not displayed ", driver);


			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_014
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify cancelling selection of items in Bonus Offers GWP Modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_231(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");



		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword ' "+ searchKey+ "' ");


			// Load the PDP Page with search keyword

			Log.message("3. Navigated to PDP Page!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color!");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size!");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. selected the 2 quantity!");

			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag!");

			pdpPage.clickOnNoThanks();
			Log.message("7. Clicked on 'No Thanks' in the GWP modal.");

			Log.message("<br>");

			Log.message("<b> Expected Result: </b> The GWP modal should get closed.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("giftProductModal"),
							pdpPage),
							"<b>Actual Result: </b>  The Bonus Offer GWP Modal is closed in the PDP page. ",
							"<b>Actual Result: </b>  The Bonus Offer GWP Modal not closed in PDP page. ",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_231
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Deal Based offers", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_168(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");


		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");


			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Registry SignIn page");

			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);


			// Load the SearchResult Page with search keyword
			SearchResultPage srchrsult  = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("3. Searched with keyword '"+ searchKey+" ");
			 PdpPage pdpPage=srchrsult.selectProductByIndex(2);
			Log.message("4. Navigated to PDP Page!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("5. selected the '" + color + "' Color!");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("6. selected the '" + size + "' size!");
			
			 pdpPage.selectQtyByIndex(2);
			Log.message("5. select the Color!");
			
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag!");


			ShoppingBagPage shoppingBagPage = myaccount.clickOnMiniCart();
			Log.message("8. Navigated to the shopping bag page");


			String bfrprice = shoppingBagPage.getProductPriceFromshoppingBagpage(); 
			Log.message("   The Price of the product with quantity 1:"+bfrprice);


			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> In my bag screen, for the selected item Deal based promotion should be displayed properly and Order should be placed.");

			Log.message("<br>");
			String aftrprice = shoppingBagPage.getProductPriceFromshoppingBagpage(); 
			Log.message("   The Price of the product after we change the product quantity from 1 to 2:"+aftrprice);
			double price = Double.parseDouble(aftrprice);
			shoppingBagPage.ChangeQuantity(1);


			/***********need to modify*************/

			//			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("msgPromo"), shoppingBagPage),
			//					"<b> Actual Result 1a:</b> Deal based promotion is displayed in the shopping bag page. ",
			//					"<b> Actual Result 1a:</b> Deal based promotion is not displayed in the shopping bag page.",
			//					driver);
			/**************************/
			Log.message("<br>");
			Log.assertThat(aftrprice.equals(bfrprice),
					"<b> Actual Result 1b:</b> The Buy 1 Get 1 free promotion applied as price of product remains same after increasing quantity from one to two.",
					"<b> Actual Result 1b:</b> The Buy 1 Get 1 free promotion is not applied as price of product changes after increasing quantity from one to two.",
					driver);

			shoppingBagPage.ChangeQuantity(1);
			Log.message("<br>");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Navigated to SignIn Page to Checkout as Registered user!");



			checkOutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Express");
			Log.message("10. Entered Shipping address as guest user.");
			checkOutPage.clickOnContinueInShipping();
			Log.message("11. clicked on continue Button in Shipping address Page!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal.");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("13. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("14. Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on 'Continue' Button in Billing Page!");

			OrderConfirmationPage orderConfirmationPage = checkOutPage.ClickOnPlaceOrderButton();
			Log.message("16. Navigated to Order Confirmation Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Order should be properly placed with Buy 1 Get 1 Free.");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderConfirmationPage.GetOrderNumber();


			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit")
					&& orderConfirmationPage.elementLayer
					.verifyPageElements(
							Arrays.asList("orderNumber"),
							orderConfirmationPage),
							"<b> Actual Result 2: </b> Order placed with after clicking on place order button and Order number is generated: "
									+ ordernumb,
									"<b> Actual Result 2: </b> Order is not placed after clicking on place order button. ",driver);

			Log.message("<br>");
			Log.message("<b> Expected Result 3: </b> Buy 1 Get 1 Free deal should be properly shown when confirming the order with payment method Discover.");

			String paymentType = orderConfirmationPage.GetCardType();
			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();

			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(
							Arrays.asList("CrdType"), orderConfirmationPage)&&(price==merchandiseTotal),
							"<b>Actual Result 3:</b> Order is placed with the Payment method '"+paymentType+"' is applied with Buy 1 Get 1 Free deal.",
							"<b>Actual Result 3:</b> Order is not placed with the mentioned Payment method.",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_168

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Bonus Offers GWP Modal in PLP page for products eligible for a Choice Bonus Product Discount", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_225(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");



		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword ' "+ searchKey+ "' ");


			// Load the PDP Page with search keyword

			Log.message("3. Navigated to PDP Page!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color!");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size!");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. selected the 2 quantity!");

			Log.message("<br>");

			Log.message("<b> Expected Result 1:</b> The “Free Gift with Purchase” message and Bonus Offers GWP Modal should get displayed in PDP page.");



			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("msgGWP"),
							pdpPage),
							"<b>Actual Result 1a:</b> The 'Free Gift with Purchase' message displayed in the PDP page as:"+pdpPage.getPromotionMessageInPDP(),
							"<b>Actual Result 1a:</b> The 'Free Gift with Purchase' message not displayed in PDP page. ",
							driver);
			Log.message("<br>");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag!");

			Log.message("<br>");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("giftProductModal"),
							pdpPage),
							"<b>Actual Result 1b:</b> The Bonus Offer GWP Modal displayed in the PDP page. ",
							"<b>Actual Result 1b:</b> The Bonus Offer GWP Modal not displayed in PDP page. ",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_225

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify selecting items in Bonus Offers GWP Modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_230(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");



		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword ' "+ searchKey+ "' ");


			// Load the PDP Page with search keyword

			Log.message("3. Navigated to PDP Page!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color!");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size!");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. selected the 2 quantity!");

			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag!");

			pdpPage.selectFreeGiftItem();
			Log.message("8. Selected Free Gift Item from the modal.");

			Log.message("<br>");

			Log.message("<b> Expected Result: </b> Only a single item should be able to select. The GWP modal should get closed when we select the free item.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("giftProductModal"),
							pdpPage),
							"<b>Actual Result: </b> The GWP modal get closed after selecting the free item.",
							"<b>Actual Result: </b> The GWP modal did not get closed after selecting the free item.",
							driver);

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to Shopping Bag page!");


			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPromoProdUPC"),
							shoppingBagPage),
							"<b>Actual Result: </b> Only single item is selected."+shoppingBagPage.getUPCofFreeGift(),
							"<b>Actual Result: </b> Only single item is not selected.",
							driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_230

	@Test(groups = { "desktop", "mobile" }, description = "Verify promotional call out message is properly shown in PLP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_193(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		String ExceptedPromo = "Buy 1 Get 1 Free";
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");

			//Navigate to PLP Page  
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("3. Navigated to 'PLP' Page!");
			String ActualPromo = plpPage.getTextPromoMessage();

			Log.message("<br>");
			Log.message("<b>Excepted Result :</b> Promotional call out message should be properly displayed in PLP page example : Buy One Get One Free!");
			Log.assertThat(
					ActualPromo.equals(ExceptedPromo),
					"<b>Actual Result :</b> Call Out Promotion Message is displayed in PLP Page!" + ActualPromo ,
					"<b>Actual Result :</b> Call Out Promotion Message is not displayed in PLP Page!",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_193

	@Test(groups = { "desktop", "mobile" }, description = "Verify Offers & Rebates Messaging is properly displayed in PDP page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_196(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		String ExceptedPromo = "Buy 1 Get 1 Free";
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");

			// Navigating to PLP page
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("3. Navigated to 'PLP' Page!");

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.selectProductByIndex(3);
			Log.message("4. Navigated to 'Pdp' Page!");
			String ActualPromo = pdpPage.getTextPromoMessage();

			//Click On Offer And Rebate Link
			pdpPage.clickOnOfferAndRebateLink();
			Log.message("5. Clicked On Offer and Rebate Link");

			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> Disclaimer Text should be properly displayed in PDP page!");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("disclaimer_Text"),pdpPage),
							"<b> Actual Result 1: </b> Disclaimer Text is displayed in PDP page!",
							"<b> Actual Result 1: </b> Disclaimer Text is not displayed in PDP page!",driver);

			Log.message("<br>");
			Log.message("<b>Excepted Result 2:</b> Offers & Rebates Messaging should be properly displayed in PDP page!");
			Log.assertThat(
					ActualPromo.equals(ExceptedPromo),
					"<b>Actual Result 2:</b> Offers & Rebates Message is displayed in PLP Page!" + ActualPromo ,
					"<b>Actual Result 2:</b> Offers & Rebates Message is not displayed in PLP Page!",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_196


	@Test(groups = { "desktop", "mobile" }, description = "Verify Offers & Rebates are properly displayed in PDP page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_199(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");

			// Navigating to PLP page
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("3. Navigated to 'PLP' Page!");

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.selectProductByIndex(8);
			Log.message("4. Navigated to 'Pdp' Page!");
			String ActualPromoInPdpPage = pdpPage.getTextPromoMessage();

			//Click On Offer And Rebate Link
			pdpPage.clickOnOfferAndRebateLink();
			Log.message("5. Clicked On Offer and Rebate Link");
			String ActualPromoInOfferandRebate = pdpPage.getTextPromoMessageInOfferAndRebate();

			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop"){
				Log.message("<br>");
				Log.message("<b> Expected Result 1: </b> Offers & Rebates tab should be shown!");
				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(
								Arrays.asList("offerAndRebateLink"),pdpPage),
								"<b> Actual Result 1: </b> Offers & Rebates tab is displayed in PDP page!",
								"<b> Actual Result 1: </b> Offers & Rebates tab is not displayed in PDP page!",driver);
			}
			if(runPltfrm == "mobile"){
				Log.message("<br>");
				Log.message("<b> Expected Result 1: </b> Offers & Rebates tab should be shown!");
				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(
								Arrays.asList("offerAndRebateLinkMobile"),pdpPage),
								"<b> Actual Result 1: </b> Offers & Rebates tab is displayed in PDP page!",
								"<b> Actual Result 1: </b> Offers & Rebates tab is not displayed in PDP page!",driver);
			}

			Log.message("<br>");
			Log.message("<b>Excepted Result 2:</b> Offers & Rebates Messaging should be properly displayed in PDP page!");
			Log.assertThat(
					ActualPromoInPdpPage.equals(ActualPromoInOfferandRebate),
					"<b>Actual Result 2:</b> Offers & Rebates Message is displayed in PLP Page!" + ActualPromoInPdpPage ,
					"<b>Actual Result 2:</b> Offers & Rebates Message is not displayed in PLP Page!",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_199




	@Test(groups = { "desktop", "mobile" }, description = "Verify whether the storefront displays 'Multiple Offers Available' for a product when the product qualifies for multiple Deal Based promotions!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_191(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		String ExceptedPromo= "Multiple Offers Available";
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");
			// Navigate to PLP page
			PlpPage plpPage = new PlpPage(driver).get();
			String promoActual = plpPage.getTextPromoMessageShirt();
			Log.message("3. Navigated to 'Plp' Page");

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.navigateToPDPByRow(28);
			Log.message("4. Navigated to 'Pdp' Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The store front should display Multiple Offers Available!");
			Log.assertThat(promoActual.equals(ExceptedPromo),
					"<b>Actual Result :</b> Multiple Offers Available is Displayed On Plp Page!",
					"<b>Actual Result :</b> Multiple Offers Available is not Displayed On Plp Page!",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_191



	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify shipping method 'Standard: 9.50' is calculated with added item in My Bag screen, when estimated total is 98.99 or Less - Registered User", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_079(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");
			Log.message("12.Entered Shipping details as signed user");
			// Get the Shipping Cost in Shipping Address Page
			String ShippingCostFrmOrderSmmary = checkoutpage.shippingCostInOrderSummary();
			Log.message("13.Shipping Cost in Order Summary in Shipping Address Page :" + ShippingCostFrmOrderSmmary);

			Log.message(
					"<b>Expected Result :</b> Shipping Cost in Order Summary of Shipping address page for standard shipment method should  be '$9.50'");
			Log.assertThat((ShippingCostFrmOrderSmmary.equals("$9.50")),
					"<b>Actual Result :</b>Shipping Cost in Order Summary of Shipping address page for standard shipment method is '$9.50'",
					"<b>Actual Result :</b>Shipping Cost in Order Summary of Shipping address page for standard shipment method is not '$9.50'");

			Log.message(
					"<b>Expected Result :</b>Shipping Method in Shipping address page should be selected as standard shipment method");
			Log.assertThat((checkoutpage.getSelectedRadioTextShippingMethod().equals("Ground")),
					"<b>Actual Result :</b>Shipping Method in Shipping address page is selected as standard shipment method",
					"<b>Actual Result :</b>Shipping Method in Shipping address page is not selected as standard shipment method");

			// Click on Continue in Shipping
			checkoutpage.clickOnContinueInShipping();
			checkoutpage.chooseOriginalAddressInModal();
			// Validate continue button in Shipping address page.
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("14.Entered Billing Address Details as Registered User");
			// Enter Card Details
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("15.Entered Card Details ");
			// Click on Continue in Billing
			checkoutpage.clickOnContinueInBilling();
			Log.message("16.Clicked on Continue Billing Page");

			Log.message("<b>Expected Result 1:</b> Page should be redirected to Place order page");

			if (Utils.getRunPlatForm().equals("mobile"))

				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutpage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);
			else
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutpage),
								"<b>Actual Result 1:</b> Page is redirected to Place order page",
								"<b>Actual Result 1:</b> Page is not redirected to Place order page", driver);
			// Get the Shipping Method in Place Order Screen
			String shippingMethodInPlaceOrder = checkoutpage.shippingMethodInPlaceOrder();

			// Get shipping cost in place order screen
			String shippingCostInPlaceOrder = checkoutpage.shippingCostInPlaceOrder();

			// Click on Place Order
			OrderConfirmationPage orderConfirmationPage;

			if (Utils.getRunPlatForm().equals("desktop")) {

				orderConfirmationPage = checkoutpage.placeOrder();
				Log.message("18.Clicked on place Order Page");

				Log.message("<b>Expected Result 2: </b> Page should be redirected to Order receipt page");

				Log.assertThat(
						orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("btnReturnToShopping"),
								orderConfirmationPage),
								"<b>Actual Result 2:</b> Page is redirected to Order receipt page",
								"<b>Actual Result 2:</b> Page is not redirected to Order receipt page", driver);

				Log.message(
						"<b>Expected Result :</b>Shipping Method in Order Receipt should be standard Shipment Method");
				Log.assertThat((shippingMethodInPlaceOrder.equals(checkoutpage.shippingMethodInOrderRecipt())),
						"<b>Actual Result :</b>Shipping Method in Order Receipt is standard Shipment Method",
						"<b>Actual Result :</b>Shipping Method in Order Receipt is not standard Shipment Method");

				Log.message(
						"<b>Expected Result :</b> Shipping Cost in Order Receipt for standard Shipment Method should be selected as $9:50");
				Log.assertThat((shippingCostInPlaceOrder.equals(checkoutpage.shippingCostInOrderReceipt())),
						"<b>Actual Result :</b>Shipping Cost in Order Receipt for standard shipment method selected is $9.50",
						"<b>Actual Result :</b>Shipping Cost in Order Receipt for standard shipment method selected is not $9.50");
			} else {
				Log.message("page not found displayed in mobile site when clicked on Place order");
			}
			Log.testCaseInfo(Comments);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_079





	@Test(groups = { "desktop" }, description = "Verify free shipping message is properly shown in PDP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_205(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		//String[] category = testData.get("SearchKey").split("\\|");
		String searchkey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		//String productname=category[3];
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			/*homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1]);
			PlpPage plppage = new PlpPage(driver).get();*/

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchkey);
			Log.message("2. Search "+ searchkey	+ " in the home page and  Navigated to 'Search Result'  Page");

			//Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();

			Log.message("3. Navigated to plp page");
			//PdpPage pdppage = plppage.navigateToPDPbyProductName(productname);
			Log.message("4. Product got selected");
			Log.message("<b>Expected Result: Free Shipping Promation should be displayed on top of Add to Shopping Bag button<>");
			Log.assertThat(pdpPage.getTextFreeShipping().contains("Free Shipping"),
					"<b>Actual Result: Free Shipping Promoation Message is displayed</b>",					
					"<b>Actual Result: Free Shipping Promoation Message is displayed</b>");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	

	}



	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify user can purchase order with Belk Gift Card and payment method from Billing Address screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_120(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments").split("\\|")[0].toString();
		String creditcard = testData.get("Payments").split("\\|")[1].toString();
		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to SignIn Page
			SignIn signinPage = homepage.headers.navigateToSignIn().get();
			Log.message("2. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Searching product in my account page with a productId
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "' and navigated to PDP");

			// Select Size, Color, Qty
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("5. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Express", shippingaddress);
			Log.message("8. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("9. Click continue Shipping button shipping method page!");

			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();
			Log.message("10. Belk Gift card is applied !");
			checkoutPage.fillingCardDetails("YES", creditcard);

			// Filling the CardDetails in the billing page
			Log.message("11. Card details filled in the billing form!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page

			checkoutPage.placeOrder();
			Log.message(
					"<b>Expected Result:</b>User able to confirm the order with multiple payment option(Belk Gift card with other Credit cards)",
					driver);

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnReturntoShopping"), checkoutPage),
					"<b>Actual Result :</b> 'User able to Place the Order with Giftcard and other payment options",
					"<b>Actual Result :</b> 'User Not able to Place the Order with Giftcard and other payment options");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}
	@Test(groups = {
	"desktop" }, description = "Verify free shipping is properly calculated in Bag Page for eligible products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_207(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// String productname=category[3];
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchkey);
			Log.message("2. Search " + searchkey + " in the home page and  Navigated to 'PDP'  Page");

			pdpPage.getTextFreeShipping();
			Log.message("3. Free shipping information is displayed in PDP Page", driver);

			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickAddToBag();
			Log.message("4. Add a product to cart");

			Log.message(
					"<b>Expected Result 1: 'Congratulation! You Qaulify for Free Shipping' Message should be displayed </b>");
			Log.assertThat((pdpPage.elementLayer.verifyPageElements(Arrays.asList("shippingPromotion"), pdpPage)),
					"<b>Actual Result: 'Congratulation! You Qaulify for Free Shipping' Message is displayed</b>",
					"<b>Actual Result: 'Congratulation! You Qaulify for Free Shipping' Message is Not displayed</b>");

			ShoppingBagPage shoppingbag = new ShoppingBagPage(driver).get();
			Log.message("5. Navigate to Shopping bag page");

			shoppingbag.headers.NavigateToBagPage();
			String total = shoppingbag.getEstimatedOrderTotal().replace("$", "");
			double Total = Double.parseDouble(total);
			float orderTotal = (float) Math.floor(Total);

			Log.message("<b>Expected Result 2: OrderTotal amount should be properly calculated in Shopping Bag Page");
			Log.assertThat(
					(shoppingbag.getShippingAmount() == shoppingbag.getShippingAmountDiscount()),
					"<b>Actual Result:</b> OrderTotal amount is properly calculated in Shopping Bag Page",
					"<b>Actual Result:</b> OrderTotal amount is not properly calculated in Shopping Bag Page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_207

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify user can add multiple shipping addresses for same 2 items in Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_098(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("\\S_")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey);

			} else {
				myAccountPage.headers.clickSignOut();

				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey);
			}

			Log.message("2. Searched with '" + searchKey + "'");

			Log.message("3. Navigated to PDP!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("2");
			Log.message("6. selected the '" + quantity + "' quantity !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage.clickOnCheckoutAsUser(emailid, password);
			Log.message("10. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("12. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address7");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("13. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("14. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address6");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("15. filled address for 2nd product and clicked save button");


			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"User should see that the address entered for the product is retained respectively in the page in a dropdown");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(checkoutPage.verifyAddressInShipToMultipleAddress(),
					"User see that the address entered for the product is retained respectively in the page in a dropdown",
					"User see that the address entered for the product is not retained respectively in the page in a dropdown",
					driver);

			// click on continue button in multiship address page
			checkoutPage.clickAddressDropown(1);
			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("16. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message("17. chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("18. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("19. Clicked on continue button in multiShipping page");
			// filling billing address
			checkoutPage.fillingBillingDetailsAsSignedInUser("No", "No", "valid_address2");
			Log.message("20. filled billing address");
			checkoutPage.fillingCardDetails("No", "card_Visa");
			Log.message("21. filled card Details");
			// clicking continue in billing page

			checkoutPage.clickOnContinueInBilling();
			Log.message("22. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("23. Clicked Place Order button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order should be properly placed from Place order screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage != null,
					"Order is properly placed from Place order screen",
					"Order is not properly placed from Place order screen", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Surcharge should be calculated and surcharges Applied should be shown");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(orderConfirmationPage.elementLayer.verifyPageListElements(Arrays.asList("fldShippingCharge"), orderConfirmationPage), 
					"Surcharge is calculated and surcharges Applied is shown",
					"Surcharge is not calculated and surcharges Applied is not shown", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("MultiShipping Address should be displayed in order placed receipt");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					orderConfirmationPage.elementLayer.verifyPageListElements(Arrays.asList("multishipAddress"),
							orderConfirmationPage),
							"MultiShipping Address is displayed in order placed receipt",
							"MultiShipping Address is not displayed in order placed receipt", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_98

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify user can add multiple shipping addresses for 2 different items in Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_099(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		String searchKey1 = searchKey[0].split("\\S_")[1];
		String searchKey2 = searchKey[1].split("\\S_")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey1);
			} else {
				myAccountPage.headers.clickSignOut();

				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey1);
			}

			Log.message("2. Searched with '" + searchKey + "'");

			Log.message("3. Navigated to PDP");

			// Selecting the Color
			String color1 = pdpPage.selectColor();
			Log.message("4. selected the '" + color1 + "' Color !");
			// Selecting the Size
			String size1 = pdpPage.selectSize();
			Log.message("5. selected the '" + size1 + "' size !");
			pdpPage.clickAddToBag();
			homePage.headers.searchAndNavigateToPDP(searchKey2);
			Log.message("6. Search " + searchKey2 + " in the PDP page ");

			Log.message("7. Navigated to PDP");

			// Selecting the Color
			String color2 = pdpPage.selectColor();
			Log.message("8. selected the '" + color2 + "' Color !");
			// Selecting the Size
			String size2 = pdpPage.selectSize();
			Log.message("9. selected the '" + size2 + "' size !");
			// Adding item to the shopping bag

			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("10. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("12. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage.clickOnCheckoutAsUser(emailid, password);
			Log.message("13. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("14. Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("15. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address7");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("16. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("19. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address6");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("20. filled address for 2nd product and clicked save button");


			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"User should see that the address entered for the product is retained respectively in the page in a dropdown");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.verifyAddressInShipToMultipleAddress(),
					"User see that the address entered for the product is retained respectively in the page in a dropdown",
					"User see that the address entered for the product is not retained respectively in the page in a dropdown",
					driver);

			// click on continue button in multiship address page
			checkoutPage.clickAddressDropown(1);
			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("21. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message("22. chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("23. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("24. Clicked on continue button in multiShipping page");
			// filling billing address
			checkoutPage.fillingBillingDetailsAsSignedInUser("No", "No", "valid_address2");
			Log.message("25. filled billing address");
			checkoutPage.fillingCardDetails("No", "card_Visa");
			Log.message("26. filled card Details");
			// clicking continue in billing page

			checkoutPage.clickOnContinueInBilling();
			Log.message("27. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("28. Clicked Place Order button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order should be properly placed from Place order screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage != null,
					"Order is properly placed from Place order screen",
					"Order is not properly placed from Place order screen", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Surcharge should be calculated and surcharges Applied should be shown");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(orderConfirmationPage.elementLayer.verifyPageListElements(Arrays.asList("fldShippingCharge"), orderConfirmationPage), 
					"Surcharge is calculated and surcharges Applied is shown",
					"Surcharge is not calculated and surcharges Applied is not shown", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("MultiShipping Address should be displayed in order placed receipt");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(
					orderConfirmationPage.elementLayer.verifyPageListElements(Arrays.asList("multishipAddress"),
							orderConfirmationPage),
							"MultiShipping Address is displayed in order placed receipt",
							"MultiShipping Address is not displayed in order placed receipt", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_99

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify added multiple items displayed in right panel of the Shopping Cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_100(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = null;
			SearchResultPage searchResultPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();


				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey[0]);
			} else {
				myAccountPage.headers.clickSignOut();
				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey[0]);
			}

			Log.message("2. Searched with '" + searchKey[0] + "'");

			Log.message("3. Navigated to PDP");

			// Selecting the Color
			String color1 = pdpPage.selectColor();
			Log.message("4. selected the '" + color1 + "' Color !");
			// Selecting the Size
			String size1 = pdpPage.selectSize();
			Log.message("5. selected the '" + size1 + "' size !");
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");
			searchResultPage = pdpPage.headers.searchProductKeyword(searchKey[1]);
			Log.message("7. Search " + searchKey[1] + " in the PDP page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			searchResultPage.navigateToPDP();
			Log.message("8. Select product from search result page!");
			// Selecting the Color
			String color2 = pdpPage.selectColor();
			Log.message("9. selected the '" + color2 + "' Color !");
			// Selecting the Size
			String size2 = pdpPage.selectSize();
			Log.message("10. selected the '" + size2 + "' size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("11. Product added to Bag !");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("12. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("13. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage.clickOnCheckoutAsUser(emailid, password);
			Log.message("14. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);
			// Filling shipping address
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");
			Log.message("15. filled shipping details");
			checkoutPage.clickOnContinueInShipping();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("16. Clicked on continue");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address2");
			Log.message("17. filled billing address");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("18. filled card details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("19. clicked on continue button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"User should see the details of multiple products added to Shopping Bag in the below sections 1. Order Summary Section. 2. Shipping Address 3. Payment Method");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("navigateOrderSummary", "navigateShippingAddress", "navigatePaymentMethod"),
							checkoutPage),
							"User see the details of multiple products for added to Shopping Bag in the below sections 1. Order Summary Section. 2. Shipping Address 3. Payment Method",
							"User not see the details of multiple products added to Shopping Bag in the below sections 1. Order Summary Section. 2. Shipping Address 3. Payment Method",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_100

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify the Order Receipt shows the correct quantity and price when user updates product quantity.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_140(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 3 different products

			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("3");
			Log.message("6. selected the '" + quantity + "' quantity !");
			String productPrice = pdpPage.getProductPrice();
			Log.message("obtained product price");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn signInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = signInPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("12. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_california_address");
			checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}


			Log.message("13. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("16. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_Hawaii_address");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("17. filled address for 2nd product and clicked save button");


			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("20. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_NewMexico_address");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("21. filled address for 3rd product and clicked save button");

			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("23. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 7);
			Log.message("24. chosen the address in dropdown for 2nd product");
			checkoutPage.selectdropdownandAddress(5, 12);
			Log.message("25. chosen the address in dropdown for 3rd product");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("26. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("27. Clicked on continue button in multiShipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("28. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("29. Card details are fillied in the billing page!");

			// int quantitybefore =
			// checkoutPage.getValueFromQuantityInOrderSummary();
			checkoutPage.clickOnEditInOrderSummary();
			Log.message("30. Clicked on 'Edit'link in the billing page!");

			shoppingBagPage.removeItemsByIntex(0);
			Log.message("31. removed one item from bag");
			int productCount = Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", ""));
			Log.message("32. obtained product count as " + productCount);
			shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("33. Navigated to checkout Login page");
			// Navigating to checkout page as user
			signInPage.clickOnCheckoutAsGuest();
			Log.message("34. Clicked on Checkout As Guest User in Sign In Page");
			// checkoutPage.clickOnYesInShipToMultipleAddress();
			// Log.message("35. Clicked on 'YES' Button in Ship to Multiple
			// Address Page");
			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("35. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 7);
			Log.message("36. chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("37. Clicked on continue button in multiaddress ");
			String txtShippingMethod = checkoutPage.getTextShippingMethodInMultiShipping();
			Log.message("38. obtained Shipping method as " + txtShippingMethod);
			checkoutPage.clickOnContinueInShipping();
			Log.message("39. Clicked on continue button in multiShipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("40. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("41. Card details are fillied in the billing page!");

			checkoutPage.clickOnContinueInBilling();
			Log.message("42. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("43. Clicked Place Order button");
			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();
			double productOfpriceAndQuantity = orderConfirmationPage.calculatePrice(productCount, productPrice);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("quantity and price should be updated as per the user changes");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(Double.compare(merchandiseTotal, productOfpriceAndQuantity) == 0,
					"quantity and price updated as per the user changes",
					"quantity and price not updated as per the user changes", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("selected shiping method should be shown in shipping address section");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(orderConfirmationPage.getShippingMethod().equalsIgnoreCase(txtShippingMethod),
					"selected shiping method is shown in shipping address section",
					"selected shiping method is not shown in shipping address section", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_140

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify order summary total is properly calculated when increasing the QTY of the added item from Order Summary panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_141(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 3 different products

			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("2");
			Log.message("6. selected the '" + quantity + "' quantity !");
			String productPrice = pdpPage.getProductPrice();
			Log.message("obtained product price");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn signInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = signInPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("12. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_california_address");
			checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("13. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("16. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_Hawaii_address");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("17. filled address for 2nd product and clicked save button");


			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("20. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message("21. chosen the address in dropdown for 2nd product");

			//
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("22. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue button in multiShipping page");
			//
			checkoutPage.clickOnEditInOrderSummary();
			Log.message("24. Clicked on 'Edit'link in the billing page!");

			shoppingBagPage.removeItemsByIntex(0);
			Log.message("25. removed one item from bag");
			shoppingBagPage.selectQuantity("3");
			Log.message("26. selected the quantity 3 from the dropdown");
			int productCount = Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", ""));
			Log.message("27. obtained product count as " + productCount);
			shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("28. Navigated to checkout Login page");
			// Navigating to checkout page as user
			signInPage.clickOnCheckoutAsGuest();
			Log.message("29. Clicked on Checkout As Guest User in Sign In Page");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("30. Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("31. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_NewMexico_address");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("32. filled address for 3rd product and clicked save button");

			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("35. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 7);
			Log.message("36. chosen the address in dropdown for 2nd product");
			checkoutPage.selectdropdownandAddress(5, 12);
			Log.message("37. chosen the address in dropdown for 3rd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("38. Clicked on continue button in multiaddress ");
			// String txtShippingMethod =
			// checkoutPage.getTextShippingMethodInMultiShipping();
			// Log.message("39. obtained Shipping method as " +
			// txtShippingMethod);
			checkoutPage.clickOnContinueInShipping();
			Log.message("39. Clicked on continue button in multiShipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("40. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("41. Card details are fillied in the billing page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("estimated order total should be recalculated");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(checkoutPage.verifyOrderTotalInOrderSummary(),
					"estimated order total is succesfully recalculated",
					"estimated order total is not succesfully recalculated", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("shipping address should be shown with shipping address #1");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("navigateShippingAddress"),
					checkoutPage), "shipping address is shown", "shipping address is not shown", driver);
			checkoutPage.clickOnContinueInBilling();
			Log.message("42. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("43. Clicked Place Order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order should be properly placed from Place order screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage != null,
					"Order is properly placed from Place order screen",
					"Order is not properly placed from Place order screen", driver);

			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();
			double productOfpriceAndQuantity = orderConfirmationPage.calculatePrice(productCount, productPrice);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("quantity and price should be updated as per the user changes");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(Double.compare(merchandiseTotal, productOfpriceAndQuantity) == 0,
					"quantity and price updated as per the user changes",
					"quantity and price not updated as per the user changes", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_141

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system splits the shipping destination address into separate shipment.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		// String searchKey1 = searchKey[0].split("\\S_")[1];
		// String searchKey2 = searchKey[1].split("\\S_")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();


			} else {
				myAccountPage.headers.clickSignOut();

			}
			homePage.headers.navigateToSignIn();
			signInPage.signInToMyAccount(emailid, password);
			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey[0]);
			Log.message("2. Search " + searchKey[0] + "'");


			Log.message("3. Navigated to 'PDP'  Page");

			// Selecting the Color
			String color1 = pdpPage.selectColor();
			Log.message("4. selected the '" + color1 + "' Color !");
			// Selecting the Size
			String size1 = pdpPage.selectSize();
			Log.message("5. selected the '" + size1 + "' size !");
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");
			SearchResultPage searchResultPage = pdpPage.headers.searchProductKeyword(searchKey[1]);
			Log.message("7. Search " + searchKey[1] + " in the PDP page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			searchResultPage.navigateToPDP();
			Log.message("8. Select product from search result page!");

			// Selecting the Color
			String color2 = pdpPage.selectColor();
			Log.message("9. selected the '" + color2 + "' Color !");
			// Selecting the Size
			String size2 = pdpPage.selectSize();
			Log.message("10. selected the '" + size2 + "' size !");
			// Adding item to the shopping bag

			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("11. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("12. Navigated to shopping bag page");
			int productCount = Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", ""));
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("13. Navigated to checkout Login page");

			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("14. Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("15. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address7");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}


			Log.message("16. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("19. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address6");
			//			checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("20. filled address for 2nd product and clicked save button");



			checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("23. chosen the address in dropdown for 1st product");

			checkoutPage.selectdropdownandAddress(3, 6);
			Log.message("24. chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("25. Clicked on continue button in multiaddress ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Each address that represents a shipping destination should be split into a separate shipment.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat((checkoutPage.countOfMultiAddress() == productCount),
					"Each address that represents a shipping destination is split into a separate shipment.",
					"Each address that represents a shipping destination is not split into a separate shipment.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_042


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify order is placed when placing order with Belk Credit card number and Belk Reward Dollars", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_094(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();

			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else{
				myAccountPage.headers.clickSignOut();
			}


			SearchResultPage searchResultPage = signInPage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message((i++) + ". Select product from search result page!");
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn Page to Checkout as Sign in user!");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsUser(emailid, password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");
			Log.message((i++) + ". Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();

			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalWithDefaults();
			}
			Log.message((i++) + ". clicked on continue button");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address6");
			Log.message((i++) + ". filled billing address");
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard1");
			Log.message((i++) + ". filled card details");
			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar1");
			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) + ". filled belk reward dollars details and click on Apply");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". clicked on continue button");
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message((i++) + ". Clicked place order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Order should be placed successfully when user place order with Belk Credit card number and Belk Reward Dollars");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage!=null,
					"Order placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					"Order is not placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_094

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user allow place order by selecting the saved Belk Rewards Credit Card and Belk Rewards Dollars", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_095(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else{
				myAccountPage.headers.clickSignOut();
			}


			SearchResultPage searchResultPage = signInPage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message((i++) + ". Select product from search result page!");
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn Page to Checkout as Sign in user!");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsUser(emailid, password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");
			Log.message((i++) + ". Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();

			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalWithDefaults();
			}
			Log.message((i++) + ". clicked on continue button");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address6");
			Log.message((i++) + ". filled billing address");
			checkoutPage.selectSavedCard("card_BelkRewardsCreditCard1");
			Log.message((i++) + ". selected saved card details");
			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar1");

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) + ". filled belk reward dollars details and click on Apply");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". clicked on continue button");
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message((i++) + ". Clicked place order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Order should be placed successfully when user place order with Belk Credit card number and Belk Reward Dollars");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage!=null,
					"Order placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					"Order is not placed successfully when user place order with Belk Credit card number and Belk Reward Dollars",
					driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_095

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify estimated order total is properly calculated when applying Belk reward dollars from billing address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_096(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");


		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);

			ShoppingBagPage bagPage = null;


			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else{
				myAccountPage.headers.clickSignOut();
			}


			SearchResultPage searchResultPage = signInPage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message((i++) + ". Select product from search result page!");
			// Select Size, Color, Qty

			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();

			String qty = pdpPage.selectQuantity();
			Log.message((i++) + ". Selected Size : " + size);
			Log.message((i++) + ". Selected Color : " + color);
			Log.message((i++) + ". Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn Page to Checkout as Sign in user!");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsUser(emailid, password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");
			Log.message((i++) + ". Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();

			boolean addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalWithDefaults();
			}
			Log.message((i++) + ". clicked on continue button");


			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar1");

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) + ". filled belk reward dollars details and click on Apply");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"The user should see that:a. Last four digits of the Card Number should be seen.b. 'Remove' link should be shown.c. The amount applied towards the order should be displayed");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(checkoutPage.getCardNumber().contains("2139"), "Last four digit is shown", 
					"Last four digit is not shown",driver);
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("trnsctnAmount"), checkoutPage), 
					"Amount shown", 
					"Amount not shown",driver);
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnRemoveInBRD"), checkoutPage), 
					"Remove link is shown", 
					"Remove link is not shown",driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_096


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system auto-select the Ground Shipping method for the Express Checkout process.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_071(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Ground shipping method should be auto-selected for the Express Checkout process.");

			Log.assertThat(checkoutPage.getShippingDetailsInPlaceOrder().contains("Standard"),
					"<b>Actual Result:</b> Ground shipping method is auto-selected for the Express Checkout process.",
					"<b>Actual Result1:</b> Ground shipping method is not auto-selected for the Express Checkout process.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_071//
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays the details in Order Summary.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_073(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials:( " + emailid + " / " + password + " )");

			// Getting default Shipping address in MyAccount Page
			String defaultShippingaddress = myAccountPage.gettextfromShippingAddressInMyAccount();
			String defaultShippingAddress1 = defaultShippingaddress.replaceAll("\\-", "");

			// Getting default Billing address in MyAccount Page
			String defaultBillingAddress = myAccountPage.getextfromBillingAddressInMyAccount();
			String defaultBillingAddress1 = defaultBillingAddress.replaceAll("\\-", "");

			// Getting default payment method in MyAacount page
			String cardtype = myAccountPage.getTextFromCardType();

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}			
			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from color swatch in the Pdp.");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp.");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			shoppingBagPage1.gettextfromOrderSummaryinShoppingPage();
			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");
			checkoutPage.getTextFromOrderSummaryinPlaceOrderPage();

			// Getting Shipping Address from Place Order Page
			String shippingAddressInPlaceOrderPage = checkoutPage.getShippingDetailsInPlaceOrder();
			String shippingaddress[] = shippingAddressInPlaceOrderPage.split("Shipping Address");
			String shippingaddress2[] = shippingaddress[1].split("Method:");
			String shippingAddressinPlaceOrderPagesplit = shippingaddress2[0].replaceAll("\\-", "");

			// Getting Shipping Address from Place Order Page
			String billingAddressInPlaceOrderPage = checkoutPage.getBillingDetailsInPlaceOrder();
			String billingdetailssplit[] = billingAddressInPlaceOrderPage.split("Address");
			String billingAdrdressinPlaceOrderPagesplit = billingdetailssplit[1].replaceAll("\\-", "");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Order Details should be displayed in the Order Summary");
			Log.assertThat(
					shoppingBagPage1.gettextfromOrderSummaryinShoppingPage()
					.contains(checkoutPage.getTextFromOrderSummaryinPlaceOrderPage()),
					"<b>Actual Result 1:</b> Order details : " + checkoutPage.getTextFromOrderSummaryinPlaceOrderPage()
					+ " are displayed in the Order Summary.",
					"<b>Actual Result 1:</b> Order details : " + checkoutPage.getTextFromOrderSummaryinPlaceOrderPage()
					+ "are not displayed in the Order Summary.",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Default Shipping Address Details should be displayed in the Order Summary");
			Log.assertThat(defaultShippingAddress1.contains(shippingAddressinPlaceOrderPagesplit.trim()),
					"<b>Actual Result 2:</b> Shipping Details : " + shippingAddressinPlaceOrderPagesplit
					+ " are displayed in the Order Summary.",
					"<b>Actual Result 2:</b> Shipping details : " + shippingAddressinPlaceOrderPagesplit
					+ " are not displayed in the Order Summary.",
					driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Default Billing Address Details should be displayed in the Order Summary");
			Log.assertThat(defaultBillingAddress1.contains(billingAdrdressinPlaceOrderPagesplit.trim()),
					"<b>Actual Result 3:</b> Billing Details : " + billingAdrdressinPlaceOrderPagesplit
					+ " are displayed in the Order Summary.",
					"<b>Actual Result 3:</b> Billing details : " + billingAdrdressinPlaceOrderPagesplit
					+ " are not displayed in the Order Summary.",
					driver);
			checkoutPage.getTextFromPaymentdetailsInExpressCheckOut();
			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> Default Payment method should be displayed in the Order Summary.");
			Log.assertThat(
					(checkoutPage.getTextFromPaymentdetailsInExpressCheckOut().trim().contains(cardtype)
							&& checkoutPage.getTextFromPaymentdetailsInExpressCheckOut().trim().contains("CVV")),
							"<b>Actual Result 4:</b>  Default Payment method : " + cardtype
							+ "  is displayed in the Order Summary.",
							"<b>Actual Result 4:</b> Default Payment method : " + cardtype
							+ "  is not displayed in the Order Summary.",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_073

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system display the Default Payment method under the Payment method.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_074(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");
			myAccountPage.gettextfromShippingAddressInMyAccount();
			String cardtype = myAccountPage.getTextFromCardType();

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			checkoutPage.getTextFromPaymentdetailsInExpressCheckOut();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Default Payment method should be displayed under the Payment method field with the CVV text field.");
			Log.assertThat(
					(checkoutPage.getTextFromPaymentdetailsInExpressCheckOut().trim().contains(cardtype)
							&& checkoutPage.getTextFromPaymentdetailsInExpressCheckOut().trim().contains("CVV")),
							"<b>Actual Result 1:</b>  Default Payment method is displayed under the Payment method field with the CVV text field. ",
							"<b>Actual Result 1:</b> Default Payment method is not be displayed under the Payment method field with the CVV text field.",
							driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> CVV text field should be blank by default.");
			Log.assertThat(checkoutPage.verifyCvvfieldIsEmpty(),
					"<b>Actual Result 2:</b> CVV text field is blank by default. ",
					"<b>Actual Result 2:</b> CVV text field is not blank by default.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> User should be able to enter numeric value in  CVV text field");
			checkoutPage.enterCVVinExpressCheckout("card_Visa");
			Log.message("<b>Actual Result 3:</b> User is able to enter numeric value in  CVV text field", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CART_074//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system display an Error message for Invalid or Empty CVV.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_075(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String CvvErrorMsg = "Please enter CVV number.";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials( " + emailid + " / " + password + " )");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			} 
			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to 'Search Result' Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from color swatch in the Pdp.");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp.");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			checkoutPage.clickOnApplyButton();
			Log.message("11. Clicked on 'Apply' Button In Place Order Page!");
			checkoutPage.getTextfromCvvErrorMsg();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Error message should be displayed when customer click on Apply button with invalid or Empty CVV field.");
			Log.assertThat((checkoutPage.getTextfromCvvErrorMsg().contains(CvvErrorMsg)),
					"<b>Actual Result:</b>  Error message is displayed when customer click on Apply button with invalid or Empty CVV field.",
					"<b>Actual Result1:</b> Error message is not displayed when customer click on Apply button with invalid or Empty CVV field.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CART_075//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system place the Order when customer enter valid CVV.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_076(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			} 
			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			checkoutPage.enterCVVinExpressCheckout("card_Amex");
			Log.message("11. Entered the Valid CVV in CVV Text Field!");

			checkoutPage.clickOnApplyButton();
			Log.message("12. Clicked on 'Apply' Button In Place Order Page!");

			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("13. Clicked on 'Place Order' button");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Order should be placed successfully when the customer enter the valid CVV number in the CVV field.");

			Log.assertThat((driver.getCurrentUrl().contains("COSummary-Submit")),
					"<b>Actual Result:</b> Order is placed successfully when the customer entered the valid CVV number in the CVV field.",
					"<b>Actual Result:</b> Order is not placed successfully when the customer entered the valid CVV number in the CVV field.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CART_076//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays 'Apply Belk Gift Card' links.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_077(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP Page!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Apply Belk Gift Card' Link Should be displayed");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkApplyGiftCardInExpressCheckout"), checkoutPage),
					"<b>Actual Result 1:</b> 'Apply Belk Gift Card' Link is displayed ",
					"<b>Actual Result 1:</b> 'Apply Belk Gift Card' Link is not displayed", driver);

			checkoutPage.clickOnApplyBelkGiftLink();
			Log.message("11. Clicked on 'Apply Belk Gift Card' Link In Express_Checkout Place Order Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After Clicking 'Apply Belk Gift Card' Link ,the page should Navigate to Billing/Payment Page");

			Log.assertThat((driver.getCurrentUrl().contains("COBilling-Start")),
					"<b>Actual Result 2:</b> After Clicking 'Apply Belk Gift Card' Link ,the page  Navigated to Billing/Payment Page",
					"<b>Actual Result 2:</b> After Clicking 'Apply Belk Gift Card' Link ,the page does not Navigated to Billing/Payment Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_077//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify system displays 'Apply Belk Reward Dollars' links.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_078(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Login with Valid Credentials ( " + emailid + " / " + password + " )");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>  'Apply Belk Reward Dollars' Link Should be displayed");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkApplyRewardCardInExpressCheckOut"), checkoutPage),
					"<b>Actual Result 1:</b> 'Apply Belk Reward Dollars' Link is displayed ",
					"<b>Actual Result 1:</b> 'Apply Belk Reward Dollars' Link is not displayed", driver);

			checkoutPage.clickOnApplyBelkRewardLink();
			Log.message("11. Clicked on 'Apply Belk Reward Card' Link In Express_Checkout Place Order Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After Clicking 'Apply Belk Reward Dollars' Link ,the page should Navigate to Billing/Payment Page");
			Log.assertThat((driver.getCurrentUrl().contains("COBilling-Start")),
					"<b>Actual Result 2:</b> After Clicking 'Apply Belk Reward Dollars' Link ,the page  Navigated to Billing/Payment Page",
					"<b>Actual Result 2:</b> After Clicking 'Apply Belk Reward Dollars' Link ,the page does not Navigated to Billing/Payment Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_078//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify validation failure message is shown for Belk rewards credit card number", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_029(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address = testData.get("Address");
		String cardDetails = "card_BelkRewardCardInvalidNumber";
		String txtInvalidCardNo = "Invalid Credit Card Number";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("14. Filled the Billing details in Billing form!");

			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");

			checkoutPage.getTextFromInvalidCreditCardErrorMsg();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Invalid Credit Card Number warning message should be displayed in Billing Address screen.");
			Log.assertThat(checkoutPage.getTextFromInvalidCreditCardErrorMsg().contains(txtInvalidCardNo),
					"<b>Actual Result 1:</b> Invalid Credit Card Number warning message is displayed in Billing Address screen.",
					"<b>Actual Result 1:</b> Invalid Credit Card Number warning message is not displayed in Billing Address screen.");

			String CreditCardLast4No = checkoutPage.getTextFromCreditCardNumberLastFourNo();
			String[] credit = CreditCardLast4No.split("");
			int lastcreditCardNOcount = 0;
			for (int i = 0; i < 4; i++) {
				if (credit[i] != "*") {
					lastcreditCardNOcount++;
				}
			}
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Only the last four digits of the card number should be shown in the Card Number field.");
			Log.assertThat(lastcreditCardNOcount == 4,
					"<b>Actual Result 2:</b> Only the last four digits of the card number is shown in the Card Number field.",
					"<b>Actual Result 2:</b> Only the last four digits of the card number not shown in the Card Number field.");

			Log.testCaseResult();


		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_029//

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify validation failure message is shown for  Belk rewards credit card number and CVV2 number", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_030(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address = testData.get("Address");
		String cardDetails = "card_BelkRewardCardInvalidCVVNumber";
		String txtInvalidSecurityNo = "Invalid Security Code";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("14. Filled the Billing details in Billing form!");

			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");

			checkoutPage.getTextFromInvalidCreditCardErrorMsg();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Invalid Security Code warning message should be displayed in Billing Address screen.");
			Log.assertThat(checkoutPage.getTextFromInvalidCreditCardErrorMsg().contains(txtInvalidSecurityNo),
					"<b>Actual Result 1:</b> Invalid Security Code warning message is displayed in Billing Address screen.",
					"<b>Actual Result 1:</b> Invalid Security Code warning message is not displayed in Billing Address screen.");

			String CreditCardLast4No = checkoutPage.getTextFromCreditCardNumberLastFourNo();
			String[] credit = CreditCardLast4No.split("");
			int lastcreditCardNOcount = 0;
			for (int i = 0; i < 4; i++) {
				if (credit[i] != "*") {
					lastcreditCardNOcount++;
				}
			}
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Only the last four digits of the card number should be shown in the Card Number field.");
			Log.assertThat(lastcreditCardNOcount == 4,
					"<b>Actual Result 2:</b> Only the last four digits of the card number is shown in the Card Number field.",
					"<b>Actual Result 2:</b> Only the last four digits of the card number not shown in the Card Number field.");

			Log.testCaseResult();


		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_030
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify bonus offer is properly displayed in PDP page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_198(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category(" + category[0] + ") -> L2-Category("
					+ category[1] + ") -> L3-Category(" + category[2] + ")");

			// Navigating to PLP page
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("3. Navigated to 'PLP' Page!");
			String ActualPromo = plpPage.getTextPromoMessage();

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.selectProductByIndex(8);
			Log.message("4. Navigated to 'Pdp' Page!");

			Log.message("<br>");
			Log.message("<b>Excepted Result 1:</b> Bonus offer should be properly displayed in PDP page!");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("promoMessage"),
							pdpPage),
							"<b>Actual Result 1:</b> Bonus offer is displayed in PDP page!" + ActualPromo ,
							"<b>Actual Result 1:</b> Bonus offer is not displayed in PDP page!",driver);

			//Click On View Details
			pdpPage.clickOnViewLink();
			Log.message("5. Clicked On View Link!");

			Log.message("<br>");
			Log.message("<b>Excepted Result 2:</b>  Message should be hyperlinked, on click/tap the Preview Modal will display!");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("ViewDetailsFirst"),
							pdpPage),
							"<b>Actual Result 2:</b>  Message is Displayed in Pdp Page!" ,
							"<b>Actual Result 2:</b>  Message is not Displayed in Pdp Page!",driver);

			// Pop Up Msg IS Not Coming
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_198




	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify card locked message is shown for Belk rewards credit card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_031(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address = testData.get("Address");
		String cardDetails = "card_BelkRewardCardInvalidCVVNumber";
		String txtInvalidSecurityNo = "Lock";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers
					.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey
					+ "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor()
					+ "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize()
					+ "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Standard", address);
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES",
					address);
			Log.message("14. Filled the Billing details in Billing form!");
			int i=0;
			while(i==2){
				checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
				Log.message("15. Filled the Belk Reward Card details in Billing form!");
				// Click continue in the Billing Page
				checkoutPage.clickOnContinueInBilling();
				Log.message("16. Clicked on 'Continue' Button in Billing Page");
				i=i+1;
			}
			checkoutPage.getTextFromInvalidCreditCardErrorMsg();
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Invalid Security Code warning message should be displayed in Billing Address screen.");
			Log.assertThat(
					checkoutPage.getTextFromInvalidCreditCardErrorMsg()
					.contains(txtInvalidSecurityNo),
					"<b>Actual Result 1:</b> Invalid Security Code warning message is displayed in Billing Address screen.",
					"<b>Actual Result 1:</b> Invalid Security Code warning message is not displayed in Billing Address screen.");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_031


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify order is placed when placing order with Belk Credit card number and Belk Reward Dollars", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_089(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address = testData.get("Address");
		String[] payment=testData.get("Payments").split("\\|");
		String cardDetails = payment[0];		
		String rewardDollarsDetails=payment[1];
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			//deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("14. Filled the Billing details in Billing form!");

			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");

			checkoutPage.fillingBelkRewardDollars(rewardDollarsDetails);
			checkoutPage.clickOnApplyBelkRewardDollars();
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");

			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result </b> Order should be placed successfully with Belk RewardCards.");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("ReturnToshipPage"),checkoutPage),
					"<b>Actual Result 1:</b> Order is placed successfully with Belk RewardCards.",
					"<b>Actual Result 1:</b> Order is not placed successfully with Belk RewardCards.");		

			Log.testCaseResult();


		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_89


	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through Belk Reward Credit Card payment method from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_109(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "Belk Reward Credit Card";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");

			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");


			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password and Navigated to Checkout Page!");

			// filling the shipping address details

			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO","valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingBelkRewardCardDetails("NO","card_BelkRewardsCreditCard1");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Belk Reward Credit Card payment method from Billing Address!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b>  User Paid the Amount Through Belk Reward Credit Card!",
					"<b>Actual Result :</b>  User cannot Paid the Amount Through Belk Reward Credit Card!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_109

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through American Express Card payment method from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_110(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "Amex";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");


			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");

			// Navigated to Check Out Page with Valid Credentials

			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address2");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO","valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage
					.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Shipping Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through American Express payment method from Billing Address!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b>  User Paid the Amount Through American Express!",
					"<b>Actual Result :</b>  User Paid the Amount Through American Express!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_110

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through Discover payment method from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_111(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "Discover";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");

			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");


			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");

			// Navigated to Check Out Page with Valid Credentials


			// filling the shipping address details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO","Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO","valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage
					.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Discover payment method from Billing Address!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b>  User Paid the Amount Through Discover!",
					"<b>Actual Result :</b>  User Paid the Amount Through Discover!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_111

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through MasterCard payment method from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_112(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "MasterCard";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",
					"valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage
					.placeOrder();
			// .clickOnPLaceorder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through MasterCard payment method from Billing Address!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b>  User Paid the Amount Through MasterCard!",
					"<b>Actual Result :</b>  User can't Paid the Amount Through MasterCard!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_112

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through Visa Card payment method from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_113(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "Visa";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();                
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",
					"valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage
					.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Shipping Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Visa payment method from Billing Address!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b>  User Paid the Amount Through Visa!",
					"<b>Actual Result :</b>  User can't Paid the Amount Through Visa!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_113


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify standard shipping charge is displayed FREE when using Gift Cards only & Combination with payment methods", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String excepted = "FREE";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Belk Reward Dollar
			checkOutPage.fillingBelkRewardDollars("belk_Reward_Dollar");
			checkOutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) +". Filled the Belk Reward Dollar details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +". Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetShippingtype();
			Log.message((i++) +". Getting Card Type!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b>  In Order placed Receipt Standard Shipping charge should be displayed as FREE should be displayed!");
			Log.assertThat(actual.equals(excepted),
					"<b>Actual Result :</b> FREE Shipping is displayed using Belk Certificate!",
					"<b>Actual Result :</b> FREE Shipping is not displayed using Belk Certificate", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_105

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify standard shipping charge is displayed *FREE* when using Gift Cards only & Combination with payment methods", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_106(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		String excepted = "FREE";
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Standard", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO","valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Card address details
			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Get Card Type From Place Order
			String actual = checkOutPage.GetShippingtype();
			Log.message((i++) +".  Getting Shipping Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> In Order placed Receipt Standard Shipping charge should be displayed as 'FREE' should be displayed!");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b> In Order placed Receipt Standard Shipping charge is displayed as FREE!",
					"<b>Actual Result :</b> In Order placed Receipt Standard Shipping charge is not displayed as FREE!",
					driver);

			//	checkOutPage.clickOnShipping();
			//	checkOutPage.clickReturnToShoppingBag();
			//	shoppingBagPage.removeItemsByIntex(0);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_106


	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify user can pay through only Belk Rewards dollars from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_114(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String excepted = "Belk Rewards Dollar";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Gift Card details
			checkOutPage.fillingBelkRewardDollars("belk_Reward_Dollar");
			checkOutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) +". Filled Gift Card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +". Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +". Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Belk Reward Dollar");
			Log.assertThat(actual.equals(excepted),
					"<b>Actual Result :</b> User Can Pay The Amount with Belk Reward Dollar",
					"<b>Actual Result :</b> User Can Not Able To Pay The Amount with Belk Reward Dollar", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_114

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify user can pay through only Belk gift card from Billing Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_115(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String excepted = "Belk Reward Credit Card";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO","valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Gift Card details
			checkOutPage.fillingBelkGiftCardDetails("card_GiftCard100");
			checkOutPage.clickOnApplyGiftCard();
			Log.message((i++) +". Filled Gift Card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +". Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b>  In Order placed Receipt payment method should be displayed as Belk gift card");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result 1:</b> User Can Pay The Amount with Belk Reward Credit Card!",
					"<b>Actual Result 1:</b> User Can not Pay The Amount with Belk Reward Credit Card!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_115


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify estimated order total is calculated for the added multiple items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_101(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] searchKey = testData.get("SearchKey").split("\\|");
		String username = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] address = testData.get("Address").split("\\|");
		int i  = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(username, password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();



			} else {
				myAccountPage.headers.clickSignOut();


			}


			// Searching 2 different products like jean & T-shirts.
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				pdpPage = signInPage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);

				Log.message((i++) + ". Searched with keyword '" + searchKey[searchIndex] + "' ");

				Log.message((i++) + ". Navigated to PDP");
				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}
			// Navigate to the Shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message((i++) + ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			SignIn checkoutSignInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage.clickOnCheckoutAsUser(username, password);
			Log.message(
					(i++) + ". Entered credentials(" + username + "/" + password + ") and navigating to 'My Account' page",
					driver);
			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'YES' Button in Ship to Multiple Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Click on 'Continue' in ShippingAddress Page");
			checkoutPage.selectShippingMethodInMultiShippingByRow(1,"Express");
			Log.message((i++) + ". Select the shipping type for product 1");
			checkoutPage.selectShippingMethodInMultiShippingByRow(1,"Express");
			Log.message((i++) + ". Select the shipping type for product 2");
			checkoutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Click On the continue button the on Select Shipping type ");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			// filling card details
			Log.message((i++) + ". Fill the billing details");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Fill the Card details in Billing form!");

			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Click continue in Billing Page.");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message((i++) + ". Place order button is clicked");

			String orderText = orderconfirmationpage.getTextFromOrderTotal();
			Log.message((i++) + ". The Order total ammount is :" + orderText);

			String totalShippingAmmount = orderconfirmationpage.getTextFromShipping();
			Log.message((i++) + ". The Total Shipping ammount is: " + totalShippingAmmount);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result 1: </b> User should see that the 'Estimated Order Total: $49.90' is displayed in right panel. ");
			Log.message("<b> Actual Result 1: </b> The Total Shipping ammount is: " + totalShippingAmmount);
			Log.message("<br>");
			Log.message("<b> Expected Result 2:  In Order placed Receipt estimated order total should be displayed.");
			Log.message("<b> Actual Result 2: </b> The Estimated Order Total is display and the ammount  is: "
					+ totalShippingAmmount, driver);
			Log.message("<br>");
			// Getting 'Order Number' from order summary page
			String ordernumb = orderconfirmationpage.GetOrderNumber();
			Log.message((i++) + ". The Order Number is : " + ordernumb);
			Log.message("<br>");
			Log.message("<b> Expected Result 3: </b> Order should be properly placed from Place order screen.");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("orderNumber"),
							orderconfirmationpage),
							"<b> Actual Result 3: </b> The Order is placed and order id is :" + ordernumb,
							"<b> Actual Result 3: </b> The order is not placed", driver);

			// Filling shipping address

			Log.message("<br>");
			Log.testCaseResult();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_101

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify standard shipping charge is displayed FREE when using Gift Cards only and Combination with payment methods.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_104(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> element = Arrays.asList("txtResourcemsg");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home page !");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Entered the email '" + emailid + "' and password '" + password
					+ "' and clicked signIn button !");
			ShoppingBagPage bagPage = myAccountPage.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// clicking on gift card link
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) + ". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");
			// clicking MiniCart
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) + ". Naviagted to Shopping bag");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");
			Log.message((i++) + ". Filled the shipping details with 'Express' method");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message((i++) + ". filled Billing Details.");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message((i++) + ". Filled the card details");
			String RateInBillingPage = checkOutPage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInBillingPage);
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on 'Continue' in billing address page");
			BrowserActions.nap(20);
			// clicking placedOrder button
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message((i++) + ". Place order button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed from Place order screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(orderconfirmationpage.elementLayer.verifyPageElements(element, orderconfirmationpage),
					"Order properly placed from Place order screen.",
					"Order is not properly placed from Place order screen.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"In Order placed Receipt Standard Shipping charge should be displayed as FREE should be displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			String RateInOrderConfirmationPage = orderconfirmationpage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInOrderConfirmationPage);

			Log.assertThat(RateInBillingPage.equals(RateInOrderConfirmationPage),
					"In Order placed Receipt Standard Shipping charge displayed as 'FREE'.",
					"In Order placed Receipt Standard Shipping charge not displayed as 'FREE' .", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_104

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify Express shipping charge is displayed '24.95' when using Gift Cards only & Combination with payment methods ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_107(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> element = Arrays.asList("txtResourcemsg");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home page !");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Entered the email '" + emailid + "' and password '" + password
					+ "' and clicked signIn button !");
			ShoppingBagPage bagPage = myAccountPage.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// clicking on gift card link

			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) + ". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");
			// clicking MiniCart
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) + ". Naviagted to Shopping bag");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Express", "valid_address1");
			Log.message((i++) + ". Filled the shipping details with 'Express' method");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message((i++) + ". filled Billing Details.");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message((i++) + ". Filled the card details");
			String RateInBillingPage = checkOutPage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInBillingPage);
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on 'Continue' in billing address page");
			BrowserActions.nap(20);
			// clicking placedOrder button
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message((i++) + ". Place order button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed from Place order screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(orderconfirmationpage.elementLayer.verifyPageElements(element, orderconfirmationpage),
					"Order properly placed from Place order screen.",
					"Order is not properly placed from Place order screen.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("In Order placed Receipt Express Shipping charge is displayed as '24.95' .");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			String RateInOrderConfirmationPage = orderconfirmationpage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInOrderConfirmationPage);

			Log.assertThat(RateInBillingPage.equals(RateInOrderConfirmationPage),
					"In Order placed Receipt Express Shipping charge displayed as '24.95' .",
					"In Order placed Receipt Express Shipping charge not displayed as '24.95' .", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_107

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify 'Overnight shipping charge is displayed '29.95' when using Gift Cards only & Combination with payment methods", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_108(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> element = Arrays.asList("txtResourcemsg");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home page !");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Entered the email '" + emailid + "' and password '" + password
					+ "' and clicked signIn button !");
			ShoppingBagPage bagPage = myAccountPage.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// clicking on gift card link
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) + ". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage)){
				GiftCardPage.clickOnSelectValue(2);
			}

			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) + ". Product added to Shopping Bag!");

			// clicking MiniCart
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) + ". Naviagted to Shopping bag");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Overnight", "valid_address1");
			Log.message((i++) + ". Filled the shipping details with 'Express' method");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message((i++) + ". filled Billing Details.");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message((i++) + ". Filled the card details");
			String RateInBillingPage = checkOutPage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInBillingPage);
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on 'Continue' in billing address page");
			BrowserActions.nap(20);
			// clicking placedOrder button
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message((i++) + ". Place order button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed from Place order screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(orderconfirmationpage.elementLayer.verifyPageElements(element, orderconfirmationpage),
					"Order properly placed from Place order screen.",
					"Order is not properly placed from Place order screen.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("In Order placed Receipt OverNight Shipping charge is displayed as '29.95' .");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			String RateInOrderConfirmationPage = orderconfirmationpage.getShippingRateInOrderConfirmationPage();
			Log.message((i++) + ". Got the shipping rate in BillingPage as :" + RateInOrderConfirmationPage);

			Log.assertThat(RateInBillingPage.equals(RateInOrderConfirmationPage),
					"In Order placed Receipt OverNight Shipping charge displayed as '29.95' .",
					"In Order placed Receipt OverNight Shipping charge not displayed as '29.95' .", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_108


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify shipping charges are displayed based on the shipping method selection for multiple shipping addresses (combination check)", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_102(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey[] = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] address = testData.get("Address").split("\\|");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 2 different products like jackets & coats and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);

				Log.message((i++) + ". Searched with keyword '" + searchKey[searchIndex] + "' ");

				Log.message((i++) + ". Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}

			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.signIn(emailid, password);
			Log.message((i++) + ". Clicked on Checkout As SignedIn User in Sign In Page");

			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'YES' Button in Ship to Multiple Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on 'Continue' in multiship address Page ");
			checkoutPage.selectShippingMethodInMultiShippingByRow(1, "Express");
			Log.message((i++) + ". Selected 'Express Shipping' for the 1st product.");
			checkoutPage.selectShippingMethodInMultiShippingByRow(2, "Overnight");
			Log.message((i++) + ". Selected 'Overnight Shipping' for the 2nd product.");

			String cost = checkoutPage.shippingCostInOrderSummary();
			Log.message((i++) + ".  Estimated Order Total :" + cost);
			checkoutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on 'Continue' in ShippingAddress Page");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address2");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked continue in Billing Page after filling billing form");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message((i++) + ". Navigated to 'Order Confirmation' Page after clicking on 'Place Order' button.");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Verify shipping charges are displayed based on the shipping method selection for multiple shipping addresses (combination check)");

			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(
							Arrays.asList("txtMerchandiseTotalCost", "txtShippingCost", "txtEstimatedTotal"),
							orderconfirmationpage),
							"<b> Actual Result: </b> Order placed after clicking on place order button and the Order Summary Details displayed.",
							"<b> Actual Result: </b> Order is not placed after clicking on place order button and the Order Summary Details are not displayed.",
							driver);
			Log.message("<br>");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderconfirmationpage.GetOrderNumber();
			Log.message("   Order Number: " + ordernumb);

			Log.testCaseResult();

			shoppingbagpage.removeAddedCartItemsInShoppingBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_102


	@Test(groups = { "desktop",
	"mobile" }, description = "Verify added multiple shipping address should be displayed below Estimated order total", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_103(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey[] = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] address = testData.get("Address").split("\\|");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 2 different products like jackets & coats and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);

				Log.message((i++) + ". Searched with keyword '" + searchKey[searchIndex] + "' ");

				Log.message((i++) + ". Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");

			}
			// Navigating to 'ShoppingBag' Page

			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.signIn(emailid, password);
			Log.message((i++) + ". Clicked on Checkout As Guest User in Sign In Page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'Yes' button in Shipping Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();


			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on 'Continue' in MultiShippingAddress Page");

			checkoutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on 'Continue' in ShippingAddress Page");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address2");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked continue in Billing Page after filling billing form");

			String shpngAdd1 = checkoutPage.getTextFromShippingAddress1();
			String shpngAdd2 = checkoutPage.getTextFromShippingAddress2();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> User should see that the both the addresses are displayed in the Shipping Address section in right panel.");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippngAddress1", "shippngAddress2"),
							checkoutPage),
							"<b> Actual Result 1: </b> Both Shipping Addresses are displayed in PlaceOrder Page."
									+ "\n Shipping Address1:" + shpngAdd1 + "\n Shipping Address2:" + shpngAdd2,
									"<b> Actual Result 1: </b> Both Shipping Addresses are not displayed in PlaceOrder Page.", driver);
			Log.message("<br>");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message((i++) + ". Clicked on 'Place Order' button in Place Order Page");

			Log.message("<br>");
			// Getting 'Order Number' from order summary page
			String ordernumb = orderconfirmationpage.GetOrderNumber();

			Log.message(
					"<b>Expected Result 2:</b> Order should be placed when clicking place order button from place order screen");

			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit") && orderconfirmationpage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"), orderconfirmationpage),
					"<b> Actual Result 2: </b> Navigated to Place Order Page and Order Number is generated after placing order."
							+ ordernumb,
							"<b> Actual Result 2: </b> Order is not placed after clicking on Place Order button. ", driver);
			Log.message("<br>");

			String EstimatedOrderTotal = orderconfirmationpage.GetOrderTotalValue();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> In Order placed Receipt multiple order's estimated order total should be displayed.");

			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("orderValue"),
							orderconfirmationpage),
							"<b> Actual Result 3: </b> The Estimated Order Total is display and the ammount  is:"
									+ EstimatedOrderTotal,
									"<b> Actual Result 3: </b> The Estimated Order Total is not display", driver);
			Log.message("<br>");
			Log.testCaseResult();

			shoppingbagpage.removeAddedCartItemsInShoppingBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_103

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify added multiple shipping address should be displayed below Estimated order total in Place order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_138(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// String searchKey[] = testData.get("SearchKey").split("\\|");

		String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");
		String[] address = testData.get("Address").split("\\|");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search different products like jackets & coats and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {
				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message((i++) + ". Searched with '" + searchKey + "'");
				Log.message((i++) + ". Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}
			// Navigating to 'ShoppingBag' Page

			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest User in Sign In Page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'Yes' button in Shipping address Page");
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on 'Continue' in multiship address Page ");

			checkoutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on 'Continue' in multiship method Page ");

			// Filling billing address details as a guest user
			checkoutPage.fillingBillingDetailsAsGuest("valid_address3");
			Log.message((i++) + ". Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Card details are fillied in the billing page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on Continue in Billing page! ");

			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message((i++) + ". Navigated to 'Order Confirmation' Page after clicking on 'Place Order' button.");

			String Address1 = orderconfirmationpage.getTextFromShippingAddressInOrderPage(1);
			String Method1 = orderconfirmationpage.getTextFromShippingMethodInOrderPage(1);

			String Address2 = orderconfirmationpage.getTextFromShippingAddressInOrderPage(2);
			String Method2 = orderconfirmationpage.getTextFromShippingMethodInOrderPage(1);

			String Address3 = orderconfirmationpage.getTextFromShippingAddressInOrderPage(3);
			String Method3 = orderconfirmationpage.getTextFromShippingMethodInOrderPage(1);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Shipping Addresses for each line item should be displayed as different section with their respective Shipping Method.");

			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(
							Arrays.asList("OrderShippingAddress1", "OrderShippingAddress2", "OrderShippingAddress3"),
							orderconfirmationpage),
							"<b> Actual Result :</b> Order Shipping List displayed along with Shipping Address and Method for each line item. ",
							"<b> Actual Result :</b> Order Shipping List not displayed along with Shipping Address and Method for each line item. ",
							driver);

			Log.message("  Shipping Address1:" + Address1);
			Log.message("<br>");
			Log.message("  Shipping Method1:" + Method1);
			Log.message("<br>");
			Log.message("  Shipping Address2:" + Address2);
			Log.message("<br>");
			Log.message("  Shipping Method2:" + Method2);
			Log.message("<br>");
			Log.message("  Shipping Address3:" + Address3);
			Log.message("<br>");
			Log.message("Shipping Method:" + Method3);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_138

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify by confirm order with payment method 'Amex' and by adding different multiple address example Georgia, Florida", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_130(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		String selectedCardType = "Amex";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);
			pdppage.selectQtyByIndex(2);
			Log.message("3.  Size is Selected from size dropdown !");
			pdppage.selectQuantity();
			Log.message("4. Quantity drowdown is selected with Quantity '2' !");
			pdppage.clickAddToBag();
			Log.message("5. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("6. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. clicked on checkoutAsGuest Button !");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("9. clicked on yes Button for MultiShipping !");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"10. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("11. Clicked on Add Link to add Address");

			LinkedHashMap<String, String> shippingDetails1 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_georgia_address");
			Log.message("12. Shipping details filled in the shipping address form");
			List<String> indexes = new ArrayList<String>(shippingDetails1.keySet());
			String StateInAddress1 = indexes.get(5).replace("select_state_", "");
			String stateCode1 = StateUtils.getStateCode(StateInAddress1);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("13. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("14. Clicked on 'Continue' Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.clickaddLink();
			LinkedHashMap<String, String> shippingDetails2 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_florida_address");
			Log.message("15. Shipping details filled in the shipping address form");
			List<String> index = new ArrayList<String>(shippingDetails2.keySet());
			String StateInAddress2 = index.get(5).replace("select_state_", "");
			String stateCode2 = StateUtils.getStateCode(StateInAddress2);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("16. Clicked on 'Save' Button on Multi Shipping Address page !");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("17. Clicked on 'Continue' Button on Address Validation popup !");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("18. Clicked on 'Save' Button on Multi Shipping Address page !");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("19. First Item's address dropdown is selected !");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message("20. Second Item's address dropdown is selected !");
			shipToMultipleAddressPage.clickContinue();
			Log.message("21. Clicked on continue Button !");
			checkoutPage.clickOnContinueInShipping();
			Log.message("22. Clicked on continue Button in shipping page !");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_alaska");
			Log.message("23. Billing details filled as address 'alaska' !");
			// filling card details
			LinkedHashMap<String, String> cardType = checkoutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("24. Card details filled as cardType 'Visa' !");
			List<String> card = new ArrayList<String>(cardType.keySet());
			String cardTypeInBilling = card.get(0).replace("select_cardtype_", "");
			checkoutPage.clickOnContinueInBilling();
			Log.message("25. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("26. clicked PlaceOrderButton in placeOrder page !");
			String stateCode1InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(0).get("State");
			String stateCode2InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(1).get("State");
			String cardTypeInOrder = orderConfirmationPage.getPaymentMethodInOrderSummary().get("CardType");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address ",
					"Order didn't placed Successfully on added shipping address ", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Added multiple shipping address New Jersey and New Mexico should be properly displayed in Order confirmation screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(stateCode1.equals(stateCode1InOrder),
					"Added multiple shipping address Georgia properly displayed in Order confirmation screen.",
					"Added multiple shipping address Georgia properly not displayed in Order confirmation screen.",
					driver);
			Log.assertThat(stateCode2.equals(stateCode2InOrder),
					"Added multiple shipping address florida properly displayed in Order confirmation screen.",
					"Added multiple shipping address florida properly not displayed in Order confirmation screen.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message(
					"Added multiple shipping address with selected payment method 'American Expresss' should be properly displayed in Order Receipt screen");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");
			Log.assertThat(cardTypeInOrder.equals(selectedCardType),
					"Added multiple shipping address with selected payment method 'American Express' properly displayed in Order Receipt screen ",
					"Added multiple shipping address with selected payment method 'American Express' not displayed in Order Receipt screen ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_130

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify by confirm order with payment method 'Master Card' and by adding different multiple address example Virginia, Washington ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_132(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> confirmationMessage = Arrays.asList("txtResourcemsg");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);
			pdppage.selectQtyByIndex(2);
			Log.message("4.  Size is Selected from size dropdown ");
			pdppage.selectQuantity();
			Log.message("5. Quantity drowdown is selected with Quantity '2' ");
			pdppage.clickAddToBag();
			Log.message("6. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to MiniCart page !");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. clicked on checkoutAsGuest Button ");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("10. clicked on yes Button for MultiShipping ");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on Add Link to add Address");
			LinkedHashMap<String, String> shippingDetails1 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_washington_address");
			Log.message("13. Shipping details filled in the shipping address form");
			List<String> indexes = new ArrayList<String>(shippingDetails1.keySet());
			String StateInAddress1 = indexes.get(5).replace("select_state_", "");
			String stateCode1 = StateUtils.getStateCode(StateInAddress1);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on 'Save' Button on Multi Shipping Address page");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("15. Clicked on 'Continue' Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.clickaddLink();
			LinkedHashMap<String, String> shippingDetails2 = shipToMultipleAddressPage
					.fillingShippingDetailsInMultiShip("NO", "valid_address_virgenia");
			Log.message("16. Shipping details filled in the shipping address form");
			List<String> index = new ArrayList<String>(shippingDetails2.keySet());
			String StateInAddress2 = index.get(5).replace("select_state_", "");
			String stateCode2 = StateUtils.getStateCode(StateInAddress2);
			// Clicking on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicked on 'Save' Button on Multi Shipping Address page ");
			// Clicking on 'Continue' button
			checkoutPage.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			Log.message("18. Clicked on 'Continue' Button on Address Validation popup ");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("19. Clicked on 'Save' Button on Multi Shipping Address page ");
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message("20. First Item's address dropdown is selected ");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message("21. Second Item's address dropdown is selected ");
			shipToMultipleAddressPage.clickContinue();
			Log.message("22. Clicked on continue Button !");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue Button in shipping page ");
			// filling billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_alaska");
			Log.message("24. Billing details filled as address 'alaska' ");
			// filling card details
			LinkedHashMap<String, String> cardType = checkoutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message("25. Card details filled as cardType 'Visa' !", driver);
			List<String> card = new ArrayList<String>(cardType.keySet());
			String cardTypeInBilling = card.get(0).replace("select_cardtype_", "");
			checkoutPage.clickOnContinueInBilling();
			Log.message("26. clicked on continue in Billing page !");
			BrowserActions.nap(20);
			// clicking on placeOrder Button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("27. clicked PlaceOrderButton in placeOrder page !");
			String stateCode1InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(0).get("State");
			String stateCode2InOrder = orderConfirmationPage.getShippingAddressInOrderSummary().get(1).get("State");
			String cardTypeInOrder = orderConfirmationPage.getPaymentMethodInOrderSummary().get("CardType");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Order should be properly placed for the added shipping address .");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(confirmationMessage, orderConfirmationPage),
					"Order placed Successfully on added shipping address !",
					"Order didn't placed Successfully on added shipping address !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(
					"Added multiple shipping address washington and virgenia should be properly displayed in Order confirmation screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(stateCode1.equals(stateCode1InOrder),
					"Added multiple shipping address washington properly displayed in Order confirmation screen.",
					"Added multiple shipping address washington properly not displayed in Order confirmation screen.",
					driver);
			Log.assertThat(stateCode2.equals(stateCode2InOrder),
					"Added multiple shipping address virgenia properly displayed in Order confirmation screen.",
					"Added multiple shipping address virgenia properly not displayed in Order confirmation screen.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b>");
			Log.message(
					"Added multiple shipping address with selected payment method master card should be properly displayed in Order Receipt screen");

			Log.message("<b>Actual Result 3: </b>");
			Log.assertThat(cardTypeInBilling.equals(cardTypeInOrder),
					"Added multiple shipping address with selected payment method master card properly displayed in Order Receipt screen !",
					"Added multiple shipping address with selected payment method master card not displayed in Order Receipt screen ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_132



	

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the 'Shipping Address' - 'In Store Pickup' section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_059(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			Log.message("<b>Expected Result 1:</b>");
			Log.message("<br>");
			Log.message("In Store Pickup Address and Phone Number should be displayed");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
							"txtInStorePickupFree", "txtInStoreName",
							"txtInStorePickupAddress", "txtInStorePhone",
							"txtInStoreState"), checkoutPage),
							"<b>Actual Result:</b> 'Store Address and Shipping Method is displayed",
							"<b>Actual Result:</b> 'Store Address and Shipping Method is not displayed",
							driver);

			Log.assertThat(
					checkoutPage.getInStoreAddress().get("ShippingMethod")
					.trim().equalsIgnoreCase("Free In-Store Pickup"),
					"<b>Actual Result:</b> 'Free In Store Pickup' is displayed",
					"<b>Actual Result:</b> 'Free In Store Pickup' is not displayed",
					driver);

			Log.message("<b>Expected Result 2:</b>");
			Log.message("<br>");
			Log.message("In Store Pickup Edit link should be enabled");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("LnkEditShipAddress"), checkoutPage),
							"<b>Actual Result:</b> 'Edit Link is Enabled for In-Store Free Pickup",
							"<b>Actual Result:</b> 'Edit Link is disabled for In-Store Free Pickup",
							driver);

			checkoutPage.clickEditShippingAddress();
			Log.message("5. Click on Edit Shipping Address");
			Log.message("<b>Expected Result 3:</b>");
			Log.message("<br>");
			Log.message("User should be able to Modify the Shipping Address");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
							"btnContinueShopping", "lnkChangeStore"),
							checkoutPage),
							"<b>Actual Result:</b> User able to Modify the In-Store Pickup ",
							"<b>Actual Result:</b> User not able to Modify the In-Store Pickup ",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop",	"mobile" }, description = "Verify system displays the item updated cart message - High Value Product", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String txtHighValueProduct = "Delivery of this item requires a signature.";
		boolean status = false;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "'");

			// Navigating to Pdp Page

			Log.message("3. Navigated to PDP!");


			pdppage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			ArrayList<String> txtLineItemMessage = shoppingBagPage.getTextFromLineItem();
			for(int index = 0 ; index<txtLineItemMessage.size(); index++){
				if(txtLineItemMessage.get(index).equals(txtHighValueProduct)){
					status = true;
					break;
				}
			}
			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b>  Cart Messages should be shown as 'Delivery of this item requires a signature'.");
			Log.assertThat(status,
					"<b>Actual Result: </b>  Cart Messages is shown as  : " + txtHighValueProduct,
					"<b>Actual Result: </b>  Cart Messages is not shown ",
					driver);
			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_005
	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Double Employee Discount in the Order Summary in the Place Order step of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_234(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to SignIn Page
			SignIn signinPage = homepage.headers.navigateToSignIn().get();
			Log.message("2. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("6. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Express", shippingaddress);
			Log.message("9. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("10. Click continue Shipping button shipping method page!");

			/*
			 * //Filling the CardDetails in the billing page
			 * checkoutPage.fillingCardDetails("YES",creditcard); Log.message(
			 * "11. Card details filled in the billing form!");
			 */
			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page
			float merchendisevalue = checkoutPage.getMerchandiseTotalValue();
			float assoicatediscount = checkoutPage.getAssociateDiscountValue();
			float firstDiscount = (merchendisevalue * 20 / 100);
			float secondDiscount = (merchendisevalue - firstDiscount) * 20 / 100;
			Log.message("<b>Expected Result</b> Associate Discount value should be calcualted correctly", driver);
			Log.assertThat((firstDiscount+secondDiscount) == assoicatediscount,
					"<b>Actual Result :</b> 'Assoicate Discount value is calculated correctly",
					"<b>Actual Result :</b> 'Assoicate Discount value is not calculated correctly");

			checkoutPage.placeOrder();
			Log.message(
					"13. User able to confirm the order with multiple payment option(Belk Gift card with other Credit cards)",
					driver);

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("PlaceOrderInHeaderEnabledInMultiShipping"), checkoutPage),
							"<b>Actual Result :</b> 'Enabled to go to the previous page",
					"<b>Actual Result :</b> 'Not Enabled to go to the previous page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Double Employee Discount Pacesetter in the Order Summary in the Place Order step of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_235(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to SignIn Page
			SignIn signinPage = homepage.headers.navigateToSignIn().get();
			Log.message("2. Navigated to 'Belk' SignIn Page from HomePage");

			// Login with Valid User creditinals
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. User Entered Credientials in User Name:" + emailid + "/Password:" + password + ".", driver);

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("6. " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Express", shippingaddress);
			Log.message("9. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("10. Click continue Shipping button shipping method page!");

			/*
			 * //Filling the CardDetails in the billing page
			 * checkoutPage.fillingCardDetails("YES",creditcard); Log.message(
			 * "11. Card details filled in the billing form!");
			 */
			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			// Clicking on Place order button in Order confirmation page
			float merchendisevalue = checkoutPage.getMerchandiseTotalValue();
			float assoicatediscount = checkoutPage.getAssociateDiscountValue();
			float firstDiscount = (merchendisevalue * 30 / 100);
			float secondDiscount = (merchendisevalue - firstDiscount) * 30 / 100;
			Log.message("<b>Expected Result</b> Associate Discount value should be calcualted correctly", driver);
			Log.assertThat((firstDiscount+secondDiscount) == assoicatediscount,
					"<b>Actual Result :</b> 'Assoicate Discount value is calculated correctly",
					"<b>Actual Result :</b> 'Assoicate Discount value is not calculated correctly");

			checkoutPage.placeOrder();
			Log.message(
					"13. User able to confirm the order with multiple payment option(Belk Gift card with other Credit cards)",
					driver);

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("PlaceOrderInHeaderEnabledInMultiShipping"), checkoutPage),
							"<b>Actual Result :</b> 'Enabled to go to the previous page",
					"<b>Actual Result :</b> 'Not Enabled to go to the previous page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Combination of Belk Rewards dollars & gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_116(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String excepted = "Belk Rewards Dollar";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");
			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");



			// filling the Belk Reward Dollar
			checkOutPage.fillingBelkRewardDollars("belk_Reward_Dollar13");
			checkOutPage.clickOnApplyBelkRewardDollars();
			Log.message((i++) +". Filled Belk Reward Dollar Succesfully!");
			// filling the Gift Card details
			checkOutPage.fillingBelkGiftCardDetails("card_GiftCard100");
			checkOutPage.clickOnApplyGiftCard();
			Log.message((i++) +". Filled Gift Card details Succesfully!");
			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Verify user can pay through Combination of Belk Reward Dollar with gift card");
			Log.assertThat(actual.equals(excepted),
					"<b>Actual Result :</b> User Can Pay The Amount In Of Combination of Belk Reward Dollar with gift card",
					"<b>Actual Result :</b> User Can't Pay The Amount In Of Combination of Belk Reward Dollar with gift card",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_116


	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Combination of Visa/master/discover/Belk credit card with gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_117(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String excepted = "Amex";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address1");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",
					"valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Gift Card details
			checkOutPage.fillingBelkGiftCardDetails("GiftCard4");
			checkOutPage.clickOnApplyGiftCard();
			Log.message((i++) +". Filled Gift Card details Succesfully!");

			// filling the Card address details
			BrowserActions.nap(10);
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			BrowserActions.nap(5);
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Combination of American Express with gift card");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b> User Can Pay The Amount In Of Combination of American Express with gift card",
					"<b>Actual Result :</b> User Can't Pay The Amount In Of Combination of American Express with gift card",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_117

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Combination of Belk credit card with gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_118(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String excepted = "Belk Rewards Credit Card";
		String emailaddress = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) +". Navigated to 'Belk' Home Page!");
			// Navigating to signIN page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailaddress,
					password);

			ShoppingBagPage bagPage = myAccountPage.miniCartPage
					.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();
			}
			else
				bagPage.headers.clickSignOut();


			// Navigated to Gift Card Page
			GiftCardsPage GiftCardPage = homePage.clickOnGiftCardLink();
			Log.message((i++) +". Navigated to Gift Card Page!");

			if(GiftCardPage.elementLayer.verifyPageElements(Arrays.asList("clickDropDown"), GiftCardPage))
				GiftCardPage.clickOnSelectValue(2);
			// Add product to shopping bag
			GiftCardPage.clickAddToBag();
			Log.message((i++) +". Product added to Shopping Bag!");

			// Navigated to shopping bag page
			ShoppingBagPage shoppingBagPage = GiftCardPage.clickOnMiniCart();
			Log.message((i++) +". Navigated to 'Mini Cart'!");

			// Navigated to Check Out Page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) +". Navigated to SignIn Page to Checkout as Login User!");

			// filling the Email_Id and Passworder details
			//	signIn.enterEmailID(emailaddress);
			//	signIn.enterPassword(password);
			//	signIn.clickBtnSignIn();
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(emailaddress, password);
			Log.message((i++) +". Filling Valid User_id and Password!");
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Express", "valid_address2");
			Log.message((i++) +". Shipping address details filled Succesfully!");

			// Clicking on continue in shipping
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) +". Navigated to Billing page from shipping page!");

			// filling the Billing address details
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",
					"valid_address1");
			Log.message((i++) +". Filled billing details Succesfully!");

			// filling the Gift Card details
			checkOutPage.fillingBelkGiftCardDetails("GiftCard4");
			checkOutPage.clickOnApplyGiftCard();
			Log.message((i++) +". Filled Gift Card details Succesfully!");

			// filling the Card address details
			BrowserActions.nap(5);
			checkOutPage.fillingBelkRewardCardDetails("NO","card_BelkRewardsCreditCard1");
			Log.message((i++) +". Filled the payment card details Succesfully!");

			// Clicking on continue in Billing Page
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) +".  Navigated to Place Order page from Billing Page!");

			// Clicking on continue in Place Order Page
			OrderConfirmationPage OrderconfirmationPage = checkOutPage.placeOrder();
			Log.message((i++) +".  Navigated to Place Order Page!");

			// Get Card Type From Place Order
			String actual = OrderconfirmationPage.GetCardType();
			Log.message((i++) +".  Getting Card Type!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b> Verify user can pay through Combination of Belk credit card with gift card");
			Log.assertThat(
					actual.equals(excepted),
					"<b>Actual Result :</b> User Can Pay The Amount In Of Combination of Belk credit card with gift card",
					"<b>Actual Result :</b> User Can Not Able To Pay The Amount In Of Through Combination of Belk credit card with gift card",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_118


	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify Bonus Offers GWP Modal in Cart page for products eligible for a Choice Bonus Product Discount", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_227(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instances
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Searching product in my account page with a productId
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with Keyword '" + searchKey + "' and Navigated to Search Result Page");
			Log.message("3. Navigated to PDP");
			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdpPage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("5. Selected size:'" + pdpPage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. The Quantity is selected ");

			// Adding product to bag
			pdpPage.clickAddToBag();
			Log.message("6. Clicked on 'Add to bag' button");



			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			Log.message("<b> Expected Result 1: </b>  The “Free Gift with Purchase” message should displayed");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("gpwProductMessage"),
							shoppingBagPage),
							"<b> Actual Result 1: </b> The “Free Gift with Purchase” message is displayed: " + shoppingBagPage.getgpwMessage(),
							"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Free Gift  message is not displayed ", driver);

			shoppingBagPage.clickOnViewDetails();
			Log.message("<b> Expected Result 2: </b> Bonus Offers GWP Modal should get displayed in shopping cart page.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("bonusproductmodalOpened"),
							shoppingBagPage),
							"<b> Actual Result 2: </b> Bonus Offers GWP Modal get displayed in shopping cart page.: ",
							"<b> Actual Result 2: </b>  Bonus Offers GWP Modal not get displayed in shopping cart page. ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_227
	@Test(groups = { "desktop" }, description = "Verify free shipping message is not displayed in PLP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_204(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1],category[2]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1] );
			PlpPage plppage = new PlpPage(driver).get();

			Log.message("3. Navigated to plp page");						
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> For Qualified product Text message should not be displayed in plp");
			Log.assertThat(
					plppage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("txtFreeShipping"), plppage),
							"<b>Actual Result:</b> Free shipping message not displayed for the qualified product in plp",
							"<b>Actual Result:</b> Free shipping message displayed for the qualified product in plp",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	

	}
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Offers & Rebates Messaging is properly displayed in PDP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_195(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homepage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey   + "' and navigated to PDP");

			pdpPage.clickOnViewDetails();

			Log.message("<b>Expected Result :</b> Offers & Rebates Messaging Tab should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("offersandRebatesTab"), pdpPage),
					"<b>Actual Result :</b> Offers & Rebates Messaging Tab displayed",
					"<b>Actual Result :</b> Offers & Rebates Messaging Tab not displayed");

			Log.message("<b>Expected Result :</b> Offers Available: header should be properly shown.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("offersandRebateHeading"), pdpPage),
					"<b>Actual Result :</b> Header properly shown.",
					"<b>Actual Result :</b> Header not properly shown.");

			Log.message("<b>Expected Result :</b> Message should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElements(Arrays.asList("offersandRebateMessage"), pdpPage),
					"<b>Actual Result :</b> Message displayed",
					"<b>Actual Result :</b> Message not displayed");

			Log.message("<b>Expected Result :</b> Disclaimer text should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElements(Arrays.asList("offersandRebateDisclaimertxt"), pdpPage),
					"<b>Actual Result :</b> Disclaimer text displayed",
					"<b>Actual Result :</b> Disclaimer text not displayed");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}//TC_BELK_CHECKOUT_195
	@Test(groups = { "desktop", "mobile" }, description = "Verify order summary total is properly calculated.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")

	public void TC_BELK_CHECKOUT_139(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");
		String[] address = testData.get("Address").split("\\|");
		// Get the web driver instance
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search different products like jackets & coats and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {
				pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message((i++) + ". Searched with '" + searchKey + "'");
				Log.message((i++) + ". Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize + "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName + "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}
			// Navigating to 'ShoppingBag' Page

			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage
					.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest User in Sign In Page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on 'Yes' button in Shipping address Page ");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Clicked on 'Continue' in multiship address Page ");

			checkoutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on 'Continue' in multiship method Page ");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message((i++) + ". Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message((i++) + ". Card details are fillied in the billing page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on Continue in Billing page! ");

			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage
					.placeOrder();

			Log.message("<br>");

			Log.message("<b>Expected Result :</b> Estimated order total should be properly displayed in Order confirmation screen.");
			Log.assertThat(
					driver.getCurrentUrl().contains("COSummary-Submit")
					&& orderconfirmationpage.elementLayer
					.verifyPageElements(
							Arrays.asList("orderNumber"),
							orderconfirmationpage),
							"<b> Actual Result :</b> Estimated order total is displayed properly in Order confirmation screen ",
					"<b> Actual Result :</b> Estimated order total is not displayed properly in Order confirmation screen");

			Log.message("<br>");
			String merchandiseTotal = orderconfirmationpage
					.getTextFromOrderSummary().replaceAll("\\$", "").trim();
			double merchandiseValue = Double.parseDouble(merchandiseTotal);
			Log.message((i++) + ". Got the text from merchandise Value and the value is :"
					+ merchandiseValue);

			String shipment = orderconfirmationpage.getTextFromShipping();
			shipment = shipment.replaceAll("\\$", "");
			double	shipValue = Double.parseDouble(shipment);
			Log.message((i++) + ". Got the text from shipping Value and the value is:"
					+ shipValue);

			String salesTax = orderconfirmationpage.getTextFromSalesTax();
			salesTax = salesTax.replaceAll("\\$", "");
			double salesTaxValue = Double.parseDouble(salesTax);
			Log.message((i++) + ". Got the text From Sales Tax and the value is :"
					+ salesTax);

			String shipSurCharge = orderconfirmationpage
					.getTextFromShipSurCharge();
			shipSurCharge = shipSurCharge.replaceAll("\\$", "");
			double shipSurChargeValue = Double.parseDouble(shipSurCharge);
			Log.message((i++) + ". Got the text From shipSurCharge and the value is :"
					+ shipSurChargeValue);

			String orderTotal = orderconfirmationpage.getTextFromOrderTotal()
					.replaceAll("\\$", "");
			double orderTotalValue = Double.parseDouble(orderTotal);
			Log.message((i++) + ". Got the text From OrderTotal and the value is"
					+ orderTotalValue);
			Log.message("<br>");
			Log.message("<b>Expected Result : </b> Estimated order total should properly calculated for the different billing and shipping address");
			Log.assertThat(
					orderTotalValue == merchandiseValue + salesTaxValue
					+ shipValue + shipSurChargeValue,
					"<b>Actual Result : </b> Estimated order total is calculated properly for the different billing and shipping address",
					"<b>Actual Result : </b> Estimated order total is not calculated properly for the different billing and shipping address",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_139





	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify Deal Based Offer promotion Buy 1, get 1 50% off applied displayed for Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_192(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			// Navigating to Pdp Page
			PdpPage pdppage = searchResultPage.navigateToPDPByRow(2);
			Log.message("3. Navigated to PDP Page!!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Promotion message should be displayed in Cart page with below details");
			//	Log.assertThat(),
			//				"<b>Actual Result :</b> ",
			//				"<b>Actual Result :</b> ",
			//				driver);

			String selectedSize=pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdppage.clickAddToBag();
			Log.message("6. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("7. Naviagte to Shopping bag");

			String msg = shoppingBagPage.getPromoMessageInShoppingBagPage();
			Log.message("</br>");
			//		Log.message(
			//				"<b>Expected Result :</b> Promotion message should be displayed in Cart page with below details");
			//		Log.assertThat(),
			//				"<b>Actual Result :</b> ",
			//				"<b>Actual Result :</b> ",
			//				driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_192








	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify Deal Based Offer promotion applied displayed for Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_208(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			// Navigating to Pdp Page
			PdpPage pdppage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!!");

			String selectedSize=pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdppage.selectQtyByIndex(3);
			Log.message("6. Selected '3' quantity from quantity dropdown in the PDP page");

			pdppage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			String msg = shoppingBagPage.getPromoMessageInShoppingBagPage();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Promotion message should be displayed in Cart page with below details");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("finalPromoMessage"), shoppingBagPage),
					"<b>Actual Result :</b> Promotion message displayed in Cart page as:"+ msg,
					"<b>Actual Result :</b> Promotion message not displayed in Cart page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_208



	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify Deal Based Offer promotion Buy 1, get 1 50% off applied displayed for Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_209(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			// Navigating to Pdp Page
			PdpPage pdppage = searchResultPage.navigateToPDPByRow(2);
			Log.message("3. Navigated to PDP Page!!");

			String selectedSize=pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			pdppage.clickAddToBag();
			Log.message("6. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("7. Naviagte to Shopping bag");

			String msg = shoppingBagPage.getPromoMessageInShoppingBagPage();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Promotion message should be displayed in Cart page with below details");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("finalPromoMessage"), shoppingBagPage),
					"<b>Actual Result :</b> Promotion message displayed in Cart page as:"+ msg,
					"<b>Actual Result :</b> Promotion message not displayed in Cart page.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_209



	




	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify price of single variation product is displayed in Mini Cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_148(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			PlpPage plpPage = new PlpPage(driver);

			// Navigating to Pdp Page
			PdpPage pdppage = plpPage.navigateToPDPByRow(2);
			Log.message("3. Navigated to PDP Page!!");

			String colorName = pdppage.selectColor();
			Log.message("4. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize = pdppage.selectSize();
			Log.message("5. Selected size: '" + selectedSize + "' from  the PDP page");

			String selectedQuantity = pdppage.selectQuantity();
			Log.message("6. Selected quantity: '" + selectedQuantity + "' from  the PDP page");

			pdppage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");

			String Price = shoppingBagPage.getStandardPriceMsg();
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Single Variation product price should be properly displayed in Mini Cart screen");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtStandardPrice"), shoppingBagPage),
					"<b>Actual Result :</b> Single Variation Product price is properly displayed and the price is displayed as '"+ Price +"'.",
					"<b>Actual Result :</b> Single Variation Product price is not properly displayed. ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_148

	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify the price of a variation master range product is displayed in Mini Cart screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_149(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			PlpPage plpPage = new PlpPage(driver);

			// Navigating to Pdp Page
			PdpPage pdppage = plpPage.navigateToPDPByRow(3);
			Log.message("3. Navigated to PDP Page!!");


			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize = pdppage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from  the PDP page");

			String selectedQuantity = pdppage.selectQuantity();
			Log.message("5. Selected quantity: '" + selectedQuantity + "' from  the PDP page");

			pdppage.clickAddToBag();
			Log.message("6. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("7. Naviagte to Shopping bag");

			String Price = shoppingBagPage.getTextNowPrice();
			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Variation master range product price range should be displayed in Mini Cart screen. ");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtNowPrice"), shoppingBagPage),
					"<b>Actual Result :</b> Variation master range product price range is properly displayed and the price is displayed as '"+ Price +"'.",
					"<b>Actual Result :</b> Variation master range product price range is not properly displayed. ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_149

	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify Purchase with Purchase promotion is properly displayed in My Bag screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_171(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered the valid credentials to login and navigated to 'MyAccount' Page!");

			PdpPage pdppage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to PDP Page! ");

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize=pdppage.selectSize();
			Log.message("6. Selected size: '" + selectedSize + "' from  size dropdown the PDP page");

			pdppage.selectQuantity();
			Log.message("7. Selected quantity as '1' from quantity dropdown in the PDP page");

			pdppage.clickAddToBag();
			Log.message("8. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page!");

			String msg = shoppingBagPage.getPromoMessageInShoppingBagPage();

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> In my bag screen, for the selected item, deal based offer promotion should be displayed properly. ");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("finalPromoMessage"), shoppingBagPage),
					"<b>Actual Result 1:</b> Deal Based Offer promotion is properly displayed on the my bag screen as "+msg,
					"<b>Actual Result 1:</b> Deal Based Offer promotion is not displayed on the my bag screen",
					driver);

			Log.message("</br>");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to Checkout Page as 'Signed' user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address2");
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("11. Filled Shipping details as Signed user");

			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' in Shipping Page!");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address3");
			checkoutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("13. Filled Billing page details as Signed user");

			checkoutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' in Billing Page!");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("15. Navigated to Order receipt Page!");

			Log.message("</br>");
			Log.message("<b>Expected Result 3:</b> Order should be properly confirmed and deal based offer promotion message should be properly shown. ");
			//	    Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList(""), shoppingBagPage),
			//                       "<b>Actual Result 3:</b> Deal Based Offer promotion is properly displayed on the my ",
			//					   "<b>Actual Result 3:</b> Deal Based Offer promotion is not displayed on the my ",
			//						driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_171

	

	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify PRODUCT combination of products + choice of bonus products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_178(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered the valid credentials to login and navigated to 'MyAccount' Page!");

			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			PlpPage plpPage = new PlpPage(driver);

			// Navigating to Pdp Page
			PdpPage pdppage = plpPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!!");


			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize=pdppage.selectSize();
			Log.message("6. Selected size: '" + selectedSize + "' from  size dropdown the PDP page");

			//pdppage.selectQtyByIndex(1);
			Log.message("7. Selected quantity as '1' from quantity dropdown in the PDP page");

			String msg = pdppage.getPromotionalCalloutMessage();
			Log.message("   The promotional message is displayed on the PDP page as '"+ msg +"' .");

			pdppage.clickAddToBagButton();
			Log.message("8. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page!");



			String promoMsg = shoppingBagPage.getPromoMessageInShoppingBagPage();



			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> In my bag screen Bonus Offers GWP PYG should be displayed properly. ");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("finalPromoMessage"), shoppingBagPage),
					"<b>Actual Result 1:</b> Deal Based Offer promotion is properly displayed on the my bag screen as '"+promoMsg ,
					"<b>Actual Result 1:</b> Deal Based Offer promotion is not displayed on the my bag screen",
					driver);
			Log.message("</br>");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to Checkout Page as 'Signed' user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Express", "valid_address2");
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("11. Filled Shipping details as Signed user");

			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' in Shipping Page!");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address3");
			checkoutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message("13. Filled Billing page details as Signed user");

			checkoutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' in Billing Page!");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("15. Navigated to Order receipt Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Order should be properly purchased with Pick Your Gift. ");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderConfirmationPage.GetOrderNumber();

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"), orderConfirmationPage),
					"<b> Actual Result 2: </b> Order placed after clicking on place order button and Order number is generated: "
							+ ordernumb,
							"<b> Actual Result 2: </b> Order is not placed after clicking on place order button. ", driver);
			Log.message("<br>");

			Log.message("</br>");
			//		Log.message("<b>Expected Result 3:</b> Order should be properly confirmed and deal based offer promotion message should be properly shown. ");
			//			    Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList(""), shoppingBagPage),
			//		                       "<b>Actual Result 3:</b> Deal Based Offer promotion is properly displayed on the my ",
			//							   "<b>Actual Result 3:</b> Deal Based Offer promotion is not displayed on the my ",
			//								driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_178

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify that the respective Cart Merging message is displayed.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_018(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lnkReturnToShoppingBag");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			pdpPage.selectQtyByIndex(2);
			pdpPage.clickAddToBag();

			Log.message("4. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Navigated to shopping bag page");
			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("6. Searched with '" + searchKey+ "'");
			Log.message("7. Navigated to PDP");
			pdpPage.selectQtyByIndex(3);
			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Error Message should be displayed when we select more quantity which is not available.");		
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblErrorMsg"), pdpPage),
					"<b>Actual Result :</b> 'Error Message is displayed when we select more quantity which is not available",
					"<b>Actual Result :</b> 'Error Message is not displayed when we select more quantity which is not available");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}




	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Associate Discount Amount in Place Order page of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_237(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String shippingaddress = testData.get("Address");
		String giftcard = testData.get("Payments");
		String couponCode = testData.get("CouponCode");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {

			cleanup(driver, emailid, password);
			Log.message("1. Lanch the Belk website");
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();

			Log.message("2. Cleanup the Shopping Bag page");
			// Searching product in my account page with a productId
			PdpPage pdpPage = shoppingBagPage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("3. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page");

			// Select Size, Color, Qty
			pdpPage.addProductToBag("1");
			Log.message("4. Product " + pdpPage.getProductName()
					+ "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("6. Entered Coupon Code and clicked on 'Apply coupon' button");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Navigate to checkout page");

			// Select only first address from the dropdown
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES",
					"Express", shippingaddress);
			Log.message("8. Address selected from Address dropdown!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("9. Navigate to Billing Address page");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("10. Click continue Shipping button shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingBelkRewardCardDetails("NO",
					"card_BelkRewardsCreditCard");
			Log.message("11. Card details filled in the billing form!");

			// Filling the Belk Gift Card details
			checkoutPage.fillingBelkGiftCardDetails(giftcard);
			checkoutPage.clickOnApplyGiftCard();

			Log.message("11. Belk Gift card is applied !");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click continue button in Billing address page!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");

			String[] discountlabel = checkoutPage
					.getdiscountvalueFromOrderSummary();
			boolean status = false;
			for (int i = 0; i < discountlabel.length; i++) {
				if (discountlabel[i].contains("Associate")) {
					status = true;
					break;
				}
			}
			Log.message("<b>Expected Result</b>Associate Discount should be displayed in the Place Order Page");
			Log.assertThat(
					status,
					"<b>Actual Result :</b> 'Associate Discount is displayed in the Place Order  Page",
					"<b>Actual Result :</b> 'Associate Discount is not displayed in the Place Order Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Associate Discount Amount in Shipping and Billing Address page of checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_236(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String couponCode = testData.get("CouponCode");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {

			cleanup(driver, emailid, password);
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();
			Log.message("1. Lanch the Belk website");

			Log.message("2. Cleanup the Shopping Bag page");
			// Searching product in my account page with a productId
			PdpPage pdpPage = shoppingBagPage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("3. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page");

			// Select Size, Color, Qty
			pdpPage.addProductToBag("1");
			Log.message("4. Product is added to the cart");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("6. Entered Coupon Code and clicked on 'Apply coupon' button");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Navigate to checkout page");

			Log.message("8. Verify the Associate Discount is displayed in the Shipping Address Page or not");
			String[] discountlabelShipping = checkoutPage
					.getdiscountvalueFromOrderSummary();

			boolean status = true;
			if (discountlabelShipping.length >= 1) {
				for (int i = 1; i <= discountlabelShipping.length; i++) {
					if (discountlabelShipping[i - 1].contains("Associate")) {
						status = false;
						break;
					}
				}
			}
			Log.assertThat(
					status,
					"<b>Actual Result :</b> 'Associate Discount is not displayed in the Shipping Address Page",
					"<b>Actual Result :</b> 'Associate Discount is displayed in the Shipping Address Page");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("9. Click on Continue button in Shipping Address Page and navigate");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			String[] discountlabelBilling = checkoutPage
					.getdiscountvalueFromOrderSummary();
			boolean status1 = true;
			for (int i = 0; i < discountlabelBilling.length; i++) {
				if (discountlabelBilling[i].contains("Associate")) {
					status1 = false;
					break;
				}
			}
			Log.assertThat(
					status1,
					"<b>Actual Result :</b> 'Associate Discount is not displayed in the Billing Page",
					"<b>Actual Result :</b> 'Associate Discount is displayed in the Billing Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify payment method enabled when increasing the quantity after applying the reward card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_142(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] address = testData.get("Address").split("\\|");
		String payment = testData.get("Payments");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 3 different products

			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("3");
			Log.message("6. selected the '" + quantity + "' quantity !");
			String productPrice = pdpPage.getProductPrice();
			Log.message("obtained product price");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn signInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = signInPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);

				if (checkoutPage.isAddressValidationModalShowing()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.ContinueAddressValidationModalInMultipleShipping();
				}

			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();


			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("26. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("27. Clicked on continue button in multiShipping page");

			checkoutPage.clickOnEditInOrderSummary();
			Log.message("30. Clicked on 'Edit'link in the billing page!");

			shoppingBagPage.removeItemsByIntex(0);
			shoppingBagPage.removeItemsByIntex(0);
			Log.message("31. removed one item from bag");
			shoppingBagPage.selectQuantity("4");
			Log.message("26. selected the quantity 3 from the dropdown");
			int productCount = Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", ""));
			Log.message("32. obtained product count as " + productCount);
			shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("33. Navigated to checkout Login page");
			// Navigating to checkout page as user
			signInPage.clickOnCheckoutAsGuest();
			Log.message("34. Clicked on Checkout As Guest User in Sign In Page");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("35. Clicked on 'YES' Button in Ship to Multiple Address Page");
			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("20. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address7");

			if (checkoutPage.isAddressValidationModalShowing()) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("21. filled address for 3rd product and clicked save button");
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("37. Clicked on continue button in multiaddress ");
			String txtShippingMethod = checkoutPage.getTextShippingMethodInMultiShipping();
			Log.message("38. obtained Shipping method as " + txtShippingMethod);
			checkoutPage.clickOnContinueInShipping();
			Log.message("39. Clicked on continue button in multiShipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("40. Billing address details are filled in billing page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Payment method pane should be enabled in billing address screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblPaymentMethod"), checkoutPage),
					"Payment method pane enabled in billing address screen",
					"Payment method pane not enabled in billing address screen", driver);
			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", payment);
			Log.message("41. Card details are fillied in the billing page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("estimated order total should be recalculated");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(checkoutPage.verifyOrderTotalInOrderSummary(),
					"estimated order total is succesfully recalculated",
					"estimated order total is not succesfully recalculated", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("shipping address should be shown with shipping address #1");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("navigateShippingAddress"),
					checkoutPage), "shipping address is shown", "shipping address is not shown", driver);
			checkoutPage.clickOnContinueInBilling();
			Log.message("42. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("43. Clicked Place Order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order should be properly placed from Place order screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage != null,
					"Order is properly placed from Place order screen",
					"Order is not properly placed from Place order screen", driver);
			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();
			double productOfpriceAndQuantity = orderConfirmationPage.calculatePrice(productCount, productPrice);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("quantity and price should be updated as per the user changes");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(Double.compare(merchandiseTotal, productOfpriceAndQuantity) == 0,
					"quantity and price updated as per the user changes",
					"quantity and price not updated as per the user changes", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_142

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify payment method enabled when decreasing the quantity after applying the reward card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_143(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] address = testData.get("Address").split("\\|");
		String payment = testData.get("Payments");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 3 different products

			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("3");
			Log.message("6. selected the '" + quantity + "' quantity !");
			String productPrice = pdpPage.getProductPrice();
			Log.message("obtained product price");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			SignIn signInPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = signInPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);

				if (checkoutPage.isAddressValidationModalShowing()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.ContinueAddressValidationModalInMultipleShipping();
				}

			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			//
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("22. Clicked on continue button in multiaddress ");
			checkoutPage.clickOnContinueInShipping();
			Log.message("23. Clicked on continue button in multiShipping page");
			//
			checkoutPage.clickOnEditInOrderSummary();
			Log.message("24. Clicked on 'Edit'link in the billing page!");

			shoppingBagPage.removeItemsByIntex(0);
			Log.message("25. removed one item from bag");

			int productCount = Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", ""));
			Log.message("27. obtained product count as " + productCount);
			shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("28. Navigated to checkout Login page");
			// Navigating to checkout page as user
			signInPage.clickOnCheckoutAsGuest();
			Log.message("29. Clicked on Checkout As Guest User in Sign In Page");
			checkoutPage.selectAddrInAddressDropDownInMultiship();	



			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("38. Clicked on continue button in multiaddress ");
			// String txtShippingMethod =
			// checkoutPage.getTextShippingMethodInMultiShipping();
			// Log.message("39. obtained Shipping method as " +
			// txtShippingMethod);
			checkoutPage.clickOnContinueInShipping();
			Log.message("39. Clicked on continue button in multiShipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("40. Billing address details are filled in billing page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Payment method pane should be enabled in billing address screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblPaymentMethod"), checkoutPage),
					"Payment method pane enabled in billing address screen",
					"Payment method pane not enabled in billing address screen", driver);
			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", payment);
			Log.message("41. Card details are fillied in the billing page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("estimated order total should be recalculated");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(checkoutPage.verifyOrderTotalInOrderSummary(),
					"estimated order total is succesfully recalculated",
					"estimated order total is not succesfully recalculated", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("shipping address should be shown with shipping address #1");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("navigateShippingAddress"),
					checkoutPage), "shipping address is shown", "shipping address is not shown", driver);
			checkoutPage.clickOnContinueInBilling();
			Log.message("42. Clicked continue button in billing page");
			// click on place order button
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message("43. Clicked Place Order button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order should be properly placed from Place order screen");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage != null,
					"Order is properly placed from Place order screen",
					"Order is not properly placed from Place order screen", driver);

			double merchandiseTotal = orderConfirmationPage.getMerchandiseTotal();
			double productOfpriceAndQuantity = orderConfirmationPage.calculatePrice(productCount, productPrice);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("quantity and price should be updated as per the user changes");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(Double.compare(merchandiseTotal, productOfpriceAndQuantity) == 0,
					"quantity and price updated as per the user changes",
					"quantity and price not updated as per the user changes", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_143

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify call out message displayed for not qualified product!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_200(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");

			// Navigating to PDP page
			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQuantity();
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag!");

			// Click on Mini cart(View bag)
			ShoppingBagPage shoppingbag = pdpPage.clickOnMiniCart();
			Log.message("5. Clicked on Minicart(View bag) button!");

			//Click On View Details
			shoppingbag.clickOnViewDetails();
			Log.message("6. Clicked On View Details!");
			String Descrption =shoppingbag.getDescriptionOfGift();

			Log.message("<br>");
			Log.message(
					"<b> Expected Result :<b> The callOutMsg followed by a 'view details' should be properly shown!");
			Log.assertThat(
					shoppingbag.elementLayer.verifyPageElements(Arrays.asList("viewDetails"), shoppingbag),
					"<b> Actual Result :<b> Clicked On View Details and Message is displayed :" + Descrption ,
					"<b> Actual Result :<b> Clicked On View Details and Message is Not displayed",driver);

			//Click on Close Pop up
			shoppingbag.clickOnclosePopUp();
			Log.message("7. Clicked On [X] in Pop Up!");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_200

	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify call out message displayed for the qualified product!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_201(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");

			// Navigating to PDP page
			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQtyByIndex(2);
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag!");

			//Selecting free Gift 
			pdpPage.selectFreeGiftItem();
			Log.message("5. Selected Free Gift!");

			// Click on Mini cart(View bag)
			ShoppingBagPage shoppingbag = pdpPage.clickOnMiniCart();
			Log.message("6. Clicked on Minicart(View bag) button!");
			String Message = shoppingbag.getgpwMessage();

			//Click On View Details
			//shoppingbag.clickOnViewDetails();
			//Log.message("6. Clicked On View Details!");
			//Not Configured In BM Till Yet 

			//		Log.message("<br>");
			//		Log.message(
			//				"<b> Expected Result :<b> The callOutMsg should be properly shown In Shopping Bag Page!");
			//		Log.assertThat(
			//				shoppingbag.elementLayer.verifyPageElements(Arrays.asList("viewDetails"), shoppingbag),
			//				"<b> Actual Result :<b> Callout MEssage is Properly Displayed in Shopping Bag Page :" + Message ,
			//				"<b> Actual Result :<b> Callout MEssage is Properly Displayed in Shopping Bag Page!",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_201




	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify call out message displayed for the qualified product!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_202(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			// Navigate to Belk Home Page
			HomePage homepage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homepage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");

			// Navigating to PDP page
			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product!");

			// Select Size, Color, Qty
			pdpPage.selectSize();
			pdpPage.selectQtyByIndex(2);
			pdpPage.clickAddToBag();
			Log.message("4. " + pdpPage.getProductName() + "Product is added into Shopping bag!");

			//Select Free Gift With Product
			pdpPage.selectFreeGiftItem();
			Log.message("5. Clicked Free Gift with Gift!");

			// Click on Mini cart(View bag)
			ShoppingBagPage shoppingbag = pdpPage.clickOnMiniCart();
			Log.message("6. Clicked on Minicart(View bag) button!");
			String Message = shoppingbag.getgpwMessage();

			Log.message("<br>");
			Log.message(
					"<b> Expected Result :<b> Free Gift should be properly shown In Shopping Bag Page!");
			Log.assertThat(
					shoppingbag.elementLayer.verifyPageElements(Arrays.asList("giftImage"), shoppingbag),
					"<b> Actual Result :<b> Callout MEssage is Properly Displayed in Shopping Bag Page :" + Message ,
					"<b> Actual Result :<b> Callout MEssage is Properly Displayed in Shopping Bag Page!",driver);


			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_202


	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify PRODUCT, with amount of qualifying products + choice of bonus products!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_176(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			Log.message("3. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page
			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdppage.selectQtyByIndex(2);
			Log.message("6. The Quantity is selected!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");

			pdppage.selectFreeGiftItem();
			Log.message("8. Select the Free Gift Item!");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			String freegiftAdded=shoppingBagPage.freeGiftAddedMessage();

			Log.message("<b> Expected Result 1: </b>  In my bag screen Bonus Offers GWP PYG should be displayed properly!");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lblFreeGiftMessage"),shoppingBagPage),
					"<b> Actual Result 1: </b> Navigate to the Shopping Bag page, the Free Gift message is displayed and the message is:  "+ freegiftAdded,
					"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Free Gift  message is not displayed ", driver);

			String UpcInShoppingBagPage = shoppingBagPage.getUPCFreeProduct();
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message("12. Entered Shipping address as guest user");

			checkOutPage.clickOnContinueInShipping();
			Log.message("13. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("14. Selected Original address in the modal");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("15. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("16. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("17. Entered card details!");

			checkOutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on 'Continue' Button in Billing Page");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("19. Clicked on 'Continue' Button in Validation Modal!");

			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("20. Clicked on the Place Order Button!");
			String UPCInorderconfimation = orderConfirmationPage.getPromoMsgforFreeGiftItem();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Order should be properly purchased with Pick Your Gift!");
			Log.assertThat(UPCInorderconfimation.equals(UpcInShoppingBagPage),
					"<b>Actual Result 2: </b> Order is placed successfully with Free Gift and navigated to order receipt page!",
					"<b>Actual Result 2: </b> Order is not placed successfully with Free Gift card and navigated to order receipt page!",
					driver);


			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_176

	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify PRODUCT, with number of qualifying products + choice of bonus products!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_177(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			// Navigating to Pdp Page
			Log.message("3. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			// selecting size from Size Dropdown in Pdp Page
			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdppage.selectQtyByIndex(4);
			Log.message("6. The Quantity is selected!");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");

			pdppage.selectFreeGiftItem();
			Log.message("8. Select the Free Gift Item!");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			String freegiftAdded=shoppingBagPage.freeGiftAddedMessage();

			Log.message("<b> Expected Result 1: </b>  In my bag screen Bonus Offers GWP PYG should be displayed properly!");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lblFreeGiftMessage"),shoppingBagPage),
					"<b> Actual Result 1: </b> Navigate to the Shopping Bag page, the Free Gift message is displayed and the message is :  "+freegiftAdded,
					"<b> Actual Result 1: </b> Navigate to the Shopping Bag page and the Free Gift  message is not displayed!", driver);

			String UpcInShoppingBagPage = shoppingBagPage.getUPCFreeProduct();
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Express");
			Log.message("12. Entered Shipping address as guest user!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("13. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("14. Selected Original address in the modal");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("15. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("16. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("17. Entered card details!");

			checkOutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on 'Continue' Button in Billing Page!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("19. Clicked on 'Continue' Button in Validation Modal!");

			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("20. Clicked on the Place Order Button!");
			String UPCInorderconfimation = orderConfirmationPage.getPromoMsgforFreeGiftItem();
			String QTY =orderConfirmationPage.getQtyforFirstProduct();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Order should be properly purchased with Pick Your Gift!");
			Log.assertThat(UPCInorderconfimation.equals(UpcInShoppingBagPage),
					"<b>Actual Result 2: </b> Order is placed successfully with Free Gift and navigated to order receipt page with QTY is :"+ QTY+ "UPC is :" + UPCInorderconfimation,
					"<b>Actual Result 2: </b> Order is not placed successfully with Free Gift card and navigated to order receipt page!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_177

	@Test(groups = { "desktop","mobile" }, description = "Verify sales tax is properly calculated in Order summary panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_135(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage  = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'" );
			Log.message("3. Navigated to PDP");

			// Select the size,color & qty.

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize + "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");


			// Navigated to shoppingbag page after clicking on minicart icon in
			// pdp page
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping bag' page. ");

			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("9. Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");

			// Filling the shipping details as a GuestUser
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Entered Shipping Address Details as Guest");

			// clicking on continue in shipping page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.clickContinueInAddressValidationModal();
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue in Shipping Address Details as Guest");

			float merchandiseTotal = checkoutPage.getMerchandiseTotalValue();
			String merchandise = String.valueOf(merchandiseTotal);
			Log.message("   Merchandise Total :"+merchandiseTotal);	

			String shipping = checkoutPage.getShippingRateInOrderConfirmationPage().trim().replace("$"," ");
			float shippingCost =  Float.parseFloat(shipping);
			Log.message("    Shipping Cost :"+ shippingCost);

			String sales = checkoutPage.GetSalesTaxValue().trim().replace("$"," ");
			float salesTax = Float.parseFloat(sales);
			Log.message("    Sales tax :"+salesTax);

			String order = checkoutPage.GetOrderTotalValue().trim().replace("$", " ");
			float orderTotal = Float.parseFloat(order);
			Log.message("    Order total :"+orderTotal);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Estimated Sales tax amount should be properly added in Order summary panel.");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtTotalOrderValure"), checkoutPage),
					"<b> Actual Result 1:</b> Estimated Sales tax amount is properly added in Order summary panel. Sales Tax: "+sales,
					"<b> Actual Result 1:</b> Estimated Sales tax amount is not properly added in Order summary panel. ",driver);
			Log.message("<br>");


			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Estimated Order Total is properly calculated based on the sales tax added in Order Summary pane.");
			Log.assertThat(orderTotal==(merchandiseTotal+shippingCost+salesTax),
					"<b> Actual Result 2:</b> Estimated Sales tax amount is properly added in Order summary panel. Sales Tax: "+sales,
					"<b> Actual Result 2:</b> Estimated Sales tax amount is not properly added in Order summary panel. ",driver);
			Log.message("<br>");

			// Filling billing address details as a guest user
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("13. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details are fillied in the billing page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue in Billing page! ");


			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Order Sales tax should be properly displayed in place order screen.");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtTotalOrderValure"), checkoutPage),
					"<b> Actual Result 3:</b> Estimated Sales tax amount is properly displayed in place order screen. Sales Tax: "+sales,
					"<b> Actual Result 3:</b> Estimated Sales tax amount is not properly displayed in place order screen. ",driver);
			Log.message("<br>");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage.placeOrder();
			Log.message("16. Navigated to Order Confirmation Page!");

			String salesTax1  = orderconfirmationpage.getTextFromSalesTax();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> Order Sales tax should be properly displayed in Order confirmation screen.");
			Log.assertThat(orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("lblSalesTax"), orderconfirmationpage),
					"<b> Actual Result 4:</b> Estimated Sales tax amount is properly displayed in Order confirmation screen. Sales Tax: "+salesTax1,
					"<b> Actual Result 4:</b> Estimated Sales tax amount is not properly displayed in Order confirmation screen. ",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_135

	@Test(groups = { "desktop", "mobile","desktop_bat","mobile_bat",
	"tablet" }, description = "Verify Belk Rewards Dollars and Belk Credit Card payment method is shown Order confirmation screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_091(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey + "' Navigate to PDP Page");

			PdpPage pdpPage =searchResultPage.selectProductByIndex(2);

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			Log.message("4. Click on close icon of  'Gwp selection modal'  !");
			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("6. Navigated to 'Mini Cart' !");

			SignIn signinpage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("9. Navigated to Checkout Page as Guest User!");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Standard");
			Log.message("10. Entered Shipping address as guest user");
			checkoutPage.clickOnContinueInShipping();
			Log.message("11. clicked on continue Button in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("12. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkoutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("16. Entered card details");
			checkoutPage.fillingBelkRewardDollars_e2e("161698733264532");
			Log.message("</br>");
			Log.message(
					"<b>Expected Result-1: </b>Verify System should displayed Belk Reward Dollars Applied sucessfully  message  !");
			Log.assertThat(!checkoutPage.GetBRDErrorMessage().contains("This certificate is no longer valid."),
					"<b>Actual Result-1 : </b> System  displayed Belk Reward Dollars  Applied sucessfully  message  !!",
					"<b>Actual Result-1: </b>  System  displayed Belk Reward Dollars not Applied sucessfully  message   !",
					driver);

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message("17. Clicked on continue button  in Billing address Page");

			checkoutPage.clickOnContinueInBilling();
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();

			Log.message("18. Clicked on 'Place order' button in Place order page");


			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>Verify Belk Rewards Dollars and Belk Reward Dollars payment method should be properly displayed In Order Confirmation screen !");
			Log.assertThat(orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("lblCardType","txtCardNumber","lblNameOnCard","txtCardPaymnet"), orderConfirmationPage),
					"<b>Actual Result : </b> Belk Rewards Dollars and Belk Reward Dollars payment method properly displayed In Order Confirmation screen !",
					"<b>Actual Result : </b> Belk Rewards Dollars and Belk Reward Dollars payment method properly not displayed In Order Confirmation screen  !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_091

	@Test(groups = { "desktop", "mobile","desktop_bat","mobile_bat",
	"tablet" }, description = "Verify warning message is shown when applying more than 10 Belk Reward Dollars certificates to the order", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_093(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey + "' Navigate to Search Result  Page");

			PdpPage pdpPage =searchResultPage.selectProductByIndex(2);
			Log.message("3. Selecting the product and Navigating to PDP Page!");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Navigated to 'Mini Cart' !");

			SignIn signinpage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("9. Navigated to Checkout Page as Guest User!");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Standard");
			Log.message("10. Entered Shipping address as guest user");
			checkoutPage.clickOnContinueInShipping();
			Log.message("11. clicked on continue Button in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("12. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkoutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("16. Entered card details");
			checkoutPage.fillingBelkRewardDollars_e2e("161698733264532");

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message("17. Clicked on continue button  in Billing address Page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>Verify you've already applied the maximum quantity allowed message should be displayed in Billing Address screen !");
			Log.assertThat(checkoutPage.GetBRDErrorMessage().contains("This certificate is no longer valid."),
					"<b>Actual Result : </b> You've already applied the maximum quantity allowed message  displayed in Billing Address screen !",
					"<b>Actual Result : </b> You've already applied the maximum quantity allowed message not displayed in Billing Address screen  !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_093

	@Test(groups = { "desktop", "mobile" }, description = "Verify Coupons promotion is properly displayed in My Bag screen!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_179(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String  promotionCoupan = testData.get("CouponCode");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page!");


			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);

			pdpPage.clickAddToBag();
			Log.message("4. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Navigated to 'Mini Cart' !");
			shoppingBagPage.applyCouponInShoppingBag(promotionCoupan);

			String PromoMessageInCartActual = shoppingBagPage.getAppliedCouponCode();
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> Promotional message should be properly displayed in My bag screen page!");
			Log.assertThat(
					!PromoMessageInCartActual.isEmpty(),
					"<b>Actual Result-1:</b> Promotional message is properly Displayed On Cart page:" + PromoMessageInCartActual,
					"<b>Actual Result-1 :</b> Promotional message is not properly Displayed On Cart page!");


			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("6. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("7. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2","Standard");
			Log.message("8. Entered Shipping address as guest user!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("9. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("10. Selected Original address in the modal!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("12. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_MasterCard");
			Log.message("13. Entered card details!");

			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' Button in Billing Page!");

			OrderConfirmationPage confirmationPage = checkOutPage.ClickOnPlaceOrderButton();
			Log.message("15. Clicked on 'Place Order' Button!");
			LinkedHashMap<String, String> priceInOrderConfirmationPage = confirmationPage.getOrderInfoInOrderSummary();

			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>Verify  Order should be placed Sucessfully !");
			Log.assertThat(
					!priceInOrderConfirmationPage.isEmpty(),
					"<b>Actual Result-2 :</b> Order placed Sucessfully!",
					"<b>Actual Result-2 :</b>Order not placed Sucessfully!");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_179
	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify resource message displays in the Order Receipt page as Guest user!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_079(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
			
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			String Message = orderConfirmationPage.getConfirmationMessage();
			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Resource message should be displayed!");
		Log.assertThat(orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("confirmationMessage"),
				orderConfirmationPage),
				"<b>Actual Result 2: </b> After Clicking On Place Order Button,Resource Message is Properly Displayed as :" + Message,
				"<b>Actual Result 2: </b> After Clicking On Place Order Button,Resource Message is not Properly Displayed!",
				driver);
		
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_079

	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify Return to Shopping button in Order Receipt page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_081(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			String CurrentUrl =  driver.getCurrentUrl().trim().replace("/Belk", "");
			Log.message("hi" +CurrentUrl);
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
			
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			
			orderConfirmationPage.ClickOnReturnToShoppingBtn();
			Log.message("20. Clicked on the Return To Shopping Bag Button!");
			String AfterClickOnReturnToBagUrl =  driver.getCurrentUrl().trim().replace("/Belk/home/", "");
			Log.message(AfterClickOnReturnToBagUrl);
			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Return to Shopping should navigate the user to Home page!");
			Log.assertThat(CurrentUrl.equals(AfterClickOnReturnToBagUrl),
					"<b>Actual Result 2: </b> After Clicking On 'Return to Shopping' Button,User is Navigated to HomePage!" + AfterClickOnReturnToBagUrl ,
					"<b>Actual Result 2: </b> After Clicking On 'Return to Shopping' Button,User is Not Navigated to HomePage!",
					driver);
		
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_081

	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify Email Option in checkbox is checked by default!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_084(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			Boolean Value = orderConfirmationPage.verifyEmailCheckBox();
			String Value1 = String.valueOf(Value);
			
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Email Option in checkbox should be checked by default!");
			Log.assertThat(Value1.equals("true"),
					"<b>Actual Result 2: </b> After Clicking Place Order,Email Option In CheckBox Is Checked!",
					"<b>Actual Result 2: </b> After Clicking Place Order,Email Option In CheckBox Is not Checked!",
					driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_084

	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify Create Account button in Order Receipt page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_085(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		
		String searchKey = testData.get("SearchKey");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			
			orderConfirmationPage.fillingCustomerDetails("valid_Details");
			Log.message("20. Filled User Details SuccessFully For New User!");

			orderConfirmationPage.ClickOnCreateAccount();
			Log.message("21. Clicked on Create Account!");
			String Url = driver.getCurrentUrl();
			
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> When the customer clicks the Create Account button, customer should be  returned to the My Account Landing page!");
			Log.assertThat(myAccountPage.elementLayer.verifyPageElements(Arrays.asList("divMyAccount"),
					myAccountPage),
					"<b>Actual Result 2: </b> After Filling all the Details,User is navigated to My Account Page and URL is!" + Url ,
					"<b>Actual Result 2: </b> After Filling all the Details,User is not navigated to My Account Page!",driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_085



	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify Product Recommendation in Order Receipt page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_082(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			
			// Recommended Cloths Option to be Configure In BM

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_082

	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verfiy Create Account in the Order Receipt page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_083(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			String Message = orderConfirmationPage.getCreateNewAccountMessage();
			
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> Forms Field Should be dispalyed Form Fields: First Name,Last Name,Email Address,Confirm Email Address,Password,Confirm Password!");
			Log.assertThat(orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("txtFirstname","txtLastname","txtEmail","txtConformEmail","txtPassword","txtConformPassword"),
							orderConfirmationPage),
							"<b> Actual Result : </b>  Forms Field are Properly displayed: First Name,Last Name,Email Address,Confirm Email Address,Password,Confirm Password!"+ " \n and Message is -->" + Message,
							"<b> Actual Result : </b>  Forms Field are not Properly displayed: First Name,Last Name,Email Address,Confirm Email Address,Password,Confirm Password",
							driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_083


	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify details displays in the Order Summary of Order Receipt page - Guest user!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_5B_080(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			
			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			
			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");
			
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
		
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");
			
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");
			
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");
			
			OrderConfirmationPage orderConfirmationPage= checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			
			LinkedHashMap<String, String> BillingDetails = orderConfirmationPage.getBillingAddressInOrderSummary();
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The Billing details should be displayed in the Order summary of Order Receipt page!");
			Log.message("<b> Actual Result : </b> The Billing details is as Follow:" + BillingDetails ,driver);
			
			LinkedHashMap<String, String> PaymentMethod =orderConfirmationPage.getPaymentMethodInOrderSummary();
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The PaymentMethod details should be displayed in the Order summary of Order Receipt page!");
			Log.message("<b> Actual Result : </b> The PaymentMethod details is as Follow:" + PaymentMethod ,driver);
			
			LinkedHashMap<String, String> OrderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The Order details should be displayed in the Order summary of Order Receipt page!");
			Log.message("<b> Actual Result : </b> The Order details is as Follow:" + OrderDetails ,driver);
			
			LinkedHashMap<String, String> PaymentDetails = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The Payment details should be displayed in the Order summary of Order Receipt page!");
			Log.message("<b> Actual Result : </b> The Payment details is as Follow:" + PaymentDetails ,driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_5B_080

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Pacesetter Employee Discount is displayed in cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_186(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String paymentBCC = testData.get("Payments");
		String couponCode = testData.get("CouponCode");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {

			cleanup(driver, emailid, password);
			Log.message("1. Lanch the Belk website");
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();

			Log.message("2. Cleanup the Shopping Bag page");
			// Searching product in my account page with a productId
			PdpPage pdpPage = shoppingBagPage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("3. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page");

			// Select Size, Color, Qty
			pdpPage.addProductToBag("1");
			Log.message("4. Product " + pdpPage.getProductName()
					+ "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("6. Entered Coupon Code and clicked on 'Apply coupon' button");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Navigate to checkout page");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("8. Navigate to Billing Address page");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("9. Click continue Shipping button shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingBelkRewardCardDetails("NO", paymentBCC);
			Log.message("10. Card details filled in the billing form!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Click continue button in Billing address page!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");

			String[] discountmessage = checkoutPage.getdiscountmessage();
			boolean status = false;
			for (int i = 1; i <= discountmessage.length; i++) {
				if (discountmessage[i - 1].contains("30%")) {
					status = true;
				}
			}
			Log.message("<b>Expected Result</b>Associate Discount should be displayed in the Place Order Page");
			Log.assertThat(
					status,
					"<b>Actual Result :</b>  Pacesetter Discount for Belk Elite member is properly calculated and displayed in the Place Order"
							+ discountmessage,
					"<b>Actual Result :</b>  Pacesetter Discount for Belk Elite member is not properly calculated and displayed in the Place Order");

			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("12. Click on Place Order button in Place Order Page");

			Log.message("<b>Expected Result</b> Order should be placed successfully with Pacesetter discount");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("btnReturntoShopping"), checkoutPage),
					"<b>Actual Result :</b>  Order should be placed successfully with Pacesetter discount",
					"<b>Actual Result :</b> Order should be placed successfully with Pacesetter discount");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Employee Discount is displayed in cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_185(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String paymentBCC = testData.get("Payments");
		String couponCode = testData.get("CouponCode");

		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		boolean addressvalidation;
		Log.testCaseInfo(testData);
		try {

			cleanup(driver, emailid, password);
			Log.message("1. Lanch the Belk website");
			ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver).get();

			Log.message("2. Cleanup the Shopping Bag page");
			// Searching product in my account page with a productId
			PdpPage pdpPage = shoppingBagPage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("3. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page");

			// Select Size, Color, Qty
			pdpPage.addProductToBag("1");
			Log.message("4. Product " + pdpPage.getProductName()
					+ "Product is added into Shopping bag");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("6. Entered Coupon Code and clicked on 'Apply coupon' button");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Navigate to checkout page");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("8. Navigate to Billing Address page");
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.clickContinueInAddressValidationModal();
				checkoutPage.clickOnContinueInShipping();
			}

			Log.message("9. Click continue Shipping button shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingBelkRewardCardDetails("NO", paymentBCC);
			Log.message("10. Card details filled in the billing form!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Click continue button in Billing address page!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the payment option.");

			String[] discountmessage = checkoutPage.getdiscountmessage();
			boolean status = false;
			for (int i = 1; i <= discountmessage.length; i++) {
				if (discountmessage[i - 1].contains("20%")) {
					status = true;
				}
			}
			Log.message("<b>Expected Result</b>Associate Discount should be displayed in the Place Order Page");
			Log.assertThat(
					status,
					"<b>Actual Result :</b>  Employee Discount is properly calculated and displayed in the Place Order"
							+ discountmessage,
					"<b>Actual Result :</b>  Employee Discount is not properly calculated and displayed in the Place Order");

			checkoutPage.ClickOnPlaceOrderButton();
			Log.message("12. Click on Place Order button in Place Order Page");

			Log.message("<b>Expected Result</b> Order should be placed successfully with Pacesetter discount");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("btnReturntoShopping"), checkoutPage),
					"<b>Actual Result :</b>  Order should be placed successfully with Pacesetter discount",
					"<b>Actual Result :</b> Order should be placed successfully with Pacesetter discount");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}
	@Test(groups = { "desktop",
	"mobile" }, description = "Verify estimated order total is properly calculated when applying the gift card from billing address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_083(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password").split("P_")[1];
		String payment = testData.get("Payments");
//		String Comments = testData.get("Comments");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(username, passwd);
			ShoppingBagPage bagPage = null;
			

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();



			} else {
				myAccountPage.headers.clickSignOut();


			}
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			Log.message("5. Selected Size : " + size);
			String color = pdpPage.selectColor();
			Log.message("6. Selected Color : " + color);
			String qty = pdpPage.selectQuantity();
			Log.message("7. Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			// Click on checkout in Shopping My bag page
			SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Overnight", "valid_address7");
			Log.message("12. Entered Shipping details as signed user");

			// Click on Continue button in shipping page
			checkoutpage.clickOnContinueInShipping();
			Log.message("13. Clicked on Continue Shipping Address Details as Registered User");
			checkoutpage.chooseOriginalAddressInModal();
			Log.message("14. Selected Original address in Modal");
			// Validate the address by clicking on continue button
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			// Enter Billing details
			checkoutpage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address1");
			Log.message("15. Entered Billing Address Details as Registered User");
			// Enter card details
			checkoutpage.fillingBelkGiftCardDetails(payment);
			checkoutpage.clickOnApplyGiftCard();
//			Log.testCaseInfo(Comments);
			Log.message("<Expected Result: </b> Error message should be displayed for gift card with zero balance");
			Log.assertThat(
					checkoutpage.getGiftCardErrMsg().equals("This card has insufficient funds."),
					"<b>Actual Result: </b> Error message displayed as " + checkoutpage.getGiftCardErrMsg(),
					"<b>Actual Result: </b> Error message not displayed", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_CHECKOUT_083
	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify PRODUCT, buy X for total promotional message is displayed in Cart page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_183(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			PdpPage pdppage = searchResultPage.selectProductByIndex(3);
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdppage.selectQtyByIndex(2);
			Log.message("6. The Quantity is selected!");
			String PriceOnPDP = pdppage.getProductPriceFromPdp();
		
			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			String Message = shoppingBagPage.getgpwMessage();
			
			Log.message("</br>");
			Log.message("<b> Expected Result 1: </b> Promotional message should be properly displayed in Shopping Bag page!");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(Arrays.asList("gpwProductMessage"),
							pdppage),
							"<b> Actual Result 1: </b> Promotional Message is Properly Dispalyed On Shopping Bag Page and Message is :" + Message,
							"<b> Actual Result 1: </b> Promotional Message is not Properly Dispalyed On Shopping Bag Page!",
							driver);
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("16. Entered card details!");

			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");

			OrderConfirmationPage orderConfirmationPage =checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			String CostInOrderConfirmationPage = orderConfirmationPage.getProductCost();
			
			Log.message("<br>");
			Log.message("<b>Actual Result 2: </b> Order should be properly purchased with applied coupon!");
			Log.assertThat(CostInOrderConfirmationPage.equals(PriceOnPDP),
					"<b> Actual Result 2: </b> Order is properly purchased with applied coupon!",
					"<b> Actual Result 2: </b> Order is not properly purchased with applied coupon!");
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_183
	
	
	
	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify PRODUCT, buy X and Y/ get Z promotional message is displayed in Cart page!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_182(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			PdpPage pdppage = searchResultPage.selectProductByIndex(1);
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			String Message = pdppage.getBogoMessage();
			pdppage.selectQtyByIndex(2);
			Log.message("6. The Quantity is selected!");
			
			Log.message("</br>");
			Log.message("<b> Expected Result 1: </b> Buy 2 and Get 1 promotional message should be properly displayed in Cart page!");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(Arrays.asList("txtBogoMessage"),
							pdppage),
							"<b> Actual Result 1: </b> Promotional Message is Properly Dispalyed On Cart Page and Message is :" + Message,
							"<b> Actual Result 1: </b> Promotional Message is not Properly Dispalyed On Cart Page!",
							driver);

			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			Float Price= shoppingBagPage.getEstimatedValue();
			String PricewithQty2 = String.valueOf(Price);
			shoppingBagPage.selectQtyByIndex(2);
			Float Price1 = shoppingBagPage.getEstimatedValue();
			String PricewithQty3 = String.valueOf(Price1);
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");

			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");

			checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			
			Log.message("<br>");
			Log.message("<b>Actual Result 2: </b> Order should be properly purchased with applied coupon!");
			Log.assertThat(PricewithQty3.equals(PricewithQty2),
					"<b> Actual Result 2: </b> Order is properly purchased with applied coupon!",
					"<b> Actual Result 2: </b> Order is not properly purchased with applied coupon!");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_182
	
	@Test(groups = { "desktop", "tablet","mobile" }, description = "Verify SHIPPING, with number of shipment qualifying products promotional message is shown!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_184(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");

			PdpPage pdppage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP!");

			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp Page!");

			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");

			pdppage.selectQuantity();
			Log.message("6. The Quantity is selected!");

			pdppage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button!");
			
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("8. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest user!");

			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Entered Shipping address as guest user!");
			String ProductNameInShipingpage = checkOutPage.getProductName();
			
			Log.message("</br>");
			Log.message("<b> Expected Result 1: </b> Shipping qualified products should be properly displayed in Shipping Page!");
			Log.assertThat(
			checkOutPage.elementLayer.verifyPageElements(Arrays.asList("productName")
					,checkOutPage),
							"<b> Actual Result 1: </b> Product is properly displayed in Shipping Address Page and name is :" + ProductNameInShipingpage,
							"<b> Actual Result 1: </b> Product is not properly displayed in Shipping Address Page!",
							driver);
			
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue Button in Shipping address Page!");

			checkOutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal!");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("15. Billing address details are entered!");
			String ProductNameInBillingpage = checkOutPage.getProductName();
			String shipingType = checkOutPage.GetShippingtype();
			
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details!");

			Log.message("</br>");
			Log.message("<b> Expected Result 2: </b> Shipping qualified products should be properly displayed in  Billing Page!");
			Log.assertThat(
			checkOutPage.elementLayer.verifyPageElements(Arrays.asList("productName")
					,checkOutPage),
							"<b> Actual Result 2: </b> Product is properly displayed in Billing Page and name is :" + ProductNameInBillingpage +"Shipping Cost is :" + shipingType,
							"<b> Actual Result 2: </b> Product is not properly displayed in Billing Page!",
							driver);
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' Button in Billing Page!");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("18. Clicked on 'Continue' Button in Validation Modal!");

			OrderConfirmationPage orderConfirmationPage = checkOutPage.ClickOnPlaceOrderButton();
			Log.message("19. Clicked on the Place Order Button!");
			String ProductNameInOrderConfirmationPage = orderConfirmationPage.getProductName();
			String ShippingCostInorderConfirmationPage = orderConfirmationPage.getShippingRateInOrderConfirmationPage();
		
			Log.message("</br>");
			Log.message("<b> Expected Result 3: </b> Shipping qualified products should be properly displayed in Place Order Page!");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(Arrays.asList("getProductName")
					,orderConfirmationPage),
							"<b> Actual Result 3: </b> Product is properly displayed in  and name is Place Order Page :" + ProductNameInOrderConfirmationPage + " Shipping Cost is :" + ShippingCostInorderConfirmationPage,
							"<b> Actual Result 3: </b> Product is not properly displayed in Place Order Page!",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		//catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//TC_BELK_CHECKOUT_184
	

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the '3.Billing' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_068(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] carddetails = testData.get("Payments").split("\\|");

		// List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signin = homePage.headers.navigateToSignIn().get();
			signin.signInFromShoppingBag(emailid, password);
			Log.message(password);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = homePage.clickOnMiniCart();
			// shoppingBagPage =
			Log.message("2. Navigated to shopping bag page");
			// clicking on checkout button in order summary
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("3. Navigated to checkout Login page");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("4. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress(0);
			Log.message("5. Clicked Add button on multiship step1 page for 1st product");
			checkoutPage
					.fillingAddressDetailsInMultipleShipping("valid_address7");

			checkoutPage
					.ContinueAddressValidationModalWithOriginalAddressInMultiShip();
			checkoutPage.clickOnSaveInAddEditAddressModal();
			Log.message("6. filled address for 1st product and clicked save button");

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("7. Click on Contine button in Shipping Address page");
			checkoutPage.clickOnContinueInShipping();

			Log.message("8. Fill the Gift card details in Billing Address page");
			checkoutPage.fillingBelkGiftCardDetails(carddetails[2]);
			String giftcardno = checkoutPage.getGiftCardNumberFromInputField();

			Log.message("9. Apply the Belk Gift card");
			checkoutPage.clickOnApplyGiftCard();

			checkoutPage.fillingBelkRewardCardDetails("NO", carddetails[1]);
			Log.message("10. Fill the Belk Reward Card Details");
			checkoutPage.clickOnApplyBelkRewardDollars();

			checkoutPage.fillingCardDetails("NO", carddetails[0]);

			Log.message("11. Click on Continue button in Billing Address Page");
			checkoutPage.clickOnContinueInBilling();

			Log.message("12. Comparing the Shipping address and Billing address");
			Log.message("Address" + checkoutPage.getShippingAddress()
					+ "Address in billing page"
					+ checkoutPage.getBillingAddress());

			Log.softAssertThat(
					Utils.compareTwoHashMap(checkoutPage.getShippingAddress(),
							checkoutPage.getBillingAddress()),
					"<b>Actual Result:</b> Billing Address is displayed correctly",
					"<b>Actual Result:</b> Billing address is not displayed correctly");

			Log.message("13. Verify Edit link is Enabled or not in the Billing Address page");
			Log.softAssertThat(
					!checkoutPage.elementLayer.verifyPageElementsDisabled(
							Arrays.asList("lnkEditBillAddress"), checkoutPage),
					"<b>Actual Result:</b> Billing Address Edit link is displayed",
					"<b>Actual Result:</b> Billing Address Edit link is not displayed");

			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPaymentDetails"), checkoutPage),
					"<b>Actual Result:</b>Payment Details section is displayed",
					"<b>Actual Result:</b>Payment Details section is not displayed");

			Log.softAssertThat(
					checkoutPage
							.getPaymentMethodText()
							.get("cardNumber")
							.equalsIgnoreCase(
									checkoutPage
											.getmaskedCardnumber(checkoutPage
													.getCardDetails(
															carddetails[0],
															"CardNumber"))),
					"<b>Actual Result:</b> Credit Card Number is masked correctly",
					"<b>Actual Result:</b>Credit Card Number is not masked correctly");

			Log.softAssertThat(
					checkoutPage
							.getGiftCardPaymentDetailsInPlaceOrder()
							.get("GiftCardNumber")
							.equalsIgnoreCase(
									checkoutPage
											.getmaskedCardnumber(giftcardno)),
					"<b>Actual Result:</b> Belk Gift Card Number is masked correctly",
					"<b>Actual Result:</b> Belk Gift Card Number is not masked correctly");

			Log.softAssertThat(
					checkoutPage.getGiftcardAmountInBillingPage().contains("$"),
					"<b>Actual Result:</b> Belk Gift Card Amount contains $ symbol",
					"<b>Actual Result:</b> Belk Gift Card Amount contains is not $ symbol");

			Log.message("11. Click on Edit Link and Fil the Credit card details");

			Log.softAssertThat(
					checkoutPage
							.getPaymentMethodText()
							.get("cardNumber")
							.equalsIgnoreCase(
									checkoutPage
											.getmaskedCardnumber(checkoutPage
													.getCardDetails(
															carddetails[1],
															"CardNumber"))),
					"<b>Actual Result:</b> Belk Reward Card Number is masked correctly",
					"<b>Actual Result:</b> Belk Reward Card Number is not masked correctly");

			Log.softAssertThat(
					checkoutPage.getGiftcardAmountInBillingPage().contains("$"),
					"<b>Actual Result:</b> Belk Reward Card Amount contains $ symbol",
					"<b>Actual Result:</b> Belk Reward Card Amount contains is not $ symbol ");

			Log.message("<b>Expected Result:</b> Payments should be displayed in BRD,Gift Card,BRC order");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
							"paymentBRC", "paymentCreditCard", "paymentBRD"),
							checkoutPage),
					"<b>Actual Result:</b> Payments are displayed in BRD,Gift Card,BRC order",
					"<b>Actual Result:</b> Payments are not displayed in BRD,Gift Card,BRC order");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}


// TC_BELK_CHECKOUT_036
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify validation failure message is shown for invalid Belk Gift card number with valid CVV", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_036(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCardInvalid");
			Log.message("12. Entered gift card details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("13. Clicked on 'Apply Gift card' button");
			
			Log.message("<b>Expected Result:</b> Error message : 'Please double-check your Gift Card code.' should be displayed when user entered the invalid Gift card");
			Log.assertThat(checkoutPage.getGiftCardErrMsg().contentEquals("Please double-check your Gift Card code."),
					"<b>Actual Result:</b> Error message is displayed when user entered the Invalid Gift card",
					"<b>Actual Result:</b> Error message is not displayed correctly when user entered the Invalid Gift card");


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_036


	// TC_BELK_CHECKOUT_034
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message for the blocked Belk reward credit card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_034(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkRewardCardDetails("NO", "card_BelkRewardsCreditCardBlocked");
			Log.message("12. Entered card details");
			checkoutPage.clickOnContinueInBilling();
			Log.message("13. clicked on continue button  in Billing address Page");
			checkoutPage.ClickOnPlaceOrderButton();
			
			Log.message("<b>Expected Result:</b> Error Message: 'We're sorry for any inconvenience, but your order cannot be processed at this time. Please call your credit card company for assistance.' should be displayed");
			Log.assertThat(checkoutPage.getOrderErrorMessage().contentEquals("We're sorry for any inconvenience, but your order cannot be processed at this time. Please call your credit card company for assistance."),
					"<b>Actual Result:</b> Error Message is displayed when user try to place the Order with Blocked Belk Credit Cards", 
					"<b>Actual Result:</b> Error Message is not displayed when user try to place the Order with Blocked Belk Credit Cards");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_034
	
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify the Order details and Order Total displayed in 'Order Summary' section in '2. Billing' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_021(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String couponcode = testData.get("CouponCode").split("S_")[1];
		String giftCard = testData.get("BelkGiftCard");
		String belkRewarddollar = testData.get("BelkRewardDollar");
		
		String color;
		String size;
		PdpPage pdpPage = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");


			// Load the SearchResult Page with search keyword
			pdpPage = homePage.searchProduct(searchKey);
			//				System.out.println(searchKey[i]);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			Log.message("3. Navigated to PDP Page !");
			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page !");

			// Select the Gift box check box
			String productname = "Erno Laszlo Firmarine Bespoke Cleansing 2PC Set";
			shoppingBagPage.checkAddGiftBoxByIndex(2);
			Log.message("8. Gift box is selected");
			// Apply the coupon code
			Log.message("9. Apply the coupon code");
			shoppingBagPage.applyCouponInShoppingBag(couponcode);
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to SignIn page !");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11. clicked on Checkout As Guest Button !");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Standard");
			Log.message("12. shipping details filled successfully !");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. clicked on continue button !");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			
			checkOutPage.fillingBelkGiftCardDetails(giftCard);
			checkOutPage.clickOnApplyGiftCard();
			
			checkOutPage.fillingBelkRewardDollars(belkRewarddollar);
			checkOutPage.clickOnApplyBelkRewardDollars();
			Log.message("14. Filled Belk reward dollar");
			
			// need to verify one more element
			Log.assertThat(checkOutPage.elementLayer.verifyPageElements(Arrays
					.asList("lblMerchandiseTotalValue",
							"lblShippingSurchargeValue",
							"lblEstimatedShippingValue",
							"lblEstimatedSalesTaxValue", "lblOrderTotalValue",
							"txtTotalCouponSavings", "txtSurchargeValue",
							"txtGiftBoxValue","txtBelkRewardDollarAmount"), checkOutPage),
							"All the Order Summary elements are displayed",
					"All the Order Summary elements are not displayed");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Order Subtotals and Order Total should be Same after applying the Coupon codes");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			float merchanvalue = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("MerchandiseTotal"));
			float couponSavings = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("CouponSavings"));
			float rewarddollar = Float.parseFloat(checkOutPage
					.getRewardDollarPrice());
			float giftboxvalue = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("Giftbox"));
			float surchargevalue = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("Surcharge"));
			float estimatedShipping = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("EstimatedShipping"));
			float estimatedSalesTaxCost = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("SalesTax"));
			float estimatedOrderTotal = Float.parseFloat(checkOutPage
					.getCostDetailsInOrderSummary().get("EstimatedOrderTotal"));
			
			
			float ordertotal = merchanvalue + giftboxvalue + surchargevalue
					+ estimatedShipping + estimatedSalesTaxCost - (couponSavings + rewarddollar);

			Log.message("MerchandiseTotal:" + merchanvalue + "CouponSavings:"
					+ couponSavings + "rewardDollar:" + rewarddollar + "Giftbox value:" + giftboxvalue
					+ "Surcharge:" + surchargevalue + "estimatedShipping:"
					+ estimatedShipping + "estimatedSalesTaxCost:"
					+ estimatedSalesTaxCost + "estimatedOrderTotal:"
					+ estimatedOrderTotal + "Order total:" + ordertotal);

			Log.assertThat(
					ordertotal == estimatedOrderTotal,
					"<>Actual Result:</b>Order Subtotals and Order Total is Same after applying the Coupon codes",
					"<>Actual Result:</b>Order Subtotals and Order Total is not Same after applying the Coupon codes",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_021

@Test(groups = { "desktop",
	"mobile" }, description = "Verify Billing Address & Payment Method Section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_027(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String txtSectionName = "billingaddress";
		String giftCard = testData.get("BelkGiftCard");
		String belkRewarddollar = testData.get("BelkRewardDollar");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

			String color = pdpPage.selectColor();
			Log.message("4. selected color :" + color);

			String size = pdpPage.selectSize();
			Log.message("5. selected size :" + size);

			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagte to checkout page", driver);

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. CheckoutAsGuest button is clicked");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address_virgenia", "Standard");
			Log.message("10. Shipping details filled ");

			checkOutPage.clickOnContinueInShipping();
			Log.message("11. Continue shipping field is clicked");
			checkOutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("12. Continue button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_alaska");
			Log.message("13. Billing details filled ");
			checkOutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("14. Card details filled");
			checkOutPage.fillingBelkGiftCardDetails(giftCard);
			checkOutPage.clickOnApplyGiftCard();
			
			checkOutPage.fillingBelkRewardDollars(belkRewarddollar);
			checkOutPage.clickOnApplyBelkRewardDollars();
			Log.message("14. Filled Belk reward dollar");
			
			
			checkOutPage.clickOnContinueInBilling();
			Log.message("15. Continue in billing field is clicked");
			BrowserActions.nap(2);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Billing Address section should displayed with billing address details ");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("BillingDetailsInPlaceOrder"),
							checkOutPage),
							"<b>Actual Result :</b> Billing Address section is displayed properly",
							"<b>Actual Result :</b> Billing Address section is not displayed properly", driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Payment Method section displaying each in separate pane Payment Method below Shipping/Billing Address");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtPaymentDetails"), checkOutPage),
					"<b>Actual Result :</b> Payment Method section displaying each in separate pane Payment Method below Shipping/Billing Address",
					"<b>Actual Result :</b> Payment Method section is not displaying each in separate pane Payment Method below Shipping/Billing Address");
			String txtCreditCardNo = checkOutPage.getTextFromCardNumber();
			Log.message("<br>");
			Log.message("16. Got the card no and the no is: " + txtCreditCardNo);
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> payment method numbers should be masked except last four digits ");
			Log.assertThat(checkOutPage.VerifyCardMask(txtCreditCardNo),
					"<b>Actual Result :</b> Credit card masked properly",
					"<b>Actual Result :</b> Credit card is not masked properly");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Belk Reward Dollars should have Certificate number & Certificate Amount in Dollars");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("singleTxtBRDNumberInSection","txtBRDAmountInSection"), checkOutPage),
					"<b>Actual Result :</b> Belk Reward Dollars have Certificate number: " + checkOutPage.getBRDNumberFromSection(0) + " &<br> Certificate Amount in Dollars: " + checkOutPage.getBRDAmountInSection(),
					"<b>Actual Result :</b> Belk Reward Dollars not have Certificate number & Certificate Amount in Dollars",driver);
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Belk Gift card should have gift card number & Amount in Dollars");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblSingleGiftCardNumberFromSection"), checkOutPage),
					"<b>Actual Result :</b> Belk Gift card have gift card number & Amount in Dollars",
					"<b>Actual Result :</b> Belk Gift card not have gift card number & Amount in Dollars",driver);
			
			checkOutPage.clickOnEditBySectionName(txtSectionName);
			Log.message("17. Edit link is clicked");
			LinkedHashMap<String, String> BillingDetailsBeforeEdit = checkOutPage.getBillingAddress();
			Log.message("19. Got the billing address before edit");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("18. Billing details edited");

			
			checkOutPage.clickOnContinueInBilling();
			Log.message("20. Continue in billing field is clicked");
			BrowserActions.nap(2);
			LinkedHashMap<String, String> BillingDetailsAfterEdit = checkOutPage.getBillingAddress();
			Log.message("21. Got the billing address after edit");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> user should be able to edit the Billing address details");
			Log.assertThat(!BillingDetailsAfterEdit.equals(BillingDetailsBeforeEdit),
					"<b>Actual Result :</b> user is able to edit the Billing address details",
					"<b>Actual Result :</b> user is not able to edit the Billing address details", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_027

@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify validation failure message is shown for  Belk rewards credit card number and CVV2 number", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_088(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");
	String emailid = testData.get("EmailAddress");
	String password = testData.get("Password");
	String address = testData.get("Address");
	
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Launching homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page");
		// Signing with a valid user id
		SignIn signinPage = homePage.headers.navigateToSignIn();
		Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

		MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
		Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

		//deleting the item from cart
		ShoppingBagPage shoppingBagPage = null;
		if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
			shoppingBagPage = myAccountPage.clickOnMiniCart();
			shoppingBagPage.removeItemsFromBag();
		}

		// Load PDP Page for Search Keyword
	    SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
		Log.message("4. Searched with keyword '" + searchKey + "'!");

		PlpPage plpPage = new PlpPage(driver);
		PdpPage pdppage = plpPage.navigateToPDPByRow(1);
		Log.message("5. Navigated to PDP!");

		// selecting color from color swatch in Pdp Page
		Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");

		// selecting size from Size Dropdown in Pdp Page

		Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");

		// Adding product to bag
		pdppage.clickAddToBag();
		Log.message("8. Clicked on 'Add to bag' button");

		// Navigating to Shopping Bag Page
		ShoppingBagPage shoppingBagPage1 =pdppage.clickOnMiniCart();
		Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

		// Clicking checkout button in order summary module
		CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
		Log.message("10. Navigated to checkout page");

		// Filling the details in the shipping page
		checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
		Log.message("11. Filled the Shipping details in Shipping form!");
		checkoutPage.clickOnContinueInShipping();
		Log.message("12. Clicked on 'Continue' Button in Shipping Page");
		checkoutPage.ContinueAddressValidationModalWithDefaults();
		Log.message("13. Clicked on 'Continue' Button in Validation Modal");
		checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
		Log.message("14. Filled the Billing details in Billing form!");

		checkoutPage.fillingBelkRewardCardDetails("NO", "card_BelkRewardsCreditCard");
		Log.message("15. Filled the Belk Reward Card details in Billing form!");

		checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollarinvalid");
		checkoutPage.clickOnApplyBelkRewardDollars();
	
		
		
		Log.message("<br>");
		Log.message(
				"<b>Expected Result 1:</b> Validation failure message should be displayed as invalid certificate number is added in Belk Reward Dollars.");
		Log.assertThat(
				checkoutPage.elementLayer.verifyPageElements(Arrays.asList("msgInvalidBelkRewardDollar"), checkoutPage),
				"<b>Actual Result: </b>  Validation failure message displayed when as invalid certificate number is added in Belk Reward Dollars as ' "+ checkoutPage.getErrorMsgOfInvalidBelkRewardDollars()+" '.",
				"<b>Actual Result: </b>  Validation failure message not displayed when as invalid certificate number is added in Belk Reward Dollars", driver);

		
		Log.testCaseResult();


	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();

		driver.quit();
	} // finally
}// TC_BELK_CHECKOUT_89






@Test(groups = { "desktop","mobile" }, description = "Verify validation failure message shown when placing order with Belk Reward Dollars without adding Belk Reward Credit card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_086(String browser) throws Exception {
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");
	
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);

	try {
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

	    SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
		Log.message("2. Searched with '" + searchKey + "'");
		
		PlpPage plpPage = new PlpPage(driver);
		PdpPage pdpPage = plpPage.navigateToPDPByRow(1);
		Log.message("3. Navigated to PDP");

		Log.message("4. Selecting Size, Color, Quantity!");
		String color = pdpPage.selectColor();
		Log.message("5. Selected Color : " + color);
		String size = pdpPage.selectSize();
		Log.message("6. Selected Size : " + size);
		
		String qty = pdpPage.selectQuantity();
		Log.message("7. Selected Quantity : " + qty);

		pdpPage.clickAddToBag();
		Log.message("8. Product is added to the cart");

		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("9. Navigated to the shopping bag page");

		// Click on checkout in Shopping My bag page
		SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
		Log.message("10. Clicked on Checkout in Order summary Page");

		// Load checkout page to checkout the products
		CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
		Log.message("11. Navigated to Checkout Page as Guest User!");

		checkoutpage.fillingShippingDetailsAsGuest("valid_address7","Overnight" );
		Log.message("12. Entered Shipping details as guest user");

		// Click on Continue button in shipping page
		checkoutpage.clickOnContinueInShipping();
		Log.message("13. Clicked on Continue Shipping Address Details as Guest User");
		
		checkoutpage.chooseOriginalAddressInModal();
		Log.message("14. Selected Original address in Modal");
    	// Validate the address by clicking on continue button
		checkoutpage.ContinueAddressValidationModalWithDefaults();
		
		// Enter Billing details
		checkoutpage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
		Log.message("15. Entered Billing Address Details as Guest User");
		
		checkoutpage.fillingBelkRewardDollars("belk_Reward_Dollar4");
		Log.message("16. Enter Reward dollar details", driver);
		checkoutpage.clickOnApplyBelkRewardDollars();
		Log.message("17. Clicked on Apply in Belk reward dollars", driver);

		Log.message("<br>");
		Log.message("<Expected Result: Validation failure message should be displayed when placing order without Belk Rewards Credit Card payment method</b>  ");
		Log.assertThat(
				checkoutpage.elementLayer.verifyPageElements(Arrays.asList("msgErrorBelkReward"), checkoutpage),
				"<b>Actual Result: </b>  Validation failure message displayed when placing order without Belk Rewards Credit Card payment method as ' "+ checkoutpage.getErrorMsgOfBelkRewardDollars()+" '.",
				"<b>Actual Result: </b>  Validation failure message not displayed when placing order without Belk Rewards Credit Card payment method", driver);

		Log.testCaseResult();
	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CHECKOUT_086
@Test(groups = { "desktop","mobile" }, description = "Verify validation failure message shown when placing order with Belk Reward Dollars, Belk Gift Card without adding Belk Reward Credit card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_087(String browser) throws Exception {
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");
	

	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);

	try {
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		
		SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
		Log.message("2. Searched with '" + searchKey + "'");
			
		PlpPage plpPage = new PlpPage(driver);
		PdpPage pdpPage = plpPage.navigateToPDPByRow(2);
		Log.message("3. Navigated to PDP");
		
		Log.message("4. Selecting Size, Color, Quantity!");
		String size = pdpPage.selectSize();
		Log.message("5. Selected Size : " + size);
		String color = pdpPage.selectColor();
		Log.message("6. Selected Color : " + color);
		String qty = pdpPage.selectQuantity();
		Log.message("7. Selected Quantity : " + qty);

		pdpPage.clickAddToBag();
		Log.message("8. Product is added to the cart");

		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("9. Navigated to the shopping bag page");

		// Click on checkout in Shopping My bag page
		SignIn signinpage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
		Log.message("10.Clicked on Checkout in Order summary Page");

		// Load checkout page to checkout the products
		CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
		Log.message("11. Navigated to Checkout Page as Guest User!");

		checkoutpage.fillingShippingDetailsAsGuest("valid_address7","Overnight");
		Log.message("12. Entered Shipping details as guest user");

		// Click on Continue button in shipping page
		checkoutpage.clickOnContinueInShipping();
		Log.message("13. Clicked on Continue Shipping Address Details as guest User");
		checkoutpage.chooseOriginalAddressInModal();
		Log.message("14. Selected Original address in Modal");
		// Validate the address by clicking on continue button
		checkoutpage.ContinueAddressValidationModalWithDefaults();
		// Enter Billing details
		checkoutpage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
		Log.message("15. Entered Billing Address Details as guest User");

		checkoutpage.fillingBelkGiftCardDetails("GiftCard43");
		Log.message("16. Entered Gift Card details");
		checkoutpage.clickOnApplyGiftCard();
		Log.message("17. Clicked on apply gift card", driver);

		checkoutpage.fillingBelkRewardDollars("belk_Reward_Dollar4");
		Log.message("18. Enter Reward dollar details", driver);
		checkoutpage.clickOnApplyBelkRewardDollars();
		Log.message("19. Clicked on Apply in Belk reward dollars", driver);

		Log.message("<br>");
		Log.message("<Expected Result: Validation failure message should be displayed when placing order without Belk Rewards Credit Card payment method</b> ");
		Log.assertThat(
				checkoutpage.elementLayer.verifyPageElements(Arrays.asList("msgErrorBelkReward"), checkoutpage),
				"<b>Actual Result: </b>  Validation failure message displayed when placing order without Belk Rewards Credit Card payment method as ' "+ checkoutpage.getErrorMsgOfBelkRewardDollars()+" '.",
				"<b>Actual Result: </b>  Validation failure message not displayed when placing order without Belk Rewards Credit Card payment method", driver);

		
		Log.testCaseResult();
	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CHECKOUT_087

@Test(groups = { "desktop",
"mobile" }, description = "Verify ability to display message, when a qualifying item is removed and the item (passenger, free gift) is no longer eligible for GWP/PYG/BOGO. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_212(String browser) throws Exception {
	
	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String emailid = testData.get("EmailAddress");
	String password = testData.get("Password");
	String searchKey = testData.get("SearchKey");
	// Get the web driver instance
	
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");
		
		MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
		Log.message("3. Navigated to My Account Page!");
		
		Log.message("4. Products removed from Cart");
		
		PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
		Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");
		Log.message("6. Navigated to PDP Page from Search Results Page");
		
		pdpPage.selectColor();
		Log.message("7.  Size is Selected from Colour dropdown");
		
		pdpPage.selectQtyByIndex(2);
		Log.message("8. Quantity dropdown is selected with Quantity 2");
		String PriceInPdP = pdpPage.getProductPriceFromPdp();
		Log.message(PriceInPdP);
		
		pdpPage.clickAddToBag();
		Log.message("9. Add to cart button is Clicked");
		
		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("10. Navigated to Cart page");
		String PriceInShoppingBag = shoppingBagPage.getProductPriceFromshoppingBagpage();
		Log.message(PriceInShoppingBag);
		
		Log.message("<br>");
		Log.message("<b>Expected Result :</b> User should be able to see Purchase with Purchase Gift!");
		Log.assertThat(PriceInShoppingBag.equals(PriceInPdP),
				"<b>Actual Result :</b> Purchase with Purchase Item is properly Displayed!",
				"<b>Actual Result :</b> Purchase with Purchase Item is not properly Displayed!", 
				driver);
		
//		Log.message("<br>");
//		Log.message(
//				"<b>Expected Result: </b> View Details link should be displayed in Cart page!");
//		Log.assertThat(
//				shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("lnkViewDetails"), shoppingBagPage),
//				"<b> Actual Result 1: </b> View Detail link is properly dispalyed!",
//				"<b> Actual Result 1: </b> View Detail link is not properly dispalyed", driver);
//		
		shoppingBagPage.removeAddedCartItemsInShoppingBag();
		Log.message("11. Items in cart removed");

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally
}// TC_BELK_CHECKOUT_212


@Test(groups = { "desktop", "mobile",
		"tablet" }, description = "Verify estimated order total is properly calculated when applying Belk reward dollars from billing address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_097(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

	String searchKey = testData.get("SearchKey");
	String emailid = testData.get("EmailAddress");
	String password = testData.get("Password");
	String dollars[] = "belk_Reward_Dollar_new|belk_Reward_Dollar_new1|belk_Reward_Dollar_new2|belk_Reward_Dollar_new3|belk_Reward_Dollar_new4|belk_Reward_Dollar_new5|belk_Reward_Dollar_new6|belk_Reward_Dollar_new7|belk_Reward_Dollar_new8|belk_Reward_Dollar_new9|belk_Reward_Dollar_new10"
			.split("\\|");
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);

	Log.testCaseInfo(testData);
	try {
		// Load HomePage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		// Signing with a valid user id
		SignIn signinPage = homePage.headers.navigateToSignIn();
		Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

		MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
		Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

		ShoppingBagPage shoppingBagPage = null;
		if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) 
		{
			shoppingBagPage = myAccountPage.clickOnMiniCart();
			shoppingBagPage.removeItemsFromBag();
		}
		Log.message("4. Products removed from Cart");

		homePage.headers.clickSignOut();
		Log.message("5. SignOut from the account");

		SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
		Log.message("6. Searched with '" + searchKey + "'");
		
		PdpPage pdpPage = searchResultPage.navigateToPDP();
		Log.message("7. Navigated to PDP");

		String selectedColor = pdpPage.selectColor();
		Log.message("8. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

		String selectedSize = pdpPage.selectSize();
		Log.message("9. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

		String selectedQuantity = pdpPage.selectQuantity();
		Log.message("10. Selected Quantity:'" + selectedQuantity + "' from the size drop down  in the PDP Page");

		pdpPage.clickAddToBag();
		Log.message("11. Product added to Shopping Bag!");

		pdpPage.clickOnMiniCart();
		Log.message("12. Navigated to 'Mini Cart'!");

		SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
		Log.message("13. Naviagte to checkout page!");

		CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsUser(emailid, password);
		Log.message("14. Entered credentials(" + emailid + "/" + password
				+ ") and navigating CheckOut page", driver);
		
		checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address1");
		Log.message("15. Filled shipping details and shipment method!");

		checkOutPage.clickOnContinueInShipping();
		checkOutPage.ContinueAddressValidationModalWithDefaults();
		Log.message("16. Clicked on Continue in shipping!");

		checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address2");
		Log.message("17. filled billing address!");

		checkOutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
		Log.message("18. filled card details!");

		for (int index = 0; index < dollars.length; index++) {
			checkOutPage.fillingBelkRewardDollars(dollars[index]);
			checkOutPage.clickOnApplyBelkRewardDollars();
		}
		Log.message("19. Filled More then 10 Belk Reward Dollars!");

		checkOutPage.clickOnContinueInBilling();
		Log.message("20. clicked on continue button!");

		Log.message("<br>");
		Log.message("<b>Expected Result:</b>");
		Log.message(
				"The user should see that an error message is displayed indicating that only 10 cards can be applied.Order should be properly placed from Place order screen.In Order placed Receipt payment method should be displayed as Belk Reward Dollars!");
		Log.message("<br>");
		Log.message("<b>Actual Result:</b>");

		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	}
	// catch
	finally {
		Log.endTestCase();
		 driver.quit();
	} // finally
}// TC_BELK_CHECKOUT_097

@Test(groups = { "desktop", "tablet",
"mobile" }, description = "Verify warning message is shown when applying invalid Belk reward dollars", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_124(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");
	String Card_type_Excepted = "Belk Reward Dollars:";

	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Load the Home Page
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		// Load the SearchResult Page with search keyword
		PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
		Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page!");

		// Load the PDP Page with search keyword
		Log.message("3. Navigated to PDP Page!");

		// Selecting the Color
		String color = pdpPage.selectColor();
		Log.message("4. selected the '" + color + "' Color!");

		// Selecting the Size
		String size = pdpPage.selectSize();
		Log.message("5. selected the '" + size + "' size!");

		// Adding item to the shopping bag
		pdpPage.clickAddToBag();
		Log.message("6. Product added to Bag!");

		// Navigating to shopping bag
		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("7. Navigated to shopping bag page!");

		// Navigating to signIn page
		SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
		Log.message("8. Navigated to SignIn page!");

		// clicking Checkout As Guest
		CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
		Log.message("9. clicked on Checkout As Guest Button!");

		// filling shipping details
		checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
		Log.message("10. shipping details filled successfully!");

		// clicking continue button
		checkOutPage.clickOnContinueInShipping();
		checkOutPage.ContinueAddressValidationModalWithDefaults();
		Log.message("11. clicked on continue button!");

		// filling billing details
		checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
		Log.message("12. Billing address details are entered!");

		// Filling Belk Reward Dollar
		checkOutPage.fillingBelkRewardDollars("belk_Reward_DollarInvalid");
		checkOutPage.clickOnApplyBelkRewardDollars();
		Log.message("13. Entered Belk Reward Dollar details!");
		String Message = checkOutPage.GetBRDErrorMessage();

		Log.message("<br>");
		Log.message("<b> Expected Result 1: </b> Continue Button Should be Disabled in the Billing Address page!");
		Log.assertThat(
				checkOutPage.elementLayer.verifyPageElementsDisabled(
						Arrays.asList("btnContinuePlaceOrder"), checkOutPage),
						"<b>Actual Result 1:</b> Continue Button is Disabled in Billing Page!",
						"<b>Actual Result 1:</b> Continue Button is not Disabled in Billing Page!",driver);

		Log.message("<br>");
		Log.message("<b> Expected Result 2: </b> An error message should be displayed indicating that the BRD applied is invalid/already-used!");
		Log.assertThat(
				checkOutPage.elementLayer.verifyPageElements(Arrays.asList("brdErrorMessage"),
						checkOutPage),
						"<b> Actual Result 2: </b> Error Message is Dispalyed on Billing Page!",
						"<b> Actual Result 2: </b> Error Message is not Dispalyed on Billing Page!", driver);
		Log.message("<b>" + Message + "<b>");

		// Filling Card Details
		checkOutPage.fillingCardDetails("NO", "card_Visa");
		Log.message("14. Payment Details Filled Successfully!");

		// Filling Belk Reward Dollar
		checkOutPage.fillingBelkRewardDollars("belk_Reward_Dollar_new");
		checkOutPage.clickOnApplyBelkRewardDollars();
		Log.message("15. Entered Belk Reward Dollar details!");

		// clicking continue button
		checkOutPage.clickOnContinueInBilling();
		Log.message("16. Clicked on 'Continue' Button in Billing Page!");

		// Click Place order
		OrderConfirmationPage orderConfirmationPage = checkOutPage.placeOrder();
		Log.message("17. Clicked on Place Order Button!");
		String Card_type_Actual = orderConfirmationPage.gettextFromPayment();

		Log.message("<br>");
		Log.message(
				"<b>Expected Result 3:</b>  Order should be successfully placed with Apply 'Belk reward dollar'!");
		Log.assertThat(Card_type_Actual.equals(Card_type_Excepted),
				"<b>Actual Result 3:</b> Order is Placed with Belk reward dollar!",
				"<b>Actual Result 3:</b> Order is not Placed with Belk reward dollar!",driver);

		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally
}// TC_BELK_CHECKOUT_124

@Test(groups = { "desktop", "mobile" }, description = "Verify PRODUCT, buy X/ get Y promotional message is properly show!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_181(String browser) throws Exception {

	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);

	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);

	String searchKey= testData.get("SearchKey");
	String promoMessageExcepted = "Buy 1 Get 1 Free";
	Log.testCaseInfo(testData);

	try {
		// Load the Home Page
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
		Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
		Log.message("3. Navigated to 'Pdp' Page with randomly selected product");
		
		Log.message("4. Selecting Size, Color, Quantity!");
		String size = pdpPage.selectSize();
		String color = pdpPage.selectColor();
		pdpPage.selectQtyByIndex(2);
		Log.message("   Selected Size : " + size);
		Log.message("   Selected Color : " + color);
		String PriceInPdp = pdpPage.getProductPriceFromPdp();
		Log.message(PriceInPdp);

		pdpPage.clickAddToBag();
		Log.message("5. Product added to Shopping Bag!");

		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("6. Navigated to 'Mini Cart' !");
		String PromoMessageInCartActual = shoppingBagPage.getPromoMessageInShoppingBagPage();
		
		SignIn signIn = (SignIn) shoppingBagPage
				.clickOnCheckoutInOrderSummary(shoppingBagPage);
		Log.message("7. Navigated to SignIn Page to Checkout as Guest user!");

		CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
		Log.message("8. Navigated to Checkout Page as Guest!");

		checkOutPage.fillingShippingDetailsAsGuest("valid_address2","Standard");
		Log.message("9. Entered Shipping address as guest user!");

		checkOutPage.clickOnContinueInShipping();
		Log.message("10. clicked on continue Button in Shipping address Page!");

		checkOutPage.chooseOriginalAddressInModal();
		Log.message("11. Selected Original address in the modal!");

		checkOutPage.ContinueAddressValidationModalWithDefaults();
		Log.message("12. Clicked on Continue Button in Address validation modal!");

		checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
		Log.message("13. Billing address details are entered!");

		checkOutPage.fillingCardDetails("NO", "card_Visa");
		Log.message("14. Entered card details!");

		checkOutPage.clickOnContinueInBilling();
		Log.message("15. Clicked on 'Continue' Button in Billing Page!");

		OrderConfirmationPage confirmationPage = checkOutPage.ClickOnPlaceOrderButton();
		Log.message("16. Clicked on 'Place Order' Button!");
		String PriceInOrderConfirmationPage = confirmationPage.getProductRateInOrderConfirmationPage();
		Log.message(PriceInOrderConfirmationPage);

//			Log.message("<br>");
//			Log.message("<b>Expected Result :</b> Promotional message should be properly displayed in Cart page!");
//			Log.assertThat(
//					PromoMessageInCartActual.equals(promoMessageExcepted),
//					"<b>Actual Result :</b> Promotional message is properly Displayed On Cart page!" + PromoMessageInCartActual,
//					"<b>Actual Result :</b> Promotional message is not properly Displayed On Cart page!");
//			
		Log.message("<br>");
		Log.message("<b>Expected Result :</b> Order should be properly purchased with applied coupon!");
		Log.assertThat(
				PriceInPdp.equals(PriceInOrderConfirmationPage),
				"<b>Actual Result :</b> Order is Purchased with valid applied coupon!",
				"<b>Actual Result :</b> Order is not Purchased with valid applied coupon!");


		Log.testCaseResult();
	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally
}// TC_BELK_CHECKOUT_181

//TC_BELK_CHECKOUT_040
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify warning message for the blocked Belk Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_040(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBelkGiftCardDetails("GiftCardBlocked");
			Log.message("12. Entered gift card details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b> Order should be placed successfully with belk gift card and navigated to order receipt page ");

			Log.assertThat(((checkoutPage.getGiftCardErrMsg().contains("Something Went Wrong"))),
					"<b>Actual Result : </b> Order is placed successfully with belk gift card and navigated to order receipt page",
					"<b>Actual Result : </b> Order is not placed successfully with belk gift card and navigated to order receipt page",
					driver);
			

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_040


//TC_BELK_CHECKOUT_041
	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Order is properly placed with Belk Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_041(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] belkGiftCard = testData.get("BelkGiftCard").split("\\|");
		String cardNumber = belkGiftCard[0];
		String pinNumber =belkGiftCard[1];

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("12. Entered Belk Reward  Credit card details");
			
			checkoutPage.fillingBelkGiftCardDetails_test(cardNumber, pinNumber);
			Log.message("13. Entered gift card details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("14. Clicked on Apply gift card button");
			Log.message("</br>");
			Log.message(
					"<b>Expected Result-1 : </b>Verify Systen should displayed 'Gift card has been applied.' successfully Message! ");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblGiftcardSuccessMsg"), checkoutPage),
					"<b>Actual Result-1: </b> Systen displayed 'Gift card has been applied.' successfully Message",
					"<b>Actual Result-1: </b> Systen not displayed 'Gift card has been applied.' successfully Message",
					driver);
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on continue button  in Billing address Page");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();

			Log.message("16. Clicked on 'Place order' button in Place order page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result-2: </b> Order should be placed successfully with belk gift card and navigated to order receipt page ");

			Log.assertThat(((orderConfirmationPage.getPaymentMethodType()).equals("Belk Gift Card")),
					"<b>Actual Result-2: </b> Order is placed successfully with belk gift card and navigated to order receipt page",
					"<b>Actual Result-2: </b> Order is not placed successfully with belk gift card and navigated to order receipt page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_041


	

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify Belk CC Elite promotion is displayed in cart page while coupon is applied", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_188(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");


		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Select the Quantity ");

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");
			Log.message("<br>");
			
		Log.message("<b> Expected Result 1: </b> In my bag screen Belk CC Elite promotion example Belk Elite Customer get Free Gift Boxes and Free Standard Shipping should be properly displayed.");
		
		//Actual Result Need To configure Belk Elite Promo Message on the Cart page 
		CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			
			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("12. Entered  Credit card details");

			Log.message("</br>");
			
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on continue button  in Billing address Page");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();

			Log.message("16. Clicked on 'Place order' button in Place order page");

			Log.message("</br>");
			Log.message("<b> Expected Result 2:</b> Order should be properly placed Belk Elite Customer get Free Gift Boxes and Free Standard Shipping promotion.");
		//Actual Result need to be configure

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_188
	
	
	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify Double Employee Discount is displayed in cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_187(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] belkGiftCard = testData.get("BelkGiftCard").split("\\|");
		String cardNumber = belkGiftCard[0];
		String pinNumber =belkGiftCard[1];

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			Log.message("6. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(3);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Select Quantity ");

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked Checkout button and navigated to Shipping page as authorized user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address4");

			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("12. Entered Belk Reward  Credit card details");
			
			
			Log.message("</br>");
			
		
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on continue button  in Billing address Page");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();

			Log.message("16. Clicked on 'Place order' button in Place order page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result-1 : </b>Order should be properly placed 'Double' promotion and Double promotion should be properly added when confirming the order! ");
			//Actual Result need to be validated 

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_187
	
	@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify PRODUCT, buy X for total promotional message is displayed in Cart page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_170(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered the valid credentials to login and navigated to 'MyAccount' Page!");

			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to PDP Page! ");
			
		    PdpPage pdppage=  searchResultPage.selectProductByIndex(1);

			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize=pdppage.selectSize();
			Log.message("6. Selected size: '" + selectedSize + "' from  size dropdown the PDP page");

			pdppage.selectQuantity();
			Log.message("7. Selected quantity as '1' from quantity dropdown in the PDP page");

			pdppage.clickAddToBag();
			Log.message("8. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page!");
			shoppingBagPage.selectQtyByIndex(2);
            
			String standardPrice = shoppingBagPage.productPricingShoopingCart();
			Float standardPriceAtShoppingPage = Float.parseFloat(standardPrice.replace("$", ""));

			float merchandiseTotalAtShoppingPage = shoppingBagPage.getMerchandiseTotal();
			
			float totalOrderPriceAtShopping =shoppingBagPage.getEstimatedValue();
			int selectedQuantity =Integer.parseInt(shoppingBagPage.getNoOfQtyInShoppingBagPage());

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> In my bag screen, for the selected item, deal based offer promotion should be displayed properly. ");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("productPricing"), shoppingBagPage),
					"<b>Actual Result 1:</b> Deal Based Offer promotion is properly displayed on the my bag screen as '"+standardPrice + " 'and the price after discount is displayed as '"+ merchandiseTotalAtShoppingPage +"'" ,
					"<b>Actual Result 1:</b> Deal Based Offer promotion is not displayed on the my bag screen",
					driver);
			Log.message("</br>");
			
			Log.message("</br>");
			Log.message("<b>Expected Result 2:</b> Amount should be properly discounted while checkout an item from Cart page. ");
			Log.assertThat((standardPriceAtShoppingPage*selectedQuantity)>totalOrderPriceAtShopping,
					"<b>Actual Result 2:</b> Amount is properly discounted on the Cart page while checkout ",
					"<b>Actual Result 2:</b> Amount is not properly discounted on the Cart page while checkout ",
					driver);
			Log.message("</br>");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to Checkout Page as 'Signed' user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");
			Log.message("11. Filled Shiiping Address  in Shipping address Page");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("13. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard1");
			Log.message("14. Entered Belk Reward  Credit card details");
			
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on 'Continue' in Billing Page!");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("16. Navigated to Order receipt Page!");
            float  merchandiseTotalAtOrder =orderConfirmationPage.getMerchandiseTotals();
			Log.message("</br>");
			Log.message("<b>Expected Result 3:</b>  Deal based offer promotion should be Appiled properly and Order should be placed");
			 Log.assertThat(merchandiseTotalAtOrder==merchandiseTotalAtShoppingPage,
			                  "<b>Actual Result 3:</b>  Deal based offer promotion  Appiled properly and Order placed Sucessfully ! ",
						   "<b>Actual Result 3:</b>  Deal based offer promotion not Appiled properly and Order notplaced Sucessfully!",
							driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_170


@Test(groups = { "desktop", "mobile","tablet" }, description = "Verify PRODUCT, buy X and Y/ get to buy Z promotional message displayed for Purchase with Purchase.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_173(String browser) throws Exception {

		//** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered the valid credentials to login and navigated to 'MyAccount' Page!");

			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' ");

			PdpPage pdppage = searchResultPage.selectProductByIndex(1);
			Log.message("3. Navigated to PDP Page!!");


			String colorName = pdppage.selectColor();
			Log.message("5. Selected color: '" + colorName + "' from color swatch in the PDP page");

			String selectedSize=pdppage.selectSize();
			Log.message("6. Selected size: '" + selectedSize + "' from  size dropdown the PDP page");

			//pdppage.selectQtyByIndex(1);
			Log.message("7. Selected quantity as '1' from quantity dropdown in the PDP page");

			String msg = pdppage.getPromotionalCalloutMessage();
			Log.message("   The promotional message is displayed on the PDP page as '"+ msg +"' .");

			pdppage.clickAddToBagButton();
			Log.message("8. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdppage.minicart.navigateToBag();
			Log.message("9. Navigate to Shopping bag Page!");

			shoppingBagPage.selectQtyByIndex(2);
			Log.message("10. Selected the quantity as '2' for the product");

			String Subtotalbfr = shoppingBagPage.getStandardSubtotalPrice();
			Log.message("  The Order Subtotal of '2' product:" + Subtotalbfr); 
			
			String promoMsg = shoppingBagPage.getPromoMessageInShoppingBagPage();

			String Subtotalafr = shoppingBagPage.getStandardSubtotalPrice();
			//Log.message(" The Order Subtotal of '3' product:" + Subtotalafr); 

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> In my bag screen for the selected item Purchase with Purchase promotion should be displayed properly . ");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("finalPromoMessage"), shoppingBagPage)&&(Subtotalbfr.equals(Subtotalafr)),
					"<b>Actual Result 1:</b> Deal Based Offer promotion is properly displayed on the my bag screen as '"+promoMsg ,
					"<b>Actual Result 1:</b> Deal Based Offer promotion is not displayed on the my bag screen",
					driver);
			Log.message("</br>");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Navigated to Checkout Page as 'Signed' user");

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address7");
			Log.message("12. Filled Shiiping Address  in Shipping address Page");
			checkoutPage.clickOnContinueInShipping();
			Log.message("13. clicked on continue button  in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("14. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			
			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard1");
			Log.message("15. Entered Belk Reward  Credit card details");
			
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' in Billing Page!");

			OrderConfirmationPage orderConfirmationPage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("17. Navigated to Order receipt Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>  Order should be properly purchased with purchased item");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderConfirmationPage.GetOrderNumber();

			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit") && orderConfirmationPage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"), orderConfirmationPage),
					"<b> Actual Result 2: </b> Order placed after clicking on place order button and Order number is generated: "
							+ ordernumb,
							"<b> Actual Result 2: </b> Order is not placed after clicking on place order button. ", driver);
			Log.message("<br>");

			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_173

@Test(groups = { "desktop", "tablet",
"mobile" }, description = "Verify the 'Shipping Address' - 'In Store Pickup' section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_024(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");
	String color;
	String size;

	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Load the Home Page
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page");
		
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to the Search Result Page");
			Log.message("3. Navigate to the PDP page");
			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message("4 selected the '" + color + "' Color !");
			// Selecting the Size
			size = pdpPage.selectSize();
			Log.message("5.selected the '" + size + "' size !");
			// Adding item to the shopping bag
			pdpPage.selectQtyByIndex(2);
			Log.message("6. Select the quantity");
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");
			

	
		// Navigating to shopping bag
		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("7. Navigated to shopping bag page");
		shoppingBagPage.clickOnFindStorelnk();
		Log.message("8. Click on the Find a Store Link on the Cart Page");
		shoppingBagPage.applyZipCodeInShoppingBag("28205");
		Log.message("9.  Add the Zip code in the Find Store Link box");
		
		// Navigating to signIn page
					SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
					Log.message("8. Navigated to SignIn page");
					// clicking Checkout As Guest
					CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
					Log.message("9. Clicked checkoutAsGuest and navigated to checkout page");
					// filling shipping details
		//checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
		Log.message("10. shipping details filled successfully");
		// clicking continue button
		checkOutPage.clickOnContinueInShipping();
		//checkOutPage.ContinueAddressValidationModalWithDefaults();
		Log.message("11. clicked continue button");
		Log.message("<br>");

		checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
		Log.message("13. Billing address details are entered!");

		checkOutPage.fillingCardDetails("NO", "card_Visa");
		Log.message("14. Entered card details");
		checkOutPage.clickOnContinueInBilling();
		Log.message("15. Clicked on 'Continue' Button in Billing Page");
		OrderConfirmationPage orderConfirmationPage = checkOutPage.ClickOnPlaceOrderButton();
		Log.message("16. Navigated to Order receipt Page!");


		Log.message("<br>");
		Log.message("In Store Pickup section should be properly displayed with below field values,Store Name,  Store Address,, Store Phone Number,Shipping Method 'In Store Pickup' 'FREE'");
	//STORE PICKUP NEED TO VALIDATE As THE pAGE NAVIGATE TO PAGE NOT FOUND
		Log.testCaseResult();

	} // try

	catch (Exception e) {
		Log.exception(e, driver);

	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CHECKOUT_024

@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays the item updated cart message - Max_buy_qty with Bag Merge", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_007(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);
	String emailid = testData.get("EmailAddress");
	String password = testData.get("Password");
	String searchKey = testData.get("SearchKey");

	// Get the web driver instancex
	final WebDriver driver = WebDriverFactory.get(browser);

	Log.testCaseInfo(testData);

	try {
		cleanup(driver, emailid, password);
		HomePage homePage = new HomePage(driver).get();

		// Searching product in my account page with a productId
		PdpPage pdpPage = homePage.headers
				.searchAndNavigateToPDP(searchKey);
		Log.message("3. Search "
				+ searchKey
				+ " in the home page and  Navigated to 'Search Result'  Page");

		// String drpsize=
		// String.valueOf(pdpPage.getSizeOptions().size()/2);

		pdpPage.addProductToBag("2");

		Log.message("4. Product " + pdpPage.getProductName()
				+ "Product is added into Shopping bag");
		pdpPage.headers.clickSignOut();

		pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
		Log.message("3. Search "
				+ searchKey
				+ " in the home page and  Navigated to 'Search Result'  Page");

		// String drpsize1= String.valueOf(pdpPage.getSizeOptions().size());

		pdpPage.addProductToBag("3");

		Log.message("4. Product " + pdpPage.getProductName()
				+ "Product is added into Shopping bag");
		// Navigating to shopping bag page from pdp page after clicking on
		// minicart icon
		ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
		Log.message("5. Clicking on Mini cart icon is navigated to the bag page");

		Log.message("6. Entered Coupon Code and clicked on 'Apply coupon' button");

		// Clicking checkout button in order summary module
		SignIn signIn = (SignIn) shoppingBagPage
				.clickOnCheckoutInOrderSummary(shoppingBagPage);
		shoppingBagPage = signIn.signInFromShoppingBag(emailid, password);

		Log.message("<b>Expected Result:</b> Global Level Cart Merge message should be displayed");
		Log.assertThat(
				shoppingBagPage
						.getGlobalMessage()
						.equalsIgnoreCase(
								" We're sorry, you have too many items in your shopping bag. The maximum number allowed is X. Please remove items to continue."),
				"<b>Actual Result</b> Global Level Cart Merge Message is displayed",
				"<b>Actual Result</b> Global Level Cart Merge Message is not displayed");

		Log.message("<b>Expected Result:</b> Item Level Cart Merge message should be displayed");
		Log.assertThat(
				shoppingBagPage.getLineMessage().equalsIgnoreCase(
						"This item in your bag has been changed."),
				"<b>Actual Result</b> Item Level Cart Merge Message is displayed",
				"<b>Actual Result</b> Item Level Cart Merge Message is not displayed");

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally
}
@Test(groups = { "desktop", "mobile" }, description = "Verify the '4.Place order' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_065(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);

	String username = testData.get("EmailAddress");
	String passwd = testData.get("Password");
	String runplatform = Utils.getRunPlatForm();
	String couponcode=testData.get("CouponCode");
	String giftcard=testData.get("Payments").split("\\|")[0];
	String creditcard=testData.get("Payments").split("\\|")[1];
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Navigate to Belk homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");

		MyAccountPage myAccountPage = signIn.signInToMyAccount(username,
				passwd);
		Log.message("3. Navigated to My Account Page!");

		ShoppingBagPage shoppingbagpage = myAccountPage.clickOnMiniCart();
		Log.message("4. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

		shoppingbagpage.applyCouponInShoppingBag(couponcode);
		// Navigated to checkout page as a registered user
		CheckoutPage checkoutPage = shoppingbagpage
				.clickCheckoutInOrderSummaryForRegUser();
		Log.message("5. Clicked on Checkout in Order summary Page");

		// Clicking on 'Yes' button for multishipping
		checkoutPage.clickOnYesInShipToMultipleAddress();
		Log.message("6. Clicked on 'Yes' button in Shipping address Page");

		checkoutPage.clickOnAddInShipToMulipleAddressByRow(1);
		Log.message("7. Clicked Add button on multiship step1 page for 1st product");
		checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address2");
		checkoutPage.chooseOriginalAddressInModal();
		if (checkoutPage.clickContinueInAddressValidationModal())
			checkoutPage.clickOnSaveInAddEditAddressModal();

		checkoutPage.clickOnContinueInMultiAddress();
		Log.message("8. Clicked on 'Continue' in multiship address Page ");

		checkoutPage.clickOnContinueInShipping();
		Log.message("9. Clicked on 'Continue' in multiship method Page ");

		// Filling billing address details as a guest user
		checkoutPage.fillingBillingDetailsAsGuest("valid_address3");
		Log.message("10. Billing address details are filled in billing page!");

		checkoutPage.fillingBelkGiftCardDetails(giftcard);
		checkoutPage.clickOnApplyGiftCard();

		checkoutPage.fillingCardDetails("NO", creditcard);
		// Filling the CardDetails in the billing page

		Log.message("11. Card details are fillied in the billing page!");

		checkoutPage.clickOnContinueInBilling();

		Log.assertThat(
				checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
						"lblFreeInStorePickup", "lblShippingAddress"),
						checkoutPage),
				"<b>Actual Result:</b> Free In Store Pickup Address and Shipping Address is displayed",
				"<b>Actual Result:</b> Free In Store Pickup Address and Shipping Address is not displayed");
		checkoutPage.verifyOrderTotalInOrderSummary();

		Log.assertThat(
				checkoutPage.elementLayer.verifyPageListElements(Arrays
						.asList("lstProductName", "lstProductthumbnail",
								"lstProductcolor", "lstProductSize",
								"lstProductQty", "lstgiftboxmessage",
								"lstsubtotal", "lstNowPrice",
								"lstOriginalPrice", "lstgiftboxmessage"),

				checkoutPage),
				"<b>Actual Result:</b>Product Thumbnail ,Product Name -> On click should navigate the customer to the PDP ,UPC ,Variations (i.e. size and / or color) ,SurchargeApplied Message ,Gift Box Added Message ,Gift Message ,Registry Message (Not shown) ,Quantity ,Price (Original and Now pricing),Coupon Name ,Coupon Discount ",
				"Product Thumbnail ,Product Name -> On click should navigate the customer to the PDP ,UPC ,Variations (i.e. size and / or color) ,SurchargeApplied Message ,Gift Box Added Message ,Gift Message ,Registry Message (Not shown) ,Quantity ,Price (Original and Now pricing)"
						+ ",Coupon Name ,Coupon Discount");

		if (runplatform == "mobile") {
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays
							.asList("btnCancelInPlaceOrderMobile",
									"btnPlaceOrderInPlaceOrderPage"),
							checkoutPage),
					"<b>Actual Result: Cancel button and Place order buttons are displayed</b>",
					"<b>Actual Result: Cancel button and Place order buttons are not displayed</b>");
		} else if (runplatform == "desktop") {
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays
							.asList("btnCancelInPlaceOrderDesktop",
									"btnPlaceOrderInPlaceOrderPage"),
							checkoutPage),
					"<b>Actual Result: Cancel button and Place order buttons are displayed</b>",
					"<b>Actual Result: Cancel button and Place order buttons are not displayed</b>");
		}

		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CART_065

@Test(groups = { "desktop",
"mobile" }, description = "Verify the 'Shipping Address' section below order Summary section in  Billing page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_064(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");
	String username = testData.get("EmailAddress");
	String passwd = testData.get("Password");
	String productName = "Arthur Court Alligator Pitcher - Online Only";
	String txtZipCodeToEnter = "28211";
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Navigate to Belk homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");

		MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
		Log.message("3. Navigated to My Account Page!");
		ShoppingBagPage bagPage = null;
		
		if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

			bagPage = myAccountPage.clickOnMiniCart();
			bagPage.removeItemsFromBag();
			
		}
		
		RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
		registrySignedUserPage.clickOnViewlink();
		registrySignedUserPage.addItemToBag(productName);
		PdpPage pdpPage = registrySignedUserPage.headers.searchAndNavigateToPDP(searchKey[0]);
		pdpPage.addProductToBag();
		pdpPage.headers.searchAndNavigateToPDP(searchKey[1]);
		pdpPage.selectStore(txtZipCodeToEnter);
		pdpPage.selectFreeInStore("YES");
		pdpPage.addProductToBag();
		ShoppingBagPage shoppingbagpage = myAccountPage.clickOnMiniCart();
		Log.message("4. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");
//		shoppingbagpage.clickOnFindStorelnk();
//		shoppingbagpage.applyZipCodeInShoppingBag(txtZipCodeToEnter);
//		shoppingbagpage.clickOnSelectStore();
		// Navigated to checkout page as a registered user
		CheckoutPage checkoutPage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
		Log.message("5. Clicked on Checkout in Order summary Page");

		// Clicking on 'Yes' button for multishipping
		checkoutPage.clickOnYesInShipToMultipleAddress();
		Log.message("6. Clicked on 'Yes' button in Shipping address Page");
		// **********needed to be updated********************//
		checkoutPage.clickOnAddInShipToMulipleAddress();
		Log.message("7. Clicked Add button on multiship step1 page for 1st product");
		checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address2");
		checkoutPage.ContinueAddressValidationModalInMultipleShipping();
		checkoutPage.selectdropdownandAddress(1, 2);
		checkoutPage.selectdropdownandAddress(2, 6);
		checkoutPage.clickOnContinueInMultiAddress();
		Log.message("11. Clicked on 'Continue' in multiship address Page ");

		checkoutPage.clickOnContinueInShipping();
		Log.message(". Clicked on 'Continue' in multiship method Page ");

		// Filling billing address details as a guest user
		checkoutPage.fillingBillingDetailsAsGuest("valid_address3");
		Log.message(". Billing address details are filled in billing page!");

		checkoutPage.fillingCardDetails("NO", "card_Visa");
		
		Log.message("<br>");
		Log.message(
				"<b>Expected Result:</b> Shipping address should be in this following order, InStore Pickup ,Ship to Home, Registry");

		Log.assertThat(
				checkoutPage.verifyShippingAddressOrder(),
				"<b>Actual Result: </b>  Shipping address is in the correct order.",
				"<b>Actual Result: </b>  Shipping address is not in the correct order.",
				driver);


		Log.message("<br>");
		Log.message(
				"<b>Expected Result:</b> The 'Edit' link should be displayed in 'Shipping Address' section - header.");

		Log.assertThat(
				checkoutPage.elementLayer.verifyPageElements(Arrays.asList("LnkEditShipAddress"), checkoutPage),
				"<b>Actual Result: </b>  'Edit Link' displayed in the 'Shipping Address' section - header.",
				"<b>Actual Result: </b>  'Edit Link' not displayed in the 'Shipping Address' section - header.",
				driver);

		checkoutPage.clickEditShippingAddress();
		Log.message(" Clicked on 'Edit Link' in Billing Address Page!");
		Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabShipping"), checkoutPage),
				"<b>Actual Result: </b>  Navigated to the 'Shipping Address' page after clicking on 'Edit' link in 'Shipping Address' section - header.",
				"<b>Actual Result: </b>  Do not Navigate to the 'Shipping Address' page after clicking on 'Edit' link in 'Shipping Address' section - header.",
				driver);
		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CART_064

@Test(groups = { "desktop",
"mobile" }, description = "Verify the 'Shipping Address' section below order Summary section in 'Place order' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_067(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

	String searchKey[] = testData.get("SearchKey").replace("S_", "").split("\\|");
	String username = testData.get("EmailAddress");
	String passwd = testData.get("Password");
	String productName = "Arthur Court Alligator Pitcher - Online Only";
	String txtZipCodeToEnter = "28211";
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Navigate to Belk homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");

		MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
		Log.message("3. Navigated to My Account Page!");
		ShoppingBagPage bagPage = null;
		
		if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

			bagPage = myAccountPage.clickOnMiniCart();
			bagPage.removeItemsFromBag();
			
		}
		
		RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage.navigateToSection("registry");
		registrySignedUserPage.clickOnViewlink();
		registrySignedUserPage.addItemToBag(productName);
		PdpPage pdpPage = registrySignedUserPage.headers.searchAndNavigateToPDP(searchKey[0]);
		pdpPage.addProductToBag();
		pdpPage.headers.searchAndNavigateToPDP(searchKey[1]);
		pdpPage.selectStore(txtZipCodeToEnter);
		pdpPage.selectFreeInStore("YES");
		pdpPage.addProductToBag();
		ShoppingBagPage shoppingbagpage = myAccountPage.clickOnMiniCart();
		Log.message("4. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");
//		shoppingbagpage.clickOnFindStorelnk();
//		shoppingbagpage.applyZipCodeInShoppingBag(txtZipCodeToEnter);
//		shoppingbagpage.clickOnSelectStore();
		// Navigated to checkout page as a registered user
		CheckoutPage checkoutPage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
		Log.message("5. Clicked on Checkout in Order summary Page");

		// Clicking on 'Yes' button for multishipping
		checkoutPage.clickOnYesInShipToMultipleAddress();
		Log.message("6. Clicked on 'Yes' button in Shipping address Page");
		// **********needed to be updated********************//
		checkoutPage.clickOnAddInShipToMulipleAddress();
		Log.message("7. Clicked Add button on multiship step1 page for 1st product");
		checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address2");
		checkoutPage.ContinueAddressValidationModalInMultipleShipping();
		checkoutPage.selectdropdownandAddress(1, 2);
		checkoutPage.selectdropdownandAddress(2, 6);
		checkoutPage.clickOnContinueInMultiAddress();
		Log.message("11. Clicked on 'Continue' in multiship address Page ");

		checkoutPage.clickOnContinueInShipping();
		Log.message(". Clicked on 'Continue' in multiship method Page ");

		// Filling billing address details as a guest user
		checkoutPage.fillingBillingDetailsAsGuest("valid_address3");
		Log.message(". Billing address details are filled in billing page!");

		checkoutPage.fillingCardDetails("NO", "card_Visa");
		checkoutPage.clickOnContinueInBilling();
		Log.message("<br>");
		Log.message(
				"<b>Expected Result:</b> Shipping address should be in this following order, InStore Pickup ,Ship to Home, Registry");

		Log.assertThat(
				checkoutPage.verifyShippingAddressOrder(),
				"<b>Actual Result: </b>  Shipping address is in the correct order.",
				"<b>Actual Result: </b>  Shipping address is not in the correct order.",
				driver);

		Log.message("<br>");
		Log.message(
				"<b>Expected Result:</b> The 'Edit' link should be displayed in 'Shipping Address' section - header.");

		Log.assertThat(
				checkoutPage.elementLayer.verifyPageElements(Arrays.asList("LnkEditShipAddress"), checkoutPage),
				"<b>Actual Result: </b>  'Edit Link' displayed in the 'Shipping Address' section - header.",
				"<b>Actual Result: </b>  'Edit Link' not displayed in the 'Shipping Address' section - header.",
				driver);

		checkoutPage.clickEditShippingAddress();
		Log.message(" Clicked on 'Edit Link' in Billing Address Page!");
		Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabShipping"), checkoutPage),
				"<b>Actual Result: </b>  Navigated to the 'Shipping Address' page after clicking on 'Edit' link in 'Shipping Address' section - header.",
				"<b>Actual Result: </b>  Do not Navigate to the 'Shipping Address' page after clicking on 'Edit' link in 'Shipping Address' section - header.",
				driver);
		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CART_067

@Test(groups = { "desktop",
"mobile" }, description = "Verify System displays the Shipping method.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_043(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);


	String username = testData.get("EmailAddress");
	String passwd = testData.get("Password");
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Navigate to Belk homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");

		MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
		Log.message("3. Navigated to My Account Page!");

		ShoppingBagPage shoppingbagpage = myAccountPage.clickOnMiniCart();
		Log.message("4. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

		// Navigated to checkout page as a registered user
		CheckoutPage checkoutPage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
		Log.message("5. Clicked on Checkout in Order summary Page");

		//checkoutPage.clickOnYesInShipToMultipleAddress();
		Log.message("Clicked on 'Yes' button");

		checkoutPage.clickOnAddInShipToMulipleAddressByRow(1);
		checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address2");
	    checkoutPage.clickContinueInAddressValidationModal();
	    checkoutPage.clickOnSaveInAddEditAddressModal();
	  /** need to update**/
	   // checkoutPage.selectdropdownandAddress(1,3);
    	checkoutPage.clickOnContinueInMultiAddress();
		Log.message("11. Clicked on 'Continue' in multiship address Page ");

		Log.message("<br>");
		Log.message("<b> Expected Result :</b> Shipping method should be displayed in the following order:<\b>");
        Log.message("<b> 1. In Store Pickup (BOPIS) - Non editable field.<\b>");
        Log.message("<b> 2. Ship to Home - Drop down field - editable.<\b>");
        Log.message("<b> 3. Registry - Drop down field - editable.<\b>");

     
		Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shipMethdInStorePickUp","drpDownShipppingMethods"),checkoutPage) ,
						"<b>Actual Result :</b> Shipping method displayed in proper order and the In Store Pickup shipping method is "+checkoutPage.getInStorePickupAddress(),
						"<b>Actual Result :</b> Shipping method not displayed in proper order ", driver);

		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CART_043

@Test(groups = { "desktop",
"mobile" }, description = "Verify the details in 'In store Pickup' shipping method.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_044(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

	String username = testData.get("EmailAddress");
	String passwd = testData.get("Password");
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	try {
		// Navigate to Belk homepage
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		SignIn signIn = homePage.headers.navigateToSignIn();
		Log.message("2. Navigated to Sign In Page!");

		MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
		Log.message("3. Navigated to My Account Page!");

		ShoppingBagPage shoppingbagpage = myAccountPage.clickOnMiniCart();
		Log.message("4. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

		// Navigated to checkout page as a registered user
		CheckoutPage checkoutPage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
		Log.message("5. Clicked on Checkout in Order summary Page");

		checkoutPage.clickOnYesInShipToMultipleAddress();
		Log.message("Clicked on 'Yes' button");
		  /** need to update**/
		checkoutPage.clickOnContinueInMultiAddress();
		Log.message("11. Clicked on 'Continue' in multiship address Page ");
	

		Log.message("<br>");
		Log.message(
				"<b> Expected Result :</b> 'Shipping method In-store Pickup FREE message should be displayed with the following details:-Store name,address&phone number");

		Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shipMethdInStorePickUp","addrInStorePickUp"),checkoutPage),
						"<b>Actual Result :</b>  Shipping method In-Store Pickup message :"+checkoutPage.getInStorePickupMethod()+" and Store details are displayed as :"+checkoutPage.getInStorePickupAddress(),
						"<b>Actual Result :</b>  Shipping method In-Store Pickup message not displayed with the Store details.", driver);

		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_CART_044

@Test(groups = { "desktop", "mobile",
"tablet" }, description = "Verify the 'Change Store' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_045(String browser) throws Exception {

// ** Loading the test data from excel using the test case id */
HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

String emailid = testData.get("EmailAddress");
String password = testData.get("Password");

// Get the web driver instance
final WebDriver driver = WebDriverFactory.get(browser);
Log.testCaseInfo(testData);
try {
// Launching Home page
HomePage homePage = new HomePage(driver, webSite).get();
Log.message("1. Navigated to 'Belk' Home Page!");

// Signing with a valid user id
SignIn signinPage = homePage.headers.navigateToSignIn();
Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
Log.message(
"3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page!");

// Navigate to Mini Cart
ShoppingBagPage shoppingBagPage = myAccountPage.clickOnMiniCart();
Log.message("4. Navigated to Shopping Bag Page!");

// Navigate to Checkout Page 
CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
Log.message("5. Navigated to Checkout Page!");

// Clicked 'Yes' on Multi Shipping Button
checkoutPage.clickOnYesInShipToMultipleAddress();
Log.message("6. Clicked 'Yes'  On Multi Shipping Button!");

// Clicking Add button to add the address
checkoutPage.clickOnAddInShipToMulipleAddressByRow(1);
Log.message("7. Clicked Add button To add address Detail For 1st product!");

Log.message("<br>");
Log.message(
		"<b> Expected Result 1: </b> The 'Change Store' link should be displayed in Step 1: Shipping Address Page!");
Log.assertThat(
		checkoutPage.elementLayer.verifyPageElements(Arrays.asList("changeStoreLnk"), checkoutPage),
		"<b> Actual Result 1: </b> Change Store Link is properly Displayed In Step1 : Shipping Address Page!",
		"<b> Actual Result 1: </b> Change Store Link is not properly Displayed In Step1 : Shipping Address Page!",
		driver);

// Filling Shipping Details
checkoutPage.fillingAddressDetailsInMultipleShipping("valid_address2");
checkoutPage.ContinueAddressValidationModalInMultipleShipping();
Log.message("8. Clicked on Continue In Validation Pop up!");

// Clicked Continue In Address Detail
checkoutPage.clickOnContinueInMultiAddress();
Log.message("9. Clicked on 'Continue' in multiship address Page!");

Log.message("<br>");
Log.message(
		"<b> Expected Result 1: </b> The 'Change Store' link should not be displayed in Step 2 and further Pages!");
Log.assertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("changeStoreLnk"), checkoutPage),
		"<b> Actual Result 1: </b> Change Store Link is not properly Displayed In Step 2 and Further pages!",
		"<b> Actual Result 1: </b> Change Store Link is not properly Displayed In Step 2 and Further pages!",
		driver);

// Clicked Continue In Shipping Detail
checkoutPage.clickOnContinueInShipping();
Log.message("10. Clicked on 'Continue' in multiship method Page!");

Log.testCaseResult();

} // try
catch (Exception e) {
Log.exception(e, driver);
} // catch
finally {
Log.endTestCase();
//driver.quit();
} // finally
}// TC_BELK_CHECKOUT_045

@Test(groups = { "desktop", "tablet",
"mobile" }, description = "Verify warning message is shown when applying Belk credit card and Reward dollars!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_CHECKOUT_092(String browser) throws Exception {

// ** Loading the test data from excel using the test case id */
HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
String searchKey = testData.get("SearchKey");

// Get the web driver instance
final WebDriver driver = WebDriverFactory.get(browser);
Log.testCaseInfo(testData);
try {
// Load the Home Page
HomePage homePage = new HomePage(driver, webSite).get();
Log.message("1. Navigated to 'Belk' Home Page!");

// Load the SearchResult Page with search keyword
PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page!");

// Load the PDP Page with search keyword
Log.message("3. Navigated to PDP Page!");

// Selecting the Color
String color = pdpPage.selectColor();
Log.message("4. selected the '" + color + "' Color!");

// Selecting the Size
String size = pdpPage.selectSize();
Log.message("5. selected the '" + size + "' size!");

// Adding item to the shopping bag
pdpPage.clickAddToBag();
Log.message("6. Product added to Bag!");

// Navigating to shopping bag
ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
Log.message("7. Navigated to shopping bag page!");

// Navigating to signIn page
SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
Log.message("8. Navigated to SignIn page!");

// clicking Checkout As Guest
CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
Log.message("9. clicked on Checkout As Guest Button!");

// filling shipping details
checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
Log.message("10. shipping details filled successfully!");

// clicking continue button
checkOutPage.clickOnContinueInShipping();
checkOutPage.ContinueAddressValidationModalWithDefaults();
Log.message("11. clicked on continue button!");

// filling billing details
checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
Log.message("12. Billing address details are entered!");

// Filling Card Details
checkOutPage.fillingCardDetails("NO", "card_BCC_E2E2");
Log.message("13. Payment Details Filled Successfully!");

// Filling Belk Reward Dollar
checkOutPage.fillingBelkRewardDollars("belk_Reward_Dollar_new4");
checkOutPage.clickOnApplyBelkRewardDollars();
Log.message("14. Entered Belk Reward Dollar details!");

// clicking continue button
checkOutPage.clickOnContinueInBilling();
Log.message("15. Clicked on 'Continue' Button in Billing Page!");

// Click Place order
checkOutPage.placeOrder();
Log.message("16. Clicked on Place Order Button!");
Log.testCaseResult();

} // try
catch (Exception e) {
Log.exception(e, driver);
} // catch
finally {
Log.endTestCase();
driver.quit();
} // finally
}// TC_BELK_CHECKOUT_092


}// checkout
