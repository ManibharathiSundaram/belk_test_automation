package com.belk.testscripts.endtoend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CheckoutPage;
import com.belk.pages.HomePage;
import com.belk.pages.OrderConfirmationPage;
import com.belk.pages.PdpPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.WishListPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.ObjAndDataToReturn;
import com.belk.reusablecomponents.e2eUtils;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class E2E_Combination {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "E2E_Combination";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System.getProperty("webSite")
				: context.getCurrentXmlTest().getParameter("webSite"));
	}


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_CHK_GC_BALANCE(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signed
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();

			checkoutPage.fillingShippingDetailsAsGuest(address, shippingMethod); // shipping

			checkoutPage.clickOnContinueInShipping();

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); // signed

			e2eUtils.checkGiftCardBalance(checkoutPage, driver);

			Log.testCaseResult();

			if(("Dhanapal".compareTo("dhanapal") > 0)){

			}

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_107

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_R(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			Log.message(i++ + ". Payment details", driver);
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_R_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			Log.message(i++ + ". Payment details", driver);
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_G_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page as Guest User!", driver);

			checkoutPage.fillingShippingDetailsAsGuest(address, shippingMethod);  
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			Log.message(i++ + ". Payment details", driver);
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_E2E_

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_G(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 

			checkoutPage.fillingShippingDetailsAsGuest(address, shippingMethod); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			Log.message(i++ + ". Payment details", driver);
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_E2E_

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_R_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);

			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_R(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);

			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_G(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver);

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);

			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_G_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver);

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();

			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);

			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_

	@Test(groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_R_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_R_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_G_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 

			checkoutPage.fillingShippingDetailsAsGuest(address, shippingMethod); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_G_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 

			checkoutPage.fillingShippingDetailsAsGuest(address, shippingMethod); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsGuest(address);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop","desktop_bat" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_WL_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		String[] couponData = testData.get("Coupon").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.createWishListAccount(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.addProductToList(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
			String productName = productDetailsInPDP.get(0).get("ProductName");

			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message(i++ + ". Navigate to wish List Page!", driver);

			e2eUtils.addProductFromListToBag(wishListPage, productName);

			ShoppingBagPage shoppingBagPage = wishListPage.headers.NavigateToBagPage();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
																														
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
			
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop","desktop_bat" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_WL_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.createWishListAccount(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.addProductToList(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
			String productName = productDetailsInPDP.get(0).get("ProductName");

			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message(i++ + ". Navigate to wish List Page!", driver);

			e2eUtils.addProductFromListToBag(wishListPage, productName);

			ShoppingBagPage shoppingBagPage = wishListPage.headers.NavigateToBagPage();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
																														
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
			
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_REG(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
		
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signInWithRegistryUser(homePage, "weddingRegistry", "valid_shipping_address6");

			ObjAndDataToReturn objAndPrdData = e2eUtils.addProductToRegistry(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			RegistrySignedUserPage registrySignedUserPage = pdpPage.headers.navigateToASingedUserRegistry();
			Log.message(i++ + ". Navigate to Registry Page!");
			String productName = productDetailsInPDP.get(0).get("ProductName");

			e2eUtils.addProductFromRegistryToBag(registrySignedUserPage, productName);

			ShoppingBagPage shoppingBagPage = registrySignedUserPage.headers.NavigateToBagPage();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			Log.message(i++ + ". Payment Details", driver);
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getRegistryShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications( productDetailsInPDP,pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder,pdtDetailsInCheckoutOrder, costDetailsInCart,
					costDetailsInCHKShipping, costDetailsInCHKBilling,costDetailsInCHKPlaceOrder, costDetailsInOrderConfirmation,
					shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation,
					billingAddressInCheckoutPage, billingAddressInPlaceOrder,billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_E2E_


	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_R_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		String[] couponData = testData.get("Coupon").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
	
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);
	
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();
	
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);
	
			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_E_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);
	
			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_E_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		String shippingMethod = testData.get("ShippingMethod");
		String[] couponData = testData.get("Coupon").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_E_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		String shippingMethod = testData.get("ShippingMethod");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SS_BOPIS_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		String[] couponData = testData.get("Coupon").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
		String shippingMethod = testData.get("ShippingMethod");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signe
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			//checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",shippingMethod, address);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
			
			

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SS_BOPIS_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String[] couponData = testData.get("Coupon").split("\\|");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signe
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			//checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",shippingMethod, address);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
			
			

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
	
	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SS_BOPIS_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String address = testData.get("Address");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
		String shippingMethod = testData.get("ShippingMethod");

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);

			// Create Account and do SignIn
			e2eUtils.signIn(homePage);

			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);

			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signe
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			//checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",shippingMethod, address);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();

			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);

			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);

			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
			
			

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();

			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);

			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();

			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));

			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SS_BOPIS_E_CUP_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		String[] couponData = testData.get("Coupon").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
		String shippingMethod = testData.get("ShippingMethod");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			// To check Giftbox
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);
	
			// To apply coupon
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signe
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			//checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",shippingMethod, address);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
			
			
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_E(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		String shippingMethod = testData.get("ShippingMethod");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!",driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver); 
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_E(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };
		
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
	
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);
	
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();
	
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);
	
			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_R_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] couponData = testData.get("Coupon").split("\\|");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
	
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);
	
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();
	
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);
	
			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_


	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_E_GB(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] giftBox = testData.get("giftBoxProductName").split("\\|");
		List<String> giftBoxProductNames = new ArrayList<String>();
		String[] usernamePasswd = { testData.get("EmailAddress"), testData.get("Password") };

		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage, usernamePasswd);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver, giftBox);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			giftBoxProductNames = objAndPrdData.giftBoxProductNames;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			e2eUtils.checkGiftBox(giftBoxProductNames, shoppingBagPage);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
	
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);
	
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();
	
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);
	
			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO",address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_ML_MS_G_CUP(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String[] address = testData.get("Address").split("\\|");
		String[] shippingType = testData.get("ShippingMethod").split("\\|");
		String[] couponData = testData.get("Coupon").split("\\|");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			e2eUtils.applyCoupon(couponData, shoppingBagPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			SignIn signIn = (SignIn)shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message(i++ + ". Navigated to Shipping Page as Guest user!", driver);
	
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message(i++ + ". Navigated to Multiple Address Shipping Page!", driver);
	
			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage.fillingAddressDetailsInMultipleShipping(address[g]);
				if (checkoutPage.clickContinueInAddressValidationModal()){
					checkoutPage.chooseOriginalAddressInModal();
					checkoutPage.clickOnSaveInAddEditAddressModal(); }
			}
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCheckoutPage = checkoutPage.selectAddrInAddressDropDownInMultiship();
			Log.message(i++ + ". Shipping details filled and Selected appropriately!", driver);
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingMethodsInMultiship = new LinkedHashMap<String, String>();
	
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message(i++ + ". Navigated to Shipping Method Tab in Checkout Page", driver);
	
			for (int r = 0; r < checkoutPage.getNosShippingMethodInMultishipping(); r++) {
				shippingMethodsInMultiship.put("ShippingMethod" + (r + 1),
						checkoutPage.selectShippingMethodInMultiShippingByRow(r + 1, shippingType[r]));
			}
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsGuest(address[0]);
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKBilling = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedList<LinkedHashMap<String, String>> shippingDetailsInCHKPlaceOrder = checkoutPage
					.getMultiShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary();
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_E2E_


	@Test(groups = { "desktop","desktop_bat" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SL_SS_WL(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
		String shippingMethod = testData.get("ShippingMethod");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.createWishListAccount(homePage);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.addProductToList(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
			String productName = productDetailsInPDP.get(0).get("ProductName");
	
			WishListPage wishListPage = pdpPage.headers.navigateToWishList();
			Log.message(i++ + ". Navigate to wish List Page!", driver);
	
			e2eUtils.addProductFromListToBag(wishListPage, productName);
	
			ShoppingBagPage shoppingBagPage = wishListPage.headers.NavigateToBagPage();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); 
																														
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", shippingMethod, address); 
			Log.message(i++ + ". Shipping Details filled into appropriate fields!", driver);
			
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage
					.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Continue Button clicked in Checkout(Shipping Tab) Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_


	@Test(enabled=false, groups = { "desktop",
	"mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_E2E_SS_BOPIS(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String address = testData.get("Address");
	
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message(i++ + ". Navigated to 'Belk' Home Page!", driver);
	
			// Create Account and do SignIn
			e2eUtils.signIn(homePage);
	
			ObjAndDataToReturn objAndPrdData = e2eUtils.doOperation(testData, driver);
	
			PdpPage pdpPage = objAndPrdData.pdpPage;
			LinkedList<LinkedHashMap<String, String>> productDetailsInPDP = objAndPrdData.prdDetailsList;
	
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message(i++ + ". Navigate to shopping bag Page!", driver);

			LinkedList<LinkedHashMap<String, String>> pdtDetailsInShoppingBagPage = shoppingBagPage.getProductDetails();
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage.getOrderSummaryDetails();
	
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage); // signe
			Log.message(i++ + ". Navigated to Shipping Tab in Checkout Page!", driver);
			
			//checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",shippingMethod, address);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutShipping = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> costDetailsInCHKShipping = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> shippingDetailsInCheckoutPage = checkoutPage.getShippingAddressInShippingPage();
	
			checkoutPage.clickOnContinueInShipping();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message(i++ + ". Navigated to Billing Tab in Checkout Page!", driver);
	
			 
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", address); 
																					
			Log.message(i++ + ". Billing address details filling appropriate fields", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInBilling = e2eUtils
					.fillPaymentDetails(testData, checkoutPage, driver);
			
			
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutBilling = checkoutPage
					.getProductDetailsInOrderSummary1();
			LinkedHashMap<String, String> shippingDetailsInCHKBilling = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> costDetailsInCHKBilling = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddressInCheckoutPage = checkoutPage.getBillingAddressInBillingPage();
	
			checkoutPage.clickOnContinueInBilling();
			Log.message(i++ + ". Navigated to Place Order Tab in Checkoutpage!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutPlaceOrder = checkoutPage
					.getProductDetailsInPlaceOrder1();
			LinkedHashMap<String, String> shippingDetailsInCHKPlaceOrder = checkoutPage.getShippingAddress();
			LinkedHashMap<String, String> billingAddressInPlaceOrder = checkoutPage.getBillingAddress();
			LinkedHashMap<String, String> costDetailsInCHKPlaceOrder = checkoutPage.getCostDetailsInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInPlaceOrder = checkoutPage
					.getPaymentDetailsInPlaceOrder(checkoutPage);
	
			OrderConfirmationPage orderConfirmationPage = checkoutPage.placeOrder();
			Log.message(i++ + ". Order Placed successfully!", driver);
	
			LinkedList<LinkedHashMap<String, String>> pdtDetailsInCheckoutOrder = orderConfirmationPage
					.getProductDetailsInOrderSummary();
			LinkedHashMap<String, String> billingAddrInOrderConfirmation = orderConfirmationPage
					.getBillingAddressInOrderSummary();
			LinkedHashMap<String, String> shippingAddrInOrderConfirmation = orderConfirmationPage
					.getShippingAddressInOrderSummary().get(0);
			LinkedHashMap<String, String> costDetailsInOrderConfirmation = orderConfirmationPage
					.getPaymentDetailsInOrderSummary();
			LinkedHashMap<String, String> orderDetails = orderConfirmationPage.getOrderInfoInOrderSummary();
			LinkedHashMap<String, LinkedHashMap<String, String>> paymentInfoInOrderConfirmation = orderConfirmationPage
					.getPaymentMethodInOrderSummary_e2e();
	
			List<String> orderDetailsKey = new ArrayList<String>(orderDetails.keySet());
			Log.message("--->Order Number : " + orderDetails.get(orderDetailsKey.get(2)));
			Log.message("--->Order Date   : " + orderDetails.get(orderDetailsKey.get(0)));
			Log.message("--->Order Time   : " + orderDetails.get(orderDetailsKey.get(1)));
	
			e2eUtils.doVerifications(productDetailsInPDP, pdtDetailsInShoppingBagPage, pdtDetailsInCheckoutShipping,
					pdtDetailsInCheckoutBilling, pdtDetailsInCheckoutPlaceOrder, pdtDetailsInCheckoutOrder,
					costDetailsInCart, costDetailsInCHKShipping, costDetailsInCHKBilling, costDetailsInCHKPlaceOrder,
					costDetailsInOrderConfirmation, shippingDetailsInCheckoutPage, shippingDetailsInCHKBilling,
					shippingDetailsInCHKPlaceOrder, shippingAddrInOrderConfirmation, billingAddressInCheckoutPage,
					billingAddressInPlaceOrder, billingAddrInOrderConfirmation, paymentInfoInBilling,
					paymentInfoInPlaceOrder, paymentInfoInOrderConfirmation, driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_E2E_
}
