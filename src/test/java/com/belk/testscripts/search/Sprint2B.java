package com.belk.testscripts.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CategoryLandingPage;
import com.belk.pages.HomePage;
import com.belk.pages.MiniCartPage;
import com.belk.pages.Pagecontent;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.QuickViewPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.footers.ListOfBrandPage;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class Sprint2B {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "Search";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to click on a entry from 'Brands' list in Search Suggestions Pop-up screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_014(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String contentheading = "buying shoes 2";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Pagecontent searchresultPagecontent = homePage
					.searchProductWithAutoSuggestionPageContent(searchKey);

			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			Log.message("<b> Expected Result: </b> Clicking on the link should take me to 'Shoes' Content page");
			Log.assertThat(
					searchresultPagecontent.getSearchPageContent().contains(
							contentheading),
					"<b> Actual Result: </b> Buying shoes 2 heading is displayed in the breadcrumb",
					"<b> Actual Result: </b> Buying shoes 2 heading is not displayed in the breadcrumb",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_014

	@Test(groups = { "desktop" }, description = "Verify system displays the following under the refinements", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_110(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is not applicable for mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> filterbyheading = Arrays.asList("sectionFilterBy");
		List<String> categoryrefinemnetoption = Arrays.asList("btnsize",
				"lblBrand");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			Log.message("<b> Expected Result: </b> 'Refine by' must be removed from the refinement and 'Filter by' option should get display on the refinement.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							filterbyheading, searchresultPage),
					"<b> Actual Result: </b> FilterBy heading is displayed on the left Nav.and RefineBy options is not displayed",
					"<b> Actual Result: </b> FilterBy heading is not displayed on the left Nav. and RefineBy option is displayed on the Left Nav. ",
					driver);
			Log.message("<b> Expected Result: </b> Category refinement should get display on the page.");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							categoryrefinemnetoption, searchresultPage),
					"<b> Actual Result: </b> CategoryRefinemnet heading is displayed on the left Nav.",
					"<b> Actual Result: </b> CategoryRefinemnet is not displayed on the left Nav. ",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_110

	@Test(groups = { "desktop" }, description = "Verify 'View More' link appear after “n” number of value", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_111(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> brandviewmoreoption = Arrays.asList("btnBrandViewMore");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			Log.message("<b> Expected Result: </b> System should display the 'View More' link.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							brandviewmoreoption, searchresultPage),
					"<b> Actual Result: </b> ViewMore option is displayed on Left Nav.",
					"<b> Actual Result: </b> ViewMore option is not displayed on Left Nav.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_111

	@Test(groups = { "desktop" }, description = "Verify system collapse back to the configured number for that refinement, when customer click on the 'View Link'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_113(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is not applicable for mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			searchresultPage.clickViewMoreOrLessLnk("Color");
			Log.message("3. Click on View More Link under color refinement value in Left Nav.");
			searchresultPage.verifyExpandToggle("Color");
			Log.message("4. Color panel is in expanded form");
			searchresultPage.clickViewMoreOrLessLnk("Color");
			Log.message("<b> Expected Result: </b> System should collapse back to the configured number for that refinement.");
			Log.message(
					"<b> Actual Result: </b> Click on View Less Link under color refinement value in Left Nav.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_113

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable sizes in Size refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_140(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();
			}

			String size = searchresultPage
					.selectUnselectFilterOptions("refinementSize");
			Log.message("3. Selecting " + size
					+ " in the refinement under color section");
			Log.message("<b> Expected Result: </b> Selectable sizes should be displayed as a checkbox list with the size number or size value.");
			String[] selectedSizeCount = size.split("\\(");
			Log.message("4. Selected size name :" + selectedSizeCount[0]
					+ " and size count" + selectedSizeCount[1]
					+ " in the refinement under size section");
			Log.message(
					"<b> Actual Result: </b> Selected size is displayed as a checkbox list with the size number or size value.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_140

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Color Refinement Heading under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_146(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String searchTextBox = "Your search results for \"" + searchKey + "\"";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> colorheading = Arrays.asList("fldColorHeading");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			Log.assertThat(
					searchresultPage.getTextFromSearchResult().contains(
							searchTextBox), searchTextBox
							+ " item is display in the search result page",
					searchTextBox
							+ " item is not display in the search result page");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();

			}

			searchresultPage.verifyRefinement("Color");
			Log.message("<b> Expected Result: </b> The refinement heading 'Color' should be displayed bold in blue color.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							colorheading, searchresultPage),
					"<b> Actual Result: </b> Colour Heading is displayed in the Let Nav.",
					"<b> Actual Result: </b> Colour heading is not  displayed in the Let Nav",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_146

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Top Rated'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_201(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is not applicable for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1]);

			PlpPage plpPage = new PlpPage(driver).get();
			plpPage.selectSortByDropDownByIndex(4);
			String getmostrecentvalue = plpPage.getSelectedSortByValue();
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation");
			Log.assertThat(getmostrecentvalue.equals(plpPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown "
							+ getmostrecentvalue
							+ " is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown "
							+ getmostrecentvalue
							+ " is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_201

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Most Recent'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_199(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1]);
			CategoryLandingPage clPage = new CategoryLandingPage(driver).get();

			clPage.selectSortByDropDownByIndex(3);
			String getmostrecentvalue = clPage.getSelectedSortByValue();
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation ");
			Log.assertThat(getmostrecentvalue.equals(clPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown  "
							+ getmostrecentvalue
							+ " is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown  "
							+ getmostrecentvalue
							+ " is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_199

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Badge Styling in Product Tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_192(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> verifyproductbadge = Arrays.asList("txtProductBadge");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with the  '" + searchKey
					+ "' in search text box and navigated to 'PDP'  Page!");
			Log.message("<b> Expected Result: </b> Product badges should be displayed in different style based on the badge type, color and text.");
			Log.assertThat(
					pdpPage.verifyBadgeRefinementText(),
					"<b> Actual Result: </b> The Badge text is present on the Product tile with the style based",
					"<b> Actual Result: </b> The Badge text is not present on the Product tile ",
					driver);
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(verifyproductbadge,
							pdpPage),
					"<b> Actual Result: </b> Product Badging is present on the Product title in the SRP page",
					"<b> Actual Result: </b>  Product Badging is not present on the Poduct title in the SRP page",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_192

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Brands Refinement Heading under product refinements panels.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_155(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> brandheading = Arrays.asList("lblBrand");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();

			}

			Log.message("<b> Expected Result: </b> The refinement heading 'Brands' should be displayed bold in blue color.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							brandheading, searchresultPage),
					"<b> Actual Result: </b> Brand heading is displayed on Left Nav.",
					"<b> Actual Result: </b> Brand heading  is not displayed on Left Nav.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_155

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable colors in Color refinement under product refinements panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_149(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();

			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("3. Selecting " + color
					+ " in the refinement under color section");

			String[] selectedColorCount = color.split("\\(");
			Log.message("<b> Expected Result: </b> Selectable colors should be displayed as a checkbox list with the Color Name, Color Swatch, and Refinement Count");
			Log.message("<b> Actual Result: </b> Selected color name :"
					+ selectedColorCount[0] + " and color count"
					+ selectedColorCount[1]
					+ " in the refinement under color section", driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_149

	@Test(groups = { "desktop" }, description = "Verify Quick View icon on the product thumbnail in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_188(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is not aplicable  for mobile");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> verifyproductbadge = Arrays.asList("txtProductBadge");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							verifyproductbadge, searchresultPage),
					"3. Product Badging is present in the Poduct title in the CLP page",
					"3.  Product Badging is not present in the Poduct title in the CLP page",
					driver);
			Log.message("<b> Expected Result: </b> When a customer hovers over a product thumbnail, the Quick View icon should display in center of the product tile");
			QuickViewPage quickViewPage = searchresultPage
					.navigateToQuickViewByIndex(1);
			Log.message("<br>");
			Log.message(
					"<b> Actual Result: </b> Quick view icon present on the product image",
					driver);
			quickViewPage.selectSizeByIndex(1);
			Log.message("3. Select Size from the drop down list");
			quickViewPage.selectColor();
			Log.message("4. Select Color from the color panel options ");
			quickViewPage.selectQuantity("4");
			Log.message("5. Select Quantity from thr drop down list");
			quickViewPage.clickAddToBag();
			Log.message("6. Click on the Add to Bag");
			MiniCartPage minicartmousehover = new MiniCartPage(driver).get();
			minicartmousehover.mouseOverMiniCart();
			Log.message("<b> Expected Result: </b>  User should see a 'Added Successfully to Bag' Mini-Cart hover after tapping on 'Add to Bag' on Quick View.");
			Log.message("<br>");
			Log.message(
					"<b> Actual Result: </b> Mouse hover on the mini cart after adding the product to the cart",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_188

	@Test(groups = { "mobile", "desktop", "tablet" }, description = "Verify that the Product Grid is shown in Category Page when template is updated in Business Manager", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_231(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> leftnavigationpanel = Arrays.asList("leftNavPanel");
		List<String> categorypageitems = Arrays.asList("drpSortBy",
				"fldToggleView");
		List<String> refinement = Arrays.asList("lstLeftNavRefinement");
		try {

			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plpPage = new PlpPage(driver).get();

			if (runPltfrm == "desktop") {
				Log.message("<b> Expected Result: </b> Left Navigation Panel");
				Log.assertThat(
						plpPage.elementLayer.verifyPageElements(
								leftnavigationpanel, plpPage),
						"<b> Actual Result: </b>  Left Navigation Panel is displayed on the CLP Page",
						"<b> Actual Result: </b>  Left Navigation Panel is not  displayed on the CLP Page",
						driver);
				Log.message("<b> Expected Result: </b> Product Grid slots above the Products and elements of Category page including Sort options and View Toggle");
				Log.assertThat(
						plpPage.elementLayer.verifyPageElements(
								categorypageitems, plpPage),
						"<b> Actual Result: </b> Category options -'toggle,grid slot view toggle' on clp page is present",
						"<b> Actual Result: </b> Category options -'toggle,grid slot view toggle' on clp page is not present",
						driver);
			}

			if (runPltfrm == "mobile") {

				plpPage.clickFilterByInMobile();
				Log.message("<b> Expected Result: </b> Left Navigation Panel");
				Log.assertThat(
						plpPage.elementLayer.verifyPageListElements(refinement,
								plpPage),
						"<b> Actual Result: </b> Refinement is displayed when clicking on 'Filter By' button",
						"<b> Actual Result: </b> Refinement is displayed when clicking on 'Filter By' button",
						driver);
				Log.assertThat(
						plpPage.elementLayer.verifyPageElements(
								categorypageitems, plpPage),
						"<b> Actual Result: </b> Category options -'toggle,grid slot view toggle' on clp page is present",
						"<b> Actual Result: </b> Category options -'toggle,grid slot view toggle' on clp page is not present",
						driver);
			}

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_231

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting Back to ___ link below the L3 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_129(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> breadcrumb = Arrays.asList("bcLastCategoryName");
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1] + ">>>" + category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			if (runPltfrm == "desktop") {
				plppage.clickOnBackButton();
				Log.message("<b> Expected Result: </b> It should be navigated to L1 categories page.");
				Log.assertThat(
						plppage.elementLayer.verifyPageElements(breadcrumb,
								plppage),
						"<b> Actual Result: </b>  Click on the Back to L1 category name link below the L3 categories in the left navigation of category refinements panel L1 category is displayed in breadcrumb in the CLP",
						"<b> Actual Result: </b> Not able to click on the  Back to L1 category name link below the L3 categories in the left navigation of category refinements panel and  L1 category is not displayed in breadcrumb in the CLP",
						driver);
			}
			if (runPltfrm == "mobile") {
				Log.assertThat(
						plppage.verifyBackToCategoyInBreadCrumb("Back to Big & Tall Clothing "),
						"3. Navigated category option is displayed in the breadcrum",
						"3. Navigated category option is not displayed in the breadcrum ");
				driver.getCurrentUrl();
				plppage.backToCategoyInBreadCrumbCategory("Back to Big & Tall Clothing ");
				Log.message("<b> Expected Result: </b> It should be navigated to Home categories page.");
				Log.message(
						"<b> Actual Result: </b>  Click on the Back to L1 category name link below the L3 categories in the left navigation of category refinements panel L1 category is displayed in breadcrumb in the CLP",
						driver);

			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_129

	@Test(groups = { "desktop", "mobile" }, description = "Verify product list row should be properly displayed in PDP page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_225(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");
			searchResultPage.clickGridListToggle("List");
			Log.message("3. Clicked on 'List' Toggle in Search Result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Product list row should be properly displayed right side of the product image");
			Log.assertThat(
					searchResultPage.getCurrentLocationOfPdtByIndex(1) == searchResultPage
							.getCurrentLocationOfPdtByIndex(2)
							&& searchResultPage
									.getCurrentLocationOfPdtByIndex(1) == searchResultPage
									.getCurrentLocationOfPdtByIndex(3)
							&& searchResultPage
									.getCurrentLocationOfPdtByIndex(1) == searchResultPage
									.getCurrentLocationOfPdtByIndex(4)
							&& searchResultPage.elementLayer
									.verifyPageListElements(
											Arrays.asList("lstProducts"),
											searchResultPage),
					"<b>Actual Result :</b> Product list row is properly displayed right side of the product image.",
					"<b>Actual Result :</b> Product list row is not properly displayed right side of the product image.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_105

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the product number  based upon the option chosen by the customer in the Items per page drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_080(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			int productCountbeforeSelectingOptions = searchResultPage
					.getProductCount();
			Log.message("3. Product Count: '"
					+ productCountbeforeSelectingOptions
					+ "' before selecting the options from 'Items per Page DropDown'");

			searchResultPage.selectItemsPerPageDropDownByText("View 60");

			Log.message("4. Selected Options From 'Items Per Page' Drop Down");
			int productCountAfterSelectingOptions = searchResultPage
					.getProductCount();
			Log.message("5. Product Count: '"
					+ productCountAfterSelectingOptions
					+ "' after selecting the options from 'Items per Page DropDown'");

			String Viewingresult = searchResultPage.getResultHitsText();
			String resultsplit[] = Viewingresult.split("\\-");
			String resultsplittoGetTheProductCount[] = resultsplit[1]
					.split(" ");
			int productCountInViewPageResult = Integer
					.parseInt(resultsplittoGetTheProductCount[1]);
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> system should displays the product number based upon the option chosen by the customer in the Items per page drop down.");
			Log.assertThat(
					productCountInViewPageResult == productCountAfterSelectingOptions,
					"<b>Actual Result :</b> System displayed the product number based upon the option chosen by the customer in the 'Items per page' drop down.",
					"<b>Actual Result :</b> System failed to display the product number based upon the option chosen by the customer in the 'Items per page' drop down.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet" }, description = "verify the deepest category in the catalog tree", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_116(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop")
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for "
							+ runPltfrm);
		else {
			// Loading the test data from excel using the test case id
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			ArrayList<String> breadcrumbText = new ArrayList<String>();
			ArrayList<String> categoryText = new ArrayList<String>();
			String[] category = testData.get("SearchKey").split("\\|");
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				homePage.headers.navigateTo(category[0], category[1],
						category[2]);
				Log.message("2. Navigated to " + category[2] + " page.");
				PlpPage plppage = new PlpPage(driver).get();

				String color1 = plppage
						.selectUnselectFilterOptions("refinementColor");
				Log.message("3. Selecting 1st Refinement");

				String color2 = plppage
						.selectUnselectFilterOptions("refinementColor");
				Log.message("4. Selecting 2nd Refinement");
				breadcrumbText = plppage.getTextInBreadcrumb();
				breadcrumbText = plppage.getTextInBreadcrumb();
				categoryText.add(0, "Home");
				for (int i = 1; i < breadcrumbText.size(); i++) {
					categoryText.add(i, category[i - 1]);
				}
				SearchResultPage searchresultpage = new SearchResultPage(driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b> By selecting the refinement values, page content should be refresh/update");
				searchresultpage.verifySpinner("refinementColor");
				Log.message(
						"<b>Actual Result 1:</b> By selecting the refinement values, page content is getting refreshed/updated",
						driver);

				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b> User should see the deepest category in the catalog tree");

				Log.assertThat(
						breadcrumbText.get(1).equals(categoryText.get(1)),
						"<b>Actual Result 2:</b> User is able to see the deepest category in the catalog tree",
						"<b>Actual Result 2:</b> User is not able to see the deepest category in the catalog tree ",
						driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_SEARCH_116

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays a View All Article link if the maximum of 7 articles are returned.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_095(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> System should displays a View All Article link if there are more than 7 articles are returned.");

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("btnViewAllArticles"),
							searchResultPage)
							&& searchResultPage.checkArticlesCount() == 7,
					"<b>Actual Result :</b> System Displayed 'View All Article' Link ,If more than 7 articles are returned",
					"<b>Actual Result :</b> System failed to Display 'View All Article' Link ,If more than 7 articles are returned");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_095

	@Test(groups = { "desktop", "mobile" }, description = "Verify system navigates the customer to previous search page, when customer click on the 'Back to Product Result' in the View All article page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			searchResultPage.checkArticlesCount();
			Log.message("3. Clicked On 'View All Articles' Link and Navigated to View All Article Page!");

			searchResultPage.clickOnBackToProductResults();
			Log.message("4. Clicked On 'Back to Product Result' Link in the View All Article page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> When customer click on the Back to Product Result in the View All article page, System should navigate the customer to previous search.");
			Log.assertThat(
					driver.getCurrentUrl().contains("/search/"),
					"<b>Actual Result 1:</b> When customer clicks on the Back to Product Result in the View All article page, System navigated the customer to previous search.",
					"<b>Actual Result 1:</b> When customer clicks on the Back to Product Result in the View All article page, System not navigated the customer to previous search.");

			searchResultPage.getTextFromSearchResult();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Your search results for <keyword> should be displayed in Breadcrum");
			Log.assertThat(
					searchResultPage.getTextFromSearchResult().contains(
							searchKey),
					"<b>Actual Result 2:</b> Your search results for <"
							+ searchKey + "> is displayed in Breadcrum.",
					"<b>Actual Result 2:</b> Your search results for <"
							+ searchKey + "> is not displayed in Breadcrum.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_105

	@Test(groups = { "desktop", "mobile" }, description = "Check the products list displayed in the Products matches list in Suggestion Pop-up list.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and AutoSuggestion PopUp is Opened");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Products matching to the entered search criteria should be listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].");
			Log.assertThat(
					homePage.elementLayer.verifyPageListElements(
							Arrays.asList("lstProductNameInPdtSuggestions"),
							homePage)
							&& homePage.getProductNameFromProductSuggestion(1)
									.contains(searchKey)
							&& homePage.elementLayer
									.verifyPageListElements(
											Arrays.asList("lstsrchSuggtnProductPrice "),
											homePage),
					"<b>Actual Result 1:</b> Products matching to the entered search criteria is listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].",
					"<b>Actual Result 1:</b> Products matching to the entered search criteria is not listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_019

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system disable the page bar, if the products is less than 30.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_082(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<b>Expected Result:</b> The page bar should be disabled when products count is lessthan 30  in the 'SearchResult' Page");
			Log.assertThat(
					searchResultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstPageNos"),
									searchResultPage),
					"<b>Actual Result:</b> The page bar is disabled when products count is lessthan 30  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The page bar is not disabled when products count is lessthan 30  in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_082

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Content Refinement' instead of 'Category Refinement' with the default styling.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_106(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			searchResultPage.checkArticlesCount();
			Log.message("3. Clicked On 'View All Articles' Button");

			Log.message("<b>Expected Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' should be displayed in the 'SearchResult' Page");
			Log.assertThat(
					(searchResultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstCategoryRefinement"),
									searchResultPage) && (searchResultPage.elementLayer
							.verifyPageElements(
									Arrays.asList("contentRefinement"),
									searchResultPage))),
					"<b>Actual Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' is displayed in the 'SearchResult' Page",
					"<b>Actual Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' is not displayed in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_106

	@Test(groups = { "desktop" }, description = "Verify system expands and the full set of refinement values displays with a 'view less' link, when customer click on the 'View more' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_123(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			int refinementCount = searchResultPage
					.refinementValuesCountViewMore("refinementSize");

			Log.message("<b>Expected Result:</b> The Refinement Values count should be 7 with 'ViewMore' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCount == 7,
					"<b>Actual Result:</b> The Refinement Values count is 7 with 'ViewMore' Link in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not 7 with 'ViewMore' Link in the 'SearchResult' Page",
					driver);

			searchResultPage.clickViewMoreLink("Size");
			Log.message("3. Clicked On 'ViewMore' Link, in the refinementSection in  the Searchresult page");

			int refinementCountAfterViewMore = searchResultPage
					.refinementValuesCountViewLess("refinementSize");
			Log.message("" + refinementCountAfterViewMore);

			Log.message("<b>Expected Result:</b> The Refinement Values count should be greaterthan 7 when we click on 'ViewMore' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCountAfterViewMore > 7,
					"<b>Actual Result:</b> The Refinement Values count is 7  greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not 7 greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					driver);

			searchResultPage.clickViewLessLnk("Size");
			Log.message("4. Clicked On 'ViewLess' Link, in the refinementSection in  the Searchresult page");

			int refinementCountAfterViewLess = searchResultPage
					.refinementValuesCountViewMore("refinementSize");

			Log.message("<b>Expected Result:</b> The Refinement Values count should be greaterthan 7 when we click on 'ViewMore' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCountAfterViewLess == 7,
					"<b>Actual Result:</b> The Refinement Values count is greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_123

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the indication of L3  sub-categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_136(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			Log.message("<b>Expected Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol should be displayed in the CategoryLanding Page");
			Log.assertThat(
					categoryLandingPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstL2Sub_Refinements"),
							categoryLandingPage),
					"<b>Actual Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol is displayed in the CategoryLanding Page",
					"<b>Actual Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol is not displayed in the CategoryLanding Page",
					driver);

			String Category_Name = categoryLandingPage
					.clickCategoryRefinementToggle();
			Log.message("3. Clicked On Category Refienment Toggle and naviagted to Next Level Refienment");

			ArrayList<String> BCValues = categoryLandingPage
					.getTextInBreadcrumb();
			String BCL3_category = BCValues.get(BCValues.size() - 1);

			Log.message("<b>Expected Result2:</b> On clicking the '>' symbol, the page should be redirected to the selected L3 sub category page");
			Log.assertThat(
					Category_Name.trim().contains(BCL3_category.trim()),
					"<b>Actual Result2:</b> On clicking the '>' symbol, the page is redirected to the selected L3 sub category page",
					"<b>Actual Result2:</b> On clicking the '>' symbol, the page is not redirected to the selected L3 sub category page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_136

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting 'Back to <Category name>' below the L5 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_143(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			ArrayList<String> BCValues = categoryLandingPage
					.getTextInBreadcrumb();
			String BCL3_category = BCValues.get(BCValues.size() - 1);

			Log.message("<b>Expected Result1:</b> By clicking on L3_category in the header Nav, It Should be naviagted to L3_categoryLanding Page");
			Log.assertThat(
					L3Category.trim().contains(BCL3_category.trim()),
					"<b>Actual Result1:</b> By clicking on L3_category in the header Nav, It is naviagted to L3_categoryLanding Page",
					"<b>Actual Result1:</b> By clicking on L3_category in the header Nav, It is not naviagted to L3_categoryLanding Page",
					driver);

			categoryLandingPage.clickOnBackButton();
			Log.message("3. Clicked On 'BackToPrevious' Refienment and naviagted to Previous Level  Category Refienment");

			ArrayList<String> BCValue = categoryLandingPage
					.getTextInBreadcrumb();
			String BCL2_category = BCValues.get(BCValue.size() - 1);

			Log.message("<b>Expected Result2:</b> By Clicking On 'BackToPrevious' Refienment, it should be naviagted to Previous Level  Category Refienment");
			Log.assertThat(
					L2Category.trim().contains(BCL2_category.trim()),
					"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is naviagted to Previous Level  Category Refienment",
					"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is not naviagted to Previous Level  Category Refienment",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_143

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Content Refinement' instead of 'Category Refinement' with the default styling.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_155(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			String[] selected_value = searchResultPage
					.selectUnselectFilterOptions("refinementSize").split("\\(");
			String selected_size = selected_value[0].trim();
			String selected_size_productCount = selected_value[1].replace(")",
					"").trim();

			int ProductCount = searchResultPage.getProductCountInPage();
			String productCount = "" + ProductCount;

			ArrayList<String> BCRefienmentValue = searchResultPage
					.getBreadCrumbLastValue();
			String BCRefinmentSizeValue = BCRefienmentValue.get(
					BCRefienmentValue.size() - 1).trim();

			Log.message("<b>Expected Result:</b> After Selecting size, the Refinement count for Size under product refinements should be same as Product Count in the SearchResult Page ");
			Log.assertThat(
					(selected_size_productCount.equals(productCount) && (selected_size
							.contains(BCRefinmentSizeValue))),
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is same as Product Count in the SearchResult Page",
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is not same as Product Count in the SearchResult Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_2B_155
	
	

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Brands under product refinements panel when a specific brand is not available for the products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_165(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<b>Expected Result:</b> After Selecting size, the Refinement count for Size under product refinements should be same as Product Count in the SearchResult Page ");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("brandRefinement"), searchResultPage),
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is same as Product Count in the SearchResult Page",
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is not same as Product Count in the SearchResult Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_165

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the full-fetched category path chosen by the customer and the current page path should not hyperlinked.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_034_2B(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String breadcrumbInMobile = "Back to Women";
		List<String> breadcrumbText = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			String selectedCatagory = searchresultPage
					.selectCatgeoryRefinement();
			Log.message("3. " + selectedCatagory
					+ "Catagory is selected under :" + searchKey);
			if (runPltfrm == "desktop") {
				String urlBeforeClickOnSelectedCatagory = driver
						.getCurrentUrl();
				Log.message("4. Got the url before clicking catagory");

				searchresultPage.clickOnCatagoryTextInBreadrcrumb();
				Log.message("5. Catagory name is clicked");

				String urlAfterClickOnSelectedCatagory = driver.getCurrentUrl();
				Log.message("6. Got the url after clicking catagory");
				Log.message("<br>");
				Log.message("<b> Expected Result: </b> System should fully fetch the category and Selected catagory ("
						+ selectedCatagory
						+ ") should not displayed as hyperlink ");
				Log.assertThat(urlBeforeClickOnSelectedCatagory
						.equals(urlAfterClickOnSelectedCatagory),
						"<b> Actual Result: </b>  Selected catagory ("
								+ selectedCatagory
								+ ") is not displayed as hyperlink",
						"<b> Actual Result: </b> Selected catagory ("
								+ selectedCatagory
								+ ") is displayed as hyperlink");

				searchresultPage.clickOnFirstSelectedCatagory();
				Log.message("7. Clicked on the serached catagory in Breadcrumb : "
						+ searchKey);
				String urlforSearchedCatagory = driver.getCurrentUrl();
				Log.message("8. Got the url after selecting the searched catagory");
				Log.message("<br>");
				Log.message("<b> Expected Result: </b> Searched catagory("
						+ searchKey + ") is not displayed as hyperlink ");
				Log.assertThat(
						!urlforSearchedCatagory
								.equals(urlAfterClickOnSelectedCatagory),
						"<b> Actual Result: </b>  Searched catagory("
								+ searchKey + ") is not displayed as hyperlink",
						"<b> Actual Result: </b> Searched catagory("
								+ searchKey + ") is displayed as hyperlink");
			} else if (runPltfrm == "mobile") {
				breadcrumbText = searchresultPage.getTextInBreadcrumb();
				Log.assertThat(
						breadcrumbText.get(0).equals(breadcrumbInMobile),
						"Searched catagory is  hyperlinked. After searching Selected catagory is not be hyperlinked.",
						"Searched catagory should be hyperlinked.After searching Selected catagory should not be hyperlinked.");

			}

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_034

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system remove the View All from the Items per page drop down, if the products is more than 90.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_083_2B(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> optionsInItems = new ArrayList<String>();
		String defaultOption = "View all";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			String valueSelect = searchresultPage
					.selectItemsPerPageDropDownByIndex(2);
			Log.message("3. Selected items per page drop down. and selected option is"
					+ valueSelect);
			searchresultPage.clickOnItemsPerPageDropDown();
			Log.message("4. Clicked on items per page drop down");
			optionsInItems = searchresultPage.getOptionValueInItemsPerPage();
			Log.message("5. Got the Option value: value are " + optionsInItems);
			boolean status = false;

			if (!optionsInItems.equals(defaultOption)) {
				status = true;
			} else {
				status = false;

			}
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> View All option should not found in drop down");
			Log.assertThat(
					status,
					"<b> Actual Result: </b> View All option is not found in drop down",
					"<b> Actual Result: </b> View All option is  found in drop down");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_083

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Article title in bold with the link and first 2 lines  of Article description", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_098_2B(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			searchresultPage.ScrollToArticle();
			Log.message("3. Scroll to the article ");
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> System should displays the Article title in bold with the link and first 2 lines of Article description.");
			Log.assertThat(
					searchresultPage.verifyArticlesTitleBold(),
					"<b> Actual Result: </b> Article title is displayed as bold",
					"<b> Actual Result: </b> Article title is not displayed as bold");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							Arrays.asList("txtDescriptionArticle"),
							searchresultPage),
					"<b> Actual Result: </b> System  displays  first 2 lines of Article description.",
					"<b> Actual Result: </b> System displays the first 2 lines of Article description.");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_083

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the state of the selected L3 category", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_137_2B(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			String selectedCatagory = searchresultPage
					.selectCatgeoryRefinement();
			Log.message("3. " + selectedCatagory
					+ "Catagory is selected under :" + searchKey);
			Log.message("<b> Expected Result: </b> The selected L3 category should be bold.");
			Log.assertThat(searchresultPage.verifyLThreeCatagoryTitleBold(),
					"<b> Actual Result: </b> Selected L3 Catagory is bold",
					"<b> Actual Result: </b> Selected L3 Catagory is not bold");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_137

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify grouping brands by letter under Brands refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_169_2B(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		String txtrefinementOptions = "brand";
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String refinementOption = "Size";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			Log.assertThat(
					searchresultPage.isRefinementPrsent(txtrefinementOptions),
					"Brand refinement present properly",
					"Brand refinement present properly");

			int refinementValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			searchresultPage.clickViewMore(txtrefinementOptions);
			Log.message("3. View more link is clicked");
			int refinementValueAfterClick = searchresultPage
					.refinementValuesCountViewMore(txtrefinementOptions);
			Log.message("<Expected Result> System should displays the first 7 letters, and then should displays a “view more” link. While clicking on the “view more” link should displays all letters. ");
			Log.assertThat(
					refinementValueAfterClick >= refinementValueBeforeClick,
					"<Actual Result>  System  displays the first 7 letters, and then should displays a “view more” link. While clicking on the “view more” link should displays all letters. ",
					"<Actual Result>  System  displays the first 7 letters, and then should displays a “view more” link. While clicking on the “view more” link should displays all letters. ");

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_124

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Articles in descending order of the most recent best matches from left to right and each articles title is  in bold with a link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_107_2B(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			searchresultPage.ScrollToArticle();
			Log.message("3. Scroll to the article ");
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> System should displays the Article title in bold with the link and first 2 lines of Article description.");
			Log.assertThat(
					searchresultPage.verifyArticlesTitleBold(),
					"<b> Actual Result: </b> Article title is displayed as bold",
					"<b> Actual Result: </b> Article title is not displayed as bold");
			Log.message("<b> Expected Result: </b> System should display the Articles in descending order");
			Log.assertThat(
					searchresultPage.verifyArticlesDecendingOrder(),
					"<b> Actual Result: </b> Articles are in descending oreder",
					"<b> Actual Result: </b> Articles are not in descending oreder");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			// driver.quit();
		}// finally

	}// TC_BELK_SEARCH_098

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the <br> tags given in Category Name in BM are not shown in Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_145_2B(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> txtCatagory = new ArrayList<String>();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			txtCatagory = searchresultPage.getTextFromCatagories();
			Log.message("3.Got the text from catagory" + txtCatagory);
			Log.message("<b> Expected Result: </b> <br> tag should not seen in Categories list");
			Log.assertThat(!txtCatagory.contains("<br>"),
					" br tag is not seen in Categories list ",
					" br tag is seen in Categories list ");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_145

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system collapse back to the configured number for that refinement, when customer click on the 'View Link'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_124_2B(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		String txtrefinementOptions = "refinementSize";
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String refinementOption = "Size";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			int refinementValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			searchresultPage.clickViewMoreLink(txtrefinementOptions);
			Log.message("3. View more link is clicked");
			searchresultPage.clickViewLessLnk(refinementOption);
			Log.message("3. View less link is clicked");
			int refinementValueAfterClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			Log.message("<b> Expected Result: </b> System should collapse back to the configured number for that refinement");
			Log.assertThat(
					refinementValueAfterClick == refinementValueBeforeClick,
					"<b> Actual Result: </b> System should collapse back to the configured number for that refinement",
					"<b> Actual Result: </b> System should collapse back to the configured number for refinement");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_124

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the default face out image and color swatch, If color specific term does not match any available variations .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_058(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// select random Swatch product
			PdpPage pdpPage = searchresultPage.selectRandomColorSwatchProduct();
			Log.message("3. selected the product which have swatch and navigated to PDP page !");
			// Get the source code of PrimaryImg
			String PrimaryImgSrc = pdpPage.getPrimaryImageColor();
			Log.message("4. Successfully got the primary Image Source Code !");
			// Get the source code of SwatchImg
			String SwatchImgSrc = pdpPage.getSelectedSwatchColorSrcCode();
			Log.message("5. Successfully got the Swatch Image Source Code !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("System should displays the default face out image and color swatch");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					PrimaryImgSrc.equals(SwatchImgSrc),
					"System displays the default face out image and color swatch !",
					"System not displayed the default face out image and color swatch !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_058

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'View All' option in the Items per page if the items number is less than 90.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_086(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// select option by index
			String selectedOption = searchresultPage
					.selectItemsPerPageDropDownByIndex(2);
			Log.message("3. selected option is '" + selectedOption + "' !");
			int productCount = searchresultPage.getProductCountInPage();
			Log.message("4. Total no of product after selection of view All is '"
					+ productCount + "' !");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("System should display the View All if the item number is less than 90");
			Log.message("(XX) = number of products within the search result, which is less than 90");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					productCount < 90,
					"System displays the count of product is less then 90 after selecting the view All(xx) !",
					"System not displays the count of product is less then 90 after selecting the view All(xx) !",
					driver);
			searchresultPage.clickOnItemsPerPageDropDown();
			ArrayList<String> OptionsInDropDown = searchresultPage
					.getOptionValueInItemsPerPage();
			String firstOption = OptionsInDropDown.get(0);
			String secondOption = OptionsInDropDown.get(1);
			String lastOption = OptionsInDropDown.get(2);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("The dropdown should display 'View 30' , 'View 60' , 'View All(XX)'");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					firstOption.contains("View 30"),
					"After clicking on dropdown 'View 30' displyed as option !",
					"After clicking on dropdown 'View 30' not displyed as option !",
					driver);
			Log.assertThat(
					secondOption.contains("View 60"),
					"After clicking on dropdown 'View 60' displyed as option !",
					"After clicking on dropdown 'View 60' not displyed as option !",
					driver);
			Log.assertThat(
					lastOption.contains("View All"),
					"After clicking on dropdown 'View All(xx)' displyed as option !",
					"After clicking on dropdown 'View All(xx)' not displyed as option !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_086

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system navigate the customer to 'View All Articles result' page, when customer click on the 'View All Articles' link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_101(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("ArticlePge");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnViewAllArticleButton();
			Log.message("3. clicked on view All Article link !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should navigate the customer to View All Articles result page .");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							element, searchresultPage),
					"System navigated the customer to View All Articles result page .",
					"System not navigated the customer to View All Articles result page .",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_101

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the recommended product for the search with the Search result Banner slot.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_108(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("SearchBanner");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnViewAllArticleButton();
			Log.message("3. clicked on view All Article link !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should display the recommended product for the keyword search and Search result Banner should get displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(element,
							searchresultPage),
					"After clicking on view all article 'Search result Banner' get displayed ! ",
					"After clicking on view all article 'Search result Banner' not get displayed ! ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_108

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion�plus/minus symbol�in the left navigation of category refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_133(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.message("* Verifying Expand/Collapse toggle in Search Listing Page");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with Keyword:'"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Listing' Page!");
			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
			}
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> It should expand or collapse the current branch of the navigation tree.");
			Log.assertThat(
					searchResultPage.VerifyCategoryToggle(),
					"<b>Actual Result 1: </b> In search Listing Page, 'Category Refinement' toggle expaned/collapsed as expected",
					"<b>Actual Result 1: </b> In search Listing Page, 'Category Refinement' toggle not expanded/collapsed as expected",
					driver);
			Log.message("</br>");
			Log.message("* Verifying Expand/Collapse toggle in Product Listing Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("3. Navigated to '" + category[0] + " > " + category[1]
					+ "' Page !");
			PlpPage plppage = new PlpPage(driver);
			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
			}
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> It should expand or collapse the current branch of the navigation tree.");
			Log.assertThat(
					plppage.VerifyCategoryToggle(),
					"<b>Actual Result 2: </b> In Product Listing Page, 'Category Refinement' toggle expaned/collapsed as expected",
					"<b>Actual Result 2: </b> In Product Listing Page, 'Category Refinement' toggle not expanded/collapsed as expected",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_133

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting the L3 category which has no L4 categories .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_138(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			String style = searchresultPage.verifyLabelStyleBold();
			Log.message("<br>");
			Log.message("<b>Expected Result : </b>");
			Log.message("The selected L3 category should be bold with its product count in parentheses and no  > symbol should displays.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(style.contains("Bold"),
					"The selected L3 category displayed with bold style !",
					"The selected L3 category not displayed with bold style !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_138

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selecting colors in Color refinement under product refinements panel for a single color", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_161(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> toogleActive = Arrays.asList("toogleActive");
		int countBeforeRefine;
		int countAfterRefine;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// Get the product count before refine
			countBeforeRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("3. Before refinement product count is :"
					+ countBeforeRefine);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			String color = searchresultPage.selectSwatchColorFromRefinment();
			Log.message("4. Filtered the products by color :" + color, driver);
			// Get the product count after refine
			countAfterRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("5. After refinement product count is :"
					+ countAfterRefine);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("The Color refinement option should displays an orange checkbox next to the item indicating that the color has been selected.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							toogleActive, searchresultPage),
					"The Color refinement option displays an orange checkbox next to the item indicating that the color has been selected !",
					"The Color refinement option not displays an orange checkbox next to the item indicating that the color has been selected !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("After selecting an available color, the product grid should refresh to show the products that match the selected color refinement.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					countBeforeRefine > countAfterRefine,
					"After selecting an available color, the product grid refreshed to show the products that match the selected color refinement !",
					"After selecting an available color, the product grid not refreshed to show the products that match the selected color refinement !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_161

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable brands in Brands refinement under product refinements panel .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_170(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// get the brand names
			ArrayList<String> BrandName = searchresultPage
					.getBrandNameInRefinement();
			Log.message("3. Got the Brand names Sucessfully !");
			List<String> tmp = new ArrayList<String>(BrandName);
			Collections.sort(tmp);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Selectable brands should be displayed as a checkbox list in alphabetical order with the brand name or brand value.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					tmp.equals(BrandName),
					"Selectable brands displayed as a checkbox list in alphabetical order with the brand name or brand value !",
					"Selectable brands not displayed as a checkbox list in alphabetical order with the brand name or brand value !",
					driver);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			int count = searchresultPage.refinementValuesCount("brand");
			searchresultPage.clickViewMoreLinkInBrandRefinement();
			int afterClickOnViewMore = searchresultPage
					.brandAfterClickViewMoreProductCount();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Brands should displays 7 value and after clicking view more count should be more than 7. ");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					count < afterClickOnViewMore,
					"Brands displays 7 value only and after clicking view more count it shows more than 7 value !",
					"Brands displays 7 value only and after clicking view more count itis not showing more than 7 value !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_170
	
	

	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_222(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			if (Utils.getRunPlatForm().equals("mobile")) {
				plpPage.clickFilterByInMobile();
				Log.message("3.Clicked on 'FilterBy' button in mobile");
			}//

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Refinement modal should be displayed when clicking on 'Filter by' button");
			Log.assertThat(
					plpPage.elementLayer.verifyPageElements(
							Arrays.asList("leftNavigationMobile"), plpPage),
					"<b>Actual Result:</b> Refinement modal is displayed when clicking on 'Filter by' button",
					"<b>Actual Result:</b> Refinement modal is not displayed when clicking on 'Filter by' button",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_222

	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_237(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			if (Utils.getRunPlatForm() == "mobile") {

				categoryLandingPage.clickFilterByInMobile();
				Log.message("3. Clicked on 'FilterBy' button");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> After tapping on 'Filter By' button, the mobile refinement modal should be displyed");

				Log.assertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								Arrays.asList("leftNavPanel"),
								categoryLandingPage),
						"<b>Actual Result:</b> After tapping on 'Filter By' button, the mobile refinement modal is displyed",
						"<b>Actual Result:</b> After tapping on 'Filter By' button, the mobile refinement modal is not displyed");

			}
			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH2B_237

	

	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_250_2B(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Filter by' text in button should be blue color");
			Log.assertThat(
					(plpPage.verifyFilterByTxtColor()),
					"<b>Actual Result 1:</b> 'Filter by' text in button is blue color",
					"<b>Actual Result 1:</b> 'Filter by' text in button is not blue color",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> 'Filter by' button border should be blue color");
			Log.assertThat(
					(plpPage.verifyFilterByBtnBorderColor()),
					"<b>Actual Result 2:</b> 'Filter by' button border is blue color",
					"<b>Actual Result 2:</b> 'Filter by' button border is not blue color",
					driver);

			if (Utils.getRunPlatForm().equals("mobile")) {
				plpPage.clickFilterByInMobile();
				Log.message("3.Clicked on 'FilterBy' button in mobile");
			}//

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Refinement modal should be displayed when clicking on 'Filter by' button");
			Log.assertThat(
					plpPage.elementLayer.verifyPageElements(
							Arrays.asList("leftNavigationMobile"), plpPage),
					"<b>Actual Result 3:</b> Refinement modal is displayed when clicking on 'Filter by' button",
					"<b>Actual Result 3:</b> Refinement modal is not displayed when clicking on 'Filter by' button",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_250_2B

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Breadcrumb only to the previous page and The last page / System will retain the state it was in when the customer left the page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_109_2B(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to Search results page");

			searchresultPage.clickViewAllArticlesButton();
			Log.message("3. Clicked on 'View all articles' button at the botton of search results page");
			// searchresultPage.getSearchTerm();

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Search Term should be displayed in articles page breadcrumb");
			Log.assertThat(
					(searchresultPage.getSearchTerm()).equals(searchKey),
					"<b>Actual Result :</b> Search Term is displayed in articles page breadcrumb",
					"<b>Actual Result :</b> Search Term is not displayed in articles page breadcrumb",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_009

	

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify color swatches for product groupings in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_188_2B(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to Search results page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Color Swatches should not be displayed for the product groupings");

			Log.assertThat(
					searchresultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstImgColorSwatches"),
									searchresultPage),
					"<b>Actual Result 1:</b> Color swatches are not displayed for the product groupings",
					"<b>Actual Result 1:</b> Color swatches are displayed for the product groupings",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Page should be redirected to PDP of the product when clickign on product name");
			PdpPage pdpPage = searchresultPage.selectProductByNameRandom();

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("drpQty"), pdpPage),
					"<b>Actual Result 2:</b> Page is redirected to PDP of the product when clicking on product name",
					"<b>Actual Result 2:</b> Page is not redirected to PDP of the product when clicking on product name",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_009

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays a maximum of 6 articles at the bottom of the product search result", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_094(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			Log.message("<b> Expected Result 1: </b> Article displayed should be less than or equal to 6");
			Log.assertThat(
					searchresultPage.verifyCountOfArticlesDisplayed(),
					"<b> Actual Result 1: </b>  Article displayed is less than or equal to 6",
					"<b> Actual Result 1: </b> Article displayed is more than 6");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH2B_094

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "System should navigate the customer to All Articles page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_104(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			searchresultPage.clickOnViewAllArticles();
			Log.message("3.Clicked on View All Articles Button");
			Log.message("<b> Expected Result 1: </b> System should Navigate customer to All Articles page");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("pageArticle"), searchresultPage),
					"<b> Actual Result 1: </b> System Navigates customer to All Articles page",
					"<b> Actual Result 1: </b> System does not Navigates customer to All Articles page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_104

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the styling of L2 category header", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_132(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category);
			CategoryLandingPage clPage = new CategoryLandingPage(driver).get();
			if (runPltfrm == "mobile") {
				clPage.clickFilterByInMobile();
			}

			Log.message("<b> Expected Result 1: </b> All the L2 category should be displayed in Blue Color Text");
			Log.assertThat(
					clPage.verifyColorIn1L2Category()
							&& clPage.verifyColorIn2L2Category()
							&& clPage.verifyColorIn3L2Category()
							&& clPage.verifyColorIn4L2Category(),
					"<b> Actual Result 1: </b>  All the L2 category is displayed in Blue Color Text",
					"<b> Actual Result 1: </b> All the L2 category is not displayed in Blue Color Text");

			Log.message("<b> Expected Result 2: </b> All the L2 category should have a toggle icon");
			Log.assertThat(
					clPage.elementLayer.verifyPageElements(
							Arrays.asList("btnToggle"), clPage),
					"<b> Actual Result 2: </b>  All the L2 category has a toggle icon",
					"<b> Actual Result 2: </b> All the L2 category does nto have a toggle icon");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_132

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting Back to Category name link below the L3 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_2B_140(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		String[] category = testData.get("CategoryLevel").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigating to Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to Product Listing Page
			CategoryLandingPage categorylandingpage = new CategoryLandingPage(
					driver);
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to Category Landing Page!");
			if (runPltfrm == "mobile") {
				categorylandingpage.clickOnFilterByOption();
				categorylandingpage.clickOnBackButton();
				Log.message("3. Clicked on 'Back to L1 category' Link");
				Log.message("</br>");
				Log.message("<b>Expected Result 1: </b> After clicking 'Back to "
						+ category[0]
						+ "' in the left navigation of Category Landing Page, The Page should be navigated to L1 categories page.");
				Log.assertThat(
						driver.getCurrentUrl().contains("beauty/")
								&& (!(driver.getCurrentUrl()
										.contains("bath-body/"))),
						"<b>Actual Result 1: </b> After clicking 'Back to "
								+ category[0]
								+ "' in the left navigation of Category Landing Page, The Page is navigated to L1 categories page.",
						"<b>Actual Result 1: </b> After clicking 'Back to "
								+ category[0]
								+ "' in the left navigation of Category Landing Page, The Page is not navigated to L1 categories page.",
						driver);
			} else {
				categorylandingpage.clickOnBackButton();
				Log.message("3. Clicked on 'Back to L1 category' Link");
				ArrayList<String> txtBreadcrumbinClp = categorylandingpage
						.getTextInBreadcrumb();
				Log.message("</br>");
				Log.message("<b>Expected Result 1: </b> After clicking 'Back to "
						+ category[0]
						+ "' in the left navigation of Category Landing Page, The Page should be navigated to L1 categories page.");
				Log.assertThat(
						txtBreadcrumbinClp.get(1).equals(category[0])
								&& driver.getCurrentUrl().contains(
										"beauty/bath-body/"),
						"<b>Actual Result 1: </b> After clicking 'Back to "
								+ category[0]
								+ "' in the left navigation of Category Landing Page, The Page is navigated to L1 categories page.",
						"<b>Actual Result 1: </b> After clicking 'Back to "
								+ category[0]
								+ "' in the left navigation of Category Landing Page, The Page is not navigated to L1 categories page.",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_2B_140

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected sizes in Size refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_153(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnSizeInRefinementMobile();
			}
			plppage.selectFirstSize();
			Log.message("3.Clicked on the first size");
			Log.message("<b> Expected Result 1: </b> The items should be displayed as per selected size");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("productSizeZero"), plppage),
					"<b> Actual Result 1: </b> The items is displayed as per selected size",
					"<b> Actual Result 1: </b> The items is not displayed as per selected size");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnSizeInRefinementMobile();
			}

			plppage.selectthirdSize();
			Log.message("<b> Expected Result 2: </b> The items should be displayed as per one of those selected size");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(Arrays.asList(
							"productSizeZero", "productSizeMiddle"), plppage),
					"<b> Actual Result 2: </b> The items is displayed as per one of those selected size",
					"<b> Actual Result 2: </b> The items is not displayed as per one of those selected size");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_153

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Not Selectable colors in Color refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_163(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("3. Navigated to PLP page (L3 Category)");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnColorInRefinementMobile();

			}
			plppage.clickonviewMoreColors();
			Log.message("<b> Expected Result 1: </b> Few color which do not have product should be disabled");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("UnSelectable"), plppage),
					"<b> Actual Result 1: </b> Few color which do not have product is disabled ",
					"<b> Actual Result 1: </b> Few color which do not have product is also enabled");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_163

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected brands in Brands refinement under product refinements panel for multiple brands", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_172(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();

			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();
			}
			plppage.selectFirstBrand();
			Log.message("3.Clicked on the first brand");
			Log.message("<b> Expected Result 1: </b> The items should be displayed as per selected Brand");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("productBrand1"), plppage),
					"<b> Actual Result 1: </b> The items is displayed as per selected Brand",
					"<b> Actual Result 1: </b> The items is not displayed as per selected Brand");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();
			}
			plppage.selectSecondBrand();
			Log.message("<b> Expected Result 2: </b> The items should be displayed as per one of those selected Brand");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("productBrand1", "productbrand2"),
							plppage),
					"<b> Actual Result 2: </b> The items is displayed as per one of those selected Brand",
					"<b> Actual Result 2: </b> The items is not displayed as per one of those selected Brand");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_172

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays selected page opens in the center allowing for easy navigation to the two previous and two next pages", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH2B_075(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResult = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			String CurrentUrl = driver.getCurrentUrl();
			PlpPage plppage = new PlpPage(driver).get();

			plppage.clickOnPage3();
			String UrlAfterClick = driver.getCurrentUrl();
			Log.message("<b> Expected Result 1: </b> The Page should be navigated to Page 3");
			Log.assertThat(
					(!CurrentUrl.equals(UrlAfterClick)),
					"<b> Actual Result 1: </b>  The Page is navigated to Page 3",
					"<b> Actual Result 1: </b> The Page is not navigated to Page 3");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH2B_075
	

}