package com.belk.testscripts.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.*;
import com.belk.pages.footers.*;
import com.belk.reusablecomponents.*;
import com.belk.support.*;

@Listeners(EmailReport.class)
public class SearchResult {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "Search";
	String BlueColor = "rgba(16, 114, 181, 1)";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check the availability of search Input box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search field should be available in the Top header of the page above the Category pane");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("txtSearch"), homePage),
					"<b>Actual Result:</b> Search field is available in the Top header of the page ",
					"<b>Actual Result:</b> Search field is not available in the Top header of the page ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_001

	@Test(groups = { "mobile" }, description = " Verify system displays a 'Filter By' button on the Mobile Search Result in SRP list view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_002(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException(
					"This testcase is not applicable for desktop.");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			searchresultPage.clickGridListToggle("List");
			Log.message("3. Clicked on List View Icon");
			Log.message("<b>Expected Result:</b> System should displays a 'Filter By' button on the Mobile Search Result.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("btnFilterByMobile"),
							searchresultPage),
					"<b>Actual Result:</b> System displays 'Filter By' button on the Mobile Search Result.",
					"<b>Actual Result:</b> System didnot display 'Filter By' button on the Mobile Search Result.",
					driver);

			searchresultPage.clickFilterByInMobile();
			Log.message("4. Clicked on 'Filter By' button in Search Result Page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> On tap of 'Filter By' button, the mobile refinements will open as a modal on top of the search results page.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("iconCloseMenu"), searchresultPage),
					"<b>Actual Result:</b> On tap of 'Filter By' button, the mobile refinements opened as a modal on top of the search results page.",
					"<b>Actual Result:</b> On tap of 'Filter By' button, the mobile refinements didnot open as a modal on top of the search results page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_002

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to Search a Store with ZIP code.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			StoreLocationPage storeLocationPage = homePage.footers
					.navigateToStore();
			Log.message("2. Navigated to 'Belk' StoreLocation Page!");

			storeLocationPage.enterZipCode("35601");
			Log.message("3. Entered Zipcode in Zipcode field !");

			storeLocationPage.clickOnSearchButton();
			Log.message("4. Clicked continue button and navigated to store content page !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Details of the store(s) for the specific ZIP code/City/State entered, should be displayed.");
			Log.assertThat(
					storeLocationPage.elementLayer.verifyPageElements(
							Arrays.asList("storeContentPage"),
							storeLocationPage),
					"<b>Actual Result:</b> Details of the store(s) for the specific ZIP code/City/State entered, displayed in StoreContentPage.",
					"<b>Actual Result:</b> Details of the store(s) for the specific ZIP code/City/State entered, not displayed in StoreContentPage.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_004

	// TC_BELK_SEARCH_005
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to search a Coupon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Page should be redirected to Coupons page");
			Log.assertThat(
					searchresultPage.elementLayer
							.verifyPageElements(
									Arrays.asList("storeContentPage"),
									searchresultPage),
					"<b>Actual Result:</b> Page is redirected to Coupons page.",
					"<b>Actual Result:</b> Page is not redirected to Coupons page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_005

	@Test(groups = { "desktop", "mobile" }, description = "Check the displays of search suggestions after entering 3 characters", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered 3 characters '" + searchKey
					+ "' in the search box");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search with 3 characters, should displayed the auto suggestion popup");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result:</b> Search auto suggestion popup is displayed for 3 characters",
					"<b>Actual Result:</b> Search auto suggestion popup is not displayed for 3 characters",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SS_007

	@Test(groups = { "desktop", "mobile" }, description = "Check the displays of search suggestions after entering less than 3 characters", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_008(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered 2 characters '" + searchKey
					+ "' in the search box");
			Log.message("<br>");

			Log.message("<b>Expected Result:</b> Search with 2 characters, should not show the auto search suggestion popup");

			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result:</b> Auto search suggestion popup is not displayed for 2 character",
					"Auto search suggestion popup getting display for 2 character",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_006

	@Test(groups = { "desktop", "mobile" }, description = "Check the displays of search suggestions after entering special character", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered keyword '" + searchKey
					+ "' in the search box");
			Log.assertThat(
					homePage.elementLayer.verifyPageListElements(
							Arrays.asList("lstProductSuggestions"), homePage),
					"3. Search auto suggestion popup is getting displayed ",
					"Search auto suggestion popup is not getting displayed",
					driver);

			homePage.clickOnProductFromProductSuggestion(1);
			Log.message("4. Clicked on 1st product link from Product suggestion panel");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By clicking the product link, it should navigate to PDP");
			Log.message(
					"<b>Actual Result:</b> On clicking any product link, it is navigated to PDP",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_008

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check the displays of search suggestions after entering special symbol", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered '" + searchKey + "' in the search box");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"3. Search suggestion is displayed.",
					"'Search suggestion' is not displayed.", driver);
			PdpPage pdpPage = homePage.clickOnProductFromSearchSuggestion(2);
			Log.message("4. Selected the first product in the search suggestion and redirected to PDP page");
			String productName = pdpPage.getProductBrandName();
			String[] splitProductName = searchKey.split("®");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product name should contain the searched keyword in the search suggestion");
			Log.assertThat((productName.toLowerCase())
					.contains(splitProductName[0].toLowerCase()),
					"<b>Actual Result:</b> The product name '" + productName
							+ "' contains the searched keyword '"
							+ splitProductName[0]
							+ "' in the search suggestion of PDP",
					"<b>Actual Result:</b> The product name '" + productName
							+ "' does not contain the searched keyword '"
							+ splitProductName[0]
							+ "' in the search suggestion of PDP", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_010

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check whether the Search Pop-up list is been disabled on changing the focus / by using the esc key", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_011(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered the keyword '" + searchKey
					+ "' in search text box");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"3. The 'Search suggestion' is displayed.",
					"The 'Search suggestion' is not displayed.", driver);
			homePage.closeSearchSuggestionPanelByEsckey();
			Log.message("4. 'ESC' Key is pressed.");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The search suggestion popup should closed when press ESC key");
			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result:</b> The 'search suggestion' is closed.",
					"<b>Actual Result:</b> The 'search suggestion' is not closed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_010

	@Test(groups = { "desktop", "mobile" }, description = "Check whether the Search Pop-up does not reappear after changing the focus using esc key even with the search term in the field..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_013(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] searchterm = searchKey.split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.enterSearchTextBox(searchterm[0]);
			Log.message("2. Entered search keyword '" + searchterm[0]
					+ "' in the search box");

			homePage.closeSearchSuggestionPanelByEsckey();
			Log.message("3. 'ESC' Key is pressed.");

			homePage.enterSearchTextBox(searchterm[1]);
			Log.message("4. Entered search keyword '" + searchterm[1]
					+ "' in the search box");
			BrowserActions.nap(5);
			Log.message("<b>Expected Result:</b> Search Pop-up pane should be displayed again, once the search term is updated.");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result:</b> Search Pop-up pane is displayed again, once the search term is updated.",
					"<b>Actual Result:</b> Search Pop-up pane is not displayed again, once the search term is updated.",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_013

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to click on a entry from 'Categories' list in Search Suggestions Pop-up screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_017(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.searchCategorySuggestion(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' in search text box");
			homePage.verifyCategorySearchSuggestion(1);
			Log.message("3. Clicked on '" + searchKey
					+ "' category from search Suggestion");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Clicking on category from search auto suggestion should be navigated to '"
					+ searchKey + "' search result page");

			SearchResultPage searchpage = new SearchResultPage(driver).get();

			Log.assertThat(searchpage != null,
					"<b>Actual Result:</b> Navigated to '" + searchKey
							+ "' search result landing page",
					"<b>Actual Result:</b> Not Navigated to '" + searchKey
							+ "' search result landing page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_013

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check the list of options displayed in the Search Suggestion pop-up screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_014(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		int maxProductCount = 10;
		String lstPhraseSuggestionHeader1 = "Looking for these brands?";
		String lstPhraseSuggestionHeader2 = "Categories";
		String lstPhraseSuggestionHeader3 = "Pages that might be interesting";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterTextInSearchfield(searchKey);
			Log.message("2. Entered the search keyword : '" + searchKey
					+ "' in search field");
			int actualProductCount = homePage.getProductCountInSuggestion();
			Log.message("<br>");
			Log.message("<u>Search suggestion panel right side verification</u>");
			Log.message("<b>Expected Result:</b>  In right side of Search suggestion panel, Product matches list should be displayed with maximum count of 10");
			Log.message("");
			Log.assertThat(
					actualProductCount <= maxProductCount,
					"<b>Actual Result1:</b> In right side of Search suggestion panel, Product matches list is displayed with maximum count of 10",
					"<b>Actual Result1:</b> In right side of Search suggestion panel, Product matches list is not displayed with maximum count of 10",
					driver);
			Log.message("<br>");
			Log.message("<u>Search suggestion panel left side verification</u>");
			Log.message("<b>Expected Result:</b> In left side of Search suggestion panel, the list should contains:Looking for this Brands list,Categories list,Pages that might be interesting' list");
			Log.message("<b>Actual Result2:</b> In left side of Search suggestion panel, the list format is displayed as below:<br>");
			Log.assertThat(homePage
					.verifySearchSuggestionsList(lstPhraseSuggestionHeader1),
					" 1. Looking for these brands list is displayed properly",
					" 1. Looking for these brands list is displayed properly",
					driver);
			Log.assertThat(homePage
					.verifySearchSuggestionsList(lstPhraseSuggestionHeader2),
					" 2. Categories list is displayed properly",
					" 2. Categories list is displayed properly", driver);
			Log.assertThat(
					homePage.verifySearchSuggestionsList(lstPhraseSuggestionHeader3),
					" 3. Pages that might be interesting list is displayed properly",
					" 3. Pages that might be interesting is displayed properly",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_011

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check the search result page on searching with a Invalid keyword", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_022(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> errorText = Arrays.asList("lblErrorText",
				"lblCorrectedSearchtxt");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' in search text box");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search result page should display the following message by navigating to relevant search results page. '0 items found for: \""
					+ searchKey + "\". Did you mean \"shoes\"?'");

			Log.message("<b>Actual Result:</b><br>");
			Log.assertThat(searchresultPage.elementLayer.verifyPageElements(
					errorText, searchresultPage),
					"  Error label is displayed on the search result page",
					"  Error label is not displayed on the search result page",
					driver);

			Log.assertThat(searchresultPage.verifyErrorMessage(searchKey),
					"  Error text '0 items found for: " + searchKey
							+ "' is displayed in search results page",
					"  Error text '0 items found for: " + searchKey
							+ "' is not displayed in search results page",
					driver);
			Log.assertThat(
					searchresultPage.verifyCorrectedSearchMessage(),
					"  Corrected Search Message 'Did you mean \"shoes\"?' is displayed in search results page",
					"  Corrected Search Message 'Did you mean \"shoes\"?' is not displayed in search results page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_017

	@Test(groups = { "desktop", "tablet" }, description = "Check the Breadcrumb of the Search result page when searched with Invalid keyword", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_024(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for '"
							+ runPltfrm + "'");
		} else {
			// ** Loading the test data from excel using the test case id */
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			String searchTextBox = "Your search results for \"" + searchKey
					+ "\"";
			List<String> breadcrumbText = new ArrayList<String>();

			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchResultPage = homePage
						.searchProductWithKeyBoard(searchKey);
				Log.message("2. Searched with invalid keyword '" + searchKey
						+ "' and navigated to " + searchKey
						+ " search result Page!");
				breadcrumbText = searchResultPage.getTextInBreadcrumb();
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> 'Your search results for '"
						+ searchKey + "'' should displayed");
				Log.assertThat(breadcrumbText.get(breadcrumbText.size() - 1)
						.equals(searchTextBox),
						"<b>Actual Result:</b> 'Your search results for \""
								+ searchKey + "\"' is displayed!",
						"<b>Actual Result:</b>  \"Your search results  \""
								+ searchKey + "\"' is not displayed!", driver);
				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_SEARCH_018

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check clicking on the Home option is Breadcrumb of the search result page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_026(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String homeLink = null;
		if (Utils.getRunPlatForm() == "mobile") {
			homeLink = "Back to Home";
		} else if (Utils.getRunPlatForm() == "desktop") {
			homeLink = "Home";
		}

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result'  Page!");

			searchresultPage.clickCategoryByNameInBreadcrumb(homeLink);
			Log.message("3. Clicked on 'Home' in breadcrumb in search results page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Clicking on 'Home' in breadcrumb, It should be navigated to home page");

			Log.assertThat(driver.getCurrentUrl().contains("home"),
					"<b>Actual Result:</b> Navigated to Home Page",
					"<b>Actual Result:</b> Not navigated to Home Page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_020

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to click on a Products link in the Products matches list", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_021(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.searchProductSuggestion(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' in search text box");

			String pdtName = homePage.getProductNameFromProductSuggestion(3);

			Log.message("3. Search Suggestion pop-up pane is displayed", driver);

			PdpPage pdpPage = homePage.clickOnProductFromProductSuggestion(3);
			Log.message("4. Clicked on the product link under product suggestions");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Clicking on Product from search suggestion should be navigated to PDP of the product");

			Log.assertThat(
					pdtName.contains(pdpPage.getProductName().replaceAll("\\n",
							" ")),
					"<b>Actual Result:</b> Navigated to PDP of clicked product '"
							+ pdtName + "'",
					"<b>Actual Result:</b> Not navigated to PDP of clicked product '"
							+ pdtName + "'", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_016

	@Test(groups = { "desktop", "tablet" }, description = "Verify system displays the Search Breadcrumb with 'Your search result for' message followed by the keyword as a link entered in the Search text box.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_029(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			String searchTextBox = "Your search results for \"" + searchKey
					+ "\"";
			List<String> breadcrumbText = new ArrayList<String>();

			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchResultPage = homePage
						.searchProductWithKeyBoard(searchKey);
				Log.message("2. Searched with keyword: '" + searchKey
						+ "' and navigated to '" + searchKey
						+ "' search result Page");
				breadcrumbText = searchResultPage.getTextInBreadcrumb();

				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b> 'Your search results for \""
						+ searchKey + "\"' should be displayed");

				Log.assertThat(
						breadcrumbText.get(breadcrumbText.size() - 1).equals(
								searchTextBox),
						"<b>Actual Result 1:</b> 'Your search results for \""
								+ searchKey
								+ "\"' is displayed in breadcrumb of search result Page",
						"<b>Actual Result 1:</b> 'Your search results for \""
								+ searchKey + "\"' is not displayed!", driver);

				searchResultPage.clickSearchTermInBreadCrumb();
				Log.message("3. Clicked on search Term '" + searchKey + "' ");

				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b> Search Term should be hyperlink");
				String urlAfterClick = driver.getCurrentUrl();
				Log.assertThat(
						(urlAfterClick
								.contains("https://development-web-belk.demandware.net/s/Belk/search/?q=shoes")),
						"<b>Actual Result 2:</b> Search Term is a hyperlink and the page gets refreshed",
						"<b>Actual Result 2:</b> Search Term is not a hyperlink and the page did not gets refreshed",
						driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				// driver.quit();
			} // finally
		}
	} // TC_BELK_SEARCH_029

	@Test(groups = { "desktop", "tablet" }, description = "Verify system refresh and displays the Search result page, when the customer clicks on the Keyword link in the Search Breadcrumb.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_030(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String searchKey = testData.get("SearchKey");

			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchResultPage = homePage
						.searchProductKeyword(searchKey);
				Log.message("2. Searched with keyword: " + searchKey
						+ " in search text box");
				Log.message("3. Navigated to '" + searchKey
						+ "' search result Page");

				String urlBeforeClicking = driver.getCurrentUrl();
				searchResultPage.clickSearchTermInBreadCrumb();
				String urlAfterClicking = driver.getCurrentUrl();
				Log.message("4. Clicked on the search term '" + searchKey
						+ "' in breadcrumb");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Page should be refreshed when clicking on the search keyword in breadcrumb");

				Log.assertThat(
						!(urlBeforeClicking.equals(urlAfterClicking)),
						"<b>Actual Result:</b> Page gets refreshed when clicking on the search keyword '"
								+ searchKey + "' in breadcrumb",
						"<b>Actual Result:</b> Page is not refreshed when clicking on the search keyword in breadcrumb",
						driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	} // TC_BELK_SEARCH_024

	@Test(groups = { "mobile" }, description = "[Mobile] Check the Breadcrumb of the Search result page when searched with Invalid keyword", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_025(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "mobile") {
			throw new SkipException(
					"This testcase(Breadcrumb verification) is applicable only for mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String catagoryname = "Back to Home";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			Log.message("<b>Expected Result:</b> The breadcrumb should be displayed as follows: <"
					+ catagoryname);
			Log.assertThat(
					searchresultPage.getTextInBreadcrumb().get(0)
							.equals(catagoryname),
					"<b>Actual Result:</b> < '"
							+ catagoryname
							+ "' is displayed properly in the breadcrumb in 'Search Result' Page",
					"<b>Actual Result:</b> < '" + catagoryname
							+ "' is not displayed properly", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_019

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system navigates the customer to Category landing page, when the customer click on the Category refinement links", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_032(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search with keyword '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result' Page!");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selecting ProductCategory from Category Refinement: "
					+ CategoryName[0]
					+ " Sub Category Refinement: "
					+ CategoryName[1]);

			String[] productFountCount = searchresultPage.getProductsFound()
					.split("\\(");
			Log.message("4. Getting Product Count From ProductFount: "
					+ productFountCount[1].replace(")", ""));

			int count = searchresultPage.getProductCount();
			String productCount = String.valueOf(count);
			Log.message("5. Fetching Product Count From SearchResultPage: "
					+ productCount);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> System should navigate to corresponding Category landing page.");

			Log.assertThat(
					productFountCount[1].replace(")", "").equals(productCount),
					"<b>Actual Result:</b> System should shown corresponding category landing page , when we click on the Category refinement link.",
					"<b>Actual Result:</b> System is not shown corresponding category landing page , when we click on the Category refinement link.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_026

	@Test(groups = { "mobile" }, description = "[Mobile] Check clicking on the Home option is Breadcrumb of the search result page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_027(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "mobile") {
			throw new SkipException(
					"This testcase(Breadcrumb verification) is applicable only for 'mobile'");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String catagoryname = "Back to Home";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			Log.assertThat(searchresultPage.getTextInBreadcrumb().get(0)
					.equals(catagoryname), "3. '" + catagoryname
					+ "' is displayed in Search Result Page", "3. '"
					+ catagoryname + "' is not displayed properly");
			searchresultPage.clickCategoryByNameInBreadcrumb(catagoryname);
			Log.message("<b>Expected Result:</b> Clicking the '" + catagoryname
					+ "' option should take back to Home page");
			Log.assertThat(driver.getCurrentUrl().contains("home"),
					"<b>Actual Result:</b> On clicking '" + catagoryname
							+ "' link, it is navigated to home Page",
					"<b>Actual Result:</b> It is not navigated to home Page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_021

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the sale products displays prices in two levels (Ex- Orig and Now) in Search Suggestions", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_028(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ArrayList<String> productDetails = homePage
					.getSalesPriceProductWithAutoSuggestion(searchKey);
			Log.message("2. Fetched Product Details from the Auto Suggestions");
			String ProductNameFromAutoSuggestion = productDetails.get(2);
			String ProductStandardPriceFromAutoSuggestion = productDetails
					.get(1).replace("$", "@").split("@")[1].split("Now")[0]
					.trim();
			String ProductSalesPriceFromAutoSuggestion = productDetails.get(0)
					.replace("$", "@").split("@")[1];

			SearchResultPage searchResultPage = new SearchResultPage(driver);
			Log.message("3. Navigated to search result Page");

			int ProductIndex = searchResultPage
					.getIndexByProductName(ProductNameFromAutoSuggestion);
			String ProductNameFromSearchResult = searchResultPage
					.getProductDetails(ProductIndex).get(0);
			String ProductStandardPriceFromSearchResult = searchResultPage
					.getProductDetails(ProductIndex).get(1);
			String ProductSalesPriceFromSearchResult = searchResultPage
					.getProductDetails(ProductIndex).get(2);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The ProductDetails having sales Price in 'Search As You Type' dialogue box should be same as the product Details in the 'SearchResult' Page");
			Log.assertThat(
					(ProductNameFromAutoSuggestion
							.contains(ProductNameFromSearchResult)
							&& (ProductStandardPriceFromAutoSuggestion
									.contains(ProductStandardPriceFromSearchResult)) && (ProductSalesPriceFromAutoSuggestion
							.contains(ProductSalesPriceFromSearchResult))),
					"<b>Actual Result:</b> The ProductDetails having sales Price in 'Search As You Type' dialogue box is same as the product Details in the 'SearchResult' Page",
					"<b>Actual Result:</b> The ProductDetails having sales Price in 'Search As You Type' dialogue box is not same as the product Details in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_028

	@Test(groups = { "mobile" }, description = "Mobile : Verify system displays the breadcrumb only for the previous page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_035(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "mobile")) {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			searchresultPage.clickOnFilterByOption();
			Log.message("3. Clicked on 'FilterBy' button in mobile");

			String[] productCategory = searchresultPage
					.selectCatgeoryRefinement().split("-");
			Log.message("4. Selecting category from Refinement: "
					+ productCategory[0]);

			String productValue = searchresultPage.getBreadCrumbProductValue()
					.replace("Back to", "");
			Log.message("5. Fetching Category from BreadCrumb: '"
					+ productValue + "'");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> System should display the breadcrumb only to the previous page. E.g Back to Previous page.");

			Log.assertThat(productCategory[0].equals(productValue.trim()),
					"<b>Actual Result:</b> The bread crumb to the previous page 'Back to "
							+ productValue + "' is displayed",
					"<b>Actual Result:</b> The bread crumb to the previous page 'Back to "
							+ productValue + "' is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_029

	@Test(groups = { "desktop", }, description = "Verify system displays the 'Refined by' search message in the Search breadcrumb. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_036(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] colorFromRefinement = searchresultPage
					.selectUnselectFilterOptions("refinementColor")
					.split("\\(");
			Log.message("3. Selected Color: " + colorFromRefinement[0]
					+ " From Refinement in SearchResultPage");

			ArrayList<String> colorsFromBreadCrumb = searchresultPage
					.getBreadCrumbLastValue();
			Log.message("4. Getting Color: " + colorsFromBreadCrumb.get(0)
					+ " from the BreadCrumb");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected Color should shown in the bread crumb");

			Log.assertThat(
					(colorFromRefinement[0].replace(" ", ""))
							.equalsIgnoreCase((colorsFromBreadCrumb.get(0))
									.replace(" ", "")),
					"<b>Actual Result:</b> selected color name: '"
							+ colorFromRefinement[0]
							+ "' is displayed in the bread crumb",
					"<b>Actual Result:</b> selected color name: '"
							+ colorFromRefinement[0]
							+ "' is not displayed in the bread crumb pls check the event log",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_030

	@Test(groups = { "desktop", }, description = "Verify system allow customer to choose the additional refinements such as 'Color, size, price' etc. and the text 'and' separates specific refinement where applicable.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_037(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] colorFromRefinement = searchresultPage
					.selectUnselectFilterOptions("refinementColor")
					.split("\\(");
			Log.message("3. Selected Color: " + colorFromRefinement[0]
					+ " From Refinement in SearchResultPage");

			ArrayList<String> colorsFromBreadCrumb = searchresultPage
					.getBreadCrumbLastValue();
			Log.message("4. Getting Color: " + colorsFromBreadCrumb.get(0)
					+ " from the Breadcrumb");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected Color should be displayed in the bread crumb");

			Log.assertThat(
					(colorFromRefinement[0].replace(" ", ""))
							.equalsIgnoreCase((colorsFromBreadCrumb.get(0))
									.replace(" ", "")),
					"<b>Actual Result:</b> selected color name: '"
							+ colorFromRefinement[0]
							+ "' is displayed in the bread crumb",
					"<b>Actual Result:</b> selected color name: '"
							+ colorFromRefinement[0]
							+ "' is not displayed in the breadcrumb pls check the event log",
					driver);

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] brandFromRefinement = searchresultPage
					.selectUnselectFilterOptions("refinementSize").split("\\(");
			Log.message("5. Selected Brand: " + brandFromRefinement[0]
					+ " from Refinement in Search Result Page");

			ArrayList<String> brandsFromBreadCrumb = searchresultPage
					.getBreadCrumbLastValue();
			Log.message("6. Getting Brand: " + brandsFromBreadCrumb.get(0)
					+ " from the Breadcrumb");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected size should be displayed in the bread crumb");

			Log.assertThat(
					(brandFromRefinement[0].replace(" ", ""))
							.equalsIgnoreCase((brandsFromBreadCrumb.get(0))
									.replace(" ", "")),
					"<b>Actual Result:</b> selected color name: '"
							+ brandFromRefinement[0]
							+ "' is displayed in the breadcrumb",
					"<b>Actual Result:</b> selected color name: '"
							+ brandFromRefinement[0]
							+ "' is not displayed in the breadcrumb pls check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally

	}// TC_BELK_SEARCH_037

	@Test(groups = { "desktop", "tablet" }, description = "Verify system displays the 'Your search results for' message include special characters if the customer has entered Special character as part of the original search term", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_031(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			String searchTextBox = "Your search results for \"" + searchKey
					+ "\"";
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				SearchResultPage searchResultPage = homePage
						.searchProductWithKeyBoard(searchKey);
				Log.message("2. Searched with keyword: " + searchKey
						+ " in search text box");
				Log.message("3. Navigated to '" + searchKey
						+ "' search result Page!");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> 'Your search results for' should have special characters should be displayed in SearchResultPage");

				Log.assertThat(
						searchResultPage.getTextInBreadcrumb().get(1)
								.contains(searchTextBox),
						"<b>Actual Result:</b> 'Your search results for: '"
								+ searchKey
								+ "' has special characters' is displayed in Search Result Page",
						"<b>Actual Result:</b> Your search results  '"
								+ searchKey
								+ "' does not have special characters", driver);
				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	} // TC_BELK_SEARCH_025

	@Test(groups = { "desktop", "tablet" }, description = "Verify once the customer choose the refinements, system refresh the page and displays the search result.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_038(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		String runPltfrm = Utils.getRunPlatForm();

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + CategoryName[0]
					+ " and Sub Category Refinement: " + CategoryName[1]
					+ " from category Refinement");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By selecting the refinement values, page content should be refresh/update");

			searchresultPage.verifySpinner("refinementColor");
			Log.message(
					"<b>Actual Result:</b> By selecting the refinement values, page content is getting refreshed/updated",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_032

	@Test(groups = { "desktop", "mobile" }, description = "Verify once the customer choose the Category refinement, system should remove all the refinements including the Searched on terms.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_033(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("   Clicked on 'FilterBy' button in mobile");
			}

			String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + CategoryName[0]
					+ " and Sub Category Refinement: " + CategoryName[1]
					+ " from category Refinement");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("   Clicked on 'FilterBy' button in mobile");
			}

			String[] color = searchresultPage.selectUnselectFilterOptions(
					"refinementColor").split("\\(");
			Log.message("4. Selected color: " + color[0]
					+ " in the refinement under color section");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("   Clicked on 'FilterBy' button in mobile");
			}

			String[] price = searchresultPage.selectUnselectFilterOptions(" ")
					.split("\\(");
			Log.message("5. Selected price: " + price[0]
					+ " in the refinement under price section");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}
			String UnSelectprice = searchresultPage
					.selectUnselectFilterOptions(" ", price[0]);
			Log.message("6. UnSelected price: " + UnSelectprice
					+ " in the refinement under color section");

			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}
			String UnSelectcolor = searchresultPage
					.selectUnselectFilterOptions("refinementColor", color[0]);
			Log.message("7. UnSelected color: " + UnSelectcolor
					+ " in the refinement under color section");

			if (Utils.getRunPlatForm() == "mobile") {

				String productValue = searchresultPage
						.getBreadCrumbProductValue().replace("Back to", "");
				Log.message("8. Fetching Category from BreadCrumb:"
						+ productValue);

				Log.message("<br>");
				Log.message("<b>Expected Result: </b>System should remove all the refinement and display only the chosen Category refinement."
						+ "Breadcrumb will display as Women -> Dress by retaining the Search keyword result.");

				Log.assertThat(
						CategoryName[1].equals(productValue.trim()),
						"<b>Actual Result: </b> Unselected refinement should be removed in the bread crumbs",
						"<b>Actual Result: </b> Unselected refinement is not  removed in the bread crumbs",
						driver);
			}
			if (Utils.getRunPlatForm() == "desktop") {

				String[] categoryL2 = CategoryName[1].split("\\(");
				Log.message("8. Selected Subcategory From Refinement: "
						+ categoryL2[0]);
				List<String> categorySelectedFromBreadcrumb = searchresultPage
						.getTextInBreadcrumb();
				Log.message("9. Fetching ProductCategory From Breadcrumb: "
						+ categorySelectedFromBreadcrumb
								.get(categorySelectedFromBreadcrumb.size() - 1));

				Log.message("<br>");
				Log.message("<b>Expected Result: </b>System should remove all the refinement and display only the chosen Category refinement."
						+ "Breadcrumb will display as Women -> Dress by retaining the Search keyword result.");

				Log.assertThat(
						categoryL2[0].replace(" ", "").equals(
								categorySelectedFromBreadcrumb
										.get(categorySelectedFromBreadcrumb
												.size() - 1).replace(" ", "")),
						"<b>Actual Result: </b> Color and size should removed from the bread crumb after unselecting Color and size in the refinements ",
						"<b>Actual Result: </b> Color and size is not remove from the bread crumb after unselecting Color and size in the refinements ",
						driver);

			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_027

	@Test(groups = { "desktop", "mobile" }, description = "Verify system wraps the search Breadcrumb, when the customer choose many number of refinement option.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_041(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] categoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + categoryName[0]
					+ " and Sub Category Refinement: " + categoryName[1]
					+ " from category Refinement");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("5. Selected color: " + color
					+ " in the refinement under color section");

			String[] selectedColorCount = color.split("\\(");
			Log.message("6. Selected color name :" + selectedColorCount[0]
					+ " and color count" + selectedColorCount[1]
					+ " in the refinement under color section");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String price = searchresultPage.selectUnselectFilterOptions(" ");
			Log.message("7. Selected price: '" + price
					+ "' in the refinement under price section");

			String[] selectedPrizeCount = price.split("\\(");
			Log.message("8. Selected prize name : '" + selectedPrizeCount[0]
					+ "' and prize count '"
					+ selectedPrizeCount[1].replace(")", "")
					+ "' in the refinement under color section");

			String[] pageCount = searchresultPage.getResultHitsText().split(
					"- ");
			Log.assertThat(
					pageCount[1].contains(selectedPrizeCount[1]
							.replace(")", "")),
					"9. '"
							+ pageCount[1]
							+ "' product should display in the Search Result Page",
					pageCount[1]
							+ " product is not display in the Search Result Page");

			if (runPltfrm == "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>'" + categoryName[1]
						+ "'should be displayed in the bread crumb");
				Log.assertThat(
						searchresultPage.getTextInBreadcrumb().get(0).trim()
								.contains(categoryName[1].trim()),
						"<b>Actual Result:</b> selected category name : '"
								+ categoryName[0]
								+ "' is displayed in the bread crumb",
						"<b>Actual Result:</b> selected category name : '"
								+ categoryName[0]
								+ "' is not displayed in the bread crumb pls check the event log");

			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Selected Category :"
						+ categoryName[1]
						+ "should be displayed in the bread crumb");
				Log.assertThat(
						searchresultPage.getTextInBreadcrumb().get(2).trim()
								.contains(categoryName[1].trim()),
						"<b>Actual Result:</b> selected category name : '"
								+ categoryName[1]
								+ "' is displayed in the bread crumb",
						"<b>Actual Result:</b> selected category name : '"
								+ categoryName[1]
								+ "' is not displayed in the bread crumb pls check the event log");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Selected Color"
						+ selectedColorCount[0]
						+ "should be displayed in the bread crumb");
				Log.assertThat(
						searchresultPage.getBreadCrumbLastValue().get(1).trim()
								.contains(selectedColorCount[0].trim()),
						"<b>Actual Result:</b> selected color name : '"
								+ selectedColorCount[0]
								+ "' is displayed in the bread crumb",
						"<b>Actual Result:</b> selected color name : '"
								+ selectedColorCount[0]
								+ "' is not displayed in the bread crumb pls check the event log");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Selected Price name :"
						+ selectedColorCount[0]
						+ "should be displayed in the bread crumb");
				Log.assertThat(
						searchresultPage.getBreadCrumbLastValue().get(0).trim()
								.contains(selectedPrizeCount[0].trim()),
						"<b>Actual Result:</b> selected price name : '"
								+ selectedPrizeCount[0]
								+ "' is displayed in the bread crumb",
						"<b>Actual Result:</b> selected price name : '"
								+ selectedPrizeCount[0]
								+ "' is not displayed in the bread crumb pls check the event log",
						driver);

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_035

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Sort by' message in the Search Breadcrumb prior to user selecting any values from the dropdown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] categoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + categoryName[0]
					+ " and Sub Category Refinement: " + categoryName[1]
					+ " from category Refinement");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Sort by' should not be displayed in breadcrumb");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("txtRefinedBy"), searchresultPage),
					"<b>Actual Result:</b> 'Sort by' is not displayed in breadcrumb.",
					"<b>Actual Result:</b> 'Sort by' is displayed in breadcrumb",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch

		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally

	}// TC_BELK_SEARCH_042

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Sort by' message in the Search Breadcrumb, when customer choose any of the Sort option from the 'Sort'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_043(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] categoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + categoryName[0]
					+ " and Sub Category Refinement: " + categoryName[1]
					+ " from category Refinement");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("5. Selected color: " + color
					+ " in the refinement under color section");

			String[] selectedColorCount = color.split("\\(");
			Log.message("6. Selected color name :" + selectedColorCount[0]
					+ " and color count(" + selectedColorCount[1]
					+ " in the refinement under color section");

			String selectedSortBy = searchresultPage.selectSortByRandom();
			Log.message("7. Selected sortby: " + selectedSortBy
					+ " in the Sory by drop down");

			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Selected SortBy Name: "
						+ selectedSortBy
						+ " should be displayed in the bread crumb");

				Log.assertThat(
						searchresultPage.getTextFromEntrieBreadCrumb().get(2)
								.contains(selectedSortBy.trim()),
						"<b>Actual Result:</b> Selected sort by  name : "
								+ selectedSortBy
								+ " is displayed in the bread crumb",
						"<b>Actual Result:</b> Selected sort by  name : "
								+ selectedSortBy
								+ " is not displayed in the bread crumb pls check the event log",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch

		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_043

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system maintain the Sort settings through the current search session until the user removes refinements or enters a new search.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_044(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			String[] selectedColorCount = color.split("\\(");
			Log.message("3. Selected color name :" + selectedColorCount[0]
					+ " and color count(" + selectedColorCount[1]
					+ " in the refinement under color section");
			String selectedSortBy = searchresultPage.selectSortByRandom();
			Log.message("4. Selected sortby: " + selectedSortBy
					+ " in the Sory by drop down");
			String selectedValue = searchresultPage.getSelectedSortByValue();
			searchresultPage.selectProduct();
			Log.message("5. Selected the product and Navigated to PdpPage!");
			BrowserActions.navigateToBack(driver);
			Log.message("6. Navigated back to searchresultPage!");

			String selectedValue1 = searchresultPage.getSelectedSortByValue();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("system should maintain the Sort settings through the current search session until the user removes refinements or enters a new search.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					selectedValue.equals(selectedValue1),
					"system maintained the Sort settings through the current search session until the user removes refinements or enters a new search.",
					"system not maintained the Sort settings through the current search session until the user removes refinements or enters a new search.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch

		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_044

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to click on a entry from 'Pages that might be interesting list' in Search Suggestions Pop-up screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_018(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String contentheading = "shoes";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Pagecontent searchresultPagecontent = homePage
					.searchWithAutoSuggestionPageContent(searchKey);

			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			Log.message("<b> Expected Result: </b> Clicking on the link should take me to 'Shoes' Content page");
			String content = searchresultPagecontent.getPageContent();
			Log.assertThat(
					content.equals(contentheading),
					"<b> Actual Result: </b> Clicking on the link it navigated to 'Shoes' Content page !",
					"<b> Actual Result: </b> Clicking on the link it's not navigated to 'Shoes' Content page !",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_018

	/*
	 * @Test(groups = { "desktop" }, description =
	 * "Verify once the customer choose the refinements, system refresh the page and displays the search result"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_038(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String searchKey = testData.get("SearchKey"); String runPltfrm =
	 * Utils.getRunPlatForm(); // Get the web driver instance final WebDriver
	 * driver = WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * 
	 * SearchResultPage searchresultPage = homePage
	 * .searchProductKeyword(searchKey); Log.message("2. Search '" + searchKey +
	 * "' in the home page and  Navigated to 'Search Result'  Page!");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
	 * .split("-"); Log.message("3. Selected category: " + CategoryName[0] +
	 * " and Sub Category Refinement: " + CategoryName[1] +
	 * " from category Refinement");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String color = searchresultPage
	 * .selectUnselectFilterOptions("refinementColor");
	 * Log.message("4. Selected color: " + color +
	 * " in the refinement under color section");
	 * 
	 * String[] selectedColorCount = color.split("\\(");
	 * Log.message("5. Selected color name:  " + selectedColorCount[0] +
	 * " and color count" + selectedColorCount[1] +
	 * " in the refinement under color section");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String size = searchresultPage.selectUnselectFilterOptions("size");
	 * Log.message("6. Selected size: " + size +
	 * " in the refinement under price section");
	 * 
	 * String[] selectedSizeCount = size.split("\\(");
	 * Log.message("7. Selected size name: " + selectedSizeCount[0] +
	 * " and size count" + selectedSizeCount[1] +
	 * " in the refinement under size section");
	 * 
	 * if (runPltfrm != "mobile") {
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> Selected color & size should be displayed in the breadcrumbs"
	 * );
	 * 
	 * Log.assertThat( searchresultPage.getBreadCrumbRefinement().contains(
	 * selectedColorCount[0].trim() + " and"),
	 * "<b>Actual Result:</b> selected color name: '" + selectedColorCount[0] +
	 * " is displayed in the bread crumb", ".selected color name : " +
	 * selectedColorCount[0] +
	 * " is not displayed in the bread crumb pls check the event log", driver);
	 * } Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally
	 * 
	 * }// TC_BELK_SEARCH_031
	 */

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check the Clearance price products in the Search suggestion list", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_020(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String contentheading = "shoes";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Pagecontent searchresultPagecontent = homePage
					.searchWithAutoSuggestionPageContent(searchKey);

			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			Log.message("<b> Expected Result: </b> Clicking on the link should take me to 'Shoes' Content page");
			String content = searchresultPagecontent.getPageContent();
			Log.assertThat(
					content.equals(contentheading),
					"<b> Actual Result: </b> Clicking on the link it navigated to 'Shoes' Content page !",
					"<b> Actual Result: </b> Clicking on the link it's not navigated to 'Shoes' Content page !",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_020

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system maintain the Refinement settings through the current search session until the user removes refinements or enters a new search", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_039(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] categoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + categoryName[0]
					+ " and Sub Category Refinement: " + categoryName[1]
					+ " from category Refinement");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("5. Selected color: " + color
					+ " in the refinement under color section");

			String[] selectedColorCount = color.split("\\(");
			Log.message("6. Selected color name: " + selectedColorCount[0]
					+ " and color count(" + selectedColorCount[1]
					+ " in the refinement under color section");

			String selectedSortBy = searchresultPage.selectSortByRandom();
			Log.message("7. Selected soryby: " + selectedSortBy
					+ " in the Sory by drop down");

			homePage.searchProductKeyword(searchKey);
			Log.message("8. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Selected SortBy Name:"
						+ selectedSortBy
						+ "should not be displayed in the bread crumb");

				Log.assertThat(
						!searchresultPage.getTextFromEntrieBreadCrumb().get(0)
								.contains(selectedSortBy.trim()),
						"<b>Actual Result:</b> Selected sort by name: "
								+ selectedSortBy
								+ " is not displayed in the bread crumb",
						"<b>Actual Result:</b> Selected sort by name: "
								+ selectedSortBy
								+ " is displayed in the bread crumb pls check the event log",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch

		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_039

	@Test(groups = { "desktop", "mobile", "tablet" }, description = " Verify once the customer click on the 'X' icon, system refresh the page and displays the updated search result page with the updated search Breadcrumb", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_040(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("3. Selected category: " + CategoryName[0]
					+ " and Sub Category Refinement: " + CategoryName[1]
					+ " from category Refinement");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] selectedColor = searchresultPage
					.selectUnselectFilterOptions("refinementColor")
					.split("\\(");
			Log.message("4. Selected color: " + selectedColor[0]
					+ " in the refinement under color section");

			String selectedCountColor = selectedColor[1].replace(")", "");
			Log.message("5. Products After selected:" + selectedColor[0]
					+ " in SearchResultPage is:" + selectedCountColor);

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String[] price = searchresultPage.selectUnselectFilterOptions(" ")
					.split("\\(");
			Log.message("6. Selected price: " + price[0]
					+ " in the refinement under price section");

			if (runPltfrm != "mobile") {
				searchresultPage.ClickXToRemoveRefinement();
				Log.message("7. Removing the Refinement By Clicking On 'X'");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> By Removing the refinement value in the bread crumb, bread crumb content and page should be update/refresh");

				Log.assertThat(
						(searchresultPage.getProductCount()) == (Integer
								.parseInt(selectedCountColor)),
						"<b>Actual Result:</b> Page Content and product count is updated after removing the refinement in the bread crumb",
						"<b>Actual Result:</b> Page Content and product count  is not updated after removing the refinement in the bread crumb",
						driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_034

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays Search Result warning message, if the keyword search results are not refined by a category", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_046(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> errorText = Arrays.asList("lblErrorText",
				"lblCorrectedSearchtxt");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' in search text box");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search result page should display the following message by navigating to relevant search results page. '0 items found for: \""
					+ searchKey + "\". Did you mean \"shoes\"?'");

			Log.message("<b>Actual Result:</b><br>");
			Log.assertThat(searchresultPage.elementLayer.verifyPageElements(
					errorText, searchresultPage),
					"  Error label is displayed on the search result page",
					"  Error label is not displayed on the search result page",
					driver);

			Log.assertThat(searchresultPage.verifyErrorMessage(searchKey),
					"  Error text '0 items found for: " + searchKey
							+ "' is displayed in search results page",
					"  Error text '0 items found for: " + searchKey
							+ "' is not displayed in search results page",
					driver);
			/*
			 * Log.assertThat( searchresultPage.verifyCorrectedSearchMessage(),
			 * "  Corrected Search Message 'Did you mean \"shoes\"?' is displayed in search results page"
			 * ,
			 * "  Corrected Search Message 'Did you mean \"shoes\"?' is not displayed in search results page"
			 * , driver);
			 */

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_046

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the product grid by default to 30 product tiles (10 X 3) per page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_047(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			String currentViewType = searchResultPage.getCurrentViewType();
			if (currentViewType != "grid") {
				searchResultPage.clickGridListToggle("Grid");
			}
			Log.message("3. Clicking 'Grid' toggle in the search results page");
			int productCount = searchResultPage.getProductCount();
			if (productCount < 30) {
				throw new Exception(
						"The product count is less than 30. So unable to test this case. Please pass the searchkey which contain more than 30 products.");
			}
			if (runPltfrm == "desktop") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The system should displays the product grid by default to 30 product tiles per page.");

				if (productCount < 3)
					Log.message(
							"<b>Actual Result:</b> The system is displayed 30 product tiles per page (10 x 3))",
							driver);
				else {
					int xLocationOfFirstProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(1);
					int xLocationOfThirdProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(3);
					Log.assertThat(
							xLocationOfFirstProduct != xLocationOfThirdProduct,
							"<b>Actual Result:</b> The system is displayed 30 product tiles per page (10 x 3))",
							"<b>Actual Result:</b> The system is not displayed 30 product tiles per page (10 x 3))",
							driver);
				}
			} else if (runPltfrm == "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The system should displays the product grid by default to 30 product tiles per page.");
				if (productCount < 3)
					Log.message(
							"<b>Actual Result:</b> The system is displayed 30 product tiles per page (15 x 2))",
							driver);
				else {
					int xLocationOfFirstProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(1);
					int xLocationOfThirdProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(3);
					Log.assertThat(
							xLocationOfFirstProduct == xLocationOfThirdProduct,
							"<b>Actual Result:</b> The system is displayed 30 product tiles per page (15 x 2))",
							"<b>Actual Result:</b> The system is not displayed 30 product tiles per page (15 x 2)",
							driver);
				}
			}

			Log.testCaseResult();
		}

		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_040

	@Test(groups = { "desktop" }, description = "Verify system displays the results in rows of 3 tiles, If there are less than 30 products in a category", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_048(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase (product tiles should be 3 per row) is not applicable for mobile");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search with keyword '" + searchKey
					+ "' and navigated to search result page");
			String currentViewType = searchResultPage.getCurrentViewType();
			if (currentViewType != "grid") {
				searchResultPage.clickGridListToggle("Grid");
			}
			Log.message("3. Clicking 'Grid' toggle in the search results page");
			int productCount = searchResultPage.getProductCount();
			if (productCount > 30) {
				throw new Exception(
						"The product count is more than 30. So unable to test this case. Please pass the searchkey which contain less than 30 products.");
			}
			if (runPltfrm == "desktop") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The system should display the product grid by default to 3 product tiles per row.");

				if (productCount < 3)
					Log.message(
							"<b>Actual Result:</b> The system is displayed 3 product tiles per row",
							driver);
				else {
					int xLocationOfFirstProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(1);
					int xLocationOfThirdProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(3);
					Log.assertThat(
							xLocationOfFirstProduct != xLocationOfThirdProduct,
							"<b>Actual Result:</b> The system is displayed 3 product tiles per row",
							"<b>Actual Result:</b> The system is not displayed 3 product tiles per row",
							driver);
				}
			}
			Log.testCaseResult();
		}

		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_041

	@Test(groups = { "desktop", "tablet" }, description = "Verify that the products with price > 0.00", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entering keyword '" + searchKey
					+ "' in the search text box");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Price should be display greater than zero in the product Suggestion");

			Log.assertThat(
					homePage.verifyPriceWithGreaterThenZero(),
					"<b>Actual Result:</b> By searching the product prize value getting display with Greater then zero",
					"<b>Actual Result:</b> By searching the product prize value is not  display with Greater then zero'",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_042

	@Test(groups = { "desktop", "mobile" }, description = "Verify system allows the user to view the number of products per page based upon on the selection from the Product view drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_051(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		int productCount;

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("drpItemsPerPage"), searchResultPage),
					"3. View drop down is displayed in Search Result Page",
					"3. View drop down is not displayed in Search Result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Selecting 'View' drop down value, the product count should be updated");

			String itemCountText = searchResultPage.getResultHitsText();
			String[] splitTotalItemCountText = itemCountText.split("of ");
			String totalItemCount = splitTotalItemCountText[1].replace(",", "");

			if (Integer.parseInt(totalItemCount) < 90) {

				searchResultPage.selectItemsPerPageDropDownByText("60");
				productCount = searchResultPage.getProductCount();
				Log.assertThat(
						productCount == 60,
						"<b>Actual Result:</b> 'View 60' is clicked and 60 products are displayed.",
						"'View 60' is clicked but 60 products are not displayed.",
						driver);
				searchResultPage.selectItemsPerPageDropDownByText("30");
				productCount = searchResultPage.getProductCount();
				Log.assertThat(
						productCount == 30,
						"<b>Actual Result:</b> 'View 30' is clicked and 30 products are displayed.",
						"'View 30' is clicked but 30 products are not displayed.",
						driver);
			} else {
				searchResultPage.selectItemsPerPageDropDownByText("60");
				productCount = searchResultPage.getProductCount();
				Log.assertThat(
						productCount == 60,
						"<b>Actual Result:</b> 'View 60' is clicked and 60 products are displayed.",
						"'View 60' is clicked but 60 products are not displayed.",
						driver);
				searchResultPage.selectItemsPerPageDropDownByText("30");
				productCount = searchResultPage.getProductCount();
				Log.assertThat(
						productCount == 30,
						"<b>Actual Result:</b> 'View 30' is clicked and 30 products are displayed.",
						"'View 30' is clicked but 30 products are not displayed.",
						driver);
				searchResultPage.selectItemsPerPageDropDownByText("90");
				productCount = searchResultPage.getProductCount();
				Log.assertThat(
						productCount == 90,
						"<b>Actual Result:</b> 'View 90' is clicked and 90 products are displayed.",
						"'View 90' is clicked but 90 products are not displayed.",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_051

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the products in list view, if the customer click on the List icon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_052(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> listView = Arrays.asList("listSearchResult");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search with keyword '" + searchKey
					+ "' and navigated to search result page");

			searchResultPage.clickGridListToggle("List");
			Log.message("3. Clicked 'List' view toggle in the search result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By clicking 'List' toggle, products are should be display in 'List' view");

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(listView,
							searchResultPage),
					"<b>Actual Result:</b> By clicking 'List' toggle, products are displayed in 'List' view",
					"By clicking 'List' toggle, products are not displayed in 'List' view",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_052

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify once the customer click on the List icon, system displays a single column of 30 product rows", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_053(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			int productCount = searchResultPage.getProductCount();
			if (productCount == 0)
				throw new Exception(
						"The product count is 0. so unable to execute this testcase.");
			else {
				String currentViewType = searchResultPage.getCurrentViewType();
				if (currentViewType != "wide") {
					searchResultPage.clickGridListToggle("List");
					Log.message("3. Clicked on 'List' toggle");
				}
				int xLocationOfFirstProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(1);
				int xLocationOfSecondProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(2);

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The products should be displayed in List view");

				Log.assertThat(
						xLocationOfFirstProduct == xLocationOfSecondProduct,
						"<b>Actual Result:</b> The products are displayed in List view.",
						"The products are not displayed in List view.", driver);

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_053

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the products in grid view, if the customer click on the grid icon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_054(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			int productCount = searchResultPage.getProductCount();
			if (productCount == 0)
				throw new Exception(
						"The product count is 0. so unable to execute this testcase.");
			else {
				String currentViewType = searchResultPage.getCurrentViewType();
				if (currentViewType != "wide") {
					searchResultPage.clickGridListToggle("List");
				}
				Log.message("3. The 'List' view is clicked");
				Log.message("<br>");
				Log.message("<b>Expected Result-1:</b> The products should displayed in List view.");

				int xLocationOfFirstProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(1);
				int xLocationOfSecondProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(2);
				Log.softAssertThat(
						xLocationOfFirstProduct == xLocationOfSecondProduct,
						"<b>Actual Result-1:</b> The products are displayed in List view.",
						"<b>Actual Result-1:</b> The products are not displayed in List view.",
						driver);
				searchResultPage.clickGridListToggle("Grid");
				Log.message("<br>");
				Log.message("4. The 'Grid' view is clicked");
				xLocationOfFirstProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(1);
				xLocationOfSecondProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(2);
				Log.message("<br>");
				Log.message("<b>Expected Result-2:</b> The products should displayed in Grid view.");
				Log.softAssertThat(
						xLocationOfFirstProduct != xLocationOfSecondProduct,
						"<b>Actual Result-2:</b> The products are displayed in Grid view.",
						"<b>Actual Result-2:</b> The products are not displayed in Grid view.",
						driver);

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_054

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the List view through the remainder of the customer’s entire session if the default is overridden, unless/until customer selects the Grid View Option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_055(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");

			int productCount = searchResultPage.getProductCount();
			if (productCount == 0)
				throw new Exception(
						"The product count is 0. so unable to execute this testcase.");
			else {
				String currentViewType = searchResultPage.getCurrentViewType();
				if (currentViewType != "wide") {
					searchResultPage.clickGridListToggle("List");

					searchResultPage.navigateToPagination("2");
					Log.message("3. click on 'Page2' in the page naivgation");

					int xLocationOfFirstProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(1);
					int xLocationOfSecondProduct = searchResultPage
							.getCurrentLocationOfPdtByIndex(2);
					Log.message("<br>");
					Log.message("<b>Expected Result: </b>System should display the products in List view throughout the entire session of the customer in the respective pages.");
					Log.assertThat(
							xLocationOfFirstProduct == xLocationOfSecondProduct,
							"<b>Actual Result:</b> The products are displayed in List view.",
							"<b>Actual Result:</b> The products are not displayed in List view.",
							driver);
				}

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_055

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the default Sort option as 'Best Seller' and also have other options as 'Most Recent', 'Top Rated', 'Recent' in the sort drop down", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_056(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		SearchResultPage searchResultPage;
		String DefaultSelection;
		String actualDefaultSelection;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Navigated to 'Belk' Search result Page!");
			// Select the product according to search keyword
			Log.message("<b>Expected Result:</b> Verify system displays the default Sort option as Featured and also have other options as Most Recent, Top Rated, Recent  in the sort drop down");
			DefaultSelection = searchResultPage.getDefaultSelection();
			actualDefaultSelection = "Featured";
			Log.softAssertThat(
					DefaultSelection.equals(actualDefaultSelection),
					"<b>Actual Result:</b> It displays that default Sort option as featured",
					"<b>Actual Result:</b> It not displays that default Sort option as featured");
			ArrayList<String> mylist = searchResultPage.getSortByList();
			ArrayList<String> expected_sortBylist = new ArrayList<String>();
			expected_sortBylist.add("Best Sellers");
			expected_sortBylist.add("Most Recent");
			expected_sortBylist.add("Top Rated");
			expected_sortBylist.add("Best Matches");

			for (String sortByValue : mylist) {
				Log.assertThat(
						expected_sortBylist.contains(sortByValue),
						"<b>Actual Result:</b> Sort drop down have '"
								+ sortByValue + "'",
						"<b>Actual Result:</b>"
								+ sortByValue
								+ " is not displayed in the 'Sort By' drop down",
						driver);
			}
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_056

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the face out image and color swatch, when the customer search for any color specific search.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_057(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> colorSwatch = Arrays.asList("lstImgColorSwatches");
		List<String> PyImage = Arrays.asList("lstImgPrimary");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("System should displays the face out image and color swatch.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							colorSwatch, searchresultPage),
					"System displays the color swatch for entered keyword!",
					"System not displayed the color swatch for entered keyword!",
					driver);
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							PyImage, searchresultPage),
					"System displays the primary Image face out for entered keyword!",
					"System not displayed the primary Image face out for entered keyword!",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_057

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Back to top' link appear at the bottom of every page of the product grid below pagination and when customer click on the link navigate to Top of the page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_061(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> backtoTop = Arrays.asList("txtBacktoTop");

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// / ----- On PLP

			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated to " + l1category + " >>" + l2category
					+ " >>" + l3category);

			PlpPage plpPage = new PlpPage(driver).get();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Back to Top' link should be displayed in product listing page");

			Log.assertThat(
					plpPage.elementLayer.verifyPageElements(backtoTop, plpPage),
					"<b>Actual Result:</b> 'Back To Top' link is displayed in product listing page",
					"<b>Actual Result:</b> 'Back To Top' link is not displayed in product listing page)",
					driver);

			plpPage.clickBackToTop();

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			Long backToTop = (Long) executor
					.executeScript("return window.pageYOffset");
			Log.message("<br>");

			Log.message("<b>Expected Result:</b> Clicking on 'Back to Top' link should scroll up the page");

			String backToTopPLP = Long.toString(backToTop);

			Log.assertThat(
					backToTopPLP.equals("0"),
					"<b>Actual Result:</b> Page is scrolled up when clicking on 'Back to Top' link in PLP",
					"<b>Actual Result:</b> Page is not scrolled up when clicking on 'Back to Top' link in PLP",
					driver);

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("<br>");
			Log.message("3. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Back to Top' link should be displayed in Search Result page");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(backtoTop,
							searchresultPage),
					"<b>Actual Result:</b> 'Back To Top' link is displayed in search results page",
					"<b>Actual Result:</b> 'Back To Top' link is not displayed in search results page)",
					driver);

			searchresultPage.clickBackToTop();

			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			Long backToTopSRP = (Long) executor1
					.executeScript("return window.pageYOffset");

			String backToTopSearch = Long.toString(backToTopSRP);
			Log.message("<br>");

			Log.message("<b>Expected Result:</b> Clicking on 'Back to Top' link should scroll up the page");

			Log.assertThat(
					backToTopSearch.equals("0"),
					"<b>Actual Result:</b> Page is scrolled up when clicking on 'Back to Top' link in search results page",
					"<b>Actual Result:</b> Page is not scrolled up when clicking on 'Back to Top' link in search results page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_053

	@Test(groups = { "mobile" }, description = "Mobile : Verify system displays the breadcrumb only to the previous page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_062(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "mobile")) {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and Navigated to 'Search Result' Page!");

			if (runPltfrm == "mobile") {
				searchresultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			String selectCatgeoryRefinement = searchresultPage
					.selectCatgeoryRefinement();
			Log.message("3. Selecting '" + selectCatgeoryRefinement
					+ "' in the refinement under category section");

			String[] selectedCategory = selectCatgeoryRefinement.split("\\(");
			Log.message("4. Selected category and sub category Name : "
					+ selectedCategory[0]
					+ "and selected sub category count :("
					+ selectedCategory[1]
					+ " in the refinement under category section");

			String[] categoryname = selectedCategory[0].split("-");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected Category name should be displayed in the breadcrumbs");
			Log.assertThat(
					searchresultPage.getTextInBreadcrumb().get(0).trim()
							.contains(categoryname[0].trim()),
					"<b>Actual Result:</b> Selected category name: "
							+ categoryname[0]
							+ " is displayed in the bread crumb",
					"<b>Actual Result:</b> Selected category name: "
							+ categoryname[0]
							+ " is not displayed in the bread crumb pls check the event log",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch

		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_054

	@Test(groups = { "mobile" }, description = "Mobile : Verify system displays a 'Filter By' button on the Mobile Search Result.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_063(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop") {
			throw new SkipException(
					"This testcase(FilterBy button) is not applicable for '"
							+ runPltfrm + "'");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched keyword: '" + searchKey
					+ "' and navigated to " + searchKey
					+ " search result Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Filter By' button should be displayed in Search Result Page");

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("btnFilterByMobile"),
							searchResultPage),
					"<b>Actual Result:</b> 'Filter By' button is displayed in Search Result Page",
					"<b>Actual Result:</b> 'Filter by' button is not displayed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_055

	@Test(groups = { "mobile" }, description = "Verify Overlay get open on top of the Search result, when the customer tap on the 'Filter by'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_064(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException(
					"This testcase is not applicable for desktop.");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			searchresultPage.clickFilterByInMobile();
			Log.message("3. Clicked on 'Filter By' button in Search Result Page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Overlay should get open on top of the Search result");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("iconCloseMenu"), searchresultPage),
					"<b>Actual Result:</b> Overlay is displayed above search result",
					"<b>Actual Result:</b> Overlay is not displayed above search result",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_Search_056

	@Test(groups = { "desktop", "tablet" }, description = "Verify system allow the user to view the number of products per page based upon on the selection from the Product view drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_065(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("drpItemsPerPage"), searchResultPage),
					"3. View drop down is displayed in Search Result Page",
					"3. View drop down is not displayed in Search Result Page",
					driver);

			String viewCount = searchResultPage
					.selectItemsPerPageDropDownByIndex(1);
			Log.message("4. Selected index value : 1 in items per page dropdown");
			int productCount = searchResultPage.getProductCount();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By selecting the Value from the view drop down; Product count should shown in the Search Result Page");
			Log.assertThat(viewCount.contains(String.valueOf(productCount)),
					"<b>Actual Result:</b> '" + viewCount
							+ "' is clicked and '" + viewCount
							+ "' products is displayed.",
					"<b>Actual Result:</b> '" + viewCount
							+ "' is clicked but '" + viewCount
							+ "' products is not displayed.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_057

	@Test(groups = { "desktop", "tablet" }, description = "Verify - Sort by value is displayed in breadcrumb.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_069(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile")
			throw new SkipException(
					"This test case is not applicable for mobile");
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> sortbyBreadcrumb = Arrays.asList("lblSortBy", "lblSorted");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected sort by value should be displayed in breadcrumb");
			String selectingValue = searchResultPage
					.selectSortByDropDownByIndex(3);
			String selectedValue = searchResultPage
					.getSortByValueInBreadcrumb();
			Log.assertThat(
					(selectingValue.equals(selectedValue))
							&& searchResultPage.elementLayer
									.verifyPageElements(sortbyBreadcrumb,
											searchResultPage),
					"<b>Actual Result:</b> Selected sort by value: '"
							+ selectedValue + "' is displayed in breadcrumb",
					"<b>Actual Result:</b> Selected sort by value: '"
							+ selectedValue
							+ "' is not displayed in breadcrumb", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_061

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the values displayed in Sort drop down", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_070(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> sortby = Arrays.asList("drpSortBy");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.assertThat(searchResultPage.elementLayer.verifyPageElements(
					sortby, searchResultPage),
					"3. The Top and Bottom 'sortby' dropdown is displayed!",
					"The sortby dropdown is not displayed!");
			ArrayList<String> lstValues = searchResultPage
					.getSortByOptionsList();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Sort by dropdown values should be displayed");

			Log.assertThat(
					(lstValues.get(0).equals("Best Sellers")
							&& lstValues.get(1).equals("Most Recent") && lstValues
							.get(2).equals("Top Rated")),
					"<b>Actual Result:</b> Sort by dropdown values [Best Sellers, Most Recent, Top Rated] is displayed",
					"<b>Actual Result:</b> Sort by dropdown values [Best Sellers, Most Recent, Top Rated] is not displayed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_062

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the system refresh and displays the Search result for the respective sort once the customer select the option from Sort drop down", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_071(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> sortby = Arrays.asList("drpSortBy", "drpSortbyBottom");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.assertThat(searchResultPage.elementLayer.verifyPageElements(
					sortby, searchResultPage),
					"3. The Top and Bottom 'sortby' dropdown is displayed!",
					"The sortby dropdown is not displayed!");
			ArrayList<String> lstValues = searchResultPage
					.getSortByOptionsList();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Page should refresh and display the Search Result page when select an option from sort dropdown.");
			for (int i = 1; i <= lstValues.size(); i++) {
				searchResultPage.selectSortByDropDownByIndex(i);
				Log.message("  Selected Sort By dropdown value by index: '" + i
						+ "'");
				searchResultPage.waitUntilSearchSpinnerDisappear();
				String currentUrl = driver.getCurrentUrl();
				String[] urlSplit = currentUrl.split("&srule=");
				String[] viewSizeInUrlsplit1 = urlSplit[1].split("&pmin=");
				String sortbyOption = viewSizeInUrlsplit1[0];
				String selectedValue = searchResultPage
						.getSelectedSortByValue();

				Log.assertThat(
						sortbyOption.replace("-", " ").equals(selectedValue),
						"<b>Actual Result-"
								+ i
								+ ":</b> Page is refreshed on selecting Sort By Option '"
								+ selectedValue
								+ "' and navigated to Search Result Page",
						"<b>Actual Result-"
								+ i
								+ ":</b> Page is not refreshed on selecting Sort By Option '"
								+ selectedValue
								+ "' and not navigated to Search Result Page",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_071

	@Test(groups = { "desktop", "tablet" }, description = "Verify system remove the sort option and return to the default search result if a new search occurs", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_072(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for mobile.");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			searchResultPage.selectSortByDropDownByIndex(1);
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("txtRefinedBy"), searchResultPage),
					"3. The 'SortBy' breadcrumb is displayed.",
					"The 'SortBy' breadcrumb is not displayed.");
			searchResultPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Again search the product " + searchKey
					+ " and navigated to Search Result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The selected sort option should be changed to default option when search a product again");

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("txtRefinedBy"), searchResultPage),
					"<b>Actual Result:</b> The default sort option is displayed and the SortBy breadcrumb is not displayed.",
					"<b>Actual Result:</b> The default sort option is not displayed and the SortBy breadcrumb is displayed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_064

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the pagination scenarios", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_073(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		SearchResultPage searchResultPage;
		int currentPageNo;
		int nextPageNo;
		int PreviousPageNo;
		int lastPageNo;

		List<String> lastPageElement = Arrays.asList("iconLastPage");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Navigated to 'Belk' Search result Page!");
			// Select the product according to search keyword
			currentPageNo = searchResultPage.getCurrentPageNo();
			Log.message("3. Current page no before cliking is :"
					+ currentPageNo);
			Log.assertThat(currentPageNo == 1,
					"Curent page no is 1 before cliking on next button !",
					"Curent page no is not 1 before cliking on next button !",
					driver);
			Log.message("<b>Expected Result:</b><br>   i. Previous number or the back arrow icon System should navigates the customer to the previous page.<br>   ii. Next number or the forward, System should navigates the customer to next page.<br>   iii. System should navigates the customer to first page of the results.<br> iv. System should returns customer to the last page of results.<br>");
			Log.message("<b>Actual Result:</b>");
			searchResultPage.navigateToPagination("Next");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			nextPageNo = searchResultPage.getCurrentPageNo();
			Log.assertThat(
					nextPageNo == currentPageNo + 1,
					" After cliking on next button it navigated to next page  !",
					" After cliking on next button it's not navigated to next page  !",
					driver);
			searchResultPage.navigateToPagination("Previous");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			PreviousPageNo = searchResultPage.getCurrentPageNo();
			Log.assertThat(
					PreviousPageNo == currentPageNo,
					" After cliking on previous button it navigated to previous page !",
					" After cliking on previous button it's not navigated to previous page !",
					driver);
			searchResultPage.navigateToPagination("Last");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			lastPageNo = searchResultPage.getCurrentPageNo();
			Log.softAssertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							lastPageElement, searchResultPage),
					" After cliking on last button it navigated to last page !",
					" After cliking on last button it's not navigated to last page !",
					driver);
			searchResultPage.navigateToPagination("First");
			Log.assertThat(
					currentPageNo == lastPageNo - (lastPageNo - 1),
					" After cliking on first button it navigated to first page !",
					" After cliking on first button it's not navigated to first page !",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_073

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system not displays pagination, if the Search result product is less than 30", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_074(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			int productCount = searchResultPage.getProductCount();
			if (productCount > 30)
				throw new Exception(
						"The product count is more than 30, So unable to test this case.");
			else {

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The pagination should not displayed.");

				Log.assertThat(searchResultPage.elementLayer
						.verifyPageElementsDoNotExist(
								Arrays.asList("txtPagination"),
								searchResultPage),
						"<b>Actual Result:</b> The product count is '"
								+ productCount
								+ "', So the pagination is not displayed.",
						"<b>Actual Result:</b> The product count is '"
								+ productCount
								+ "', So the pagination is displayed.", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_066

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays default MAX 30 items in the search result page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_076(String browser) throws Exception {

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product count shoud between 1 to 30 per page");

			Log.assertThat(
					searchResultPage.getProductCount() <= 30,
					"<b>Actual Result:</b> The product count is '"
							+ searchResultPage.getProductCount() + "'.",
					"<b>Actual Result:</b> The product count is not between 1 to 30 per page i.e.,(Product count="
							+ searchResultPage.getProductCount() + ").", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_068

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays Showing 1 - X items for less than 30 items.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_077(String browser) throws Exception {

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			String totalCount = searchResultPage.getResultHitsText();
			int count = 0;
			if (totalCount.contains("of")) {
				totalCount = totalCount.split("of ")[1];
				count = Integer.parseInt(totalCount.replace(",", ""));
			} else {
				count = searchResultPage.getProductCount();
			}
			Log.message("3. " + count
					+ " products are displayed in the search result page!");
			if (count > 30)
				throw new Exception("The product count is not between 1 to 30.");
			else {
				String itemCountText = searchResultPage.getResultHitsText()
						.trim();
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> 'Viewing 1 - " + count
						+ "' should be displayed.");

				Log.assertThat(
						itemCountText.equals("Viewing 1 - " + count + " of "
								+ count), "<b>Actual Result:</b> '"
								+ itemCountText + "' is displayed!",
						"<b>Actual Result:</b> '" + itemCountText
								+ "' is not displayed!", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_077

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays 'Showing 1 - 30 of X results' for more than 30 items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_078(String browser) throws Exception {

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			String itemCountText = searchResultPage.getResultHitsText();
			int count;
			count = searchResultPage.getProductCount();
			String[] splitTotalItemCountText = itemCountText.split("of ");
			String totalItemCount = splitTotalItemCountText[1];
			searchResultPage.navigateToPagination("2");
			count += searchResultPage.getProductCount();
			if (count < 30)
				throw new Exception(
						"Product count is not greater than 30, Hence unable to test this case");
			else {
				searchResultPage.navigateToPagination("1");
				Log.message("3. '" + totalItemCount
						+ "' products are displayed in the Search result page!");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> System should display 'Viewing 1 - 30 of "
						+ totalItemCount + " results' for more than 30 items.");
				Log.assertThat(
						itemCountText.equals("Viewing 1 - 30 of "
								+ totalItemCount),
						"<b>Actual Result:</b> 4. '" + itemCountText
								+ "' is displayed! in the search result page",
						"<b>Actual Result:</b> 4. '"
								+ itemCountText
								+ "' is not displayed! in the search result page",
						driver);

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_070

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays maximum of 5 pages with first and last page in the Search result.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_079(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			int productCount = searchResultPage.getProductCount();
			if (productCount < 30)
				throw new Exception(
						"The product count is less then or equal to 30, so the pagination is not displayed.");
			else {
				ArrayList<String> pageNos;
				pageNos = searchResultPage.getVisiblePageNos();
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> System should display the maximum of 5 pages with first and last page in the Search result.");
				searchResultPage.navigateToPagination("Next");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("iconFirstPage"),
								searchResultPage),
						"<b>Actual Result:</b> 'First Page' is displayed in the search result page",
						"<b>Actual Result:</b> 'First Page' is not displayed in the search result page");
				Log.assertThat(
						searchResultPage.elementLayer
								.verifyPageElements(
										Arrays.asList("iconLastPage"),
										searchResultPage),
						"<b>Actual Result:</b> 'Last Page' is displayed in the search result page",
						"<b>Actual Result:</b> 'Last Page' is not displayed in the search result page");
				Log.assertThat(
						pageNos.size() <= 5,
						"<b>Actual Result:</b> Pagination: '"
								+ pageNos
								+ "' are displayed maximum of 5 pages in the search result page.",
						"<b>Actual Result:</b> Pagination: '"
								+ pageNos
								+ "' are displayed more than 5 pages in the search result page.",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_071

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system refresh and displays the Search result, when customer choose the option from the Items per page drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_081(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			String itemCountText = searchResultPage.getResultHitsText();
			String[] splitTotalItemCountText = itemCountText.split("of ");
			String totalItemCount = splitTotalItemCountText[1].replace(",", "");

			if (Integer.parseInt(totalItemCount) < 90)
				throw new Exception(
						"The total product count is less than 90. So unable to test this case");
			else {
				String selectedValue = searchResultPage
						.selectItemsPerPageDropDownRandom();
				Log.message("3. Selected option: '" + selectedValue
						+ "' from Items per page dropdown");
				searchResultPage.waitUntilSearchSpinnerDisappear();
				int productCount = searchResultPage.getProductCount();
				String viewSizeInUrl = driver.getCurrentUrl();
				String viewSizeInUrlsplit[] = viewSizeInUrl.split("sz=");
				String[] viewSizeInUrlsplit1 = viewSizeInUrlsplit[1]
						.split("\\&");
				int selectedView = Integer.parseInt(viewSizeInUrlsplit1[0]);
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> System should get refresh and display the Search result.");
				Log.assertThat(
						productCount == selectedView,
						"<b>Actual Result:</b> The system got refreshed and the search result page is diplayed with '"
								+ productCount + "' products.",
						"<b>Actual Result:</b> The system is not getting refreshed.",
						driver);
				Log.testCaseResult();
			}
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_081

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system return to default stage, when customer proceed a new search.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_084(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			String defaultValue = searchresultPage
					.getCurrentValueInItemPerPageDropdown();
			Log.message("3. ByDefault 'view per page' option is '"
					+ defaultValue);

			// select option by index
			String selectedOption = searchresultPage
					.selectItemsPerPageDropDownByIndex(1);
			Log.message("4. selected 'view per page' option is '"
					+ selectedOption + "' !");
			String ChangedValue = searchresultPage
					.getCurrentValueInItemPerPageDropdown();
			Log.message("5. After made changes 'view per page' option is '"
					+ ChangedValue);

			int productCount = searchresultPage.getProductCountInPage();
			Log.message("6. Total no of product after selection of view All is '"
					+ productCount + "' !");

			searchresultPage.headers.searchProductKeyword(searchKey);
			Log.message("7. ReSearched for product and observed the 'view per page' value !");

			String reSearchedDefaultValue = searchresultPage
					.getCurrentValueInItemPerPageDropdown();
			Log.message("5. After Re-Search 'view per page' option is '"
					+ reSearchedDefaultValue);

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should get return to the default stage in the Items per page.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					defaultValue.equals(reSearchedDefaultValue),
					"System returns the default stage in the Items per page.",
					"System didn't returns the default stage in the Items per page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_084

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system navigates the customer to the 'Articles Found' content by jumping the customer down the search results page, when customer click on the 'Article Found' link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_089(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblArticel");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnArticlesFound();
			Log.message("3. clicked on Article Found link !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should navigate the customer to 'Article Found' content which is present below the Search result.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(element,
							searchresultPage),
					"System navigated the customer to 'Article Found' content which is present below the Search result.",
					"System navigated the customer to 'Article Found' content which is present below the Search result.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_089

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the 'Product count' based on the Search term", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_090(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			String[] productFoundCount = searchresultPage.getProductsFound()
					.split("\\(");
			String[] productViewCount = searchresultPage.getResultHitsText()
					.split("of ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Searching the product, it should show the product Found Count in the Search result Page");

			Log.assertThat(
					productFoundCount[1].contains(productViewCount[1]),
					"<b>Actual Result:</b> Prodct found: "
							+ productFoundCount[1] + "and Product View : "
							+ productViewCount[1]
							+ "both count are equals in the search result page",
					"<b>Actual Result:</b> Prodct found count:"
							+ productFoundCount[1]
							+ "and Product View : "
							+ productViewCount[1]
							+ " both count are not equals in the search result page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_081

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the 'Article found (XX)' based upon the article found based on the search term", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_091(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> ArticlesFound = Arrays.asList("lnkArticlesFound");
		String txtArticles = "Articles Found (";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.softAssertThat(
					searchresultPage.elementLayer.verifyPageElements(
							ArticlesFound, searchresultPage),
					"3. Articles found link is  displayed on Belk search result page",
					"Articles found link is not displayed on Belk search result page");

			searchresultPage.clickOnArticlesFound();
			Log.message("4. Clicked on article found link in the search result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking Article link it should shown article section with Article count");

			Log.assertThat(
					searchresultPage.getTextFromArticles()
							.contains(txtArticles),
					"<b>Actual Result:</b>"
							+ searchresultPage.getTextFromArticles() + " and "
							+ txtArticles + " both are equals",
					"<b>Actual Result:</b>"
							+ searchresultPage.getTextFromArticles() + " and "
							+ txtArticles + " both not are equals");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_082

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the related articles section at the bottom of product search results", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_092(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> ArticlesFound = Arrays.asList("lnkArticlesFound");
		List<String> ArticlesList = Arrays.asList("lstArticles");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.softAssertThat(
					searchresultPage.elementLayer.verifyPageElements(
							ArticlesFound, searchresultPage),
					"3. Articles found link is  displayed on Belk search result page",
					"Articles found link is not displayed on Belk search result page");

			searchresultPage.clickOnArticlesFound();
			Log.message("4. Clicked on article found link in the search result page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>  System should display the related articles section at the bottom of product search results.");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							ArticlesList, searchresultPage),
					"<b>Actual Result:</b> Related articles section should shown at the bottom of product search results.",
					"<b>Actual Result:</b> Related articles section is not shown at the bottom of product search results.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_083

	@Test(groups = { "desktop", "mobile" }, description = " Verify system displays the article based upon the Recent search and whenever related article is found for the search term", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_093(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> ArticlesFound = Arrays.asList("lnkArticlesFound");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search' "
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.softAssertThat(
					searchresultPage.elementLayer.verifyPageElements(
							ArticlesFound, searchresultPage),
					"3. Articles found link is  displayed on Belk search result page",
					"Articles found link is not displayed on Belk search result page");

			searchresultPage.clickOnArticlesFound();
			Log.message("4. Clicked on article found link in the search result page");

			String articlesCount = searchresultPage.getTextFromArticles()
					.split("\\(")[1];

			int articlesCountfromList = searchresultPage.checkArticlesCount();
			String count = "" + articlesCountfromList;

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>  Article should shown in the search result page whenever article is found ");

			Log.assertThat(
					count.equals((articlesCount.replace(")", ""))),
					"<b>Actual Result:</b> Article should shown in the recent search result page",
					"<b>Actual Result:</b> Article is not shown in the recent search result page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_084

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays Related articles on every Search Results page, independent of any paging or refinement.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_096(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> ArticlesFound = Arrays.asList("lnkArticlesFound");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search with keyword '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.softAssertThat(
					searchresultPage.elementLayer.verifyPageElements(
							ArticlesFound, searchresultPage),
					"3. Articles found link is  displayed on search result page",
					"Articles found link is not displayed on search result page");

			searchresultPage.clickOnArticlesFound();
			Log.message("4. Clicked on article found link in the search result page");

			String articlesCount = searchresultPage.getTextFromArticles()
					.split("\\(")[1];

			searchresultPage.navigateToPagination("2");
			searchresultPage.waitUntilSearchSpinnerDisappear();
			Log.message("5. Navigated To next SearchResultPage");

			Log.softAssertThat(
					searchresultPage.elementLayer.verifyPageElements(
							ArticlesFound, searchresultPage),
					"6. Articles found link is  displayed on Belk search result page",
					"Articles found link is not displayed on Belk search result page");

			String articlesCount1 = searchresultPage.getTextFromArticles()
					.split("\\(")[1];

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Related articles should shown on every Search Results page,");

			Log.assertThat(
					articlesCount.equals(articlesCount1),
					"<b>Actual Result:</b> Related articles shown on every Search Results page",
					"<b>Actual Result:</b> Related articles is not shown on every Search Results page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_096

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the article content in 3 row of two.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_097(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");

			searchResultPage.checkArticlesCount();
			Log.message("3. Clicked On 'View All Articles' Link");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The system should displays the Articles by default to 3 rows of 2 in the SearchResult page.");

			if (Utils.getRunPlatForm() == "desktop") {
				int YLocationOfFirstProduct = searchResultPage
						.getCurrentYLocationOfArticleByIndex(1);
				int YLocationOfThirdProduct = searchResultPage
						.getCurrentYLocationOfArticleByIndex(3);
				Log.assertThat(
						YLocationOfFirstProduct != YLocationOfThirdProduct,
						"<b>Actual Result:</b> The system is displayed the Articles by default to 3 rows of 2 in the SearchResult page.)",
						"<b>Actual Result:</b> The system is not displayed the Articles by default to 3 rows of 2 in the SearchResult page.",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				int XLocationOfFirstProduct = searchResultPage
						.getCurrentXLocationOfArticleByIndex(1);
				int XLocationOfThirdProduct = searchResultPage
						.getCurrentXLocationOfArticleByIndex(2);
				Log.assertThat(
						XLocationOfFirstProduct == XLocationOfThirdProduct,
						"<b>Actual Result:</b> The system is displayed the Articles by default to 3 rows of 2 in the SearchResult page.)",
						"<b>Actual Result:</b> The system is not displayed the Articles by default to 3 rows of 2 in the SearchResult page.",
						driver);
			}

			Log.testCaseResult();
		}

		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_097

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'View All Articles' along with the number of Articles returned from the Search result.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_100(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnArticlesFound();
			Log.message("3. clicked on Article Found link !");

			String Article = searchresultPage.getTextFromArticles()
					.replace(" ", "").trim();
			String No_Of_Article = Article.split("Found")[1];

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should displays the 'View All Articles' along with the number of Articles returned from the Search result.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					Article.contains(No_Of_Article),
					"System displayed the Article found with No. of Article below the Search result.",
					"System not displayed the Article found with No. of Article below the Search result.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_100

	@Test(groups = { "desktop" }, description = "Verify system displays the Breadcrumb only to the previous page and The last page / System will retain the state it was in when the customer left the page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_110(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile.");
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			String BreadCrumbInSLP = searchresultPage.getBreadCrumbEndValue();
			searchresultPage.clickOnViewAllArticleButton();
			Log.message("3. clicked on view All Article link !");
			String BreadCrumbInArticlePage = searchresultPage
					.getArticleBreadcrumbText();

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should display the Breadcrumb only to the previous page.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					BreadCrumbInSLP.equalsIgnoreCase(BreadCrumbInArticlePage),
					"System displays the Breadcrumb only to the previous page.",
					"System not displayed the Breadcrumb only to the previous page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_110

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'View More' link appear after 'n' number of value and this configured per attribute in the Business Manager by setting a cutoff threshold.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_122(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should displays the 'View More' link. ");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							Arrays.asList("ViewMorelnkInRefinement"),
							searchresultPage),
					"System displayed the 'View More' link in searcResultPage. ",
					"System not displayed the 'View More' link in searcResultPage. ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_122

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the available refinement after clicking 'view more' Button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_125(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		String txtrefinementOptions[] = "refinementSize|refinementColor|brand"
				.split("\\|");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage
						.clickCollapseExpandToggle(txtrefinementOptions[0]);
			}
			int refinementValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions[0]);
			Log.message("3. Got the count of Size refinement before clciking view more link !"
					+ refinementValueBeforeClick);
			searchresultPage.clickViewMoreLink(txtrefinementOptions[0]);
			Log.message("4. View more link is clicked", driver);
			int refinementValueAfterClick = searchresultPage
					.refinementValuesCountViewMore(txtrefinementOptions[0]);
			Log.message("5. Got the count of refinement after view more link"
					+ refinementValueAfterClick);

			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage
						.clickCollapseExpandToggle(txtrefinementOptions[1]);
			}

			int colorValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions[1]);
			Log.message("6. Got the count of color refinement before clciking view more link !"
					+ colorValueBeforeClick);
			searchresultPage.clickViewMoreLink(txtrefinementOptions[1]);
			Log.message("7. View more link is clicked", driver);
			int colorValueAfterClick = searchresultPage
					.refinementValuesCountViewMore(txtrefinementOptions[1]);
			Log.message("8. Got the count of refinement after view more link"
					+ colorValueAfterClick);

			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage
						.clickCollapseExpandToggle(txtrefinementOptions[2]);
			}

			int BrandValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions[2]);
			Log.message("9. Got the count of Brand refinement before clciking view more link !"
					+ BrandValueBeforeClick);
			searchresultPage.clickViewMoreLink(txtrefinementOptions[2]);
			Log.message("10. View more link is clicked", driver);
			int BrandValueAfterClick = searchresultPage
					.refinementValuesCountViewMore(txtrefinementOptions[2]);
			Log.message("11. Got the count of refinement after view more link"
					+ BrandValueAfterClick);

			Log.message("<br>");
			Log.message("<b> Expected Result: </b> System should display rest number of hide 'size' , 'color' , 'Brand' after clicking viewMore link.");
			Log.message("<br>");

			Log.assertThat(
					refinementValueAfterClick > refinementValueBeforeClick,
					"<b> Actual Result 1: </b> System displayed rest number of hide size after clicking viewMore link.",
					"<b> Actual Result 1: </b> System not displayed rest number of hide size after clicking viewMore link.",
					driver);

			Log.assertThat(
					colorValueAfterClick > colorValueBeforeClick,
					"<b> Actual Result 2: </b> System displayed rest number of hide color after clicking viewMore link.",
					"<b> Actual Result 2: </b> System not displayed rest number of hide color after clicking viewMore link.",
					driver);

			Log.assertThat(
					BrandValueAfterClick > BrandValueBeforeClick,
					"<b> Actual Result 3: </b> System displayed rest number of hide Brand after clicking viewMore link.",
					"<b> Actual Result 3: </b> System not displayed rest number of hide Brand after clicking viewMore link.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_125

	@Test(groups = { "desktop" }, description = "Verify system displays updated search result page and breadcrumb by clearing all the refinements when customer click on the 'Clear All' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_126(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> refinedBy = Arrays.asList("lblRefinedBy");

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("3. Selecting " + color
					+ " in the refinement under color section");

			String size = searchresultPage
					.selectUnselectFilterOptions("refinementSize");
			Log.message("4. Selecting " + size
					+ " in the refinement under size section");

			searchresultPage.clickClearAllLnk();
			Log.message("5. Clicked 'ClearAll' link");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Refinements selected should be cleared in breadcrumb when clicking on 'Clear All' link");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElementsDoNotExist(
							refinedBy, searchresultPage),
					"<b>Actual Result:</b> Selected refinements are cleared",
					"<b>Actual Result:</b> Selected refinements are not cleared",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_115

	@Test(groups = { "mobile" }, description = "Mobile : Verify system expand or collapse based on an accordion icon in the refinements.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_129(String browser) throws Exception {
		// For Mobile alone
		// ** Loading the test data from excel using the test case id */
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "mobile") {
			throw new SkipException(
					"This testcase(Filter By button) is applicable only for 'mobile'");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String filterType = "refinementSize";
		String filter = "refinementSize";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			searchresultPage.clickFilterByInMobile();
			Log.message("3. Clicked on 'FilterBy' button");
			searchresultPage.clickCollapseExpandToggle(filterType);
			Log.message("3. Clicked on expand toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 1. System should expand the refinements option, when customer click on the + icon");
			Log.assertThat(searchresultPage.verifyExpandToggle(filter),
					"<b>Actual Result:</b>  filter type " + filter
							+ " list is expanded properly in PLP page",
					"<b>Actual Result:</b>  filter type " + filter
							+ " is not expanded  in PLP page", driver);
			searchresultPage.clickCollapseExpandToggle(filterType, "collapse");
			Log.message("3. Clicked on collapse toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 2. System should collapse the refinements option, when customer click on the - icon.");
			Log.assertThat(!searchresultPage.verifyExpandToggle(filter),
					"<b>Actual Result:</b>  filter type " + filter
							+ " is collapsed properly in PLP page",
					"<b>Actual Result:</b>  filter type " + filter
							+ " is not collapsed in PLP page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH__118

	@Test(groups = { "mobile" }, description = "Mobile : Verify system hide the refinements, when customer click on the 'X' icon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_130(String browser) throws Exception {
		if (!(Utils.getRunPlatForm() == "mobile"))
			throw new SkipException(
					"This test  case is applicable only for mobile");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page for Search key : "
					+ searchKey);

			searchResultPage.headers.clickMobileHamburgerMenu();
			Log.message("3. Mobile Menu Opened!", driver);

			searchResultPage.closeMobileHamburgerMenu();
			Log.message("4. Clicked 'X' Button in Mobile amburger Menu");

			Log.message("<br><b>Expected Result : </b>User should not see the Refinement Modal");
			Log.assertThat(searchResultPage.elementLayer
					.verifyPageElementsDoNotExist(
							Arrays.asList("islnkHamburgerOpened"),
							searchResultPage),
					"<b>Actual Result : </b>Refinement Modal is closed!",
					"<b>Actual Result : </b>Refinement Modal is still active",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_119

	@Test(groups = { "desktop", "mobile" }, description = "Verify that the 'Sort By' label is shown in Search Results Page below the Sort By dropdown.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_131(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String sortby = "Sort By:";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			String txtSortBy = searchresultPage.getTextFromSortBy();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> . Label Sort By should be shown above the Sort By dropdown, : (Colon) is present after the text 'Sort By'");
			Log.assertThat(
					txtSortBy.equals(sortby),
					"<b>Actual Result:</b> 3.Label Sort By is shown above the Sort By dropdown,  : (Colon) is present after the     text 'Sort By'",
					"<b>Actual Result:</b> 3.Label Sort By is not shown properly",
					driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_120

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion�plus/minus symbol�in the left navigation of category refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_133(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.message("* Verifying Expand/Collapse toggle in Search Listing Page");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with Keyword:'"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Listing' Page!");
			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
			}
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> It should expand or collapse the current branch of the navigation tree.");
			Log.assertThat(
					searchResultPage.VerifyCategoryToggle(),
					"<b>Actual Result 1: </b> In search Listing Page, 'Category Refinement' toggle expaned/collapsed as expected",
					"<b>Actual Result 1: </b> In search Listing Page, 'Category Refinement' toggle not expanded/collapsed as expected",
					driver);
			Log.message("</br>");
			Log.message("* Verifying Expand/Collapse toggle in Product Listing Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("3. Navigated to '" + category[0] + " > " + category[1]
					+ "' Page !");
			PlpPage plppage = new PlpPage(driver);
			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
			}
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> It should expand or collapse the current branch of the navigation tree.");
			Log.assertThat(
					plppage.VerifyCategoryToggle(),
					"<b>Actual Result 2: </b> In Product Listing Page, 'Category Refinement' toggle expaned/collapsed as expected",
					"<b>Actual Result 2: </b> In Product Listing Page, 'Category Refinement' toggle not expanded/collapsed as expected",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_133

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the sub-categories of L2s", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_134(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String categoryLevel = testData.get("CategoryLevel");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// navigate to category landing page for a random l2

			String[] category = homePage.headers
					.navigateToRandomCategory(categoryLevel);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to Category Landing Page!");

			int HeaderNavl3Count = plppage.headers.getL3CategoryListByIndex(
					Integer.parseInt(category[3]),
					Integer.parseInt(category[4])).size();

			int LeftNavl3Count = plppage.lstSubCategoryRefinement.size();
			Log.message("<br>");
			Log.message("     L3 Categories In Category Refinement");
			for (int i = 1; i <= LeftNavl3Count; i++)
				Log.message("        "
						+ i
						+ ". "
						+ plppage.lstSubCategoryRefinement.get(i - 1).getText()
								.trim());

			System.out.println("---------->>>>>>L3 Refinement size :: "
					+ LeftNavl3Count);
			if (LeftNavl3Count > 0) {
				if (LeftNavl3Count == HeaderNavl3Count) {
					List<String> headerNavL2 = plppage.headers.getL3ListText(
							Integer.parseInt(category[3]),
							Integer.parseInt(category[4]));
					List<String> LeftNavL2 = plppage
							.getLeftNavigationListText();
					Log.message("<br>");
					Log.message("<b> Expected Result : </b> All of the L3 categories and sub-categories should be listed beneath the L2s in Left Navigation.");
					Log.assertThat(
							Utils.compareTwoList1(headerNavL2, LeftNavL2),
							"<b>Actual Result :</b> L3 Category displayed beneath L2 in Left Navigation!",
							"<b>Actual Result :</b> L3 Category in Left Navigation not displayed same as Header Navigation",
							driver);
				} else {
					Log.fail(
							"Actual Result :</b> L3 Categoryin Left Navigation not displayed With all L3 Categories in Header Navigation",
							driver);
				}
			}
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_123

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the product count of L2 and L3 categories and sub-categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider", enabled = false)
	public void TC_BELK_SEARCH_135(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String categoryLevel = testData.get("CategoryLevel");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// navigate to category landing page for a random l2

			homePage.headers.navigateToRandomCategory(categoryLevel);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to Category Landing Page!");

			int LeftNavl3Count = plppage.lstSubCategoryRefinement.size();
			List<String> LeftNavCount = plppage
					.getCategoryRefinementItemsCount();

			Log.message("</br>");
			Log.message("<b> Expected Result : </b>All categories should display the product count properly!");
			for (int i = 0; i < LeftNavl3Count; i++) {
				if (LeftNavCount.get(i).isEmpty())
					Assert.fail("<b>Actual Result : </b>Not All categories display the product count as expected");
			}
			Log.message("<b>Actual Result : </b>All categories display the product count as expected!");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_124

	@Test(groups = { "desktop" }, description = "Verify selecting the L3 category which has L4 categories.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_139(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile.");

		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page with '"
					+ L1Category + "' and '" + L2Category + "' and '"
					+ L3Category + "' ");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("In breadcrumb system should display the L3Category value.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			ArrayList<String> BreadCrumb = categoryLandingPage
					.getBreadCrumbValue();
			String BreadCrumbL3 = BreadCrumb.get(2);
			Log.assertThat(BreadCrumbL3.equals(L3Category),
					"In breadcrumb L3 category displayed.",
					"In breadcrumb L3 category not displayed.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("The page should refresh and the category refinements should displays the L3 category name and it’s sub­categories (L4) with a breadcrumb link.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			String L4CategoryName = categoryLandingPage
					.getL4CategoryNameByIndex(1);
			categoryLandingPage.clickL4CategoryByIndex(1);
			Log.message("3. clicked on L4 Category.");
			ArrayList<String> BreadCrumb1 = categoryLandingPage
					.getBreadCrumbValue();
			String BreadCrumbL4 = BreadCrumb1.get(3);

			Log.assertThat(
					L4CategoryName.equals(BreadCrumbL4),
					"The page refreshed and the category refinements displayed the L3 category name and it’s sub­categories (L4) with a breadcrumb link.",
					"The page refreshed and the category refinements not displayed the L3 category name and it’s sub­categories (L4) with a breadcrumb link.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>");
			Log.message("The 'Back to L2 category name' link should display after clicking on L4Category.");
			Log.message("<br>");
			Log.message("<b>Actual Result 3:</b>");

			Log.assertThat(
					categoryLandingPage.elementLayer.verifyPageElements(
							Arrays.asList("btnBackarrowLeftNav"),
							categoryLandingPage),
					"The 'Back to L2 category name' link  displayed after clicking on L4Category.",
					"The 'Back to L2 category name' link not displayed after clicking on L4Category.",
					driver);

			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_139

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Filter By options in the left navigation of product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_146(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		String runPltfrm = Utils.getRunPlatForm();
		String searchRefinementColor = "Color";
		String searchRefinementBrand = "Brand";
		String searchRefinementSize = "Size";
		String searchRefinementPrice = "Price";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("<br>");
			Log.message("<u>Verifying Product List Page</u>");
			Log.message("2. Navigated to PLP page", driver);
			if (runPltfrm == "mobile") {

				plppage.clickFilterByInMobile();
			}
			Log.message("<b>Expected Result: </b>The following Filter By options should be displayed below the product refinements panel in the left navigations of PLP pages:"
					+ "<br>a. Color <br> b. Price <br> c. Brands <br> d. Size");
			Log.assertThat(plppage.verifyRefinement(searchRefinementColor),
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is displayed in Plppage properly",
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is not displayed in Plp page properly", driver);
			Log.assertThat(plppage.verifyRefinement(searchRefinementBrand),
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is displayed displayed in Plp page properly",
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is not displayed in Plp page properly");
			Log.assertThat(plppage.verifyRefinement(searchRefinementSize),
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is displayed displayed in Plp page properly",
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is not displayed in Plp page properly");
			Log.assertThat(plppage.verifyRefinement(searchRefinementPrice),
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is displayed displayed in Plp page properly",
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is not displayed in Plp page properly");
			Log.message("<br>");
			Log.message("<u>Verifying Category Landing Page</u>");
			plppage.headers.navigateTo(category[0], category[1]);
			CategoryLandingPage clppage = new CategoryLandingPage(driver);
			Log.message("3. Navigated to CLP page", driver);
			if (runPltfrm == "mobile") {

				clppage.clickFilterByInMobile();
			}
			Log.message("<b>Expected Result: </b>The following Filter By options should be displayed below the product refinements panel in the left navigations of CLP pages:"
					+ "<br> a. Color <br> b. Price <br> c. Brands <br> d. Size");
			Log.assertThat(clppage.verifyRefinement(searchRefinementColor),
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is displayed in Clppage properly",
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is not displayed Clppage properly", driver);
			Log.assertThat(clppage.verifyRefinement(searchRefinementBrand),
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is displayed in Clppage properly",
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is not displayed in Clppageproperly");
			Log.assertThat(clppage.verifyRefinement(searchRefinementSize),
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is displayed in Clppage properly",
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is not displayed Clppage properly");
			Log.assertThat(clppage.verifyRefinement(searchRefinementPrice),
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is displayed in Clppage properly",
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is not displayed in Clppage properly");
			if (runPltfrm == "mobile") {
				clppage.clickCloseIconInRefinementSection();

			}
			Log.message("<br>");
			Log.message("<u>Verifying Search Result Page</u>");
			if (runPltfrm == "mobile") {
				plppage.clickOnCloseIcon();
			}
			SearchResultPage searchresultPage = plppage.headers
					.searchProductKeyword(searchKey);
			Log.message("4. Navigated to Search Result Page");
			Log.message("5. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();

			}
			Log.message("<b>Expected Result: </b>The following Filter By options should be displayed below the product refinements panel in the left navigations of SLP pages:"
					+ "<br> a. Color <br> b. Price <br> c. Brands <br> d. Size");
			Log.assertThat(
					searchresultPage.verifyRefinement(searchRefinementColor),
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is displayed in Slppage properly",
					"<b>Actual Result: a. </b>Search Refinement '"
							+ searchRefinementColor
							+ "' is not displayed in Slppageproperly", driver);
			Log.assertThat(
					searchresultPage.verifyRefinement(searchRefinementBrand),
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is displayed in Slppage properly",
					"<b>Actual Result: b. </b>Search Refinement '"
							+ searchRefinementBrand
							+ "' is not displayed in Slppageproperly");
			Log.assertThat(
					searchresultPage.verifyRefinement(searchRefinementSize),
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is displayed in Slppage properly",
					"<b>Actual Result: c. </b>Search Refinement '"
							+ searchRefinementSize
							+ "' is not displayed in Slppageproperly");
			Log.assertThat(
					searchresultPage.verifyRefinement(searchRefinementPrice),
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is displayed in Slppage properly",
					"<b>Actual Result: d. </b>Search Refinement '"
							+ searchRefinementPrice
							+ "' is not displayed in Slppage properly");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_146

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Size Refinement Heading under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_148(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to PLP page");

			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
			}
			Log.assertThat(
					plppage.verifyColorOfSizeRefinementText(),
					"3. The refinement heading Size is displayed as blue color in plppage",
					"3. The refinement heading Size is not displayed as blue color in plppage");
			/*
			 * CategoryLandingPage clppage = plppage.navigateToCLP();
			 * 
			 * if (runPltfrm == "mobile") {
			 * 
			 * clppage.clickFilterByInMobile(); }
			 * Log.assertThat(clppage.verifyColorOfSizeRefinementText(),
			 * "4. The refinement heading Size is displayed  blue color in clppage"
			 * ,
			 * "4. The refinement heading Size is not displayed as blue color in clp page"
			 * , driver);
			 */

			if (runPltfrm == "mobile") {
				plppage.clickOnCloseIcon();

			}
			SearchResultPage searchresultPage = plppage.headers
					.searchProductKeyword(searchKey);
			Log.message("4. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();

			}
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The refinement heading 'Size' should be displayed bold in blue color.");
			Log.assertThat(
					searchresultPage.verifyColorOfSizeRefinementText(),
					"<b>Actual Result: </b>The refinement heading 'Size' is displayed  blue color in searchresultPage",
					"<b>Actual Result: </b>The refinement heading 'Size' is not displayed as blue color in searchresultPage",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_137

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Expand/Collapse option for Size refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_149(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");

		String runPltfrm = Utils.getRunPlatForm();
		String filterType = "refinementSize";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("<br>");
			Log.message("<u>Verifying PLP</u>");
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to PLP page", driver);
			if (runPltfrm == "mobile") {

				plppage.clickFilterByInMobile();
				plppage.clickCollapseExpandToggle(filterType);
			}
			Log.message("<br>");
			Log.message("3. Clicked expand toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> It should expand or collapse the Size refinement section in the page.");
			Log.assertThat(
					plppage.verifyExpandToggle(filterType),
					"  Filter type "
							+ filterType
							+ "<b>Actual Result: </b> list is expanded properly in plppage",
					"filter type "
							+ filterType
							+ "<b>Actual Result: </b> is not expanded  in plppage",
					driver);
			plppage.clickCollapseExpandToggle(filterType);

			Log.message("4. Clicked collapse toggle of '" + filterType + "'");

			Log.assertThat(!plppage.verifyExpandToggle(filterType),
					" <b>Actual Result: </b> Filter type " + filterType
							+ " is collapsed properly in plppage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not collapsed in plppage", driver);
			if (runPltfrm == "desktop") {
				plppage.clickCollapseExpandToggle(filterType);

				Log.message("5. Clicked expand toggle of '" + filterType + "'");
				Log.assertThat(
						plppage.verifyExpandToggle(filterType),
						"<b>Actual Result: </b>  Filter type " + filterType
								+ " list is expanded properly in plppage",
						"filter type "
								+ filterType
								+ "<b>Actual Result: </b> is not expanded  in plppage",
						driver);
			}

			/*
			 * Log.message("<u>Verifying Category Landing Page</u>");
			 * CategoryLandingPage clppage = plppage.navigateToCLP(); if
			 * (runPltfrm == "mobile") {
			 * 
			 * clppage.clickFilterByInMobile();
			 * clppage.clickCollapseExpandToggle(filterType); }
			 * Log.message("<br>");
			 * Log.message("6. Clicked collapse toggle of '" + filterType +
			 * "'"); Log.message("<br>"); Log.message("<b>Actual Result: </b>");
			 * Log.assertThat(clppage.verifyExpandToggle(filterType),
			 * ". filter type " + filterType +
			 * " list is expanded properly in clppage", "filter type " +
			 * filterType + " is not expanded as default in clppage", driver);
			 * clppage.clickCollapseExpandToggle(filterType);
			 * Log.message("7. Clicked collapse toggle of '" + filterType +
			 * "'"); Log.message("<br>"); Log.message("<b>Actual Result: </b>");
			 * Log.assertThat(!clppage.verifyExpandToggle(filterType),
			 * "  Filter type " + filterType +
			 * " is collapsed properly in clppage", "filter type " + filterType
			 * + " is not collapsed in clppage", driver); if (runPltfrm ==
			 * "desktop") { clppage.clickCollapseExpandToggle(filterType);
			 * Log.message("8. Clicked expand toggle of '" + filterType + "'");
			 * Log.message("<br>"); Log.message("<b>Actual Result: </b>");
			 * Log.assertThat(clppage.verifyExpandToggle(filterType),
			 * "  Filter type " + filterType +
			 * " list is expanded properly in clppage", "filter type " +
			 * filterType + " is not expanded  in plppage", driver); }
			 * 
			 * if (runPltfrm == "mobile") {
			 * clppage.clickCloseIconInRefinementSection();
			 * 
			 * } Log.message("<br>");
			 */
			Log.message("<u>Verifying Search Result Page</u>");

			if (runPltfrm == "mobile") {
				plppage.clickOnCloseIcon();

			}

			SearchResultPage searchresultPage = plppage.headers
					.searchProductKeyword(searchKey);
			Log.message("6. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(filterType);
			}
			Log.message("7. Clicked expand toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> It should expand or collapse the Size refinement section in the page.");

			Log.assertThat(
					searchresultPage.verifyExpandToggle(filterType),
					"<b>Actual Result: </b>  Filter type " + filterType
							+ " list is expanded properly in searchresultPage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not expanded as default in searchresultPage",
					driver);

			searchresultPage.clickCollapseExpandToggle(filterType);
			Log.message("8. Clicked collapse toggle of '" + filterType + "'");

			Log.assertThat(searchresultPage.verifyExpandToggle(filterType),
					"<b>Actual Result: </b>  Filter type " + filterType
							+ " is collapsed properly in searchresultPage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not collapsed in searchresultPage", driver);
			if (runPltfrm == "desktop") {
				searchresultPage.clickCollapseExpandToggle(filterType);
				Log.assertThat(searchresultPage.verifyExpandToggle(filterType),
						"  Filter type " + filterType
								+ " list is expanded properly in plppage",
						"filter type " + filterType
								+ " is not expanded  in plppage", driver);
				Log.testCaseResult();
			}
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_SEARCH_139

	/*
	 * @Test(groups = { "desktop", "mobile", "tablet" }, description =
	 * "Verify - Clicking on (+)/(-) for Brand refinement in product refinement panel"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_157(String
	 * browser) throws Exception {
	 * 
	 * String runPltfrm = Utils.getRunPlatForm();
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String searchKey = testData.get("SearchKey"); String filterType =
	 * "brand"; final WebDriver driver = WebDriverFactory.get(browser);
	 * Log.testCaseInfo(testData);
	 * 
	 * 
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * 
	 * SearchResultPage searchresultPage = homePage
	 * .searchProductKeyword(searchKey); Log.message("2. Search '" + searchKey +
	 * "' in the home page and  Navigated to 'Search Result'  Page!");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption(); }
	 * 
	 * Log.message(
	 * "<br><b>Expected Result: </b> Clicking on (-) in Brand Refinement should collapse the refinement"
	 * );
	 * 
	 * Log.assertThat(searchresultPage.clickCollapseToggle(filterType),
	 * "<b>Actual Result: </b> Clicking on (-) in " + filterType +
	 * " refinement is collapsed properly in search results page",
	 * "<b>Actual Result: </b> Clicking on (-) in " + filterType +
	 * " refinement is not collapsed in search results page", driver);
	 * 
	 * Log.message(
	 * "<br><b>Expected Result : </b> Clicking on (+) in Brand Refinement should expand the refinement"
	 * );
	 * 
	 * 
	 * Log.assertThat(searchresultPage.clickCollapseToggle(filterType),
	 * "<b>Actual Result : </b> Clicking on (+) in " + filterType +
	 * " refinement is expanded properly in search results page",
	 * "<b>Actual Result : </b> Clicking on (+) in " + filterType +
	 * " refinement is not expanded in search results page", driver);
	 * 
	 * Log.testCaseResult();
	 * 
	 * } // try catch (Exception e) { Log.exception(e, driver); } // catch
	 * finally { Log.endTestCase(); driver.quit(); } // finally } //
	 * TC_BELK_SEARCH_157
	 */

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Refinement count for Brands under product refinements panel ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_174(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		SearchResultPage searchResultPage;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword

			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Search '"
					+ searchkey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.message("<b>Expected Result:</b> Verify Refinement count for Brand under product refinements panel ");

			searchResultPage.verifyCountBrandRefinement();

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("lstBrandCountByRefinement"),
							searchResultPage),
					"<b>Actual Result:</b> Count for the products is displayed next to the brand name",
					"<b>Actual Result:</b> Count for the products is not displayed next to the brand name");

			/*
			 * for (int i = 0; i < CountByRefinement; i++) { int refineCount =
			 * searchResultPage.verifyCountBrandRefinement(i);
			 * searchResultPage.waitUntilSearchSpinnerDisappear();
			 * 
			 * 
			 * String viewCountText = searchResultPage.getResultHitsText();
			 * String[] splitCountText; if (viewCountText.contains("of")) {
			 * splitCountText = viewCountText.split("of "); } else {
			 * splitCountText = viewCountText.split("- "); }
			 * 
			 * int totalProductCount = Integer
			 * .parseInt(splitCountText[splitCountText.length - 1]);
			 * Log.assertThat( refineCount == totalProductCount,
			 * "Count in Brand Refine page is equals to the count of products filtered in searchResultPage - "
			 * + totalProductCount,
			 * "Count in Brand Refine page is not equals to the count of products filtered in searchResultPage - "
			 * + totalProductCount, driver); }
			 */

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			// driver.quit();
		}// finally

	}// TC_BELK_SEARCH_174

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the displays of prices in Price refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_175(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}
			int rangeCount = searchResultPage.getPriceRangeValueCount();
			Log.message("<br> <b>Expected Result : </b>User should see the '$' symbol in Price Range");
			for (int i = 0; i < rangeCount; i++) {
				if (searchResultPage.getPriceRangeByRow(i).contains("$"))
					Log.fail("<b>Actual Result : </b>'$' symbol is not displayed in Price Range");
			}
			Log.message("<b>Actual Result : </b>'$' symbol is displayed in Price Range");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_164

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Price Refinement Heading under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_176(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
			}

			Log.message("<br> <b>Expected Result : </b>User should see Price font in Blue Color");
			Log.assertThat(
					SearchUtils.getLabelColor(
							searchResultPage.refinementPriceLabel).equals(
							BlueColor),
					"<b>Actual Result: </b>Price font is displayed in Blue",
					"<b>Actual Result: </b>Price font is not displayed in Blue",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_165

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Expand/Collapse option for Price refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_177(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>The size refinement should be expanded by default.");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleClosed"),
								searchResultPage),
						"<b>Actual Result: </b>Price Refinement Collapsed by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>Price Refinement is not Collapsed by Default",
						driver);
				searchResultPage.priceToggle(true);

				Log.message("<br>3. Price toggle Clicked!<br>");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>An accordion (+/­) symbol should be displayed for the Price refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleExpanded"),
								searchResultPage),
						"<b>Actual Result: </b>Price Refinement Should be Expanded and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Price Refinement is not Expanded",
						driver);
			} else {
				Log.message("<br><b>Expected Result: </b>The size refinement should be expanded by default.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleExpanded"),
								searchResultPage),
						"<b>Actual Result: </b>Price Refinement Expanded by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>Price Refinement is not Expanded by Default",
						driver);
				searchResultPage.priceToggle(false);

				Log.message("<br>3. Price toggle collapsed!<br>");
				Log.message("<br><b>Expected Result: </b> An accordion (+/­) symbol should be displayed for the Price refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleClosed"),
								searchResultPage),
						"<b>Actual Result: </b>Price Refinement Collapsed and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Price Refinement is not Expanded",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_166

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion (+) / (­) symbol for Price refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_178(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>It should expand or collapse the Price refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleClosed"),
								searchResultPage),
						"<b>Actual Result : </b>1. Price Refinement Collapsed by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result : </b>Price Refinement is not Collapsed by Default",
						driver);

				searchResultPage.priceToggle(true);
				Log.message("</br>");
				Log.message("3. Price toggle expanded!</br>");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleExpanded"),
								searchResultPage),
						"<b>Actual Result : </b>2. Price Refinement Expanded and Accordion symbol(+) should be Present",
						"<b>Actual Result : </b>Price Refinement is not Expanded",
						driver);
			} else {
				Log.message("</br><b>Expected Result : </b>It should expand or collapse the Price refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleExpanded"),
								searchResultPage),
						"<b>Actual Result : </b>1. Price Refinement Expanded by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result : </b>Price Refinement is not Expanded by Default",
						driver);

				searchResultPage.priceToggle(false);
				Log.message("<br>3. Price toggle expanded!<br>");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("pricetoggleClosed"),
								searchResultPage),
						"<b>Actual Result : </b>2. Price Refinement Collapsed and Accordion symbol(+) should be Present",
						"<b>Actual Result : </b>Price Refinement is not Expanded by Default",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_167

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable prices in Price refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_179(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
				searchResultPage.priceToggle(true);
			}
			Log.message("<br><b>Expected Result : </b>User Should see the check box for price range");
			Log.assertThat(
					searchResultPage
							.refinementCheckBoxStatus(searchResultPage.refinementPrice),
					"<b>Actual Result : </b>Check boxes for price range is displayed!",
					"<b>Actual Result : </b>Check box is not displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_168

	@Test(groups = { "desktop", "mobile" }, description = "Verify Selecting prices in Price refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_180(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");

			if (Utils.getRunPlatForm() == "mobile") {
				searchResultPage.clickFilterByInMobile();
			}

			String Selected_Price = searchResultPage
					.selectUnselectFilterOptions(" ").split("\\(")[0].trim();
			Log.message("3. Selected Price:"
					+ Selected_Price
					+ " from the price refinement pane in the SearchResult Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The price refinement option should displays an checkbox next to the item indicating that the price range has been selected");

			if (Utils.getRunPlatForm() == "mobile") {
				searchResultPage.clickFilterByInMobile();
			}

			Log.assertThat(
					searchResultPage.VerifySelectedFilterOptions(" ",
							Selected_Price),
					"<b>Actual Result:</b> The price refinement option is displayed an checkbox next to the item indicating that the price range has been selected",
					"<b>Actual Result:</b> The price refinement option is not displayed an checkbox next to the item indicating that the price range has been selected");

			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_180

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected prices in Price refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_181(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			if (runPltfrm == "mobile") {
				plpPage.clickFilterByInMobile();
				plpPage.clickCollapseExpandToggle(" ");

			}

			String priceinPlp = plpPage.selectUnselectFilterOptions(" ");
			priceinPlp = priceinPlp.split("\\(")[0].trim();
			Log.message("3. Selected price: '" + priceinPlp
					+ "' in the refinement under price section in plp page");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The price refinement option should displays an orange checkbox next to the item indicating that the price range has been selected in plp page.");
			if (runPltfrm == "mobile") {
				plpPage.clickFilterByInMobile();
				plpPage.clickCollapseExpandToggle(" ");

			}
			Log.assertThat(
					plpPage.elementLayer.verifyPageListElements(
							Arrays.asList("chkRefinMentActive"), plpPage),
					"<b> Actual Result : </b> price refinement option displays an orange checkbox next to the item indicating that the price range has been selected in plp page.",
					"<b> Actual Result : </b>price refinement option not displays an orange checkbox next to the item indicating that the price range has been selected in plp page. ");
			Log.message("<br>");
			ArrayList<String> textInBreadCrumbValue = plpPage
					.getBreadCrumbLastValues();
			Log.message("4. Got the text from the refinement breadcrumb and the text is"
					+ textInBreadCrumbValue);
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> After selecting price only those products having one of those price range should displays in the product grid");
			Log.assertThat(
					textInBreadCrumbValue.get(0).equals(priceinPlp),
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in plp page",
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in plp page");
			Log.message("<br>");
			if (runPltfrm == "mobile") {
				plpPage.clickOnCloseIcon();
			}

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("5. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(" ");

			}

			String price = searchresultPage.selectUnselectFilterOptions(" ");
			price = price.split("\\(")[0].trim();
			Log.message("6. Selected price: '" + price
					+ "' in the refinement under price section in clp page");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The price refinement option should displays an orange checkbox next to the item indicating that the price range has been selected in slp page.");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(" ");

			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							Arrays.asList("chkRefinMentActive"),
							searchresultPage),
					"<b> Actual Result : </b> price refinement option displays an orange checkbox next to the item indicating that the price range has been selected in slp page.",
					"<b> Actual Result : </b>price refinement option not displays an orange checkbox next to the item indicating that the price range has been selected in slp page. ");
			Log.message("<br>");
			ArrayList<String> textInBreadCrumbValueinslp = searchresultPage
					.getBreadCrumbLastValue();
			Log.message("7. Got the text from the refinement breadcrumb and the text is"
					+ textInBreadCrumbValueinslp);

			Log.message("<br>");
			Log.message("<b> Expected Result : </b> After selecting price only those products having one of those price range should displays in the product grid in slp page");
			Log.assertThat(
					textInBreadCrumbValueinslp.get(0).equals(price),
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in slp page",
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in slp page");
			Log.message("<br>");
			if (runPltfrm == "mobile") {
				searchresultPage.clickOnCloseIcon();
			}
			homePage.headers.navigateTo(category[0], category[1]);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("8. Navigated  to CLP Page with L1-Category("
					+ category[0] + ") -> L2-Category(" + category[1] + ")");

			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(" ");

			}

			String priceInClp = categoryLandingPage
					.selectUnselectFilterOptions(" ");
			priceInClp = priceInClp.split("\\(")[0].trim();
			Log.message("9. Selected price: '" + price
					+ "' in the refinement under price section in Clp page");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The price refinement option should displays an orange checkbox next to the item indicating that the price range has been selected in Clp page.");
			if (runPltfrm == "mobile") {
				categoryLandingPage.clickFilterByInMobile();
				categoryLandingPage.clickCollapseExpandToggle(" ");

			}
			Log.assertThat(
					categoryLandingPage.elementLayer.verifyPageListElements(
							Arrays.asList("chkRefinMentActive"),
							searchresultPage),
					"<b> Actual Result : </b> price refinement option displays an orange checkbox next to the item indicating that the price range has been selected in Clp page.",
					"<b> Actual Result : </b>price refinement option not displays an orange checkbox next to the item indicating that the price range has been selected in Clp page. ");
			Log.message("<br>");
			ArrayList<String> textInBreadCrumbValueinclp = categoryLandingPage
					.getBreadCrumbLastValue();
			Log.message("10. Got the text from the refinement breadcrumb and the text is"
					+ textInBreadCrumbValueinclp);
			String textInBreadCrumbValueinClp = textInBreadCrumbValueinclp
					.get(0).replace("[", "").replace("]", "");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> After selecting price only those products having one of those price range should displays in the product grid in Clp page");
			Log.assertThat(
					textInBreadCrumbValueinClp.equals(priceInClp),
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in Clp page",
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in Clp page");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_181

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Refinement count for Price under product refinements panel ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_183(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		SearchResultPage searchResultPage;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword

			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Search '"
					+ searchkey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			Log.message("<b>Expected Result:</b> Verify Refinement count for Price under product refinements panel ");
			Log.message("<b>Actual Result:</b>");

			int CountByRefinement = searchResultPage
					.lstPriceCountByRefinement();
			Log.message("Total price refine count is : " + CountByRefinement);

			for (int i = 0; i < CountByRefinement; i++) {
				int refineCount = searchResultPage.clickPriceRefinement(i);
				searchResultPage.waitUntilSearchSpinnerDisappear();
				// int CurrentPageProductCount =
				// searchResultPage.totalProducts.size();

				String viewCountText = searchResultPage.getResultHitsText();
				String[] splitCountText;
				if (viewCountText.contains("of")) {
					splitCountText = viewCountText.split("of ");
				} else {
					splitCountText = viewCountText.split("- ");
				}

				int totalProductCount = Integer
						.parseInt(splitCountText[splitCountText.length - 1]);
				Log.assertThat(
						refineCount == totalProductCount,
						"Count in Price Refine page is equals to the count of products filtered in searchResultPage - "
								+ totalProductCount,
						"Count in Price Refine page is not equals to the count of products filtered in searchResultPage - "
								+ totalProductCount, driver);
			}

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_172

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify color swatches when a product has more than 5 colors associated with it", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_185(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		int swatchCountInRow;
		int swatchCountOnViewAll;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchkey);
			Log.message("2. Search '"
					+ searchkey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			swatchCountInRow = searchResultPage.getColorSwatchCount("all");
			Log.message("Color swatch count:" + swatchCountInRow);
			Log.message("<b>Expected Result 1:</b>The first 5 swatches should be displayed in a row and a View all colors (X) link should displayed below to view the remaining swatches.");

			Log.assertThat(
					swatchCountInRow == (5),
					"<b>Actual Result 1:</b> All swatch count is 5 before cliking on view more button !",
					"<b>Actual Result 1:</b> All swatch count is not equal to 5 before cliking on view more button !",
					driver);
			swatchCountOnViewAll = searchResultPage
					.getMouseHoveredOnSwatchCount();

			Log.message("<b>Expected Result 2:</b>Swatch Count should be more than 5 when hovering on view more button");

			Log.assertThat(
					swatchCountOnViewAll > (5),
					"<b>Actual Result 2:</b> All swatch count is more than 5 on hovering on view more button !",
					"<b>Actual Result 2:</b> All swatch count is not more than 5 on hovering on view more button !",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_174

	@Test(groups = { "desktop", "tablet" }, description = "Verify clicking on the “View all Colors” link for a product with more than 5 colors associated with it", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_186(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		String PrimaryImgSrc = null;
		String SwatchImgSrc = null;
		SearchResultPage searchResultPage;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Search '"
					+ searchkey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			// Select the product according to search keyword
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>When a customer clicks the “View all Colors” link, all the color swatches in rows of 5 should be displayed. On hover of the swatch thumbnail, the product tile should update the main image to reflect the selected color.");

			SwatchImgSrc = searchResultPage.getMouseHoveredOnSwatch(6);
			Log.message("3. It mouse hovered on view more button !");
			PrimaryImgSrc = searchResultPage.getPrimaryImageColor();
			// Verify message
			Log.assertThat(
					PrimaryImgSrc.equals(SwatchImgSrc),
					"<b>Actual Result:</b> Selected color is displaying with same color in SearchResultPage ! ",
					"<b>Actual Result:</b> Selected color is not displaying with same color in SearchResultPage ! ",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_175

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the color swatch from product tiles  ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_187(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		String srcBeforeCliking = null;
		String srcAfterCliking = null;
		SearchResultPage searchResultPage;
		PdpPage pdpPage;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Search '"
					+ searchkey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			// Select the product according to search keyword
			String primaryIMGsrc = searchResultPage.getPrimaryImageColor(1,
					"more than 5");

			srcBeforeCliking = searchResultPage.getMouseHovered(1);
			pdpPage = new PdpPage(driver).get();
			Log.message("3. Navigated to 'Belk' PDP Page!");
			srcAfterCliking = pdpPage.getSelectedSwatchColors(1, "all");
			Log.message("<b>Expected Result:</b><br>   Verify Refinement count for Price under product refinements panel ");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					srcBeforeCliking.equals(srcAfterCliking),
					"Selected color is displaying with same color in PDP page ! ",
					"Selected color is not displaying with same color in PDP ! ",
					driver);
			driver.navigate().back();
			String primaryIMGsrc1 = searchResultPage.getPrimaryImageColor(1,
					"more than 5");
			Log.assertThat(primaryIMGsrc.equals(primaryIMGsrc1),
					"it came back with same product !",
					"it came back with different product !", driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_SEARCH_176

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Product Name for products in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_191(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String brandName;
		String productName;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to '" + searchKey
					+ "' search result Page!");

			int productsCount = searchResultPage.totalProducts.size();
			Log.message("3. Total no. of products displayed in a page are: "
					+ productsCount);

			Log.message("</br><b>Expected Result : </b>For Standard Products/Variation Master Products/Product Sets, "
					+ "the product name field should display [Brand] [Product Name]."
					+ "When brand does not exist, it should remove the space for brand.");

			for (int i = 1, j = 1; i <= productsCount; i++) {
				brandName = searchResultPage.getBrandName(i);
				productName = searchResultPage.getProductName(i);
				Log.message("   " + j++ + ". Brand Name : " + brandName
						+ "Product Name : " + productName);

				if ((brandName.equals(" ") || brandName.contains("null"))) {
					Log.fail(
							"<br><b>Actual Result : </b>Brand Name not displayed for product No.: "
									+ i, driver);
				}
				if (productName.isEmpty() || productName.contains("null")) {
					Log.fail(
							"<br><b>Actual Result : </b>Product Name not displayed for product No.: "
									+ i, driver);
				}
			}

			Log.message(
					"<br><b>Actual Result : </b>For Standard Products/Variation Master Products/Product Sets, "
							+ "the product name field displayed as [Brand] [Product Name]."
							+ "When brand does not exist, it removed the space for brand.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_180

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the Product Name in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_192(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> drpQty = Arrays.asList("drpQty");

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// / ----- On PLP
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated to " + l1category + " >> " + l2category
					+ " >> " + l3category);
			PlpPage plpPage = new PlpPage(driver).get();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be navigated to PDP of the product");

			plpPage.navigateToPDPWithProductId();

			PdpPage pdpPage = new PdpPage(driver).get();
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(drpQty, pdpPage),
					"<b>Actual Result:</b> Navigated to PDP of the product when clicked on the product name in PLP",
					"<b>Actual Result:</b> Not navigated to PDP of the product when clicked on the product name in PLP",
					driver);

			// / ----- On search results page

			Log.message("<br>");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("3. Search '" + searchKey
					+ "' and  Navigated to 'Search Result'  Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be navigated to PDP of the product");

			searchresultPage.verifyClickProductName(2);

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(drpQty, pdpPage),
					"<b>Actual Result:</b> Navigated to PDP of the product when clicked on the product name in search results page",
					"<b>Actual Result:</b> Not navigated to PDP of the product when clicked on the product name in search results page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_192

	@Test(groups = { "desktop", "mobile" }, description = "Verify Pricing for the products in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_193(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.searchProduct("290074881038W");
			Log.message("2. Searched with keyword '" + searchKey + "' .");
			Log.message("3. Navigated to PDP page with ProductId");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Pricing details should be displayed properly for Single Variation Product in the PDP page");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPrice"), pdpPage),
					"<b>Actual Result:</b> The Pricing details is displayed properly for Single Variation Product in the PDP page",
					"<b>Actual Result:</b> The Pricing details is not displayed properly for Single Variation Product in the PDP page");

			Log.message("<br>");
			homePage.searchProduct("Anne Cole");
			Log.message("4. Navigated to searchResultPage with productId");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Pricing details should be displayed properly for ProductSet in the PDP page");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElements(
							Arrays.asList("txtPriceInProductSet"), pdpPage),
					"<b>Actual Result:</b> The Pricing details is displayed properly for ProductSet in the PDP page",
					"<b>Actual Result:</b> The Pricing details is not displayed properly for ProductSet in the PDP page");

			Log.message("<br>");
			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("6. NAvigated to CategoryLanding Page");

			categoryLandingPage.navigateToPDPWithProductId();
			Log.message("7. Navigated to PDP Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Pricing details should be displayed properly for Master Product in the Category page");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPrice"), pdpPage),
					"<b>Actual Result:</b> After selecting the price range, the selected price range should be shown in the 'SearchResult' Page",
					"<b>Actual Result:</b> After selecting the price range, the selected price range should be shown in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH2B_193

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Review Stars for standard and variation master products in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_195(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to '" + searchKey
					+ "' search result Page!");

			int productsCount = searchResultPage.totalProducts.size();
			Log.message("3. Total no. of products displayed in a page are: "
					+ productsCount);

			Log.message("</br><b>Expected Result : </b>When a product has a review star rating, the review stars icons should display beneath the product name on the product tile.");
			for (int i = 1; i <= productsCount; i++) {
				if (searchResultPage.verifyReviewStarsByRow(i))
					Log.fail(
							"'Review Stars' ratings not displayed for the Product No. '"
									+ i + "' in Grid", driver);
			}
			Log.message(
					"</br><b>Actual Result : 'Review Stars' ratings displayed for all the Products</b>",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_184

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Review Stars in product tiles for products which do not have any reviews", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_197(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			int productsCount = searchResultPage.totalProducts.size();
			Log.message("3. Total no. of products displayed in a page are: "
					+ productsCount);

			Log.message("<br><b>Expected Result : </b>When a product has a review star rating, the review stars icons should display beneath the product name on the product tile.");
			for (int i = 1; i <= productsCount; i++) {
				if (searchResultPage.verifyReviewStarsByRow(i))
					Log.fail(
							"'Review Stars' ratings not displayed for the Product No. '"
									+ i + "' in Grid", driver);
			}
			Log.message(
					"<br><b>Actual Result : 'Review Stars' ratings displayed for all the Products</b>",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_186

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Badge Icon in Product Tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_201(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> productBadge = Arrays.asList("txtProductBadge");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with the  '" + searchKey
					+ "' in search text box and navigated to 'PDP'  Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Product Badge should be displayed below the product in Search Results page");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(productBadge,
							pdpPage),
					"<b>Actual Result:</b> Product Badge is displayed below the product in Search Results page",
					"<b>Actual Result:</b> Product Badge is not displayed below the product in Search Results page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet" }, description = "Verify - selected color is displayed in breadcrumb", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_207(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		ArrayList<String> breadcrumbText = new ArrayList<String>();
		ArrayList<String> categoryText = new ArrayList<String>();
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		List<String> breadcrumbRefinement = Arrays
				.asList("lblBreadcrumbRefinement");

		final WebDriver driver = WebDriverFactory.get(browser);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[2] + " page.");
			PlpPage plppage = new PlpPage(driver).get();
			String color = plppage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("3. Selecting " + color
					+ " in the refinement under color section");
			breadcrumbText = plppage.getTextInBreadcrumb();
			categoryText.add(0, "Home");

			for (int i = 1; i < breadcrumbText.size(); i++) {
				categoryText.add(i, category[i - 1]);
			}

			Log.assertThat(Utils.compareTwoList(categoryText, breadcrumbText),
					"4. The list is '" + breadcrumbText
							+ "' is displayed in the breadcrumb.",
					"4.The list is '" + breadcrumbText
							+ "' is not displayed in the breadcrumb.");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected color '" + color
					+ "' should be displayed in breadcrumb");

			Log.assertThat(plppage.elementLayer.verifyPageElements(
					breadcrumbRefinement, plppage),
					"<b>Actual Result:</b> Selected color '" + color
							+ "' is displayed in Breadcrumb",
					"<b>Actual Result:</b> Selected color '" + color
							+ "' is not displayed in Breadcrumb", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_197

	@Test(groups = { "desktop", "tablet" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on navigation.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_206(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop")
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for "
							+ runPltfrm);
		else {
			// Loading the test data from excel using the test case id
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			ArrayList<String> breadcrumbText = new ArrayList<String>();
			ArrayList<String> categoryText = new ArrayList<String>();
			String[] category = testData.get("SearchKey").split("\\|");
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				// Navigate to L3 category
				Log.message("<br><u>Verifying L3 category navigation</u>");
				homePage.headers.navigateTo(category[0], category[1],
						category[2]);
				Log.message("2. Navigated to " + category[2] + " page.");
				PlpPage plppage = new PlpPage(driver).get();
				breadcrumbText = plppage.getTextInBreadcrumb();
				categoryText.add(0, "Home");
				for (int i = 1; i < breadcrumbText.size(); i++) {
					categoryText.add(i, category[i - 1]);
				}
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumbs should show the full path of navigation (Ex: Home > "
						+ category[0]
						+ " > "
						+ category[1]
						+ " > "
						+ category[2] + ")");
				Log.assertThat(
						Utils.compareTwoList(categoryText, breadcrumbText),
						"<b>Actual Result:</b> The breadcrumbs '"
								+ breadcrumbText
								+ "' is displayed with full path of the navigation ",
						"<b>Actual Result:</b> The breadcrumbs '"
								+ breadcrumbText
								+ "' not is displayed with full path of the navigation ",
						driver);

				// Navigate to L2 category
				Log.message("<br>");
				Log.message("<u>Verifying L2 category navigation</u>");
				plppage.headers.navigateTo(category[0], category[1]);
				Log.message("3. Navigated to " + category[1] + " page.");
				breadcrumbText = plppage.getTextInBreadcrumb();
				categoryText.clear();
				categoryText.add(0, "Home");
				for (int i = 1; i < breadcrumbText.size(); i++) {
					categoryText.add(i, category[i - 1]);
				}
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumbs should show the full path of navigation (Ex: Home > "
						+ category[0] + " > " + category[1] + ")");
				Log.assertThat(
						Utils.compareTwoList(categoryText, breadcrumbText),
						"<b>Actual Result:</b> The breadcrumbs '"
								+ breadcrumbText
								+ "' is displayed with full path of the navigation ",
						"<b>Actual Result:</b> The breadcrumbs '"
								+ breadcrumbText
								+ "' is not displayed with full path of the navigation ",
						driver);
				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_SEARCH_196

	@Test(groups = { "desktop", "tablet" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Featured'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_210(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2]);
			PlpPage plpapge = new PlpPage(driver).get();
			plpapge.selectSortByDropDownByIndex(2);
			Log.message("3. Selected sort by drop down by index value : 2");
			plpapge.getSelectedSortByValue();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Value selected in dropdown should be displayed in breadcrumb");

			Log.assertThat(
					plpapge.getSelectedSortByValue().equals(
							plpapge.getSelectedSortByValueInBreadcrumb()),
					"<b>Actual Result:</b> Selected option in dropdown 'Featured' is displayed in breadcrumb",
					"<b>Actual Result:</b> Selected option in dropdown 'Featured' is  not displayed in breadcrumb",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_210

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Best Sellers'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_212(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2]);

			PlpPage plpPage = new PlpPage(driver).get();
			plpPage.selectSortByDropDownByIndex(4);
			plpPage.selectSortByDropDownByIndex(1);
			String getmostrecentvalue = plpPage.getSelectedSortByValue();
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation");
			Log.assertThat(getmostrecentvalue.equals(plpPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown '"
							+ getmostrecentvalue
							+ "' is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown '"
							+ getmostrecentvalue
							+ "' is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_212

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Most Recent'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_213(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2]);

			PlpPage plpPage = new PlpPage(driver).get();
			plpPage.selectSortByDropDownByIndex(3);
			String getmostrecentvalue = plpPage.getSelectedSortByValue();
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation");
			Log.assertThat(getmostrecentvalue.equals(plpPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown '"
							+ getmostrecentvalue
							+ "' is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown '"
							+ getmostrecentvalue
							+ "' is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_213

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Product Listing Page - Grid View is displayed when the page is set in Business Manager.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_203(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] category = testData.get("CategoryLevel").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to Product Listing Page!");
			SearchResultPage searchresultpage = new SearchResultPage(driver);
			String Viewtype = searchresultpage.getCurrentViewType();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Product Listing Page should be displayed in"
					+ Viewtype + " View .");
			Log.assertThat(searchresultpage.getCurrentViewType() == ("grid"),
					"<b>Actual Result:</b> The Product Listing Page is displayed in "
							+ Viewtype + " View.",
					"<b>Actual Result:</b> The Product Listing Page is not displayed in "
							+ Viewtype + " View.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_SEARCH_203

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Width and Height of the product container in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_200(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category[] = testData.get("SearchKey").split("\\|");
		;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("<b>Expected Result:</b> The product listed in PlP pages should have fixed Width:216 and Height 307");

			Log.assertThat(
					plppage.verifyProductSize(),
					"<b>Actual Result:</b> The product listed in PlP pages is having fixed Width:216 and Height 307",
					"<b>Actual Result:</b> The product listed in PlP pages is not having fixed Width:216 and Height 307",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_200

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Product list slot banner is properly displayed in PLP page .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_216(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("contentBanner");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("Product list slot banner should be properly displayed in Product Listing Page .");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(element,
							searchresultPage),
					"Product list slot banner properly displayed in Product Listing Page !",
					"Product list slot banner properly nopt displayed in Product Listing Page !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_2B_216

	/*
	 * @Test(groups = { "desktop" }, dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_203(String browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String l1category = testData.get("L1_category"); String l2category =
	 * testData.get("L2_category"); String l3category =
	 * testData.get("L3_category");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(l1category, l2category, l3category);
	 * Log.message("2. Navigated to " + l1category + " >> " + l2category +
	 * " >> " + l3category); PlpPage plppage=new PlpPage(driver).get();
	 * plppage.selectSortByDropDownByIndex(3);
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> By Selecting the sort by value it should be shown in the breadcrumb"
	 * ); String selectedValue = plppage.getSelectedSortByValue();
	 * Log.assertThat( selectedValue.equals(plppage
	 * .getSelectedSortByValueInBreadcrumb()),
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is shown in the breadcrumb",
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is not shown in the breadcrumb", driver); Log.testCaseResult(); } //
	 * try catch (Exception e) { Log.exception(e, driver); } // catch finally {
	 * Log.endTestCase(); driver.quit(); } // finally } // TC_BELK_SEARCH_203
	 */

	/*
	 * 
	 * @Test(groups = { "desktop" }, dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_204(String browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String l1category = testData.get("L1_category"); String l2category =
	 * testData.get("L2_category"); String l3category =
	 * testData.get("L3_category");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(l1category, l2category, l3category);
	 * Log.message("2. Navigated to " + l1category + " >> " + l2category +
	 * " >> " + l3category); PlpPage plppage=new PlpPage(driver).get();
	 * plppage.selectSortByDropDownByIndex(2); plppage.getSelectedSortByValue();
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> By Selecting the sort by value it should be shown in the bread crumb"
	 * ); String selectedValue = plppage.getSelectedSortByValue();
	 * Log.assertThat( selectedValue.equals(plppage
	 * .getSelectedSortByValueInBreadcrumb()),
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is shown in the breadcrumb",
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is not shown in the breadcrumb", driver); Log.testCaseResult(); } //
	 * try catch (Exception e) { Log.exception(e, driver); } // catch finally {
	 * Log.endTestCase(); driver.quit(); } // finally } // TC_BELK_SEARCH_204
	 * 
	 * 
	 * 
	 * @Test(groups = { "desktop" }, dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_205(String browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String l1category = testData.get("L1_category"); String l2category =
	 * testData.get("L2_category"); String l3category =
	 * testData.get("L3_category");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(l1category, l2category, l3category);
	 * Log.message("2. Navigated to " + l1category + " >> " + l2category +
	 * " >> " + l3category); PlpPage plppage=new PlpPage(driver).get();
	 * plppage.selectSortByDropDownByIndex(4); plppage.getSelectedSortByValue();
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> By Selecting the sort by value it should be shown in the bread crumb"
	 * ); String selectedValue = plppage.getSelectedSortByValue();
	 * Log.assertThat( selectedValue.equals(plppage
	 * .getSelectedSortByValueInBreadcrumb()),
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is shown in the breadcrumb",
	 * "<b>Actual Result:</b> Selected sort by option: " + selectedValue +
	 * " is not shown in the breadcrumb", driver); Log.testCaseResult(); } //
	 * try catch (Exception e) { Log.exception(e, driver); } // catch finally {
	 * Log.endTestCase(); driver.quit(); } // finally } // TC_BELK_SEARCH_205
	 */
	/*
	 * @Test(groups = { "desktop", "mobile", "tablet" }, description =
	 * "Verify - Grid view toggle is displayed in product list page",
	 * dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_207(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName);
	 * 
	 * String l1category = testData.get("L1_category"); String l2category =
	 * testData.get("L2_category"); String l3category =
	 * testData.get("L3_category");
	 * 
	 * final WebDriver driver = WebDriverFactory.get(browser);
	 * Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(l1category, l2category, l3category);
	 * Log.message("2. Navigated to PLP of " + l1category + " >> " + l2category
	 * + " >> " + l3category);
	 * 
	 * PlpPage PlpPage = new PlpPage(driver).get();
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> Grid View toggle should displayed in PLP");
	 * 
	 * Log.assertThat( PlpPage.elementLayer.verifyPageElements(
	 * Arrays.asList("toggleGrid"), PlpPage),
	 * "<b>Actual Result:</b> Grid View toggle is displayed",
	 * "<b>Actual Result:</b> Grid View toggle is not displayed", driver);
	 * Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally } // TC_BELK_SEARCH_207
	 */
	/*
	 * @Test(groups = { "mobile" }, description =
	 * "Verify the breadcrumbs shown on Mobile.", dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_209(String browser) throws Exception { String
	 * runPltfrm = Utils.getRunPlatForm(); if (runPltfrm != "mobile") throw new
	 * SkipException(
	 * "This testcase('Back To Home' link verification) is not applicable for "
	 * + runPltfrm); else { // Loading the test data from excel using the test
	 * case id HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName); // Get the web
	 * driver instance final WebDriver driver = WebDriverFactory.get(browser);
	 * List<String> breadcrumbText = new ArrayList<String>();
	 * 
	 * String category = testData.get("SearchKey"); Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(category); CategoryLandingPage
	 * categoryLandingPage = new CategoryLandingPage(driver).get();
	 * Log.message("2. Clicked on L1 category '" + category + "' from Header");
	 * breadcrumbText = categoryLandingPage.getTextInBreadcrumb();
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> The breadcrumbs on mobile should have link to the previous level i.e., Incase of L1 Category link the breadcrumbs Is '"
	 * +breadcrumbText.get(0)+"'"); Log.assertThat(
	 * breadcrumbText.get(0).equals("Back to Home"),
	 * "<b>Actual Result:</b> The breadcrumb is displayed as '"
	 * +breadcrumbText.get(0)+"'",
	 * "<b>Actual Result:</b> The breadcrumb is not displayed as '"
	 * +breadcrumbText.get(0)+"'", driver); Log.testCaseResult(); } // try catch
	 * (Exception e) { Log.exception(e, driver); } // catch finally {
	 * Log.endTestCase(); driver.quit(); } // finally } }// TC_BELK_SEARCH_209
	 *//*
		 * @Test(groups = { "mobile" }, description =
		 * "Verify the breadcrumbs shown on Mobile.", dataProviderClass =
		 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
		 * public void TC_BELK_SEARCH_210(String browser) throws Exception {
		 * String runPltfrm = Utils.getRunPlatForm(); if (runPltfrm != "mobile")
		 * throw new SkipException(
		 * "This testcase('Back To L3/L2 category' link verification) is not applicable for "
		 * + runPltfrm); else { // Loading the test data from excel using the
		 * test case id HashMap<String, String> testData
		 * =TestDataExtractor.initTestData(workbookName, sheetName); // Get the
		 * web driver instance final WebDriver driver =
		 * WebDriverFactory.get(browser); List<String> breadcrumbText = new
		 * ArrayList<String>();
		 * 
		 * String[] category = testData.get("SearchKey").split("\\|");
		 * Log.testCaseInfo(testData);
		 * 
		 * try { HomePage homePage = new HomePage(driver, webSite).get();
		 * Log.message("1. Navigated to 'Belk' Home Page!");
		 * 
		 * // Navigate to L3 category
		 * 
		 * homePage.headers.navigateTo(category[0], category[1], category[2]);
		 * PlpPage plppage = new PlpPage(driver).get();
		 * Log.message("2. Navigated to PLP page (L3 Category)"); breadcrumbText
		 * = plppage.getTextInBreadcrumb(); Log.message("<br>"); Log.message(
		 * "<b>Expected Result:</b> The breadcrumbs on mobile should have link to the previous level"
		 * ); Log.softAssertThat( breadcrumbText.get(0).equals("Back to " +
		 * category[1]),
		 * "<b>Actual Result:</b> Breadcrumb text and last category name ('" +
		 * breadcrumbText + "' = '[Back to " + category[1] + "]') are equal!",
		 * "<b>Actual Result:</b> Breadcrumb text and last category name are not equal!"
		 * , driver);
		 * 
		 * // Navigate to L2 category Log.message("<br>");
		 * plppage.headers.navigateTo(category[0], category[1]);
		 * Log.message("3. Navigated to PLP page (L2 Category)"); breadcrumbText
		 * = plppage.getTextInBreadcrumb(); Log.message("<br>"); Log.message(
		 * "<b>Expected Result:</b> The breadcrumbs on mobile should have link to the previous level"
		 * ); Log.assertThat( breadcrumbText.get(0).equals("Back to " +
		 * category[0]),
		 * "<b>Actual Result:</b> Breadcrumb text and last category name ('" +
		 * breadcrumbText + "' = '[Back to " + category[0] + "]') are equal!",
		 * "<b>Actual Result:</b> Breadcrumb text and last category name are not equal!"
		 * , driver);
		 * 
		 * } // try catch (Exception e) { Log.exception(e, driver); } // catch
		 * finally { Log.endTestCase(); driver.quit(); } // finally } }//
		 * TC_BELK_SEARCH_210
		 */
	@Test(groups = { "mobile" }, description = "Verify the availability of 'Filter By' button in Category landing page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_221(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		if (runPltfrm != "mobile") {
			throw new SkipException(
					"This testcase(Filter By mobile) is applicable only for Mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("CategoryLevel").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[0] + " >>" + category[1]
					+ " >>" + category[2]);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Filter By' button & text should be shown with blue color");

			SearchResultPage searchpage = new SearchResultPage(driver).get();
			Log.assertThat(
					searchpage.verifyFilterByColor(),
					"<b>Actual Result:</b> 'Filter By' button & text displayed with blue color",
					"'Filter By' button & text not displayed with blue color",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_SEARCH_211

	/*
	 * @Test(groups = { "desktop", "mobile", "tablet" }, description =
	 * "Verify the availability of 'List View' toggle icon in PLP",
	 * dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_213(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName);
	 * 
	 * String l1category = testData.get("L1_category"); String l2category =
	 * testData.get("L2_category"); String l3category =
	 * testData.get("L3_category");
	 * 
	 * final WebDriver driver = WebDriverFactory.get(browser);
	 * Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(l1category, l2category, l3category);
	 * Log.message("2. Navigated to " + l1category + " >>" + l2category + " >>"
	 * + l3category);
	 * 
	 * SearchResultPage SearchResultPage = new SearchResultPage(driver) .get();
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> List View toggle icon should be displayed in PLP"
	 * );
	 * 
	 * Log.assertThat( SearchResultPage.elementLayer.verifyPageElements(
	 * Arrays.asList("toggleList"), SearchResultPage),
	 * "<b>Actual Result:</b> List View toggle icon is displayed in PLP",
	 * "<b>Actual Result:</b> List View toggle icon is not displayed  in PLP",
	 * driver);
	 * 
	 * Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally }
	 */
	@Test(groups = { "desktop" }, description = "Verify the breadcrumbs displayed in Category Landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_227(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile.");
		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		ArrayList<String> breadcrumbText = new ArrayList<String>();
		int catagoryno = 0;
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();

			Log.message("2. Navigated to PDP with Product set '"
					+ breadcrumbText + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The breadcrumbs should show the full path of navigation (Ex: Home > Women > Petite Clothing > Sweaters)");
			breadcrumbText = (ArrayList<String>) plppage.getTextInBreadcrumb();
			Log.message("<b>Actual Result:</b>");
			for (int i = 0; i < category.length; i++) {
				catagoryno = i + 1;
				Log.assertThat(
						category[i].equals(breadcrumbText.get(catagoryno)),
						" Navigated L " + catagoryno + " catagory Name : '"
								+ category[i]
								+ "' is same as in the breadcrumb path",
						" Navigated L " + catagoryno + " catagory Name : '"
								+ category[i]
								+ "' is different from breadcrumb path", driver);

			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_217

	@Test(groups = { "desktop", "mobile" }, description = "Verify Category Search refinement panel should be properly displayed in PLP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_228(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to Search Result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				Log.message("3. FilterBy button is clicked.");
			}
			String selectedCatagory = searchresultPage
					.selectCatgeoryRefinement();
			Log.message("4. Catagory is selected and the selected catagory is: "
					+ selectedCatagory);
			if (selectedCatagory != null)
				Log.message("5. Catagory is selected and the selected catagory is: "
						+ selectedCatagory);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The category search refinement panel should be properly displayed in Search result page");
			Log.assertThat(
					searchresultPage != null,
					"<b>Actual Result:</b> The category search refinement panel is properly displayed in Search result page",
					"<b>Actual Result:</b> The category search refinement panel is not properly displayed in Search result page",
					driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_218

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the functionality of List View toggle icon in Category Landing Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_224(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResult = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");

			PlpPage plppage = new PlpPage(driver).get();

			plppage.clickOnListView();

			int xLocationOfFirstProduct = searchResult
					.getCurrentLocationOfPdtByIndex(1);
			int xLocationOfThirdProduct = searchResult
					.getCurrentLocationOfPdtByIndex(3);

			Log.assertThat(
					xLocationOfFirstProduct == xLocationOfThirdProduct,
					"<b>Actual Result:</b> The product is displayed with product list view",
					"<b>Actual Result:</b> The product is not displayed with product list view",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_224

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the availability of Filter By button in Category landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_232(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();

			Log.message("<b> Expected Result 1: </b> User should see the Category Landing Slot 'cat-landing-slot-row3' in Category Landing Page and should be displayed full width row");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("ImgBanner"), plppage),
					"<b> Actual Result 1: </b> User is able to see the Category Landing Slot 'cat-landing-slot-row3' in Category Landing Page and should be displayed full width row",
					"<b> Actual Result 1: </b> User is not able to see the Category Landing Slot 'cat-landing-slot-row3' in Category Landing Page and should be displayed full width row");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_236(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException(
					"This testcase is not applicable for desktop.");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				Log.message("3. FilterBy button is clicked.");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> User should see the mobile refinement modal after tapping on 'Filter By' button");
				Log.assertThat(
						searchresultPage.elementLayer.verifyPageListElements(
								Arrays.asList("lstRefinementFilterOptions"),
								searchresultPage),
						"<b>Actual Result:</b> mobile refinement modal is present properly",
						"<b>Actual Result:</b> mobile refinement modal is present properly",
						driver);
				Log.testCaseResult();
			}
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_227

	/*
	 * @Test(groups = { "mobile" }, description =
	 * "Verify the breadcrumbs shown on Mobile.", dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_233(String browser) throws Exception {
	 * 
	 * String runPltfrm = Utils.getRunPlatForm(); if (runPltfrm != "mobile") {
	 * throw new SkipException(
	 * "This testcase(Breadcrumb verification) is applicable only for Mobile");
	 * }
	 * 
	 * HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName); String[]
	 * category = testData.get("SearchKey").split("\\|");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData); try { HomePage
	 * homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(category[0], category[1], category[2]);
	 * Log.message("2. Navigated to PLP Page"); PlpPage plppage = new
	 * PlpPage(driver).get(); Log.message("<br>"); Log.message(
	 * "<b>Expected Result-1:</b> The breadcrumbs on mobile should have link to the previous level i.e., incase of L2 ,L3 Category link the breadcrumbs Is 'Back to Petite Clothing'"
	 * ); Log.assertThat( plppage.getTextInBreadcrumb().get(0)
	 * .equals("Back to Men's Clothing"),
	 * "<b>Actual Result:</b> 'Back to Men's Clothing' is displayed properly",
	 * "<b>Actual Result:</b> 'Back to Men's Clothing' is not displayed properly"
	 * ); plppage.clickCategoryByNameInBreadcrumb("Back to Men's Clothing");
	 * Log.assertThat(
	 * plppage.getTextInBreadcrumb().get(0).equals("Back to Men"),
	 * "<b>Actual Result:</b> 'Back to Men' is displayed properly",
	 * "<b>Actual Result:</b> Back to Men' is not displayed properly");
	 * plppage.clickCategoryByNameInBreadcrumb("Back to Men"); Log.assertThat(
	 * plppage.getTextInBreadcrumb().get(0).equals("Back to Home"),
	 * "<b>Actual Result:</b> 'Back to Home' is displayed properly",
	 * "<b>Actual Result:</b> 'Back to Home' is not displayed properly",
	 * driver); plppage.clickCategoryByNameInBreadcrumb("Back to Home");
	 * Log.message("3. Back to home is clicked.");
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result-2:</b> The page should be redirected to home page");
	 * Log.assertThat( driver.getCurrentUrl().contains("home"),
	 * "<b>Actual Result-2:</b>The page is getting navigated to Home page",
	 * "<b>Actual Result-2:</b>The page is not getting navigated to Home page",
	 * driver); Log.testCaseResult(); } // try catch (Exception e) {
	 * Log.exception(e, driver); } // catch finally { Log.endTestCase();
	 * driver.quit(); } // finally } // TC_BELK_SEARCH_233
	 */

	/*
	 * @Test(groups = { "Mobile" }, description =
	 * "Verify the availability of 'Filter By' button in Category landing page."
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_234(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName);
	 * 
	 * String[] category = testData.get("CategoryLevel").split("\\|"); String
	 * runPltfrm = Utils.getRunPlatForm();
	 * 
	 * if (runPltfrm != "mobile") { throw new SkipException(
	 * "This testcase(Breadcrumb verification) is applicable only for Mobile");
	 * }
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData); try { HomePage
	 * homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.headers.navigateTo(category[0], category[1], category[2]);
	 * PlpPage plppage = new PlpPage(driver).get();
	 * Log.message("2. Navigated to PLP page", driver);
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> The FilterBy button should be displayed in blue color font with blue outline."
	 * );
	 * 
	 * Log.assertThat( plppage.verifyFilterByInMobile(),
	 * "<b>Actual Result:</b> FilterBy is displyed in blue color font with blue outline "
	 * ,
	 * "<b>Actual Result:</b> FilterBy is not displyed in blue color font with blue outline"
	 * ); Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally
	 * 
	 * }// TC_BELK_SEARCH_234
	 *//*
		 * @Test(groups = { "mobile" }, description =
		 * "Verify - clicking on'Filter By' button in Category landing page.",
		 * dataProviderClass = DataProviderUtils.class, dataProvider =
		 * "parallelTestDataProvider") public void TC_BELK_SEARCH_235(String
		 * browser) throws Exception { String runPltfrm =
		 * Utils.getRunPlatForm(); if (runPltfrm != "mobile") { throw new
		 * SkipException(
		 * "This testcase(Breadcrumb verification) is applicable only for Mobile"
		 * ); }
		 * 
		 * HashMap<String, String> testData
		 * =TestDataExtractor.initTestData(workbookName, sheetName);
		 * 
		 * String l1category = testData.get("L1_category"); String l2category =
		 * testData.get("L2_category"); String l3category =
		 * testData.get("L3_category");
		 * 
		 * final WebDriver driver = WebDriverFactory.get(browser);
		 * Log.testCaseInfo(testData);
		 * 
		 * try {
		 * 
		 * HomePage homePage = new HomePage(driver, webSite).get();
		 * Log.message("1. Navigated to 'Belk' Home Page!");
		 * homePage.headers.navigateTo(l1category, l2category, l3category);
		 * Log.message("2. Navigated to " + l1category + " >>" + l2category +
		 * " >>" + l3category);
		 * 
		 * PlpPage plpPage = new PlpPage( driver).get();
		 * plpPage.clickFilterByInMobile();
		 * Log.message("3. Clicked on 'FilterBy' button ");
		 * 
		 * Log.message("<br>"); Log.message(
		 * "<b>Expected Result:</b> Mobile refinement modal should be dispalyed when clicking on 'Filter By' button "
		 * );
		 * 
		 * Log.assertThat( plpPage.elementLayer.verifyPageListElements(
		 * Arrays.asList("lstRefinementFilterOptionsInMobile"), plpPage),
		 * "<b>Actual Result:</b> Refinement is displayed when clicking on 'Filter By' button"
		 * ,
		 * "<b>Actual Result:</b> Refinement is not displayed when clicking on 'Filter By' button"
		 * , driver);
		 * 
		 * Log.testCaseResult();
		 * 
		 * } // try catch (Exception e) { Log.exception(e, driver); } // catch
		 * finally { Log.endTestCase(); driver.quit(); } // finally
		 * 
		 * } // TC_BELK_SEARCH_235
		 */
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Shop by Brand page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_256(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.footers.scrollToFooter();
			ListOfBrandPage lbppage = homePage.footers.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message("<br>");
			if (runPltfrm == "desktop") {
				Log.message("<b>Expected Result 1:</b> Desktop - Breadcrumb reads 'Home > Shop by Brand");
				Log.assertThat(
						lbppage.VerifyCategoryNameInBreadcrumb(),
						"<b>Actual Result1 :</b>  Breadcrumb 'Home < Shop by Brand' in List of brand page is displayed properly",
						"<b>Actual Result 1:</b>  Breadcrumb 'Home < Shop by Brand' in List of brand page is not displayed properly",
						driver);
			} else if (runPltfrm == "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b> Mobile - Breadcrumbs reads 'Back to Home'");
				Log.assertThat(
						lbppage.VerifyCategoryNameInBreadcrumb(),
						"<b>Actual Result1 :</b>  Breadcrumb 'Back to Home' in List of brand page is displayed properly",
						"<b>Actual Result 1:</b>  Breadcrumb 'Back to Home' in List of brand page is not displayed properly",
						driver);
			}

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> 'Shop by Brand' banner slot above the brands displayed.");
			Log.assertThat(
					lbppage.elementLayer.verifyPageElements(
							Arrays.asList("txtShopByBrandBanner"), lbppage),
					"<b>Actual Result 2:</b>  Shop by brand banner is displayed properly",
					"<b>Actual Result 2:</b>  Shop by brand banner is not displayed,driver",
					driver);
			if (runPltfrm == "mobile") {
				lbppage.clickFilterByInMobile();
			}
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Left Navigation Panel with Category Refinement list should dispalayed for L1 level category name.");
			Log.assertThat(
					lbppage.elementLayer.verifyPageListElements(
							Arrays.asList("fldCatagoryRefinement"), lbppage),
					"<b>Actual Result 3:</b> Catagory refinement list is displayed properly",
					"<b>Actual Result 3:</b> Catagory refinement list is not displayed properly",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_246

	/*
	 * @Test(groups = { "desktop", "mobile", "tablet" }, description =
	 * "Verify - 'Find a Brand' Text box in List of Brands page",
	 * dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_248(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData
	 * =TestDataExtractor.initTestData(workbookName, sheetName);
	 * 
	 * final WebDriver driver = WebDriverFactory.get(browser);
	 * Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * 
	 * ListOfBrandPage listofBrand = homePage.footers .navigateToListBrand();
	 * Log.message(
	 * "2. Clicked on 'List Of Brands' link and navigated to List of Brands page"
	 * );
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> 'Enter brand name' should be displayed in the text field"
	 * );
	 * 
	 * Log.assertThat( listofBrand.verifyTxtEnterBrand(),
	 * "<b>Actual Result:</b> 'Enter brand name' is displayed in the text field"
	 * ,
	 * "<b>Actual Result:</b> 'Enter brand name' is not displayed in the text field"
	 * , driver);
	 * 
	 * Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally } // TC_BELK_SEARCH_248
	 */

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selecting sizes in Size refinement under product refinements panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_152(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> toogleActive = Arrays.asList("toogleActivelnk");
		int countBeforeRefine;
		int countAfterRefine;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// Get the product count before refine
			countBeforeRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("3. Before refinement product count is :"
					+ countBeforeRefine, driver);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			String Size = searchresultPage
					.selectUnselectFilterOptions("refinementSize");
			Log.message("4. Filtered the products by color :" + Size, driver);
			// Get the product count after refine
			countAfterRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("5. After refinement product count is :"
					+ countAfterRefine);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("The size refinement option should displays an orange checkbox next to the item indicating that the size has been selected.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							toogleActive, searchresultPage),
					"The Color refinement option displays an orange checkbox next to the item indicating that the color has been selected !",
					"The Color refinement option not displays an orange checkbox next to the item indicating that the color has been selected !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(" After selecting an available size, the product grid should refresh to show the products that match the selected size refinement.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					countBeforeRefine > countAfterRefine,
					"After selecting an available color, the product grid refreshed to show the products that match the selected color refinement !",
					"After selecting an available color, the product grid not refreshed to show the products that match the selected color refinement !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_152

	@Test(groups = { "desktop" }, description = " Verify that the Shop by Brand page - Search Box - Invalid Input.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_259(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String brandName = testData.get("BrandName");
		List<String> alertMsg = Arrays.asList("alertMsg");
		String txtAlertMsg = "We’re sorry, that brand was not found. Please check your spelling and try again.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Searching invalid Brand name in the list of brands page, It should shown the alert message in Red(#971D22) color");

			listOfBrandPage.searchBrand(brandName);
			Log.assertThat(
					listOfBrandPage.elementLayer.verifyPageElements(alertMsg,
							listOfBrandPage),
					"<b>Actual Result:</b> Searching invalid Brand name in the list of brands page, It  shown the alert message ",
					"<b>Actual Result:</b> Searching invalid Brand name in the list of brands page, It is not shown the alert message ");
			String txtalert = listOfBrandPage.getTextFromAlertMsg();
			Log.assertThat(txtalert.equals(txtAlertMsg),
					"<b>Actual Result:</b> Showing the alert error message as '"
							+ txtAlertMsg + "'",
					"<b>Actual Result:</b> The alert error message is not shown as '"
							+ txtAlertMsg + "'", driver);
			Log.assertThat(
					listOfBrandPage.verifyColorOfAlertMsg(),
					"<b>Actual Result:</b> The alert message in Red(#971D22) color",
					"<b>Actual Result:</b> The alert message in Red(#971D22) color",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_249

	@Test(groups = { "desktop" }, description = " Verify that the Shop by Brand page - Search Box - valid Input.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_260(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String brandName = testData.get("BrandName");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");

			SearchResultPage searchresultPage = listOfBrandPage
					.searchBrand(brandName);
			Log.message("3. Search '" + brandName
					+ " in ListOfbrands Page and Navigate to SearchResultPage");

			String BrandNameFromBreadCrumb = searchresultPage
					.getTextFromSearchResult().split("\"")[1];
			Log.message("4. Fetching brand name from Breadcrumb: "
					+ BrandNameFromBreadCrumb);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Searching the Brand name in the list of brands page, It should navigate to corresponding search result page");

			Log.assertThat(
					brandName.equals(BrandNameFromBreadCrumb.replace("\"", "")),
					"<b>Actual Result:</b> Brand Name:"
							+ BrandNameFromBreadCrumb.replace("\"", "")
							+ " is shown in the search result page",
					"<b>Actual Result:</b> Brand Name: "
							+ BrandNameFromBreadCrumb.replace("\"", "")
							+ " is not shown in the search result page", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_250

	@Test(groups = { "desktop" }, description = "Verify the Brand Accordion section for expand in Shop by Brand page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_262(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Brand toggle should be expanded in the List of brand page");

			Log.assertThat(
					listOfBrandPage.VerifyBrandToggle(),
					"<b>Actual Result:</b> Brand toggle is expand in the List of brand page",
					"<b>Actual Result:</b> Brand toggle is not  expand in the List of brand page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_252

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the mobile Filter By button is displayed for Clearance Landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_252(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */
		if (runPltfrm == "desktop") {
			throw new SkipException(
					"This test case is not applicable for desktop");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(searchKey);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ searchKey);
			SearchResultPage searchResult = new SearchResultPage(driver).get();
			searchResult.clickOnBtnWomen();
			Log.message("3.Clicked on Women");
			Log.message("<b>Expected Result:</b>	The mobile Filter By button should be displayed for Clearance Landing page and the button is in blue color");

			Log.assertThat(
					searchResult.verifyColorForFilterByMobile(),
					"<b>Actual Result:</b>	The mobile Filter By button is displayed for Clearance Landing page and the button is in blue color",
					"<b>Actual Result:</b>	Either the mobile Filter By button is not displayed for Clearance Landing page or the button is not displayed in blue color",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_252

	@Test(groups = { "mobile" }, description = "Verify the Clearance Landing page breadcrumbs.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_253(String browser) throws Exception {
		if (Utils.getRunPlatForm() != "mobile") {
			throw new SkipException(
					"This testcase is applicable for 'Mobile' only");
		} else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				ClearancePage clearancePage = homePage.headers
						.navigateToClearancePage();
				Log.message("2. Navigated to Clearance Page");
				List<String> breadcrumlist = clearancePage
						.getTextInBreadcrumb();
				Log.message("<b>Expected Result:</b> User should see the breadcrumbs 'BackToHome'.");
				Log.assertThat(
						breadcrumlist.get(0).equals("Back to Home"),
						"<b>Actual Result:</b>  Breadcrumb text is 'BackToHome'",
						"<b>Actual Result:</b>  Breadcrumb text is not 'BackToHome'",
						driver);
				Log.testCaseResult();
			}// try
			catch (Exception e) {
				Log.exception(e, driver);
			}// catch
			finally {
				Log.endTestCase();
				driver.quit();
			}// finally
		}
	}// TC_BELK_SEARCH_253

	@Test(groups = { "desktop" }, description = "Verify the Clearance Landing page breadcrumbs.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_254(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase is applicable for 'Desktop' only");
		} else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				ClearancePage clearancePage = homePage.headers
						.navigateToClearancePage();
				Log.message("2. Navigated to Clearance Page");
				List<String> breadcrumlist = clearancePage
						.getTextInBreadcrumb();
				Log.message("<b>Expected Result:</b> User should see the breadcrumbs 'Home > Clearance'.");
				Log.assertThat(
						breadcrumlist.get(0).equals("Home")
								& breadcrumlist.get(1).equals("Clearance"),
						"<b>Actual Result:</b>  Breadcrumb text is 'Home > Clearance'",
						"<b>Actual Result:</b>  Breadcrumb text is not 'Home > Clearance'",
						driver);
				Log.testCaseResult();
			}// try
			catch (Exception e) {
				Log.exception(e, driver);
			}// catch
			finally {
				Log.endTestCase();
				driver.quit();
			}// finally
		}
	}// TC_BELK_SEARCH_254

	@Test(groups = { "desktop" }, description = " Verify the Brand Accordion section for collapse in Shop by Brand page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_263(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Brand toggle should be collapsed  in the List of brand page");

			Log.assertThat(
					listOfBrandPage.VerifyBrandToggle(),
					"<b>Actual Result:</b> Brand toggle is collapse in the List of brand page",
					"<b>Actual Result:</b> Brand toggle is not  collapse in the List of brand page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_253

	@Test(groups = { "desktop", "mobile", "tablet" }, description = " Verify that the Brand Accordion section - greater than 12 values in Shop by Brand page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_264(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			if (Utils.getRunPlatForm() == "desktop") {
				int count = listOfBrandPage.checkBrandColumnsCount();
				Log.message("3. The  Brand Columns count in desktop is: "
						+ count);
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> By Expanding the Brand, Brand List should shown in '3' columns");

				Log.assertThat(
						count == 3,
						"<b>Actual Result:</b> Brand List is shown in the '3' Columns",
						"<b>Actual Result:</b> Brand List is not shown in the '3' Columns",
						driver);
			}
			if (Utils.getRunPlatForm() == "mobile") {
				int count = listOfBrandPage.checkBrandColumnsCount();
				Log.message("3. The  Brand Columns count in desktop is: "
						+ count);
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> By Expanding the Brand, Brand List should shown in '1' columns");
				Log.assertThat(
						count == 0,
						"<b>Actual Result:</b> Brand List is shown in the '1' Columns",
						"<b>Actual Result:</b> Brand List is not shown in the '1' Columns",
						driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_254

	@Test(groups = { "desktop" }, description = "Verify that user is navigated to Category Landing Page for configured value.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_265(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");

			String selected_Category = listOfBrandPage.selectCategoryByIndex(5)
					.split("\\(")[0];
			Log.message("3. Select Category '"
					+ selected_Category
					+ "' in ListOfBrandsPage and Navigate to Category Landing Page");

			CategoryLandingPage clp = new CategoryLandingPage(driver);
			ArrayList<String> categoryName = clp.getTextInBreadcrumb();
			Log.message("4. Fetching CategoryName From BreadCrumb:"
					+ categoryName.get(1) + "in Category Landing Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>By Selecting category refinement it should navigate to corresponding 'Category landing' page ");

			Log.assertThat(selected_Category.equals(categoryName.get(1)),
					"<b>Actual Result:</b> Selected Category: "
							+ selected_Category
							+ " is navigate to category landing page",
					"<b>Actual Result:</b> Selected Category: "
							+ selected_Category
							+ " is not navigate to category landing page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_255

	@Test(groups = { "desktop" }, description = "Verify that user is navigated to Search Results Page for non-configured value.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_266(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");

			String selectedBrandName = listOfBrandPage.selectBrand();
			Log.message("3. Selected the Brand Name From Options:"
					+ selectedBrandName);

			SearchResultPage searchresultpage = new SearchResultPage(driver);
			ArrayList<String> brandNameFromBC = searchresultpage
					.getBreadCrumbLastValue();
			Log.message("4. Fetching BrandName From BreadCrumb: "
					+ brandNameFromBC.get(0));

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>By Selecting Brand in the List of brand page , it should navigate to corresponding 'search result' page ");

			Log.assertThat((selectedBrandName.replace(" ", ""))
					.equals(brandNameFromBC.get(0).replace(" ", "")),
					"<b>Actual Result:</b> Selected Brand: "
							+ selectedBrandName.replace(" ", "")
							+ " is shown in the Search result page",
					"<b>Actual Result:</b> Selected Brand: "
							+ selectedBrandName.replace(" ", "")
							+ " is not shown in the Search result page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_256

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the 'Back to Top' link is displayed in the Shop by Brand page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_267(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listofBrand = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Back To top link should be displayed in Brands Page");
			Log.assertThat(
					listofBrand.elementLayer.verifyPageElements(
							Arrays.asList("lblBackToTop"), listofBrand),
					"<b>Actual Result:</b> 'Back To top' link is displayed in Brands Page",
					"<b>Actual Result:</b> 'Back To top' link is not displayed in Brands Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_257

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify -  Clicking the 'Back to Top' link in Brands page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_268(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			ListOfBrandPage listofBrand = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");

			listofBrand.clickBackToToplink();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			Long backToTopBrandPage = (Long) executor
					.executeScript("return window.pageYOffset");
			Log.message("<br>");

			Log.message("<b>Expected Result:</b> Clicking on 'Back to Top' link should scroll up the page");

			String brandBackToTop = Long.toString(backToTopBrandPage);

			Log.assertThat(
					brandBackToTop.equals("0"),
					"<b>Actual Result:</b> Page is scrolled up when clicking on 'Back to Top' link in Brands Page",
					"<b>Actual Result:</b> Page is not scrolled up when clicking on 'Back to Top' link in Brands Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_258

	@Test(groups = { "mobile" }, description = "Verify that the Shop by Brand page - Mobile - 'Filter by' Button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_269(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException(
					"This testcase is only applicable for mobile");

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listofBrand = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			listofBrand.clickFilterBy();
			Log.message("3. Filter By button is clicked");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Mobile refinement should be displayed when clicking on 'Filter By' button ");

			Log.assertThat(
					listofBrand.elementLayer.verifyPageListElements(
							Arrays.asList("lstLeftNavRefinement"), listofBrand),
					"<b>Actual Result:</b> Mobile Refinement is displayed when clicking on 'Filter By' button",
					"<b>Actual Result:</b> Mobile Refinement is not displayed when clicking on 'Filter By' button",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_269

	/*
	 * @Test(groups = { "desktop" }, description =
	 * "Verify Home text in the bread crumb", dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_260(String browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName); String searchKey = testData.get("SearchKey");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * 
	 * SearchResultPage searchresultPage = homePage
	 * .clickOnSearchSuggestions(searchKey);
	 * Log.message("2. Clicked the search keyword from 'Search for \"" +
	 * searchKey + " \" ?' in the auto suggestion");
	 * 
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected result: </b> Home text should display one time in the breadcrumb"
	 * );
	 * 
	 * Log.assertThat( (searchresultPage.verifyBreadCrumbHomeCount() == 1),
	 * "<b>Actual result: </b> Home is getting displayed one time in the breadcrumb"
	 * ,
	 * "<b>Actual result: </b> Home is getting displayed twice in the breadcrumb"
	 * , driver);
	 * 
	 * Log.testCaseResult();
	 * 
	 * } // try catch (Exception e) { Log.exception(e, driver); } // catch
	 * finally { Log.endTestCase(); driver.quit(); } // finally
	 * 
	 * }// TC_BELK_SEARCH_260
	 */

	/*
	 * 
	 * @Test(groups = { "desktop", "mobile" }, description =
	 * "Verify the Enhanced Search Suggestion options is navigating to Search result page"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_264(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName); String searchKey = testData.get("SearchKey");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!"); SearchResultPage
	 * searchresultpage = homePage .searchProductKeyword(searchKey);
	 * Log.message("2. Entered the keyword '" + searchKey +
	 * "' in the search box"); Log.assertThat(
	 * searchresultpage.elementLayer.verifyPageElements(
	 * Arrays.asList("lnkSearchData"), searchresultpage),
	 * "3. The search data is displyed with 'Did you mean'",
	 * "3. The search data is not displayed with 'Did you mean'"); String
	 * urlBeforeClickSeachData = driver.getCurrentUrl();
	 * searchresultpage.clickOnSearchData();
	 * Log.message("4. The system try to click the search data '" + searchKey +
	 * "'"); String urlAfterClickSearchData = driver.getCurrentUrl();
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> The search data should be clickable and the page should redirected to search result page when click on it."
	 * ); Log.assertThat(
	 * !(urlBeforeClickSeachData.equals(urlAfterClickSearchData)),
	 * "<b>Actual Result:</b> The search data is clicked and the page is redirected to search result page."
	 * ,
	 * "<b>Actual Result:</b> In the search data couldn't click the 'jeans' from Did you mean \"jeans\"?"
	 * , driver); Log.testCaseResult(); } // try catch (Exception e) {
	 * Log.exception(e, driver); } // catch finally { Log.endTestCase();
	 * driver.quit(); } // finally
	 * 
	 * }// TC_BELK_SEARCH_264
	 */

	/*
	 * @Test(groups = { "desktop", "mobile" }, description =
	 * "Verify the Brand link with Special Symbol in Search suggestion list is navigating to right page"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_265(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName); String searchKey = testData.get("SearchKey");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try {
	 * 
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * homePage.enterSearchTextBox(searchKey);
	 * Log.message("2. Entered the keyword '" + searchKey +
	 * "' in the search box");
	 * homePage.selectSearchSuggestionProductWithIndex(2);
	 * Log.message("3. Selected the product which contain the special character."
	 * ); SearchResultPage searchresultpage = new SearchResultPage(driver)
	 * .get(); Log.message("4. The page is navigated to Search result page");
	 * Log.message("<br>"); Log.message(
	 * "<b>Expected Result:</b> The search result page should display without any 'did you mean' sentence and it should navigated to right page"
	 * ); Log.assertThat(
	 * searchresultpage.elementLayer.verifyPageElementsDoNotExist(
	 * Arrays.asList("lblCorrectedSearchtxt"), searchresultpage),
	 * "<b>Actual Result:</b> The search result page is displayed without 'Did you mean' sentence and it is navigated to right page"
	 * ,
	 * "<b>Actual Result:</b> The search result page is displayed with 'Did you mean' sentence and it is not navigated to right page"
	 * , driver); Log.testCaseResult(); } // try catch (Exception e) {
	 * Log.exception(e, driver); } // catch finally { Log.endTestCase();
	 * driver.quit(); } // finally
	 * 
	 * }// TC_BELK_SEARCH_265
	 */
	@Test(groups = { "desktop", "mobile" }, description = "Check that the link is highlighted in the Search Suggestion pop-up screen upon hover.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_015(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Entered search keyword '" + searchKey
					+ "' in the search box");
			Log.message("<b>Expected Result:</b> Hovering on any link shown under 'Looking for these pages'/'Categories'/'Pages that might be interesting should be Hyperlinks");
			Log.assertThat(
					homePage.verifyHyperLinkforLinksFromSearchPopupPane(),
					"<b>Actual Result:</b> Hovering on links shown under 'Looking for these pages'/'Categories'/'Pages that might be interesting are Hyperlinks.",
					"<b>Actual Result:</b> Hovering on links shown under 'Looking for these pages'/'Categories'/'Pages that might be interesting are not Hyperlinks.",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_015

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Check to click on a entry from 'Brands' list in Search Suggestions Pop-up screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_016(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String contentheading = "buyingshoes2";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Pagecontent searchresultPagecontent = homePage
					.searchProductWithAutoSuggestionPageContent(searchKey);

			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> Clicking on the link should take me to 'Shoes' Content page");
			Log.assertThat(
					searchresultPagecontent.getPageContent().contains(
							contentheading),
					"<b> Actual Result: </b> Buying shoes 2 heading is displayed in the breadcrumb",
					"<b> Actual Result: </b> Buying shoes 2 heading is not displayed in the breadcrumb",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_016

	/*
	 * @Test(groups = { "desktop"}, description =
	 * "Verify system displays the following under the refinements",
	 * dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_110(String
	 * browser) throws Exception { String runPltfrm = Utils.getRunPlatForm(); if
	 * (runPltfrm == "mobile") { throw new SkipException(
	 * "This test case is not applicable for mobile"); }
	 * 
	 * HashMap<String, String> testData =
	 * TestDataExtractor.initTestData(workbookName, sheetName); String searchKey
	 * =testData.get("SearchKey");
	 * 
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData); List<String>
	 * filterbyheading= Arrays.asList("sectionFilterBy"); List<String>
	 * categoryrefinemnetoption= Arrays.asList("btnsize","lblBrand"); try {
	 * HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!"); SearchResultPage
	 * searchresultPage=homePage.searchProductKeyword(searchKey);
	 * Log.message("2. Navigated to "+searchKey+" search result Page!");
	 * 
	 * Log.message(
	 * "<b> Expected Result: </b> 'Refine by' must be removed from the refinement and 'Filter by' option should get display on the refinement."
	 * ); Log.assertThat(searchresultPage.elementLayer.verifyPageElements(
	 * filterbyheading, searchresultPage),
	 * "<b> Actual Result: </b> FilterBy heading is displayed on the left Nav.and RefineBy options is not displayed"
	 * ,
	 * "<b> Actual Result: </b> FilterBy heading is not displayed on the left Nav. and RefineBy option is displayed on the Left Nav. "
	 * ,driver); Log.message(
	 * "<b> Expected Result: </b> Category refinement should get display on the page."
	 * );
	 * 
	 * Log.assertThat(searchresultPage.elementLayer.verifyPageElements(
	 * categoryrefinemnetoption, searchresultPage),
	 * "<b> Actual Result: </b> CategoryRefinemnet heading is displayed on the left Nav."
	 * ,
	 * "<b> Actual Result: </b> CategoryRefinemnet is not displayed on the left Nav. "
	 * ,driver);
	 * 
	 * Log.testCaseResult();
	 * 
	 * }// try catch (Exception e) { Log.exception(e, driver); }// catch finally
	 * { Log.endTestCase(); driver.quit(); }// finally
	 * 
	 * }// TC_BELK_SEARCH_110
	 */
	/*
	 * @Test(groups = { "desktop"}, description =
	 * "Verify 'View More' link appear after “n” number of value",
	 * dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_111(String
	 * browser) throws Exception { String runPltfrm = Utils.getRunPlatForm(); if
	 * (runPltfrm == "mobile") { throw new SkipException(
	 * "This test case is applicable only for mobile"); }
	 * 
	 * HashMap<String, String> testData =
	 * TestDataExtractor.initTestData(workbookName, sheetName); String searchKey
	 * =testData.get("SearchKey");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * List<String> brandviewmoreoption= Arrays.asList("btnBrandViewMore"); try
	 * { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!"); SearchResultPage
	 * searchresultPage=homePage.searchProductKeyword(searchKey);
	 * Log.message("2. Navigated to "+searchKey+" search result Page!");
	 * Log.message
	 * ("<b> Expected Result: </b> System should display the 'View More' link."
	 * ); Log.assertThat(searchresultPage.elementLayer.verifyPageElements(
	 * brandviewmoreoption, searchresultPage),
	 * "<b> Actual Result: </b> ViewMore option is displayed on Left Nav.",
	 * "<b> Actual Result: </b> ViewMore option is not displayed on Left Nav."
	 * ,driver);
	 * 
	 * Log.testCaseResult();
	 * 
	 * }// try catch (Exception e) { Log.exception(e, driver); }// catch finally
	 * { Log.endTestCase(); driver.quit(); }// finally
	 * 
	 * }// TC_BELK_SEARCH_111
	 */
	/*
	 * @Test(groups = { "desktop"}, description =
	 * "Verify system collapse back to the configured number for that refinement, when customer click on the 'View Link'"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_124(String
	 * browser) throws Exception { String runPltfrm = Utils.getRunPlatForm(); if
	 * (runPltfrm == "mobile") { throw new SkipException(
	 * "This test case is not applicable for mobile"); }
	 * 
	 * HashMap<String, String> testData =
	 * TestDataExtractor.initTestData(workbookName, sheetName); String searchKey
	 * =testData.get("SearchKey");
	 * 
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!"); SearchResultPage
	 * searchresultPage=homePage.searchProductKeyword(searchKey);
	 * Log.message("2. Navigated to "+searchKey+" search result Page!");
	 * searchresultPage.clickViewMoreOrLessLnk("Color"); Log.message(
	 * "3. Click on View More Link under color refinement value in Left Nav.");
	 * searchresultPage.verifyExpandToggle("Color");
	 * Log.message("4. Color panel is in expanded form");
	 * searchresultPage.clickViewMoreOrLessLnk("Color"); Log.message(
	 * "<b> Expected Result: </b> System should collapse back to the configured number for that refinement."
	 * ); Log.message(
	 * "<b> Actual Result: </b> Click on View Less Link under color refinement value in Left Nav."
	 * ,driver);
	 * 
	 * Log.testCaseResult();
	 * 
	 * }// try catch (Exception e) { Log.exception(e, driver); }// catch finally
	 * { Log.endTestCase(); driver.quit(); }// finally
	 * 
	 * }// TC_BELK_SEARCH_113
	 */
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable sizes in Size refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_151(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();
			}

			String size = searchresultPage
					.selectUnselectFilterOptions("refinementSize");
			Log.message("3. Selecting " + size
					+ " in the refinement under color section");
			Log.message("<b> Expected Result: </b> Selectable sizes should be displayed as a checkbox list with the size number or size value.");
			String[] selectedSizeCount = size.split("\\(");
			Log.message("4. Selected size name :" + selectedSizeCount[0]
					+ " and size count" + selectedSizeCount[1]
					+ " in the refinement under size section");
			Log.message(
					"<b> Actual Result: </b> Selected size is displayed as a checkbox list with the size number or size value.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_140

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Color under product refinements panel when a specific color is not available for the products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_156(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<b>Expected Result:</b> Specific color is not available for the products, the Color should not be displayed as a possible refinement under product refinements panel. ");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("fldColorRefinement"),
							searchResultPage),
					"<b>Actual Result:</b> Specific color is not available for the products, the Color is not be displayed as a possible refinement under product refinements panel.",
					"<b>Actual Result:</b> Specific color is not available for the products, the Color is displayed as a possible refinement under product refinements panel.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_156

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Color Refinement Heading under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_157(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String searchTextBox = "Your search results for \"" + searchKey + "\"";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> colorheading = Arrays.asList("fldColorHeading");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			Log.assertThat(
					searchresultPage.getTextFromSearchResult().contains(
							searchTextBox), searchTextBox
							+ " item is display in the search result page",
					searchTextBox
							+ " item is not display in the search result page");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();

			}

			searchresultPage.verifyRefinement("Color");
			Log.message("<b> Expected Result: </b> The refinement heading 'Color' should be displayed bold in blue color.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							colorheading, searchresultPage),
					"<b> Actual Result: </b> Colour Heading is displayed in the Let Nav.",
					"<b> Actual Result: </b> Colour heading is not  displayed in the Let Nav",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_146

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Top Rated'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_211(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is not applicable for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1]);

			PlpPage plpPage = new PlpPage(driver).get();
			plpPage.selectSortByDropDownByIndex(4);
			String getmostrecentvalue = plpPage.getSelectedSortByValue();
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation");
			Log.assertThat(getmostrecentvalue.equals(plpPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown "
							+ getmostrecentvalue
							+ " is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown "
							+ getmostrecentvalue
							+ " is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_201

	@Test(groups = { "desktop" }, description = "Verify that the breadcrumbs displays the full path of the hierarchy based on sort results by 'Most Recent'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_199(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1]);
			CategoryLandingPage clPage = new CategoryLandingPage(driver).get();

			clPage.selectSortByDropDownByIndex(3);
			String getmostrecentvalue = clPage.getSelectedSortByValue();
			Log.message("<b> Expected Result: </b> The breadcrumbs should show the full path of navigation ");
			Log.assertThat(getmostrecentvalue.equals(clPage
					.getSelectedSortByValueInBreadcrumb()),
					"<b> Actual Result: </b> Selected option in dropdown  "
							+ getmostrecentvalue
							+ " is displayed in breadcrumb",
					"<b> Actual Result: </b> Selected option in dropdown  "
							+ getmostrecentvalue
							+ " is  not displayed in breadcrumb");
			Log.message("3. Drop down option is selected ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_199

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Badge Styling in Product Tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_202(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> verifyproductbadge = Arrays.asList("txtProductBadge");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with the  '" + searchKey
					+ "' in search text box and navigated to 'PDP'  Page!");
			Log.message("<b> Expected Result: </b> Product badges should be displayed in different style based on the badge type, color and text.");
			Log.assertThat(
					pdpPage.verifyBadgeRefinementText(),
					"<b> Actual Result: </b> The Badge text is present on the Product tile with the style based",
					"<b> Actual Result: </b> The Badge text is not present on the Product tile ",
					driver);
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(verifyproductbadge,
							pdpPage),
					"<b> Actual Result: </b> Product Badging is present on the Product title in the SRP page",
					"<b> Actual Result: </b>  Product Badging is not present on the Poduct title in the SRP page",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_192

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Brands Refinement Heading under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_166(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> brandheading = Arrays.asList("lblBrand");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (runPltfrm == "mobile") {

				searchresultPage.clickFilterByInMobile();

			}

			Log.message("<b> Expected Result: </b> The refinement heading 'Brands' should be displayed bold in blue color.");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							brandheading, searchresultPage),
					"<b> Actual Result: </b> Brand heading is displayed on Left Nav.",
					"<b> Actual Result: </b> Brand heading  is not displayed on Left Nav.",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_155

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable colors in Color refinement under product refinements panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_160(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");

			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();

			}

			String color = searchresultPage
					.selectUnselectFilterOptions("refinementColor");
			Log.message("3. Selecting " + color
					+ " in the refinement under color section");

			String[] selectedColorCount = color.split("\\(");
			Log.message("<b> Expected Result: </b> Selectable colors should be displayed as a checkbox list with the Color Name, Color Swatch, and Refinement Count");
			Log.message("<b> Actual Result: </b> Selected color name :"
					+ selectedColorCount[0] + " and color count"
					+ selectedColorCount[1]
					+ " in the refinement under color section", driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_149

	@Test(groups = { "desktop" }, description = "Verify Quick View icon on the product thumbnail in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_198(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			String searchKey = testData.get("SearchKey");
			List<String> quickViewIcon = Arrays.asList("btnQuickView");
			List<String> quickViewPopUp = Arrays.asList("quickViewPopUp");

			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				// Load HomePage
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page !");
				SearchResultPage searchresultPage = homePage
						.searchProductKeyword(searchKey);
				Log.message("2. Searched with the keyword '" + searchKey
						+ "' and navigated to SearchResultPage !");
				// MouseHover On QuickView Link
				searchresultPage.hoverOnQuickView();
				Log.message("3. MouseHover on QuickView Link !");
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b>");
				Log.message("When a customer hovers over a product thumbnail, the Quick View icon should displays in center of the product tile.");
				Log.message("<br>");
				Log.message("<b>Actual Result 1:</b>");
				Log.assertThat(
						searchresultPage.elementLayer.verifyPageElements(
								quickViewIcon, searchresultPage),
						"When a customer hovers over a product thumbnail, the Quick View icon displayed in center of the product tile !",
						"When a customer hovers over a product thumbnail, the Quick View icon not displayed in center of the product tile !",
						driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b>");
				Log.message("When the customer clicks the quick view icon, it should open the Quick View modal window.");
				Log.message("<br>");
				Log.message("<b>Actual Result 2:</b>");
				QuickViewPage QuickViewPage = searchresultPage
						.navigateToQuickView();
				Log.message("4. Clicked on QuickView Link and Navigated to QuickViewPage !");
				Log.assertThat(
						QuickViewPage.elementLayer.verifyPageElements(
								quickViewPopUp, QuickViewPage),
						"When the customer clicks the quick view icon, it opens the Quick View modal window !",
						"When the customer clicks the quick view icon, it didn't opens the Quick View modal window !",
						driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 3:</b>");
				Log.message("User should see a 'Added Successfully to Bag' Mini-Cart hover after tapping on 'Add to Bag' on Quick View.");
				Log.message("<br>");
				Log.message("<b>Actual Result 3:</b>");
				QuickViewPage.addProductToBag();
				Log.message("5. Product Added to my Bag !");
				String ExpectedSucessMsg = QuickViewPage.getSuccessMsg();
				String ActualSucessMsg = "Added to Shopping Bag";
				Log.assertThat(
						ExpectedSucessMsg.equals(ActualSucessMsg),
						"'Added Successfully to Bag' displayed after tapping on 'Add to Bag' on Quick View !",
						"'Added Successfully to Bag' not displayed after tapping on 'Add to Bag' on Quick View !",
						driver);
				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			}
		} // finally
	} // TC_BELK_SEARCH_198

	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_237(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			if (Utils.getRunPlatForm() == "mobile") {

				categoryLandingPage.clickFilterByInMobile();
				Log.message("3. Clicked on 'FilterBy' button");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> After tapping on 'Filter By' button, the mobile refinement modal should be displyed");

				Log.assertThat(
						categoryLandingPage.elementLayer.verifyPageElements(
								Arrays.asList("leftNavPanel"),
								categoryLandingPage),
						"<b>Actual Result:</b> After tapping on 'Filter By' button, the mobile refinement modal is displyed",
						"<b>Actual Result:</b> After tapping on 'Filter By' button, the mobile refinement modal is not displyed");

			}
			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_237

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Product Grid is shown in Category Page when template is updated in Business Manager", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_241(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category[] = testData.get("SearchKey").split("\\|");
		List<String> RefinementFilterOptions = Arrays
				.asList("lstRefinementFilterOptions");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("<b> Expected Result 1: </b>	Left Navigation Panel should be available in category landing page ");
			Log.assertThat(
					plppage.verifyLeftPaneInCategoryPage(),
					"<b> Actual Result 1: </b>	Left Navigation Panel is available in category landing page ",
					"<b> Actual Result 1: </b>	Left Navigation Panel is not available in category landing page ");

			Log.message("<b> Expected Result 2: </b>	Sort Options should be available in category landing page ");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("drpSortBy"), plppage),
					"<b> Actual Result 2: </b>	Sort Options is available in category landing page ",
					"<b> Actual Result 2: </b>	Sort Options is not available in category landing page ");

			Log.message("<b> Expected Result 3: </b>	Toggle icon should be available in category landing page ");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("listView", "girdView"), plppage),
					"<b> Actual Result 3: </b>	Toggle icon is available in category landing page ",
					"<b> Actual Result 3: </b>	Toggle icon is not available in category landing page ");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_241

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting Back to ___ link below the L3 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_140(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> breadcrumb = Arrays.asList("bcLastCategoryName");
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0] + " >>> " + category[1] + ">>>" + category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			if (runPltfrm == "desktop") {
				plppage.clickOnBackButton();
				Log.message("<b> Expected Result: </b> It should be navigated to L1 categories page.");
				Log.assertThat(
						plppage.elementLayer.verifyPageElements(breadcrumb,
								plppage),
						"<b> Actual Result: </b>  Click on the Back to L1 category name link below the L3 categories in the left navigation of category refinements panel L1 category is displayed in breadcrumb in the CLP",
						"<b> Actual Result: </b> Not able to click on the  Back to L1 category name link below the L3 categories in the left navigation of category refinements panel and  L1 category is not displayed in breadcrumb in the CLP",
						driver);
			}
			if (runPltfrm == "mobile") {
				Log.assertThat(
						plppage.verifyBackToCategoyInBreadCrumb("Back to Big & Tall Clothing "),
						"3. Navigated category option is displayed in the breadcrum",
						"3. Navigated category option is not displayed in the breadcrum ");
				driver.getCurrentUrl();
				plppage.backToCategoyInBreadCrumbCategory("Back to Big & Tall Clothing ");
				Log.message("<b> Expected Result: </b> It should be navigated to Home categories page.");
				Log.message(
						"<b> Actual Result: </b>  Click on the Back to L1 category name link below the L3 categories in the left navigation of category refinements panel L1 category is displayed in breadcrumb in the CLP",
						driver);

			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_129

	@Test(groups = { "desktop", "mobile" }, description = "Verify product list row should be properly displayed in PDP page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_225(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");
			searchResultPage.clickGridListToggle("List");
			Log.message("3. Clicked on 'List' Toggle in Search Result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Product list row should be properly displayed right side of the product image");
			Log.assertThat(
					searchResultPage.getCurrentLocationOfPdtByIndex(1) == searchResultPage
							.getCurrentLocationOfPdtByIndex(2)
							&& searchResultPage
									.getCurrentLocationOfPdtByIndex(1) == searchResultPage
									.getCurrentLocationOfPdtByIndex(3)
							&& searchResultPage
									.getCurrentLocationOfPdtByIndex(1) == searchResultPage
									.getCurrentLocationOfPdtByIndex(4)
							&& searchResultPage.elementLayer
									.verifyPageListElements(
											Arrays.asList("lstProducts"),
											searchResultPage),
					"<b>Actual Result :</b> Product list row is properly displayed right side of the product image.",
					"<b>Actual Result :</b> Product list row is not properly displayed right side of the product image.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_105

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the product number based upon the option chosen by the customer in the items per page drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_080(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			int productCountbeforeSelectingOptions = searchResultPage
					.getProductCount();
			Log.message("3. Product Count: '"
					+ productCountbeforeSelectingOptions
					+ "' before selecting the options from 'Items per Page DropDown'");

			searchResultPage.selectItemsPerPageDropDownByText("View 60");

			Log.message("4. Selected Options From 'Items Per Page' Drop Down");
			int productCountAfterSelectingOptions = searchResultPage
					.getProductCount();
			Log.message("5. Product Count: '"
					+ productCountAfterSelectingOptions
					+ "' after selecting the options from 'Items per Page DropDown'");

			String Viewingresult = searchResultPage.getResultHitsText();
			String resultsplit[] = Viewingresult.split("\\-");
			String resultsplittoGetTheProductCount[] = resultsplit[1]
					.split(" ");
			int productCountInViewPageResult = Integer
					.parseInt(resultsplittoGetTheProductCount[1]);
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> System should displays the product number based upon the option chosen by the customer in the Items per page drop down.");
			Log.assertThat(
					productCountInViewPageResult == productCountAfterSelectingOptions,
					"<b>Actual Result :</b> System displayed the product number based upon the option chosen by the customer in the 'Items per page' drop down.",
					"<b>Actual Result :</b> System failed to display the product number based upon the option chosen by the customer in the 'Items per page' drop down.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "tablet" }, description = "Verify the deepest category in the catalog tree", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_116(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop")
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for "
							+ runPltfrm);
		else {
			// Loading the test data from excel using the test case id
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			ArrayList<String> breadcrumbText = new ArrayList<String>();
			ArrayList<String> categoryText = new ArrayList<String>();
			String[] category = testData.get("SearchKey").split("\\|");
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				homePage.headers.navigateTo(category[0], category[1],
						category[2]);
				Log.message("2. Navigated to " + category[2] + " page!");

				SearchResultPage searchResultPage = new SearchResultPage(driver);
				int productCountbeforeSelectingOptions = searchResultPage
						.getProductCountInPage();
				Log.message("3. Product Count: '"
						+ productCountbeforeSelectingOptions
						+ "' before selecting the Refinement");
				PlpPage plppage = new PlpPage(driver).get();
				String color = plppage
						.selectUnselectFilterOptions("refinementColor");
				Log.message("4. Selected 1st Refinement:" + color);

				String size = plppage
						.selectUnselectFilterOptions("refinementSize");
				Log.message("5. Selected 2nd Refinement:" + size);
				int productCountAfterSelectingOptions = searchResultPage
						.getProductCountInPage();
				Log.message("6. Product Count: '"
						+ productCountAfterSelectingOptions
						+ "' after selecting the Refinement'");

				breadcrumbText = plppage.getTextInBreadcrumb();
				categoryText.add(0, "Home");
				for (int i = 1; i < breadcrumbText.size(); i++) {
					categoryText.add(i, category[i - 1]);
				}
				searchResultPage.verifySpinner("refinementColor");
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b> By selecting the refinement values, page content should be refresh/update");
				Log.assertThat(
						(!(productCountbeforeSelectingOptions == productCountAfterSelectingOptions)),
						"<b>Actual Result 1:</b> By selecting the refinement values, page content is getting refreshed/updated",
						"<b>Actual Result 1:</b> By selecting the refinement values, page content is not getting refreshed/updated",
						driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b> User should see the deepest category in the catalog tree");

				Log.assertThat(
						breadcrumbText.get(1).equals(categoryText.get(1)),
						"<b>Actual Result 2:</b> User is able to see the deepest category '"
								+ categoryText.get(1) + "' in the catalog tree",
						"<b>Actual Result 2:</b> User is not able to see the deepest category '"
								+ categoryText.get(1)
								+ "' in the catalog tree ", driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_SEARCH_116

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays a View All Article link if the maximum of 7 articles are returned.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_095(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> System should displays a View All Article link if there are more than 7 articles are returned.");

			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("btnViewAllArticles"),
							searchResultPage)
							&& searchResultPage.checkArticlesCount() == 7,
					"<b>Actual Result :</b> System Displayed 'View All Article' Link ,If more than 7 articles are returned",
					"<b>Actual Result :</b> System failed to Display 'View All Article' Link ,If more than 7 articles are returned");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_095

	@Test(groups = { "desktop", "mobile" }, description = "Verify system navigates the customer to previous search page, when customer click on the 'Back to Product Result' in the View All article page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			searchResultPage.checkArticlesCount();
			Log.message("3. Clicked On 'View All Articles' Link and Navigated to View All Article Page!");

			searchResultPage.clickOnBackToProductResults();
			Log.message("4. Clicked On 'Back to Product Result' Link in the View All Article page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> When customer click on the Back to Product Result in the View All article page, System should navigate the customer to previous search.");
			Log.assertThat(
					driver.getCurrentUrl().contains("/search/"),
					"<b>Actual Result 1:</b> When customer clicks on the Back to Product Result in the View All article page, System navigated the customer to previous search.",
					"<b>Actual Result 1:</b> When customer clicks on the Back to Product Result in the View All article page, System not navigated the customer to previous search.");

			searchResultPage.getTextFromSearchResult();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Your search results for <keyword> should be displayed in Breadcrum");
			Log.assertThat(
					searchResultPage.getTextFromSearchResult().contains(
							searchKey),
					"<b>Actual Result 2:</b> Your search results for <"
							+ searchKey + "> is displayed in Breadcrum.",
					"<b>Actual Result 2:</b> Your search results for <"
							+ searchKey + "> is not displayed in Breadcrum.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_105

	@Test(groups = { "desktop", "mobile" }, description = "Check the products list displayed in the Products matches list in Suggestion Pop-up list.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and AutoSuggestion PopUp is Opened");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Products matching to the entered search criteria should be listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].");
			Log.assertThat(
					homePage.elementLayer.verifyPageListElements(
							Arrays.asList("lstProductNameInPdtSuggestions"),
							homePage)
							&& homePage.getProductNameFromProductSuggestion(1)
									.contains(searchKey)
							&& homePage.elementLayer
									.verifyPageListElements(
											Arrays.asList("lstsrchSuggtnProductPrice "),
											homePage),
					"<b>Actual Result 1:</b> Products matching to the entered search criteria is listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].",
					"<b>Actual Result 1:</b> Products matching to the entered search criteria is not listed with the following details ,'Brand Name' followed by 'Product name' and 'Price details' [Org Price & Now price].");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_019

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system disable the page bar, if the products is less than 30.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_082(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page bar should be disabled when products count is lessthan 30  in the 'SearchResult' Page");
			Log.assertThat(
					searchResultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstPageNos"),
									searchResultPage),
					"<b>Actual Result:</b> The page bar is disabled when products count is lessthan 30  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The page bar is not disabled when products count is lessthan 30  in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_082

	@Test(groups = { "desktop" }, description = "Verify system expands and the full set of refinement values displays with a 'view less' link, when customer click on the 'View more' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_123(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			int refinementCount = searchResultPage
					.refinementValuesCountViewMore("refinementSize");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Refinement Values count should be 7 with 'ViewMore' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCount == 7,
					"<b>Actual Result:</b> The Refinement Values count is 7 with 'ViewMore' Link in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not 7 with 'ViewMore' Link in the 'SearchResult' Page",
					driver);

			Log.message("<br>");
			searchResultPage.clickViewMoreLink("refinementSize");
			Log.message("3. Clicked On 'ViewMore' Link, in the refinementSection in the Searchresult page");

			int refinementCountAfterViewMore = searchResultPage
					.refinementValuesCountViewLess("refinementSize");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Refinement Values count should be greaterthan 7 when we click on 'ViewMore' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCountAfterViewMore > 7,
					"<b>Actual Result:</b> The Refinement Values count is 7  greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not 7 greaterthan 7 when we click on 'ViewMore' Link  in the 'SearchResult' Page",
					driver);

			Log.message("<br>");
			searchResultPage.clickViewLessLnk("refinementSize");
			Log.message("4. Clicked On 'ViewLess' Link, in the refinementSection in  the Searchresult page");

			int refinementCountAfterViewLess = searchResultPage
					.refinementValuesCountViewMore("refinementSize");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Refinement Values count should be 7 when we click on 'ViewLess' Link in the 'SearchResult' Page");
			Log.assertThat(
					refinementCountAfterViewLess == 7,
					"<b>Actual Result:</b> The Refinement Values count is 7 when we click on 'ViewLess' Link  in the 'SearchResult' Page",
					"<b>Actual Result:</b> The Refinement Values count is not 7 when we click on 'ViewLess' Link  in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_123

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the indication of L3  sub-categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_136(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			if (Utils.getRunPlatForm() == "mobile") {
				categoryLandingPage.clickFilterByInMobile();
			}

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol should be displayed in the CategoryLanding Page");
			Log.assertThat(
					categoryLandingPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstL2Sub_Refinements"),
							categoryLandingPage),
					"<b>Actual Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol is displayed in the CategoryLanding Page",
					"<b>Actual Result1:</b> If a L3 category has a sub­category nested beneath, a  > symbol is not displayed in the CategoryLanding Page",
					driver);

			Log.message("<br>");
			String Category_Name = categoryLandingPage
					.clickCategoryRefinementToggle();
			Log.message("3. Clicked On Category Refienment Toggle and naviagted to Next Level Refienment");

			if (Utils.getRunPlatForm() == "desktop") {
				ArrayList<String> BCValues = categoryLandingPage
						.getTextInBreadcrumb();
				String BCL3_category = BCValues.get(BCValues.size() - 1);

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> On clicking the '>' symbol, the page should be redirected to the selected L3 sub category page");

				Log.assertThat(
						Category_Name.trim().contains(BCL3_category.trim()),
						"<b>Actual Result2:</b> On clicking the '>' symbol, the page is redirected to the selected L3 sub category page",
						"<b>Actual Result2:</b> On clicking the '>' symbol, the page is not redirected to the selected L3 sub category page",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				categoryLandingPage.clickFilterByInMobile();
				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> On clicking the '>' symbol, the page should be redirected to the selected L3 sub category page");
				Log.assertThat(
						categoryLandingPage.getTxtOnBackButton().trim()
								.contains(L2Category),
						"<b>Actual Result2:</b> On clicking the '>' symbol, the page is redirected to the selected L3 sub category page",
						"<b>Actual Result2:</b> On clicking the '>' symbol, the page is not redirected to the selected L3 sub category page",
						driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_136

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting 'Back to <Category name>' below the L5 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_143(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			if (Utils.getRunPlatForm() == "desktop") {

				ArrayList<String> BCValues = categoryLandingPage
						.getTextInBreadcrumb();
				String BCL3_category = BCValues.get(BCValues.size() - 1);

				Log.message("<br>");
				Log.message("<b>Expected Result1:</b> By clicking on L3_category in the Hamburger Nav, It Should be naviagted to L3_categoryLanding Page");
				Log.softAssertThat(
						L3Category.trim().contains(BCL3_category.trim()),
						"<b>Actual Result1:</b> By clicking on L3_category in the Hamburger Nav, It is naviagted to L3_categoryLanding Page",
						"<b>Actual Result1:</b> By clicking on L3_category in the Hamburger Nav, It is not naviagted to L3_categoryLanding Page",
						driver);

				Log.message("<br>");
				categoryLandingPage.clickOnBackButton();
				Log.message("3. Clicked On 'BackToPrevious' Refienment and naviagted to Previous Level  Category Refienment");

				ArrayList<String> BCValue = categoryLandingPage
						.getTextInBreadcrumb();
				String BCL2_category = BCValue.get(BCValue.size() - 1);

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> By Clicking On 'BackToPrevious' Refienment, it should be naviagted to Previous Level  Category Refienment");
				Log.assertThat(
						L2Category.trim().contains(BCL2_category.trim()),
						"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is naviagted to Previous Level  Category Refienment",
						"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is not naviagted to Previous Level  Category Refienment",
						driver);
			}
			if (Utils.getRunPlatForm() == "mobile") {

				categoryLandingPage.clickOnFilterByOption();

				Log.message("<br>");
				Log.message("<b>Expected Result1:</b> By clicking on L3_category in the header Nav, It Should be naviagted to L3_categoryLanding Page");
				Log.softAssertThat(
						categoryLandingPage.getTxtOnBackButton().trim()
								.contains(L2Category.trim()),
						"<b>Actual Result1:</b> By clicking on L3_category in the header Nav, It is naviagted to L3_categoryLanding Page",
						"<b>Actual Result1:</b> By clicking on L3_category in the header Nav, It is not naviagted to L3_categoryLanding Page",
						driver);
				categoryLandingPage.clickOnBackButton();

				categoryLandingPage.clickOnFilterByOption();

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> By Clicking On 'BackToPrevious' Refienment, it should be naviagted to Previous Level  Category Refienment");
				Log.assertThat(
						categoryLandingPage.getTxtOnBackButton().trim()
								.contains(L1Category.trim()),
						"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is naviagted to Previous Level  Category Refienment",
						"<b>Actual Result2:</b> By Clicking On 'BackToPrevious' Refienment, it is not naviagted to Previous Level  Category Refienment",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_143

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'Content Refinement' instead of 'Category Refinement' with the default styling.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_106(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			searchResultPage.checkArticlesCount();
			Log.message("3. Clicked On 'View All Articles' Button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' should be displayed in the 'SearchResult' Page");
			Log.assertThat(
					(searchResultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstCategoryRefinement"),
									searchResultPage) && (searchResultPage.elementLayer
							.verifyPageElements(
									Arrays.asList("contentRefinement"),
									searchResultPage))),
					"<b>Actual Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' is displayed in the 'SearchResult' Page",
					"<b>Actual Result:</b> By Clicking on 'ViewAllArticles' button, the 'Content Refinement' is not displayed in the 'SearchResult' Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_106

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Brands under product refinements panel when a specific brand is not available for the products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_165(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> After Selecting size, the Refinement count for Size under product refinements should be same as Product Count in the SearchResult Page ");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("brandRefinement"), searchResultPage),
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is same as Product Count in the SearchResult Page",
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is not same as Product Count in the SearchResult Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_165

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the full-fetched category path chosen by the customer and the current page path should not hyperlinked.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_034(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String breadcrumbInMobile = "Back to Women";
		List<String> breadcrumbText = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			String selectedCatagory = searchresultPage
					.selectCatgeoryRefinement();
			Log.message("3. " + selectedCatagory
					+ "Catagory is selected under :" + searchKey);
			if (runPltfrm == "desktop") {
				String urlBeforeClickOnSelectedCatagory = driver
						.getCurrentUrl();
				Log.message("4. Got the url before clicking catagory");

				searchresultPage.clickOnCatagoryTextInBreadrcrumb();
				Log.message("5. Catagory name is clicked");

				String urlAfterClickOnSelectedCatagory = driver.getCurrentUrl();
				Log.message("6. Got the url after clicking catagory");
				Log.message("<br>");
				Log.message("<b> Expected Result: </b> System should fully fetch the category and Selected catagory ("
						+ selectedCatagory
						+ ") should not displayed as hyperlink ");
				Log.assertThat(urlBeforeClickOnSelectedCatagory
						.equals(urlAfterClickOnSelectedCatagory),
						"<b> Actual Result: </b>  Selected catagory ("
								+ selectedCatagory
								+ ") is not displayed as hyperlink",
						"<b> Actual Result: </b> Selected catagory ("
								+ selectedCatagory
								+ ") is displayed as hyperlink");

				searchresultPage.clickOnFirstSelectedCatagory();
				Log.message("7. Clicked on the serached catagory in Breadcrumb : "
						+ searchKey);
				String urlforSearchedCatagory = driver.getCurrentUrl();
				Log.message("8. Got the url after selecting the searched catagory");
				Log.message("<br>");
				Log.message("<b> Expected Result: </b> Searched catagory("
						+ searchKey + ") is not displayed as hyperlink ");
				Log.assertThat(
						!urlforSearchedCatagory
								.equals(urlAfterClickOnSelectedCatagory),
						"<b> Actual Result: </b>  Searched catagory("
								+ searchKey + ") is not displayed as hyperlink",
						"<b> Actual Result: </b> Searched catagory("
								+ searchKey + ") is displayed as hyperlink");
			} else if (runPltfrm == "mobile") {
				breadcrumbText = searchresultPage.getTextInBreadcrumb();
				Log.assertThat(
						breadcrumbText.get(0).equals(breadcrumbInMobile),
						"Searched catagory is  hyperlinked. After searching Selected catagory is not be hyperlinked.",
						"Searched catagory should be hyperlinked.After searching Selected catagory should not be hyperlinked.");

			}

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_034

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system remove the View All from the Items per page drop down, if the products is more than 90", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_083(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> optionsInItems = new ArrayList<String>();
		String defaultOption = "View all";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			String valueSelect = searchresultPage
					.selectItemsPerPageDropDownByIndex(2);
			Log.message("3. Selected items per page drop down. and selected option is"
					+ valueSelect);
			searchresultPage.clickOnItemsPerPageDropDown();
			Log.message("4. Clicked on items per page drop down");
			optionsInItems = searchresultPage.getOptionValueInItemsPerPage();
			Log.message("5. Got the Option value: value are " + optionsInItems);
			boolean status = false;

			if (!optionsInItems.equals(defaultOption)) {
				status = true;
			} else {
				status = false;

			}
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> View All option should not found in drop down");
			Log.assertThat(
					status,
					"<b> Actual Result: </b> View All option is not found in drop down",
					"<b> Actual Result: </b> View All option is  found in drop down");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_083

	@Test(groups = { "desktop", "mobile" }, description = "Verify value displayed in the Items per page drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_085(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		ArrayList<String> expected_ItemsPerPageoptions = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");

			Log.message("<b>Expected Result:</b> View 30 should be the default value for Items Per Page Dropdown.");
			String selectedoption = searchresultPage
					.getSelectedOptionFromItemsPerPageDropDown();
			ArrayList<String> mylist = searchresultPage
					.getItemsPerPageOptionList();
			Log.assertThat(
					selectedoption.equals("View 30"),
					"<b>Actual Result:</b> View 30 is default value for Items Per Page Dopdown.",
					"<b>Actual Result:</b> View 30 is not the default value for Items Per Page Dopdown.",
					driver);
			expected_ItemsPerPageoptions.add("View 30");
			expected_ItemsPerPageoptions.add("View 60");
			expected_ItemsPerPageoptions.add("View 90");
			for (String itemsperpagevalue : mylist) {

				Log.softAssertThat(expected_ItemsPerPageoptions
						.contains(itemsperpagevalue),

				"<b>Actual Result:</b> ItemsPerPage drop down have '"
						+ itemsperpagevalue + "'", "<b>Actual Result:</b>"
						+ itemsperpagevalue
						+ " is not displayed in the 'ItemsPerPage' drop down",
						driver);
			}

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_085

	/*
	 * 
	 * @Test(groups = { "desktop", "mobile" }, description =
	 * "Verify Item per page dropdown", dataProviderClass =
	 * DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	 * public void TC_BELK_SEARCH_084(String browser) throws Exception {
	 * 
	 * HashMap<String, String> testData =
	 * TestDataExtractor.initTestData(workbookName, sheetName);
	 * 
	 * String searchKey = testData.get("SearchKey");
	 * 
	 * // Get the web driver instance final WebDriver driver =
	 * WebDriverFactory.get(browser); Log.testCaseInfo(testData);
	 * 
	 * int productCount;
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!"); SearchResultPage
	 * searchResultPage = homePage.searchProductKeyword(searchKey);
	 * Log.message("2. Search with keyword '" + searchKey +
	 * "' and navigated to search result page"); Log.assertThat(
	 * searchResultPage
	 * .elementLayer.verifyPageElements(Arrays.asList("drpItemsPerPage"),
	 * searchResultPage),
	 * "3. View drop down is displayed in Search Result Page",
	 * "3. View drop down is not displayed in Search Result Page");
	 * 
	 * String itemCountText = searchResultPage.getResultHitsText(); String[]
	 * splitTotalItemCountText = itemCountText.split("of "); String
	 * totalItemCount = splitTotalItemCountText[1].replace(",", "");
	 * 
	 * if (Integer.parseInt(totalItemCount) < 60) throw new
	 * Exception("The product count should be more than 60."); // throw new
	 * Exception("The total product count is less than 90. // So unable to test
	 * this case");
	 * 
	 * searchResultPage.selectItemsPerPageDropDownByText("60"); productCount =
	 * searchResultPage.getProductCount(); Log.assertThat(productCount == 60,
	 * "4. 'View 60' is clicked and 60 products is displayed.",
	 * "'View 60' is clicked but 60 products is not displayed.", driver); String
	 * itemPerPageText=searchResultPage.getCurrentValueInItemPerPageDropdown();
	 * Log.assertThat(itemPerPageText.equals("View 60"),
	 * "5. 'View 60' is displayed in the dropdown"
	 * ,"View 60 is not displayed in the dropdown",driver); searchResultPage =
	 * homePage.searchProductKeyword(searchKey);
	 * Log.message("6. Search with keyword '" + searchKey +
	 * "' and navigated to search result page");
	 * itemPerPageText=searchResultPage.getCurrentValueInItemPerPageDropdown();
	 * Log.assertThat(itemPerPageText.equals("View 30"),
	 * "'<b>Actual Result:</b> View 30' (Default value) is displayed in the dropdown"
	 * ,"'View 30' (Default value) is not displayed in the dropdown",driver);
	 * Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally
	 * 
	 * }// TC_BELK_SEARCH_076
	 */
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Article title in bold with the link and first 2 lines of article description", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_098(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			searchresultPage.ScrollToArticle();
			Log.message("3. Scroll to the article ");
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> System should displays the Article title in bold with the link and first 2 lines of Article description.");
			Log.assertThat(
					searchresultPage.verifyArticlesTitleBold(),
					"<b> Actual Result: </b> Article title is displayed as bold",
					"<b> Actual Result: </b> Article title is not displayed as bold");

			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							Arrays.asList("txtDescriptionArticle"),
							searchresultPage),
					"<b> Actual Result: </b> System  displays  first 2 lines of Article description.",
					"<b> Actual Result: </b> System displays the first 2 lines of Article description.");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_083

	@Test(groups = { "desktop", "mobile" }, description = "Verify system navigate the customer to Article page, when customer click on the 'Read more' link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_099(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			PrivacyAndPolicyPage privacyAndPolicyPage = searchresultPage
					.navigateToPrivacyPolicyPage();
			Log.message("3. Clicked Read more link from Article");
			Log.message("<b>Expected Result: </b>On Clicking Readmore link from Article system should navigate the customer to the correspomnding Article page ");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "policy-guidelines")
							&& privacyAndPolicyPage != null,
					"<b>Actual Result:</b> Navigate to correct URL: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Navigated to wrong URL: "
							+ driver.getCurrentUrl(), driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_099

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the state of the selected L3 category", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_137(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				plpPage.clickFilterByInMobile();
			}

			Log.message("<b> Expected Result: </b> The selected L3 category should be bold.");
			Log.assertThat(
					plpPage.verifyLThreeCatagoryTitleBold().contains("Medium"),
					"<b> Actual Result: </b> Selected L3 Catagory is bold",
					"<b> Actual Result: </b> Selected L3 Catagory is not bold",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_137

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion (+/­) symbol for Brands refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_168(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("  Clicked on 'FilterBy' button in mobile");
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>It should expand or collapse the Brand refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleClosed"),
								searchResultPage),
						"<b>Actual Result : </b> Brand Refinement Collapsed by Default and Accordion symbol(+) should be Present",
						"<b>Actual Result : </b> Brand Refinement is not Collapsed by Default",
						driver);

				searchResultPage.brandToggle(true);
				Log.message("</br>");
				Log.message("3. Brand toggle expanded!</br>");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleExpanded"),
								searchResultPage),
						"<b>Actual Result : </b>Brand Refinement Expanded and Accordion symbol(-) should be Present",
						"<b>Actual Result : </b>Brand Refinement is not Expanded",
						driver);
			} else {
				Log.message("</br><b>Expected Result : </b>It should expand or collapse the Price refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleExpanded"),
								searchResultPage),
						"<b>Actual Result : </b>Brand Refinement Expanded by Default and Accordion symbol(+) should be Present",
						"<b>Actual Result : </b>Brand Refinement is not Expanded by Default",
						driver);

				searchResultPage.brandToggle(false);
				Log.message("<br>3. Brand toggle expanded!<br>");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleClosed"),
								searchResultPage),
						"<b>Actual Result : </b>Brand Refinement Collapsed and Accordion symbol(-) should be Present",
						"<b>Actual Result : </b>Brand Refinement is not Expanded by Default",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_168

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify grouping brands by letter under Brands refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_169(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		String txtrefinementOptions = "brand";
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String refinementOption = "brand";
		List<String> brandExpand = Arrays.asList("lstBrandExpand");
		int refinementValue = 7;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> Brand refinement should present properly");
			Log.assertThat(
					searchresultPage.isRefinementPrsent(txtrefinementOptions),
					"<b> Actual Result: </b> Brand refinement present properly",
					"<b> Actual Result : </b> Brand refinement present properly",
					driver);
			Log.message("<br>");
			ArrayList<String> brandName = searchresultPage
					.getBrandNameInRefinement();
			Log.message("3. Got the Brand names Sucessfully !");
			List<String> brandNameAfterSort = new ArrayList<String>(brandName);
			Collections.sort(brandNameAfterSort);
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Selectable brands should be displayed as a checkbox list in alphabetical order with the brand name or brand value.");
			Log.assertThat(
					brandNameAfterSort.equals(brandName),
					"<b>Actual Result  :</b> Brands refinement should displays in alphabetical order",
					"<b>Actual Result :</b> Brands refinement should displays in alphabetical order",
					driver);
			Log.message("<br>");
			int refinementValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			Log.message("4. Got the refinement count and the count is"
					+ refinementValueBeforeClick);
			Log.assertThat(
					refinementValue == refinementValueBeforeClick,
					"<b>Actual Result :</b> System displays the first 7 letters",
					"<b>Actual Result :</b> System not displays the first 7 letters");
			searchresultPage
					.clickCollapseExpandToggleofSubSection(refinementOption);
			Log.message("5. Clicked expand toggle of sub section for brand");
			Log.message("<b> Expected Result : </b> Each section should be separated by a sub­section with the letter of the alphabet that matches the brand names");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							brandExpand, searchresultPage),
					"<b>Actual Result :</b> Each section separated by a sub­section with the letter of the alphabet that matches the brand names",
					"<b>Actual Result :</b> Each section should be separated by a sub­section with the letter of the alphabet that matches the brand names.");
			Log.message("6. Got the refinement count and the count is "
					+ refinementValueBeforeClick);
			searchresultPage.clickViewMore(txtrefinementOptions);
			Log.message("7. view more link is clicked");
			int refinementValueAfterClick = searchresultPage
					.refinementValuesCountViewMore(txtrefinementOptions);
			Log.message("8. Got the brand refinement count value after clicking ");
			Log.message("<br>");
			Log.message("<b>Expected Result 3b:</b>  While clicking on the “view more” link should displays all letters. ");
			Log.assertThat(
					refinementValueAfterClick >= refinementValueBeforeClick,
					"<b>Actual Result :</b>  While clicking on the “view more” link should displays all letters. ",
					"<b>Actual Result :</b>  System  displays the first 7 letters, and then should displays a “view more” link. While clicking on the “view more” link should displays all letters. ");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_169_2B

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Articles in descending order of the most recent best matches from left to right and each articles title is  in bold with a link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_107(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			searchresultPage.ScrollToArticle();
			Log.message("3. Scroll to the article ");
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> System should displays the Article title in bold with the link and first 2 lines of Article description.");
			Log.assertThat(
					searchresultPage.verifyArticlesTitleBold(),
					"<b> Actual Result: </b> Article title is displayed as bold",
					"<b> Actual Result: </b> Article title is not displayed as bold");
			Log.message("<b> Expected Result: </b> System should display the Articles in descending order");
			Log.assertThat(
					searchresultPage.verifyArticlesDecendingOrder(),
					"<b> Actual Result: </b> Articles are in descending oreder",
					"<b> Actual Result: </b> Articles are not in descending oreder");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_098

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the <br> tags given in Category Name in BM are not shown in Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_145(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> txtCatagory = new ArrayList<String>();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			txtCatagory = searchresultPage.getTextFromCatagories();
			Log.message("3.Got the text from catagory" + txtCatagory);
			Log.message("<b> Expected Result: </b> <br> tag should not seen in Categories list");
			Log.assertThat(!txtCatagory.contains("<br>"),
					" br tag is not seen in Categories list ",
					" br tag is seen in Categories list ");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_145

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system collapse back to the configured number for that refinement, when customer click on the View Link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_124(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		String txtrefinementOptions = "refinementSize";
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String refinementOption = "Size";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			int refinementValueBeforeClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			Log.message("3. Got the count of refinement before view more link"
					+ refinementValueBeforeClick);
			searchresultPage.clickViewMoreLink(txtrefinementOptions);
			Log.message("4. View more link is clicked", driver);
			searchresultPage.clickViewLessLnk(txtrefinementOptions);
			Log.message("5. View less link is clicked", driver);
			Log.message("<br>");
			int refinementValueAfterClick = searchresultPage
					.refinementValuesCount(txtrefinementOptions);
			Log.message("6. Got the count of refinement after view less link"
					+ refinementValueAfterClick);
			Log.message("<b> Expected Result: </b> System should collapse back to the configured number for that refinement");
			Log.assertThat(
					refinementValueAfterClick == refinementValueBeforeClick,
					"<b> Actual Result: </b> System collapse back to the configured number for that refinement",
					"<b> Actual Result: </b> System should collapse back to the configured number for refinement");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_124

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the default face out image and color swatch, If color specific term does not match any available variations .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_058(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// select random Swatch product
			PdpPage pdpPage = searchresultPage.selectRandomColorSwatchProduct();
			Log.message("3. selected the product which have swatch and navigated to PDP page !");
			// Get the source code of PrimaryImg
			String PrimaryImgSrc = pdpPage.getPrimaryImageColor();
			Log.message("4. Successfully got the primary Image Source Code !");
			// Get the source code of SwatchImg
			String SwatchImgSrc = pdpPage.getSelectedSwatchColorSrcCode();
			Log.message("5. Successfully got the Swatch Image Source Code !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("System should displays the default face out image and color swatch");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					PrimaryImgSrc.equals(SwatchImgSrc),
					"System displays the default face out image and color swatch !",
					"System not displayed the default face out image and color swatch !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_058

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the 'View All' option in the Items per page if the items number is less than 90.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_086(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// select option by index
			String selectedOption = searchresultPage
					.selectItemsPerPageDropDownByIndex(2);
			Log.message("3. selected option is '" + selectedOption + "' !");
			int productCount = searchresultPage.getProductCountInPage();
			Log.message("4. Total no of product after selection of view All is '"
					+ productCount + "' !");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("System should display the View All if the item number is less than 90");
			Log.message("(XX) = number of products within the search result, which is less than 90");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					productCount < 90,
					"System displays the count of product is less then 90 after selecting the view All(xx) !",
					"System not displays the count of product is less then 90 after selecting the view All(xx) !",
					driver);
			searchresultPage.clickOnItemsPerPageDropDown();
			ArrayList<String> OptionsInDropDown = searchresultPage
					.getOptionValueInItemsPerPage();
			String firstOption = OptionsInDropDown.get(0);
			String secondOption = OptionsInDropDown.get(1);
			String lastOption = OptionsInDropDown.get(2);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("The dropdown should display 'View 30' , 'View 60' , 'View All(XX)'");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					firstOption.contains("View 30"),
					"After clicking on dropdown 'View 30' displyed as option !",
					"After clicking on dropdown 'View 30' not displyed as option !",
					driver);
			Log.assertThat(
					secondOption.contains("View 60"),
					"After clicking on dropdown 'View 60' displyed as option !",
					"After clicking on dropdown 'View 60' not displyed as option !",
					driver);
			Log.assertThat(
					lastOption.contains("View All"),
					"After clicking on dropdown 'View All(xx)' displyed as option !",
					"After clicking on dropdown 'View All(xx)' not displyed as option !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_086

	@Test(groups = { "mobile" }, description = " Verify system display the List view around all pages.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_087(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			searchresultPage.clickGridListToggle("List");
			Log.message("3. Clicked on List View Icon");
			searchresultPage.navigateToPagination("Next");
			Log.message("4. Navigated to Second Page");
			String currentview = searchresultPage.getCurrentViewType();
			Log.message("<b>Expected Result:</b> Product list view should not be changed if user navigated to different pages");
			Log.assertThat(
					currentview.equals("wide"),
					"<b>Actual Result:</b> Product list view is not changed if user navigated to different pages.",
					"<b>Actual Result:</b> Product list view changed if user navigated to different pages.",
					driver);
			searchresultPage.deleteAllCookies();
			Log.message("5. All cookies deleted");
			String currentviewafterdeletecookies = searchresultPage
					.getCurrentViewType();
			Log.message("<b>Expected Result:</b> Product list view  should change to grid after deleting all the Cookies.");
			Log.assertThat(
					currentviewafterdeletecookies.equals("grid"),
					"<b>Actual Result:</b> Product list view  changed to grid after deleting all the Cookies.",
					"<b>Actual Result:</b> Product list view  didnot change to grid after deleting all the Cookies.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_Search_087

	@Test(groups = { "desktop" }, description = "Verify that the 'View <x>' dropdown value is retained upon refinement.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_088(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with Keyword: "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");

			String selectedvaluebeforerefinement = searchresultPage
					.selectItemsPerPageDropDownByText("60");
			searchresultPage.waitUntilSearchSpinnerDisappear();
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickOnFilterByOption();
			}
			searchresultPage.selectUnselectFilterOptions(" ", "$0 - $19.99");
			searchresultPage.waitUntilSearchSpinnerDisappear();
			String selectedvalueafterrefinement = searchresultPage
					.getSelectedOptionFromItemsPerPageafterchangingValue();

			Log.message("<b>Expected Result:</b> The 'View x' value should be retained in the dropdown after refinement is applied ");
			Log.assertThat(
					selectedvaluebeforerefinement
							.equals(selectedvalueafterrefinement),
					"<b>Actual Result:</b>The 'View x' value is retained in the dropdown after refinement is applied ",
					"<b>Actual Result:</b> The 'View x' value is not retained in the dropdown after refinement is applied ",
					driver);
			Log.message("<b>Expected Result:</b> The 'View x' should be in Black color. ");
			Log.assertThat(
					searchresultPage.verifyTextColorFromItemsPerPageDropDown(),
					"<b>Actual Result:</b> The 'View x' is in Black color",
					"<b>Actual Result:</b>  The 'View x' is not in Black color",
					driver);
			Log.message("<b>Expected Result:</b> The value in 'Sort By' dropdown should be in Black color.");
			Log.assertThat(
					searchresultPage.verifyTextColorOptionSortByDropdown(),
					"<b>Actual Result:</b> The value in 'Sort By' dropdown is in Black color",
					"<b>Actual Result:</b> The value in 'Sort By' dropdown is not in Black color",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_088

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system navigate the customer to 'View All Articles result' page, when customer click on the 'View All Articles' link.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_101(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("ArticlePge");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnViewAllArticleButton();
			Log.message("3. clicked on view All Article link !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should navigate the customer to View All Articles result page .");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							element, searchresultPage),
					"System navigated the customer to View All Articles result page .",
					"System not navigated the customer to View All Articles result page .",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_101

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the recommended product for the search with the Search result Banner slot.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_108(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("SearchBanner");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			searchresultPage.clickOnViewAllArticleButton();
			Log.message("3. clicked on view All Article link !");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message("System should display the recommended product for the keyword search and Search result Banner should get displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(element,
							searchresultPage),
					"After clicking on view all article 'Search result Banner' get displayed ! ",
					"After clicking on view all article 'Search result Banner' not get displayed ! ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_108

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting the L3 category which has no L4 categories", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_138(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			String style = searchresultPage.verifyLabelStyleBold();
			Log.message("<br>");
			Log.message("<b>Expected Result : </b>");
			Log.message("The selected L3 category should be bold with its product count in parentheses and no  > symbol should displays.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");
			Log.assertThat(style.contains("Bold"),
					"The selected L3 category displayed with bold style !",
					"The selected L3 category not displayed with bold style !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_138

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selecting colors in Color refinement under product refinements panel for a single color", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_161(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> toogleActive = Arrays.asList("toogleActive");
		int countBeforeRefine;
		int countAfterRefine;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// Get the product count before refine
			countBeforeRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("3. Before refinement product count is :"
					+ countBeforeRefine);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			String color = searchresultPage.selectSwatchColorFromRefinment();
			Log.message("4. Filtered the products by color :" + color, driver);
			// Get the product count after refine
			countAfterRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("5. After refinement product count is :"
					+ countAfterRefine);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("The Color refinement option should displays an orange checkbox next to the item indicating that the color has been selected.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							toogleActive, searchresultPage),
					"The Color refinement option displays an orange checkbox next to the item indicating that the color has been selected !",
					"The Color refinement option not displays an orange checkbox next to the item indicating that the color has been selected !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("After selecting an available color, the product grid should refresh to show the products that match the selected color refinement.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					countBeforeRefine > countAfterRefine,
					"After selecting an available color, the product grid refreshed to show the products that match the selected color refinement !",
					"After selecting an available color, the product grid not refreshed to show the products that match the selected color refinement !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_161

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selectable brands in Brands refinement under product refinements panel .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_170(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// get the brand names
			ArrayList<String> BrandName = searchresultPage
					.getBrandNameInRefinement();
			Log.message("3. Got the Brand names Sucessfully !");
			List<String> tmp = new ArrayList<String>(BrandName);
			Collections.sort(tmp);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Selectable brands should be displayed as a checkbox list in alphabetical order with the brand name or brand value.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					tmp.equals(BrandName),
					"Selectable brands displayed as a checkbox list in alphabetical order with the brand name or brand value !",
					"Selectable brands not displayed as a checkbox list in alphabetical order with the brand name or brand value !",
					driver);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
			}
			int count = searchresultPage.refinementValuesCount("brand");
			searchresultPage.clickViewMoreLinkInBrandRefinement();
			int afterClickOnViewMore = searchresultPage
					.brandAfterClickViewMoreProductCount();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Brands should displays 7 value and after clicking view more count should be more than 7. ");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					count < afterClickOnViewMore,
					"Brands displays 7 value only and after clicking view more count it shows more than 7 value !",
					"Brands displays 7 value only and after clicking view more count itis not showing more than 7 value !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_170

	// TC_BELK_SEARCH_222
	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_222(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			if (Utils.getRunPlatForm().equals("mobile")) {
				plpPage.clickFilterByInMobile();
				Log.message("3.Clicked on 'FilterBy' button in mobile");
			}//

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Refinement modal should be displayed when clicking on 'Filter by' button");
			Log.assertThat(
					plpPage.elementLayer.verifyPageElements(
							Arrays.asList("leftNavigationMobile"), plpPage),
					"<b>Actual Result:</b> Refinement modal is displayed when clicking on 'Filter by' button",
					"<b>Actual Result:</b> Refinement modal is not displayed when clicking on 'Filter by' button",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_222

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the availability of 'List View' toggle icon in PLP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_223(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");
		String l3category = testData.get("L3_category");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated to " + l1category + " >>" + l2category
					+ " >>" + l3category);

			SearchResultPage SearchResultPage = new SearchResultPage(driver)
					.get();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> List View toggle icon should be displayed in PLP");

			Log.assertThat(
					SearchResultPage.elementLayer.verifyPageElements(
							Arrays.asList("toggleList"), SearchResultPage),
					"<b>Actual Result:</b> List View toggle icon is displayed in PLP",
					"<b>Actual Result:</b> List View toggle icon is not displayed  in PLP",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	// TC_BELK_SEARCH_127
	@Test(groups = { "mobile" }, description = "Mobile : Verify system displays a 'Filter By' option and when customer tap on it, a modal get open on the Search result grid.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_127(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String SearchKey = testData.get("SearchKey");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(SearchKey);
			Log.message("2. Searched with the keyword '" + SearchKey
					+ "' and navigated to SearchResultPage !");

			if (Utils.getRunPlatForm().equals("mobile")) {
				searchresultPage.clickOnFilterByOption();
				Log.message("3.Clicked on 'FilterBy' button in mobile");
			}//

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Refinement modal should be displayed when clicking on 'Filter by' button");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("leftNavigationMobile"),
							searchresultPage),
					"<b>Actual Result:</b> Refinement modal is displayed when clicking on 'Filter by' button",
					"<b>Actual Result:</b> Refinement modal is not displayed when clicking on 'Filter by' button",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_127

	// TC_BELK_SEARCH_250
	@Test(groups = { "mobile" }, description = "Verify the functionality of 'Filter By' button in Category landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_250(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Filter by' text in button should be blue color");
			Log.assertThat(
					(plpPage.verifyFilterByTxtColor()),
					"<b>Actual Result 1:</b> 'Filter by' text in button is blue color",
					"<b>Actual Result 1:</b> 'Filter by' text in button is not blue color",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> 'Filter by' button border should be blue color");
			Log.assertThat(
					(plpPage.verifyFilterByBtnBorderColor()),
					"<b>Actual Result 2:</b> 'Filter by' button border is blue color",
					"<b>Actual Result 2:</b> 'Filter by' button border is not blue color",
					driver);

			if (Utils.getRunPlatForm().equals("mobile")) {
				plpPage.clickFilterByInMobile();
				Log.message("3.Clicked on 'FilterBy' button in mobile");
			}//

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Refinement modal should be displayed when clicking on 'Filter by' button");
			Log.assertThat(
					plpPage.elementLayer.verifyPageElements(
							Arrays.asList("leftNavigationMobile"), plpPage),
					"<b>Actual Result 3:</b> Refinement modal is displayed when clicking on 'Filter by' button",
					"<b>Actual Result 3:</b> Refinement modal is not displayed when clicking on 'Filter by' button",
					driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_250

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Breadcrumb only to the previous page and The last page / System will retain the state it was in when the customer left the page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_109(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to Search results page");

			searchresultPage.clickViewAllArticlesButton();
			Log.message("3. Clicked on 'View all articles' button at the botton of search results page");
			// searchresultPage.getSearchTerm();

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Search Term should be displayed in articles page breadcrumb");
			Log.assertThat(
					(searchresultPage.getSearchTerm()).equals(searchKey),
					"<b>Actual Result :</b> Search Term is displayed in articles page breadcrumb",
					"<b>Actual Result :</b> Search Term is not displayed in articles page breadcrumb",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_009

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify color swatches for product groupings in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_188(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);

			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to Search results page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Color Swatches should not be displayed for the product groupings");

			Log.assertThat(
					searchresultPage.elementLayer
							.verifyPageListElementsDoNotExist(
									Arrays.asList("lstImgColorSwatches"),
									searchresultPage),
					"<b>Actual Result 1:</b> Color swatches are not displayed for the product groupings",
					"<b>Actual Result 1:</b> Color swatches are displayed for the product groupings",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Page should be redirected to PDP of the product when clickign on product name");
			PdpPage pdpPage = searchresultPage.selectProductByNameRandom();

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("drpQty"), pdpPage),
					"<b>Actual Result 2:</b> Page is redirected to PDP of the product when clicking on product name",
					"<b>Actual Result 2:</b> Page is not redirected to PDP of the product when clicking on product name",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_009

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Product Thumbnail for product sets in product tiles", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_190(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String[] category = testData.get("CategoryLevel").split("\\|");
		String searchkey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigating to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.message("<br>");
			Log.message("* Verifying Thumbnail Image in ProductListingPage ");
			Log.message("<br>");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to Product Listing Page!");

			PlpPage plppage = new PlpPage(driver).get();
			String imageUrlinListingPage = plppage.getImageUrlInListPage(2);
			String imageurlinListingPagesplit[] = imageUrlinListingPage
					.split("comp");
			Log.message("3. Got the Product ImageUrl in ProductListingPage: "
					+ imageUrlinListingPage);

			PdpPage pdpPage = plppage.navigateToPDPByRow(2);
			Log.message("4. Navigated to PDP from ProductListingPage!");
			String thumbnailImageUrl = pdpPage.getThumbnailImageUrl(1);
			String thumbnailImageUrlsplit[] = thumbnailImageUrl.split("comp");
			Log.message("5. Got the Thumbnail ImageUrl in Pdp: "
					+ thumbnailImageUrl);

			String imageUrlInPdp = pdpPage.getImageUrl();
			String imageUrlInPdpsplit[] = imageUrlInPdp.split("comp");
			Log.message("6. Got the Main ImageUrl in Pdp: " + imageUrlInPdp);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> In ProductListingPage, The product set thumbnail image should displays the main image assigned to the product set.");

			Log.assertThat(
					(imageurlinListingPagesplit[0]
							.equals(thumbnailImageUrlsplit[0]) && (imageurlinListingPagesplit[0]
							.equals(imageUrlInPdpsplit[0]))),
					"<b>Actual Result 1:</b> In ProductListingPage, The product set thumbnail image displayed the main image assigned to the product set.",
					"<b>Actual Result 1:</b> In ProductListingPage, The product set thumbnail image not displayed the main image assigned to the product set.");
			Log.message("<br>");
			Log.message("* Verifying Thumbnail Image in SearchListingPage ");
			Log.message("<br>");

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchkey);
			Log.message("7. Searched with the keyword: " + searchkey
					+ " and Navigated to Search Listing Page!");
			String imageUrlinListingPage1 = searchresultpage
					.getImageUrlInListPage(2);
			String imageurlinListingPagesplit1[] = imageUrlinListingPage1
					.split("comp");
			Log.message("8. Got the Product ImageUrl in SearchListingPage: "
					+ imageUrlinListingPage1);

			PdpPage pdpPage1 = searchresultpage.navigateToPDPByRow(2);
			Log.message("9. Navigated to PDP from SearchListingPage!");
			String thumbnailImageUrl1 = pdpPage1.getThumbnailImageUrl(1);
			String thumbnailImageUrlsplit1[] = thumbnailImageUrl1.split("comp");
			Log.message("10. Got the Thumbnail ImageUrl in Pdp: "
					+ thumbnailImageUrl1);
			String imageUrlInPdp1 = pdpPage1.getImageUrl();
			String imageUrlInPdpsplit1[] = imageUrlInPdp1.split("comp");
			Log.message("11. Got the Main ImageUrl in Pdp: " + imageUrlInPdp1);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> In SearchListingPage, The product set thumbnail image should displays the main image assigned to the product set.");

			Log.assertThat(
					(imageurlinListingPagesplit1[0]
							.equals(thumbnailImageUrlsplit1[0]) && (imageurlinListingPagesplit1[0]
							.equals(imageUrlInPdpsplit1[0]))),
					"<b>Actual Result 2:</b> In SearchListingPage, The product set thumbnail image displayed the main image assigned to the product set.",
					"<b>Actual Result 2:</b> In SearchListingPage, The product set thumbnail image not displayed the main image assigned to the product set.");
			Log.message("<br>");
			Log.message("* Verifying Thumbnail Image in CategoryLandingPage ");
			Log.message("<br>");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("12. Navigated to Category Listing Page!");
			String imageUrlinListingPage2 = categoryLandingPage
					.getImageUrlInListPage(2);
			String imageurlinListingPagesplit2[] = imageUrlinListingPage2
					.split("comp");
			Log.message("13. Got the Thumbnail ImageUrl in Pdp: "
					+ imageUrlinListingPage2);

			PlpPage plppage1 = new PlpPage(driver).get();
			PdpPage pdpPage11 = plppage1.navigateToPDPByRow(2);
			Log.message("14. Navigated to PDP from CategoryLandingPage!");
			String thumbnailImageUrl2 = pdpPage11.getThumbnailImageUrl(1);
			String thumbnailImageUrlsplit2[] = thumbnailImageUrl2.split("comp");
			Log.message("15. Got the Thumbnail ImageUrl in Pdp: "
					+ thumbnailImageUrl2);
			String imageUrlInPdp2 = pdpPage.getImageUrl();
			String imageUrlInPdpsplit2[] = imageUrlInPdp2.split("comp");
			Log.message("16. Got the Main ImageUrl in Pdp: " + imageUrlInPdp2);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> In CategoryLandingPage,The product set thumbnail image should displays the main image assigned to the product set.");

			Log.assertThat(
					(imageurlinListingPagesplit2[0]
							.equals(thumbnailImageUrlsplit2[0]) && (imageurlinListingPagesplit2[0]
							.equals(imageUrlInPdpsplit2[0]))),
					"<b>Actual Result 3:</b> In CategoryLandingPage, The product set thumbnail image displayed the main image assigned to the product set.",
					"<b>Actual Result 3:</b> In CategoryLandingPage, The product set thumbnail image not displayed the main image assigned to the product set.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_190

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays a maximum of 6 articles at the bottom of the product search result", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_094(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			Log.message("<b> Expected Result 1: </b> Article displayed should be less than or equal to 6");
			Log.assertThat(
					searchresultPage.verifyCountOfArticlesDisplayed(),
					"<b> Actual Result 1: </b>  Article displayed is less than or equal to 6",
					"<b> Actual Result 1: </b> Article displayed is more than 6");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_094

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system navigates the customer to 'All Articles' page, when customer click on the View All Articles link or if a search resulted in just articles (no products)", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_104(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			searchresultPage.clickOnViewAllArticles();
			Log.message("3.Clicked on View All Articles Button");
			Log.message("<b> Expected Result 1: </b> System should Navigate customer to All Articles page");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("pageArticle"), searchresultPage),
					"<b> Actual Result 1: </b> System Navigates customer to All Articles page",
					"<b> Actual Result 1: </b> System does not Navigates customer to All Articles page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_104

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the styling of L2 category header", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_132(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category);
			CategoryLandingPage clPage = new CategoryLandingPage(driver).get();
			if (runPltfrm == "mobile") {
				clPage.clickFilterByInMobile();
			}

			Log.message("<b> Expected Result 1: </b> All the L2 category should be displayed in Blue Color Text");
			Log.assertThat(
					clPage.verifyColorIn1L2Category()
							&& clPage.verifyColorIn2L2Category()
							&& clPage.verifyColorIn3L2Category()
							&& clPage.verifyColorIn4L2Category(),
					"<b> Actual Result 1: </b>  All the L2 category is displayed in Blue Color Text",
					"<b> Actual Result 1: </b> All the L2 category is not displayed in Blue Color Text");

			Log.message("<b> Expected Result 2: </b> All the L2 category should have a toggle icon");
			Log.assertThat(
					clPage.elementLayer.verifyPageElements(
							Arrays.asList("btnToggle"), clPage),
					"<b> Actual Result 2: </b>  All the L2 category has a toggle icon",
					"<b> Actual Result 2: </b> All the L2 category does nto have a toggle icon");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_132

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected sizes in Size refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_153(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnSizeInRefinementMobile();
			}

			Log.message("<b> Expected Result 1: </b> The items should be displayed as per selected size");
			Log.assertThat(
					plppage.selectFirstSize(),
					"<b> Actual Result 1: </b> The items is displayed as per selected size",
					"<b> Actual Result 1: </b> The items is not displayed as per selected size");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnSizeInRefinementMobile();
			}

			Log.message("<b> Expected Result 2: </b> The items should be displayed as per one of those selected size");
			Log.assertThat(
					plppage.selectthirdSize(),
					"<b> Actual Result 2: </b> The items is displayed as per one of those selected size",
					"<b> Actual Result 2: </b> The items is not displayed as per one of those selected size");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_153

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Refinement count for Color under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_164(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");
			if (runPltfrm == "mobile") {
				searchResultPage.clickFilterByInMobile();
			}
			String[] selected_value = searchResultPage
					.selectUnselectFilterOptions("refinementColor")
					.split("\\(");
			String selected_color = selected_value[0].trim();
			Log.message("3. Selected the Color: '" + selected_color
					+ "' from FilterBy Option in the SearchListingPage");
			String selected_color_productCount = selected_value[1].replace(")",
					"").trim();
			Log.message("4. The available ProductCount for Selected Color: '"
					+ selected_color + "' is " + selected_color_productCount);

			int ProductCount = searchResultPage.getProductCountInPage();
			String productCount = "" + ProductCount;
			Log.message("5. After Selecting '" + selected_color
					+ "', The Total ProductCount in the SearchListingPage is "
					+ productCount);
			Log.message("<b>Expected Result:</b> The available products in stock for: '"
					+ selected_color
					+ "' Color should displays the value in parentheses next to the refinement value.");
			Log.assertThat((selected_color_productCount.equals(productCount)),
					"<b>Actual Result:</b> The available products in stock for: '"
							+ selected_color + "' Color displayed the value: '"
							+ productCount
							+ "' in parentheses next to the refinement value.",
					"<b>Actual Result:</b> The available products in stock for: '"
							+ selected_color
							+ "' Color not displayed the value: '"
							+ productCount
							+ "' in parentheses next to the refinement value.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_155

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Refinement count for Size under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_155(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			if (Utils.getRunPlatForm() == "mobile") {
				searchResultPage.clickFilterByInMobile();
			}

			String[] selected_value = searchResultPage
					.selectUnselectFilterOptions("refinementSize").split("\\(");
			Log.message("3. Selected Size from the size refinements pane in the SearchResult Page");
			String selected_size = selected_value[0].trim();
			String selected_size_productCount = selected_value[1].replace(")",
					"").trim();

			int ProductCount = searchResultPage.getProductCountInPage();
			String productCount = "" + ProductCount;

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> After Selecting size, the Refinement count for Size under product refinements should be same as Product Count in the SearchResult Page ");
			Log.assertThat(
					(selected_size_productCount.equals(productCount)),
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is same as Product Count in the SearchResult Page",
					"<b>Actual Result:</b> After Selecting size, the Refinement count for Size under product refinements is not same as Product Count in the SearchResult Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_155

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Not Selectable colors in Color refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_163(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("3. Navigated to PLP page (L3 Category)");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnColorInRefinementMobile();

			}
			plppage.clickonviewMoreColors();
			Log.message("<b> Expected Result 1: </b> Few color which do not have product should be disabled");
			Log.assertThat(
					plppage.elementLayer.verifyPageElements(
							Arrays.asList("UnSelectable"), plppage),
					"<b> Actual Result 1: </b> Few color which do not have product is disabled ",
					"<b> Actual Result 1: </b> Few color which do not have product is also enabled");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_163

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected brands in Brands refinement under product refinements panel for multiple brands", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_172(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to 'Belk' Category Landing Page for Search Category : "
					+ category[0]
					+ " >>> "
					+ category[1]
					+ " >>> "
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();

			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();
			}

			Log.message("<b> Expected Result 1: </b> The items should be displayed as per selected Brand");
			Log.assertThat(
					plppage.selectBrandOne(),
					"<b> Actual Result 1: </b> The items is displayed as per selected Brand",
					"<b> Actual Result 1: </b> The items is not displayed as per selected Brand");
			if (runPltfrm == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();
			}

			Log.message("<b> Expected Result 2: </b> The items should be displayed as per one of those selected Brand");
			Log.assertThat(
					plppage.selectSecondBrand(),
					"<b> Actual Result 2: </b> The items is displayed as per one of those selected Brand",
					"<b> Actual Result 2: </b> The items is not displayed as per one of those selected Brand");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_172

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays selected page opens in the center, allowing for easy navigation to the two previous and two next pages.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_075(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.clickOnSearchSuggestions(searchKey);
			Log.message("2. Clicked the search keyword from 'Search for \""
					+ searchKey + " \" ?' in the auto suggestion");
			String CurrentUrl = driver.getCurrentUrl();
			PlpPage plppage = new PlpPage(driver).get();
			plppage.clickOnPage3();
			searchResultPage.waitUntilSearchSpinnerDisappear();
			String UrlAfterClick = driver.getCurrentUrl();
			Log.message("<b> Expected Result 1: </b> The Page should be navigated to Page 3");
			Log.assertThat(
					(!(CurrentUrl.equals(UrlAfterClick))),
					"<b> Actual Result 1: </b>  The Page is navigated to Page 3",
					"<b> Actual Result 1: </b> The Page is not navigated to Page 3");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_075

	@Test(groups = { "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_444(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String l1category = testData.get("L1_category");
		String l2category = testData.get("L2_category");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(l1category, l2category);
			Log.message("2. Navigated to Search Result Page of category1: '"
					+ l1category + "' and category2: '" + l2category + "'");

			SearchResultPage searchresultpage = new SearchResultPage(driver)
					.get();
			homePage = searchresultpage.clickOnHome();
			Log.message("Clicked on 'Home' link in the breadcrumb");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Home Page should shown When we click on Home in the breadcrumb");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "/home")
							&& (homePage != null),
					"<b>Actual Result:</b> By Clicking the Home in the breadcrumb it is navigate to Home Page",
					"<b>Actual Result:</b> By Clicking the Home in the breadcrumb is not navigate to Home Page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_444

	@Test(groups = { "desktop", "mobile" }, description = "Verify women landing should display when we search women", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_955(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with L1 category('" + searchKey + "')");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> L1 category's('" + searchKey
					+ "') search results page should be display");

			Log.assertThat(
					searchResultPage.getProductsFound().contains(
							"Products Found"),
					"<b>Actual Result:</b> ('"
							+ searchKey
							+ "') Product details displayed in the search result page",
					"<b>Actual Result:</b> 'Page Not Found' error getting displayed when search with L1 category('"
							+ searchKey + "')", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH_955

	@Test(groups = { "mobile" }, description = "Verify 'Shop By Brand' breadcrumb should not display in the mobile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_1018(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrand = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link in the footer");
			Log.message("3. Navigated to 'List Of Brands' Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Shop by brand' should not shown in the Bread crumb ");

			Log.assertThat(
					listOfBrand.VerifyCategoryNameInBreadcrumb(),
					"<b>Actual Result:</b> 'Shop by brand' is not shown in the Bread crumb, It's displayed as 'Back To Home' ",
					"<b>Actual Result:</b> 'Shop by brand' is shown in the Bread crumb ",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_1018

	/*
	 * @Test(groups = { "desktop", "mobile", "tablet" }, description =
	 * "Verify system maintain the Refinement settings through the current search session until the user removes refinements or enters a new search"
	 * , dataProviderClass = DataProviderUtils.class, dataProvider =
	 * "parallelTestDataProvider") public void TC_BELK_SEARCH_039(String
	 * browser) throws Exception {
	 * 
	 * HashMap<String, String> testData = TestDataExtractor.initTestData(
	 * workbookName, sheetName);
	 * 
	 * String searchKey = testData.get("SearchKey"); String runPltfrm =
	 * Utils.getRunPlatForm(); String[] productFountCount = null; // Get the web
	 * driver instance final WebDriver driver = WebDriverFactory.get(browser);
	 * Log.testCaseInfo(testData);
	 * 
	 * try { HomePage homePage = new HomePage(driver, webSite).get();
	 * Log.message("1. Navigated to 'Belk' Home Page!");
	 * 
	 * SearchResultPage searchresultPage = homePage
	 * .searchProductKeyword(searchKey); Log.message("2. Search '" + searchKey +
	 * "' in the home page and  Navigated to 'Search Result'  Page!");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
	 * .split("-"); Log.message("3. Selected category: '" + CategoryName[0] +
	 * "' and Sub Category Refinement: '" + CategoryName[1] +
	 * "' from category Refinement");
	 * 
	 * if (runPltfrm == "mobile") { productFountCount =
	 * searchresultPage.getProductsFound().split("\\(");
	 * Log.message("4. Getting Product Count From ProductFount: "+
	 * productFountCount[1]); searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String color = searchresultPage
	 * .selectUnselectFilterOptions("refinementColor");
	 * Log.message("4. Selected color: " + color +
	 * " in the refinement under color section");
	 * 
	 * String[] selectedColorCount = color.split("\\(");
	 * Log.message("5. Selected color name: " + selectedColorCount[0] +
	 * " and color count (" + selectedColorCount[1] +
	 * " in the refinement under color section");
	 * 
	 * if (runPltfrm == "mobile") { searchresultPage.clickOnFilterByOption();
	 * Log.message("  Clicked on 'FilterBy' button in mobile"); }
	 * 
	 * String UnSelectcolor = searchresultPage
	 * .selectUnselectFilterOptions("refinementColor", selectedColorCount[0]);
	 * Log.message("7. UnSelected coloe: " + UnSelectcolor +
	 * " in the refinement under color section");
	 * 
	 * if (runPltfrm == "mobile") { Log.message("<br>");
	 * Log.message("<b>Expected Result:</b>" + productFountCount[1].replace(")",
	 * "") + " should be displayed in the search result page");
	 * 
	 * String[] pageCount = searchresultPage.getResultHitsText() .split("- ");
	 * Log.assertThat( pageCount[1].contains(productFountCount[1].replace(")",
	 * "")), "<b>Actual Result:</b> Selected sub category count : " +
	 * productFountCount[1] +
	 * "  product should display in the Search Result Page",
	 * "<b>Actual Result:</b> Selected sub category count : " +
	 * productFountCount[1] +
	 * " product is not display in the Search Result Page"); } else {
	 * 
	 * Log.message("<br>");
	 * Log.message("<b>Expected Result:</b> Un seleted color count: " +
	 * selectedColorCount[0] +
	 * " product should not be displayed in the search result page");
	 * 
	 * Log.assertThat( !searchresultPage.getBreadCrumbLastValue().contains(
	 * selectedColorCount[0].trim()),
	 * "<b>Actual Result:</b> Unselected color name: " + selectedColorCount[0] +
	 * " is not displayed in the bread crumb",
	 * "<b>Actual Result:</b> Unselected color name: " + selectedColorCount[0] +
	 * " is  displayed in the bread crumb pls check the event log", driver); }
	 * Log.testCaseResult(); } // try catch (Exception e) { Log.exception(e,
	 * driver); } // catch finally { Log.endTestCase(); driver.quit(); } //
	 * finally
	 * 
	 * }// TC_BELK_SEARCH_033
	 */

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the products in list view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_066(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		SearchResultPage searchResultPage;
		List<String> element = Arrays.asList("verifyListView");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			searchResultPage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Navigated to 'Belk' Search result Page!");

			Log.message("<b>Expected Result:</b> System should display the products in list view.");
			searchResultPage.clickGridListToggle("List");
			Log.assertThat(searchResultPage.elementLayer.verifyPageElements(
					element, searchResultPage),
					" <b>Actual Result:</b> Products are in list view !",
					" <b>Actual Result:</b> Products are not in list view !",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_066

	@Test(groups = { "desktop" }, description = "Verify that the '>' icon is clickable in Category Refinement Section on Search Results Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_144(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo("testData");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search "
					+ searchKey
					+ " in the home page and  Navigated to 'Search Result'  Page!");
			Log.assertThat(
					searchresultPage.verifyColorOfSizeRefinementText(),
					"3. The refinement heading Size is displayed  blue color in searchresultPage",
					"3. The refinement heading Size is not displayed as blue color in searchresultPage");

			String[] CategoryName = searchresultPage.selectCatgeoryRefinement()
					.split("-");
			Log.message("4.. Selecting ProductCategory from Refinement: "
					+ CategoryName[0]);
			Log.message(
					"5. navigated to the selected category landing page upon clicking on '>' for any category.",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_133

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion symbol '+' or '-' for Size refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_150(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");

		String runPltfrm = Utils.getRunPlatForm();
		String filterType = "refinementSize";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("<br>");
			Log.message("<u>Verifying PLP</u>");
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to PLP page", driver);
			if (runPltfrm == "mobile") {

				plppage.clickFilterByInMobile();
				plppage.clickCollapseExpandToggle(filterType);
			}
			Log.message("<br>");
			Log.message("3. Clicked expand toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> It should expand or collapse the Size refinement section in the page.");
			Log.assertThat(
					plppage.verifyExpandToggle(filterType),
					"  Filter type "
							+ filterType
							+ "<b>Actual Result: </b> list is expanded properly in plppage",
					"filter type "
							+ filterType
							+ "<b>Actual Result: </b> is not expanded  in plppage",
					driver);
			plppage.clickCollapseExpandToggle(filterType);

			Log.message("4. Clicked collapse toggle of '" + filterType + "'");

			Log.assertThat(!plppage.verifyExpandToggle(filterType),
					" <b>Actual Result: </b> Filter type " + filterType
							+ " is collapsed properly in plppage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not collapsed in plppage", driver);
			if (runPltfrm == "desktop") {
				plppage.clickCollapseExpandToggle(filterType);

				Log.message("5. Clicked expand toggle of '" + filterType + "'");
				Log.assertThat(
						plppage.verifyExpandToggle(filterType),
						"<b>Actual Result: </b>  Filter type " + filterType
								+ " list is expanded properly in plppage",
						"filter type "
								+ filterType
								+ "<b>Actual Result: </b> is not expanded  in plppage",
						driver);
			}

			Log.message("<u>Verifying Search Result Page</u>");

			if (runPltfrm == "mobile") {
				plppage.clickOnCloseIcon();

			}

			SearchResultPage searchresultPage = plppage.headers
					.searchProductKeyword(searchKey);
			Log.message("6. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(filterType);
			}
			Log.message("7. Clicked expand toggle of '" + filterType + "'");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> It should expand or collapse the Size refinement section in the page.");

			Log.assertThat(
					searchresultPage.verifyExpandToggle(filterType),
					"<b>Actual Result: </b>  Filter type " + filterType
							+ " list is expanded properly in searchresultPage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not expanded as default in searchresultPage",
					driver);

			searchresultPage.clickCollapseExpandToggle(filterType);
			Log.message("8. Clicked collapse toggle of '" + filterType + "'");

			Log.assertThat(searchresultPage.verifyExpandToggle(filterType),
					"<b>Actual Result: </b>  Filter type " + filterType
							+ " is collapsed properly in searchresultPage",
					"<b>Actual Result: </b> filter type " + filterType
							+ " is not collapsed in searchresultPage", driver);
			if (runPltfrm == "desktop") {
				searchresultPage.clickCollapseExpandToggle(filterType);
				Log.assertThat(searchresultPage.verifyExpandToggle(filterType),
						"  Filter type " + filterType
								+ " list is expanded properly in plppage",
						"filter type " + filterType
								+ " is not expanded  in plppage", driver);

				Log.message("<u>Verifying Category Landing Page</u>");
				searchresultPage.headers.navigateTo(category[0], category[1]);
				CategoryLandingPage clppage = new CategoryLandingPage(driver);
				if (runPltfrm == "mobile") {

					clppage.clickFilterByInMobile();
					clppage.clickCollapseExpandToggle(filterType);
				}
				Log.message("<br>");
				Log.message("6. Clicked collapse toggle of '" + filterType
						+ "'");
				Log.message("<br>");
				Log.message("<b>Actual Result: </b>");
				Log.assertThat(clppage.verifyExpandToggle(filterType),
						". filter type " + filterType
								+ " list is expanded properly in clppage",
						"filter type " + filterType
								+ " is not expanded as default in clppage",
						driver);
				clppage.clickCollapseExpandToggle(filterType);
				Log.message("7. Clicked collapse toggle of '" + filterType
						+ "'");
				Log.message("<br>");
				Log.message("<b>Actual Result: </b>");
				Log.assertThat(!clppage.verifyExpandToggle(filterType),
						"  Filter type " + filterType
								+ " is collapsed properly in clppage",
						"filter type " + filterType
								+ " is not collapsed in clppage", driver);
				if (runPltfrm == "desktop") {
					clppage.clickCollapseExpandToggle(filterType);
					Log.message("8. Clicked expand toggle of '" + filterType
							+ "'");
					Log.message("<br>");
					Log.message("<b>Actual Result: </b>");
					Log.assertThat(clppage.verifyExpandToggle(filterType),
							"  Filter type " + filterType
									+ " list is expanded properly in clppage",
							"filter type " + filterType
									+ " is not expanded  in plppage", driver);
				}

				if (runPltfrm == "mobile") {
					clppage.clickCloseIconInRefinementSection();

				}
				Log.message("<br>");
				Log.testCaseResult();
			}
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_SEARCH_139

	@Test(groups = { "mobile" }, description = "Verify the breadcrumbs shown, when Category Listing page(with no left navigation) is in L2 or L3 Category ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_240(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();
			String breadCrumbValue = "Back to ".concat(category[1]);

			Log.message("<br>");
			Log.message("<b>Expected Result : <b> '" + breadCrumbValue
					+ "' should be displayed in breadcrumb");
			Log.assertThat(
					plpPage.getTextInBreadcrumb().get(0)
							.equals(breadCrumbValue),
					"<b>Actual Result:</b> < '"
							+ breadCrumbValue
							+ "' is displayed properly in the breadcrumb in 'Search Result' Page",
					"<b>Actual Result:</b> < '" + breadCrumbValue
							+ "' is not displayed properly", driver);

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_SEARCH_240

	@Test(groups = { "mobile" }, description = "Mobile: Verify the breadcrumbs shown, when Category Listing page (with product grid) is an L1 Category", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_242(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "desktop")
			throw new SkipException(
					"This testcase(Breadcrumb verification) is not applicable for "
							+ runPltfrm);
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String category = testData.get("CategoryLevel");
		String txtBackToHome = "Back to Home";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigating to Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to Product Listing Page
			homePage.headers.navigateTo(category);
			Log.message("2. Navigated to Product Listing Page!");
			CategoryLandingPage categorylandingpage = new CategoryLandingPage(
					driver);
			ArrayList<String> txtBreadcrumb = categorylandingpage
					.getTextInBreadcrumb();
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> The breadcrumbs on mobile should have link to the previous level i.e., incase of L1 Category: '"
					+ category
					+ "' link the breadcrumbs should be 'Back to Home'");

			Log.assertThat(txtBreadcrumb.contains(txtBackToHome),
					"<b>Actual Result :</b> Breadcrumb: " + txtBreadcrumb
							+ " is properly shown in L1 Category: '" + category
							+ "'Page.", "<b>Actual Result :</b> Breadcrumb: "
							+ txtBreadcrumb
							+ " is not properly shown in L1 Category: '"
							+ category + "'Page.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_242

	@Test(groups = { "mobile" }, description = "Mobile: Verify the breadcrumbs shown, when Category Listing page(with product grid) is an L2 or L3 Category�", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_243(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String L1Category = testData.get("L1_category");
		String L2Category = testData.get("L2_category");
		String L3Category = testData.get("L3_category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(L1Category, L2Category, L3Category);
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver);
			Log.message("2. Navigated to CategoryLanding Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> When we navigate to sub category, then the breadcrumb on mobile should be displayed link that 'Back to Previous level Category'");

			ArrayList<String> BcValue = categoryLandingPage
					.getTextInBreadcrumb();
			String Category_Name = BcValue.get(BcValue.size() - 1);

			Log.assertThat(
					Category_Name.contains(L2Category),
					"<b>Actual Result:</b> When we navigate to sub category, then the breadcrumb on mobile is displayed link that 'Back to Previous level Category'",
					"<b>Actual Result:</b> When we navigate to sub category, then the breadcrumb on mobile is not displayed link that 'Back to Previous level Category'");

			Log.testCaseResult();

		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_SEARCH2B_243

	@Test(groups = { "desktop", "mobile" }, description = "Check whether the Search Pop-up does not reappear after changing the focus/by using esc key even with the search term in the field.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_012(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchterm = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.enterSearchTextBox(searchterm);
			Log.message("2. Entered search keyword '" + searchterm
					+ "' in the search box");

			homePage.closeSearchSuggestionPanelByEsckey();
			Log.message("3. 'ESC' Key is pressed.");
			Log.message("<b>Expected Result:</b> Search Pop-up pane should not reappear after changing the focus/by using esc key even when the search term is retained in the field.");
			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("autoSuggestPopup"), homePage)
							& homePage.getEnteredTextFromSearchTextBox()
									.equals(searchterm),
					"<b>Actual Result:</b> Search Pop-up pane did not reappear after changing the focus/by using esc key even when the search term is retained in the field.",
					"<b>Actual Result:</b> Search Pop-up pane reappear after changing the focus/by using esc key even when the search term is retained in the field.",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_012

	@Test(groups = { "desktop", "mobile" }, description = "Verify color swatches for variation products in product tiles .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_184(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page!");
			Log.message("3. Hover on the 2nd color swatch");
			String colorswatchsrc2 = searchResultPage.hoverColorOnProductTile(
					1, 2);
			BrowserActions.nap(3);
			String primaryimagesrc2 = searchResultPage.getPrimaryImageColor();
			Log.message("4. Hover on the 3rd color swatch");
			String colorswatchsrc3 = searchResultPage.hoverColorOnProductTile(
					1, 3);
			BrowserActions.nap(3);
			String primaryimagesrc3 = searchResultPage.getPrimaryImageColor();
			Log.message("<b>Expected Result:</b> Hovering over a color swatch should change the product thumbnail Image color.");
			Log.assertThat(
					colorswatchsrc2.split("&crop")[0].equals(primaryimagesrc2)
							& colorswatchsrc3.split("&crop")[0]
									.equals(primaryimagesrc3),
					"<b>Actual Result:</b> Hovering over a color swatch changed the product thumbnail Image color.",
					"<b>Actual Result:</b> Hovering over a color swatch did not change the product thumbnail Image color.",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_SEARCH_184

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Exclusive Brand Asset Banner in Shop by Brand page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_257(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ListOfBrandPage listOfBrandPage = homePage.footers
					.navigateToListBrand();
			Log.message("2. Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see that the Exclusive Brand Asset banner is displayed in Shop by Brand page as configured in Business Manager.");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					listOfBrandPage.elementLayer.verifyPageElements(
							Arrays.asList("lblExclusiveBrandAssetBanner"),
							listOfBrandPage),
					"Exclusive Brand Asset banner is displayed in Shop by Brand page",
					"Exclusive Brand Asset banner is not displayed in Shop by Brand page");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_257

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Shop by Brand page - Search box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_258(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i=1;
			Log.message((i++)+". Navigated to 'Belk' Home Page!");
			ListOfBrandPage listofBrand = homePage.footers
					.navigateToListBrand();
			Log.message((i++)+". Clicked on 'List Of Brands' link and navigated to List of Brands page");
			Log.message((i++)+". Observed the search box in Shop by Brands page");	
			Log.message("<br>");					
			Log.message("<b>Expected Result:</b> Search-Textbox should be displayed above the list of brands.");
			Log.assertThat(listofBrand.verifyTxtSearchBrandAppearsAboveListofBrands(),
							"<b>Actual Result:</b> Search-Textbox is displayed above the list of brands",
							"<b>Actual Result:</b> Search-Textbox is not displayed above the list of brands",
							driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_258

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the accordion (+) / (­) symbol for Color refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_159(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message(" 3. Clicked on 'FilterBy' button in mobile");
				Log.message("<br>");
				Log.message("<b>Expected Result 1: </b>It should expand or collapse the Color refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("btncolor"), searchResultPage),
						"<b>Actual Result 1a: </b>Color Refinement Collapsed by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result 1a: </b>Color Refinement is not Collapsed by Default",
						driver);

				searchResultPage.colorToggle(true);
				Log.message("</br>");
				Log.message("4. Color toggle expanded!</br>");
				Log.message("<br>");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("viewColorExpand"),
								searchResultPage),
						"<b>Actual Result 1b: </b> Color Refinement Expanded and Accordion symbol(+) should be Present",
						"<b>Actual Result 1b: </b> Color Refinement is not Expanded",
						driver);
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>It should expand or collapse the Color refinement section in the page.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("viewColorExpand"),
								searchResultPage),
						"<b>Actual Result 1a: </b> Color Refinement Expanded by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result 1a: </b>ColorRefinement is not Expanded by Default",
						driver);

				searchResultPage.colorToggle(false);
				Log.message("<br>3. Color toggle expanded!<br>");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("viewColorExpand"),
								searchResultPage),
						"<b>Actual Result 1b: </b> Color Refinement Collapsed and Accordion symbol(+) should be Present",
						"<b>Actual Result 1b: </b> Color Refinement is not Expanded by Default",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_159

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Expand/Collapse option for Color refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_158(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("3.  Clicked on 'FilterBy' button in mobile");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>The Color refinement should be expanded by default.");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("btncolor"), searchResultPage),
						"<b>Actual Result: </b>Color Refinement Collapsed by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>Color Refinement is not Collapsed by Default",
						driver);
				searchResultPage.colorToggle(true);

				Log.message("<br>3. Color toggle Clicked!<br>");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>An accordion (+/­) symbol should be displayed for the Color refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("viewColorExpand"),
								searchResultPage),
						"<b>Actual Result: </b>Price Refinement Should be Expanded and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Price Refinement is not Expanded",
						driver);
			} else {
				Log.message("<b>Expected Result: </b>The size refinement should be expanded by default.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("viewColorExpand"),
								searchResultPage),
						"<b>Actual Result: </b>Color Refinement Expanded by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>color Refinement is not Expanded by Default",
						driver);
				searchResultPage.colorToggle(false);

				Log.message("<br> Color toggle collapsed!<br>");
				Log.message("<br><b>Expected Result: </b> An accordion (+/­) symbol should be displayed for the Color refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("btncolor"), searchResultPage),
						"<b>Actual Result: </b>Color Refinement Collapsed and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Color Refinement is not Expanded",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_158

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Expand/Collapse option for Brands refinement in product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_167(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			if (runPltfrm == "mobile") {
				searchResultPage.clickOnFilterByOption();
				Log.message("3.  Clicked on 'FilterBy' button in mobile");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>The Brand refinement should be expanded by default.");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleClosed"),
								searchResultPage),
						"<b>Actual Result: </b>Brand Refinement Collapsed by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>Brand Refinement is not Collapsed by Default",
						driver);
				searchResultPage.brandToggle(true);

				Log.message("<br> Brand toggle Clicked!<br>");
				Log.message("</br>");
				Log.message("<b>Expected Result: </b>An accordion (+/­) symbol should be displayed for the Brand refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleExpanded"),
								searchResultPage),
						"<b>Actual Result: </b>Brand Refinement Should be Expanded and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Brand Refinement is not Expanded",
						driver);
			} else {
				Log.message("<b>Expected Result: </b>The brand refinement should be expanded by default.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleExpanded"),
								searchResultPage),
						"<b>Actual Result: </b>Brand Refinement Expanded by Default and Accordion symbol(-) should be Present",
						"<b>Actual Result: </b>Brand Refinement is not Expanded by Default",
						driver);
				searchResultPage.brandToggle(false);

				Log.message("<br> Brand toggle collapsed!<br>");
				Log.message("<br><b>Expected Result: </b> An accordion (+/­) symbol should be displayed for the brand refinement option.");
				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								Arrays.asList("brandToggleClosed"),
								searchResultPage),
						"<b>Actual Result: </b>Brand Refinement Collapsed and Accordion symbol(+) should be Present",
						"<b>Actual Result: </b>Brand Refinement is not Expanded",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_167

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selected colors in Color refinement under product refinements panel for multiple colors.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_162(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> toogleActive = Arrays.asList("toogleActivelnk");
		int countBeforeRefine;
		int countAfterRefine;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// Get the product count before refine
			countBeforeRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("3. Before refinement product count is :"
					+ countBeforeRefine);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			String colorInFirstTry = searchresultPage
					.selectSwatchColorFromRefinmentByIndex(2);
			Log.message(
					"4. Filtered the products by color :" + colorInFirstTry,
					driver);
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}

			String colorIn2ndTry = searchresultPage
					.selectSwatchColorFromRefinmentByIndex(4);
			Log.message("5. Filtered the products by color :" + colorIn2ndTry,
					driver);
			// Get the product count after refine
			countAfterRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("6. After refinement product count is :"
					+ countAfterRefine);
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("The Color refinement option should displays an orange checkbox next to the item indicating that the color has been selected.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			if (Utils.getRunPlatForm() == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickOnViewColorExpandForMobileView();
			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							toogleActive, searchresultPage),
					"The Color refinement option displays an orange checkbox next to the item indicating that the color has been selected !",
					"The Color refinement option not displays an orange checkbox next to the item indicating that the color has been selected !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("When customers selects multiple colors, only those products having one of those colors should displays in the product grid.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					countBeforeRefine > countAfterRefine,
					"After selecting multiple color, the product grid refreshed to show the products that match the selected color refinement !",
					"After selecting multiple color, the product grid not refreshed to show the products that match the selected color refinement !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_162

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Selecting brands in Brands refinement under product refinements panel for a single brand.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_171(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '" + searchKey
					+ "' and navigated to SearchResultPage !");
			// Get the product count before refine
			int countBeforeRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("3. Before refinement product count is :"
					+ countBeforeRefine);
			PlpPage plppage = new PlpPage(driver).get();

			if (Utils.getRunPlatForm() == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();
			}

			plppage.selectFirstBrand();
			Log.message("4. Selected Brand and Filtered the SearchResult !");

			// Get the product count after refine
			int countAfterRefine = Integer.parseInt(searchresultPage
					.getResultHitsText().replace(",", "").split("of ")[1]
					.trim());
			Log.message("5. After refinement product count is :"
					+ countAfterRefine);
			if (Utils.getRunPlatForm() == "mobile") {
				plppage.clickFilterByInMobile();
				plppage.clickOnBrandInRefinementMobile();

			}
			plppage.ExpandFirstBrand();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("The Brand refinement option should displays an orange checkbox next to the item indicating that the brand has been selected.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageElements(
							Arrays.asList("toogleActive"), searchresultPage),
					"The brand refinement option displays an orange checkbox next to the item indicating that the color has been selected !",
					"The brand refinement option not displays an orange checkbox next to the item indicating that the color has been selected !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message(" After selecting an available brand, the product grid should refresh to show the products that match the selected brand refinement.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			Log.assertThat(
					countBeforeRefine > countAfterRefine,
					"After selecting an available brand, the product grid refreshed to show the products that match the selected color refinement !",
					"After selecting an available brand, the product grid not refreshed to show the products that match the selected color refinement !",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_SEARCH_171

	@Test(groups = { "mobile" }, description = "Mobile : Verify system displays the breadcrumb only to the previous page in SRP list view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_270(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "mobile")) {
			throw new SkipException(
					"This test case is applicable only for mobile");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			searchresultPage.clickOnFilterByOption();
			Log.message("3. Clicked on 'FilterBy' button in mobile");

			String productValue = searchresultPage.getBreadCrumbProductValue()
					.replace("Back to", "");
			Log.message("4. Fetching Category from BreadCrumb: '"
					+ productValue + "'");

			searchresultPage.clickOnListToggleIcon();
			Log.message("5. Clicked on List Toggle");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> System should display the breadcrumb only to the previous page. E.g Back to Previous page.");

			Log.assertThat(productValue.trim().equals("Search Results"),
					"<b>Actual Result:</b> The breadcrumb to the previous page 'Back to "
							+ productValue + "' is displayed",
					"<b>Actual Result:</b> The breadcrumb to the previous page 'Back to "
							+ productValue + "' is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_029

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system hide the refinement by hidden setting a custom preference", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_119(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<b>Expected Result:</b> System should not display the refinemnt section");
			Log.assertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lstRefinement"), searchResultPage),
					"<b>Actual Result:</b> Refinement section is displayed",
					"<b>Actual Result:</b> Refinement section is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_156

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the content asset id must match the pattern keyword, promo, promo event code and must be assigned to the Search Banner folder. If the value has not match to the content asset ID, the platform displays that global banner instead of the search slot banner.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_115(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			Log.message("<b>Expected Result:</b> Global banner slot should be displayed instead of search slot banner");
			Log.assertThat(
					searchResultPage.elementLayer
							.verifyPageElements(
									Arrays.asList("globalSlotBanner"),
									searchResultPage),
					"<b>Actual Result:</b> Global banner slot is displayed instead of search slot banner",
					"<b>Actual Result:</b> Global banner slot is not displayed instead of search slot banner",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SEARCH_156
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Brand Accordion section is shown in Shop by Brand page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SEARCH_261(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		String runPltfrm = Utils.getRunPlatForm();
		Log.testCaseInfo(testData);

		try {
			int i =1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++)+". Navigated to 'Belk' Home Page!");
			ListOfBrandPage listofBrand = homePage.footers.navigateToListBrand();
			Log.message((i++)+". Clicked on 'List of Brands' link on footer.");
			Log.message((i++)+". Scrolled down on Shop by Brand page.");			
			Log.message("<br>");		
			
			if (runPltfrm != "mobile") 
			{
								
				Log.message("<b>Expected Result:</b>");
				Log.message(" a. An accordion menu with Brands split up alphabetically with alphabet in bold and a '+' next to the alphabet.");
				Log.message(" b. The first letter in the accordion should be expanded by default.");
				Log.message("<br>");
				Log.message("<b>Actual Result:</b>");
				Log.assertThat(listofBrand.verfiyBrandHeadingsOrderedAndFormatted(),
						"An accordion menu with Brands is splitted up alphabetically in bold",
						"An accordion menu with Brands is not splitted up alphabetically in bold",
						driver);
				Log.assertThat(listofBrand.elementLayer.verifyPageElements(
							Arrays.asList("lblActiveBrandHead"), listofBrand),
							"The first letter in the accordion is expanded and a '+' next to the alphabet.",
							"The first letter in the accordion is not expanded",
							driver);
				}
			if (runPltfrm == "mobile") 
			{
				
				Log.message("<b>Expected Result:</b>");
				Log.message(" a. An accordion menu with Brands split up alphabetically with alphabet in bold and a '+' next to the alphabet.");
				Log.message(" b. The first letter in the accordion should not be expanded by default in mobile ");
				Log.message("<br>");
				Log.message("<b>Actual Result:</b>");
				Log.assertThat(listofBrand.verfiyBrandHeadingsOrderedAndFormatted(),
						"An accordion menu with Brands is splitted up alphabetically",
						"An accordion menu with Brands is not splitted up alphabetically",
						driver);
				Log.assertThat(listofBrand.elementLayer.verifyPageElements(
							Arrays.asList("lblFirstBrandHeadMobile"), listofBrand),
							"The first letter in the accordion is not expanded in mobile",
							"The first letter in the accordion is expanded in mobile",
							driver);
			}
		
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_SEARCH_261

}
