package com.belk.testscripts.productdetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CategoryLandingPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.EnlargeViewPage;
import com.belk.pages.FindInStorePage;
import com.belk.pages.HomePage;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.ProfilePage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.DataProviderUtils;
import com.belk.support.DateTimeUtility;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class PDPFinal {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "PDP";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop"}, description = "Verify Product Breadcrum Navigate to particular Pdp Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_002(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm != "desktop")
			throw new SkipException("This testcase(Breadcrumb verification) is not applicable for " + runPltfrm);
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		ArrayList<String> breadcrumbText = new ArrayList<String>();
		ArrayList<String> breadcrumbTextInPdp = new ArrayList<String>();
		ArrayList<String> categoryText = new ArrayList<String>();

		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to " + category[0] + ">" + category[1] + ">" + category[2] + " page.");

			CategoryLandingPage catagoryLandingPage = new CategoryLandingPage(driver);
			
			breadcrumbText = catagoryLandingPage.getTextInBreadcrumb();
			
            Log.message("3. Navigated to Pdp Page");
			
			
			PdpPage pdpPage = catagoryLandingPage.navigateToPDPWithProductId();
			breadcrumbTextInPdp = pdpPage.getTextInBreadcrumb();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Product BreadCrum Should be Displayed with the Navigation path");
            Log.assertThat(breadcrumbTextInPdp.get(0).equals(breadcrumbText.get(0))
							&& breadcrumbTextInPdp.get(1).equals(breadcrumbText.get(1))
							&& breadcrumbTextInPdp.get(2).equals(breadcrumbText.get(2))
							&& breadcrumbTextInPdp.get(3).equals(breadcrumbText.get(3))
							&& breadcrumbTextInPdp.get(4).contains(pdpPage.getProductName()),
					"<b>Actual Result:</b> Product BreadCrum : " + breadcrumbTextInPdp
							+ " is Displayed with the Navigation path",
					"<b>Actual Result:</b> Product BreadCrum : " + breadcrumbTextInPdp
							+ " is Displayed with the Navigation path",driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_002
	
	@Test(groups = { "desktop", "mobile" }, description = "To verify the product name on PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

			String productNameFromBredcrumb = pdpPage
					.getProductNameFromBreadCrumb();
			Log.message("4. Getting ProductName from  breadcrumb: '"
					+ productNameFromBredcrumb + "'in 'Pdp' page");

			String productName = pdpPage.getProductName();
			Log.message("5. Getting  productName: '" + productName
					+ "' below the BrandName in 'PDP' Page");

			if (Utils.getRunPlatForm() != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b>Product Name: '"
						+ productName + "' should be shown in 'PDP' Page");
				Log.assertThat(productNameFromBredcrumb.trim().contains(productName.trim()),
						"<b>Actual Result:</b> Product Name '" + productName
								+ "' is shown  in 'PDP' Page",
						"<b>Actual Result:</b> Product Name '" + productName
								+ "' is  not shown  in 'PDP' Page", driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_009

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays Price in PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_014(String browser) throws Exception {
		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			// Navigating to PDP page
			PdpPage pdpPage = plpPage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP");

			// Getting the standard price before selecting Qty
			String standardPrice_beforeChangingQty = pdpPage.getStandardPrice();
			Log.message("4. The price displayed before selecting Qty is: "
					+ standardPrice_beforeChangingQty);

			// Selecting Quantity
			pdpPage.selectQuantity("2");
			Log.message("5. Selected Qty value '2' from the drop down");

			// Getting the standard price after selecting Qty
			String standardPrice_afterChangingQty = pdpPage.getStandardPrice();
			Log.message("6. The price displayed after selecting Qty is: "
					+ standardPrice_afterChangingQty);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Pricing should be displayed appropriately based on the quantities!");

			Log.assertThat(
					standardPrice_beforeChangingQty
							.equals(standardPrice_afterChangingQty),
					"<b>Actual Result:</b> Pricing displayed appropriately based on the quantities!",
					"<b>Actual Result:</b> Pricing not displayed appropriatley on selecting different Qty values",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_011

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays Color of the product in PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String category[] = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			// get the image url of 1st product
			String plpProductImageColor = plpPage.getProductImageUrlByIndex(2)
					.split("&layer")[0];
			// Select the product according to search keyword
			PdpPage pdpPage = plpPage.selectProductByIndex(2);
			Log.message("3. Clicked 2nd product and navigated to PDP from PLP !");

			// get the primary image url
			String pdpImageColor = pdpPage.getPrimaryImageColor();
			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Color of the product displayed in PDP should be same as the color selected in PLP");
			Log.assertThat(
					plpProductImageColor.equals(pdpImageColor),
					"<b>Actual Result : </b>Color of the product: '"
							+ pdpImageColor
							+ "' displayed in PDP <b>is</b> same as the color selected in PLP: '"
							+ plpProductImageColor + "'",
					"<b>Actual Result : </b>Color of the product: '"
							+ pdpImageColor
							+ "' displayed in PDP <b>not</b> same as the color selected in PLP: '"
							+ plpProductImageColor + "'", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_014

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system changes the primary/faceout image depending upon color swatch selection", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_021(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchkey = testData.get("SearchKey");
		String primaryImageSrc = null;
		String colorSwatchSrc = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchkey);
			Log.message("2. Searched with keyword '" + searchkey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Color should be changed on primary/faceout image depending upon the selection on color swatches");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			for (int i = 1; i <= pdpPage.getColorSwatchCount("all"); i++) {
				pdpPage.selectColorByIndex(i, "all");
				primaryImageSrc = pdpPage.getPrimaryImageColor();
				colorSwatchSrc = pdpPage.getSelectedSwatchColor(i, "all");
				// Verify message
				Log.assertThat(
						primaryImageSrc.equals(colorSwatchSrc),
						"Selected color: <br>'" + primaryImageSrc
								+ "'<br> is displaying with same color: <br>'"
								+ primaryImageSrc + "'<br> for swatch : " + i,
						"Selected color: <br>'"
								+ primaryImageSrc
								+ "'<br> is not displaying with same color: <br>'"
								+ primaryImageSrc + "'<br> swatch : " + i,
						driver);
			}
			Log.message(
					"Color changed on primary/faceout image depending upon the selection on color swatches",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_016

	@Test(groups = { "desktop", "tablet" }, description = "Verify system disables the Add to Bag button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_027(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String size = "38 x 30";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");

			Log.message("3. Navigated to 'Pdp' Page of selected product");

			String selectedSize = pdpPage.selectSize(size);
			Log.message("4. Selected size: '" + selectedSize
					+ "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName
					+ "' from color swatch in the PDP page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Add to bag Button should be disabled when selecting 'Gray out' color in the color swatch");

			Log.assertThat(
					!pdpPage.verifyAddtoBagIsEnabled(),
					"<b>Actual Result:</b> Add to bag Button is disabled when selecting Gray out product in the 'Pdp' Page",
					"<b>Actual Result:</b> Add to bag Button is enabaled when selecting Gray out product in the 'Pdp' Page plz check the event loag",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_022

	@Test(groups = { "desktop", "mobile" }, description = "Verify system by default select the color and size of the product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_028(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Pdp' Page with ProductId");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'By default Color' and 'Size' Should be selected in the 'PDP' page if the product has only one 'Color' and 'Size'");

			Log.assertThat(
					pdpPage.verifyAddtoBagIsEnabled(),
					"<b>Actual Result:</b> By default 'Color' and 'Size' is selected in the 'PDP' page",
					"<b>Actual Result:</b> By default Color' and 'Size' is not select in the 'PDP' page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_023

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the size drop down in ascending order.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_029(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with searched product");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking the 'Size' drop down, 'Values' should shown in the ascending order");

			Log.assertThat(
					pdppage.checkSizeOrder(),
					"<b>Actual Result:</b> By Clicking the 'Size' drop down, 'Values' are shown in the ascending order",
					"<b>Actual Result:</b> By Clicking the 'Size' drop down, 'Values' not shown in the ascending order",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_024

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system display the Badge images", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_036(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> productBadge = Arrays.asList("productBadge");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Entered keyword '" + searchKey
					+ "' in the search box");

			PdpPage pdpPage = searchResultpage.navigateToPDPWithProductId();
			Log.message("3. Navigated to PDP of the product searched");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Product Badge should be displayed below the product in Product Details Page");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(productBadge,
							pdpPage),
					"<b>Actual Result:</b> Product Badge is displayed below the product in Product Details Page",
					"<b>Actual Result:</b> Product Badge is not displayed below the product in Product Details Page)",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_030

	// TC_BELK_PDP_035
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify social media on PDP and allow user to click.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> socialMedia = Arrays.asList("lnkPintrest", "lnkFacebook",
				"lnkTwitter", "lnkGooglePlus", "lnkEmail");

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to pdpPage with randomly selected product");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' should shown in the 'PDP'");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(socialMedia,
							pdpPage),
					"<b>Actual Result1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' are displayed in the 'PDP'",
					"<b>Actual Result1:</b> Social Media icons are not displayed in the 'PDP'",
					driver);
			/*
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result2:</b> By Clicking on 'Pinterest' Icon, It should navigate to corresponding 'Pinterest' page"
			 * );
			 * 
			 * // Load Pinterest Page pdpPage.navigateToPinterestPage();
			 * Log.softAssertThat(socialLinks[2].replaceAll(" ",
			 * "").contains("pinterest"),
			 * "<b>Actual Result2:</b>  By Clicking on Pinterest Icon, It is navigated to 'Pintrest' page "
			 * ,
			 * "<b>Actual Result2:</b>  By Clicking on Pinterest Icon, It is not navigated to 'Pintrest' page "
			 * , driver); BrowserActions.navigateToBack(driver);
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result3:</b> By Clicking on 'FaceBook' Icon, It should navigate to corresponding 'FaceBook' page"
			 * );
			 * 
			 * pdpPage.navigateToFacebookPage();
			 * Log.softAssertThat(socialLinks[0].replaceAll(" ",
			 * "").contains("facebook"),
			 * "<b>Actual Result3:</b> By Clicking on FaceBook Icon, It is navigated to 'FaceBook' page"
			 * ,
			 * "<b>Actual Result3:</b> By Clicking on FaceBook Icon, It is not navigated to 'FaceBook' page"
			 * , driver); BrowserActions.navigateToBack(driver);
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result4:</b> By Clicking on 'Twitter' Icon, It should navigate to corresponding 'Twitter' page"
			 * );
			 * 
			 * pdpPage.navigateToTwitterPage();
			 * Log.softAssertThat(socialLinks[1].replaceAll(" ",
			 * "").contains("twitter"),
			 * "<b>Actual Result4:</b> By Clicking on Twitter Icon, It is navigated to 'Twitter' page "
			 * ,
			 * "<b>Actual Result4:</b> By Clicking on Twitter Icon, It is not navigated to 'Twitter' page"
			 * , driver);
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result5:</b> By Clicking on 'GooglePlus' Icon, It should navigate to corresponding 'GooglePlus' page"
			 * );
			 * 
			 * pdpPage.navigateToGooglePlusPage();
			 * Log.softAssertThat(socialLinks[5].replaceAll(" ",
			 * "").contains("google"),
			 * "<b>Actual Result5:</b> By Clicking on GooglePlus Icon, It is navigated to 'GooglePlus' page"
			 * ,
			 * "<b>Actual Result5:</b> By Clicking on GooglePlus Icon, It is not navigated to 'GooglePlus' page"
			 * , driver); BrowserActions.navigateToBack(driver);
			 */
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_035

	@Test(groups = { "desktop", "mobile" }, description = "Verify system enables add to bag button in the PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_055(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to pdpPage with randomly selected product");

			String selectedColor = pdpPage.selectColor();
			Log.message("4. Selected Color: '" + selectedColor
					+ "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("5. Selected size: '" + selectedSize
					+ "' from the size drop down in the PDP Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'ADDToBAg' button should be Enabled, After selecting 'Size', 'Color' in the 'PDP' Page");

			Log.assertThat(
					pdpPage.verifyAddtoBagIsEnabled(),
					" <b>Actual Result:</b> 'ADDToBAg' button is Enabled, After selecting 'size', 'color' in the PDP Page ",
					"<b>Actual Result:</b> 'ADDToBAg' button is not Enabled, After selecting 'size', 'color' in the PDP Page");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_045

	@Test(groups = { "desktop", "mobile" }, description = "Verify - add to registry as guest user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_056(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("3. Naviagte to pdpPage with randomly selected product");

			String selectedColor = pdpPage.selectColor();
			Log.message("4. Selected Color: '" + selectedColor
					+ "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("5. Selected size: '" + selectedSize
					+ "' from the size drop down  in the PDP Page");

			RegistryGuestUserPage registryGuest = pdpPage
					.navigateToRegistryAsGuest();
			Log.message("6. Clicked on 'AddToRegistry' link in the PDP Page as a 'Guest user' ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking on 'AddToRegistry' button in pdp page as a Guest user, It should redirect to 'Registry' Page ");
			Log.assertThat(
					registryGuest != null,
					"<b>Actual Result:</b> By Clicking on 'AddToRegistry' button in pdp page as a Guest user, It is redirect to 'Registry' Page  "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> By Clicking on 'AddToRegistry' button in pdp page as a Guest user, It is not redirect to 'Registry' Page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_046

	@Test(groups = { "desktop", "mobile" }, description = "Verify - add to registry as registered user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_057(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("5. Naviagte to pdpPage with randomly selected product");

			String selectedColor = pdpPage.selectColor();
			Log.message("6. Selected Color: '" + selectedColor
					+ "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("7. Selected size: '" + selectedSize
					+ "' from the size drop down  in the PDP Page");

			RegistrySignedUserPage registrySignedPage = pdpPage
					.navigateToRegistryAsSignedUser();
			Log.message("8. Clicked on 'AddToRegistry' link in the PDP Page as a 'Signed user' ");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking on 'AddToRegistry' button in pdp page as a 'Signed user', It should Navigate to 'Registry' Signed user page");

			Log.assertThat(
					registrySignedPage != null,
					"<b>Actual Result:</b> By Clicking on 'AddToRegistry' button in pdp page as a 'Signed user', It is Navigate to 'Registry' Signed user page "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b>By Clicking on 'AddToRegistry' button in pdp page as a 'Signed user', It is not Navigate to 'Registry' Signed user page ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_047

	@Test(groups = { "desktop", "mobile" }, description = "Verify - add products to registry as a register user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_059(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lblAddedRegistry = "Item Added to Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("4. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("5. Naviagte to pdpPage with randomly selected product");

			String selectedColor = pdpPage.selectColor();
			Log.message("6. Selected Color: '" + selectedColor
					+ "' from the color swatches in the PDP Page");

			String selectedSize = pdpPage.selectSize();
			Log.message("7. Selected size: '" + selectedSize
					+ "' from the size drop down  in the PDP Page");

			pdpPage.clickOnRegistrySign();
			Log.message("8. Click on 'AddToRegistry' button in the PDP Page as a 'Registry user' ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Clicking on 'AddToRegistry' button in the PDP page as a 'Registry user', It should shown Product added successfully message");

			Log.assertThat(
					pdpPage.getTextFromAddedRegistry().contains(
							lblAddedRegistry),
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button in the PDP page, it shows :"
							+ pdpPage.getTextFromAddedRegistry() + " message",
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button in the PDP page, it does not show :"
							+ pdpPage.getTextFromAddedRegistry() + " message",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_048

	@Test(groups = { "desktop", "mobile" }, description = "Verify the close icon in the enlarge modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_067(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to '" + searchKey
					+ "' search result Page!", driver);

			PdpPage pdppage = searchResultPage.selectProductByIndex(2);
			Log.message(
					"3. Navigated to PDP of the 2 nd product from search results page",
					driver);

			EnlargeViewPage enlargeViewPage = pdppage.verifyClickToEnlarge();
			Log.message("4. Clicked 'click to enlarge' link");

			enlargeViewPage.closeEnlargeViewByCloseButton();
			Log.message("5. Clicked 'X' icon  in Enlarge View popup");
			pdppage = new PdpPage(driver).get();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Enlarge view pop up should be closed by 'X' icon ");
			Log.assertThat(
					pdppage != null,
					"<b>Actual Result:</b> 'Enlarge view pop up' is closed by 'X' icon",
					"<b>Actual Result:</b> 'Enlarge view pop up' is not closed by 'X' icon",
					driver);

			pdppage.verifyClickToEnlarge();
			Log.message("<br>6. Clicked 'click to enlarge' link");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Enlarge view pop up should be closed by pressing ESC button ");
			enlargeViewPage.closeEnlargeViewByEsckey();
			Log.assertThat(
					pdppage != null,
					"<b>Actual Result:</b> 'Enlarge view pop up' is closed by pressing ESC button",
					"<b>Actual Result:</b> 'Enlarge view pop up' is not closed by pressing ESC button",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_056

	/*
	 * Blocker: Video thumbnails are not displaying now in PDP So this script is
	 * Partially automated
	 */
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the alternate images in the enlarge modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_065(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to '" + searchKey
					+ "' search result Page!", driver);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.selectProductByIndex(1);
			Log.message("3. Navigated to PDP from Search results page !");

			String selectedColor = pdpPage.getSelectedColor();
			Log.message("4. Color: '" + selectedColor + "' is selected");
			int nosOfThumbs = pdpPage.getImageThumbnailsCount();

			// Clicking on Click-To-Enlarge
			pdpPage.clickToEnlarge();
			Log.message("5. Clicked on 'Click to Zoom' Link!");

			String selectedColorInModal = pdpPage.getSelectedColor();
			int nosOfThumbsInModal = pdpPage.getImageThumbnailsCount();

			Log.message("</br>");
			Log.message("<b>Expected Result : </b>Enlarge modal should display all the alternate product images for the active color selected");
			Log.assertThat(
					selectedColor.equals(selectedColorInModal)
							&& (nosOfThumbs == nosOfThumbsInModal),
					"<b>Actual Result : </b>Enlarge modal displayed wilt all the alternate product images for the active color selected!",
					"<b>Actual Result : </b>Enlarge modal not displayed as Expected");

			/*
			 * Log.message("</br>"); Log.message("<b>Expected Result : </b>");
			 * Log.assertThat(true, "<b>Actual Result : </b>",
			 * "<b>Actual Result : </b>");
			 */

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_065
	
	@Test(groups = { "desktop", "tablet" }, description = "Verify Breadcrumb in the Gift Card details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider", enabled = false)
	public void TC_BELK_PDP_079(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase (Breadcrumb full path) is not applicable in mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> breadcrumbTextActual = new ArrayList<>();
		List<String> breadcrumbTextExpected = new ArrayList<>();
		breadcrumbTextExpected.add("Home");
		breadcrumbTextExpected.add("Gift Card Category");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Entered keyword '" + searchKey
					+ "' and the page is navigated to search result page");

			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Naviagte to PDP Page with randomly selected Gift card product");
			breadcrumbTextExpected.add(pdpPage.getProductName());
			breadcrumbTextActual = pdpPage.getTextInBreadcrumb();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>'" + breadcrumbTextExpected
					+ "' should shown in the bread crumb");
			Log.assertThat(breadcrumbTextActual.equals(breadcrumbTextExpected),
					"<b>Actual Result:</b> '" + breadcrumbTextExpected
							+ "' is display in the bread crumb",
					"<b>Actual Result:</b> '" + breadcrumbTextActual
							+ "' is not display in the bread crumb", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_079

	@Test(groups = { "desktop", "tablet" }, description = "Verify Clearing the Brand Refined list is Not Navigating to Home page. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_BE_1092(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Naviagte to 'quick View' Page with Gift card"
					+ searchKey + "product");

			SearchResultPage searchResultpage = pdpPage.clickBrandname();
			Log.message("4. By clicking Brand name it is redirect to Search result Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Home Page should shown When we click on 'X' button in the bread crumb");

			searchResultpage.ClickXToRemoveRefinement();
			Log.message("5. Click on bread crumb 'X' button in the search result page");

			Log.assertThat(driver.getCurrentUrl().contains("/search/"),
					"<b>Actual Result:</b>Clicking on 'X' button in the Search result page is navigate to Home Page"
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b>Clicking on 'X' button in the Search result page is not navigate to Home Page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_BE_1092

	// TC_BELK_PDP_001
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the Product Breadcrumb.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2] + " page");
			PdpPage pdppage = plppage.selectProductByIndex(1);
			Log.message("3. Selected the first product from product landing page");
			Log.message("4. PDP page is displayed with selected product");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("primaryImage"), pdppage),
					"5. The main image is displayed in the PDP page",
					"The main image is not displayed in the PDP page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product breadcrumb should be displayed above the main product image.");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("breadcrumbAboveMainImage"), pdppage),
					"<b>Actual Result:</b> The product breadcrumb is displayed above the product main image",
					"<b>Actual Result:</b> The product breadcrumb is not displayed above the product main image",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_001

	// TC_BELK_PDP_033
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the Scroll Arrow near the Product Thumbnail image", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_039(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> elements = Arrays.asList("btnPreviousImage",
				"btnNextImage");

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The product should be having more than 3 thumbnail images");
			Log.assertThat(
					pdpPage.getCountOfThumbnailImages() >= 4,
					"<b>Actual Result-1:</b> This product have more than 3 thumbnail images!",
					"<b>Actual Result-1:</b> This product is not have more than 3 thumbnail images");

			pdpPage.clickImageNavArrowButton("Next");
			Log.message("<br>");
			Log.message("4. Clicked on next navigation arrow");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> On clicking the next navigation arrow, the system should slide 1 position to the right (>)");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-2:</b> The system slides 1 position to the right (>)",
					"<b>Actual Result-2:</b> The system does not slide 1 position to the right (>)",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> Scroll arrows '<' and '>' should be displayed when 4 or more alternate images or videos exist");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(elements, pdpPage),
					"<b>Actual Result-3:</b> Scroll arrows '<' and '>' are displayed when 4 or more alternate images or videos exist",
					"<b>Actual Result-3:</b> Scroll arrows '<' and '>' are not displayed when 4 or more alternate images or videos exist");

			pdpPage.clickImageNavArrowButton("Previous");
			Log.message("<br>");
			Log.message("5. Clicked on previous navigation arrow");
			Log.message("<br>");
			Log.message("<b>Expected Result-4:</b> On clicking the previous navigation arrow, the system should slide 1 position to the left (<)");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-4:</b> The system slides 1 position to the left (<)",
					"<b>Actual Result-4:</b> The system does not slide 1 position to the left (<)",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_033

	// TC_BELK_PDP_050
	@Test(groups = { "desktop", "mobile" }, description = "Verify - add product to wish list as a registered user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_061(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page");
			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("4. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2] + " page");
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("5. Navigated to PDP page with randomly selected  product");
			pdpPage.clickAddToWishListLink();
			Log.message("6. 'Add to wish list' link is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Added to wishlist message should be displayed.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtWishListMessage"), pdpPage),
					"<b>Actual Result:</b> Added to wishlist message is displayed",
					"<b>Actual Result:</b> Added to wishlist message is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_050

	// TC_BELK_PDP_095

	@Test(groups = { "desktop", "mobile" }, description = "Verify scroll arrow on the alternative image is properly displayed in product Set Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_110(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to Product Set Page with first product");
			
			String color=pdpPage.selectColor();

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The product should be having more than 3 thumbnail images");

			if (pdpPage.getCountOfThumbnailImages() <= 3) {
				throw new Exception(
						"This product set contain less than 4 thumbnail images, so unable to test this case.");
			}
			int count = pdpPage.getCountOfThumbnailImages();
			Log.assertThat(
					count >= 4,
					"<b>Actual Result-1:</b> This product have more than 3 thumbnail images!",
					"<b>Actual Result-1:</b> This product is not have more than 3 thumbnail images",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The previous and next image arrow should displayed and while clicking on it, the image should be changed.");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-2a:</b> The Previous image Button is disabled by default",
					"<b>Actual Result-2a:</b> The Previous Button should not be enabled by default",
					driver);

			pdpPage.clickImageNavArrowButton("Next");
			Log.message("<br>4. The Next image button is clicked");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-2b:</b> The Previous image button is enabled after clicking the Next image button",
					"<b>Actual Result-2b:</b> The Previous image button should not be disabled after clicking the Next image button");

			int i = 1;
			while (i < (count - 3)) {
				pdpPage.clickImageNavArrowButton("Next");
				i++;
			}
			Log.message("<br>5. Navigated to last image");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnNextImage"), pdpPage),
					"<b>Actual Result-2c:</b> The Next image Button is disabled by default when last image is displayed",
					"<b>Actual Result-2c:</b> The Next Button should not be enabled by default when last image is displayed");
			pdpPage.clickImageNavArrowButton("Previous");
			Log.message("<br>5. The Previous image button is clicked");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("btnNextImage"), pdpPage),
					"<b>Actual Result-2d:</b> The Next image button is enabled after clicking the Previous image button",
					"<b>Actual Result-2d:</b> The Next image button should not be disabled after clicking the Previous image button",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_110

	// TC_BELK_PDP_049
	@Test(groups = { "desktop", "mobile" }, description = "Verify - add product to wish list as a guest user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to " + category[0] + " >> " + category[1]
					+ " >> " + category[2] + " PLP page");
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Navigated to PDP with random product");
			pdpPage.clickAddToWishListLink();
			Log.message("4. 'Add to wish list' link is clicked!");
			// SignIn signIn = new SignIn(driver).get();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should be navigated to Create Account page.");
			WishListPage wishlistpage = new WishListPage(driver).get();
			if (Utils.getRunPlatForm() == "mobile") {
				Log.assertThat(
						wishlistpage.elementLayer.verifyPageElements(
								Arrays.asList("txtWishlistMobileHeading"),
								wishlistpage),
						"<b>Actual Result:</b> The page is navigated to Create Account page and the Wishlist Heading is present in the breadcrum",
						"<b>Actual Result:</b>  The page is not navigated to Create Account page and the Wishlist Heading is not present in the breadcrumAccount page",
						driver);
			}

			if (Utils.getRunPlatForm() == "Desktop") {
				Log.assertThat(
						wishlistpage.elementLayer.verifyPageElements(
								Arrays.asList("txtWishListHeading"),
								wishlistpage),
						"<b>Actual Result:</b> The page is navigated to Create Account page and the Wishlist Heading is present in the breadcrum",
						"<b>Actual Result:</b>  The page is not navigated to Create Account page and the Wishlist Heading is not present in the breadcrumAccount page",
						driver);

				Log.assertThat(
						wishlistpage.elementLayer.verifyPageElements(
								Arrays.asList("txtWishListHeading"),
								wishlistpage),
						"<b>Actual Result:</b> The page is navigated to Create Account page and the Wishlist Heading is present in the breadcrum",
						"<b>Actual Result:</b>  The page is not navigated to Create Account page and the Wishlist Heading is not present in the breadcrumAccount page",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_049

	// TC_BELK_PDP_052
	@Test(groups = { "desktop", "mobile" }, description = "Verify Click to Enlarge link below the product main image in PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_063(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> enlargePopUp = Arrays.asList("enlargePopUP");

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page with Keyword : '"
					+ searchKey + "'");
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP of the product selected randomly");

			pdpPage.verifyClickToEnlarge();
			Log.message("4. Clicked on 'Click to Enlarge' link in PDP");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Enlarge view pop up should be displayed when clicking on 'Click to Enlarge' link ");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(enlargePopUp,
							pdpPage),
					"<b>Actual Result:</b> 'Enlarge View pop up' is displayed when clicking on 'Click to Enlarge' link",
					"<b>Actual Result:</b> 'Enlarge View pop up' is not displayed when clicking on 'Click to Enlarge' link",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
			// }
	}// TC_BELK_PDP_052

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the brand name with hyperlink in product detail page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1] + "-->" + category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdppage = plppage.selectProductByIndex(1);
			Log.message("2. Selected 1st product and navigated to PDP page");
			String brandName = pdppage.getTextFromBrand();
			Log.message("<br>");
			Log.message("<b>Expected Result</b> The product's brand name with hyperlink should be displayed on product details page");
			String urlBeforeClicking = driver.getCurrentUrl();
			pdppage.clickBrandname();
			ArrayList<String> productname = plppage.getBrandNameInProductlist();
			boolean status = false;
			//			for (int i = 0; i < productname.size(); i++) {
			//				if (productname.get(i).contains(brandName))
			//					status = true;
			//				else {
			//					status = false;
			//					break;
			//				}
			//			}
			Log.assertThat(!(urlBeforeClicking.equals(driver
					.getCurrentUrl())),
					"<b>Actual Result:</b> The product's brand name '"
							+ brandName
							+ "' is displayed as a hyperlink in PDP",
							"<b>Actual Result:</b> The product's brand name '"
									+ brandName
									+ "' is not displayed as a hyperlink in PDP",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_006

	// TC_BELK_PDP_027
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the close('X') icon on size chart window and allow user to click on it.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_032(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP of the product selected randomly");

			pdpPage.clickSizeChartLink();
			Log.message("4. Size chart link is clicked in the 'PDP' Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Size chart module should be show in the PDP Page");
			Log.message("<br>");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("fldSizeDialogueBox"), pdpPage),
					"<b>Actual Result:</b> Size chart module is displayed ",
					"<b>Actual Result:</b>  Size chart  module is not displayed",
					driver);

			pdpPage.closeSizeChartByCloseButton();
			Log.message("5. Clicked on 'X' button in the size chart module");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Size chart should be closed when user click on 'X' icon");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("fldSizeDialogueBox"), pdpPage),
					" <b>Actual Result:</b> Size chart module is closed by clicking Close 'X' button",
					" <b>Actual Result:</b> Size chart module is not close by clicking Close 'X' button",
					driver);

			pdpPage.clickSizeChartLink();
			Log.message("6. Size chart link is clicked in the 'PDP' Page");

			pdpPage.closeSizechartByClickingOtherElement(homePage);
			Log.message("7. Clicked on any Locator in the size chart module");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Size chart should be closed when user click on  any Locator in the size chart module");
			Log.message("<br>");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("fldSizeDialogueBox"), pdpPage),
					" <b>Actual Result:</b> Size chart closes by clicking element other than size chart ",
					" <b>Actual Result:</b> Size chart does not close by clicking element other than size chart",
					driver);

			pdpPage.clickSizeChartLink();
			Log.message("8. Size chart link is clicked in the 'PDP' Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Size chart should be closed when user press ESC key");
			Log.message("<br>");

			pdpPage.closeSizechartByEsckey();
			Log.message("9. Size chart module should closed when pressing Escape key");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("fldSizeDialogueBox"), pdpPage),
					"<b>Actual Result:</b> Size chart closes by pressing Escape key",
					"<b>Actual Result:</b> Size chart do not close by pressing Escape key",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_027

	// TC_BELK_PDP_015
	@Test(groups = { "desktop" }, description = "Verify system highlights the color on mouse hover and is selected on click.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_020(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"Hover functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Plp page is properly loded");
			PdpPage pdppage = plppage.selectProductWithColorSwatch();
			Log.message("3. Selected product with color swatch and navigated to PDP");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Color should be highlighted on mouseover and selected.");
			Log.assertThat(
					pdppage.verifyColorOfMouseHoverForNonSelectedSwathches(),
					"<b>Actual Result:</b>  Color is highlighted properly while mouse hover  non selected color",
					"<b>Actual Result:</b>  Mouse Over Color is not highlighted properly for non selected color",
					driver);
			Log.assertThat(
					pdppage.verifyColorOfMouseHoverForSelectedSwatch(),
					"<b>Actual Result:</b>  Color is highlighted properly while mouse hover  selected color",
					"<b>Actual Result:</b>  Mouse Over Color is not highlighted properly for selected color");

			pdppage.selectColorByIndex(1);
			Log.message("4. Slected color by index value: 1");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Color should be changed on primary/faceout image depending upon the selection on color swatches");
			Log.assertThat(
					pdppage.verifySelectedColorIsDisplayed(),
					"<b>Actual Result:</b>  Color is changed on primary/faceout image depending upon the selection on color swatches",
					"<b>Actual Result:</b>  Color is not changed on primary/faceout image depending upon the selection on color swatches",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_015

	// TC_BELK_PDP_031
	@Test(groups = { "desktop", "mobile", "desktop_bat", "mobile_bat" }, description = "Verify system displays the 'Hover to Zoom/ Click to Enlarge'.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_037(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> clickToEnlarge = Arrays.asList("lnkClickToEnlarge");
		List<String> zoomLinks = Arrays.asList("txtHoverToZoom");

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page with Keyword : "
					+ searchKey);

			// Load QuickView Page
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP of the product selected randomly");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> 'Click to enlarge' link should be displayed in PDP ");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(clickToEnlarge,
							pdpPage),
					"<b>Actual Result1:</b> 'Click to enlarge' link is displayed in PDP",
					"<b>Actual Result1:</b> 'Click to enlarge' link is not displayed in PDP",
					driver);

			if (runPltfrm == "desktop") {

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> 'Hover to zoom' link should be displayed in PDP ");

				Log.assertThat(pdpPage.elementLayer.verifyPageElements(zoomLinks,
								pdpPage),
						"<b>Actual Result2:</b> 'Hover to zoom' link is displayed in PDP",
						"<b>Actual Result2:</b> 'Hover to zoom' link is not displayed in PDP",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_037
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the color swatches in the enlarge modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_066(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the  '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result'  Page!");
			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("3. Navigated to PDP of the product selected randomly");
			pdpPage.getAllColorSwatchCount();

			EnlargeViewPage enlargeViewPage = pdpPage.verifyClickToEnlarge();
			Log.message("4. Clicked on 'Click to Enlarge' link below the product main image");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> When a user selects a new color, the product’s faceout image and associated alternate images should get updated with the selected color within the enlarged image window.");

			Log.assertThat(
					(enlargeViewPage.getSelectedSwatchColors(1)
							.equals(enlargeViewPage.getFaceoutImageColors())),
					"<b>Actual Result:</b> The product faceout image and associated alternate images is updated with the selected color within the enlarged image window.",
					"<b>Actual Result:</b> The product faceout image and associated alternate images is not updated with the selected color within the enlarged image window.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_PDP_055

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the active image in the enlarge modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_064(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			String color=pdpPage.selectColor();
			Log.message("3. Selected color: '"+color+"' !");

			// Clicking on Click-To-Enlarge
			pdpPage.clickToEnlarge();
			Log.message("4. Clicked on 'Click to Zoom' Link!");

			Log.message("</br>");
			Log.message("<b>Expected Result : </b>The hi­resolution image should be displayed as the active image");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("clickToZoomDialog"), pdpPage),
					"<b>Actual Result : </b>The hi­resolution image displayed as the active image as Expected!",
					"<b>Actual Result : </b>The hi­resolution image not displayed as Expected!");

			Log.message("<b>Expected Result : </b>The hi­resolution image should be displayed with the color variation</b>");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("clickToZoomSwatchColor"), pdpPage),
					"<b>Actual Result : </b>The hi­resolution image displayed with the color variation as Expected!",
					"<b>Actual Result : </b>The hi­resolution image not displayed with the color variation as Expected!");

			Log.message("<b>Expected Result : The hi­resolution image should be displayed as the active image with image view selected.</b>");
			String[] src = pdpPage.getImageFromZoomModal();
			Log.assertThat(
					src[0].equals(src[1]),
					"<b>Actual Result : </b>The hi­resolution image displayed as the active image with image view selected as Expected.",
					"<b>Actual Result : </b>The hi­resolution image not displayed as Expected.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_064

	// TC_BELK_PDP_051
	@Test(groups = { "desktop" }, description = "Verify system Mouse hover to zoom on product image", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_062(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is not applicable for mobile");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);
			String searchKey = testData.get("SearchKey");

			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			List<String> productZoom = Arrays.asList("imgPrimary");

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				SearchResultPage searchResultpage = homePage
						.searchProductKeyword(searchKey);
				Log.message("2. Navigated to Search Result Page with Keyword : '"
						+ searchKey + "'");

				PdpPage pdpPage = searchResultpage.navigateToPDP();
				Log.message("3. Navigated to PDP of the product selected randomly");
				pdpPage.verifyProductMouseHover();

				Log.message("4. Mouse is hovered on product");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Product image should be zoomed when hovering mouse on the product ");

				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(productZoom,
								pdpPage),
						"<b>Actual Result:</b> Main Product image is zoomed when hovering mover on the product image",
						"<b>Actual Result:</b> Main Product image is not zoomed when hovering mover on the product image",
						driver);

				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	} // TC_BELK_PDP_051

	// TC_BELK_PDP_026
	@Test(groups = { "desktop", "mobile" }, description = "Verify PDP sizechart displays in the PDP page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_031(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Pdp' Page with ProductId");
			Log.message("3. Navigated to PDP of the product selected randomly");

			pdpPage.clickSizeChartLink();
			Log.message("4. Size chart link is clicked", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Size chart link should navigate to a window that shows a size chart.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("fldSizeDialogueBox"), pdpPage),
					"<b>Actual Result:</b> Size chart dialogue box is opened",
					"<b>Actual Result:</b> Size chart dialogue box is not opened");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_026

	@Test(groups = { "desktop" }, description = "Verify system displays the Search page by clicking the brand name.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_008(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String txtRefinedBy = "Refined by";
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1] + "-->" + category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("3. Navigated to plp page");
			PdpPage pdppage = plppage.navigateToPDPWithProductId();
			Log.message("4. Product got selected");
			String brandName = pdppage.getTextFromBrand();
			SearchResultPage searchresult = pdppage.clickBrandname();
			Log.message("5. Brand name is clicked");
			ArrayList<String> productname = searchresult
					.getBrandNameInProductlist();
			Log.message("6. Search result page is loaded after clicking the brand name '"
					+ brandName + "' in pdp page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>If no brand landing page exists while clicking on brand name hyperlink, the customer is redirected to a Search Results page with the matching Brand search refinement preselected");
			Log.assertThat(
					searchresult.getTextRefinedByfromBreadcrumb().equals(
							txtRefinedBy),
					"<b>Actual Result:</b> Search result page is properly loaded for the selected brand",
					"<b>Actual Result:</b> Search result page is not loaded for the selected brand");
			boolean status = false;
			for (int i = 0; i < productname.size(); i++) {
				if (productname.get(i).contains(brandName))
					status = true;
				else {
					status = false;
					break;
				}
			}
			Log.assertThat(
					status,
					"<b>Actual Result:</b> All product listed in the search result page contain the brand name: "
							+ brandName + "",
					"<b>Actual Result:</b> All products listed in the brand page do not contain the brand name: "
							+ brandName + "", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_007

	@Test(groups = { "desktop", "mobile" }, description = "Verify - Clicking on privacy policy link in size chart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_034(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdppage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP of the product selected ");
			pdppage.clickSizeChartLink();
			Log.message("4. Size chart link is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Privacy Policy link should be present at the bottom of the size chart link.");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("lnkPrivacyPolicy"), pdppage),
					"<b>Actual Result:</b> Privacy policy link is present in size chart link",
					"<b>Actual Result:</b> Privacy policy link is not present in size chart link");
			PrivacyAndPolicyPage privacyAndPolicyPage = pdppage
					.clickPrivacyPolicyLink();
			Log.message("<br>");
			Log.message("5. Clicked Privacy policy clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> on click, the customer is taken to the privacy policy page within Customer Service.");

			Log.assertThat(
					privacyAndPolicyPage.getContentHeader().contains(
							"privacyContent"),
					"<b>Actual Result:</b> Privacy policy page is loaded properly",
					"<b>Actual Result:</b> Privacy policy page is not loaded properly",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_028

	@Test(groups = { "desktop", "mobile" }, description = "Verify pop up closed by clicking close button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_114(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			/*
			 * homePage.headers.navigateTo(category[0], category[1],
			 * category[2]); Log.message(
			 * "2. Navigated to Product landing page of: " + category[0] +"-->"+
			 * category[1] +"-->"+ category[2]);
			 * 
			 * PlpPage plppage = new PlpPage(driver).get(); CategoryLandingPage
			 * clppage = plppage.selectCatgeoryRefinement(); Log.message(
			 * "3. Random catagory is selected in plp page");
			 */

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Product name should be properly shown below the selected Brand name.");
			Log.assertThat(
					pdpPage.verifyProductNameDisplayedUnderBrandName(),
					"<b>Actual Result:</b> Product name is properly shown below the selected Brand name.",
					"Product name is not properly shown below the selected Brand name.");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> product image should be properly shown.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("imgOfProduct"), pdpPage),
					"<b>Actual Result:</b> Product image is displayed in the pdp page",
					"<b>Actual Result:</b> Product image is not displayed in the pdp page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_099

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays UPC for variation masters product and variations product. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblUPC");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> UPC should not be displayed for variation masters product.");
			Log.message("<br>");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(element,
							pdpPage),
					"<b>Actual Result:</b> Without any veriation UPC is not displaying in page !",
					"<b>Actual Result:</b> Without any veriation UPC is displaying in page !",
					driver);
			String colorone=pdpPage.selectColor();
			Log.message("5. Color is selected and the selected color is "+colorone);
			
			pdpPage.selectSizeByIndex(1);
			Log.message("4. Selected Size by index: 1 from the size drop down");
			
			String UPC1 = pdpPage.getUPCValue();
			Log.message("5. Getting UPC:"
					+ UPC1
					+ "in the PDP Page after selected the size in the size drop down");
			String colortwo=pdpPage.selectColor();
			Log.message("5. Color is selected and the selected color is "+colortwo);
			
			pdpPage.selectSizeByIndex(2);
			Log.message("6. Selected Size by index: 2 from the size drop down");
			
			String UPC2 = pdpPage.getUPCValue();
			Log.message("7. Getting UPC:"
					+ UPC2
					+ "in the PDP Page after selected size in the size drop down");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> UPC should be displayed and on click of a variation product, the UPC will update to reflect the variation value.");
			Log.message("<br>");

			Log.assertThat(
					!UPC1.equals(UPC2),
					"<b>Actual Result:</b> The UPC value is varying on Selection of different variation !",
					"<b>Actual Result:</b> The UPC value is not varying on Selection of different variation !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_010

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays Online Only Message in the PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String expectedMessage;
		String actualMessage;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP Page !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("Online only message should be displayed below qty dropdown in the PDP.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			pdpPage.selectSize();
			expectedMessage = pdpPage.getOnlineOnlyMessage();
			actualMessage = "Online Only";
			Log.assertThat(expectedMessage.equals(actualMessage),
					"Online only message is displaying !",
					"Online only message is not displaying !", driver);
			Log.assertThat(
					pdpPage.verifyOnlineOnlyMessageBelowQTY(),
					"The OnlineOnly message is displaying below the Quantity dropdown !",
					"The OnlineOnly message is not displaying below the Quantity dropdown !",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_041

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Shipping Threshold Message in Product Set Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_120(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> For Qualified product Text message should be displayed product is qualified for free Shipping");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("shippingPromotion"), pdpPage),
					"<b>Actual Result:</b> Free shipping message displayed for the qualified product",
					"<b>Actual Result:</b> Free shipping message not displayed for the qualified products",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_104

	@Test(groups = { "desktop", "mobile" }, description = "Verify pop up closed by clicking close button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider", enabled = false)
	public void TC_BELK_PDP_092(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("Selected catagory is " + category[0] + category[1]
					+ category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to plppage");
			PdpPage pdppage = plppage.navigateToPDPWithProductId();
			Log.message("3. Product got selected");
			pdppage.clickOnFindStore();
			Log.message("4. 'FindStore' link is clicked");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"5. 'FindStore' link is displayed",
					"'FindStore' link is not displayed");
			pdppage.closeDialogueBoxByCloseButton();
			Log.message("6. Clicked 'X' button in popup");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Find a store popup should close after clicking the close button");
			Log.assertThat(
					!pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"<b>Actual Result:</b> Find a store popup was closed after clicking the close button",
					"<b>Actual Result:</b> Find a store popup was close after clicking the close button");
			pdppage.clickOnFindStore();
			Log.message("<br>7. 'FindStore' link is clicked");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"8. Find a store popup was opened",
					" Find a store popup was not opened");
			pdppage.closeDialogueBoxByClickingOtherElementImage();
			Log.message("<br>9. Clicking an element other than popup");
			Log.message("<b>Expected Result:</b> Find a store popup should close after clicking the outside element");
			Log.assertThat(
					!pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"<b>Actual Result:</b> Find a store popup was closed after clicking the outside element",
					"<b>Actual Result:</b> Find a store popup was not closed after clicking the outside element");
			pdppage.clickOnFindStore();
			Log.message("<br>10. 'FindStore' link is clicked");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"11. Find a store popup is displayed",
					"Find a store popup is not displayed");
			Log.message("<b>Expected Result:</b> Find a store popup should close after clicking the esc button");
			pdppage.closeDialogueBoxByEsckey();
			Log.message("<br>12. Clicked 'ESC' key");
			Log.assertThat(
					!pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldFindStore"), pdppage),
					"<b>Actual Result:</b> Find a store popup should close after clicking the esc button",
					"<b>Actual Result:</b> Find a store popup should close after clicking the esc button");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_PDP_078

	@Test(groups = { "desktop", "mobile" }, description = "To verify product's rating and reviews on PDP under product name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_044(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to PLP with selected category is L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdppage = plppage.navigateToPDPWithProductId();
			Log.message("3. Product got selected");
			Log.message("<b>Expected Result:</b> Ratings and reviews should be displayed under product name");
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldRating"), pdppage),
					"<b>Actual Result:</b> Ratings present properly in pdp page",
					"<b>Actual Result:</b> Ratings are not present properly in pdp page",
					driver);
			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(
							Arrays.asList("fldReview"), pdppage),
					"<b>Actual Result:</b> Ratings present properly in pdp page",
					"<b>Actual Result:</b> Ratings are not present properly in pdp page",
					driver);
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_PDP_037

	@Test(groups = { "desktop", "mobile" }, description = "Verify system navigates user to the Product master category.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		ArrayList<String> breadcrumblist;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		String breadcrumblastvalue;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("3. Navigated from PLP Page to PDP Page by selecting the first Product");

			breadcrumblist = pdppage.getTextInBreadcrumb();
			Log.message("4. Getting the Product's bread crumb");
			int index = 0;
			if (Utils.getRunPlatForm() != "mobile") {
				index = breadcrumblist.size() - 2;
			} else {
				index = breadcrumblist.size() - 1;
			}
			plpPage = pdppage.clickCategoryByNameInBreadcrumb(breadcrumblist
					.get(index));
			Log.message("5. Clicking on the Product's Breadcrumb '"
					+ breadcrumblist.get(index) + "' from PDP Page");
			breadcrumblist = plpPage.getTextInBreadcrumb();
			index = breadcrumblist.size() - 1;
			breadcrumblastvalue = breadcrumblist.get(index);
			Log.message("<b>Expected Result:</b>Clicking on the Product's Last BreadCrumb from the PDP Page should be navigated to PLP Page.");
			if (Utils.getRunPlatForm() != "mobile") {
				Log.assertThat(
						breadcrumblastvalue.equalsIgnoreCase(l3category),
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page successfully navigated to PLP page.",
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page failed to navigated to  PLP page.",
						driver);
			} else {
				Log.assertThat(
						breadcrumblastvalue.equalsIgnoreCase("Back to "
								+ l3category),
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page successfully navigated to PLP Page.",
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page failed to navigated to PLP Page.",
						driver);
			}
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_PDP_003

	@Test(groups = { "desktop", "mobile" }, description = "Verify the color SKU variation selected state for variant products that are not grey out..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_024(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> pdpPageList = Arrays.asList("btnAddtoBag");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			String Size = pdppage.selectSize();
			Log.message("4. " + Size + " is Selected from size dropdown");
			Log.message("<b>Expected Result:</b> Add to cart button should be disabled for Out of Stock Item");
			Log.softAssertThat(
					!pdppage.elementLayer.verifyPageElements(pdpPageList,
							pdppage),

					"<b>Actual Result:</b> Add to Cart button is disabled for Out of stock Item",
					"<b>Actual Result:</b> Add to Cart button is not disabled for Out of stock Item");
			pdppage.selectColorByIndex(1);
			Log.message("5. Swatch is selected");
			Log.message("<b>Expected Result:</b> Add to cart button should be enabled for  InStock Item");
			Log.softAssertThat(
					pdppage.elementLayer.verifyPageElements(pdpPageList,
							pdppage),

					"<b>Actual Result:</b> Add to Cart button is enabled for Instock Item",
					"<b>Actual Result:</b> Add to Cart button is not enabled for  Instock Item");
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_PDP_019

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system disables add to bag button when size not selected", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_054(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web waitForDisabledElementdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("3. Naviagte to 'Pdp' Page with randomly selected product");

			String color = pdpPage.selectColor();
			Log.message("4. Selet the color '" + color
					+ "' from the swatches on the PDP page");
			Log.message("<br> Expected Result:</br> Add to bag button should be disabled when user is not selected the size for the product.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDisabled(
							Arrays.asList("btnAddtoBag"), pdpPage),
					"<br> Actual Result: </br> Add to bag button is disabled when user is not selected the size for the product.",
					"<br> Actual Result: </br> Add to bag button is not disabled when user is not selected the size for the product.");

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_044

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays the Product Main image in PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_035(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web waitForDisabledElementdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");
			// Navigate to PLP page
			SearchResultPage searchResultPage = new SearchResultPage(driver)
					.get();
			String sUrl = searchResultPage.getPrimaryImageColor(1);
			Log.message("3. The PLP page image URL  is " + sUrl);

			// Navigate to PDP page
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("3. Naviagte to 'Pdp' Page with randomly selected product");
			String pUrl = pdpPage.getProductImageUrl().split("&layer=")[0];
			Log.message("4. Src image Url at PDP page " + pUrl);
			Log.message("<br>");
			Log.message("<b> Expected Result: </b> Product main image should be displayed as the same way as display in PLP");

			// to verify PLP & PDP URL is same
			Log.assertThat(
					sUrl.equals(pUrl),
					"<b> Actual Result: </b> PDP main image URL is same as PLP  ",
					"<b> Actual Result: </b> PDP main image URL is not same as PLP",
					driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_029

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify closing the zip code store modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_080(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web waitForDisabledElementdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> pdpfindstorebox = Arrays.asList("belkBrandName");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("3. Naviagte to 'Pdp' Page with randomly selected product");
			pdpPage.clickFindStore();
			Log.message("4. Clicked on the 'Find Store' Link on the PDP page.");

			// 1. Get closed while clicking (X) icon
			pdpPage.closeFindStoreBoxByCrossIcon();
			Log.message("5. Clicked on 'X' icon in Find Store Box");
			Log.message("<b> Expected Result: </b> The zip code store modal should get closed while clicking (X) icon.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(pdpfindstorebox,
							pdpPage),
					"<b> Actual Result: </b> The zip code store pop_up get closed while clicking (X) icon.",
					"<b> Actual Result: </b> The zip code store pop_up not closed while clicking (X) icon.",
					driver);

			// 2. Get closed while clicking anywhere outside the modal
			pdpPage.clickFindStore();
			Log.message("6. Clicked on the 'Find Store' Link on the PDP page.");
			pdpPage.closeFindStoreClickingAnywhere();
			Log.message("7. Clicked anywhere other than 'Find Store' popup");
			Log.message("<b> Expected Result: </b> The zip code store modal should get closed while clicking anywhere outside the modal.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(pdpfindstorebox,
							pdpPage),
					"<b> Actual Result: </b> The zip code store pop_up get closed while while clicking anywhere outside the modal.",
					"<b> Actual Result: </b> The zip code store pop_up not get closed while clicking anywhere outside the modal.",
					driver);

			// 3. Get closed while pressing the ESC key.
			pdpPage.clickFindStore();
			Log.message("8. Clicked on the 'Find Store' Link on the PDP page.");
			pdpPage.closeFindStoreByEsc();
			Log.message("9. Clicked 'ESC' key");
			Log.message("<b> Expected Result: </b> The zip code store modal should get closed while pressing the ESC key.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(pdpfindstorebox,
							pdpPage),
					"<b> Actual Result: </b> The zip code store pop_up get closed while pressing the ESC key.",
					"<b> Actual Result: </b> The zip code store pop_up not get closed while pressing the ESC key.",
					driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_068

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify 'Description' tab in PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_069(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web waitForDisabledElementdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> pdpdescriptiontabdesktop = Arrays.asList(
				"txtDescriptionPdpdesktop", "txtProductDescription");
		List<String> pdpdescriptiontabmobile = Arrays.asList(
				"txtDescriptionPdpmobile", "txtProductDescription");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search '"
					+ searchKey
					+ "' in the home page and  Navigated to 'Search Result'  Page!");

			PdpPage pdpPage = searchresultPage.navigateToPDP();
			Log.message("3. Naviagte to 'Pdp' Page with randomly selected product");

			if (Utils.getRunPlatForm() == "desktop") {
				Log.message("<b> Expected Result: </b> Description tab should be visible below the PDP main image and Description for the product should be displayed . ");
				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(
								pdpdescriptiontabdesktop, pdpPage),
						"<b> Actual Result: </b> Description tab is visible below the PDP main image and Description for the product is displayed .",
						"<b> Actual Result: </b> Description tab is not visible below the PDP main image and Description for the product should not be displayed ",
						driver);

			}

			if (Utils.getRunPlatForm() == "mobile") {
				pdpPage.scrollDownMobile();
				Log.message("<b> Expected Result: </b> Description tab should be visible below the PDP main image and Description for the product should be displayed . ");
				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(
								pdpdescriptiontabmobile, pdpPage),
						"<b> Actual Result: </b> In PDP page Description tab and Description for the product is displayed .",
						"<b> Actual Result: </b> In PDP page Description tab and Description for the product is not displayed . ",
						driver);

			}

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_058

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify system displays the navigation path on Product Breadcrumb", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		ArrayList<String> breadcrumblist;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];

		List<String> breadCrumb = Arrays.asList("lnkBreadCrumb");
		List<String> homeInBreadCrumb = Arrays.asList("bcHome");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("3. Navigated from PLP Page to PDP Page by selecting the first Product");

			breadcrumblist = pdppage.getTextInBreadcrumb();
			Log.message("4. Getting the Product's bread crumb- "
					+ breadcrumblist);

			String productName = pdppage.getProductName();
			Log.message("The product name is- " + productName);

			String brandName = pdppage.getProductBrandName();
			Log.message("The product brand name is- " + brandName);

			Log.message("<b> Expected Result: </b> The Product breadcrumb should be displayed with the navigation path");
			Log.assertThat(
					pdppage.elementLayer
							.verifyPageElements(breadCrumb, pdppage),
					"<b> Actual Result: </b> The Product breadcrumb displayed with the navigation path",
					"<b> Actual Result: </b> The Product breadcrumb not displayed with the navigation path",
					driver);

			Log.assertThat(
					pdppage.elementLayer.verifyPageElements(homeInBreadCrumb,
							pdppage),
					"<b> Actual Result: </b> Home link displayed properly in product breadcrumb",
					"<b> Actual Result: </b> Home link missing in product breadcrumb",
					driver);

			for (String childCategory : category) {
				Log.assertThat(
						breadcrumblist.contains(childCategory),
						"<b> Actual Result: </b> In product breadcrumb navigation path, the child category- '"
								+ childCategory + "' displayed properly",
						"<b> Actual Result: </b>  In product breadcrumb navigation path, the child category- '"
								+ childCategory + "' is missing ", driver);
			}

			String productNameWithBrand = brandName.concat(" " + productName);
			Log.assertThat(
					breadcrumblist.contains(productNameWithBrand),
					"<b> Actual Result: </b> In product breadcrumb navigation path, the product name - '"
							+ productName + "' displayed properly as a link",
					"<b> Actual Result: </b>  In product breadcrumb navigation path, the product name - '"
							+ productName + "' is missing ", driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_3B_PDP_003

	@Test(groups = { "desktop", "mobile" }, description = "Verify whether the 'Select a Registry' modal dialog is displayed when a registered user with multiple registries is selecting the 'Add to Registry' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_058(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String shippingaddress = testData.get("Address");;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		List<String> registryModal = Arrays.asList("modalDeleteRegistry");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");
			
			int registriesToBeCreated = 2;
			for (int i = 1; i <= registriesToBeCreated; i++) {
				RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
						.navigateToSection("registry");
				Log.message("5. Navigated to Edit profile page to add belk employee details");

				// Click on Create New Registry button
				registrySignedUserPage.clickOnCreateNewRegistry();
				Log.message("4. Click on Create New Registry button");

				// Enter the valid Event details to register
				registrySignedUserPage
						.fillingRegistryDetails("weddingRegistry");
				Log.message("5. Filling the valid event details");

				// Click on Continue button in Step 1
				registrySignedUserPage.clickContinueInCreateRegistry();
				Log.message("6. Click on Continue button in Event Registry page");

				// Update the PreEvent Address and Fill Post Event address
				registrySignedUserPage
						.fillingEventShippingdetails(shippingaddress);
				Log.message("7. Update the Pre Event address and Fill Post Event address");

				// Click on Continue button in Shipping address section
				registrySignedUserPage.clickOnContinueButtonInEventShipping();
				Log.message("8. Click on Continue button in shipping address");

				// Click on Submit button
				registrySignedUserPage.clickOnSubmitButton();
				Log.message("9. Click on Subtmit button in Post Event address");
			}		
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("11. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("12. Navigated from PLP Page to PDP Page by selecting the random Product");

			pdppage.selectSize();
			Log.message("13. Selected size from the product");
			
			pdppage.selectColor();
			Log.message("14. Selected color from the product");
			
			pdppage.selectQuantity();
			Log.message("15. Selected qty from the product");
			
			RegistrySignedUserPage registrySignedUserPage=pdppage.navigateToRegistryAsSignedUser();
			Log.message("16. Clicked on 'Add item to registry' button in PDP page");
			
			Log.message("<b> Expected Result: </b> 'Select a Registry' modal should be displayed on clicking 'Add to registry' button in PDP");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(registryModal,
							registrySignedUserPage),
					"<b> Actual Result: </b> 'Select a Registry' modal displayed on clicking 'Add to registry' button in PDP",
					"<b> Actual Result: </b> 'Select a Registry' modal not displayed on clicking 'Add to registry' button in PDP",
					driver);
		
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_058
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify whether the copy message for selecting the registry to which the product needs to be added is displayed in the dialog box opened", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_059(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String shippingaddress = testData.get("Address");;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		List<String> registryModal = Arrays.asList("modalDeleteRegistry");
		List<String> copyMessage = Arrays.asList("txtCopyMessage");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");
			
			int registriesToBeCreated = 2;
			for (int i = 1; i <= registriesToBeCreated; i++) {
				RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
						.navigateToSection("registry");
				Log.message("5. Navigated to Edit profile page to add belk employee details");

				// Click on Create New Registry button
				registrySignedUserPage.clickOnCreateNewRegistry();
				Log.message("4. Click on Create New Registry button");

				// Enter the valid Event details to register
				registrySignedUserPage
						.fillingRegistryDetails("weddingRegistry");
				Log.message("5. Filling the valid event details");

				// Click on Continue button in Step 1
				registrySignedUserPage.clickContinueInCreateRegistry();
				Log.message("6. Click on Continue button in Event Registry page");

				// Update the PreEvent Address and Fill Post Event address
				registrySignedUserPage
						.fillingEventShippingdetails(shippingaddress);
				Log.message("7. Update the Pre Event address and Fill Post Event address");

				// Click on Continue button in Shipping address section
				registrySignedUserPage.clickOnContinueButtonInEventShipping();
				Log.message("8. Click on Continue button in shipping address");

				// Click on Submit button
				registrySignedUserPage.clickOnSubmitButton();
				Log.message("9. Click on Subtmit button in Post Event address");
			}			
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("11. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("12. Navigated from PLP Page to PDP Page by selecting the random Product");

			pdppage.selectSize();
			Log.message("13. Selected size from the product");
			
			pdppage.selectColor();
			Log.message("14. Selected color from the product");
			
			pdppage.selectQuantity();
			Log.message("15. Selected qty from the product");
			
			RegistrySignedUserPage registrySignedUserPage=pdppage.navigateToRegistryAsSignedUser();
			Log.message("16. Clicked on 'Add item to registry' button in PDP page");
			
			Log.message("<b> Expected Result: </b> 'Select a Registry' modal should be displayed on clicking 'Add to registry' button in PDP");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(registryModal,
							registrySignedUserPage),
					"<b> Actual Result: </b> 'Select a Registry' modal displayed on clicking 'Add to registry' button in PDP",
					"<b> Actual Result: </b> 'Select a Registry' modal not displayed on clicking 'Add to registry' button in PDP",
					driver);
			
			Log.message("<b> Expected Result: </b> The copy message should be displayed in 'Select a Registry' dialog box");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(copyMessage,
							registrySignedUserPage),
					"<b> Actual Result: </b> Copy message displayed in the 'Select a Registry' dialog box",
					"<b> Actual Result: </b> Copy message not displayed in the 'Select a Registry' dialog box",
					driver);	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_059
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify whether all the registries of the logged in user are displayed as table records below the message in the dialog box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String shippingaddress = testData.get("Address");;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		List<String> registryModal = Arrays.asList("modalDeleteRegistry");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");
		
			int registriesToBeCreated = 2;
			for (int i = 1; i <= registriesToBeCreated; i++) {
				RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
						.navigateToSection("registry");
				Log.message("5. Navigated to Edit profile page to add belk employee details");

				// Click on Create New Registry button
				registrySignedUserPage.clickOnCreateNewRegistry();
				Log.message("4. Click on Create New Registry button");

				// Enter the valid Event details to register
				registrySignedUserPage
						.fillingRegistryDetails("weddingRegistry");
				Log.message("5. Filling the valid event details");

				// Click on Continue button in Step 1
				registrySignedUserPage.clickContinueInCreateRegistry();
				Log.message("6. Click on Continue button in Event Registry page");

				// Update the PreEvent Address and Fill Post Event address
				registrySignedUserPage
						.fillingEventShippingdetails(shippingaddress);
				Log.message("7. Update the Pre Event address and Fill Post Event address");

				// Click on Continue button in Shipping address section
				registrySignedUserPage.clickOnContinueButtonInEventShipping();
				Log.message("8. Click on Continue button in shipping address");

				// Click on Submit button
				registrySignedUserPage.clickOnSubmitButton();
				Log.message("9. Click on Subtmit button in Post Event address");
			}			
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("11. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("12. Navigated from PLP Page to PDP Page by selecting the random Product");

			pdppage.selectSize();
			Log.message("13. Selected size from the product");
			
			pdppage.selectColor();
			Log.message("14. Selected color from the product");
			
			pdppage.selectQuantity();
			Log.message("15. Selected qty from the product");
			
			RegistrySignedUserPage registrySignedUserPage=pdppage.navigateToRegistryAsSignedUser();
			Log.message("16. Clicked on 'Add item to registry' button in PDP page");
			
			Log.message("<b> Expected Result: </b> 'Select a Registry' modal should be displayed on clicking 'Add to registry' button in PDP");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(registryModal,
							registrySignedUserPage),
					"<b> Actual Result: </b> 'Select a Registry' modal displayed on clicking 'Add to registry' button in PDP",
					"<b> Actual Result: </b> 'Select a Registry' modal not displayed on clicking 'Add to registry' button in PDP",
					driver);
			
			int actual_registries_count=registrySignedUserPage.lstRegistries.size();
			Log.message("<b> Expected Result: </b> All the registries of the logged in user should be displayed");
			Log.assertThat(
					actual_registries_count==registriesToBeCreated,
					"<b> Actual Result: </b> All the registries of the logged in user displayed successfully",
					"<b> Actual Result: </b> All the registries of the logged in user not displayed",
					driver);	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_060
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify whether the columns Event name, Event Type & Date are displayed and showing the respective values of each registries in the dialog box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_061(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		environmentPropertiesReader=EnvironmentPropertiesReader
				.getInstance("checkout");
		String eventType=environmentPropertiesReader.getProperty("weddingRegistry").split("\\|")[0];
		String eventDate=environmentPropertiesReader.getProperty("weddingRegistry").split("\\|")[1];
	
		String shippingaddress = testData.get("Address");;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		List<String> registryModal = Arrays.asList("modalDeleteRegistry");
		List<String> registryFields = Arrays.asList("lblEventName","lblEventType","lblEventDate");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");
	
			int registriesToBeCreated = 2;
			for (int i = 1; i <= registriesToBeCreated; i++) {
				RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
						.navigateToSection("registry");
				Log.message("5. Navigated to Edit profile page to add belk employee details");

				// Click on Create New Registry button
				registrySignedUserPage.clickOnCreateNewRegistry();
				Log.message("4. Click on Create New Registry button");

				// Enter the valid Event details to register
				registrySignedUserPage
						.fillingRegistryDetails("weddingRegistry");
				Log.message("5. Filling the valid event details");

				// Click on Continue button in Step 1
				registrySignedUserPage.clickContinueInCreateRegistry();
				Log.message("6. Click on Continue button in Event Registry page");

				// Update the PreEvent Address and Fill Post Event address
				registrySignedUserPage
						.fillingEventShippingdetails(shippingaddress);
				Log.message("7. Update the Pre Event address and Fill Post Event address");

				// Click on Continue button in Shipping address section
				registrySignedUserPage.clickOnContinueButtonInEventShipping();
				Log.message("8. Click on Continue button in shipping address");
	
				// Click on Submit button
				registrySignedUserPage.clickOnSubmitButton();
				Log.message("9. Click on Subtmit button in Post Event address");
			}			
			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("10. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("11. Navigated from PLP Page to PDP Page by selecting the random Product");

			pdppage.selectSize();
			Log.message("12. Selected size from the product");
			
			pdppage.selectColor();
			Log.message("13. Selected color from the product");
			
			pdppage.selectQuantity();
			Log.message("14. Selected qty from the product");
			
			RegistrySignedUserPage registrySignedUserPage=pdppage.navigateToRegistryAsSignedUser();
			Log.message("15. Clicked on 'Add item to registry' button in PDP page");
			
			Log.message("<b> Expected Result: </b> 'Select a Registry' modal should be displayed on clicking 'Add to registry' button in PDP");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(registryModal,
							registrySignedUserPage),
					"<b> Actual Result: </b> 'Select a Registry' modal displayed on clicking 'Add to registry' button in PDP",
					"<b> Actual Result: </b> 'Select a Registry' modal not displayed on clicking 'Add to registry' button in PDP",
					driver);
			
			Log.message("<b> Expected Result: </b> The columns Event name, Event Type & Date should be displayed");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(registryFields,
							registrySignedUserPage),
					"<b> Actual Result: </b> The columns Event name, Event Type & Date displayed in registry modal",
					"<b> Actual Result: </b> The columns Event name, Event Type & Date missing in registry modal",
					driver);	
		
			for(int i=1;i<=registriesToBeCreated;i++)
			{
				Log.message("<b> Expected Result: </b> Event name value should be displayed properly in registry modal");
				Log.assertThat(
						registrySignedUserPage.getEventNameByRow(i-1).equalsIgnoreCase("Event"),
						"<b> Actual Result: </b> Event name value- '"+registrySignedUserPage.getEventNameByRow(i-1)+"' displayed properly at row: "+i+" in registry modal",
						"<b> Actual Result: </b> Event name value is missing at row: "+i+" in registry modal",
						driver);	
				
				Log.message("<b> Expected Result: </b> Event name value should be displayed properly in registry modal");
				Log.assertThat(
						eventType.contains(registrySignedUserPage.getEventTypeByRow(i-1)),
						"<b> Actual Result: </b> Event type value- '"+registrySignedUserPage.getEventTypeByRow(i-1)+"' displayed properly at row: "+i+" in registry modal",
						"<b> Actual Result: </b> Event type value is missing at row: "+i+" in registry modal",
						driver);	
				
				Log.message("<b> Expected Result: </b> Event name value should be displayed properly in registry modal");
				Log.assertThat(
						registrySignedUserPage.getEventDateByRow(i-1).equalsIgnoreCase(eventDate),
						"<b> Actual Result: </b> Event Date value- '"+registrySignedUserPage.getEventDateByRow(i-1)+"' displayed properly at row: "+i+" in registry modal",
						"<b> Actual Result: </b> Event Date value is missing at row: "+i+" in registry modal",
						driver);	
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_061
	
	@Test(groups = { "desktop", "tablet",
	"mobile" }, description = "Verify system displays the Shipping Threshold message.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_050(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");


		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");


			homePage.headers.navigateTo(searchKey[0], searchKey[1], searchKey[2]);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Plp Page");

			// Navigating to product with productID
			PdpPage pdpPage = plpPage.navigateToPDPWithProductId();
			Log.message("3. Select product from search result page!");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			double productPrice;
			String txtProductPrice = pdpPage.getProductPrice();
			if(txtProductPrice.contains("$"))
				productPrice = Double.parseDouble(txtProductPrice.split("\\$")[1]);
			
			else
				productPrice = Double.parseDouble(txtProductPrice);
			double freeShippingCost = Double.parseDouble(pdpPage.getTextFreeShipping().split("\\$")[1]);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should see that the Shipping Threshold message is displayed.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(
					Arrays.asList("txtFreeShipping"), pdpPage) &&
					pdpPage.getTextFreeShipping().contains("Free Shipping"), 
					"<b>Actual Result 1: </b> User see that the Shipping Threshold message is displayed. " + pdpPage.getTextFreeShipping(), 
					"<b>Actual Result 1: </b> Shipping Threshold message is not displayed.", 
					driver);
			Log.message("<br>");
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Bag !");


			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");

			int productCount = 1;
			while(true){

				if(productCount * productPrice >= freeShippingCost){
					Log.message("<br>");
					
					Log.message(
							"<b>Expected Result 2:</b> User should see that the threshold message for qualified");
		Log.assertThat(
							shoppingBagPage.getTextfromFreeshipping().contains("qualify"), 
							"<b> Actual Result 2: </b> User see the threshold message as " + shoppingBagPage.getTextfromFreeshipping(), 
							"<b> Actual Result 2: </b> Threshold message is not properly displayed.", 
							driver);
					break;
				}

				else{
					Log.message("<br>");
						Log.message(
							"<b>Expected Result 3:</b>User should see that the threshold message for not yet qualified");
					
				Log.assertThat(
							shoppingBagPage.getTextfromFreeshipping().contains("Spend"), 
							"<b>Actual Result 3:</b> User see the threshold message as " + shoppingBagPage.getTextfromFreeshipping(), 
							"<b>Actual Result 3:</b> Threshold message is not properly displayed.", 
							driver);
					shoppingBagPage.selectQtyByIndex(productCount++);
				}

			}
			
			shoppingBagPage.removeItemsFromBag();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_050
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays grey out for the partially Out of Stock products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_022(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String size = "46 30";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage
					.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "'");
			
			Log.message("3. Navigated to 'Pdp' Page!");
			String selectedSize = pdpPage.selectSize(size);
			Log.message("4. Selected size: '" + selectedSize
					+ "' from color swatch in the PDP page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Color should be greyed out for the partially out of stock items");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("greyedOutColor"), pdpPage), 
					"Color is greyed out for the partially out of stock items", 
					"Color is not greyed out for the partially out of stock items", 
					driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_022
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the category landing page by clicking the brand name.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_007(String browser) throws Exception {
		// Loading the test data from excel using the test case id
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");
			PlpPage plpPage = new PlpPage(driver).get();

			//Selecting Brand from refinement panel
			plpPage.selectBrandOne();
			Log.message("Selected brand");
			String BrandnameOnRefinment= plpPage.getText();
			Log.message("3. Selected Brand "+BrandnameOnRefinment+" From Refinment Panel!");

			// Navigate to PDP page
			PdpPage pdpPage = plpPage.navigateToPDP();
			String BrandnameOnPdp =pdpPage.getText();
			Log.message("4. Navigate to PDP page and Selected a Product!");


			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search Result page should be displayed when user click on the Brand name from PLP!");
			Log.assertThat(
					BrandnameOnPdp.equals(BrandnameOnRefinment),
					"<b>Actual Result:</b> Specific product are Displayed after Selecting the Brand Name On plp!",
					"<b>Actual Result:</b> Specific product are not Displayed after Selecting the Brand Name On plp!",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} //TC_BELK_3B_PDP_007
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the Promotion message on PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_015(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");

			String promotionmsg = pdpPage.getPromotionalCalloutMessage();
			
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Promotion message should be displayed for the applicable products under the price link and Promo callout message should display 'View Details' link");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("calloutMessage","lnkViewDetails"), pdpPage),
					"<b>Actual Result 1a:</b> Promotion message :"+ "'" +promotionmsg + "'displayed on the PDP page!",								
					"<b>Actual Result 1a:</b> Promotion message not displayed on the PDP page!",driver);


		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_015

	@Test(groups = { "desktop", "mobile" }, description = "Verify SKU variant(color) are not displayed for fully Out of stock items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_023(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			pdpPage.selectColorByIndex(1);
			pdpPage.selectSizeByIndex(1);
			Log.message("<b>Expected Result: </b>Color should not be displayed for the fully Out of stock items");
			Log.assertThat(pdpPage.verifyColorGreyedOut(), "<b>Actual Result: </b>Color is not displayed for the fully Out of stock item", "<b>Actual Result: </b>Color is displayed for the fully Out of stock item", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_023

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the alternate Product Thumbnail image on PDP and allow user to click on it.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_038(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			pdpPage.selectColor();
			int noOfThumbailImgs = pdpPage.getCountOfThumbnailImages();
			Log.message("<b>Expected Result: </b>When a product has more than a single image associated with it, those Product thumbnails should display below the main image");
			Log.assertThat(
					(noOfThumbailImgs > 1)
							&& pdpPage
									.verifyThumbnailImgDisplayedUnderPrimaryImg(noOfThumbailImgs),
					"<b>Actual Result: </b>Product thumbnails are displayed below the main image for a product which has more than single image",
					"<b>Actual Result: </b>Product thumbnails are not displayed below the main image for a product which has more than single image", driver);
			pdpPage.clickThumbnailImageByIndex(noOfThumbailImgs);
			Log.message("<br");
			Log.message("3. Clicked alternate thumbnail image");
			Log.message("<br");
			Log.message("<b>Expected Result: </b>When the user clicks the alternate image, it should replace the main image of the product");
			String thumbnailURL = pdpPage.getThumbnailImageUrl(noOfThumbailImgs).split("&layer")[0];
			String primaryImgURL = pdpPage.getPrimaryImageColor();
			Log.assertThat(
					thumbnailURL.equals(primaryImgURL),
					"<b>Actual Result: </b>When the user clicks the alternate image, it is replaced with the main image of the product",
					"<b>Actual Result: </b>When the user clicks the alternate image, it is not replaced with the main image of the product", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_038
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify the allowed quantity of the product does not exceed the max_buy_qty.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_045(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			Log.message("<b>Pre-requisite: </b>Set max_buy_qty attribute for a product: '"+searchKey+"' as '7' in Business Manager");
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			pdpPage.selectColorByIndex(1);
			pdpPage.selectSizeByIndex(1);
			List<String> lstOptions = pdpPage.getQuantityOptions();
			Log.message("3. Clicked quantity dropdown");
			Log.message("<b>Expected Result: </b>User should see that maximum value displayed in the dropdown is '7'.");
			Log.assertThat(
					lstOptions.size() == 6,
					"<b>Actual Result: </b>User see that maximum value displayed in the dropdown is '7' as per given in Business Manager",
					"<b>Actual Result: </b>User can't see that maximum value displayed in the dropdown is '7' as per given in Business Manager", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_045

	@Test(groups = { "desktop", "mobile" }, description = "Verify the message after store details are fetched", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_077(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to 'Pdp' Page with randomly selected product");
			
			pdpPage.clickOnFindStore();
			Log.message("3. Clicked on 'Find in store' link");
			
			Log.message("<br>");			
			Log.message("<b>Expected Result 1:</b> 'Find in store' pop up should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("FindInstorePopUp"),
							pdpPage),
					"<b>Actual Result 1:</b> 'Find in store' pop up is displayed",
					"<b>Actual Result 1:</b> 'Find in store' pop up is not displayed",
					driver);
			
			pdpPage.enterZipcodeForStoreSearch("28205");
			Log.message("4. Entered Zipcode");
			
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Store heading should be displayed");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("storeHeading"),
							pdpPage),
					"<b>Actual Result 2:</b> Store heading is displayed",
					"<b>Actual Result 2:</b> Store heading is not displayed",
					driver);
							
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_077
	
	//TC_BELK_PDP_077
	@Test(groups = { "desktop", "mobile" }, description = "Verify Change your location link in the store search results window", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_078(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to 'Pdp' Page with randomly selected product");
			
			pdpPage.clickOnFindStore();
			Log.message("3. Clicked on 'Find in store' link");
			
			pdpPage.enterZipcodeForStoreSearch("28205");
			Log.message("4. Entered Zipcode");			
			
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Store heading should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("storeHeading"),
							pdpPage),
					"<b>Actual Result 1:</b> Store heading is displayed",
					"<b>Actual Result 1:</b> Store heading is not displayed",
					driver);
					
			Log.message("<br>");	
			Log.message("<b>Expected Result 2:</b> 'Change your location' link should be displayed");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkChangeLocation"),
							pdpPage),
					"<b>Actual Result 2:</b> 'Change your location' link is displayed",
					"<b>Actual Result 2:</b> 'Change your location' link is not displayed",
					driver);
					
				
			pdpPage.clickOnChangeYourLocation();
			Log.message("5. Clicked on 'Change your location' link ");
			
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> 'Find in store' pop up should be displayed");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("FindInstorePopUp"),
							pdpPage),
					"<b>Actual Result 3:</b> 'Find in store' pop up is displayed",
					"<b>Actual Result 3:</b> 'Find in store' pop up is not displayed",
					driver);
			
			pdpPage.enterZipcodeForStoreSearch("28205");
			Log.message("6. Entered Zipcode");		
			
			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> Search Result for the zipcode should be displyed");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("storeResult"),
							pdpPage),
					"<b>Actual Result 4:</b> Search Result for the zipcode is displyed",
					"<b>Actual Result 4:</b> Search Result for the zipcode is not displyed",
					driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_078
	
	//TC_BELK_PDP_077
	@Test(groups = { "desktop", "mobile" }, description = "Verify ​'Reviews' tab in PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_072(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to 'Pdp' Page with randomly selected product");
			
			pdpPage.scrollDownToReviewTab();
			
			Log.message("<br>");
			
			Log.message("<b>Expected Result :</b> Review tab should be displayed");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("reviewTab"),
							pdpPage),
					"<b>Actual Result :</b> Review tab is displayed",
					"<b>Actual Result :</b> Review tab is not displayed",
					driver);
			
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_078
	
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays limited availability message", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_048(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] message = testData.get("SocailMediaLinks").split("\\|");
		String actualMessage = null;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			pdpPage.selectColorByIndex(1);
			pdpPage.selectSizeByIndex(1);
			Log.message("<b>Expected Result: </b>Limited Availability message: '"+message[0]+"' should be shown for only 1 item left in stock");
			actualMessage=pdpPage.StockTxt();
			Log.softAssertThat(actualMessage.equals(message[0]), "<b>Actual Result: </b>Limited Availability message: '"+message[0]+"' is shown for only 1 item left in stock", "<b>Actual Result: </b>Limited Availability message: '"+message[0]+"' is not shown for only 1 item left in stock", driver);
			pdpPage.selectSizeByIndex(1);
			Log.message("<b>Expected Result: </b>Limited Availability message: '"+message[1]+"' should be shown for only 2 item left in stock");
			actualMessage=pdpPage.StockTxt();
			Log.softAssertThat(actualMessage.equals(message[1]), "<b>Actual Result: </b>Limited Availability message: '"+message[1]+"' is shown for only 2 item left in stock", "<b>Actual Result: </b>Limited Availability message: '"+message[1]+"' is not shown for only 2 item left in stock", driver);
			pdpPage.selectSizeByIndex(2);
			Log.message("<b>Expected Result: </b>Limited Availability message: '"+message[2]+"' should be shown for only 3 item left in stock");
			actualMessage=pdpPage.StockTxt();
			Log.softAssertThat(actualMessage.equals(message[2]), "<b>Actual Result: </b>Limited Availability message: '"+message[2]+"' is shown for only 3 item left in stock", "<b>Actual Result: </b>Limited Availability message: '"+message[2]+"' is not shown for only 3 item left in stock", driver);
			pdpPage.selectSizeByIndex(3);
			Log.message("<b>Expected Result: </b>Limited Availability message: '"+message[3]+"' should be shown for only 4 item left in stock");
			actualMessage=pdpPage.StockTxt();
			Log.softAssertThat(actualMessage.equals(message[3]), "<b>Actual Result: </b>Limited Availability message: '"+message[3]+"' is shown for only 4 item left in stock", "<b>Actual Result: </b>Limited Availability message: '"+message[3]+"' is not shown for only 4 item left in stock", driver);
			pdpPage.selectSizeByIndex(4);
			Log.message("<b>Expected Result: </b>Limited Availability message: '"+message[4]+"' should be shown for only 5 item left in stock");
			actualMessage=pdpPage.StockTxt();
			Log.softAssertThat(actualMessage.equals(message[4]), "<b>Actual Result: </b>Limited Availability message: '"+message[4]+"' is shown for only 5 item left in stock", "<b>Actual Result: </b>Limited Availability message: '"+message[4]+"' is not shown for only 5 item left in stock", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_048
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify Enter Zip Code field in the zip code store modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_076(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			FindInStorePage findInStorePage = pdpPage.headers.navigateToFindInStore();
			Log.message("<b>Expected Result: </b>Enter Zip Code text field should be displayed in the zip code store modal");
			Log.assertThat(
					findInStorePage.elementLayer.verifyPageElements(
							Arrays.asList("fldZipcode"), findInStorePage),
					"<b>Actual Result: </b>Enter Zip Code text field is displayed in the zip code store modal",
					"<b>Actual Result: </b>Enter Zip Code text field is not displayed in the zip code store modal", driver);
			findInStorePage.typeZipCode("28205");
			Log.message("3. Entered zipcode '28205' in Zipcode text field");
			Log.message("<b>Expected Result: </b>The user should be able to enter a zip code to search for stores");
			Log.assertThat(
					findInStorePage.getZipCodeEnteredValue().equals("28205"),
					"<b>Actual Result: </b>Enter Zip Code text field is displayed in the zip code store modal",
					"<b>Actual Result: </b>Enter Zip Code text field is not displayed in the zip code store modal", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_076
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify the Add to Shopping Bag button when color not selected", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_051(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Color is not selected");
			Log.message("<b>Expected Result: </b>User should see that the 'Add to Shopping Bag' button is disabled upon not selecting the color field.");
			Log.assertThat(!pdpPage.verifyAddtoBagIsEnabled(), "<b>Actual Result: </b>The 'Add to Shopping Bag' button is disabled upon not selecting the color field.", "<b>Actual Result: </b>The 'Add to Shopping Bag' button is not disabled upon not selecting the color field.", driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_051
}
