package com.belk.testscripts.productdetails;

import java.util.*;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.*;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.support.*;


@Listeners(EmailReport.class)
public class ProductSetFinal {
	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "PDP";
	List<String> collectionBadge = Arrays.asList("productBadge");

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Product Image in the Product Set details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_107(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		String productId = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(productId);
			Log.message("2. Searched with keyword '" + productId
					+ "' and Navigated to PDP Page");
			Log.message("3. Selected Product by index and navigated to PDP!");

			String mainImgSrc = pdpPage.getProductImageUrl().split("_")[0].trim();
			Log.message("4. Get the selected Product image URL");
			String firstProductImgSrc = pdpPage.getImageOfProductSetByRow(1);
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> The product set image should display the main image assigned to the product set.");
			Log.message("<b>Expected Result 1a: </b> When no image is assigned, the system should display the first image assigned to the first available product in the product set.");
			Log.assertThat(mainImgSrc.equals(firstProductImgSrc),"<b>Actual Result 1: </b>The product set image displayed as Expected!","<b>Actual Result 1: </b>The product set image not displayed as Expected.",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_092
	

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify collection Badges is properly displayed in product Set Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_108(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);
		String searchkey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchkey);
			Log.message("2. Navigated to '" + searchkey + "' search result Page");

			// Select the product according to search keyword
			PdpPage pdpPage = searchResultpage.selectProductByIndex(1);
			Log.message("3. Selected the product by index and navigated to PDP!");

			// Verifying the existence of Product badge in PDP
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> In Product Sets page below product thumbnail page 'Collection' text should be properly shown");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(collectionBadge,
							pdpPage),
					"<b>Actual Result 1: </b> In Product Sets page, 'Collection' badge displayed properly",
					"<b>Actual Result 1: </b> The Collection badge not displayed in PDP of product set",
					driver);

			// Clicking on Product badge
			pdpPage.clickProductSetBadge();
			Log.message("4. Clicked on Product badge in Product set PDP !");

			// Verifying the existence of Product badge in PDP
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> Page should not be redirected to any page when clicking on 'Collection' badge");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(collectionBadge,
							pdpPage),
					"<b>Actual Result 2: </b> Page not redirected to any page when clicking on 'Collection' badge",
					"<b>Actual Result 2: </b> Page redirected to other page when clicking on 'Collection' badge",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_093

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify alternative image is properly displayed in product Set Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_109(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);
		String searchkey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchkey);
			Log.message("2. Searched with keyword '" + searchkey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page !");

			// Verifying the existence of Product badge in PDP
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> Alternative images should be shown below collection badges in Product Sets Page");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstImageThumbnail"), pdpPage),
					"<b>Actual Result 1: </b> Alternative images displayed below collection badges in Product Sets Page",
					"<b>Actual Result 1: </b> Alternative images not displayed below collection badges in Product Sets Page",
					driver);
			Log.message("</br>");
		  String imagesViewBeforeRightClick = pdpPage.getCarouselStyle();
			Log.message("4. Navigated to the PDP and verify the images carousel style before clicked on the arrow");

			pdpPage.clickRightImageArrow();
			Log.message("5. Clicked on the Right Image arrow in the PDP");
			String imagesViewAfterRightClick = pdpPage.getCarouselStyle();
			Log.message("6. The carousal style after clicked on right arrow.");
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> While clicking on right arrow in alternative image panel, images should slide to the right side");
			Log.assertThat(
					!imagesViewBeforeRightClick
							.equals(imagesViewAfterRightClick),
					"<b>Actual Result 2: </b> Images slided to the right side on clicking right arrow",
					"<b>Actual Result 2: </b> Images not slided to the right side on clicking right arrow",
					driver);
			pdpPage.clickLeftImageArrow();
			
			Log.message("7. Clicked on the Left Image arrow in the PPD");
			String imagesViewAfterLeftClick = pdpPage.getCarouselStyle();
			Log.message("8. The carousal style after clicked on Left arrow.");
			Log.message("</br>");
			Log.message("<b>Expected Result 3: </b> While clicking on left arrow in alternative image panel, images should slide to the right side");
			Log.assertThat(
					!imagesViewAfterLeftClick.equals(imagesViewAfterRightClick),
					"<b>Actual Result 3: </b> Images slided to the left side on clicking left arrow",
					"<b>Actual Result 3: </b> Images not slided to the left side on clicking left arrow",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_094

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify UPC ID is properly displayed below the product image in Product Set page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_116(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblUPC");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP Page !");
			pdpPage.selectColorByIndex(1);
			Log.message("4. Select the Color.");
			pdpPage.selectSizeByIndex(1);
			Log.message("5. Select the Size.");
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> When selecting any size from size dropdown box UPC id should be and displayed below the Product name in Product Set page.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(element, pdpPage),
					"<b>Actual Result 1: </b> UPC is displaying below the Product name in Product Set page !",
					"<b>Actual Result 1: </b> UPC is not  displaying below the Product name in Product Set page !",
					driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_116
	
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify UPC ID is not displayed below the product image in Product Set page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_117(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblUPC");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage =  homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and navigated to search PDP  Page");
			
			Log.message("<br>");
			Log.message("<b>Expected Result : </b> When size are not selected from 'Size' drop down box, UPC id should not be displayed below the Product name in Product Set page.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(element, pdpPage),"<b>Actual Result : </b> UPC is not displaying below the Product name in Product Set page without selecting the size !","<b>Actual Result 1: <b> UPC is displaying below the Product name in Product Set page without selecting the size !",driver);
			
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_117
	

	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify 'Add to Shopping Bag' button when selecting Size from Product set page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_123(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		ArrayList<String> UPCinPDPpage = new ArrayList<String>();
		ArrayList<String> UPCinBagPage = new ArrayList<String>();
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP Page !");
			int count = pdpPage.childProductSets.size();
			Log.message("4. Count of product is :" + count);
			// pdpPage.verifyAddtoBagIsEnabled();
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElementsDisabled(
							Arrays.asList("btnAddToShoppingBag"), pdpPage),
					"5. 'Add to Shopping Bag' button is disabled when size is not selected.",
					"5. 'Add to Shopping Bag' button not disabled when size is not selected.",
					driver);
			String size = pdpPage.selectSize();
			Log.message("6. Select the size: " + size);
			UPCinPDPpage = pdpPage.getUPCofProductSetPage();
			Log.message("7. UPC of product is :" + UPCinPDPpage);
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> Add to Shopping Bag button should be enabled in Product Set Page.");
			Log.assertThat(
					(pdpPage.elementLayer.verifyPageListElements(
							Arrays.asList("btnAddToShoppingBag"), pdpPage)),
					"<b>Actual Result 1: </b> 'Add to Shopping Bag' button is enabled when size is selected.",
					"<b>Actual Result 1: </b> 'Add to Shopping Bag' button is disabled when size is selected.",
					driver);
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b> Add all to Shopping Bag button should not be enabled, when only one child product is selected.",
					driver);
			Log.assertThat(
					!pdpPage.verifyAddAlltoBagIsEnabled(),
					"<b>Actual Result 2: </b> AddAllToBag button is disabled after selecting only one Child Product !",
					"<b>Actual Result 2: </b> AddAllToBag button is not disabled after selecting only one Child Product !",
					driver);
			Log.message("</br>");
			Log.message("<b>Expected Result 3: </b> When Clicking Add to Shopping Bag button, the selected item should be properly added in My Shopping Bag.");
			pdpPage.clickAddToBagByIndex(1);
			Log.message("8.Clicked on the AddToShoppingBag Button !");
			pdpPage.minicart.navigateToBag();
			Log.message("9. Navigated to the mini cart");
			UPCinBagPage = pdpPage.getUPCofProductSetPageInBag();
			Log.message("10. UPC of product is :" + UPCinBagPage);
			Log.assertThat(UPCinPDPpage.equals(UPCinBagPage),
					"<b>Actual Result 3: </b> The product is added properly in to MyBag !",
					"<b>Actual Result 3: </b> The product is not added properly in to MyBag !", driver);
			Log.testCaseResult();

		}// try

		catch (Exception e) {
			Log.exception(e, driver);

		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_123

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'Add to Shopping Bag' button when selecting Qty from Product set page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_124(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page !");

			int total_child_productSets = pdpPage.childProductSets.size();
			Log.message("4. The total no. of child product sets displayed are- "+ total_child_productSets);

			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> \"Add to Shopping Bag\" button should be enabled when Qty alone not selected in Product set page");
			Log.assertThat(pdpPage.elementLayer.verifyPageListElementsDisabled(
							Arrays.asList("btnAddToShoppingBag"), pdpPage),
					"<b>Actual Result 1: </b> \"Add to Shopping Bag\" buttons enabled when size is selected",
					"<b>Actual Result 1: </b>\"Add to Shopping Bag\" buttons got enabled even when size/Qty not selected",driver);
			Log.message("<br>");
			
			// select random size in product sets
			pdpPage.selectSizeRandomInProductSet();
			Log.message("5. Random size selected for all the products in Product Set!");

			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b> \"Add all to Shopping Bag\" button should not be enabled in Product Set page until all child products are selected");

			Log.assertThat(
					(pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("addAllToShoppingBagForProductSet"),
							pdpPage)),
					"<b>Actual Result 2: </b> \"Add all to Shopping Bag\" button got enabled only after selecting sizes of all the sizes",
					"<b>Actual Result 2: </b> \"Add all to Shopping Bag\" button got enabled even without selecting 'size' of all childs",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_108

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding registry in product set detail page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_125(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			 MyAccountPage myAcc=signIn.signInToMyAccount(emailid, password);
             ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
             if(!bagPage.isShoppingBagEmpty()){
				  bagPage.removeItemsFromBag();
				  }
            
			// Load PDP Page for Search Keyword 
            PdpPage pdpPage = myAcc.headers
 					.searchAndNavigateToPDP(searchKey);
 			Log.message("2. Searched with keyword '" + searchKey
 					+ "' and Navigated to PDP Page");
			Log.message("5. Navigated to Product Set Page loaded with child products properly!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b>Verify size is not selected in product sets.");

			Log.assertThat(pdpPage.verifyDefaultSizeStatusInProductSet(),
					"<b>Actual Result 1: </b>Size is not selected!",
					"<b>Actual Result 1: </b>Size is selected.",driver);
			
			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b>Verify 'Add to Shopping Bag' button is disabled when size is not selected.");
			Log.assertThat(
					(pdpPage.elementLayer.verifyPageListElementsDisabled(Arrays.asList("btnAddToShoppingBag"), pdpPage)),
					"<b>Actual Result 2: </b>'Add to Shopping Bag' button is disabled when size is not selected.",
					"<b>Actual Result 2: </b>'Add to Shopping Bag' button not disabled when size is not selected.",
					driver);

			Log.message("<br>");
           // select random size in product sets
			pdpPage.selectColorByIndex(1);
			Log.message("6. Color is selected");
			pdpPage.selectSizeRandomInProductSet();
			Log.message("7. Random size selected for all the products in Product Set!");

			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b>Verify 'Add to Shopping Bag' button should be enabled when size selected.");
			Log.assertThat(
					(pdpPage.elementLayer.verifyPageListElements(Arrays.asList("btnAddToShoppingBag"), pdpPage)),
					"<b>Actual Result 3: </b>'Add to Shopping Bag' button enabled when size selected.",
					"<b>Actual Result 3: </b>'Add to Shopping Bag' button not enabled when size selected.",
					driver);
			
			Log.message("<br>");
			Log.message("<b>Expected Result 4: </b>Verify 'Add to Registry' button should be enabled when size selected.");
			Log.assertThat((pdpPage.elementLayer.verifyPageListElements(Arrays.asList("btnRegistrySigneduserInProductSet"), pdpPage)),"<b>Actual Result 4: </b>'Add to Registry' button enabled when size selected.","<b>Actual Result 4: </b>'Add to Registry' button not enabled when size selected.",driver);
			Log.message("</br>");
			// Click on Registry link on 
			RegistryGuestUserPage registryguestuserpage = pdpPage.clickOnRegistrySignInProductSet(2);
			Log.message("8. Navigated to My Registry Page properly!");
            // Checking the elements in Registry Page 
			Log.message("<br>");
			Log.message("<b>Expected Result 5: </b>Verify the fields  Event name,Event Type,Date,Apply and Cancel button ");
			Log.assertThat(registryguestuserpage.elementLayer.verifyPageElements(Arrays.asList("eventName","eventDate","eventType"), registryguestuserpage) &&
							registryguestuserpage.elementLayer.verifyPageElements(Arrays.asList("cancelButtonRegistryPage","continueButtonRegistryPage"), registryguestuserpage),
					"<b>Actual Result 5: </b> These Fields should be properly shown in Registry screen: Event name,Event Type,Date,Apply and Cancel button.",
					"<b>Actual Result 5: </b> These Fields should be properly shown in Registry screen: Event name,Event Type,Date,Apply and Cancel button.",
					driver);
            Log.message("<br>");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_109
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding items to wish list in product set detail page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_127(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = myAccountPage.headers
 					.searchAndNavigateToPDP(searchKey);
 			Log.message("2. Searched with keyword '" + searchKey
 					+ "' and Navigated to PDP Page");
			Log.message("5. Navigated to PDP from Search results page !");

			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b>Verify size is not selected in product sets.");

			Log.assertThat(pdpPage.verifyDefaultSizeStatusInProductSet(),
					"<b>Actual Result 1: </b>Size is not selected!",
					"<b>Actual Result 1: </b>Size is selected.",driver);

			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b>Verify 'Add to Shopping Bag' button is disabled when size is not selected.");
			Log.assertThat(pdpPage.elementLayer.verifyPageListElementsDisabled(
							Arrays.asList("btnAddToShoppingBag"), pdpPage),
					"<b>Actual Result 2: </b>'Add to Shopping Bag' button is disabled when size is not selected.",
					"<b>Actual Result 2: </b>'Add to Shopping Bag' button not disabled when size is not selected.",
					driver);
            Log.message("<br>");
			// select random size in product sets
			pdpPage.selectSizeRandomInProductSet();
			Log.message("6. Random size selected for all the products in Product Set!");
           // Clicking on Add To Wishlist Link for 1st Product in product set
			pdpPage.clickAddToWishListLinkByProduct(1);
			Log.message("7. 'Add To Wishlist' link clicked for 1'st Product!");

			// verify 'Item added to List' success message
			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b>User should see 'Item added to list' success message.");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstWishListMessage"), pdpPage),
					"<b>Actual Result 3: </b>'Item added to list' success message is displayed!",
					"<b>Actual Result 3: </b>'Item added to list' success message not displayed",
					driver);

			// verify 'Item added to List' success message is displayed in green
			// color with tick mark
			Log.message("<br>");
			Log.message("<b>Expected Result 4: </b>User should see 'Item added to list' success message.");
			Log.assertThat(
					pdpPage.verifyWishListMessageByProduct(1,pdpPage),
					"<b>Actual Result 4: </b>'Item added to list' success message is displayed!",
					"<b>Actual Result 4: </b>'Item added to list' success message not displayed",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_111

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding multiple items to wish list in product set detail page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_128(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = myAccountPage.headers
 					.searchAndNavigateToPDP(searchKey);
 			Log.message("2. Searched with keyword '" + searchKey
 					+ "' and Navigated to PDP Page");
			Log.message("5. Navigated to PDP from Search results page !");

			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b>Verify size is not selected in product sets.");

			Log.assertThat(pdpPage.verifyDefaultSizeStatusInProductSet(),
					"<b>Actual Result 1: </b>Size is not selected!",
					"<b>Actual Result 1: </b>Size is selected.",driver);

			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b>Verify \"Add to Shopping Bag\" button is disabled when size is not selected.");
			Log.assertThat(pdpPage.elementLayer.verifyPageListElementsDisabled(
							Arrays.asList("btnAddToShoppingBag"), pdpPage),
					"<b>Actual Result 2: </b>\"Add to Shopping Bag\" button is disabled when size is not selected.",
					"<b>Actual Result 2: </b>\"Add to Shopping Bag\" button not disabled when size is not selected.",
					driver);

			Log.message("<br>");
			// select random size in product sets
			pdpPage.selectSizeRandomInProductSet();
			Log.message("6. Random size selected for all the products in Product Set!");

			// Clicking on Add To Wishlist Link for 1st Product in product set
			pdpPage.clickAddToWishListLinkByProduct(1);
			Log.message("7. 'Add To Wishlist' link clicked for 1'st Product!");

			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b>User should see \"Item added to list\" success message.");
			Log.assertThat(pdpPage.verifyWishListMessageByProduct(1,pdpPage),
					"<b>Actual Result 3: </b>\"Item added to list\" success message is displayed!",
					"<b>Actual Result 3: </b>\"Item added to list\" success message not displayed",
					driver);
			Log.message("</br>");
			// Clicking on Add To Wishlist Link for 2nd Product in product set
			pdpPage.clickAddToWishListLinkByProduct(2);
			Log.message("8. 'Add To Wishlist' link clicked for 2nd Product!");

			Log.message("<br>");
			Log.message("<b>Expected Result 4: </b>User should see \"Item added to list\" success message.");
			Log.assertThat(pdpPage.verifyWishListMessageByProduct(2,pdpPage),
					"<b>Actual Result 4: </b>\"Item added to list\" success message is displayed!",
					"<b>Actual Result 4: </b>\"Item added to list\" success message not displayed",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_112
	
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Buy All For xx.xx text in product Set Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_129(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
 					.searchAndNavigateToPDP(searchKey);
 			Log.message("2. Searched with keyword '" + searchKey
 					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page!");
			Log.message("</br>");
			// verify 'Buy All For $xx.xx label
			Log.message("<b>Expected Result 1: </b> User Should see above 'Add all Shopping to Bag' button 'Buy All For$xx.xx'");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lblBuyAll"),
							pdpPage),
					"<b>Actual Result 1: </b> 'Buy All for $xx.xx' is displayed above 'Add all Shopping to Bag' button!",
					"<b>Actual Result 1: </b> 'Buy All for $xx.xx' not displayed above 'Add all Shopping to Bag' button!",driver);
			Log.message("</br>");
			// verify that page should not be redirected to any other page when
			// click 'Buy All For' label
			String urlBeforClickingBuyAll = driver.getCurrentUrl();
			Log.message("4. The Before Clicked URL is: " + urlBeforClickingBuyAll);
			pdpPage.clickBuyForAll();
			Log.message("5.Clicked Buy For All in the PDP.");
			String urlAfterClickingBuyAll = driver.getCurrentUrl();
			Log.message("6. The After Clicked URL is: " + urlAfterClickingBuyAll);
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> While clicking on 'Buy All for $xx.xx' text page should not redirected to any page.");
			Log.assertThat(
					urlBeforClickingBuyAll.equals(urlAfterClickingBuyAll),
					"<b>Actual Result 2: </b>Page is not redirected to any page while clicking on 'Buy All for $xx.xx' text!",
					"<b>Actual Result 2: </b>Page is not redirected to any page while clicking on 'Buy All for $xx.xx' text!",
					driver);

			// verify sum of all products in product set is equal to Buy All for
			// $xx.xx
			Log.message("</br>");
			Log.message("<b>Expected Result 3: </b> Buy All for $xx.xx' where $xx.xx should be sum of all the child products current sale price.");
			Log.assertThat(
					pdpPage.verifyTotalSumInBuyForAll(),
					"<b>Actual Result 3: </b> 'Buy All for $xx.xx' where $xx.xx is sum of all the child products current sale price.",
					"<b>Actual Result 3: </b> 'Buy All for $xx.xx' where $xx.xx is not sum of all the child products current sale price.",
					driver);
           Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_PDP_113
	
	@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify 'Add All To Shopping Bag' button in product Set Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_130(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailId = testData.get("EmailAddress");
		String passWord = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		ArrayList<String> UPCinPDPpage = new ArrayList<String>();
		ArrayList<String> UPCinBagPage = new ArrayList<String>();
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAcc = signIn.signInToMyAccount(emailId, passWord);
			ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP Page !");
			int count = pdpPage.childProductSets.size();
			Log.message("4. count of product is :" + count);
			pdpPage.selectSizeDropDownsInProductSet();
			pdpPage.selectColor();
			UPCinPDPpage = pdpPage.getUPCofProductSetPage();
			Log.message("5. UPC of products are :" + UPCinPDPpage);
			// Collections.sort(UPCinPDPpage);
			Log.message("</br>");
			Log.message("<b>Expected Result 1: </b> 'Add all To Shopping Bag' button should be enabled only when all child products are selected in Product Set page.");
			Log.assertThat(
					pdpPage.verifyAddAlltoBagIsEnabled(),
					"<b>Actual Result 1: </b> AddAllToBag button is Enabled when all child products are selected !",
					"<b>Actual Result 1: </b> AddAllToBag button is not Enabled when all child products are selected!",
					driver);
			pdpPage.clickAddToBag("product set");
			pdpPage.minicart.navigateToBag();
			Log.message("6. Clicked on MiniCart and Navigated to ShoppingBagPage !");
			UPCinBagPage = pdpPage.getUPCofProductSetPageInBag();
			Log.message("7. UPC of products are :" + UPCinBagPage);
			// Collections.sort(UPCinPDPpage);
			boolean Status = false;
			for (String UPC : UPCinPDPpage) {
				if (UPCinBagPage.contains(UPC)) {
					Status = true;
				} else {
					Status = false;
					break;
				}
			}
			Log.message("</br>");
			Log.message("<b>Expected Result 2: </b> User should be allowed to place all the order available in My Shopping bag screen.");

			Log.assertThat(
					Status,
					"<b> Actual Result 2: </b> The Added Product is same in MyBagPage !",
					"<b> Actual Result 2: </b> The Added Product is not same in MyBagPage !",
					driver);
			Log.testCaseResult();
		}// try

		catch (Exception e) {
			Log.exception(e, driver);

		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_PDP_130

	
	@Test(groups = { "desktop", "mobile" }, description = "Verify rating and review details is properly displayed in product set page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_118(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers
					.searchAndNavigateToPDP(searchkey);
			Log.message("2. Searched with keyword '" + searchkey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>Without selecting any size from dropdown box Rating & Review details should be properly displayed below the product name");
			Log.assertThat(pdppage.verifyRateAndReviewBeforeSelectSize() , "<b>Actual Result:</b> Rate and reviews are present properly below product name ", "<b>Actual Result:</b> Rate and reviews are not present properly below product name");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>When size is selected Rating & Review details should be properly displayed below the UPC Id.");
			pdppage.selectSizeDropDownInProductSet();
			Log.assertThat(pdppage.verifyRateAndReviewAfterSelectSize(), "<b>Actual Result:</b> Rate and reviews are present properly below product upc number after slecting size", "<b>Actual Result:</b> Rate and reviews are not present properly below product upc number after selecting size");
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}//TC_BELK_PDP_118

	@Test(groups = { "desktop", "mobile" }, description = "Verify product name is properly shown below brand name", 

			dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_115(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchkey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers
					.searchAndNavigateToPDP(searchkey);
			Log.message("2. Searched with keyword '" + searchkey
					+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to PDP from Search results page !");

			Log.message("<br>");

			Log.message("<b>Expected Result:</b> Product name should be properly shown below the selected Brand name.");
			Log.assertThat(pdppage.verifyProductNameDisplayedUnderBrandName(), "<b>Actual Result:</b> Product name is properly shown below the selected Brand name.", "<b>Actual Result:</b> Product name is not properly shown below the selected Brand name.");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> product image should be properly shown.");
			Log.assertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("imgOfProduct"), pdppage), "<b>Actual Result:</b> Product image is displayed in the pdp page properly", "<b>Actual Result:</b> Product image is not displayed in the pdp page properly");
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}//TC_BELK_PDP_115

}
