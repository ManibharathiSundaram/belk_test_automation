package com.belk.testscripts.productdetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CategoryLandingPage;
import com.belk.pages.CouponsPage;
import com.belk.pages.HomePage;
import com.belk.pages.PlpPage;
import com.belk.pages.QuickViewPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.WishListPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class QuickViewFinal {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "PDP";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System.getProperty("webSite")
				: context.getCurrentXmlTest().getParameter("webSite"));
	}

	// TC_BELK_QUICKVIEW_119
	@Test(groups = {
			"desktop" }, description = "Verify badges in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_136(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

			String[] searchKey = testData.get("SearchKey").split("\\|");
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			List<String> productBadge = Arrays.asList("btnProductBadge");

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey[0]);
				Log.message("2. Searched with the  '" + searchKey
						+ "' in search text box and navigated to 'Search Result'  Page!");
				QuickViewPage quickview = searchresultPage.navigateToQuickViewByIndex(1);
				Log.message("3. Clicked on Quick View button");
				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b> Product Badge should be displayed below the product in Search Results page");
				Log.assertThat(searchresultPage.elementLayer.verifyPageElements(productBadge, quickview),
						"<b>Actual Result:</b> Product Badge is displayed below the product image in Quick view page",
						"<b>Actual Result:</b> Product Badge is not displayed displayed below the produt image in Quick view page)",
						driver);
				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	} // TC_BELK_QUICKVIEW_119

	@Test(groups = {
			"desktop" }, description = "Verify clicking on the social media links in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_139(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] socialLinks = testData.get("SocailMediaLinks").split(",");
		List<String> socialMedia = Arrays.asList("lnkPintrest", "lnkFacebook", "lnkTwitter", "lnkGooglePlus",
				"lnkEmail");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewpage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to quickView Page with randomly selected product");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' should shown in the 'QuickView' page");

			Log.assertThat(quickViewpage.elementLayer.verifyPageElements(socialMedia, quickViewpage),
					"<b>Actual Result 1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' are displayed in the 'QuickView' page",
					"<b>Actual Result 1:</b> Social Media icons are not displayed in the 'QuickView' Page", driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> By Clicking on 'Pinterest' Icon, It should navigate to corresponding 'Pinterest' page");

			quickViewpage.navigateToPinterestPage();
			Log.softAssertThat(socialLinks[2].replaceAll(" ", "").contains("pinterest"),
					"<b>Actual Result 2:</b> By Clicking on Pinterest Icon, It is navigated to 'Pinterest' page",
					"<b>Actual Result 2:</b> By Clicking on Pinterest Icon, It is not navigated to 'Pinterest' page",
					driver);
			// BrowserActions.navigateToBack(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> By Clicking on 'FaceBook' Icon, It should navigate to corresponding 'FaceBook' page");

			quickViewpage.navigateToFacebookPage();
			Log.softAssertThat(socialLinks[0].replaceAll(" ", "").contains("facebook"),
					"<b>Actual Result 3:</b> By Clicking on FaceBook Icon, It is navigated to 'FaceBook' page",
					"<b>Actual Result 3:</b> By Clicking on FaceBook Icon, It is not navigated to 'FaceBook' page",
					driver);
			// BrowserActions.navigateToBack(driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result4:</b> By Clicking on 'Twitter' Icon, It should navigate to corresponding 'Twitter' page");

			quickViewpage.navigateToTwitterPage();
			Log.softAssertThat(socialLinks[1].replaceAll(" ", "").contains("twitter"),
					"<b>Actual Result 4:</b> By Clicking on Twitter Icon, It is navigated to 'Twitter' page ",
					"<b>Actual Result 4:</b> By Clicking on Twitter Icon, It is not navigated to 'Twitter' page",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 5:</b> By Clicking on 'GooglePlus' Icon, It should navigate to corresponding 'GooglePlus' page");

			quickViewpage.navigateToGooglePlusPage();
			Log.softAssertThat(socialLinks[5].replaceAll(" ", "").contains("google"),
					"<b>Actual Result 5:</b> By Clicking on GooglePlus Icon, It is navigated to 'GooglePlus' page",
					"<b>Actual Result 5:</b> By Clicking on GooglePlus Icon, It is not navigated to 'GooglePlus' page",
					driver);
			BrowserActions.navigateToBack(driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_122

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify UPC for the variation masters product in Quick view screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_144(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblUPC");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			// Load the QuickView Page with search keyword
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
			Log.message("3. Navigated to QuickViewPage !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("UPC should not be displayed for variation masters product.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(!searchResultPage.QVStateNone(), "QuickViewScreen properly loaded !",
					"QuickViewScreen not loaded properly !");
			quickViewPage.selectSizeByIndex(1);
			Log.message("4. Selected random size value .");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElementsDoNotExist(element, quickViewPage),
					"UPC is not displaying for variation Master Product !",
					"UPC is displaying for variation Master Product !", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_144

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify UPC for the variation product in Quick view screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_145(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lblUPC");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			// Load the QuickView Page with search keyword
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickViewPage !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"UPC should be displayed with variation selected for variation product only after selecting color and size.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(!searchResultPage.QVStateNone(), "QuickViewScreen properly loaded !",
					"QuickViewScreen not loaded properly !");
			quickViewPage.selectSize();
			Log.message("4. Selected random size value .");
			quickViewPage.selectColor();
			Log.message("5. Selected random color value .");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(element, quickViewPage),
					"UPC value displayed with variation selected for variation product only after selecting color and size.",
					"UPC value not displayed with variation selected for variation product only after selecting color and size.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_145

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the product image displayed on 'Quick view' for a Product set.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_173(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] searchKey = testData.get("SearchKey").split("\\|");
		List<String> element = Arrays.asList("productSetImg");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load the SearchResult Page with search keyword
			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey[0]);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to search result Page");
			// Navigated to the Quick View window
			// int index = searchresultPage.getIndexByProductName(searchKey[1]);
			QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to the Quick View page");
			Log.assertThat(!searchresultPage.QVStateNone(), "QuickViewScreen properly loaded !",
					"QuickViewScreen not loaded properly !");
			// quickViewPage.selectSizeByIndex(1);
			Log.message("4. Selected random size value .");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");

			Log.message("UPC should  be displayed for variation masters product.");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(element, quickViewPage),
					"<b>Actual Result:</b>UPC is there in variation Master Product !",
					"<b>Actual Result:</b>UPC is not there in variation Master Product !", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_173

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify pricing details in Quick view screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_147(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
				Log.message("2. Searched with the  '" + searchKey
						+ "' in search text box and navigated to 'Search Result'  Page!");

				QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
				Log.message("3. Clicked on Quick View button");

				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b> Standard price, original price, now price should be displayed on quick view page");
				Log.message("<b>Actual Result:</b><br>");
				Log.assertThat(quickViewPage.verifyStdPriceDetails(), " Standard Price has $ symbol",
						" Standard Price does not have $ symbol");

				Log.assertThat(quickViewPage.verifyOrgPriceDetails(), " Original Price has $ symbol",
						" Original Price does not have $ symbol");

				Log.assertThat(quickViewPage.verifyNowPriceDetails(), " Now Price has $ symbol",
						" Now Price does not have $ symbol", driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}// TC_BELK_QUICKVIEW_130

	// TC_BELK_QUICKVIEW_133
	@Test(groups = { "desktop",
			"tablet" }, description = "Verify Color selection in Quick view screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_151(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {

			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
				Log.message("2. Searched with the  '" + searchKey
						+ "' in search text box and navigated to 'Search Result'  Page!");

				QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
				Log.message("3. Clicked on Quick View button");
				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b> Color of the swatch selected and the product color should be same");

				Log.assertThat(quickViewPage.getSelectedSwatchColors(),
						"<b>Actual Result:</b> Color of the swatch selected and the primary image are same",
						"<b>Actual Result:</b> Color of the swatch selected and the primary image are not same",
						driver);

				/*
				 * if (searchresultPage.getColorSwatchCount() == quickViewPage
				 * .getColorSwatchesCount()) { Log.assertThat(
				 * (quickViewPage.getSelectedSwatchColors()
				 * .equals(quickViewPage .getFaceoutImageColors())),
				 * "<b>Actual Result:</b> Color of the swatch selected and the primary image are same"
				 * ,
				 * "<b>Actual Result:</b> Color of the swatch selected and the primary image are not same)"
				 * , driver);
				 * 
				 * }
				 */

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	}

	@Test(groups = {
			"desktop" }, description = "Verify Size selection in Quick view screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_153(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is not applicable for mobile");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
			String searchKey = testData.get("SearchKey");

			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			List<String> sizeDropDown = Arrays.asList("drpSize");

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
				Log.message("2. Navigated to " + searchKey + " search result Page!", driver);
				QuickViewPage qvPage = searchResultPage.navigateToQuickViewWithProductId();

				Log.message("<b>Expected Result:</b><br> Size dropdown should be displayed in Quick View Page");

				Log.assertThat(qvPage.elementLayer.verifyPageElements(sizeDropDown, qvPage),
						"<b>Actual Result:</b><br> Size dropdown is diplayed in quick view page",
						"<b>Actual Result:</b><br> Size dropdown is not displayed in quick view page");

				qvPage.selectSizeByIndex(1);
				Log.message("3. Size selected from Size Dropdown Box ");

				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}

	}

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify selecting maximum number of qty from 'QTY' drop down box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_157(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String size = testData.get("size");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Product List Page with product Id : " + searchKey);

			// Load Quick view Page
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickView Page!");

			// Selecting size
			quickViewPage.selectSizeByIndex(3);
			Log.message("4. Size selected from Size Dropdown Box: " + size);

			// Selecting maximum Quantity
			quickViewPage.selectQuantity("24");
			Log.message("5. Selected Maximum Quantity(24)!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>Error should not be reported when selecting maximum number of quantity from Quick view screen.");
			Log.assertThat(quickViewPage.getLimitedAvailabilityMessage().isEmpty(),
					"<b>Actual Result : </b>Error not reported when selecting maximum number of quantity from Quick view screen!",
					"<b>Actual Result : </b>Error is reported when selecting maximum number of quantity from Quick view screen!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_138

	@Test(groups = {
			"desktop" }, description = " Verify 'Add to Shopping Bag' button behavior on Quick view", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_163(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to QuickViewPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("4. Selected Color: '" + selectedColor + "' from the color swatches in the QuickView Page");

			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("5. Selected size: '" + selectedSize + "' from the size drop down  in the QuickView Page");

			if (Utils.waitForDisabledElement(driver, quickViewPage.btnAddtoBag, 2)) {
				String reSelectedColor = quickViewPage.selectColor();
				Log.message(
						"5. Selected Color: '" + reSelectedColor + "' from the color swatches in the QuickView Page");
			}

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After configure of valid product, the 'AddToBag' button should be enabled in 'QuickView' Page");

			Log.assertThat(!(Utils.waitForDisabledElement(driver, quickViewPage.btnAddtoBag, 2)),
					"<b>Actual Result:</b> After configure of valid product, the 'AddToBag' button is enabled in 'QuickView' Page",
					"<b>Actual Result:</b> After configure of valid product, the 'AddToBag' button is not enabled in 'QuickView' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_144

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Registry' link behavior on Quick view as a guest user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_164(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to QuickViewPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("4. Selected Color:'" + selectedColor + "' from the color swatches in the QuickView Page");

			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("5. Selected size:'" + selectedSize + "' from the size drop down  in the QuickView Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'registry' link on QuickView page, It should Navigated to 'Registry Guest User' page");

			RegistryGuestUserPage registryGuest = quickViewPage.navigateToRegistryAsGuest();
			Log.assertThat(registryGuest != null,
					"<b>Actual Result:</b> After Clicking on 'registry' link on QuickView page, It is Navigated to 'Registry Guest User' page "
							+ driver.getCurrentUrl(),
					"<b>Expected Result:</b> After Clicking on 'registry' link on QuickView page, It is not Navigated to'Registry Guest User' page ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_145

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Registry' link behavior on Quick view as a signed-in user without a registry.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_165(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickViewPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("4. Selected Color:'" + selectedColor + "' from the color swatches in the QuickView Page");

			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("5. Selected size:'" + selectedSize + "' from the size drop down  in the QuickView Page");

			if (!quickViewPage.verifyAddToRegistryLinkEnabled()) {
				selectedColor = quickViewPage.selectColor();
			}
			RegistryGuestUserPage registryGuest = quickViewPage.navigateToRegistryAsGuest();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToRegistry' in QuickView page as a signed user, It should redirect to 'registry' Page ");

			Log.assertThat(registryGuest != null,
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' in QuickView page as a signed user, It is redirect to 'registry' Page  "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' in QuickView page as a signed user, It is not redirect to 'registry' Page  ",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_146

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Registry' link behavior on Quick view as a signed-in user with a registry.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_166(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lblAddedRegistry = "Item Added to Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickView();
			Log.message("5. Navigated  to QuickViewPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("6. Selected Color:'" + selectedColor + "' from the color swatches in the QuickView Page");

			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("7. Selected size:'" + selectedSize + "' from the size drop down  in the QuickView Page");

			quickViewPage.clickOnRegistrySign();
			Log.message("8. Clicked on 'AddToRegistry' button in the Quickview page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToRegistry' as a signed user in QuickView page, It should shown success message in the 'QuickView' page. ");

			Log.message(quickViewPage.getTextFromAddedRegistry());
			Log.assertThat(quickViewPage.getTextFromAddedRegistry().contains(lblAddedRegistry),
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button  in QuickView page as a signed user, it is shown  a success message :"
							+ quickViewPage.getTextFromAddedRegistry() + " in the 'QuickView' page.",
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button  in QuickView page as a signed user, it is not shown  a success message :"
							+ quickViewPage.getTextFromAddedRegistry() + " in the 'QuickView' page.",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_147

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Wish list' link behavior on Quick view as guest user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_167(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
			Log.message("3. Navigated  to QuickPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("4. Selected Color: '" + selectedColor + "' from the color swatches in the QuickView Page");

			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("5. Selected size: '" + selectedSize + "' from the size drop down  in the QuickView Page");

			quickViewPage.clickAddToWishList();
			Log.message("6. 'Add to wish list' button is clicked! in quickview page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToWishList' in QuickView page as a Guest user, It should navigate to the WishList Page. ");

			WishListPage wishListPage = new WishListPage(driver).get();
			Log.assertThat(wishListPage != null,
					"<b>Actual Result:</b> By Clicking on 'AddToWishlist' Button in Quickview as a 'Guest user', It is navigated to WishList page",
					"<b>Actual Result:</b> By Clicking on 'AddToWishlist' Button in Quickview as a 'Guest user', It is not  navigated to WishList page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_148

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Wish list' link behavior on Quick view as signed-in user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_168(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lblSuccessMessage = "Item added to Wish List";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
			Log.message("5. Navigated to QuickViewPage with randomly selected product");

			String selectedColor = quickViewPage.selectColor();
			Log.message("6. Selected Color: '" + selectedColor + "' from the color swatches in the QuickView Page");
			Utils.waitForPageLoad(driver);
			String selectedSize = quickViewPage.selectSize();
			Log.message("7. Selected size: '" + selectedSize + "' from the size drop down  in the QuickView Page");

			quickViewPage.clickAddToWishList();
			Log.message("8. Clicked on 'Add to wish list' button in the QuickView Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> By Clicking on 'Add to wishlist' button in the Quick view page as a 'Signed user', Item  Added to wishlist' message should shown under 'AddtoWishList' button");

			Log.assertThat(quickViewPage.getTextFromAddedWishList().trim().equals(lblSuccessMessage.trim()),
					"<b>Actual Result:</b> By Clicking on 'Add to wishlist' button in the Quick view page as a 'Signed user', It is shown "
							+ quickViewPage.getTextFromAddedWishList() + "message under 'AddtoWishList' button",
					"<b>Actual Result:</b> By Clicking on 'Add to wishlist' button in the Quick view page as a 'Signed user', It is not shown "
							+ quickViewPage.getTextFromAddedWishList() + "message under 'AddtoWishList' button",
					driver);

			quickViewPage.closeQuickViewPage();
			searchResultPage.headers.navigateToWishList();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_149

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the alternative image behavior on 'Quick view' for a Product set", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_174(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] searchKey = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey[0]);
			Log.message("2. Searched with keyword '" + searchKey[0] + "' and Navigated to search result Page");
			// Navigated to the Quick View window
			QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to the Quick View page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>User should see that Thumbnails shown below the product image on Quickview module.");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("productThumbnails"), quickViewPage),
					"<b>Actual Result : </b>Thumbnails shown below the product image on Quickview module!",
					"<b>Actual Result : </b>Thumbnails NOT shown below the product image on Quickview module.", driver);

			String SelectedThumbnailSrc = quickViewPage.selectRandomThumbnail();
			String PrimaryImageSrc = quickViewPage.getPrimaryImageSrc();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>Main product image should be replaced with the selected alternate image on clicking the thumbnail.");

			Log.assertThat(PrimaryImageSrc.equals(SelectedThumbnailSrc),
					"<b>Actual Result : </b>Main product image is replaced with the selected alternate image on clicking the thumbnail!",
					"<b>Actual Result : </b>Main product image not replaced with the selected alternate image on clicking the thumbnail.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_154

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify UPC for variation master product in a Product set on 'Quick view' module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_185(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		List<String> upcLabel = Arrays.asList("lblUPC");

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Search for the product
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Navigate to QuickView Page
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();// .navigateToQuickViewByIndex(0);
			Log.message("3. Navigated to QuickView Page !");

			// Selecting size in Quick view page
			quickViewPage.selectSizeByIndex(1);
			Log.message("4. Selected the size on QuickView page !");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should see that the UPC is not displayed for a Variation Master product of a Productset!");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElementsDoNotExist(upcLabel, quickViewPage),
					"<b>Actual Result:</b> UPC not displayed for a Variation Master product of a Productset",
					"<b>Actual Result:</b> UPC displayed for a Variation Master product of a Productset", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_163

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify UPC for variation product in a Product set on 'Quick view' module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_186(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);
		List<String> upcLabel = Arrays.asList("lblUPC");

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to product listing page by clicking on L1->L2->L3 categories!");

			PlpPage plpPage = new PlpPage(driver).get();

			// Navigate to QuickView Page
			QuickViewPage quickViewPage = plpPage.navigateToQuickViewByIndex(1);
			Log.message("3. Navigated to QuickView Page !");

			// Selecting size in Quick view page
			quickViewPage.selectSizeByIndex(1);
			Log.message("4. Selected the size on QuickView page !");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should see that the UPC is displayed for a variation product of a Productset!");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(upcLabel, quickViewPage),
					"<b>Actual Result:</b> UPC displayed for a variation product of a Productset",
					"<b>Actual Result:</b> UPC details not displayed for a variation product of a Productset", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_164

	// TC_BELK_QUICKVIEW_118
	@Test(groups = {
			"desktop" }, description = "Verify product main image in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_134(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to " + category[0] + " >> " + category[1] + " >> " + category[2] + " page");
			String imageUrlInPLP = plppage.getImageName(1);
			QuickViewPage quickViewPage = plppage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on Quick View button");
			Log.message("4. Quick view popup is displayed");
			String imageUrlInQuickView = quickViewPage.getImageName(1);
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> Selected product main image should be properly displayed.");
			Log.assertThat(imageUrlInPLP.equals(imageUrlInQuickView),
					"<b>Actual Result-1:</b> The selected product main image is displayed",
					"<b>Actual Result-1:</b> The selected product main image is not displayed");
			String urlBeforClicking = driver.getCurrentUrl();
			quickViewPage.clickPrimaryImage();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Page should not be redirected to any page when clicking on product main image");
			Log.assertThat(urlBeforClicking.equals(driver.getCurrentUrl()),
					"<b>Actual Result:</b> Page is not redirected to any page when clicking on product main image",
					"<b>Actual Result:</b> Page is redirected to any page when clicking on product main image", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_118

	// TC_BELK_QUICKVIEW_120
	@Test(groups = {
			"desktop" }, description = "Verify alternative image is properly displayed in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_137(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on Quick View button");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The product should be having more than 3 thumbnail images");
			int count = quickViewPage.getCountOfThumbnailImages();
			Log.assertThat(count >= 4, "<b>Actual Result-1:</b> This product have more than 3 thumbnail images!",
					"<b>Actual Result-1:</b> This product is not have more than 3 thumbnail images");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b> The previous and next image arrow should displayed and while clicking on it, the image should be changed.");
			Log.message("<b>Actual Result-2:</b><br>");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPreviousImage"),
							quickViewPage),
					" The Previous image Button is disabled by default",
					" The Previous Button should not be enabled by default", driver);

			quickViewPage.clickImageNavArrowButton("Next");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("btnPreviousImage"), quickViewPage),
					" The Previous image button is enabled after clicking the Next image button",
					" The Previous image button should not be disabled after clicking the Next image button");
			int i = 1;
			while (i < (count - 3)) {
				quickViewPage.clickImageNavArrowButton("Next");
				i++;
			}
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImage"),
							quickViewPage),
					" The Next image Button is disabled when last image is displayed",
					" The Next Button should not be enabled when last image is displayed");
			quickViewPage.clickImageNavArrowButton("Previous");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(Arrays.asList("btnNextImage"), quickViewPage),
					" The Next image button is enabled after clicking the Previous image button",
					" The Next image button should not be disabled after clicking the Previous image button", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_120

	@Test(groups = {
			"desktop" }, description = "Verify Brand name in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_141(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on Quick View button");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The brand name should displayed in the quick view popup");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lnkBrandName"), quickViewPage),
					"<b>Actual Result-1:</b> The brand name is displayed in the quick view popup",
					"<b>Actual Result-1:</b> The brand name is not displayed in the quick view popup", driver);
			Log.message("<br>");
			String urlBeforeClick = driver.getCurrentUrl();
			quickViewPage.clickBrandLink();
			Log.message("3. Clicked on Brandname link");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The brand name should be hyperlink");
			String urlAfterClick = driver.getCurrentUrl();
			Log.assertThat(!(urlAfterClick.equals(urlBeforeClick)),
					"<b>Actual Result-2:</b> The brand name is a hyperlink and the page is redirected to search result page",
					"<b>Actual Result-2:</b> The brand name is not hyperlink and the page is not redirected to search result page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_124

	// TC_BELK_QUICKVIEW_143
	@Test(groups = {
			"desktop" }, description = "Verify Add To Shopping Bag button in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_162(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Quick view popup is displayed");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Add to Shopping Bag button should be disabled before selecting the size.");
			String disableStatus = quickViewPage.getTextFromAttribute();
			Log.assertThat(disableStatus.equals("true"),
					"<b>Actual Result:</b> The Add to Shopping Bag button is disabled before selecing the size.",
					"<b>Actual Result:</b> The Add to Shopping Bag button is enabled before selecing the size.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_COUPON_143

	// TC_BELK_QUICKVIEW_162
	@Test(groups = {
			"desktop" }, description = "Verify the product name of a Product set on 'Quick view' module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_184(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with search key: '" + searchKey + "' and navigated to search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on 'Quick View' button of first product");
			Log.message("4. Navigated to Quick View Page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product name should displayed in the quick view popup");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lblProductSetProductName"),
							quickViewPage),
					"<b>Actual Result:</b> The product name is displayed in the quick view popup",
					"<b>Actual Result:</b> The product name is not displayed in the quick view popup", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_162

	// TC_BELK_COUPON_188
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify breadcrumbs on Coupon Details page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_210(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String expectedMobileBreadcrumb = "Back to Home";
		List<String> expectedDesktopBreadcrumb = new ArrayList<>();
		expectedDesktopBreadcrumb.add("Home");
		expectedDesktopBreadcrumb.add("Coupons");
		List<String> breadcrumbText = new ArrayList<>();
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponsPage = homePage.headers.navigateToCoupons();
			Log.message("2. Click the coupon link and the page is redirected to coupons page");

			Log.message("<br>");

			if (runPltfrm == "mobile") {
				Log.message("<b>Expected Result:</b> User should see the below breadcrumbs: '< Back to Home'");
				Log.assertThat(couponsPage.getTextInBreadcrumb().get(0).equals(expectedMobileBreadcrumb),
						"<b>Actual Result:</b> '" + expectedMobileBreadcrumb + "' is displayed properly",
						"<b>Actual Result:</b> '" + expectedMobileBreadcrumb + "' is not displayed properly", driver);
			} else if (runPltfrm == "desktop") {
				Log.message("<b>Expected Result:</b> User should the see below breadcrumbs: 'Home > Coupons'");
				breadcrumbText = couponsPage.getTextInBreadcrumb();
				Log.assertThat(breadcrumbText.equals(expectedDesktopBreadcrumb),
						"<b>Actual Result:</b> '" + expectedDesktopBreadcrumb + "' breadcrumb is displayed properly",
						"<b>Actual Result:</b> '" + expectedDesktopBreadcrumb
								+ "' breadcrumb is not displayed properly",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_COUPON_188

	// TC_BELK_COUPON_192
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the Coupon Code in Coupon Details Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_214(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponsPage = homePage.headers.navigateToCoupons();
			Log.message("2. Clicked the coupon link and is redirected to coupons page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should see the Coupon code with text 'Use Code:' <coupon code> on Coupon details section");
			boolean status = false;
			ArrayList<String> lstCoupons = couponsPage.getCouponCode();
			for (int i = 0; i < lstCoupons.size(); i++) {
				if (lstCoupons.get(i).contains("Use Code:")) {
					status = true;
				} else {
					status = false;
					break;
				}
			}
			Log.assertThat(status,
					"<b>Actual Result:</b> The Coupon code with text 'Use Code:' <coupon code> on Coupon details section is displayed",
					"<b>Actual Result:</b> The Coupon code with text 'Use Code:' <coupon code> on Coupon details section is not displayed",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_192

	@Test(groups = {
			"desktop" }, description = "Verify the Brand Name on 'Quick view' for a Product set", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_180(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Navigated to " + searchKey + " search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Quick view popup is displayed");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b> The brand name should displayed in the quick view popup and it should not be clickable");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lnkBrandNameProductSet"),
							quickViewPage),
					"<b>Actual Result-1:</b> The brand name is displayed in the quick view popup",
					"<b>Actual Result-1:</b> The brand name is not displayed in the quick view popup", driver);
			String currentUrlBeforeClick = driver.getCurrentUrl();
			quickViewPage.clickBrandLinkForProductSet();
			Log.message("<br>");
			Log.message("4. Clicked the brand name in quick view page");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The brand name should not be hyperlink");
			String currentUrlAfterClick = driver.getCurrentUrl();
			Log.assertThat((currentUrlBeforeClick.equals(currentUrlAfterClick)),
					"<b>Actual Result-2:</b> The brand name is not a hyperlink and the page is  not redirected to any other page when clicked on it",
					"<b>Actual Result-2:</b> The brand name is a hyperlink.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_159

	// TC_BELK_COUPON_193
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Add Coupon to Bag button in Coupon details page for Valid Coupon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_215(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponsPage = homePage.navigateToCouponsPage();
			Log.message("2. Clicked the coupon link and is redirected to coupons page");
			Log.assertThat(couponsPage.elementLayer.verifyPageElements(Arrays.asList("btnAddCouponToBag"), couponsPage),
					"3. The 'Add Coupon to Bag' button is displayed!",
					"The 'Add Coupon to Bag' button is not displayed!", driver);
			couponsPage.clickAddCouponToBag();
			Log.message("4. The 'Add coupon to Bag' button is clicked!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The coupon added success message should displayed.");
			Log.assertThat(
					couponsPage.elementLayer.verifyPageElements(Arrays.asList("txtCouponAddedMessage"), couponsPage),
					"<b>Actual Result:</b> The Coupon added success message '" + couponsPage.getSuccessMessage()
							+ "' is displayed!",
					"<b>Actual Result:</b> The coupon added success message is not displayed!", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_COUPON_193

	// TC_BELK_COUPON_195
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'View In-store' coupon button on Coupon Details Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_217(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponsPage = homePage.headers.navigateToCoupons();
			Log.message("2. Clicked the coupon link and is redirected to coupons page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'View In-Store' button should be displayed in the coupon page.");
			Log.assertThat(
					couponsPage.elementLayer.verifyPageListElements(Arrays.asList("lstBtnViewInStore"), couponsPage),
					"<b>Actual Result:</b> The 'View In-Store' button is displayed in the coupon page",
					"<b>Actual Result:</b> The 'View In-Store' button is not displayed in the coupon page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_COUPON_195

	// TC_BELK_QUICKVIEW_126
	@Test(groups = {
			"desktop" }, description = "Verify Product name in Quick View screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_143(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to Product landing page of: " + category[0] + "-->" + category[1] + "-->"
					+ category[2]);

			plppage.selectCatgeoryRefinement();
			Log.message("3. Pdp page is loaded");
			Log.message("<br>");
			QuickViewPage quickViewPage = plppage.navigateToQuickViewWithProductId();
			Log.message("4. Clicked on QuickView button");
			Log.message("<b>Expected Result1:</b> Product name should be displayed under Brand name.");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("fldProductName"), quickViewPage),
					"<b>Actual Result1:</b>Brand name is displayed above the product name",
					"<b>Actual Result1:</b>Brand name is not displayed above the product name");
			Log.message("<br>");
			String urlofQuickView = driver.getCurrentUrl();
			quickViewPage.clickProductName();
			Log.message("<b>Expected Result2:</b>  Page should not be redirected while clicking on the  product name.");
			String urlofQuickViewAfterClickProdcut = driver.getCurrentUrl();
			Log.assertThat(urlofQuickView.equals(urlofQuickViewAfterClickProdcut),
					"<b>Actual Result2:</b> Page is not be redirected while clicking on the product name",
					"<b>Actual Result2:</b> Page is redirected while clicking on the product name", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_126

	// TC_BELK_QUICKVIEW_137
	@Test(groups = {
			"desktop" }, description = "Verify Quantity dropdown box in Quick view screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_156(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to Product landing page of: " + category[0] + "-->" + category[1] + "-->"
					+ category[2]);
			plppage.selectCatgeoryRefinement();
			Log.message("3. Pdp page is loaded");
			QuickViewPage quickViewPage = plppage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on Quick View button");
			String selectedValue = quickViewPage.selectQunatityDropDown();
			Log.message("5. Quantity drop down is selected");

			String valueAfterSelect = quickViewPage.getSelectedQunatityValue();
			Log.message("5. Selected quantity: '" + valueAfterSelect + "' from the quantity drop down");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>User should allowed to select any one quantity available in quantity drop down box.");
			Log.assertThat(valueAfterSelect.equals(selectedValue),
					"<b>Actual Result:</b> User is allowed to select any one quantity available in quantity drop down box. ",
					"<b>Actual Result:</b> User is not allowed to select any one quantity available in quantity drop down box.",
					driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_137

	// TC_BELK_COUPON_189
	@Test(groups = {
			"desktop" }, description = "Verify the Left Navigation on Coupons Details page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_211(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponspage = homePage.navigateToCouponsPage();
			Log.message("2. Navigated to Coupons Page!");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should see the left navigation panel displayed with details like <br> <b>   1.</b> Belk TV Ads <br> <b>   2.</b> Text to Join options");
			Log.assertThat(
					couponspage.elementLayer.verifyPageElements(Arrays.asList("lblLeftMiddleNavpane"), couponspage),
					"<b>Actual Result1:</b> Belk TV Ads option is displayed properly",
					"<b>Actual Result1:</b> Belk TV Ads option is not displayed properly");
			Log.assertThat(
					couponspage.elementLayer.verifyPageElements(Arrays.asList("lblLeftbottomNavPane"), couponspage),
					"<b>Actual Result2:</b> Text to Join option is displayed properly",
					"<b>Actual Result2:</b> Text to Join option is not displayed properly", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_COUPON_189

	// TC_BELK_COUPON_187
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'Coupons' link in Home Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_209(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			CouponsPage couponsPage = homePage.navigateToCouponsPage();
			Log.message("2. The page is redirected to coupons page");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>User should be navigated to the Coupons Details Page.");
			Log.assertThat(couponsPage.elementLayer.verifyPageElements(Arrays.asList("txtCouponHeading"), couponsPage),
					"<b>Actual Result: </b>The coupons page is loaded properly",
					"<b>Actual Result: </b>The coupons page is not loaded", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_COUPON_187

	@Test(groups = {
			"desktop" }, description = "Verify clicking on the Brand name hyper link in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_142(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String txtRefinedBy = "Refined by";
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			CategoryLandingPage clppage = new CategoryLandingPage(driver);
			Log.message("2. Navigated to Product landing page of: " + category[0] + "-->" + category[1] + "-->"
					+ category[2]);

			QuickViewPage quickViewPage = clppage.navigateToQuickViewWithProductId();
			Log.message("4. Quick view page is loded properly");
			String brandName = quickViewPage.getTextFromBrand();
			quickViewPage.clickBrandname();
			SearchResultPage searchresult = new SearchResultPage(driver).get();
			ArrayList<String> productname = searchresult.getBrandNameInProductlist();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>If no brand landing page exists while clicking on brand name hyperlink, the customer is redirected to a Search Results page with the matching Brand search refinement preselected");
			Log.assertThat(searchresult.getTextRefinedByfromBreadcrumb().equals(txtRefinedBy),
					"<b>Actual Result:</b> Search result page is properly loaded.Refined By text is present properly in breadcrumb",
					"<b>Actual Result:</b> Search result page is properly loaded.Refined By text is not present properly in breadcrumb.");
			boolean status = false;
			for (int i = 0; i < productname.size(); i++) {
				if (productname.get(i).contains(brandName))
					status = true;
				else {
					status = false;
					break;
				}
			}
			Log.assertThat(status,
					"<b>Actual Result:</b> All product listed in the search result page contain the brand name: "
							+ brandName + "",
					"<b>Actual Result:</b> All products listed in the brand page do not contain the brand name: "
							+ brandName + "");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_142

	@Test(groups = {
			"desktop" }, description = "Verify social media options on 'Quick view' module for a Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_195(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] socialLinks = testData.get("SocailMediaLinks").split(",");
		List<String> socialMedia = Arrays.asList("lnkPintrest", "lnkFacebook", "lnkTwitter", "lnkGooglePlus",
				"lnkEmail");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewpage = searchResultPage.navigateToQuickView();
			Log.message("3.Navigated to quickView Page with randomly selected product");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' should shown in the 'QuickView' page",
					driver);

			Log.assertThat(quickViewpage.elementLayer.verifyPageElements(socialMedia, quickViewpage),
					"<b>Actual Result1:</b> Social Media icons 'Pinterest, Facebook, Twitter, GooglePlus, Email' are displayed in the 'QuickView' page",
					"<b>Actual Result1:</b> Social Media icons are not displayed in the 'QuickView' Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> By Clicking on 'Pinterest' Icon, It should navigate to corresponding 'Pinterest' page");

			quickViewpage.navigateToPinterestPage();
			Log.softAssertThat(socialLinks[2].replaceAll(" ", "").contains("pinterest"),
					"<b>Actual Result2:</b>  By Clicking on Pinterest Icon, It is navigated to 'Pintrest' page ",
					"<b>Actual Result2:</b>  By Clicking on Pinterest Icon, It is not navigated to 'Pintrest' page ",
					driver);
			BrowserActions.navigateToBack(driver);

			// Log.message("<br>");
			// Log.message(
			// "<b>Expected Result2:</b> By Clicking on 'Pinterest' Icon, It
			// should navigate to corresponding 'Pinterest' page");
			//
			// Log.message("<br>");
			// Log.message(
			// "<b>Expected Result3:</b> By Clicking on 'FaceBook' Icon, It
			// should navigate to corresponding 'FaceBook' page");
			//
			// quickViewpage.navigateToFacebookPage();
			// Log.softAssertThat(socialLinks[0].replaceAll(" ",
			// "").contains("facebook"),
			// "<b>Actual Result3:</b> By Clicking on FaceBook Icon, It is
			// navigated to 'FaceBook' page",
			// "<b>Actual Result3:</b> By Clicking on FaceBook Icon, It is not
			// navigated to 'FaceBook' page",
			// driver);
			// BrowserActions.navigateToBack(driver);
			//
			// Log.message("<br>");
			// Log.message(
			// "<b>Expected Result4:</b> By Clicking on 'Twitter' Icon, It
			// should navigate to corresponding 'Twitter' page");
			//
			// quickViewpage.navigateToTwitterPage();
			// Log.softAssertThat(socialLinks[1].replaceAll(" ",
			// "").contains("twitter"),
			// "<b>Actual Result4:</b> By Clicking on Twitter Icon, It is
			// navigated to 'Twitter' page ",
			// "<b>Actual Result4:</b> By Clicking on Twitter Icon, It is not
			// navigated to 'Twitter' page",
			// driver);
			//
			// Log.message("<br>");
			// Log.message(
			// "<b>Expected Result5:</b> By Clicking on 'GooglePlus' Icon, It
			// should navigate to corresponding 'GooglePlus' page");
			//
			// quickViewpage.navigateToGooglePlusPage();
			// Log.softAssertThat(socialLinks[5].replaceAll(" ",
			// "").contains("google"),
			// "<b>Actual Result5:</b> By Clicking on GooglePlus Icon, It is
			// navigated to 'GooglePlus' page",
			// "<b>Actual Result5:</b> By Clicking on GooglePlus Icon, It is not
			// navigated to 'GooglePlus' page",
			// driver);
			// BrowserActions.navigateToBack(driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_195

	@Test(groups = {
			"desktop" }, description = "Verify the product name on 'Quick view' for Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_197(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickView Page with randomly selected product");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product name " + quickViewPage.getBrandName()
					+ "should displayed in the quick view page");

			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lblProductName"), quickViewPage),
					"<b>Actual Result:</b> The brand name '" + quickViewPage.getBrandName()
							+ "' is displayed in the quick view page",
					"<b>Actual Result:</b> The brand name is not displayed in the quick view page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_197

	@Test(groups = {
			"desktop" }, description = "Verify the product description on 'Quick view' module for Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_199(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewByIndex(1);
			Log.message("3. Navigated to QuickView page with randomly selected product");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The product  description should be  displayed above 'check gift card balance' link in the quick view page");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lblProductDescription"),
							quickViewPage),
					"<b>Actual Result:</b> The product  description is  displayed above 'check gift card balance' link in the quick view page",
					"<b>Actual Result:</b> The product  description is  not displayed above 'check gift card balance' link in the quick view page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_199

	@Test(groups = {
			"desktop" }, description = "Verify child product details on 'Quick view' module for Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_200(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickView page with randomly selected product");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>The product on gift card quick view page should display the brand , product Name , Value Variation , Quantity  , Low Availability message");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lnkBrandName", "lblProductName",
							"ValueDropDown", "SelectQty", "msgLimitedAvalilability"), quickViewPage),
					"<b>Actual Result:</b> The product on gift card quick view page  displays the brand , product Name , Value Variation , Quantity  , Low Availability message",
					"<b>Actual Result:</b> The product on gift card quick view page is not displaying  the brand , product Name , Value Variation , Quantity  , Low Availability message",
					driver);
			quickViewPage.selectSize();

			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(Arrays.asList("UPC"), quickViewPage),
					"<b>Actual Result:</b> The product on gift card quick view page  displays the UPC  in the quick view page",
					"<b>Actual Result:</b> The product on gift card quick view page the UPC is not displayed  in the quick view page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_178

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Registry' link behavior on Quick view as a guest user for a Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_203(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to QuickViewPage with randomly selected product");

			String selectedValue = quickViewPage.selectSize();
			Log.message("4. Selected value:'" + selectedValue + "' from the value drop down  in the QuickView Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'registry' link in the  QuickView page as 'Guest user', It should Navigated to 'Sign in' page");

			RegistryGuestUserPage registryGuest = quickViewPage.navigateToRegistryAsGuest();
			Log.assertThat(registryGuest != null,
					"<b>Actual Result:</b> After Clicking on 'registry' link in the  QuickView page as 'Guest user', It is Navigated to 'Sign in' page"
							+ driver.getCurrentUrl(),
					"<b>Expected Result:</b>After Clicking on 'registry' link in the  QuickView page as 'Guest user', It is not Navigated to 'Sign in' page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_181

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Registry' link behavior on Quick view as a signed-in user for a Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_204(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lblAddedRegistry = "Item Added to Registry";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to search result Page");

			QuickViewPage quickViewPage = searchresultPage.navigateToQuickViewWithProductId();
			Log.message("5. Navigated to QuickViewPage with randomly selected product");

			String selectedValue = quickViewPage.selectSize();
			Log.message("6. Selected value:'" + selectedValue + "' from the value drop down  in the QuickView Page");

			quickViewPage.clickOnRegistrySign();
			Log.message("8. Clicked on 'AddToRegistry' button in the Quickview page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToRegistry' as a signed user in QuickView page, It should shown success message in the 'QuickView' page. ");
			Log.assertThat(quickViewPage.getTextFromAddedRegistryForGiftCard().contains(lblAddedRegistry),
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button  in QuickView page as a signed user, it is shown  a success message :"
							+ quickViewPage.getTextFromAddedRegistryForGiftCard() + " in the 'QuickView' page.",
					"<b>Actual Result:</b> After Clicking on 'AddToRegistry' button  in QuickView page as a signed user, it is not shown  a success message :"
							+ quickViewPage.getTextFromAddedRegistryForGiftCard() + " in the 'QuickView' page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_204

	@Test(groups = {
			"desktop" }, description = "Verify 'Add to Wish list' link behavior on Quick view as signed-in user for Gift card.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_206(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lblAddedWishList = "Item added to Wish List";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page");

			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewpage = searchresultPage.navigateToQuickViewWithProductId();
			Log.message("5. Navigated to QuickPage with randomly selected product");

			String selectedValue = quickViewpage.selectSize();
			Log.message("6. Selecting value:'" + selectedValue + "'  from  value drop down in the QuickView Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Added to wishlist message should be displayed.");

			quickViewpage.clickAddToWishList();
			Log.message("7.clicked on 'Add to wish list' button in the QuickView Page");

			Log.assertThat(quickViewpage.getTextFromAddedWishList().trim().contains(lblAddedWishList.trim()),
					"<b>Actual Result:</b> After Clicking on 'AddToWishList'  as a signed user is shown  a success message :"
							+ quickViewpage.getTextFromAddedWishList() + " in QuickView page,",
					"<b>Actual Result:</b> After Clicking on 'AddToWishList'  as a signed user is not shown  a success message :"
							+ quickViewpage.getTextFromAddedWishList() + " in QuickView page,",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_206

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the Close functionality of the Quickview module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_151(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		List<String> quickviewwindowclosebutton = Arrays.asList("btnCloseQV");
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigated to the category page
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to the category Page");
			SearchResultPage searchresultPage = new SearchResultPage(driver).get();

			// Navigated to the QuickView page
			QuickViewPage quickview = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to the Quick View page");
			Log.assertThat(quickview.elementLayer.verifyPageElements(quickviewwindowclosebutton, quickview),
					"4. QV window close[X] button is visible", "4. QV window close[X] button is not visible");
			// Quick view window get closed by clicking[X] close icon
			quickview.closeQuickViewPage();
			Log.message(
					"<br> Expectedresult: </br> User should see that the Quickview module closes on clicking 'X' close button.");
			Log.message(
					"<br> Actualresult: </br> Quick view window get close on the clicking of [X] close icon on the quick view page");
			// Quick view window get closed by ESC Key
			searchresultPage.navigateToQuickView();
			quickview.closeQuickViewPageByEsc();
			Log.message(
					"<br> Expectedresult: </br> User should see that the Quickview module closes by pressing ESC Key.");
			Log.message(
					"<br> Actualresult: </br>  Quick view window get close by pressing ESC Key the quick view page get closed",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_151

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the product image displayed on 'Quickview' for a Productset.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_152(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);
		List<String> quickviewmainimage = Arrays.asList("quickViewContent", "imageMainQuickviewWindow");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigated to the category page
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to the category Page");
			SearchResultPage searchresultPage = new SearchResultPage(driver).get();
			// Navigated to the Quick View window
			QuickViewPage quickview = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to the Quick View page");
			Utils.waitForPageLoad(driver);
			Log.message(
					"<br> Expected Result:</br> User should see that the Main Product image is displayed on the Quickview module.");
			Log.assertThat(quickview.elementLayer.verifyPageElements(quickviewmainimage, quickview),
					"<br> Actual Result:</br> Main Product image is displayed on the Quickview module",
					"<br> Actual Result:</br> Main Product image is not displayed on the Quickview module", driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_152

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify Badge is displayed on 'Quick view' for a Product set", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_153(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] searchKey = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);
		List<String> badgetextdisplay = Arrays.asList("txtProductBadgeQuickViewInProductSet");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigated to the category page
			/*
			 * homePage.headers.navigateTo(category[0], category[1],
			 * category[2]); Log.message("2. Navigated to the category Page");
			 */
			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey[0]);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to search result Page");
			// Navigated to the Quick View window
			int index = searchresultPage.getIndexByProductName(searchKey[1]);
			QuickViewPage quickview = searchresultPage.navigateToQuickViewByIndex(index);
			Log.message("3. Navigated to the Quick View page");

			Log.message(
					"<br> Expected Result:</br> User should see that the Badge is displayed below the product image on the Quick view module.");
			Log.assertThat(quickview.elementLayer.verifyPageElements(badgetextdisplay, quickview),
					"<br> Actual Result:</br> Badge text is displayed below the product image on the Quick view module.",
					"<br> Actual Result:</br> Badge text is not displayed below the product image on the Quick view module",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_153

	@Test(groups = {
			"desktop" }, description = "Verify the alternative image behavior on 'Quick view' for a Product set", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_154(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] searchKey = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultPage = homePage.searchProductKeyword(searchKey[0]);
			Log.message("2. Searched with keyword '" + searchKey + "' and Navigated to search result Page");
			// Navigated to the Quick View window
			int index = searchresultPage.getIndexByProductName(searchKey[1]);
			QuickViewPage quickview = searchresultPage.navigateToQuickViewByIndex(index);
			Log.message("3. Navigated to the Quick View page");
			Log.message("<br> Expected Result: </br> Thumbnails shown below the product image on Quick view module.");
			Log.assertThat(quickview.elementLayer.verifyPageElements(Arrays.asList("imgThumbnailImageQV"), quickview),
					"<b>Actual Result:</b> Thumbnail images are present on the Quick view module",
					"<b>Actual Result:</b> Thumbnail images are not present on the Quick view module");
			Log.message(
					"<br> Expected Result: </br> Main product image is replaced with the selected alternate image on clicking the thumbnail on QV page.");
			quickview.clickOnThumbnailImage();
			Log.message(
					"<br> Actual Result: </br>  Main product image is replaced with the selected alternate image on clicking the thumbnail on QV page.",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_154

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the scroll arrow on 'Quickview' for a Productset with more than 4 alternate views.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_155(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String[] searchKey = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> quickviewscrollimagenxtarrow = Arrays.asList("btnNextImage");
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey[0]);
			Log.message("2. Navigated to Search Result Page");

			// Navigate to Quick view Page
			int index = searchResultPage.getIndexByProductName(searchKey[1]);
			QuickViewPage quickview = searchResultPage.navigateToQuickViewByIndex(index);

			// QuickView Page check for thumbnail scroll arrows
			Log.assertThat(quickview.elementLayer.verifyPageElements(quickviewscrollimagenxtarrow, quickview),
					"3. In QuickView Page scroll arrows '>' and '<'  is displayed as it has  more than 4 alternate images ",
					"3. In QuickView Page scroll arrows '>' and '<'  is not displayed as it has  less than 4 alternate images");

			// Click on the > < in thumbnail image in the Quick view page
			quickview.clickImageNavArrowButton("Next");
			Log.message(
					"<br> Expected Result</br>  User should see display scroll arrows '>' and '<' when a Productset has more than 4 alternate images or videos");
			Log.message("<br> Actual Result </br>  Click on the Next arrow button in the thumbnail image to scroll");
			quickview.clickImageNavArrowButton("Previous");
			Log.message(
					"<br> Expected Result</br>  User should see display scroll arrows '>' and '<' when a Productset has more than 4 alternate images or videos");
			Log.message("<br> Actual Result </br> Click on the Previous arrow button in the thumbnail image to scroll",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_155

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify the behavior on clicking Brand Name for a Brand (without a relevant Category page) hyperlink on 'Quickview' module for a Productset.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_PDP_160(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (!(runPltfrm == "desktop"))
			throw new SkipException("This testcase is applicable only in Desktop");
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigated to the category page
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated to the category Page");
			SearchResultPage searchresultPage = new SearchResultPage(driver).get();
			QuickViewPage quickview = searchresultPage.navigateToQuickView();
			Log.message("3. Navigated to the Quick View page");
			String BrandName = quickview.getBrandName();
			Log.message("4. The brand name is " + BrandName);
			quickview.clickBrandLink();
			Log.message(
					"<br> Expected Result: </br> User should be redirected to search result page refined by the brand name with the matching Brand search refinement preselected.");
			Log.message(
					"<br> Actual Result: </br> Navigate to the Search Result Page by clicking the brand link on the Quick View page",
					driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_160

	@Test(groups = {
			"desktop" }, description = "Verify the Check Gift Card Balance link on 'Quick view' module for Gift card", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_198(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		List<String> element = Arrays.asList("giftBalance");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to quick view Page with '" + searchKey + "' Gift Card");

			Log.message(
					"<b>Expected Result:</b> Check Balance Link should shown for the Gift card product in the Quick view page");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(element, quickViewPage),
					"<b>Actual Result:</b> Check Balance Link is displayed for the Gift card product in the Quick view page",
					"<b>Actual Result:</b>Check Balance Link is not displayed for the Gift card product in the Quick view page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_176

	@Test(groups = {
			"desktop" }, description = "Verify the behavior on clicking Brand Name for a Brand (without a relevant Category page) hyperlink on 'Quick view' module for a Product set.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_183(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String txtRefinedBy = "Refined by";
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to search result page");
			QuickViewPage quickViewPage = searchresultpage.navigateToQuickView();
			Log.message("3. Quick view button is clicked ");
			String brandName = quickViewPage.getTextFromBrand();
			quickViewPage.clickBrandname();
			Log.message("4. Brand name is clicked");
			SearchResultPage searchresult = new SearchResultPage(driver).get();
			Log.message("4. Search result page is properly loaded after clicking brand name in quickview page");
			ArrayList<String> productname = searchresult.getBrandNameInProductlist();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>User should be redirected to search result page refined by the brand name with the matching Brand search refinement preselected.");
			Log.assertThat(searchresult.getTextRefinedByfromBreadcrumb().equals(txtRefinedBy),
					"<b>Actual Result:</b> Search result page is properly loaded for the selected brand",
					"<b>Actual Result:</b> Search result page is not loaded for the selected brand");
			boolean status = false;
			for (int i = 0; i < productname.size(); i++) {
				if (productname.get(i).contains(brandName))
					status = true;
				else {
					status = false;
					break;
				}
			}
			Log.assertThat(status,
					"<b>Actual Result:</b> All product listed in the search result page contain the brand name: "
							+ brandName + "",
					"<b>Actual Result:</b> All products listed in the brand page do not contain the brand name: "
							+ brandName + "");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_161

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'Show Exclusions' link in Coupon details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_213(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			CouponsPage couponsPage = homePage.navigateToCouponsPage();
			Log.message("2. The page is redirected to coupons page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>  'Show Exclusions' link is displayed in Coupons Details section.");
			Log.assertThat(
					couponsPage.elementLayer.verifyPageListElements(Arrays.asList("lstCouponTitle"), couponsPage),
					"<b>Actual Result: </b>The coupons page is loaded properly",
					"<b>Actual Result: </b>The coupons page is not loaded", driver);
			Log.assertThat(
					couponsPage.elementLayer
							.verifyPageListElementsDoNotExist(Arrays.asList("lstCouponsexclusionsExpand"), couponsPage),
					"<b>Actual Result: Show Exclusion field for all coupons is collapsed properly",
					"<b>Actual Result: Show Exclusion field for all coupons is not collapsed properly", driver);

			couponsPage.clickOnShowExclusionsLink();
			Log.message("<br>");
			Log.message("3. Clicked on 'Show Exclusions' link");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Exclusions list is expanded on tapping the link.");
			Log.message("<b>Actual Result:</b> Show Exclusion link is clicked for all coupons");
			Log.assertThat(
					couponsPage.elementLayer.verifyPageListElements(Arrays.asList("lstCouponsexclusionsExpand"),
							couponsPage),
					"<b>Actual Result:</b> Show Exclusion fld for all coupons is expanded properly on tapping the link",
					"<b>Actual Result:</b> Show Exclusion fld for all coupons is not expanded properly on tapping the link",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_COUPON_191

	@Test(groups = { "desktop" }, description = "Verify 'Add to Wish list' link behavior on Quick view as guest user for Gift card.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_205(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("This testcase is only applicable for desktop");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to " + searchKey + " from search result Page!");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewByIndex(3);
			Log.message("3. Navigated to quick view Page with the ProductId!");

			 quickViewPage.selectValueRandomInProductSet(1);
			Log.message("4. Selecting the child product in the ProductSet!");

			quickViewPage.clickAddToWishList();
			Log.message("5.After Selecting the Value,Click on the AddToWishList Button!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToWishList'  as a Guest user in QuickView page,should navigate to the WishList Page. ");

			WishListPage wishListPage = new WishListPage(driver).get();
			Log.assertThat(wishListPage != null,
					"<b>Actual Result:</b> By Clicking on 'AddToWishlist' Button is navigated to WishList page ",
					"<b>Actual Result:</b> By Clicking on 'AddToWishlist' Button link is Not navigated to WishList page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_205

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify limited availability message in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_158(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String size = testData.get("size");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page for Search Key : " + searchKey);

			// Load Quick view Page
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickView Page!");

			// verify quick view page loaded properly
			Log.message("</br>");
			Log.message("<b>Expected Result : </b>Quick View Page should be loaded properly");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("quickViewContent", "lnkBrandName", "btnCloseQV"), quickViewPage),
					"<b>Actual Result : </b>QuickView Page <b>is</b> loaded Properly!",
					"<b>Actual Result : </b>Quickview Page <b>not</b> loaded Properly!", driver);

			Log.message("</br>");
			// Selecting size
			quickViewPage.selectSizeByIndex(1);
			Log.message("4. Size selected from Size Dropdown Box: " + size);

			// Selecting maximum Quantity
			String qty = quickViewPage.selectQuantity();
			Log.message("5. Selected Maximum Quantity("+ qty +") from the quantity drop down!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>Error should not be reported when selecting maximum number of quantity from Quick view screen.");
			Log.assertThat(!(quickViewPage.getLimitedAvailabilityMessage().isEmpty()),
					"<b>Actual Result : </b>Error not reported when selecting maximum number of quantity from Quick view screen!",
					"<b>Actual Result : </b>Error is reported when selecting maximum number of quantity from Quick view screen!",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_139

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify 'Add to Shopping Bag' button when no variation(s) are selected on 'Quick view'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_179(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase (QuickView) is only applicable for desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Product Level1 Category : " + searchKey);

			// Load PDP Page for Search Keyword
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to PDP from Search results page !");

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Verify size is not selected in product sets.");

			Log.assertThat(quickViewPage.verifyDefaultSizeStatusInProductSet(),
					"<b>Actual Result : </b>Size is not selected!", "<b>Actual Result : </b>Size is selected.");

			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b>Verify 'Add to Shopping Bag' button is disabled when size is not selected.");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnAddtoBag"), quickViewPage),
					"<b>Actual Result : </b>'Add to Shopping Bag' button is disabled when size is not selected.",
					"<b>Actual Result : </b>'Add to Shopping Bag' button not disabled when size is not selected.");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_179

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify 'Add to Shopping Bag' button after variation(s) are selected on 'Quick view'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_202(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase (QuickView) is only applicable for desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page for Search key : " + searchKey);

			// Load PDP Page for Search Keyword
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to QuickView Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Verify size is not selected in product sets.");

			Log.assertThat(quickViewPage.verifyDefaultSizeStatusInProductSet(),
					"<b>Actual Result : </b>Size is not selected!", "<b>Actual Result : </b>Size is selected.");

			// verify 'Add to Shopping Bag' button disabled when size is not
			// selected
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b>Verify 'Add to Shopping Bag' button is disabled when size is not selected.");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageListElementsDisabled(Arrays.asList("btnAddtoBag"),
							quickViewPage),
					"<b>Actual Result : </b>'Add to Shopping Bag' button is disabled when size is not selected.",
					"<b>Actual Result : </b>'Add to Shopping Bag' button not disabled when size is not selected.");

			Log.message("<br>");
			// select random size in product sets
			quickViewPage.selectSizeRandomInProductSet();
			Log.message("4. Random size selected for all the products in Product Set!");

			// verify 'Add to Shopping Bag' button enabled
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b>Verify 'Add to Shopping Bag' button is enabled after size is selected.");
			Log.assertThat(
					quickViewPage.elementLayer.verifyPageListElements(Arrays.asList("btnAddtoBag"), quickViewPage),
					"<b>Actual Result : </b>'Add to Shopping Bag' button is enabled after size selected.",
					"<b>Actual Result : </b>'Add to Shopping Bag' button not enabled after size selected.");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_202

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify coupon details in the Coupon details section.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_COUPON_212(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			CouponsPage couponspage = homePage.navigateToCouponsPage();
			Log.message("2. Navigated to Coupons Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Coupon Name, End Date should be displayed in coupon details page");
			couponspage.getCouponPanelCount();

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_COUPON_190

	@Test(groups = {
			"desktop" }, description = "Verify stand alone product in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_QUICKVIEW_133(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			CategoryLandingPage clppage = new CategoryLandingPage(driver);
			Log.message("2.Plp page is properly loded");
			QuickViewPage quickViewPage = clppage.navigateToQuickViewWithProductId();
			Log.message("4.Qucik view page is loaded");
			String selectedValue = quickViewPage.selectQunatityDropDown();
			Log.message("4. Quantity drop down is selected");
			Log.message(
					"<b>Expected Result:</b>User should allowed to select any one quantity available in quantity drop down box.");
			String valueAfterSelect = quickViewPage.getSelectedQunatityValue();
			Log.assertThat(valueAfterSelect.equals(selectedValue),
					"<b>Actual Result:</b> User is allowed to select any one quantity available in quantity drop down box. ",
					"<b>Actual Result:</b> User is not allowed to select any one quantity available in quantity drop down box.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_QUICKVIEW_133
	
	@Test(groups = { "desktop" }, description = "Verify Shipping Threshold message for ineligible products in Quick view module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_188(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.headers.navigateTo(searchKey[0], searchKey[1], searchKey[2]);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Plp Page");

			QuickViewPage quickViewPage = plpPage.navigateToQuickView();
			Log.message("3. Navigated to quickView Page with randomly selected product");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see that the Shipping Threshold message is not displayed.");
			Log.assertThat(!quickViewPage.elementLayer.verifyPageElements(
					Arrays.asList("shippingThresholdMessage"), quickViewPage), 
					"As the product is inelegible for free shipping, the Shipping Threshold message is not displayed", 
					"The product is inelegible for free shipping but the Shipping Threshold message is displayed", 
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_QUICKVIEW_188
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify Shipping Threshold message for eligible products in Quick view module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_189(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			homePage.headers.navigateTo(searchKey[0], searchKey[1], searchKey[2]);
			PlpPage plpPage = new PlpPage(driver).get();
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Plp Page");

			QuickViewPage quickViewPage = plpPage.navigateToQuickView();
			Log.message("3. Navigated to quickView Page with randomly selected product");
			String message = "Free Shipping";
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see that the Shipping Threshold message is displayed.");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(
					Arrays.asList("shippingThresholdMessage"), quickViewPage) &&
					quickViewPage.getThresholdMessage().contains(message), 
					"<b> Actual Result : </b> As the product is elegible for free shipping, the Shipping Threshold message is displayed as "
											+ quickViewPage.getThresholdMessage(), 
					"<b> Actual Result : </b> The product is elegible for free shipping but the Shipping Threshold message is not displayed", 
					driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_189
	
	


}
