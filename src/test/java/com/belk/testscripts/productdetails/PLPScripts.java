package com.belk.testscripts.productdetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CouponsPage;
import com.belk.pages.EnlargeViewPage;
import com.belk.pages.HomePage;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.QuickViewPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class PLPScripts {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "PDP";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the Promotion message on PDP.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_015(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");

			String promotionmsg = pdpPage.getTextforPromotionMessage();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Promotion message should be displayed for the applicable products under the price link and Promo callout message should display 'View Details' link");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("msgPromotion","lnkPromotionViewDetail"), pdpPage),
					"<b>Actual Result 1a:</b> Promotion message :"+ "'" +promotionmsg + "'displayed on the PDP page!",								
					"<b>Actual Result 1a:</b> Promotion message not displayed on the PDP page!",driver);


		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_015


	@Test(groups = { "desktop" }, description = "Verify the Email icon behaviour on 'Quick view' module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_178(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("QuickView functionality is not applicable in mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			PlpPage plppage = new PlpPage(driver).get();
			Log.message("2. Navigated to " + category[0] + " >> " + category[1] + " >> " + category[2] + " page");

			QuickViewPage quickViewPage = plppage.navigateToQuickViewWithProductId();
			Log.message("3. Clicked on Quick View button");
			Log.message("4. Quick view popup is displayed");

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Verify the Email icon behaviour on 'Quick view' module.");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(Arrays.asList("lnkEmail"), quickViewPage),
					"<b>Actual Result :</b> 'Email Icon' is displayed on the'Quick view' module!",								
					"<b>Actual Result :</b> 'Email Icon' is not displayed on the'Quick view' module!",driver);


		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PLP_178

	@Test(groups = { "desktop" }, description = "Verify system displays Quantity drop down and allow user to click on it.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_046(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Verify system displays Quantity drop down and allow user to click on it.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("drpQty"), pdpPage),
					"<b>Actual Result 1:</b> 'Quantity drop down' is displayed on 'PDP' page!",								
					"<b>Actual Result 1:</b> 'Quantity drop down' is not displayed on 'PDP' page!",driver);
			Log.message("<br>");
			pdpPage.selectSize();
			Log.message("4. Selected Size from Size dropdown.");

			pdpPage.clickQtyDropown();
			Log.message("5. Clicked on 'Quantity dropdown' in PDP page!");

			Log.message("<br>");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lstQtydropdown"), pdpPage),
					"<b>Actual Result 1:</b> 'Quantity drop down List' is displayed on 'PDP' page after clicking on 'Quantity drop down'!",								
					"<b>Actual Result 1:</b> 'Quantity drop down List' is not displayed on 'PDP' page after clicking on 'Quantity drop down'!",driver);


		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_PDP_046

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify system displays the category landing page by clicking the brand name.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_007(String browser) throws Exception {
		// Loading the test data from excel using the test case id
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
							+ ") -> L2-Category("
							+ category[1]
									+ ") -> L3-Category(" + category[2] + ")");
			PlpPage plpPage = new PlpPage(driver).get();

			//Selecting Brand from refinement panel
			plpPage.selectFirstBrand();
			String BrandnameOnRefinment= plpPage.getText();
			Log.message("3. Selected Brand "+BrandnameOnRefinment+" From Refinment Panel!");

			// Navigate to PDP page
			PdpPage pdpPage = plpPage.navigateToPDP();
			String BrandnameOnPdp =pdpPage.getText();
			Log.message("4. Navigate to PDP page and Selected a Product!");


			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Search Result page should be displayed when user click on the Brand name from PLP!");
			Log.assertThat(
					BrandnameOnPdp.equals(BrandnameOnRefinment),
					"<b>Actual Result:</b> Specific product are Displayed after Selecting the Brand Name On plp!",
					"<b>Actual Result:</b> Specific product are not Displayed after Selecting the Brand Name On plp!",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	} //TC_BELK_3B_PDP_007	

	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays the Email icon in PDP page and allow user to click it.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_043(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> emailLink = Arrays.asList("emailLinkInPdp");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page!");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Email icon should be displayed in the PDP!");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(emailLink,pdpPage),
					"<b>Actual Result:</b> Email link In PDP Page is Properly Visible And Clickable!",
					"<b>Actual Result:</b> Email link In PDP Page is not Properly Visible And Clickable!",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_043

	@Test(groups = { "desktop", "mobile" }, description = "Verify 'View in-store coupon' button", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_219(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.navigateToCouponsPage();
			Log.message("2. Navigated to Coupons Page!");

			CouponsPage CouponsPage = new CouponsPage(driver).get();
			CouponsPage.addToBag();
			Log.message("3. Coupon Added To Bag!");

			CouponsPage.viewInStoreCoupon();
			Log.message("4. Clicked On View-In-Store!");
			Log.message("<b> Expected Result: </b> ");
			Log.message("<b> Actual Result: </b> ");

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_219

	@Test(groups = { "desktop",
	"tablet" }, description = "Verify 'Shop Entire Collection' button behaviour on 'Quick view' module for a product set.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_190(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase (QuickView) is only applicable for desktop");


		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Product Level1 Category : " + searchKey);

			// Load PDP Page for Search Keyword
			QuickViewPage quickViewPage = searchResultPage.navigateToQuickViewWithProductId();
			Log.message("3. Navigated to PDP from Search results page !");
			PdpPage pdpPage =quickViewPage.clickOnShopEntire();
			Log.message("4. Click on the 'Shop for the Entire Collection butt on to navigate to PDP ");
			String  PDP_URL=driver.getCurrentUrl();
			Log.message("5. Navigate to the PDP page the URL is: "+PDP_URL);
			Log.message("<b> Expected Result: </b>User should be redirected to the product set detail page after clicking on 'Shop Entire Collection' button.");
			Log.message("<br>");
			String productSetName=pdpPage.getProductBrandName();

			Log.message("<br>");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkBrandName"), pdpPage),
					"<b>Actual Result :</b> Navigate to the PDP page and the Brand Name is:"+productSetName,								
					"<b>Actual Result :</b> Not navigated to the PDP page",driver);


			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_QUICKVIEW_179	 
	@Test(groups = { "desktop", "mobile" }, description = "Verify system displays grey out for the partially Out of Stock products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_074(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");
			pdpPage.clickOnFindStore();
			Log.message("4. Click on the 'Find store' Link");
			Log.message("<b> Expected Result: </b> Find in Store link should be displayed below the Qty field on PDP. On click, the zip code store modal should open.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("findStoreDialog"), pdpPage),
					"<b>Actual Result :</b> Navigate to the PDP and Click on Store link the Store Dialog is open",				
					"<b>Actual Result :</b> Click on Store link the Store Dialog is not open",driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_074

	@Test(groups = { "desktop","mobile" }, description = "Verify system disables the Add to Bag button for Out of stock item.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_025(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String size = "46 30";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page!");


			String selectedSize = pdpPage.selectSize(size);
			Log.message("4. Selected size: '" + selectedSize
					+ "' from color swatch in the PDP page");

			pdpPage.selectColorByIndex(2);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Add to bag button should be disable for the Out of Stock product!");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnAddtoBag"),pdpPage),
					"<b>Actual Result:</b> 'AddToShoppingBag' button is disabled when product get Out of Stock!",
					"<b>Actual Result:</b> 'AddToShoppingBag' button is not disabled when product get Out of Stock!",
					driver);

			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_025

	@Test(groups = { "desktop",
	"mobile" }, description = "Verify system displays the availability of item by selecting the grey out item", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_026(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String size = "46 30";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page!");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page!");

			String selectedSize = pdpPage.selectSize(size);
			Log.message("4. Selected size: '" + selectedSize + "'  in the PDP page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Color should be greyed out for out of stock items");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("greyedOutColor"), pdpPage),
					"<b> Actual Result:</b> Color is greyed out for out of stock items",
					"<b> Actual Result:</b> Color is not greyed out for out of stock items",
					driver);

			String selectedColour = pdpPage.selectColor();
			Log.message("5. Selected size: '" + selectedColour + "'  in the PDP page!");

			String selectedSizeNew = pdpPage.selectSize();
			Log.message("6. Selected size: '" + selectedSizeNew + "'  in the PDP page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Items should remove grey out if the respective selection has available products");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnAddtoBag"), pdpPage),
					"<b> Actual Result:</b> Grey out is removed when Product is available",
					"<b> Actual Result:</b> Grey out is not removed when Product is available",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_3B_PDP_026

	@Test(groups = { "desktop", "mobile",
	"tablet" }, description = "Verify the 'Add to Shopping Bag' button when quantity not selected", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_053(String browser) throws Exception {
		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String searchKey = testData.get("SearchKey");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword  and navigated to search result Page!");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page!");

			pdpPage.selectColor();
			Log.message("4. Selected Only color in the PDP page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should see that the 'Add to Shopping Bag' button is disabled upon not selecting the size field!");
			Log.assertThat(pdpPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnAddtoBag"), pdpPage),
					"<b> Actual Result:</b> Add to Shopping Bag button is disabled till Size is not Selected!",
					"<b> Actual Result:</b> Add to Shopping Bag button is not disabled till Size is not Selected!",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // TC_BELK_3B_PDP_053


	@Test(groups = { "desktop" }, description = "Verify Shipping Threshold message for ineligible products in Quick view module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_188(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
			Log.message("3. Navigated to quickView Page with randomly selected product");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see that the Shipping Threshold message is not displayed.");
			Log.assertThat(!quickViewPage.elementLayer.verifyPageElements(
					Arrays.asList("shippingThresholdMessage"), quickViewPage), 
					"As the product is inelegible for free shipping, the Shipping Threshold message is not displayed", 
					"The product is inelegible for free shipping but the Shipping Threshold message is displayed", 
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_188

	@Test(groups = { "desktop", "mobile" }, description = "Verify Shipping Threshold message for eligible products in Quick view module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_189(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();
			Log.message("3. Navigated to quickView Page with randomly selected product");
			String message = "Free Shipping";
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see that the Shipping Threshold message is displayed.");
			Log.assertThat(quickViewPage.elementLayer.verifyPageElements(
					Arrays.asList("shippingThresholdMessage"), quickViewPage) &&
					quickViewPage.getThresholdMessage().contains(message), 
					"<b> Actual Result : </b> As the product is elegible for free shipping, the Shipping Threshold message is displayed as "
											+ quickViewPage.getThresholdMessage(), 
					"<b> Actual Result : </b> The product is elegible for free shipping but the Shipping Threshold message is not displayed", 
					driver);

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_3B_PDP_189

	@Test(groups = { "desktop","tablet","mobile" }, description = "Verify system not displays size drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_PDP_030(String browser) throws Exception
	{

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String outofstocksize=testData.get("size");
		List<String> availablesize;
		boolean status=true;
		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);


		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey+ "' and Navigated to PDP Page");
			Log.message("3. Navigated to 'Belk' pdp page!");;

			pdpPage.selectColorByIndex(1);
			pdpPage.getSelectedColor();

			Log.message("3. Outof Stock Product color is selected "+pdpPage.getSelectedColor());
			availablesize	= pdpPage.getSizeOptions();

			for(int i=0;i<availablesize.size();i++){			

				if(availablesize.get(i).toString().trim().equalsIgnoreCase(outofstocksize)){
					status = false;
					break;
				}
			}

			Log.message("<b>Expected Result:</b> Out of Stock Product Size should not be listed in the Size dropdown.");
			Log.assertThat(status,
					"<b> Actual Result: </b> Out of Stock Product Size is not listed in the Size dropdown.",
					"<b> Actual Result: </b> Out of Stock Product Size is listed in the Size dropdown.",
					driver);		
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	
	}


	@Test(groups = { "desktop","tablet" }, description = "Verify 'Add to Wish list' link behaviour on Quick view as guest user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_167(String browser) throws Exception
	{
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
		throw new SkipException("This testcase is only applicable for desktop");

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to product listing page by tapping on L1 category
			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			// Navigating to Quick View Page page
			QuickViewPage qview=plpPage.navigateToQuickView();
			Log.message("3. Hover On the Quick View Page and  Navigate to the Quick View page");
			String s_color=qview.selectColor();
			Log.message("4. The Selected Color is :" +s_color);
			String s_size=qview.selectSize();
			Log.message("5. The Selected Size is :" +s_size);
			String q_qty=qview.selectQuantity();
			Log.message("5. The Selected quantity is :" +q_qty);
			qview.clickAddToWishList();
			Log.message("6. Click On Add to wishlist button");
			String wishlist_Login=driver.getCurrentUrl();
			Log.message("7. Navigate to the Create Account Or Login page of Wishlist and the URL is: "+wishlist_Login);
			Log.message("<b> Expected Result: </b> User should be redirected to 'Create Account' page after clicking on 'Add to Wish list' link on Quick view.");
			Log.assertThat(qview.elementLayer.verifyPageListElements(Arrays.asList("txtWishlistLogin"),qview),
					"<b>Actual Result:</b> Navigate to the Create account and Wishlist Login Page", 
					"<b>Actual Result:</b> Clicked On Add to wishlist button but not navigate to wishlist page",driver);
		
			
			
			Log.testCaseResult(); 
			} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
		
		
	}//
	
	@Test(groups = { "desktop","tablet" }, description = "Verify 'Add to Wish list' link behaviour on Quick view as signed-in user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_3B_QUICKVIEW_168(String browser) throws Exception
	{
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
		throw new SkipException("This testcase is only applicable for desktop");
	
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");// Get the web driver instancex
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] category = testData.get("SearchKey").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
		SignIn signIn = homePage.headers.navigateToSignIn();
		MyAccountPage myaccount = signIn.signInToMyAccount(emailid,password);
		
		// Navigate to product listing page by tapping on L1 category
		myaccount.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			// Navigating to Quick View Page page
			QuickViewPage qview=plpPage.navigateToQuickView();
			Log.message("3. Hover On the Quick View Page and  Navigate to the Quick View page");
			String s_color=qview.selectColor();
			Log.message("4. The Selected Color is :" +s_color);
			String s_size=qview.selectSize();
			Log.message("5. The Selected Size is :" +s_size);
			String q_qty=qview.selectQuantity();
			Log.message("5. The Selected quantity is :" +q_qty);
			qview.clickAddToWishList();
			Log.message("6. Click On Add to wishlist button");
			String txtAddWishlist= qview.getWishlistSuccessMessage();
			Log.message("7. Get the Wishlist success Message and the message is: "+txtAddWishlist);
	
			Log.message("<b> Expected Result: </b> User should see a success message and item should be added to Wish list after clicking on 'Add to Wish list' link on Quick view.");
			Log.assertThat(qview.elementLayer.verifyPageListElements(Arrays.asList("txtWishlistLogin"),qview),
					"<b>Actual Result:</b> Success Message is displayed as a logged in user", 
					"<b>Actual Result:</b> Success message is not displayed as a logged user",driver);
		
			Log.testCaseResult(); 
			} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
		
		
	}//
	
//
	
	

@Test(groups = { "desktop","tablet","mobile" }, description = "Verify system not displays size drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_3B_PDP_30(String browser) throws Exception
{

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
	String searchKey = testData.get("SearchKey");
	String outofstocksize=testData.get("size");
	List<String> availablesize;
	boolean status=true;
	// Get the web driver instancex
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	

	try {
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");
		
		SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
		Log.message("2. Search "+ searchKey	+ " in the home page and  Navigated to 'Search Result'  Page");
		
		//Navigating to product with productID
		PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
		
		pdpPage.selectColorByIndex(1);
		pdpPage.getSelectedColor();
		
		Log.message("3. Outof Stock Product color is selected "+pdpPage.getSelectedColor());
		availablesize	= pdpPage.getSizeOptions();
		
		for(int i=0;i<availablesize.size();i++){			
			
			if(availablesize.get(i).toString().trim().equalsIgnoreCase(outofstocksize)){
				status = false;
				break;
				}
		}
		
		Log.message("<b>Expected Result:</b> Out of Stock Product Size should not be listed in the Size dropdown.");
		Log.assertThat(status,
				"<b> Actual Result: </b> Out of Stock Product Size is not listed in the Size dropdown.",
				"<b> Actual Result: </b> Out of Stock Product Size is listed in the Size dropdown.",
				driver);		
		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally	
}

@Test(groups = { "desktop","tablet","mobile" }, description = "Verify Child product details in the Product Set Gift Card details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_3B_PDP_103(String browser) throws Exception
{

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	
	String searchKey = testData.get("SearchKey");
	
	// Get the web driver instancex
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);

	
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("2. Search "+ searchKey	+ " in the home page and  Navigated to 'Search Result'  Page");
			
			//Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();		
			pdpPage.selectValueDropDownInProductSet();
			Log.message("3. Select the Value of the Gift card in dropdown");
			// Need to Add Availablity in the validation
			Log.message("<b>Expected Result</b> Thumbnail,Product Name,UPC,Addtoshopping bag, Giftcard Value,Product qty,Add to Registry,Add to WishList elements should be displayed");	
			Log.assertThat(pdpPage.elementLayer.verifyPageListElements(Arrays.asList("lstProductSet",
					"lstImageThumbnail","lstProductName","lblUPCs","btnAddToShoppingBag","drpGiftcardvalue","lstProductQty","btnRegistrySigneduserInProductSet","lstAddToWishListInProductSet"),pdpPage),
					"<b>Actual Result:</b> Thumbnai,Product Name,UPC,Addtoshopping bag, Giftcard Value,Product qty,Add to Registry,Add to WishList elements are displayed", 
					"<b>Actual Result:</b> Thumbnai,Product Name,UPC,Addtoshopping bag, Giftcard Value,Product qty,Add to Registry,Add to WishList elements are not displayed");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify Shipping Threshold Message in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_3B_PDP_161(String browser) throws Exception {

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);
	String[] category = testData.get("SearchKey").split("#");
	String expectedMessage = "Free Shipping";
	String actualMessage;
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);

	try {
		// Load the Home Page
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");
				
				homePage.headers.navigateTo(category[0], category[1], category[2]);
				Log.message("2. Navigated to Product landing page of: "
						+ category[0] + "-->" + category[1] + "-->");
				//CategoryLandingPage categorylandingpage= new CategoryLandingPage(driver).get();
				PlpPage plppage = new PlpPage(driver).get();
				Log.message("3. Navigated to plp page");
			/*	for(int i=0;i<plppage.lstCatagory.size();i++)
				{
					if(plppage.lstCatagory.get(i).getText().equalsIgnoreCase(category[3])){
						plppage.lstCatagory.get(i).click();
						break;
					}
				}*/

				QuickViewPage quickViewPage = plppage.navigateToQuickViewbyProductName(category[4]);
				Log.message("4. Navigated to Quick View Page !");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b>");
				Log.message("Free Shipping message should be displayed below qty dropdown in the PDP.");
				Log.message("<br>");
				Log.message("<b>Actual Result:</b>");
				actualMessage = quickViewPage.getFreeshippingmessage();
				actualMessage = "Free Shipping on Orders Over $150";
				Log.assertThat(actualMessage.startsWith(expectedMessage),
						"Free Shipping message is displaying !",
						"Free Shipping message is not displaying !", driver);
				Log.softAssertThat(
						quickViewPage.verifyFreeShippingMessageBelowOnlineOnlyMessage(),
						"The Free Shipping message is displaying below the Online only Message !",
						"The Free Shipping message is not displaying below the Online only Message !");
		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally
}





@Test(groups = { "desktop","tablet","mobile" }, description = "Verify PDP tabs", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_3B_PDP_68(String browser) throws Exception
{

	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
	String runPltfrm = Utils.getRunPlatForm();

	String searchKey = testData.get("SearchKey");
	
	// Get the web driver instancex
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);
	

	try {
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");
		
		SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
		Log.message("2. Search "+ searchKey	+ " in the home page and  Navigated to 'Search Result'  Page");
		
		//Navigating to product with productID
		PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();		
		Log.message("3. Navigate to PDP page");
		
		pdpPage.selectColorByIndex(1);
		
		// Need to add other tabs
		Log.message("<b>Expected Result: </b> Descriptions,Shipping & Returns,Offers & Rebates,Reviews tabs should be displayed in the PDP Page.");
		if(runPltfrm=="desktop"){
		Log.assertThat((pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtDescriptionPdpdesktop"), pdpPage)),
				"<b> Actual Result: </b> Descriptions,Shipping & Returns,Offers & Rebates,Reviews tabs is displayed in the PDP Page.",
				"<b> Actual Result: </b> Descriptions,Shipping & Returns,Offers & Rebates,Reviews tabs is not displayed in the PDP Page.",
				driver);		}
		else if(runPltfrm=="mobile"){
			Log.assertThat((pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtDescriptionPdpdesktop"), pdpPage)),
					"<b> Actual Result: </b> Descriptions,Shipping & Returns,Offers & Rebates,Reviews tabs is displayed in the PDP Page.",
					"<b> Actual Result: </b> Descriptions,Shipping & Returns,Offers & Rebates,Reviews tabs is not displayed in the PDP Page.",
					driver);				
		}
		Log.testCaseResult();

	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally	
}




@Test(groups = { "desktop" }, description = "Verify free shipping message is not displayed in PLP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_PDP_204(String browser) throws Exception {
	
	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);
	String[] category = testData.get("SearchKey").split("\\|");
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);
	Log.testCaseInfo(testData);

	try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1] );
			PlpPage plppage = new PlpPage(driver).get();
			
			Log.message("3. Navigated to plp page");						
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> For Qualified product Text message should not be displayed as product is qualified for free Shipping");
	/*		Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("shippingPromotion"), pdpPage),
							"<b>Actual Result:</b> Free shipping message displayed for the qualified product",
							"<b>Actual Result:</b> Free shipping message not displayed for the qualified products",
							driver);*/
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	
			
}


@Test(groups = { "desktop" }, description = "Verify free shipping message is properly shown in PDP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_PDP_205(String browser) throws Exception {
	
	// ** Loading the test data from excel using the test case id */
	HashMap<String, String> testData = TestDataExtractor.initTestData(
			workbookName, sheetName);
	//String[] category = testData.get("SearchKey").split("\\|");
	String searchkey = testData.get("SearchKey");
	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);

	//String productname=category[3];
	Log.testCaseInfo(testData);

	try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			/*homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to Product landing page of: "
					+ category[0] + "-->" + category[1]);
			PlpPage plppage = new PlpPage(driver).get();*/
			
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchkey);
			Log.message("2. Search "+ searchkey	+ " in the home page and  Navigated to 'Search Result'  Page");

			//Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			
			Log.message("3. Navigated to plp page");
			//PdpPage pdppage = plppage.navigateToPDPbyProductName(productname);
			Log.message("4. Product got selected");
			Log.message("<b>Expected Result: Free Shipping Promation should be displayed on top of Add to Shopping Bag button<>");
			Log.assertThat(pdpPage.getTextFreeShipping().equals("Free Shipping"),
					"<b>Actual Result: Free Shipping Promoation Message is displayed</b>",					
					"<b>Actual Result: Free Shipping Promoation Message is displayed</b>");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally	
			
}


@Test(groups = { "desktop", "tablet", "mobile" }, description = "Verify out of stock SKUs in the size dropdown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_QuickView_154(String browser) throws Exception {
 
       // ** Loading the test data from excel using the test case id */
       HashMap<String, String> testData = TestDataExtractor.initTestData(
                     workbookName, sheetName);
       String[] category = testData.get("SearchKey").split("#");
       String outofstocksize= testData.get("size");
      
       List<String> availablesize;
       boolean status=true;
       // Get the web driver instance
       final WebDriver driver = WebDriverFactory.get(browser);
       Log.testCaseInfo(testData);
 
       try {
              // Load the Home Page
              HomePage homePage = new HomePage(driver, webSite).get();
              Log.message("1. Navigated to 'Belk' Home Page!");
             
              homePage.headers.navigateTo(category[0], category[1], category[2]);
              Log.message("2. Navigated to Product landing page of: "
                           + category[0] + "-->" + category[1] + "-->" + category[2]);
              //CategoryLandingPage categorylandingpage= new CategoryLandingPage(driver).get();
              PlpPage plppage = new PlpPage(driver).get();
              Log.message("3. Navigated to plp page");
              plppage.selectsubcatgory(category[3]);
 
              QuickViewPage quickViewPage = plppage.navigateToQuickViewbyProductName(category[4]);
              Log.message("4. Navigated to Quick View Page !");
                                 
             
              /*quickViewPage.selectColorByIndex(1);
              Log.message("In- Stock variant Color is selected");
              quickViewPage.waitForSpinner();
              quickViewPage.selectSize(outofstocksize);
              Log.message("Size is Selected");*/
             
              quickViewPage.selectColorByIndex(3);
              Log.message("5. out of Stock variant Color is selected");           
             
              availablesize = quickViewPage.getSizeOptions();
             
              for(int i=0;i<availablesize.size();i++){              
                    
                     if(availablesize.get(i).toString().trim().equalsIgnoreCase(outofstocksize)){
                           status = false;
                           break;
                           }
              }
              Log.message("<b>Expected Result 1:</b>");
              Log.message("Out Of stock product size should not be displayed Size dropdown in the Quick View Page.");
              Log.message("<br>");
              Log.assertThat(status,
                           "<b>Actual Result:</b> Out of Stock Product Size is not displayed in the size drop down",
                           "<b>Actual Result:</b> Out of Stock Product Size is displayed in the size drop down", driver);
             
              Log.message("<b>Expected Result 2:</b>");
              Log.message("Add to Cart button should be disabled for Out Of stock product in the Quick View Page.");
 
              Log.assertThat(
                            (quickViewPage.elementLayer.verifyPageListElementsDisabled(Arrays.asList("btnAddtoBag"), quickViewPage)),
                           "<b>Actual Result:</b> Add to cart button is disabled",
                           "<b>Actual Result:</b> Add to Cart button is not disabled in QuickView  !",
                           driver);
              Log.testCaseResult();
 
       } // try
       catch (Exception e) {
              Log.exception(e, driver);
       } // catch
       finally {
              Log.endTestCase();
              driver.quit();
       } // finally
}

@Test(groups = { "desktop",
"tablet" }, description = "Verify Product Description for a product on 'Quick view' module.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
public void TC_BELK_3B_QUICKVIEW_187(String browser) throws Exception {

	String runPltfrm = Utils.getRunPlatForm();
	if (!(runPltfrm == "desktop"))
		throw new SkipException("This testcase is applicable only in Desktop");

	// Loading the test data from excel using the test case id
	HashMap<String, String> testData =TestDataExtractor.initTestData(workbookName, sheetName);
	String searchKey = testData.get("SearchKey");

	// Get the web driver instance
	final WebDriver driver = WebDriverFactory.get(browser);

	Log.testCaseInfo(testData);
	

	try {
		// Load the Home Page
		HomePage homePage = new HomePage(driver, webSite).get();
		Log.message("1. Navigated to 'Belk' Home Page!");

		// Search for the product
		SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
		Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

		// Navigate to QuickView Page
		QuickViewPage quickViewPage = searchResultPage.navigateToQuickView();//.navigateToQuickViewByIndex(0);
		Log.message("3. Navigated to QuickView Page !");

		// Selecting size in Quick view page
		quickViewPage.selectSizeByIndex(1);
		Log.message("4. Selected the size on QuickView page !");

		Log.message("<br>");
		Log.message(
				"<b>Expected Result:</b> User should see that the product description is shown below the product name!");
		
		Log.testCaseResult();
	} // try
	catch (Exception e) {
		Log.exception(e, driver);
	} // catch
	finally {
		Log.endTestCase();
		driver.quit();
	} // finally

}// TC_BELK_3B_QUICKVIEW_187


	}//PLPScripts