package com.belk.testscripts.shoppingbag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.syntax.jedit.InputHandler.home;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CheckoutPage;
import com.belk.pages.CouponsPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.FindInStorePage;
import com.belk.pages.HomePage;
import com.belk.pages.MiniCartPage;
import com.belk.pages.OrderConfirmationPage;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShipToMultipleAddressPage;
import com.belk.pages.ShippingMethodPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.AddressBookPage;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.CustomerServicePage;
import com.belk.pages.footers.FaqPage;
import com.belk.pages.footers.PrivacyAndPolicyPage;
import com.belk.pages.registry.RegistryGuestUserPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.reusablecomponents.StateUtils;
import com.belk.reusablecomponents.e2eUtils;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;
import com.thoughtworks.selenium.webdriven.commands.Check;

@Listeners(EmailReport.class)
public class Cart {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "Cart";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System.getProperty("webSite")
				: context.getCurrentXmlTest().getParameter("webSite"));
	}// init

	public void cleanup(WebDriver driver, String username, String password) throws Exception {
		HomePage homePage = new HomePage(driver, webSite).get();
		SignIn signInPage = homePage.headers.navigateToSignIn();
		MyAccountPage myAcc = signInPage.signInToMyAccount(username, password);
		ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
		bagPage.removeItemsFromBag();
	}

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Order Summary details in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_230(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page from home page");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			int productsCount = searchresultPage.totalProducts.size();
			Log.message("6.Total no. of products displayed in a page are: " + productsCount);

			int random = Utils.getRandom(1, productsCount);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchresultPage.selectProductByIndex(random);
			Log.message("7. Navigated to PDP from Search results page !");

			// Clicking on 'Add to bag' button in PDP
			pdpPage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button in PDP page");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("9. Navigated to shopping bag page");

			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("10. Navigated to Shipping page as authorized user!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Order Summary section should display 'Edit Link', 'Product details', 'Pricing details', 'Estimated Order Total' in Checkout-Shipping page");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("ShippingInHeaderEnabled"),
							checkoutPage),
					"<b>Actual Result 1:</b> 'Edit Link' section displayed in the order summary of Checkout-Shipping page",
					"<b>Actual Result 1:</b> 'Edit Link' section missing in the order summary of Checkout-Shipping page");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("productDetailsOrderSummary"),
							checkoutPage),
					"<b>Actual Result 2:</b> 'Product details' section displayed in the order summary of Checkout-Shipping page",
					"<b>Actual Result 2:</b> 'Product details' section missing in the order summary of Checkout-Shipping page");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("pricingDetails"), checkoutPage),
					"<b>Actual Result 3:</b> 'Pricing details' section displayed in the order summary of Checkout-Shipping page",
					"<b>Actual Result 3:</b> 'Pricing details' section missing in the order summary of Checkout-Shipping page");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("estimatedOrderTotal"), checkoutPage),
					"<b>Actual Result 4:</b> 'Estimated Order Total' section displayed in the order summary of Checkout-Shipping page",
					"<b>Actual Result 4:</b> 'Estimated Order Total' section missing in the order summary of Checkout-Shipping page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_230

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Edit link in Order Summary section in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_231(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page from home page");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);

			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			int productsCount = searchresultPage.totalProducts.size();
			Log.message("6. Total no. of products displayed in a page are: " + productsCount);

			Random r = new Random();
			int random = r.nextInt(productsCount - 1) + 1;

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchresultPage.selectProductByIndex(random);
			Log.message("7. Navigated to PDP from Search results page !");

			// Clicking on 'Add to bag' button in PDP
			pdpPage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button in PDP page");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("9. Navigated to shopping bag page");

			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to Shipping page as authorized user!");

			// Clicking on Edit link in Order Summary of Shipping page
			checkoutPage.clickOnEditBySectionName("ordersummary");
			Log.message("11. Clicked on 'Edit' link in Order summary of Shipping page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> User should be redirected to My Shopping Bag page on clicking 'Edit' link in Checkout-Shipping page");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("divCart"), shoppingBagPage),
					"<b>Actual Result:</b> User redirected to My Shopping Bag page on clicking 'Edit' link in Checkout-Shipping page",
					"<b>Actual Result:</b> User not redirected to My Shopping Bag page on clicking 'Edit' link in Checkout-Shipping page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_231

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Return to Shopping Bag link in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_232(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page from home page");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			int productsCount = searchresultPage.totalProducts.size();
			Log.message("6. Total no. of products displayed in a page are: " + productsCount);

			Random r = new Random();
			int random = r.nextInt(productsCount - 1) + 1;

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchresultPage.selectProductByIndex(random);
			Log.message("7. Navigated to PDP from Search results page !");

			// Clicking on 'Add to bag' button in PDP
			pdpPage.addProductToBag();
			Log.message("8. Clicked on 'Add to bag' button in PDP page");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("9. Navigated to shopping bag page");

			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to Shipping page as authorized user!");

			// Clicking on 'Return to Shopping Bag' link in Shipping page
			shoppingBagPage = checkoutPage.clickReturnToShoppingBag();
			Log.message("11. Clicked on 'Return to Shopping Bag' link in Shipping page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> User should be redirected to My Shopping Bag page on clicking 'Return to Shopping Bag' link in Checkout-Shipping page");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("divCart"), shoppingBagPage),
					"<b>Actual Result:</b> User redirected to My Shopping Bag page on clicking 'Return to Shopping Bag' link in Checkout-Shipping page",
					"<b>Actual Result:</b> User not redirected to My Shopping Bag page on clicking 'Return to Shopping Bag' link in Checkout-Shipping page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_232

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Continue button in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_234(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page from home page");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			int productsCount = searchresultPage.totalProducts.size();
			Log.message("6. Total no. of products displayed in a page are: " + productsCount);

			Random r = new Random();
			int random = r.nextInt(productsCount - 1) + 1;

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchresultPage.selectProductByIndex(random);
			Log.message("7. Navigated to PDP from Search results page !");

			// Clicking on 'Add to bag' button in PDP
			pdpPage.addProductToBag();
			Log.message("8. Clicked on 'Add to bag' button in PDP page");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("9. Navigated to shopping bag page");

			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to Shipping page as authorized user!");

			// Clicking on 'Return to Shopping Bag' link in Shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address6");
			Log.message("11. Clicked on 'Return to Shopping Bag' link in Shipping page");

			// Clicking on 'Continue Shopping' link in Shipping page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on 'Continue' link in Checkout-Shipping page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> User should be redirected to Checkout-Billing page on clicking 'Continue' link in Checkout-Shipping page");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkoutPage),
					"<b>Actual Result:</b> User redirected to Checkout-Billing page on clicking 'Continue' link in Checkout-Shipping page",
					"<b>Actual Result:</b> User not redirected to Checkout-Billing page on clicking 'Continue' link in Checkout-Shipping page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_234

	// OnHold- Links under Shop with Confidence are not working
	@Test(enabled = false, groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Shop With Confidence options in Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_235(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'SignIn' Page from home page");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("5. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			int productsCount = searchresultPage.totalProducts.size();
			Log.message("Total no. of products displayed in a page are: " + productsCount);

			Random r = new Random();
			int random = r.nextInt(productsCount - 1) + 1;

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchresultPage.selectProductByIndex(random);
			Log.message("6. Navigated to PDP from Search results page !");

			// Clicking on 'Add to bag' button in PDP
			pdpPage.clickAddToBag();
			Log.message("7. Clicked on 'Add to bag' button in PDP page");

			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("8. Navigated to shopping bag page");

			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to Shopping page as authorized user!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Shop With Confidence section should be displayed with the links-'Shipping Rates','Easy Returns','FAQs','User Service','Privacy and Security Guarantee'");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblShopWithConfidence"), checkoutPage),
					"<b>Actual Result:</b> Shop With Confidence section displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> Shop With Confidence section not displayed at the bottom of Checkout-Shipping page");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkShippingRates"), checkoutPage),
					"<b>Actual Result:</b> 'Shipping Rates' link displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> 'Shipping Rates' link not displayed at the bottom of Checkout-Shipping page");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkEasyReturns"), checkoutPage),
					"<b>Actual Result:</b> 'Easy Returns' link displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> 'Easy Returns' link not displayed at the bottom of Checkout-Shipping page");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkSFAQs"), checkoutPage),
					"<b>Actual Result:</b> 'FAQs' link displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> 'FAQs' link not displayed at the bottom of Checkout-Shipping page");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkCustomerService"), checkoutPage),
					"<b>Actual Result:</b> 'User Service' link displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> 'User Service' link not displayed at the bottom of Checkout-Shipping page");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkPrivacyandSecurityGuarantee"),
							checkoutPage),
					"<b>Actual Result:</b> 'Privacy and Security Guarantee' link displayed at the bottom of Checkout-Shipping page",
					"<b>Actual Result:</b> 'Privacy and Security Guarantee' link not displayed at the bottom of Checkout-Shipping page");

			FaqPage faqPage = checkoutPage.clickFAQs();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Clicking on 'FAQs' link in Checkout-Shipping page should redirect the user to FAQs page");
			Log.assertThat(
					faqPage.elementLayer.verifyPageElements(Arrays.asList("lnkPrivacyandSecurityGuarantee"), faqPage),
					"<b>Actual Result:</b> Clicking on 'FAQs' link in Checkout-Shipping page redirected the user to FAQs page",
					"<b>Actual Result:</b> Clicking on 'FAQs' link in Checkout-Shipping page didn't redirect the user to FAQs page");

			CustomerServicePage customerServicePage = checkoutPage.clickCustomerService();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Clicking on 'Customer Service' link in Checkout-Shipping page should redirect the user to Customer Service page");
			Log.assertThat(
					customerServicePage.elementLayer.verifyPageElements(Arrays.asList("lnkPrivacyandSecurityGuarantee"),
							customerServicePage),
					"<b>Actual Result:</b> Clicking on 'Customer Service' link in Checkout-Shipping page redirected the user to Customer Service page",
					"<b>Actual Result:</b> Clicking on 'Customer Service' link in Checkout-Shipping page didn't redirect the user to Customer Service page");

			PrivacyAndPolicyPage privacyAndPolicyPage = checkoutPage.clickPrivacyandSecurityGuarantee();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Clicking on 'Privacy and Policy' link in Checkout-Shipping page should redirect the user to Privacy and Policy page");
			Log.assertThat(
					privacyAndPolicyPage.elementLayer
							.verifyPageElements(Arrays.asList("lnkPrivacyandSecurityGuarantee"), privacyAndPolicyPage),
					"<b>Actual Result:</b> Clicking on 'Privacy and Policy' link in Checkout-Shipping page redirected the user to Privacy and Policy page",
					"<b>Actual Result:</b> Clicking on 'Privacy and Policy' link in Checkout-Shipping page didn't redirect the user to Privacy and Policy page");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_235

	@Test(groups = {
			"desktop" }, description = "Verify adding product to the shopping bag", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_025(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("Hover functionality is not applicable in mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "'page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b> The mini­cart should automatically open when the User adds a product to shopping cart from the PDP");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("addedToMyBagPopUp"), pdpPage),
					"<b>Actual Result-1:</b> The mini­cart is automatically open when the User adds a product to shopping cart from the PDP",
					"<b>Actual Result-1:</b> The mini­cart is notautomatically open when the User adds a product to shopping cart from the PDP",
					driver);
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			String productNameFromPDP = pdpPage.getProductName().replaceAll("\\s+", "");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The minicart should displayed when mouse hover on the minicart.");
			minicartpage.mouseOverMiniCart();
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("addedToMyBagPopUp"), pdpPage),
					"<b>Actual Result-2:</b> The minicart is displayed when mouse hover on the minicart.",
					"<b>Actual Result-2:</b> The minicart is not displayed when mouse hover on the minicart.", driver);
			String productNameFromMinicart = minicartpage.getProductNameByIndex(1).replaceAll("\\s+", "");
			productNameFromMinicart = productNameFromMinicart.replaceAll(" ", "");
			productNameFromPDP = productNameFromPDP.replace(" ", "");
			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> The selected product should be added to the mini-cart.");
			Log.assertThat(productNameFromMinicart.contains(productNameFromPDP),
					"<b>Actual Result-3:</b> The selected product '" + productNameFromMinicart
							+ "' is added to the mini-cart.",
					"<b>Actual Result-3:</b> The selected product '" + productNameFromMinicart
							+ "' is not added to the mini-cart.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_025

	@Test(groups = {
			"desktop" }, description = "Verify system displays the product details in Mini Cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_028(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("Hover functionality is not applicable in mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			String productNameFromPDP = pdpPage.getProductName().replaceAll("\\s+", "");
			minicartpage.mouseOverMiniCart();
			String productNameFromMinicart = minicartpage.getProductNameByIndex(1).replaceAll("\\s+", "");
			productNameFromMinicart = productNameFromMinicart.replaceAll(" ", "");
			productNameFromPDP = productNameFromPDP.replace(" ", "");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b> The most recently added product should be added to the top with the image exposed.");
			Log.assertThat(productNameFromMinicart.contains(productNameFromPDP),
					"<b>Actual Result-1:</b> The most recently added product '" + productNameFromMinicart
							+ "' is added to the top.",
					"<b><b>Actual Result-1:</b> The most recently added product '" + productNameFromMinicart
							+ "' is not added to the top.",
					driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b> The product details (Product Image, Product Name, Variation(s), Quantity and Price) should displayed in the minicart.");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageElements(Arrays.asList("imgMinicartImage"), minicartpage),
					"<b>Actual Result:</b> The product image is displayed in the minicart",
					"<b><b>Actual Result:</b> The product image is not displayed in the minicart.");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageListElements(Arrays.asList("productNameInMinicart"),
							minicartpage),
					"<b>Actual Result:</b> The product name '" + minicartpage.getProductNameByIndex(1)
							+ "' is displayed in the minicart",
					"<b>Actual Result:</b> The product name is not displayed in the minicart");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageListElements(Arrays.asList("colorInMinicart"), minicartpage),
					"<b>Actual Result:</b> The product color '" + minicartpage.getProductColorByIndex(1)
							+ "' is displayed in the minicart",
					"<b>Actual Result:</b> The product color is not displayed in the minicart");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageListElements(Arrays.asList("sizeInMinicart"), minicartpage),
					"<b>Actual Result:</b> The product size '" + minicartpage.getProductSizeByIndex(1)
							+ "' is displayed in the minicart",
					"<b>Actual Result:</b> The product size is not displayed in the minicart");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageListElements(Arrays.asList("quantityInMinicart"), minicartpage),
					"<b>Actual Result:</b> The product quantity '" + minicartpage.getProductQuantityByIndex(1)
							+ "' is displayed in the minicart",
					"<b>Actual Result:</b> The product quantity is not displayed in the minicart");
			Log.assertThat(
					minicartpage.elementLayer.verifyPageListElements(Arrays.asList("pricesInMinicart"), minicartpage),
					"<b>Actual Result:</b> The product price '" + minicartpage.getProductPriceByIndex(1)
							+ "' is displayed in the minicart",
					"<b>Actual Result:</b> The product price is not displayed in the minicart", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_028

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify when the User clicks the view bag icon, they should be navigated to the Cart page with the list of products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_032(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");
			int productCountFromViewBag = Integer
					.parseInt(minicartpage.headers.getQuantityFromBag().replace(" item(s)", ""));
			int productCountFromCartPage = minicartpage.getProductTotalQuantity();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The page should be navigated to the Cart page with the list of products.");
			Log.assertThat(
					(driver.getCurrentUrl().contains("Cart-Show"))
							&& (productCountFromCartPage == productCountFromViewBag),
					"<b>Actual Result:</b> The page is navigated to the Cart page with the list of products",
					"<b><b>Actual Result:</b> The page is not navigated to the Cart page with the list of products",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_032

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify clicking on the view bag icon when there are NO items in the cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_035(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("3. Minicart icon is clicked and navigated to Shopping bag page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The text 'Your Cart is Empty' should be display in the Cart page.");
			Log.assertThat(
					(shoppingbagpage.elementLayer.verifyPageElements(Arrays.asList("txtYourShoppingBagEmpty"),
							shoppingbagpage)),
					"<b>Actual Result:</b> The text 'Your Cart is Empty' is displayed in the Cart page.",
					"<b><b>Actual Result:</b> The text 'Your Cart is Empty' is not displayed in the Cart page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_035

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Breadcrumb in empty cart with no coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_046(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			/*
			 * homePage.headers.navigateTo(category[0], category[1]);
			 * Log.message("2. Navigated to '" + category[0] + " >> " +
			 * category[1] + "' page");
			 */
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("3. Minicart icon is clicked and navigated to Shopping bag page");
			/*
			 * ArrayList<String> breadcrumb = shoppingbagpage
			 * .getTextInBreadcrumb();
			 */
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The breadcrumb 'My Bag' should be displayed.");
			Log.assertThat((shoppingbagpage.getEmptycarttitle().equals("My Bag")),
					"<b>Actual Result-1:</b> The breadcrumb 'My Bag' is displayed.",
					"<b><b>Actual Result-1:</b> The breadcrumb 'My Bag' is not displayed", driver);
			String UrlBeforeClickBreadcrumb = driver.getCurrentUrl();
			shoppingbagpage.txtemptycarttitle.click();
			Log.message("<br>");
			Log.message("4. Clicked on the breadcrumb");
			String urlAfterClickBreadcrumb = driver.getCurrentUrl();
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The breadcrumb should not be hyperlink.");
			Log.assertThat(UrlBeforeClickBreadcrumb.equals(urlAfterClickBreadcrumb),
					"<b>Actual Result-2:</b> The breadcrumb is not a hyperlink",
					"<b>Actual Result-2:</b> The breadcrumb is a hyperlink and page is redirected to another page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_046

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify when the User clicks the view bag icon, they should be navigated to the Cart page with the list of products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_074(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to PLP Page
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to 'PLP' Page of '" + category[0] + " >> " + category[1]);
			PlpPage plppage = new PlpPage(driver).get();

			// Navigate to PDP Page
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Navigated to PDP page with randomly selected product.");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");

			// Navigating to 'ShoppingBag' Page
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("8. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			int productCountFromViewBag = Integer
					.parseInt(minicartpage.headers.getQuantityFromBag().replace(" item(s)", ""));
			int productCountFromCartPage = minicartpage.getProductTotalQuantity();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> The page should be navigated to the 'MiniCart' page with the list of products.");
			Log.assertThat(
					(driver.getCurrentUrl().contains("Cart-Show"))
							&& (productCountFromCartPage == productCountFromViewBag),
					"<b>Actual Result1:</b> The page is navigated to the 'MiniCart' page with the list of products",
					"<b>Actual Result1:</b> The page is not navigated to the 'MiniCart' page with the list of products",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result2:</b> The coupon pane should be displayed below the line item details");
			Log.assertThat(shoppingbagpage.verifyCouponPaneBelowProductList(),
					"<b>Actual Result2:</b> The coupon pane is displayed below the line items details",
					"<b>Actual Result2:</b> The coupon pane is not displayed below the line items details", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_074

	@Test(groups = {
			"desktop" }, description = "Verify Order Subtotal in the mini-cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_036(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException("Hover functionality is not applicable in mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "'page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			minicartpage.mouseOverMiniCart();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The subtotal of the line items should be displayed below all the products");
			Log.assertThat(minicartpage.verifySubTotalBelowListOfProducts(),
					"<b>Actual Result:</b> The subtotal of the line items is displayed below all the products",
					"<b>Actual Result:</b> The subtotal of the line items is not displayed below all the products",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_036

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Shipping details in Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_224(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("2. Login with ( " + emailid + " / " + password + " )");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			} else {
				shoppingBagPage = myaccount.headers.NavigateToBagPage();
			}
			shoppingBagPage.headers.navigateTo(category[0], category[1]);
			Log.message("3. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("4. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("5. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("6. Minicart icon is clicked and navigated to Shopping bag page");
			int productCount = minicartpage.getMinicartCount();
			CheckoutPage checkoutpage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Clicked on Checkout button and navigated to Checkout page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Return to shopping bag' link should displayed");
			Log.softAssertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lnkReturnToShoppingBag"), checkoutpage),
					"<b>Actual Result:</b> 'Return to shopping bag' link is displayed",
					"<b>Actual Result:</b> 'Return to shopping bag' link is not displayed", driver);
			if (productCount > 1) {
				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b> 'Do you want to ship to multiple addresses?' with Yes button should displayed if the product quantity is morethan one.");
				Log.softAssertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblShipToMultipleAddress"),
								checkoutpage),
						"<b>Actual Result:</b> The product count is '" + productCount
								+ "'. So the 'Do you want to ship to multiple addresses?' with Yes button is displayed",
						"<b>Actual Result:</b> The product count is '" + productCount
								+ "'. So the 'Do you want to ship to multiple addresses?' with Yes button is not displayed");
			}
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The following sections should displayed ('Shipping address, Shipping methods, order summary with Eidt link, Continue button and Shop with confidence options')");
			Log.softAssertThat(
					(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("fldShippingAddressSection",
							"shippingMethodSection", "fldShopWithConfidenceSection"), checkoutpage))
					&& (checkoutpage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnContinueToBill"),
							checkoutpage)),
					"<b>Actual Result:</b> The following sections are displayed ('Shipping address, Shipping methods, Continue button and Shop with confidence options')",
					"<b>Actual Result:</b> The following sections are not displayed ('Shipping address, Shipping methods, Continue button and Shop with confidence options')",
					driver);
			Log.message(checkoutpage.getTextFromSectionHeaderBySectionName("ordersummary"));
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Order summary section should displayed.");
			Log.assertThat(
					(checkoutpage.elementLayer.verifyPageListElements(Arrays.asList("lstSectionHeader"),
							checkoutpage))
					&& (checkoutpage.getTextFromSectionHeaderBySectionName("ordersummary").contains("Order Summary")),
					"<b>Actual Result:</b> Order summary section is displayed.",
					"<b>Actual Result:</b> Order summary section is not displayed.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_224

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Select or Add Shipping Address section in Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_225(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> shippingAddressElements = Arrays.asList("txtShippingAddressName", "txtFirstNameInShipping",
				"txtLastNameInShipping", "txtAddress_1InShipping", "txtAddress_2InShipping", "txtCityInShipping",
				"drpStateInShipping", "txtPhoneInShipping", "txtZipcodeInShipping");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("2. Login with ( " + emailid + " / " + password + " )");
			ShoppingBagPage shoppingBagPage = myaccount.headers.NavigateToBagPage();
			shoppingBagPage.removeAddedCartItems();
			shoppingBagPage.headers.navigateTo(category[0], category[1]);
			Log.message("3. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("4. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("5. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("6. Minicart icon is clicked and navigated to Shopping bag page");
			CheckoutPage checkoutpage = shoppingbagpage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("7. Clicked Checkout button in Shopping bag page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The following field should displayed in Shipping address section (Saved Address, Address Name, First Name, Last Name, Address1, Address2(Optional), City, State, Zip code and Phone number)");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(shippingAddressElements, checkoutpage),
					"<b>Actual Result:</b> The following field is displayed in Shipping address section (Saved Address, Address Name, First Name, Last Name, Address1, Address2(Optional), City, State, Zip code and Phone number)",
					"<b>Actual Result:</b> The following field is not displayed in Shipping address section (Saved Address, Address Name, First Name, Last Name, Address1, Address2(Optional), City, State, Zip code and Phone number)",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_225

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system navigate the User to Place Order screen when clicking Continue button from Billing Address pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_198(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String CouponCode = testData.get("CouponCode");
		String SurchargeAppliedMessage = "Surcharge Applied";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Load PDP Page for Search Keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");
			// selecting size from Size Drop down in Pdp Page
			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");
			float ProductpriceofOneProduct = Float.parseFloat(pdppage.getProductPrice());
			Log.message("6. Price Of a Selected Product is " + ProductpriceofOneProduct);
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("7. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");
			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			shoppingBagPage.applyCouponInShoppingBag(CouponCode);
			Log.message("10. Coupon is Applied to the Product");
			float AppliedCouponprice = shoppingBagPage.getAppliedCouponPrice();
			float Surcharge = Float.parseFloat(shoppingBagPage.txtShippingSurchargeAmmount().split("\\$")[1]);
			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("11. Navigated to checkout page!");
			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("12. Clicked on CheckOutAsGuest Button");
			checkoutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("13. Shipping Address details are entered in the Shipping Address Page!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("14. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("15. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("16. Billing address details are entered in the Billing Page!");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("17. Entered card details!");
			checkoutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("19. Clicked on 'Continue' Button in Validation Modal");
			float SubTotalInPlaceOrderPage = Float.parseFloat(checkoutPage.PrdSubtotal().split("\\$")[1]);
			Float ExpextedItemSubtotal = (ProductpriceofOneProduct) * (quantity) + (Surcharge) - (AppliedCouponprice);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Product Panel should be displayed in Place Order screen with the following details : Product image, Product name, Variations (i.e. size and / or color),Shipping Surcharge Applied Message, Quantity, Price (Original and Now pricing), Coupon Name, Subtotal Calculated value = (current price of the product) * (quantity) (sum of prorated deal based offers per line item) (sum of prorated coupon amount per line item).");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("lstProductNamesInOrderSummary"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("productImageInPlaceOrderPage"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSize"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtColor"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblQuantityValue"), checkoutPage)
					&& checkoutPage.getTextFromSurchargeAppliedMsg().contains(SurchargeAppliedMessage)
					&& checkoutPage.elementLayer
							.verifyPageElements(Arrays.asList("NowPriceInBillingPageOnOrderSummary"), checkoutPage)
					&& checkoutPage.elementLayer
							.verifyPageElements(Arrays.asList("OrgPriceInBillingPageOnOrderSummary"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtCoupon"), checkoutPage)
					&& SubTotalInPlaceOrderPage == ExpextedItemSubtotal,
					"<b>Actual Result 1:</b> Product Panel is displayed in Place Order screen with the following details : Product image, Product name, Variations (i.e. size and / or color),Shipping Surcharge Applied Message, Quantity, Price (Original and Now pricing), Coupon Name, Subtotal Calculated value "
							+ SubTotalInPlaceOrderPage
							+ " = (current price of the product) * (quantity) (sum of prorated deal based offers per line item) (sum of prorated coupon amount per line item).",
					"<b>Actual Result 1:</b> Product Panel is not displayed in Place Order screen with the following details : Product image, Product name, Variations (i.e. size and / or color),Shipping Surcharge Applied Message, Quantity, Price (Original and Now pricing), Coupon Name, Subtotal Calculated value "
							+ SubTotalInPlaceOrderPage
							+ " = (current price of the product) * (quantity) (sum of prorated deal based offers per line item) (sum of prorated coupon amount per line item).",
					driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Order Summary Panel should be displayed in Place Order screen with the following details: Merchandise Total, Shipping Surcharge, Estimated Shipping, Estimated Sales Tax,OrderTotal");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("txtMerchandiseTotalCost"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("txtTotalCouponSavings"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtShippingCost"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtEstimatedSalesTaxCost"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtTotalOrderValure"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSurchargeValue"), checkoutPage),
					"<b>Actual Result 2:</b> Order Summary Panel is displayed in Place Order screen with the following details: Merchandise Total, Shipping Surcharge, Estimated Shipping, Estimated Sales Tax,OrderTotal",
					"<b>Actual Result 2:</b> Order Summary Panel is not displayed in Place Order screen with the following details: Merchandise Total, Shipping Surcharge, Estimated Shipping, Estimated Sales Tax,OrderTotal",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_198//

	@Test(groups = {
			"desktop" }, description = "Verify system allows the User to Edit Shipping Address in Place Order page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_199(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String[] editData = testData.get("EditData").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");
			//
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6. Clicked on 'Checkout' button in Shopping bag page");
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7. Clicked on 'Checkout as Guest' button and navigated to Shipping address page");
			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("8. Filled Shipping details and selected the shipping method", driver);
			checkoutpage.clickOnContinueInShipping();
			Log.message("9. Clicked on 'Continue' button in Shipping address page");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Clicked 'Continue' from the popup and again clicked the 'Continue' button");
			Log.message("11. Navigated to Billing address page");
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Filled billing details");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the payment card details", driver);
			checkoutpage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' button from the Billing page");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b>Edit link should be displayed in Shipping Address pane");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageListElements(Arrays.asList("editOrderSummary"), checkoutpage),
					"<b>Actual Result-1:</b> Edit link is displayed in Shipping address pane",
					"<b>Actual Result-1:</b> Edit link is not displayed in Shipping address pane", driver);
			checkoutpage.clickOnEditBySectionName("shippingaddress");
			checkoutpage.editFirstNameInAddressBySection("shippingaddress", editData);
			Log.message("<br>");
			Log.message("15. Edited and updated the shipping address");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>User should able to edit the Shipping address");
			Log.assertThat(checkoutpage.getFirstNameInAddressBySection("shippingaddress").contains(editData[0]),
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' in first name and it is updated in shipping address pane.",
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' in first name and it is not updated in shipping address pane.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_199

	@Test(groups = {
			"desktop" }, description = "Verify system allows the User to Edit Billing Address in Place Order page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_200(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String[] editData = testData.get("EditData").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");
			//
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6. Clicked on 'Checkout' button in Shopping bag page");
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7. Clicked on 'Checkout as Guest' button and navigated to Shipping address page");
			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("8. Filled Shipping details and selected the shipping method", driver);
			checkoutpage.clickOnContinueInShipping();
			Log.message("9. Clicked on 'Continue' button in Shipping address page");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Clicked 'Continue' from the popup and again clicked the 'Continue' button");
			Log.message("11. Navigated to Billing address page");
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Filled billing details");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the payment card details", driver);
			checkoutpage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' button from the Billing page");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b>Edit link should be displayed in Billing Address pane");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageListElements(Arrays.asList("editOrderSummary"), checkoutpage),
					"<b>Actual Result-1:</b> Edit link is displayed in Billing address pane",
					"<b>Actual Result-1:</b> Edit link is not displayed in Billing address pane", driver);
			checkoutpage.clickOnEditBySectionName("billingaddress");
			checkoutpage.editFirstNameInAddressBySection("billingaddress", editData);
			Log.message("<br>");
			Log.message("15. Edited and updated the billing address");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>User should able to edit the Billing address");
			Log.assertThat(checkoutpage.getFirstNameInAddressBySection("billingaddress").contains(editData[0]),
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' in first name and it is updated in Billing address pane",
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' in first name and it is not updated in billing address pane",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_200

	@Test(groups = {
			"desktop" }, description = "Verify system allows the User to Edit Payment method in Place Order page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_201(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		String[] editData = testData.get("EditData").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");
			//
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6. Clicked on 'Checkout' button in Shopping bag page");
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7. Clicked on 'Checkout as Guest' button and navigated to Shipping address page");
			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("8. Filled Shipping details and selected the shipping method", driver);
			checkoutpage.clickOnContinueInShipping();
			Log.message("9. Clicked on 'Continue' button in Shipping address page");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Clicked 'Continue' from the popup and again clicked the 'Continue' button");
			Log.message("11. Navigated to Billing address page");
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Filled billing details");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the payment card details", driver);
			checkoutpage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' button from the Billing page");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b>Edit link should be displayed in Payment method pane");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageListElements(Arrays.asList("editOrderSummary"), checkoutpage),
					"<b>Actual Result-1:</b> Edit link is displayed in Payment method pane",
					"<b>Actual Result-1:</b> Edit link is not displayed in Payment method pane", driver);
			checkoutpage.clickOnEditBySectionName("paymentmethod");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			checkoutpage.editNameOnCardInPaymentMethod(editData);
			Log.message("<br>");
			Log.message("15. Edited and updated the payment method");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>User should able to edit the Payment method pane");
			Log.assertThat(checkoutpage.getFirstNameInAddressBySection("paymentmethod").equals(editData[0]),
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' the name and it is updated in Payment method pane",
					"<b>Actual Result-2:</b> User edited '" + editData[0]
							+ "' the name and it is not updated in Payment method pane",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_201

	@Test(groups = {
			"desktop" }, description = "Verify pages navigates to My Bag screen when clicking 'Edit' link in Shipping Address from Place Order screen - behavior", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_202(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "' page");
			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");
			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");
			//
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6. Clicked on 'Checkout' button in Shopping bag page");
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7. Clicked on 'Checkout as Guest' button and navigated to Shipping address page");
			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("8. Filled Shipping details and selected the shipping method", driver);
			checkoutpage.clickOnContinueInShipping();
			Log.message("9. Clicked on 'Continue' button in Shipping address page");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Clicked 'Continue' from the popup and again clicked the 'Continue' button");
			Log.message("11. Navigated to Billing address page");
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Filled billing details");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the payment card details", driver);
			checkoutpage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' button from the Billing page");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b>User should redirected to Place Order screen.");
			String className = checkoutpage.getClassNameOfStepsInCheckoutPage("placeorder").split(" ")[1];
			Log.assertThat(className.equals("active"),
					"<b>Actual Result-1:</b>User is redirected to Place Order screen.",
					"<b>Actual Result-1:</b>User is not redirected to Place Order screen.", driver);
			Log.message("<br>");
			checkoutpage.clickOnCancelButtonInPlaceOrder();
			Log.message("15. Clicked on 'Cancel' button from the place order screen");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b>Order should cancel successfully and 'Continue to Shopping' button should be displayed when clicking Cancel button from Place order screen");
			Log.assertThat(
					(checkoutpage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("tabPlaceOrder"),
							checkoutpage))
					&& (checkoutpage.elementLayer.verifyPageElements(Arrays.asList("btnContinueShopping"),
							checkoutpage)),
					"<b>Actual Result-2:</b> Order cancelled successfully and 'Continue to Shopping' button is displayed.",
					"<b>Actual Result-2:</b> Order is not cancelled successfully and/or 'Continue to Shopping' button is not displayed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_202

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'View All Coupons' link in Coupons section of cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_064(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to the shopping bag page");

			CouponsPage couponPage = shoppingBagPage.clickViewAllcoupon();
			Log.message("10. Clicked on 'View all Coupons' link ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be navigated to Coupons Page");

			Log.assertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("btnAddCouponToBag"), couponPage),
					"<b>Actual Result:</b> Navigated to Coupons Page",
					"<b>Actual Result:</b> Not navigated to Coupons Page", driver);

			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'Continue Shopping' button in cart with items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_065(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// selecting size
			String size = pdpPage.addProductToBag().split("\\|")[1];
			Log.message("4. Size '" + size + "' is selected from Size dropdown in PDP Page");

			// Getting Current URl
			String previousURL = driver.getCurrentUrl();

			// Adding Product to Bag
			Log.message("5. Product is added to the cart");

			// Navigating to shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Clicked on Bag and navigated to Shopping bag page");

			// click on 'continue' shopping buttton
			shoppingBagPage.clickContinueShopping();
			Log.message("7. Clicked on 'Continue shopping' button");

			// Getting current URL
			String currentURL = driver.getCurrentUrl();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> By Clicking on 'Continue Shopping', it should be navigated to Home Page");

			Log.assertThat(currentURL.trim().equals("https://development-web-belk.demandware.net/s/Belk/home/"),
					"<b>Actual Result:</b> By Clicking on 'Continue Shopping', it is navigated to Home Page",
					"<b>Actual Result:</b> By Clicking on 'Continue Shopping', it is not navigated to Home Page");

			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Checkout for Not Logged In Users from cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_066(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String couponCode = testData.get("CouponCode");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "' and Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("5. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("6. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");

			shoppingBagPage.enterCouponCode(couponCode);
			Log.message("8. Enter '" + couponCode + "' in Coupon code field");
			Log.assertThat(couponCode.equals(shoppingBagPage.getAppliedCouponCode()),
					"9. Coupon code '" + couponCode + "' is entered",
					"9. Coupon code '" + couponCode + "' is not entered");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("10.Clicked on Checkout button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be navigated to Login Page");

			Log.assertThat(siginPage.elementLayer.verifyPageElements(Arrays.asList("btnLoginIn"), siginPage),
					"<b>Actual Result:</b> Navigated to Login Page",
					"<b>Actual Result:</b> Not navigated to Login Page", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_CART_066

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Need a Coupon Code Link in empty cart with coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_049(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			ShoppingBagPage shoppingBagPage = homePage.ClikOnBagForWithNoProducts();
			Log.message("2. Navigated to shopping bag page");

			CouponsPage couponsPage = shoppingBagPage.clickNeedCoupon();
			Log.message("3. Clicked on 'Need a coupon' link in shopping bag page and navigated to coupons page");

			couponsPage.clickAddCouponToBag();
			Log.message("4. 'Add coupon to bag' button is clicked");

			Log.assertThat(
					couponsPage.elementLayer.verifyPageElements(Arrays.asList("txtCouponAddedMessage"), couponsPage),
					"5. Coupon is added to the cart", "5. Coupon is not added to the cart");

			couponsPage.ClikOnBagForWithNoProducts();
			Log.message("6. Navigated to shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Need a Coupon' link should not be displayed in Shopping bag page");
			Log.assertThat(shoppingBagPage.verifyNeedCouponlink(),
					"<b>Actual Result:</b> 'Need a Coupon' link is not displayed in Shopping bag page",
					"<b>Actual Result:</b> 'Need a Coupon' link is displayed in Shopping bag page", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_CART_049

	@Test(groups = {
			"mobile" }, description = "Mobile : Verify system disappears the Mini Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_043(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to PDP of the product");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product is added to the cart");

			pdpPage.clickOnMiniCartIcon();
			Log.message("9. Clicked on mini cart");

			pdpPage.clickOnLogo();
			Log.message("10. Clicked on belk Logo ");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Mini Cart overlay should not be displayed");

			Log.assertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("cartOverlay"), pdpPage),
					"<b>Actual Result:</b> Mini Cart Overlay is not displayed",
					"<b>Actual Result:</b> Mini Cart Overlay is displayed", driver);

			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify 'Continue Shopping' button in cart with items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_267(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			String homeURL = driver.getCurrentUrl();

			// Clicked on bag
			ShoppingBagPage shoppingBagPage = homePage.ClikOnBagForWithNoProducts();
			Log.message("2. Clicked on Bag and navigated to Shopping bag page");

			// Clicked on continue shopping
			shoppingBagPage.clickContinueShoppingWithoutProducts();
			Log.message("3. Clicked on 'Continue shopping' button");

			String currentURL = driver.getCurrentUrl();

			Log.message("<br>");
			Log.message("<b>Expected Result :</b> User should be navigated to Home Page");

			Log.assertThat(currentURL.equals(homeURL), "<b>Actual Result :</b> Navigated to home page",
					"<b>Actual Result :</b> Not navigated to home page", driver);

			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_CART_267

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify View Bag icon in mini-cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_039(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Random product got selected. Navigated to PDP");
			pdpPage.addProductToBag();
			Log.message("4. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. View bag button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be navigated to the Shopping bag page.");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("divCart"), shoppingBagPage),
					"<b>Actual Result:</b> User is navigated to shopping bag page ",
					"<b>Actual Result:</b> User is not navigated to shopping bag page", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_CART_039

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Add Coupon to Bag button in Coupon details page for Invalid Coupon", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_021(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String couponCode = testData.get("CouponCode");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			pdpPage.addProductToBag();
			Log.message("3. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicked on the minicart icon");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("divCart"), shoppingBagPage),
					"5. Navigated to shopping bag page", "5. Not navigated to shopping bag page", driver);
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("6. Invalid coupon is entered and Apply coupon button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> User should see that an Error Message is displayed");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtInvalidCouponError"),
							shoppingBagPage),
					"<b>Actual Result-1:</b> Error message for invalid coupon applied is displayed properly",
					"<b>Actual Result-1:</b> Error message for invalid coupon applied is not displayed properly",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> Coupon should not get applied to the item in the bag.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAppliedCoupon"),
							shoppingBagPage),
					"<b>Actual Result-2:</b> Coupon is not applied to the item in the bag.",
					"<b>Actual Result-2:</b> Coupon is  applied to the item in the bag.", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_CART_021

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Shipping Method details in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_229(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. SignIn to the application with ('" + emailid + " / " + password
					+ "') navigated to My Account Page!");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with keyword '" + searchKey);
			Log.message("5. Navigated to PDP Page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("8. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("9. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to 'Shopping bag'!");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked Checkout button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Shipping Method Section should be displayed");

			Log.assertThat(checkoutPage.verifySippmentType(), "<b>Actual Result:</b> Shipping methods are displayed",
					"<b>Actual Result:</b> Shipping methods are not displayed");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_229

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Need a Coupon Code Link in empty cart with no coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_044(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.assertThat(
					homePage.headers.getQuantityFromBag().equals("0")
							|| homePage.headers.getQuantityFromBag().equals("0 item(s)"),
					"2. No product has been added to the shopping bag", "2. product has been added to the shopping bag",
					driver);
			ShoppingBagPage shoppingBagPage = homePage.headers.NavigateToBagPage();
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAppliedCoupon"),
							shoppingBagPage),
					"3. User has not added any coupon to the bag.", "3. User has added coupon to the bag.", driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Need a Coupon Code Link should be available below the Continue Shopping button");
			Log.assertThat(shoppingBagPage.verifyNeedCouponDisplayedBelowContinueShopping(),
					"<b>Actual Result-1:</b> Need a coupons Link is present",
					"<b>Actual Result-1:</b> Need a coupons Link is not present ", driver);
			Log.message("<br>");
			shoppingBagPage.clickNeedCoupon();
			Log.message("3. Need a coupon link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b>On click Need a coupon link, the User should be taken to the coupons page");

			Log.assertThat(Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "coupons"),
					"<b>Actual Result-2:</b> The page is getting navigated to coupon page",
					"<b>Actual Result-2:</b> The page is not getting navigated to coupon page", driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_CART_044

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the message in empty cart with no coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_047(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		final WebDriver driver = WebDriverFactory.get(browser);
		String msgEmptyBag = "Your shopping bag is empty.";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.assertThat(
					homePage.headers.getQuantityFromBag().equals("0")
							|| homePage.headers.getQuantityFromBag().equals("0 item(s)"),
					"2. No product has been added to the shopping bag",
					"2. product has been added to the shopping bag");
			ShoppingBagPage shoppingBagPage = homePage.headers.NavigateToBagPage();
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAppliedCoupon"),
							shoppingBagPage),
					"3. User has not added any coupon to the bag.", "3. User has added  coupon to the bag.");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Text 'Your shopping bag is empty. Please add items to your shopping bag...' message should be shown");
			Log.assertThat(shoppingBagPage.getTextFromEmptyBag().equals(msgEmptyBag),
					"<b>Actual Result:</b> 'Your shopping bag is empty. Please add items to your shopping bag...' message is properly shown",
					"<b>Actual Result:</b> 'Your shopping bag is empty. Please add items to your shopping bag...' message is not properly shown",
					driver);
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally

	}// TC_BELK_CART_047

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Continue Shopping button in empty cart with no coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_048(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String urlOfSearchPage;
		String urlOfContinueShopPage;
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			Log.assertThat(
					homePage.headers.getQuantityFromBag().equals("0")
							|| homePage.headers.getQuantityFromBag().equals("0 item(s)"),
					"2. No product has been added to the shopping bag",
					"2. product has been added to the shopping bag");
			ShoppingBagPage shoppingBagPage = homePage.headers.NavigateToBagPage();
			Log.message("3. Navigated to Shopping Bag page");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAppliedCoupon"),
							shoppingBagPage),
					"4. User has not added any coupon to the bag.", "4. User has added  coupon to the bag.");
			shoppingBagPage.clickContinueShoppingWithoutProducts();
			Log.message("5. Continue Shopping link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b>On click Continue Shopping link, the User should be taken to the Home page");
			Log.assertThat(homePage != null, "<b>Actual Result-1:</b> The page is getting navigated to Home page",
					"<b>Actual Result-2:</b> The page is not getting navigated to Home page", driver);

			Log.assertThat(webSite.equals(driver.getCurrentUrl()) && homePage != null,
					"<b>Actual Result:</b> Clicked on  Continue Shopping link and navigated to Home page with the expected url: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Clicked on Continue Shopping link and not navigated to Home page and the actual url is : "
							+ driver.getCurrentUrl());

			Log.message("<br>");
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message("6. Searched with keyword '" + searchKey + "' and navigated to search result Page");
			urlOfSearchPage = driver.getCurrentUrl();
			searchResultPage.headers.NavigateToBagPage();
			Log.message("7. Navigated to Shopping Bag page");
			shoppingBagPage.clickContinueShoppingWithoutProducts();
			Log.message("8. Clicked Continue Shopping button");
			urlOfContinueShopPage = driver.getCurrentUrl();
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>The User should be taken back to their last browsed page on click.");
			Log.assertThat(urlOfSearchPage.equals(urlOfContinueShopPage),
					"<b>Actual Result-2:</b> The User is taken back to their last browsed page on click",
					"<b>Actual Result-2:</b> The User is not taken back to their last browsed page on click", driver);
			Log.message("<br>");
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_048

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system navigates the User to Billing Address page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_166(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'PDP' Page with randomly selected product");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Naviagted to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagted to checkout page");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. 'CheckoutAsGuest' button is clicked");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("10. Filled the shipping address");
			checkOutPage.clickOnContinueInShipping();
			String zipCodeInSuggestedAddress = checkOutPage.returnZipCodeInSuggestedAddress();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Navigated to Billing page from shipping page!");
			String zipcodeInShipping = checkOutPage.returnShippingZipCodeInBillingAddressPanel();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Suggested Address should be properly displayed in Billing Address panel");
			Log.assertThat(zipCodeInSuggestedAddress.equals(zipcodeInShipping),
					"<b>Actual Result:</b> Suggested Address is properly displayed in Billing Address panel",
					"<b>Actual Result:</b> Suggested Address is not properly displayed in Billing Address panel",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_166

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays Footer links under Shipping Address panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_167(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Naviagte to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagte to checkout page");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. CheckoutAsGuest button is clicked");
			Log.message(
					"<b>Expected Result:</b>Following should be displayed as Footer link under Shipping Address panel,Shop with Confidence Label :Shipping Rates,Easy Returns,FAQS,User Services,Privacy and Security Guarantee");
			Log.assertThat(checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lnkFooter"), checkOutPage),
					"<b>Actual Result:</b> Shop with Confidence Label:Shipping Rates,Easy Returns,FAQS,User Services,Privacy and Security Guarantee are present properly",
					"<b>Actual Result:</b> Shop with Confidence Label:Shipping Rates,Easy Returns,FAQS,User Services,Privacy and Security Guarantee are not present properly");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_167

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system navigates the User to 'Step 1 page in Cart screen. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_135(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		String cardDetails = "card_Visa";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		// List<String> shoppingBagPageElement = Arrays.asList("charCountMsg");

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");

			// Navigate to PDP Page for Search Keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Select Size, Colour and add to Bag
			pdpPage.addProductToBag();
			Log.message("8. Add product to bag from pdpPage !");

			// Navigate to Mini Cart Bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Clicking on Mini cart icon is navigated to the bag page");

			// Click on Check out Button
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigate to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", address);
			Log.message("11. Shipping details entered in Checkout Page!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling billingDetails
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("12. Billing details entered in checkout page!", driver);

			// Filling paymentDetails
			checkoutPage.fillingCardDetails("NO", cardDetails);
			Log.message("13. Card Details Filled Successfully!", driver);

			// Click on Continue Buuton
			checkoutPage.clickOnContinueInBilling();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicking on Continue in the Billing Page.");

			// Click on Billing Panel in Header
			checkoutPage.clickOnShippingInHeader();
			String shippingReturnUrl = driver.getCurrentUrl();
			Log.message("15. Clicking on Edit on Billing Page.");

			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b>System should navigate the User to Checkout Step 1 Shipping page when User click on the Billing Panel.");
			Log.message("<b> Actual Result: </b> The Return Page shipping URl is :" + shippingReturnUrl);
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("ShippingInHeaderEnabled"),
							checkoutPage),
					"<b> Actual Result: </b> Billing button is enabled when redirect to shipping page from place order page",
					"<b> Actual Result: <b> Billing button is disabled when redirect to shipping page from place order page",
					driver);

			Log.testCaseResult();
			BrowserActions.nap(3);
			shoppingBagPage = checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_135

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the checkout steps for non-multi ship options.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_130(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Laod HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Load SignIn Page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Navigating to SearchResultPage
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("8. Product is added to the cart");

			// naviagting to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. By clicking on 'minicart' icon, It Naviagted to Shopping bag");

			CheckoutPage checkoutpage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Naviagte to checkout page", driver);
			Log.message(
					"The following Checkout steps should be displayed for the Non-Multi ship option, Shipping, Billing, Place Order");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("tabShipping"), checkoutpage),
					"<b>Actual Result:</b> Shipping field is present",
					"<b>Actual Result:</b> Shipping field is not present");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("tabBilling"), checkoutpage),
					"<b>Actual Result:</b> Shipping field is present",
					"<b>Actual Result:</b> Billing field is not present");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("tabPlaceOrder"), checkoutpage),
					"<b>Actual Result:</b> Shipping field is present",
					"<b>Actual Result:</b> Payment field is not present");

		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally
	}// TC_BELK_CART_130

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Free Shipping message in the shopping cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_082(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String txtfreeshoppingmsg = "Congratulations! You qualify for Free Shipping";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigating to SearchResultPage
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Navigating to PDP page
			PdpPage pdppage = searchresultpage.navigateToPDPWithProductId();
			Log.message("3. Navigated to pdpPage with productId");

			// selecting color
			String color = pdppage.selectColor();
			Log.message("4. Selected Color'" + color + "'from the color swatch in the pdp page!");

			// selecting Size
			String size = pdppage.selectSize();
			Log.message("5. Selected size'" + size + "' from Size dropdown in the pdp page!");

			// selecting Quantity
			pdppage.selectQtyByIndex(4);
			Log.message("6. Selected quantity '4' from the  Quantity dropdown in the pdp page!");

			// Getting Product Name from BreadCrumb
			String productName = pdppage.getProductNameFromBreadCrumb();
			Log.message("7. Getting Product Name:" + productName + "from breadcrum in the pdp page!");

			// Adding Product to Bag
			pdppage.clickAddToBag();
			Log.message("8. Selected product" + productName + "is added to bag from pdpPage!");

			// Navigating to 'ShoppingBag' Page
			ShoppingBagPage shoppingbagpage = pdppage.minicart.navigateToBag();
			Log.message("9. By clicking on 'Minicart' icon ,It Navigated to  'ShoppingBag' page!");

			// Getting 'FreeShipping' text
			shoppingbagpage.getTextfromFreeshipping();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After adding product in the shoppingBag page, Free shipping message should be shown ");
			Log.assertThat(shoppingbagpage.getTextfromFreeshipping().trim().contains(txtfreeshoppingmsg),
					"<b>Actual Result:</b> After adding product in shoppingBag page , Free shipping message is shown:'"
							+ shoppingbagpage.getTextfromFreeshipping().trim() + "' in the 'ShoppingBag' page.",
					"<b>Actual Result:</b> After adding product in shoppingBag page , Free shipping message is  not shown:'"
							+ shoppingbagpage.getTextfromFreeshipping().trim() + "' in the 'ShoppingBag' page.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_082

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays checkout steps for the Non-Multi ship option.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_131(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result:</b> In shipping Details Page, Without shipping address details filled, page should not be continued");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnContinueToBill"),
							checkOutPage),
					"<b>Actual Result:</b> In shipping Details Page, Without shipping address details filled cannot continue to billing tab!",
					"<b>Actual Result:</b> In shipping Details Page, Without shipping address details filled 'Continue' button is enabled",
					driver);

			// filling the shipping address details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address6", "Standard");
			Log.message("</br>");
			Log.message("9. Shipping address details filled!");
			Log.message("</br>");
			Log.message(
					"<b>Expected Result:</b> By filling shipping details, then the 'continue' button should be Enabled");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("btnContinueToBill"), checkOutPage),
					"<b>Actual Result:</b> By filling shipping details, then the 'continue' button is Enabled",
					"<b>Actual Result:</b> By filling shipping details, then the 'continue' button is not Enabled",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_131

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays the Subtotal value in My Bag screen without Coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_125(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");
			PdpPage pdppage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");
			Log.message("4. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float ProductpriceofOneProduct = Float.parseFloat(pdppage.getProductPrice());
			Log.message("6. Price Of a Selected Product is " + ProductpriceofOneProduct);
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("7. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("8. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to Shoppingbagpage!");
			String ItemSubtotalsplit[] = shoppingbagpage.PrdSubtotal().split("\\$");
			System.out.println(ItemSubtotalsplit[1]);
			float ItemSubTotal = Float.parseFloat(ItemSubtotalsplit[1]);
			Log.message("10. Item SubTotal is " + ItemSubTotal);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Subtotal value should be Calculated by, Calculated value = (current price of the product) * (quantity) (sum of prorated deal based offers per line item)");
			Log.assertThat((ItemSubTotal == (ProductpriceofOneProduct) * (quantity)),
					"<b>Actual Result :</b> Subtotal value is Calculated by, Calculated value " + ItemSubTotal
							+ " = (current price of the product: " + ProductpriceofOneProduct + ") * (quantity: "
							+ quantity + ") (sum of prorated deal based offers per line item)",
					"<b>Actual Result :</b> Subtotal value is not Calculated by, Calculated value " + ItemSubTotal
							+ " = (current price of the product: " + ProductpriceofOneProduct + ") * (quantity: "
							+ quantity + ") (sum of prorated deal based offers per line item)",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays the Subtotal value in My Bag screen with Coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_126(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			// Navigating to PDP page
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			Log.message("4. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float ProductpriceofOneProduct = Float.parseFloat(pdppage.getProductPrice());
			Log.message("6. Price Of a Selected Product is " + ProductpriceofOneProduct);
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("7. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("8. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("10. Coupon is Applied to the Product");
			float ItemSubtotalAfterCouponApplied = shoppingbagpage.getSubTotalAfterUpdatedQuantity();
			String ItemSubtotalsplit[] = shoppingbagpage.PrdSubtotal().split("\\$");
			float ItemSubTotal = Float.parseFloat(ItemSubtotalsplit[1]);
			Log.message("11. Item SubTotal is " + ItemSubTotal);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Subtotal value should be Calculated by, Calculated value = (current price of the product) * (quantity) (sum of prorated deal based offers per line item)");
			Log.assertThat((ItemSubTotal == ItemSubtotalAfterCouponApplied),
					"<b>Actual Result :</b> Subtotal value is Calculated by, Calculated value ("
							+ ItemSubtotalAfterCouponApplied
							+ ") = (current price of the product) * (quantity) (sum of prorated deal based offers per line item)",
					"<b>Actual Result :</b> Subtotal value is not Calculated by, Calculated value ("
							+ ItemSubtotalAfterCouponApplied
							+ ") = (current price of the product) * (quantity) (sum of prorated deal based offers per line item)",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_126

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify backward navigation during the checkout process for non-multichip option.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_132(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			// filling the shipping address details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Shipping address details filled!");

			// getting shipping page url and clicking on continue in shipping
			// page
			String shippingUrlbefore = driver.getCurrentUrl();
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. Navigated to Billing page from shipping page!");

			// click on continue for suggested address by default if Address
			// validation modal opens
			checkOutPage.ContinueAddressValidationModalWithDefaults();

			// clicking on shipping in header navigation and getting the page
			// url
			checkOutPage.clickOnShippingInHeader();
			Log.message("13. Clicked on Shipping Tab in Header!");
			String shippingUrlafter = driver.getCurrentUrl();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result:</b> System should be allowed to navigate the User to a previously completed step which is enabled.");
			Log.assertThat(shippingUrlbefore.equals(shippingUrlafter),
					"<b>Actual Result:</b> System allowed to navigate the User to a previously completed step which is enabled.!",
					"<b>Actual Result:</b> System not allowed to navigate the User to a previously completed step which is enabled.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_132

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows the User to check \"Skip steps by using your default shipping address and payment method with Express Checkout\" checkbox.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_155(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping Bag Page' !");

			// Load signIn page to continue as registered user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as User!");

			// Check the Checkbox Use Express Checkout
			signIn.clickOnExpressCheckOutcheckBox();
			Log.message("10. Use ExpressCheckout checkbox checked!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, password);

			Log.message("11. Navigated to Checkout Page as Registered User!");

			Log.message("</br>");
			Log.message("<b>Expected Result : </b>Express Checkout: Place Order page should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("PlaceOrderInHeaderEnabled"),
							checkOutPage),
					"<b>Actual Result : </b>Express Checkout: Place Order page displayed.!",
					"<b>Actual Result : </b>Express Checkout: Place Order page Not displayed.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_155

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system navigates the User to \"Authenticated Checkout Step 1 - Shipping\" page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_157(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// Select Size, Color, Qty
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product is added to the cart");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as registered user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to SignIn Page to Checkout as User!");

			// Check the Checkbox Use Express Checkout
			signIn.clickOnExpressCheckOutcheckBox();
			Log.message("11. Use ExpressCheckout checkbox checked!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, passwd);
			Log.message("12. Navigated to Checkout Page as Registered User!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("ShippingInHeaderEnabled"),
							checkOutPage),
					"<b>Actual Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page displayed.!",
					"<b>Actual Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page <b>not</b> displayed.",
					driver);

			Log.message("</br>");
			Log.message("<b>Expected Result : </b>User should not have default shipping address.");
			Log.assertThat(checkOutPage.getFirstNameInShippingAddress().isEmpty(),
					"<b>Actual Result : </b>User don't have default shipping address!",
					"<b>Actual Result : </b>User Have default shipping Address.", driver);

			Log.testCaseResult();
			BrowserActions.nap(3);
			shoppingBagPage = checkOutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_157

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system navigates the User to \"Authenticated Checkout Step 1 - Billing/ Payment page.\" page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_158(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// Select Size, Color, Qty
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as registered user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as User!");

			// Check the Checkbox Use Express Checkout
			signIn.clickOnExpressCheckOutcheckBox();
			Log.message("10. Use ExpressCheckout checkbox checked!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>\"Authenticated Checkout Step 2 - Billing\" page should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkOutPage),
					"<b>Actual Result : </b>\"Authenticated Checkout Step 2 - Billing\" page displayed.!",
					"<b>Actual Result : </b>\"Authenticated Checkout Step 2 - Billing\" page <b>not</b> displayed.",
					driver);

			Log.testCaseResult();

			BrowserActions.nap(3);
			shoppingBagPage = checkOutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_158

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system navigate the User to \"Authenticated Checkout Step 1 - Billing/ Payment page.\" page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_159(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");
			// Select Size
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping Bag Page' !");

			// Load signIn page to continue as registered user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as User!");

			// Check the Checkbox Use Express Checkout
			signIn.clickOnExpressCheckOutcheckBox();
			Log.message("10. Use ExpressCheckout checkbox checked!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, passwd);
			Log.message("11. Navigated to Checkout Page as Registered User!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>\"Authenticated Checkout Step 2 - Billing\" page should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkOutPage),
					"<b>Actual Result : </b>\"Authenticated Checkout Step 2 - Billing\" page displayed.!",
					"<b>Actual Result : </b>\"Authenticated Checkout Step 2 - Billing\" page <b>not</b> displayed.",
					driver);

			Log.message("</br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_159

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system navigates the User to \"Authenticated Checkout Step 1 - Shipping\" page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_160(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping Bag Page' !");

			// Load signIn page to continue as registered user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as User!");

			Log.message("</br>");
			Log.message("<b>Expected Result : </b>'Use Express Checkout' checkbox should be disbled!");
			Log.assertThat(!(signIn.isUseExpressCheckoutChecked()),
					"<b>Actual Result : </b>'Use Express Checkout' checkbox not checked!",
					"<b>Actual Result : </b>'Use Express Checkout' checkbox is checked.", driver);

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, passwd);
			Log.message("10. Navigated to Checkout Page as Registered User!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("ShippingInHeaderEnabled"),
							checkOutPage),
					"<b>Actual Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page displayed.!",
					"<b>Actual Result : </b>\"Authenticated Checkout Step 1 - Shipping\" page <b>not</b> displayed.",
					driver);
			// Clean up
			shoppingBagPage = checkOutPage.clickReturnToShoppingBag();
			if (Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage.removeItemsFromBag();
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_160

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Guest Checkout page is properly opened when clicking \"Checkout\" button from My Bag screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_161(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : '" + searchKey + "'");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDPWithProductId();
			Log.message("3. Navigated to PDP from Search results page!");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("7. Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to Shopping bag page!");

			String shoppingBagPageURL = driver.getCurrentUrl();

			// getting number of products in shopping bag
			int productCount = shoppingBagPage.getProductCount();

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Clicked on 'Checkout' button");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			Log.message("</br><table>");
			Log.message(
					"<tr><td colspan=2><b>Expected Result : </b>Following field should be properly shown!</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(
							Arrays.asList("tabShipping", "tabBilling", "tabPlaceOrder"), checkOutPage),
					"<tr><td><b>Checkout Header</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Checkout Heade</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			if (productCount > 1) {
				Log.softAssertThat(
						checkOutPage.elementLayer.verifyPageElements(Arrays.asList("btnYesForMultipleAddressShipping"),
								checkOutPage),
						"<tr><td><b>Ship To Mulitple Address</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
						"<tr><td><b>Ship To Mulitple Address</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");
			}

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtFirstNameInShipping"), checkOutPage),
					"<tr><td><b>First Name</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>First Name<</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtLastNameInShipping"), checkOutPage),
					"<tr><td><b>Last Name</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Last Name</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_1InShipping"), checkOutPage),
					"<tr><td><b>Address-1</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Address-1</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_2InShipping"), checkOutPage),
					"<tr><td><b>Address-2</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Address-2</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtCityInShipping"), checkOutPage),
					"<tr><td><b>City</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>City</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtZipcodeInShipping"), checkOutPage),
					"<tr><td><b>ZipCode</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>ZipCode</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtPhoneInShipping"), checkOutPage),
					"<tr><td><b>Phone</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Phone</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(checkOutPage.verifySippmentType(),
					"<tr><td><b>Shipment Types</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Shipment Types</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblColor"), checkOutPage),
					"<tr><td><b>Color</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Color</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblSize"), checkOutPage),
					"<tr><td><b>Size</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Size</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblQuantity"), checkOutPage),
					"<tr><td><b>Quantity</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Quantity</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblPrice"), checkOutPage),
					"<tr><td><b>Price</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Price</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblMerchandiseTotalValue"),
							checkOutPage),
					"<tr><td><b>Merchandise Total</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Merchandise Total</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippingSurchargeValue"),
							checkOutPage),
					"<tr><td><b>Shipping Surcharge</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Shipping Surcharge</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblEstimatedShippingValue"),
							checkOutPage),
					"<tr><td><b>Estimated Shipping</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Estimated Shipping</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblEstimatedSalesTaxValue"),
							checkOutPage),
					"<tr><td><b>Estimated Sales Tax</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Estimated Sales Tax</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblOrderTotalValue"), checkOutPage),
					"<tr><td><b>Order Total</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Order Total</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");
			Log.message("</table>");

			Log.assertThat(true, "Screen Shot: ", "", driver);
			Log.testCaseResult();

			shoppingBagPage = new ShoppingBagPage(driver, shoppingBagPageURL).get();
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_161

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows User to add multiple valid Shipping Address in Shipping Address pane.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_162(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultpage;
			PdpPage pdpPage = null;

			for (int i = 2; i <= 3; i++) {

				// Load PDP Page for Search Keyword
				searchResultpage = homePage.searchProductKeyword(searchKey);
				Log.message((i) + " a. Navigated to Search Result Page : '" + searchKey + "'");

				// Load PDP Page for Search Keyword
				pdpPage = searchResultpage.navigateToPDPByRow(i + 2);
				Log.message((i) + " b. Navigated to PDP from Search results page !");

				// Select Size, Color, Qty
				// select the quantity
				String pdtDetails = pdpPage.addProductToBag();
				Log.message((i) + "c. selected size: " + pdtDetails.split("\\|")[1]);
				// Select the colour
				Log.message((i) + "d. selected color: " + pdtDetails.split("\\|")[0]);
				// select the quantity
				Log.message((i) + "e. selected qty1: " + pdtDetails.split("\\|")[2]);
				// Add the product to the bag

				Log.message((i) + " f. Product added to Shopping Bag!");
			}
			// Load shopping bag page
			pdpPage.waitForMiniCartOverlayDisappears();
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Navigated to Shopping bag page!");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5.Clicked on 'Checkout' button");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Clicked on 'Checkout as Guest' button and navigated to Checkout Page as Guest!");

			// Click on Yes in Ship To Multiple Addresses
			checkOutPage.clickOnYesInShipToMultipleAddress();
			Log.message("7. Clicked on Yes Button in Ship to Multiple Addresses!");

			// Clicking on Add link in first product in product list
			checkOutPage.clickOnAddInShipToMulipleAddress();
			Log.message("8. Clicked on Add in Ship to Multiple Addresses!");
			// Filling the address details in pop-up
			checkOutPage.fillingAddressDetailsInMultipleShipping("valid_address1");
			checkOutPage.ContinueAddressValidationModalInMultipleShipping();
			Log.message("9. Clicked on 'Continue' button from the validation popup");
			Log.message("</br><table>");
			Log.message(
					"<tr><td colspan=2><b>Expected Result: </b>Following field should be properly shown in Shipt to Multiple Address Tab!</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(
							Arrays.asList("lnkProductNameInShipToMultipleAddress"), checkOutPage),
					"<tr><td><b>Product Name</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Product Name</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer
							.verifyPageListElements(Arrays.asList("productImageInShipToMultipleAddress"), checkOutPage),
					"<tr><td><b>Product Image</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Product Image</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(
							Arrays.asList("selectAddressInShipToMultipleAddress"), checkOutPage),
					"<tr><td><b>Address dropdown</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Address dropdown</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(checkOutPage.verifyAddressInShipToMultipleAddress(),
					"<tr><td><b>Address List</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Address List</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lnkAddInShipToMultipleAddress"),
							checkOutPage),
					"<tr><td><b>Add Link</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Add Link</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lnkEditInShipToMultipleAddress"),
							checkOutPage),
					"<tr><td><b>Edit Link</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Edit Link</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");
			Log.message("</table>");

			Log.assertThat(true, "Screen Shot : ", "", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.message("</table>");
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_162

	@Test(enabled = false, groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays warning message for invalid Shipping address on Shipping Address pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_163(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// Select Size, Color, Qty
			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			// Log.message(" Selected Quantity : " + qty);

			// add product to shopping bag
			pdpPage.clickAddToBag();
			Log.message("5. Product added to Shopping Bag!");
			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Navigated to SignIn Page to Checkout as Guest!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. Navigated to Checkout Page as Guest!");

			// fill invalid shipping details
			checkOutPage.fillingShippingDetailsAsGuest("inValid_address1", "Standard");
			Log.message("9. Shipping Details entered successfully!");

			// clicking on continue
			// checkOutPage.clickOnShippingAddress();
			// Log.message("10. Continue Button in Shipping Address page
			// clicked!", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Warning Message should be displayed.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(
							Arrays.asList("errMsgCityInShipping", "errMsgZipcodeInShipping", "errMsgPhoneInShipping"),
							checkOutPage),
					"<b>Actual Result : </b>Warning Message displayed!",
					"<b>Actual Result : </b>Warning Message <b>not</b> displayed", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Continue Button should be disabled.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnContinueToBill"),
							checkOutPage),
					"<b>Actual Result : </b>Continue Button Disabled!",
					"<b>Actual Result : </b>Continue Button <b>not</b> Disabled", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_163

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows to add new address in Shipping Address on Shipping Address pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_164(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// Select Size
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");
			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as Guest!");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			// filling the shipping address details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Shipping address details filled!");

			// clicking on continue in shipping page
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Contine in Shipping tab!");

			if (!(checkOutPage.isAddressValidationModalShowing())) {
				Log.fail("Cannot Validate this Script due to Address Validation modal not opened!");
			}

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1: </b>\"Use suggested address\" and \"Use original address as entered\" option should shown with Cancel and Continue button");
			Log.assertThat(
					checkOutPage.verifyRadioOptionsInAddressValidationModal() && checkOutPage.elementLayer
							.verifyPageElements(Arrays.asList("btnContinueInAddressValidationModal",
									"btnCancelInAddressValidationModal"), checkOutPage),
					"<b>Actual Result 1: </b>\"Use suggested address\" and \"Use original address as entered\" option is shown with Cancel and Continue button!",
					"<b>Actual Result 1: </b>\"Use suggested address\" and \"Use original address as entered\" option not displayed as expected",
					driver);

			// choose use original address
			checkOutPage.chooseOriginalAddressInModal();
			Log.message("<br>");
			Log.message("13. Orginial Address Selected!");
			checkOutPage.ContinueAddressValidationModalWithDefaults();

			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b>Page should be properly redirected to Billing Address Pane.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkOutPage),
					"<b>Actual Result 2: </b>Page properly redirected to Billing Address Pane!",
					"<b>Actual Result 2: </b>Page <b>not</b> redirected to Billing Address Pane", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_164

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays \"Order Details\" in order Summary panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_168(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : '" + searchKey + "'");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page!");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product added to Shopping Bag!");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");

			// Load signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Clicked on 'Checkout' button");

			// Load checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as Guest!");

			Log.message("</br><table>");
			Log.message(
					"<tr><td colspan=2><b>Expected Result: </b>Following field should be properly shown!</td></tr>");

			Log.softAssertThat(checkOutPage.getProductNameColorInOrderSummary().equals("rgba(16, 114, 181, 1)"),
					"<tr><td><b>Product Name Color</b></td><td bgcolor='green'><font color='white'>Displayed in Blue Color</font></td></tr>",
					"<tr><td><b>Product Name Color</b></td><td bgcolor='#FF0000'>Not Displayed in Blue Color</td></tr>");

			Log.softAssertThat(checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblSize"), checkOutPage),
					"<tr><td><b>Size</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Size</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblColor"), checkOutPage),
					"<tr><td><b>Color</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Color</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageListElements(Arrays.asList("lblQuantity"), checkOutPage),
					"<tr><td><b>Quantity</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Quantity</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(checkOutPage.verifyPriceHasCurrencySymbol(),
					"<tr><td><b>Price</b></td><td bgcolor='green'><font color='white'>Displayed with $ symbol</font></td></tr>",
					"<tr><td><b>Price</b></td><td bgcolor='#FF0000'>$ symbol Not displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblMerchandiseTotalValue"),
							checkOutPage),
					"<tr><td><b>Merchandise Total</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Merchandise Total</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippingSurchargeValue"),
							checkOutPage),
					"<tr><td><b>Shipping Surcharge</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Shipping Surcharge</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblEstimatedShippingValue"),
							checkOutPage),
					"<tr><td><b>Estimated Shipping</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Estimated Shipping</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblEstimatedSalesTaxValue"),
							checkOutPage),
					"<tr><td><b>Estimated Sales Tax</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Estimated Sales Tax</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");

			Log.softAssertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("lblOrderTotalValue"), checkOutPage),
					"<tr><td><b>Order Total</b></td><td bgcolor='green'><font color='white'>Displayed</font></td></tr>",
					"<tr><td><b>Order Total</b></td><td bgcolor='#FF0000'>Not Displayed</td></tr>");
			Log.message("</table>");
			Log.message("" + driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.message("</table>");
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_168

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows the User to to see the 'Checkout as Guest' button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_147(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("CheckoutAsGuest");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("6. Product is added to the cart");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("5. Navigated to shopping bag page");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("6. Navigated to SignIn page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("'Checkout as Guest' button should be displayed for the Guest user.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(signIn.elementLayer.verifyPageElements(element, signIn),
					"'Checkout as Guest' button is displayed for the Guest user.",
					"'Checkout as Guest' button is not displayed for the Guest user.", driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_147

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system redirects to Shipping Address page on click of the 'Checkout as Guest' button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_148(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> element = Arrays.asList("lnkReturnToShoppingBag");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("6. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");

			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message(
					"Resource message should be displayed as 'You can checkout without creating an account. You'll have a chance to create an account later below the 'Checkout as Guest' button.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			String actual_resourcemessage = signIn.getResourceMessageBelowCheckoutGuest();
			Log.assertThat(actual_resourcemessage.equals(signIn.RESOURCEMESSAGE_BELOW_GUESTCHECKOUT),
					"Resource Message displayed You'll have a chance to create an account later below the 'Checkout as Guest' button. !",
					"Resource Message not displayed below the 'Checkout as Guest' button!", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Shipping address page should be displayed.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			// Navigating to checkout-shipping page
			CheckoutPage checkoutPage = signIn.clikOnCheckoutAsGuest();
			Log.message("9. Navigated to checkout-shipping page");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(element, checkoutPage),
					"Navigated to Shipping page on clicking 'Checkout as Guest' in signIn page!",
					"Not navigated to Shipping page on clicking 'Checkout as Guest' in signIn page", driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_148

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays resource message as 'Please enter your email address and password to login to your account' below the 'Returning User' Text.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_149(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Resource message should be displayed as 'Please enter your email address and password to login to your account' below the Returning User Text.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");

			String resourcemessage = signIn.getResourceMessageBelowReturningUser();
			Log.assertThat(resourcemessage.equals(signIn.RESOURCEMESSAGE_BELOW_RETURNINGUSER),
					"Resource Message: 'Please enter your email address and password to login to your account' displayed below the 'Returning User'",
					"Resource Message is not displaying below Returning users", driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_149

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays Email and Password field in the Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_150(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("txtUserName", "txtPassWord");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");

			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Email and Password field should be displayed in the Checkout page below the Returning User text.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElements(element, signIn),
					"Email and Password fields are displaying in the Checkout page below the Returning User section",
					"Email and Password fields are not displaying in the Checkout page below the Returning User section",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_150

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows to Login the Belk site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_151(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String UserName[] = testData.get("EmailAddress").split("\\|");
		String password[] = testData.get("Password").split("\\|");
		List<String> errorMesg = Arrays.asList("invalidLoginErrorMsg");
		List<String> shoppingpageElement = Arrays.asList("divCart");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("6. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");

			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");
			// Entering invalid user name and password in SignIn page
			signIn.enterEmailID(UserName[0]);
			signIn.enterPassword(password[0]);
			Log.message("9. Entered invalid user name and password in SignIn page");
			// Clicking on SignIn button in SignIn page
			signIn.clickBtnSignIn();
			Log.message("10. Clicked on SignIn button after entering invalid id and password");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message("Warning message should be displayed when User enter the invalid credentials");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElements(errorMesg, signIn),
					"Error message displaying when user enterd the wrong emailId and PassWord !",
					"Error message is not displaying when user enterd the wrong emailId and PassWord !", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Login should be done successfully with the valid Email and password in the checkout page.");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");
			// Entering valid user name and password in SignIn page
			signIn.enterEmailID(UserName[1]);
			signIn.enterPassword(password[1]);
			Log.message("11. Entered valid user name and password in SignIn page");
			// Clicking on SignIn button in SignIn page
			signIn.clickBtnSignIn();
			Log.message("12. Clicked on SignIn button after entering valid id and password");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(shoppingpageElement, shoppingBagPage),
					"User loggedin successfully with valid Email and password in the checkout page",
					"User didnt get Loggedin with valid Email and password in the checkout page", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_151

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify system displays the 'Forgot password' link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_152(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
			String searchKey = testData.get("SearchKey");

			List<String> element = Arrays.asList("verifyForgetPassword");
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				// Load the Home Page
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				// Load the SearchResult Page with search keyword
				PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
				Log.message("2. Searched with keyword '" + searchKey + "' !");
				Log.message("3. Navigated to PDP Page !");
				// select the quantity
				String pdtDetails = pdpPage.addProductToBag();
				Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
				// Select the color
				Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
				// select the quantity
				Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
				// Add the product to the bag
				// Navigating to shopping bag
				ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
				Log.message("7. Navigated to shopping bag page");
				// Navigating to signIn page
				SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
				Log.message("8. Navigated to SignIn page");
				Log.message("<br>");
				Log.message("<b>Expected Result 1:</b>");
				Log.message("Forgot password link should be displayed above the Password field in the checkout page.");
				Log.message("<br>");
				Log.message("<b>Actual Result 1:</b>");
				Log.assertThat(signIn.elementLayer.verifyPageElements(element, signIn),
						"Forgot password link displayed above the Password field in the checkout page.",
						"Forgot password link not displayed above the Password field in the checkout page.", driver);
				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b>");
				Log.message(
						"Request to reset your password message should be displayed, when User mouse over the 'Forgot password' link.");
				Log.message("<br>");
				Log.message("<b>Actual Result 2:</b>");
				// Mouse hovering on Forget password link
				String recovery = signIn.hoverOnForgetPwdLink();
				Log.message("9. Mouse hovered on 'Forget password' link", driver);

				Log.assertThat(recovery.equals(signIn.RESET_PWD_MESG),
						"Request to reset your password message displayed, when User mouse over the 'Forgot password' link.",
						"Request to reset your password message not displayed, when User mouse over the 'Forgot password' link.",
						driver);
				Log.testCaseResult();

			} // try

			catch (Exception e) {
				Log.exception(e, driver);

			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}

	}// TC_BELK_CART_152

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system navigates the User to 'Password Recovery' page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_153(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> element = Arrays.asList("passwordRecoveryPage");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' !");
			Log.message("3. Navigated to PDP Page !");
			// Selecting the Color
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			// Navigating to shopping bag
			MiniCartPage miniCartPage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingBagPage = miniCartPage.navigateToBag();
			Log.message("7. Navigated to shopping bag page");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignIn page");
			// Clicking on Forgot password link in SignIn page
			signIn.clikForgotPwd();
			Log.message("9. Clicked on Forgot password link in SignIn page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("'Password Recovery' page should be displayed, when User Click on the 'Forgot password' link.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(signIn.elementLayer.verifyPageElements(element, signIn),
					"'Password Recovery' page  displayed, when User Click on the 'Forgot password' link.",
					"'Password Recovery' page not displayed, when User Click on the 'Forgot password' link.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_153

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays the 'Return to Shopping' link in the 'Checkout' page for the Non-Multi-ship option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_133(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		List<String> element = Arrays.asList("lnkReturnToShoppingBag");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigated to SignIn page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Belk' SignIn Page!");

			// Logging in the application
			signIn.signInToMyAccount(emailid, password);
			Log.message("3. Loggedin to application with userdId- '" + emailid + "' and password- " + password);

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' !");
			Log.message("5. Navigated to PDP Page !");
			// Selecting the Color
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("8. Product is added to the cart");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to shopping bag page");
			// Navigating to shipping page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to checkout-Shipping page as authorized user!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("User should see that 'Return to Shopping' link is displayed in the 'Checkout' page.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(element, checkoutPage),
					"'Return to Shopping' link displayed in the 'Checkout-shipping' page to User",
					"'Return to Shopping' link not displayed in the 'Checkout-shipping' page to User", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_133

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify system allows the User to add 'Belk Rewards Credit Card' as Payment option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_173(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException("This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
			String searchKey = testData.get("SearchKey");
			String emailid = testData.get("EmailAddress");
			String password = testData.get("Password");

			List<String> element = Arrays.asList("nameOnCard", "CardNumber", "cvvOfCard");
			List<String> elements = Arrays.asList("toolTips");

			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);
			try {
				// Load the Home Page
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				// Navigated to SignIn page
				SignIn signIn = homePage.headers.navigateToSignIn();
				Log.message("2. Navigated to 'Belk' SignIn Page!");

				// Logging in the application
				MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
				Log.message("3. Loggedin to application with userdId- '" + emailid + "' and password- " + password);

				ShoppingBagPage shoppingBagPage = null;
				if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
					shoppingBagPage = myAccountPage.clickOnMiniCart();
					shoppingBagPage.removeItemsFromBag();
				}
				// Load the SearchResult Page with search keyword
				PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
				Log.message("4. Searched with keyword '" + searchKey + "' !");
				Log.message("5. Navigated to PDP Page !");
				// Selecting the Color
				String pdtDetails = pdpPage.addProductToBag();
				Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
				// Select the color
				Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
				// select the quantity
				Log.message("8. selected qty1: " + pdtDetails.split("\\|")[2]);
				// Add the product to the bag
				Log.message("9. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");
				Log.message("10. Product is added to the cart");
				pdpPage.waitForMiniCartOverlayDisappears();
				// Navigating to shopping bag
				shoppingBagPage = pdpPage.clickOnMiniCart();
				Log.message("11. Navigated to shopping bag page");
				// Navigating to shipping page
				CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage
						.clickOnCheckoutInOrderSummary(shoppingBagPage);
				Log.message("12. Navigated to checkout-Shipping page as authorized user!");
				// filling the shipping details
				checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address4");
				Log.message("13. shipping details filled successfully !");
				checkoutPage.clickOnContinueInShipping();
				checkoutPage.ContinueAddressValidationModalWithDefaults();
				// selecting the card
				checkoutPage.fillingCardDetails("NO", "card_EmptyBelkRewardsCreditCard");
				Log.message("14. selected the card as 'Belk Rewards Credit Card' ");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b>");
				Log.message("Below fields should be displayed when selecting 'Belk Rewards Credit Card' as card type");
				Log.message("Name on Card,Card Number,CVV are mandatory .");
				Log.message("Security Tooltip should be displayed while hovering mouse on 'What is This' link.");
				Log.message("<br>");
				Log.message("<b>Actual Result:</b>");
				Log.assertThat(checkoutPage.elementLayer.verifyPageElements(element, checkoutPage),
						"It displays the Name on Card,Card Number,CVV are mandatory .",
						"It is not displaying the Name on Card,Card Number,CVV are mandatory .", driver);
				checkoutPage.mouseOverOnWhatIsThis();
				Log.assertThat(checkoutPage.elementLayer.verifyPageElements(elements, checkoutPage),
						"Security Tooltip displayed while hovering mouse on 'What is This' link.",
						"Security Tooltips not displayed while hovering mouse on 'What is This' link.", driver);
				Log.testCaseResult();

			} // try

			catch (Exception e) {
				Log.exception(e, driver);

			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}

	}// TC_BELK_CART_173

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the scroll bar is enabled when more than 4 products are added in mini-cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_031(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		List<String> ScrollBar = null;
		ScrollBar = Arrays.asList("scrollbar");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.  Navigated to 'Belk' Home Page!", driver);

			SignIn signInPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked on SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccPage = signInPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);

			MiniCartPage minicartpage = myAccPage.mouseOverMiniCart();
			Log.message("4. Mouse hover on the Minicart in the Minicart Page!");
			int itemsaddedinminicart = minicartpage.getTotalItemInMinicart();
			Log.message("5. Number of Items in Minicart : " + itemsaddedinminicart);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Scroll bar should be enabled in the minicart overlay when more than 4 products are added");

			Log.assertThat(minicartpage.elementLayer.verifyPageElements(ScrollBar, minicartpage),
					"<b>Actual Result 1:</b> After adding '" + itemsaddedinminicart
							+ "' products in the minicart, Scroll bar is enabled in Minicart Overlay",
					"<b>Actual Result 1:</b> After adding '" + itemsaddedinminicart
							+ "' products in minicart, Scroll bar is disabled in Minicart Overlay, Please check the event log",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Recently added product (1st product) Image should be visible in the View bag   ");
			Log.assertThat(minicartpage.verify1stProductImageIsVisible(),
					"<b>Actual Result 2:</b> Recently added product (1st product) Image is visible in the View bag ",
					"<b>Actual Result 2:</b> Recently added product (1st product) Image is not visible in the View bag",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Except the 1st product,all the added product Image should be invisible in the View Bag ");
			Log.assertThat(minicartpage.verifyImageIsInvisibleForMorethanOneProduct(),
					"<b>Actual Result 3:</b> Except the 1st product,all the added product Image is invisible",
					"<b>Actual Result 3:</b> All the added product image is Visible", driver);
			Log.message("<br>");
			minicartpage.clickon2ndProducttoggle();
			Log.message("6. Clicked on 2nd Product Toggle");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> When the User clicks the arrow, the product Image should be displayed for the additional products in the mini-cart");

			Log.assertThat(!minicartpage.verifyImageIsInvisibleForMorethanOneProduct(),
					"<b>Actual Result 4:</b> When the User clicks the arrow, the product Image is displayed for the additional products in the mini-cart",
					"<b>Actual Result 4:</b> When the User clicks the arrow, the product Image is not displayed for the additional products in the mini-cart",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 5:</b> Only 4 products should be visible in mini-cart ");
			Log.assertThat(
					minicartpage.verify5thProductIsNotVisibleInTheViewBag()
							&& minicartpage.verify4thProductIsVisibleIntheViewBag(),
					"<b>Actual Result 5:</b> Only 4 products is visible in mini-cart",
					"<b>Actual Result 5:</b> More than 4 Product is visible", driver);
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}// TC_BELK_CART_031//

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = " Verify system disables the Scroll bar in Mini Cart page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_041(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		List<String> ScrollBar = null;
		ScrollBar = Arrays.asList("scrollbar");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccPage = signInPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered Credentials in the Signin Page!");

			MiniCartPage minicartpage = myAccPage.mouseOverMiniCart();
			Log.message("4. Mouse hovered the Minicart in the Minicart Page!");

			int itemsaddedinminicart = minicartpage.getTotalItemInMinicart();
			Log.message("5. Number of items in Minicart is : " + itemsaddedinminicart);

			if (itemsaddedinminicart < 4) {
				Log.message("<br>");
				Log.message(
						"<b>Expected Result:</b> After adding less than 4 products in the minicart Scrollbar should be disabled ");
				Log.assertThat(minicartpage.elementLayer.verifyPageElementsDoNotExist(ScrollBar, minicartpage),
						"<b>Actual Result:</b>  After adding  less than 4 products in minicart, Scroll bar is disabled in Minicart Overlay",
						"<b>Actual Result:</b>  After adding  less than 4 products  in minicart, Scroll bar is enabled in Minicart Overlay, Please check the event log",
						driver);
			}
		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch

		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_041

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the Order Summary details in Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_080(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> orderdetails = null;
		orderdetails = Arrays.asList("lblOrderMerchandiseTotal", "lblOrderCouponSavings", "lblOrderSurcharge",
				"lblOrderShippingAmount", "lblOrdertotal");
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to  Pdp Page!");

			pdppage.addProductToBag();
			Log.message("3. Selected product is added to bag from PdpPage!");

			ShoppingBagPage shoppingbagpage = pdppage.minicart.navigateToBag();
			Log.message("4. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag("14549496");
			Log.message("5. Coupon is Applied to the Product");

			homePage.headers.searchAndNavigateToPDP("0438566800305 ");
			pdppage.addProductToBag();
			Log.message("6. Selected product is added to bag from PdpPage!");

			pdppage.minicart.navigateToBag();
			Log.message("7. Navigated to Shoppingbagpage!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Order Details should be displayed in ShoppingbagPage");
			Log.assertThat(shoppingbagpage.elementLayer.verifyPageElements(orderdetails, shoppingbagpage),
					"<b>Actual Result:</b> Order details are displayed in the ShoppingBagpage ",
					"<b>Actual Result:</b> Order details are not displayed in the ShoppingBagpage as expected, Please check the event log",
					driver);
			/*
			 * ArrayList<String> orderdetail =
			 * shoppingbagpage.getTextfromodrerdetails(); Log.message(
			 * "6.Verified order summary is:" + orderdetail);
			 */
		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_080

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify ' ADD a Free gift message' is displayed  in Cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_103(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> chckbxAddFreeMsg = null;
		chckbxAddFreeMsg = Arrays.asList("lstCheckBoxAddFreeGiftMessage");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigating to searchResultpage
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Navigating to PDP Page
			PdpPage pdpPage = searchresultpage.navigateToPDP();
			Log.message("3. Navigated to  Pdp Page with randomly selected product");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");
			// Navigating to shopping BAg Page
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. By clicking on 'minicart'icon , It  Navigated to Shoppingbagpage!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> 'Add a Free Gift Message' checkbox should be displayed in the 'ShoppingBag' page");
			Log.assertThat(shoppingbagpage.elementLayer.verifyPageListElements(chckbxAddFreeMsg, shoppingbagpage),
					"<b>Actual Result:</b> 'Add a Free Gift Message' is  displayed in the ShoppingBag page ",
					"<b>Actual Result:</b> 'Add a Free Gift Message' is not displayed in the ShoppingBag page as expected, Please check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {

			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_103

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify AddGiftBox checkbox is disabled by default", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_104(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		List<String> chckbxAddFreeMsg = null;
		chckbxAddFreeMsg = Arrays.asList("chckboxAddfreemsg");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigating to SearchResultPage
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Navigating to PDP Page
			PdpPage pdpPage = searchresultpage.navigateToPDP();
			Log.message("3. Navigated to PDP Page with randomly selected product");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			// clicking on 'Minicart' Icon
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. By clicking on 'Minicart' icon , It Navigated to 'ShoppingBag' page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By default, system unchecks the 'Add a Free Gift Message' checkbox.");
			Log.assertThat(!shoppingbagpage.verifyGiftMessage(0),
					"<b>Actual Result:</b> 'Add a Free Gift Message' check box is unchecked by default   in the ShoppingBagpage ",
					"<b>Actual Result:</b> 'Add a Free Gift Message' check box is enabled in the Shoppingbagpage, Please check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {

			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_104

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the number of items in Mini Cart is shown.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_029(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.  Navigated to 'Belk' Home Page!", driver);
			int count = 2;
			while (count < 5) {

				PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
				Log.message(count + "a. Searched with keyword '" + searchKey + "' and navigated to search result Page");
				Log.message(count + "b. Navigated to Pdp Page!");
				Log.message(count + "c. Selecting Size, Color, Quantity!");
				String pdtDetails = pdpPage.addProductToBag();
				Log.message("    Selected size : " + pdtDetails.split("\\|")[1]);

				Log.message("    Selected color : " + pdtDetails.split("\\|")[0]);

				Log.message("    Selected quantity : " + pdtDetails.split("\\|")[2]);

				Log.message("    Product added to Minicart!");
				Log.message(count + "c. Product is added to ShoppingBag from pdpPage!");
				count++;
				pdpPage.waitForMiniCartOverlayDisappears();
			}
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			minicartpage.mouseOverMiniCart();
			int quantityCountInMinicartPopup = minicartpage.getQuantityOfAllProductInMinicart();
			Log.message("5. Quantity '" + quantityCountInMinicartPopup + "' is displayed in the minicart product list");
			int quantityCountInMinicartIcon = Integer
					.parseInt(minicartpage.headers.getQuantityFromBag().replace(" item(s)", ""));
			Log.message("6. Quantity '" + quantityCountInMinicartIcon + "' is displayed in the minicart icon");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Mini cart should be displayed with the number of items in it ");
			Log.assertThat((quantityCountInMinicartIcon == quantityCountInMinicartPopup),
					"<b>Actual Result:</b>  No of items in the minicart is displayed as " + quantityCountInMinicartIcon,
					"<b>Actual Result:</b>  No of items in the cart is not displayed as " + quantityCountInMinicartIcon
							+ ", Please check the event log",
					driver);
		} // try
		catch (Exception e) {
			Log.exception(e, driver);

		} finally {
			Log.endTestCase();
			driver.quit();

		} // finally
	}// TC_BELK_CART_029

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "The Product Recommendations details should be available below the Coupons section in the empty cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_050(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = myaccount.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			CouponsPage couponsPage = bagPage.clickNeedCoupon();
			Log.message((i++) + ". Clicked on NeedCoupon link and navigated to couponPage !");

			couponsPage.clickAddCouponByIndex(1);
			Log.message((i++) + ". Clicked on Add coupon to bag !");

			couponsPage.miniCartPage.navigateToBag();
			Log.message((i++) + ". Navigated to Shopping bag page !");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> When a coupon has been applied to the cart, but there are no products in the cart, the coupon section should display below the empty cart messaging.");

			Log.assertThat(bagPage.elementLayer.verifyPageElements(Arrays.asList("CouponContainsInCart"), bagPage),
					"<b>Actual Result : </b> When a coupon has been applied to the cart, but there are no products in the cart, the coupon section displayed below the empty cart messaging.",
					"<b>Actual Result : </b> When a coupon has been applied to the cart, but there are no products in the cart, the coupon section not displayed below the empty cart messaging.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_050

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify 'Continue Shopping' link in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_075(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile") {
			throw new SkipException("This testcase is only applicable for desktop");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			String homeUrl = driver.getCurrentUrl();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to Search Result Page
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page.");

			// Navigate to Product description Page
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message("3. Navigated to PDP page with the selected product.");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("7. Product is added to the cart");

			// Navigating to 'Shopping Bag' Page
			ShoppingBagPage bagpage = pdpPage.minicart.navigateToBag();
			Log.message("8. By clicking on 'MiniCart' icon , It Navigated to Shopping Bag page!");

			// Navigating to HomePage
			bagpage.clickContinueShopping();
			Log.message("9. Clicked on 'ContinueShopping' link in the 'Shopping Bag' Page");

			// Getting HomePage URL
			String returnUrl = driver.getCurrentUrl().split("/home")[0];

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After clicking on 'Continue Shopping' link in the 'ShoppingBag' Page, It should navigate to the 'HomePage'");
			Log.assertThat(returnUrl.equalsIgnoreCase(homeUrl),
					"<b>Actual Result:</b>  After clicking on 'Continue Shopping' link in the 'ShoppingBag' Page, It is navigated to the 'HomePage'",
					"<b>Actual Result:</b>  After clicking on 'Continue Shopping' link in the 'ShoppingBag' Page, It is not navigated to the 'HomePage'",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_075

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Checkout button behavior in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_076(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("btnCheckoutAsGuest", "txtUserName", "txtPassWord");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigate to Search Result Page
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page.");

			// Navigate to Product description Page
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message("3. Navigated to PDP page with the selected product.");

			// selecting color
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. Selected size : " + pdtDetails.split("\\|")[1]);

			Log.message("5. Selected color : " + pdtDetails.split("\\|")[0]);

			Log.message("6. Selected quantity : " + pdtDetails.split("\\|")[2]);

			Log.message("7. Product added to Minicart!");

			// Clicking on 'MiniCart' icon
			ShoppingBagPage bagpage = pdpPage.clickOnMiniCart();
			Log.message("8. By clicking on 'MiniCart' icon , It Navigated to Shopping Bag page!");

			// Click on Check Out Button
			SignIn signin = (SignIn) bagpage.clickOnCheckoutInOrderSummary(bagpage);
			Log.message(
					"9. By clicking on 'Checkout' Button in Order Summary Section in Shopping Bag Page, It Navigated to SignIn Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> 'Checkout as Guest' button ,Username & Password fields should be displayed in 'CheckOutLogin' Page.");

			Log.assertThat(signin.elementLayer.verifyPageElements(element, signin),
					"<b>Actual Result:</b> 'Checkout as Guest' button ,Username & Password fields is displayed in 'CheckOutLogin' Page.",
					"<b>Actual Result:</b> 'Checkout as Guest' button ,Username & Password fields is not displayed in 'CheckOutLogin' Page.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_076

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Return to Shopping link behavior in the Checkout page for the Non-Multi-ship option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_134(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Navigate to Search Result Page
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Click on add to bag
			pdpPage.addProductToBag();
			Log.message("6. Product Added to Shopping Bag!");

			// Click on shopping bag
			ShoppingBagPage bagpage = pdpPage.clickOnMiniCart();
			String currentUrl = driver.getCurrentUrl();
			Log.message("7. Navigated to Shopping Bag Page!");

			// Click on Check Out Button
			CheckoutPage checkoutPage = (CheckoutPage) bagpage.clickOnCheckoutInOrderSummary(bagpage);
			Log.message(
					"8. Navigated to Checkout Page By clicking Checkout Button in Order Summary Section in Shopping Bag Page!");

			// Click on Return to Bag
			bagpage = checkoutPage.clickReturnToShoppingBag();
			String returnUrl = driver.getCurrentUrl();
			Log.message("9. Navigated return Back to Shopping bag Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should be on Home Page after clicking 'Continue Shopping' link on Shopping Bag Page");
			Log.assertThat(returnUrl.equalsIgnoreCase(currentUrl),
					"<b>Actual Result:</b>User navigated on Home Page after clicking 'Continue Shopping' link on Shopping Bag Page.",
					"<b>Actual Result:</b>User not navigated on Home Page after clicking 'Continue Shopping' link on Shopping Bag Page.",
					driver);

			Log.testCaseResult();

			bagpage.removeItemsFromBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_134

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows User to add multiple shipping Address in Shipping Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_143(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lnkAddInShipToMultipleAddress", "lnkEditInShipToMultipleAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			// Load sign in page
			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);

			// Navigate to Search Result Page
			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("4. Navigated to Search Result Page for searchKey: '" + searchKey + "'");

			// Navigate to Product description Page
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message("5. Navigated to Product description Page");

			// Click on add to bag
			pdpPage.selectQtyByIndex(2);
			pdpPage.addProductToBag();
			Log.message("6. Product Added to Shopping Bag!");

			// Click on shopping bag
			ShoppingBagPage bagpage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to Shopping Bag Page!");

			// Click on Check Out Button
			CheckoutPage checkoutPage = (CheckoutPage) bagpage.clickOnCheckoutInOrderSummary(bagpage);
			Log.message("8. Clicked on the 'Checkout' button and navigated to Checkout page!");

			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("9. Clicked on 'YES' Button in Ship to Multiple Address Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b> MultiShip Checkout Shipping Address page should be displayed for each item should have quantity as '1' min");
			Log.assertThat((checkoutPage.getValueFromQuantityInOrderSummary() >= 1),
					"<b>Actual Result-1:</b> MultiShip Checkout Shipping Address pagedisplayed for each item should have quantity as '1' min ",
					"<b>Actual Result-1:</b> MultiShip Checkout Shipping Address page not displayed for each item should have quantity as '1' min ",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b> 'Add' and 'Edit' link should displayed in the MultiShip Checkout Shipping page.");
			Log.assertThat(checkoutPage.elementLayer.verifyPageListElements(element, checkoutPage),
					"<b>Actual Result-2:</b> 'Add' and 'Edit' link is displayed in the MultiShip Checkout Shipping page!",
					"<b>Actual Result-2:</b> 'Add' and 'Edit' link is not displayed in the MultiShip Checkout Shipping page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_143

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays the Footer content.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_146(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		List<String> element = Arrays.asList("checkoutFooterMiddle");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			ShoppingBagPage bagpage = myAccountPage.headers.NavigateToBagPage();
			bagpage.removeAddedCartItemsInShoppingBag();

			// Navigate to Search Result Page
			SearchResultPage searchResultPage = bagpage.headers.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page for searchKey: " + searchKey);

			// Navigate to Product description Page
			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message("3. Navigated to Product description Page");

			// Click on add to bag
			pdpPage.addProductToBag();
			Log.message("4. Item added to Shopping Bag!'");

			// Click on shopping bag
			pdpPage.waitForMiniCartOverlayDisappears();
			bagpage = pdpPage.clickOnMiniCart();

			Log.message("5. Navigated to Shopping Bag Page!");

			// Click on Check Out Button
			CheckoutPage checkoutPage = (CheckoutPage) bagpage.clickOnCheckoutInOrderSummary(bagpage);
			Log.message("6. Navigated to Checkout Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should be on Checkout Login Screen and should see buttons,'Checkout as Guest' button ,Username & Password.");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(element, checkoutPage),
					"<b>Actual Result:</b>User is in Checkout Login Screen and seeing buttons'Checkout as Guest' button,Username & Password.",
					"<b>Actual Result:</b>User not in Checkout Login Screen and not seeing buttons'Checkout as Guest' button,Username & Password.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_146

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify User can edit from Mini Cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_086(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load SignIn Page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Signing Into Account
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");

			// Getting ProductName from BreadCrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("10. Product is added to the cart");

			// clicking on 'Minicart' icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. After Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// clicking on EditDetails
			shopingBagPage.clickEditDetails(productName);
			Log.message("12. After Clicking on 'Edit Details' link, It  Navigated to 'UpdateShoppingBag' Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> After Clicking On 'Edit Dtails', the 'UpdateShoppingBag' button should be displayed in the EditDetails PopUp ");

			Log.softAssertThat(
					shopingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnUpdateShoppingBag"),
							shopingBagPage),
					"<b>Actual Result1:</b> After Clicking On 'Edit Dtails', the 'UpdateShoppingBag' button is displayed in the EditDetails PopUp",
					"<b>Actual Result1:</b> After Clicking On 'Edit Dtails', the 'UpdateShoppingBag' button is not displayed in the EditDetails PopUp",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result2:</b> The Product name should be disabled in the 'EditDetails' PopUp");

			Log.softAssertThat(!(shopingBagPage.getElementType().equals("a")),
					"<b>Actual Result2:</b> The Product name is disabled in the ''EditDetails' PopUp",
					"<b>Actual Result2:</b> The Product name is not disabled in the 'EditDetails' PopUp", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_086

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify User can edit from Mini Cart screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_088(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load SignIn Page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Signing Into Account
			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting ProductName from the BreadCrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);

			Log.message("10. Product is added to the cart");

			// Navigating to shoppingbag Page
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon,It Navigated to 'Shopping Bag' Page");

			// clicking on Editdetails
			shopingBagPage.clickEditDetails(productName);
			Log.message("12. By Clicking on 'Edit Details' link, It  Navigated to 'EditDetails' PopUp");

			// updating Quantity
			String UpadatedQtyInUpdateShoppingBag = shopingBagPage.selectQuantity();
			Log.message("13. Selected quantity:'" + UpadatedQtyInUpdateShoppingBag
					+ "' from the Quantity dropdown in the 'EditDetails' PopUp");
			shopingBagPage.clickUpdateShoppingBag();

			// Getting updated quantity from shopping bag page
			int productIndex = shopingBagPage.getIndexByProductName(productName);
			String updatedQtyInShoppingBag = shopingBagPage.getQuantityValue(productIndex);
			Log.message("14. After Editing the quantity, the  updated Quantity:'" + updatedQtyInShoppingBag
					+ "' in the Shopping Bag Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>The MiniCart Product Count should be updated with the selected quantity.");

			Log.softAssertThat((updatedQtyInShoppingBag.contains(UpadatedQtyInUpdateShoppingBag)),
					"<b>Actual Result:</b> The Product Count is updated with the selected quantity.",
					"<b>Actual Result:</b> The Product Count is not updated with the selected quantity.", driver);

			shopingBagPage.removeAddedCartItemsInShoppingBag();
			Log.event("Items Removed from bag");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_088

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify that when the user changes the color then the product information is updated accordingly.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_089(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load SignIn Page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// SignIn To Account
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Navigating to PDP page
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting ProductName from Breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("10. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			// click on 'Minicart' icon
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon,It Navigated to 'Shopping Bag' Page");

			// click on EditDetails
			shoppingBagPage.clickEditDetails(productName);
			Log.message("12. By Clicking on 'Edit Details' link, It  Navigated to 'EditDetails' PopUp");

			// Updating color
			String UpadatedColor = shoppingBagPage.selectColor();
			Log.message(
					"13. Selected Color:'" + UpadatedColor + "' from the Color swatches in the 'EditDetails' PopUp");

			shoppingBagPage.clickUpdateShoppingBag();
			Log.message("14. Clicked on 'UpdateShoppingBag' Button and updated the color of the product");

			// Getting updating color from shopping bag page
			int productIndex = shoppingBagPage.getIndexByProductName(productName);
			String updatedColorInShoopingBag = shoppingBagPage.getColorValue(productIndex);
			Log.message("15. After Editing the details, the  updated Color:'" + updatedColorInShoopingBag
					+ "' in the Shopping Bag Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b>The Selected ProductColor in 'EditDetails' PopUp, should be Updated in 'ShoppingBag' page");

			Log.softAssertThat(updatedColorInShoopingBag.contains(UpadatedColor),
					"<b>Actual Result1:</b>The Selected ProductColor in 'EditDetails' PopUp, is Updated in 'ShoppingBag' page.",
					"<b>Actual Result1:</b>The Selected ProductColor in ''EditDetails' PopUp, is  not Updated in 'ShoppingBag' page.",
					driver);

			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.event("Items Removed from bag");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_089

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Edit page is properly closed and when clicking Cancel or Esc button.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_091(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> locator = Arrays.asList("editDetailsPopup");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("4. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("8. Product is added to the cart");

			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. By Clicking on 'MiniCart' Icon,It Navigated to 'Shopping Bag' Page");

			new ShoppingBagPage(driver);

			shopingBagPage.clickEditDetails(productName);
			Log.message("10. By Clicking on 'Edit Details' link, It  Navigated to 'UpdateShoppingBag' Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> After Clicking on 'Cancel'  in the EditDetails PopUP, the EditDetails POpUp Should be closed");

			shopingBagPage.clickOnEditDetailsCancel();
			Log.assertThat((shopingBagPage.elementLayer.verifyPageElementsDoNotExist(locator, shopingBagPage)),
					"<b>Actual Result1:</b> After Clicking on 'Cancel'  in the EditDetails PopUP, the EditDetails PopUp is closed",
					"<b>Actual Result1:</b> After Clicking on 'Cancel'  in the EditDetails PopUP, the EditDetails PopUp is not closed",
					driver);

			if (Utils.getRunPlatForm().equals("desktop")) {
				shopingBagPage.clickEditDetails(productName);
				Log.message("<br>");
				Log.message(
						"<b>Expected Result2:</b> After pressing the 'ESC' key on the keyboard, the EditDetails PopUp Should be closed");

				shopingBagPage.closeEditDetailsByEsc();
				Log.assertThat((shopingBagPage.elementLayer.verifyPageElementsDoNotExist(locator, shopingBagPage)),
						"<b>Actual Result2:</b> After pressing the 'ESC' key on the keyboard, the EditDetails PopUp is closed",
						"<b>Actual Result2:</b> After pressing the 'ESC' key on the keyboard, the EditDetails PopUp is not closed",
						driver);
			}
			shopingBagPage.clickEditDetails(productName);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result3:</b> By clicking anywhere outside the EditDetails popup, the EditDetails PopUp Should be closed");
			shopingBagPage.closeEditDetilsByClickingOtherElement();
			Log.assertThat((shopingBagPage.elementLayer.verifyPageElementsDoNotExist(locator, shopingBagPage)),
					"<b>Actual Result3:</b> By clicking anywhere outside the EditDetails popup, the EditDetails PopUp is closed",
					"<b>Actual Result3:</b> By clicking anywhere outside the EditDetails popup, the EditDetails PopUp is not closed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_091

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system display Add Gift Box checkbox.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_100(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> chckbxAddgiftbox = null;
		chckbxAddgiftbox = Arrays.asList("chckboxAddagiftboxText");
		String txtboxandribbon = "A gift box & ribbon are included,";
		String txtitemisnotwrapped = "but the item will not be gift wrapped.";
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");

			PdpPage pdppage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> System should display 'Add a Free Gift Box' checkbox");
			Log.assertThat(shoppingbagpage.elementLayer.verifyPageElements(chckbxAddgiftbox, shoppingbagpage),
					"<b>Actual Result 1:</b> 'Add a Gift Box' Check box is  displayed in the ShoppingBagpage ",
					"<b>Actual Result 1:</b> 'Add a  Gift Box' Check box is not displayed  in the Shoppingbagpage, PLease Check the Log event ");

			String pricesplit = shoppingbagpage.getAddGiftBoxPrice();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Price should be displayed in $ x.xx format");
			Log.assertThat((pricesplit.split("\\.")[1].length() == 2),
					"<b>Actual Result 2:</b> Price is displayed in $ x.xx format",
					"<b>Actual Result 2:</b> Price is not displayed in $ x.xx format, Please Check the Log event");

			String txtboxandribbon1 = shoppingbagpage.getTextfromAddGiftBoxandRibbon();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Below 'Add Gift Box' checkbox 'Box and Ribbon are included' should be shown");
			Log.assertThat(txtboxandribbon1.equals(txtboxandribbon),
					"<b>Actual Result 3:</b> Below the 'Add Gift Box' checkbox 'Box and Ribbon are included' message is shown",
					"<b>Actual Result 3:</b> Below the 'Add Gift Box' checkbox 'Box and Ribbon are included' message is not shown, Please Check the Log event ");

			String txtItemnotwrapped = shoppingbagpage.getTextfromAddGiftItemsNotWrapped();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> Below 'Add Gift Box' checkbox 'Item is not wrapped' should be shown");
			Log.assertThat(txtItemnotwrapped.equals(txtitemisnotwrapped),
					"<b>Actual Result 4:</b> Below the 'Add Gift Box' checkbox 'Item is not wrapped' message is shown",
					"<b>Actual Result 4:</b> Below the Add Gift Box checkbox 'Item is not wrapped' message is not shown, Please Check the Log event");

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_100

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify User can select or unselect Add Gift Box checkbox.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_101(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");
			PdpPage pdppage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");
			Log.message("4. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By default, system unchecks the 'Add a  Gift Box' checkbox.");
			Log.assertThat(!shoppingbagpage.isAddGiftCheckBoxSelected(),
					"<b>Actual Result 1:</b> 'Add a Gift Box' Check box is unchecked by default  in the ShoppingBagpage ",
					"<b>Actual Result 1:</b> 'Add a  Gift Box' is enabled in the Shoppingbagpage'", driver);
			shoppingbagpage.checkAddGiftBox();

			Log.message("8. Selected 'Add a GiftBox CheckBox' in the shoppingBag page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b>  User should be allowed to select the 'Add Gift Box' checkbox in the line item of the product.");
			Log.assertThat(shoppingbagpage.isAddGiftCheckBoxSelected(),
					"<b>Actual Result 2:</b> User is able to select the 'Add Gift Box' checkbox in the line item of the product. ",
					"<b>Actual Result 2:</b> User is not able to select the 'Add Gift Box' checkbox in the line item of the product. ",
					driver);
			String pricesplit = shoppingbagpage.getAddGiftBoxPrice();
			float giftboxprice = Float.parseFloat(pricesplit);
			Log.message("9. Total price of GiftBox is:" + giftboxprice + " in line items ");
			String totaladdgiftboxpricefromordersummary = shoppingbagpage.getpriceofgiftboxfromordersummary();
			float giftboxpricefromodersummary = Float.parseFloat(totaladdgiftboxpricefromordersummary);
			Log.message("10. Total price of giftbox is:" + giftboxpricefromodersummary + "in the orrder summary");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Gift box price should be calculated for each and every product in the line item.");
			Log.assertThat((giftboxprice == giftboxpricefromodersummary),
					"<b>Actual Result 3:</b> Gift box price is calculated for each and every product in the line item",
					"<b>Actual Result 3:</b> Gift box price is not  calculated for each and every product in the line item, Please check the Log event");
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_101

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Wish list Login page is displayed when guest user tries to add a product to 'Wish List'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_115(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigated to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Getting the ProductName from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("4. Selected ProductName:'" + productName + "' from the 'PDP' page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product is added to the cart");

			// Navigating to 'ShoppingBag' page,after clicking on 'MiniCart'
			// icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Clicking on 'AddToWishList' link
			shopingBagPage.clickOnAddToWishList(productName);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> After Clicking on 'AddToWishList'  as a Guest user in ShoppingBag page, It should navigate to the WishListLogIn Page. ");
			WishListPage wishListPage = new WishListPage(driver).get();

			Log.assertThat(wishListPage != null,
					"<b>Actual Result :</b> By Clicking on 'AddToWishlist' Button is navigated to WishListLogIn page ",
					"<b>Actual Result :</b> By Clicking on 'AddToWishlist' Button link is Not navigated to WishListLogIn page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_115

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system add the product to a 'Wish list'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_116(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String txtWishList = "Wish List";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("10. Product is added to the cart");

			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			shopingBagPage.clickOnAddToWishList(productName);
			Log.message("12. Item Added to WishList");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToWishList' as a Signed User in ShoppingBag page, It should Add Item To wishList ");
			String wishlist = shopingBagPage.checkItemAddedToWishList(productName);
			Log.softAssertThat(txtWishList.contains(wishlist),
					"<b>Actual Result:</b> By Clicking on 'AddToWishList' as a Signed User in ShoppingBag page, It Adds Item To wishList ",
					"<b>Actual Result:</b> By Clicking on 'AddToWishList' as a Signed User in ShoppingBag page, It is not Adding Item To wishList",
					driver);

			WishListPage wishListPage = shopingBagPage.headers.navigateToWishList();
			wishListPage.clickDeleteItem();
			Log.event(" Items Removed from WishList");
			pdpPage.clickOnMiniCart();
			shopingBagPage.removeAddedCartItemsInShoppingBag();
			Log.event("Items Removed from bag");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_116

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system add the product to 'Registry' - Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_119(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Getting the productname from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("4. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("8. Product is added to the cart");

			// Navigating to 'ShoppingBag' page, after Clicking on 'MiniCart'
			// icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Clicking on 'Registry'link
			shopingBagPage.clickOnRegistry(productName);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> After Clicking on 'AddToRegistry'  as a Guest user in ShoppingBag page, It should navigate to the RegistryLogIn Page. ");
			RegistryGuestUserPage registryGuestUserPage = new RegistryGuestUserPage(driver).get();

			Log.assertThat(registryGuestUserPage != null,
					"<b>Actual Result :</b> By Clicking on 'AddToRegistry' Button is navigated to RegistryLogIn page ",
					"<b>Actual Result :</b> By Clicking on 'AddToRegistry' Button link is Not navigated to RegistryLogIn page",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_119

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system add the product to a 'Wish list'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_120(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Entering the credentials in the signIn page
			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching a product with product keyword like 'jeans'
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting the productname from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("10. Product is added to the cart");

			// Navigated to the shoppingBagPage by clicking on minicart icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Clicking on Registry Link in shopping bag page
			RegistrySignedUserPage registrySignedUserPage = shopingBagPage.clickOnRegistry(productName);
			Log.message("12. By Clicking on 'Registry', It Navigated to'Registry' Page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'AddToRegistry' as a Signed User in ShoppingBag page, It should Add Item To Registry ");
			Log.softAssertThat(registrySignedUserPage.checkItemAddedToRegistry(productName),
					"<b>Actual Result:</b> By Clicking on 'AddToRegistry' as a Signed User in ShoppingBag page, It Adds Item To Registry ",
					"<b>Actual Result:</b> By Clicking on 'AddToRegistry' as a Signed User in ShoppingBag page, It is not Adding Item To Registry",
					driver);

			registrySignedUserPage.clickDeleteItem();
			registrySignedUserPage.clickMiniCart();
			shopingBagPage.removeAddedCartItemsInShoppingBag();
			Log.event("Items Removed from bag");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_120

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify 'Add to Registry' link is displayed for eligible products in Cart page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_121(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Entering the credentials in the signIn page
			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching a product with product keyword like 'jeans'
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting the productname from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("10. Product is added to the cart");

			// Navigated to the shoppingBagPage by clicking on minicart icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Getting the the product name by index
			int productIndex = shopingBagPage.getIndexByProductName(productName);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The 'AddToRegistry' link Should be displayed  in the 'ShoppingBag' page ");
			Log.softAssertThat(shopingBagPage.checkAddToRegistry(productIndex),
					"<b>Actual Result:</b> The 'AddToRegistry' link is displayed  in the 'ShoppingBag' page ",
					"<b>Actual Result:</b> The 'AddToRegistry' link is not displayed  in the 'ShoppingBag' page ",
					driver);

			shopingBagPage.removeAddedCartItemsInShoppingBag();
			Log.event("Items Removed from bag");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_121

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Check gift card balance link in the Product Set Gift Card details page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);

		try {
			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked on SignIn button in the header and navigated to 'SignIn' Page!", driver);

			// Entering the credentials in the signIn page
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			} else {
			}

			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("8. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			// Navigated to the shoppingBagPage by clicking on minicart icon

			pdpPage.waitForMiniCartOverlayDisappears();

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");

			// Getting UPC value of the products in the shoppingBagPage
			String itemIndex = shoppingBagPage.getIndexByUPC(pdtDetails.split("\\|")[3].trim());
			if (itemIndex.equals("no"))
				Log.fail("No items found in the shopping bag page...");

			// Checking the checkboxbutton only for the selected item
			shoppingBagPage.checkAddFreeGiftMessage(Integer.parseInt(itemIndex));
			Log.message("10. Checking the checkbox for Adding the 'free giftMessage' box for the selected product.");

			// After checking gift message box appears
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Message box should be displayed, after checking 'Add a Free Gift Message' checkbox. ");
			Log.assertThat(shoppingBagPage.verifyGiftMessage(Integer.parseInt(itemIndex)),
					"<b>Actual Result 1:</b> Message box displayed, after checking 'Add a Free Gift Message' checkbox.",
					"<b>Actual Result 1:</b> Message box not displayed, after checking 'Add a Free Gift Message' checkbox.",
					driver);
			Log.message("<br>");
			// Unchecking the checkbox for the selected product
			shoppingBagPage.checkAddFreeGiftMessage(Integer.parseInt(itemIndex));
			Log.message("11. Unchecking the checkbox for removing 'Add free giftmessage' box");

			// After unchecking gift message box should disappear
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Message box should be hidden, after unchecking 'Add a Free Gift Message' checkbox. ");
			Log.assertThat(!(shoppingBagPage.verifyGiftMessage(Integer.parseInt(itemIndex))),
					"<b>Actual Result 2:</b> Message box is hidden, after unchecking the 'Add a Free Gift Message' checkbox.",
					"<b>Actual Result 2:</b> Message box is not hidden, after unchecking the 'Add a Free Gift Message' checkbox",
					driver);

			Log.testCaseResult();
			// Removing all the added items from the shopping bag page
			shoppingBagPage.removeItemsFromBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_105

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system allows to add character in the Message box ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_106(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked on SignIn button in the header and navigated to 'SignIn' Page!", driver);

			// Entering the credentials in the signIn page
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);
			ShoppingBagPage shoppingBagPage = null;
			PdpPage pdpPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			} else {
				pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			}
			Log.message("4. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("8. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			// Navigated to the shoppingBagPage by clicking on minicart icon

			pdpPage.waitForMiniCartOverlayDisappears();

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");

			// Getting UPC value of the products in the shoppingBagPage
			String itemIndex = shoppingBagPage.getIndexByUPC(pdtDetails.split("\\|")[3].trim());
			if (itemIndex.equals("no"))
				Log.fail("No items found in the shopping bag page...");

			// Checking the checkbox to add free giftMessage only for the
			// selected item
			shoppingBagPage.checkAddFreeGiftMessage(Integer.parseInt(itemIndex));
			Log.message("10. Checking the checkbox for Adding the 'free giftMessage' box for the selected product.");

			// After checking gift message box appears and the watermark is
			// displaying below box
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> Verify system should displayed the '250 character remaining' watermark text below the Message box");

			Log.assertThat(shoppingBagPage.verifyWaterMarkByIndex(Integer.parseInt(itemIndex)),
					"<b>Actual Result 1:</b> 250 characters remaining watermark text is displayed below the Message box",
					"<b>Actual Result 1:</b> 250 characters remaining watermark text is not displayed below the Message box",
					driver);

			// Adding 250 characters long message in the message box and taking
			// the count after and before adding.
			String charCountBefore = shoppingBagPage.getTextFromRemainCharInGiftMessage(Integer.parseInt(itemIndex));
			shoppingBagPage.putTextInGiftMessageBox(Integer.parseInt(itemIndex));
			String charCountAfter = shoppingBagPage.getTextFromRemainCharInGiftMessage(Integer.parseInt(itemIndex));
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> System should decrease the characters count,after entering the characters in the Message box.");
			Log.assertThat(!(charCountBefore.equals(charCountAfter)),
					"<b>Actual Result 2:</b>System decrease the characters count,after entering the character in the Message box!",
					"<b>Actual Result 2:</b>System not decrease the characters count,after entering the character in the Message box!",
					driver);

			Log.testCaseResult();

			// Removing the all the added items from the shopping bag page
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_106

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows to add character in the Message box ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_137(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		String cardDetails = "card_Visa";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching product in my account page with a productId
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			// Add the product to the bag
			Log.message("8. Product is added to the cart");
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("9. Clicking on Minicart icon is navigated to the bag page");
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Standard", address);
			Log.message("11. Shipping details filled in shipping form!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// After continue, Fill billing Details in the billing page
			checkoutPage.fillingBillingDetailsAsSignedInUser("YES", "YES", "valid_address1");
			Log.message("12. Billing details filled in billing form!", driver);

			checkoutPage.fillingCardDetails("NO", cardDetails);
			Log.message("13. Card Details filled in billing form!", driver);
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicking on Continue in the Billing Page.");
			// Verifying Elements On the Place Order Page: Modules with EditLink
			// and Place order Button
			Log.message(
					"<b>Expected Result 1:</b>Verify system displays Place Order Button the Checkout steps for the Non-MultiShip option.");
			if (Utils.getRunPlatForm().equals("desktop")) {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutPage),
						"<b>Actual Result 1:</b> 'Place Order Buttion' displayed in the place order page",
						"<b>Actual Result 1:</b> 'Place Order Buttion' not displayed in the place order page");
			} else {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutPage),
						"<b>Actual Result 1:</b> 'Place Order Buttion' displayed in the place order page",
						"<b>Actual Result 1:</b> 'Place Order Buttion' not displayed in the place order page");
			}
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingModule"), checkoutPage)
							&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingModuleEditLink"),
									checkoutPage),
					"<b>Actual Result 2:</b> 'Shipping Address Details Section with Edit Link' displayed in the place order page",
					"<b>Actual Result 2:</b> 'Shipping Address Module with Edit Link' not displayed in the place order page");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingModule"), checkoutPage)
							&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingModuleEditLink"),
									checkoutPage),
					"<b>Actual Result 2:</b> 'Billing Address Details Section with Edit Link' displayed in the place order page",
					"<b>Actual Result 2:</b> 'Billing Address Module with Edit Link' not displayed in the place order page");

			Log.testCaseResult();
			BrowserActions.nap(3);
			shoppingBagPage = checkoutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_137

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify system displays the Checkout steps for the MultiShip option. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_138(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page",
					driver);

			// Searching product in my account page with a productId
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			pdpPage.selectColor();
			Log.message("6. Colour is selected randomly.");

			pdpPage.selectSize();
			Log.message("7. Size is selected randomly.");

			pdpPage.selectQtyByIndex(2);
			Log.message("8. Qty selected.");

			pdpPage.clickAddToBag();
			Log.message("9. Add product to bag from pdpPage !");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("10. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Navigate to checkout page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("12. Clicking on 'Yes' button for multi shipping");

			// Select only first address from the dropdown
			checkoutPage.selectingAddressInMultiShipDropdown();
			Log.message("13. Selecting address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("14. Clicked on continue in MultiShipping Address Page!");
			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("15. Clicked on continue in MultiShipping Method Page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("YES", "card_Amex");
			Log.message("16. Filled valid card details in MultiShipping Billing Page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on continue in MultiShipping Billing Page!");
			// Verify Elements on the Place order Page of multishipping: Modules
			// and links are verified along with the place order button
			Log.message(
					"<b>Expected Result 1:</b>Verify system displays Place Order Button the Checkout steps for the MultiShip option.");
			if (Utils.getRunPlatForm() == "desktop") {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderDesktop"),
								checkoutPage),
						"<b>Actual Result 1:</b> 'Place Order Buttion' displayed in the place order page",
						"<b>Actual Result 1:</b> 'Place Order Buttion' not displayed in the place order page");
			}
			if (Utils.getRunPlatForm() == "mobile") {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderMobile"),
								checkoutPage),
						"<b>Actual Result 1:</b> 'Place Order Buttion' displayed in the place order page",
						"<b>Actual Result 1:</b> 'Place Order Buttion' not displayed in the place order page");
			}
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingModule"), checkoutPage)
							&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingModuleEditLink"),
									checkoutPage),
					"<b>Actual Result 2:</b> 'Shipping Address Details Section with Edit Link' displayed in the place order page",
					"<b>Actual Result 2:</b> 'Shipping Address Module with Edit Link' not displayed in the place order page");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingModule"), checkoutPage)
							&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingModuleEditLink"),
									checkoutPage),
					"<b>Actual Result 2:</b> 'Billing Address Details Section with Edit Link' displayed in the place order page",
					"<b>Actual Result 2:</b> 'Billing Address Module with Edit Link' not displayed in the place order page");

			Log.testCaseResult();

			shoppingBagPage = checkoutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_138

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays the Checkout steps for the MultiShip option. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_141(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);

			// Searching product in my account page with a productId
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			String color = pdpPage.selectColor();
			Log.message("6. Selected color:" + color);

			String size = pdpPage.selectSize();
			Log.message("7. Selected size:" + size);

			pdpPage.selectQtyByIndex(3);
			Log.message("8. Quantity is selected randomly.");

			pdpPage.clickAddToBag();
			Log.message("9. Clicked on 'Add product to bag' from PDP Page!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Clicked on Minicart icon and navigated to the shopping bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked on 'Checkout' button and navigate to checkout page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("12. Clicked 'Yes' Button in Ship to Multiple Address!");

			// Select only first address from the dropdown
			checkoutPage.selectingAddressInMultiShipDropdown();
			Log.message("13. Selected the address from the Address dropdown!");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("14. Clicked on 'Continue' button in multishipping address page!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("15. Clicked on 'Continue' button in the shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("YES", "card_Visa");
			Log.message("16. Card details filled successfully!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on 'Continue' in multishipping address page!");

			// Clicking on the link will navigate page to billing page
			checkoutPage.clickReturnToBilling();
			Log.message("18. Clicked on 'Returned to Billing'!");

			// Verify system navigates the User to "Shopping bag" page by
			// checking Billing Header get enabled in billing page
			Log.message("<b>Expected Result:</b>Verify system navigated the User to 'Billing' page.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabledInMultiAddress"),
							checkoutPage),
					"<b>Actual Result:</b> 'Billing Tab' enabled in the billing page",
					"<b>Actual Result:</b> 'Billing Tab' disabled in the billing page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_141

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Edit link for Payment Method in Checkout-Place Order page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_248(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. Selected size: " + pdtDetails.split("\\|")[1]);
			// Selected color
			Log.message("5. Selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. Selected Quantity: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Product added to Shopping Bag!");
			// Navigating to ShoppingBag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Mini Cart' !");
			// Navigating to Checkoutpage
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to SignIn Page to Checkout as a Guest!");
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("10. Navigated to Checkout Page as a Guest!");
			// Filling Shipping Details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Shipping address details are entered!");
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue Button");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");
			// Filling Billing Details
			checkOutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("14. Billing address details are entered!");
			// Filling Card Details
			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("15. Entered card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on Continue Button in the Billing Page and Navigated to Place Order Page!");
			// Editing Payment Session
			checkOutPage.clickOnEditBySectionName("paymentmethod");
			Log.message("17. Clicked on Edit Link in Place Order Page and Navigated to Billing page !");

			Log.message("</br>");
			Log.message("<b>Expected Result 1 :</b>  Page should be navigated to Billing Address Page");

			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_1InBilling"), checkOutPage),
					"<b>Actual Result 1 : </b> Page navigated to Billing Address Page",
					"<b>Actual Result 1 : </b> Page is not navigated to Billing Address Page", driver);

			checkOutPage.fillingCardDetails("NO", "card_Discover", "2");
			String creditCarddrp = checkOutPage.getCreditCardTypeDropDown();
			checkOutPage.clickOnContinueInBilling();
			String creditCardInPaymentSection = checkOutPage.getCreditCardType();

			Log.message("</br>");
			Log.message("<b>Expected Result 2 :</b>  Card details should be updated");
			Log.assertThat(creditCarddrp.equals(creditCardInPaymentSection),
					"<b>Actual Result 2 : </b> Card details has been updated",
					"<b>Actual Result 2 : </b> Card details not updated", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_248

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Saved Address dropdown in Checkout-Billing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_237(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity

			Log.message("8. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("10. Navigated to 'Mini Cart' !");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Clicked Checkout button");

			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Standard", "valid_address6");
			Log.message("12. Shipping details entered in Checkout Page!");

			checkoutPage.clickOnContinueInShipping();
			Log.message("13. Clicked on Continue button in Shipping address page");

			checkoutPage.ContinueAddressValidationModalWithDefaults();

			String savedBillingAddress = checkoutPage.selectSavedBillingAddressByIndex(2);

			Log.message("14. Selected the saved address in Billing address page");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Shipping Address1 should be the default Shipping address");
			String billingAddress1 = checkoutPage.getBillingAddressOne();
			Log.assertThat(savedBillingAddress.contains(billingAddress1),
					"<b>Actual Result 1:</b> Default Shipping Address1 is default Shipping address1",
					"<b>Actual Result 1:</b> Default Shipping Address1 is not the default Shipping address1");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Shipping Address City should be the default Shipping address city");
			String billingCity = checkoutPage.getBillingAddressCity();
			Log.assertThat(savedBillingAddress.contains(billingCity),
					"<b>Actual Result 2:</b> Default Shipping Address City is default Shipping address city ",
					"<b>Actual Result 2:</b> Default Shipping Address City is not the default Shipping address city");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Shipping Address Zipcode should be the default Shipping address Zipcode");
			String billingZipCode = checkoutPage.getBillingAddressZipCode();
			Log.assertThat(savedBillingAddress.contains(billingZipCode),
					"<b>Actual Result 3:</b> Default Shipping Address Zipcode is default Shipping address Zipcode ",
					"<b>Actual Result 3:</b> Default Shipping Address Zipcode is not the default Shipping address Zipcode",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_237

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the default Shipping Address in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_226(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			AddressBookPage AddressBookPage = (AddressBookPage) myAccountPage.navigateToSection("addressbook");

			LinkedHashMap<String, String> defaultShippingDetail = AddressBookPage.getDefaultShippingAddressDetails();
			Log.message("defaultShippingDetail====" + defaultShippingDetail);
			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Select Size
			String size = pdpPage.selectSize();
			Log.message("6. Selected Size : " + size);
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// Add the product to the bag
			Log.message("9. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Navigated to 'Mini Cart' !");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Checkout button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Default Address should be selected in the 'Saved Address' dropdown");

			LinkedHashMap<String, String> shippingDetails = checkoutPage.getShippingAddressDetails();
			Log.message("shippingDetails====" + shippingDetails);

			/*
			 * String defaultAddress = checkoutPage.getDefaultShippingAddress();
			 * 
			 * Log.assertThat( defaultAddress.contains("(Default Address)"),
			 * "<b>Actual Result 1:</b> Default Address is selected in the 'Saved Address' dropdown"
			 * ,
			 * "<b>Actual Result 1:</b> Default Address is not selected in the  'Saved Address' dropdown"
			 * );
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 2:</b> Shipping Address1 should be the default Shipping address"
			 * ); String shippingAddress1 =
			 * checkoutPage.getShippingAddressOne(); <<<<<<< .mine
			 * 
			 * Log.assertThat(defaultAddress.contains(shippingAddress1), |||||||
			 * .r1929 Log.assertThat(defaultAddress.contains(shippingAddress1),
			 * ======= Log.assertThat(
			 * defaultAddress.contains(shippingAddress1), >>>>>>> .r1965
			 * "<b>Actual Result 2:</b> Default Shipping Address1 is default Shipping address1"
			 * ,
			 * "<b>Actual Result 2:</b> Default Shipping Address1 is not the default Shipping address1"
			 * );
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 3:</b> Shipping Address City should be the default Shipping address city"
			 * ); String shippingCity = checkoutPage.getShippingAddressCity();
			 * Log.assertThat( defaultAddress.contains(shippingCity),
			 * "<b>Actual Result 3:</b> Default Shipping Address City is default Shipping address city "
			 * ,
			 * "<b>Actual Result 3:</b> Default Shipping Address City is not the default Shipping address city"
			 * );
			 * 
			 * Log.message("<br>"); Log.message(
			 * "<b>Expected Result 4:</b> Shipping Address Zipcode should be the default Shipping address Zipcode"
			 * ); String shippingZipCode =
			 * checkoutPage.getShippingAddressZipCode(); Log.assertThat(
			 * defaultAddress.contains(shippingZipCode),
			 * "<b>Actual Result 4:</b> Default Shipping Address Zipcode is default Shipping address Zipcode "
			 * ,
			 * "<b>Actual Result 4:</b> Default Shipping Address Zipcode is not the default Shipping address Zipcode"
			 * , driver);
			 */

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_226

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify success message after adding product the shopping bag", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_026(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to the PLP page.
			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2. Navigated to '" + category[0] + " >> " + category[1] + "'page");
			PlpPage plppage = new PlpPage(driver).get();

			// Navigate to the PDP page
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3. Selected first product and navigated to PDP page");

			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color!");

			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size!");

			pdpPage.selectQtyByIndex(2);
			Log.message("6. selected the 2 quantity!");

			pdpPage.clickAddToBagButton();
			Log.message("7. Product added to Bag!");

			String successmessage = pdpPage.getAddToBagMessage();

			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b> A success message should be displayed in green above the first product in mini-cart opened after adding item to cart.");

			Log.assertThat(successmessage.equals("Added to Shopping Bag"),
					"<b> Actual Result: </b> The mini-cart is automatically open and the success message is "
							+ successmessage,
					"<b> Actual Result: </b> The mini-cart is not automatically open and the success message is not visible",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_026

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system update the 'Order Summary' on removal of items.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_114(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey[] = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked on SignIn button in the header and navigated to 'SignIn' Page!", driver);

			// Entering the credentials in the signIn page
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);

			PdpPage pdpPage = null;
			// Searching 2 different products like jean & T-shirts.
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				// Searching a product with product keyword
				pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message("4. Searched with keyword '" + searchKey + "' and navigated to search result Page");

				Log.message("5. Navigated to 'Pdp' Page ");

				// To add product to bag from desktop
				if (Utils.getRunPlatForm() == "desktop") {
					pdpPage.addProductToBag();

				}
				// To add product to bag from mobile
				if (Utils.getRunPlatForm() == "mobile") {

					pdpPage.addProductToBag();
					// To click on the mini cart pop_up to add another product
					// to the bag.
					pdpPage.clickOnMiniCart();
				}

			}

			ShoppingBagPage shopingbgpage = pdpPage.clickOnMiniCart();
			Log.message("6. Navigated to Shopping bag page!");

			// Getting the Estimated value before deleting the product from the
			// cart
			float estimatedval1 = shopingbgpage.getEstimatedValue();
			Log.message("7. Estimated value before deleting the product: " + estimatedval1);

			// To remove the added product from the shopping bag page by index
			// value.
			shopingbgpage.removeItemsByIntex(1);
			Log.message("8. Removing an item from the 'Shopping bag' page");

			// Getting the Estimated value after deleting the product from the
			// cart
			float estimatedval2 = shopingbgpage.getEstimatedValue();
			Log.message("9. Estimated value after deleting the product: " + estimatedval2);
			Log.message("<br>");
			Log.message(
					"<b> Expected Result : </b> 'Order Summary' should be updated,after removing the items from the Cart by using 'Remove' link.");
			Log.softAssertThat((estimatedval1) != (estimatedval2),
					"<b> Actual Result : </b> The Product get deleted from the shopping bag page and updated message is diplayed ",
					"<b> Actual Result : </b> The Product not get deleted from the shopping bag page and empty bag message is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_114

	@Test(groups = { "desktop",
			"mobile" }, description = "verify 'Limited Availability' messages displays under the Quantity drop down.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_128(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Sign in page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// To check the product with quantity stock message on the PDP
			SearchResultPage srchrsltpg = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			PdpPage pdpPage = srchrsltpg.selectProductByIndex(1);
			Log.message("5. Navigated to PDP");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Limited Stock message should be displayed on the PDP page based on the stock quantity of the product");
			// Out of stock message method
			pdpPage.outOfStockTxt();

			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtmessageoutofstock"), pdpPage),
					"<b>Actual Result: </b> Navigate to PDP and Limited Stock message is displayed  based on the stock quantity of the product",
					"<b>Actual Result: </b> Navigate to PPD and Limited Stock message is not displayed  ", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_128

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify order summary details for the added item in the Order Summary panel in Step 1 Shipping Address Screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_203(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.Navigated to 'Belk' Home Page");

			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2.Navigated to '" + category[0] + " >> " + category[1] + "' page");

			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3.Selected first product and navigated to PDP page");

			pdpPage.addProductToBag();
			Log.message("4. Color, size are selected and 'Add to shopping bag' button is clicked");

			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5. Minicart icon is clicked and navigated to Shopping bag page");

			String PrName = shoppingbagpage.PrdName();
			Log.message("7.Product name in shopping bag page is :" + PrName);

			String PrSize = shoppingbagpage.PrdSize();
			Log.message("8.Product Size in shopping bag page is :" + PrSize);

			String PrColor = shoppingbagpage.PrdColor();
			Log.message("9.Product Color in shopping bag page is :" + PrColor);

			String PrSubtotal = shoppingbagpage.getEstimatedOrderTotal();
			Log.message("10.Product Subtotal in shopping bag page is :" + PrSubtotal);

			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("11.Clicked on Checkout in Order summary Page");

			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("12.Clicked on Checkout As Guest User in Sign In Page");

			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("txtPhoneInShipping"), checkoutpage),
					"13.The page is navigated to Shipping Address screen",
					"13.The page is not navigated to Shipping Address screen");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2 :</b> Product Name added should be matched with the product displayed in Order summary section");

			Log.assertThat((checkoutpage.PrdName().contains(PrName)),
					"<b>Actual Result 2 : </b>Product Name added is matched with the product displayed in Order summary section",
					"<b>Actual Result 2 : </b>Product Name added is not matched with the product displayed in Order summary section",
					driver);

			Log.message(
					"<b>Expected Result 3 :</b> Product Size added should be matched with the product displayed in Order summary section");

			Log.assertThat((PrSize.equals(checkoutpage.PrdSize())),
					"<b>Actual Result 3 : </b>Product Size added is matched with the product displayed in Order summary section",
					"<b>Actual Result 3 : </b>Product Size added is not matched with the product displayed in Order summary section",
					driver);

			Log.message(
					"<b>Expected Result 4 :</b> Product Color added should be matched with the product displayed in Order summary section");

			Log.assertThat((PrColor.equals(checkoutpage.PrdColor())),
					"<b>Actual Result 4 : </b>Product Color added is matched with the product displayed in Order summary section",
					"<b>Actual Result 4 : </b>Product Color added is not matched with the product displayed in Order summary section",
					driver);

			Log.message(
					"<b>Expected Result 5 :</b> Product Subtotal added should be matched with the product displayed in Order summary section");
			LinkedHashMap<String, String> costDetails = checkoutpage.getCostDetailsInOrderSummary();
			String subTotal = String.valueOf(Double.parseDouble(costDetails.get("MerchandiseTotal"))
					+ Double.parseDouble(costDetails.get("EstimatedShipping"))
					+ Double.parseDouble(costDetails.get("SalesTax")));
			Log.assertThat((PrSubtotal.contains(subTotal)),
					"<b>Actual Result 5 : </b>Product subtotal added is matched with the product displayed in Order summary section",
					"<b>Actual Result 5 : </b>Product Subtotal added is not matched with the product displayed in Order summary section",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_203

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify page redirected from My Bag screen when clicking Edit link in Billing Address from Place Order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_213(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.Navigated to 'Belk' Home Page");

			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2.Navigated to '" + category[0] + " >> " + category[1] + "' page");

			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3.Selected first product and navigated to PDP page");

			pdpPage.addProductToBag();
			Log.message("4.Color, size are selected and 'Add to shopping bag' button is clicked");

			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5.Minicart icon is clicked and navigated to Shopping bag page");

			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6.Clicked on Checkout in Order summary Page");

			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7.Clicked on Checkout As Guest User in Sign In Page");

			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Express");
			Log.message("8.Entered Shipping Address Details as Guest");

			checkoutpage.clickOnContinueInShipping();
			Log.message("9.Clicked on Continue Shipping Address Details as Guest");

			checkoutpage.ContinueAddressValidationModalWithDefaults();
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("10.Entered Billing Address Details as Guest");

			checkoutpage.fillingCardDetails("Visa", "card_Visa");
			Log.message("11.Entered Card Details as Guest");

			checkoutpage.clickOnContinueInBilling();
			Log.message("12.Clicked on Continue Billing Page");

			String Firstname = checkoutpage.getFirstnameInBillingAddressInPlaceOrderScrn();
			String Lastname = checkoutpage.getLastnameInBillingAddressInPlaceOrderScrn();
			String Address1 = checkoutpage.getAddressInBillingAddressInPlaceOrderScrn();
			String City = checkoutpage.getCityInBillingAddressInPlaceOrderScrn();
			String PhoneNumber = checkoutpage.getPhoneNoInBillingAddressInPlaceOrderScrn();

			checkoutpage.clickEditBillingAddress();
			Log.message("13.Clicked on Edit link in Billing Address");

			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_1InBilling"), checkoutpage),
					"The page is navigated to Billing Address screen",
					"The page is not navigated to Billing Address screen");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Added Billing Address Firstname details should be properly displayed in Billing Address screen");

			Log.assertThat(((checkoutpage.getFirstnameInBillingAddressPage()).equals(Firstname)),
					"<b>Actual Result 1: </b> Added Billing Address Firstname details is properly displayed in Billing Address screen",
					"<b>Actual Result 1: </b>  Added Billing Address Firstname details is not displayed properly in Billing Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2:</b> Added Billing Address Lastname details should be properly displayed in Billing Address screen");

			Log.assertThat(((checkoutpage.getLastnameInBillingAddress()).equals(Lastname)),
					"<b>Actual Result 2: </b> Added Billing Lastname details is properly displayed in Billing Address screen",
					"<b>Actual Result 2: </b>  Added Billing Lastname details is not displayed properly in Billing Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 3:</b> Added Billing Address details should be properly displayed in Billing Address screen");
			Log.assertThat(((checkoutpage.getAddressInnBillingAddress()).equals(Address1)),
					"<b>Actual Result 3: </b> Added Billing Address details is properly displayed in Billing Address screen",
					"<b>Actual Result 3: </b>  Added Billing Address details is not displayed properly in Billing Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 4:</b> Added Billing Address City should be properly displayed in Billing Address screen");
			Log.assertThat(((checkoutpage.getCityInBillingAddress()).equals(City)),
					"<b>Actual Result 4: </b> Added Billing City details is properly displayed in Billing Address screen",
					"<b>Actual Result 4: </b>  Added Billing City details is not displayed properly in Billing Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 5:</b> Added Billing Address Phone should be properly displayed in Billing Address screen");
			Log.assertThat(((checkoutpage.getPhoneNoInBillingAddress()).equals(PhoneNumber)),
					"<b>Actual Result 5: </b> Added Billing Phone details is properly displayed in Billing Address screen",
					"<b>Actual Result 5: </b>  Added Billing Phone details is not displayed properly in Billing Address screen",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_213

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system updates and displays the Payment method in Payment Method Panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_215(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String[] category = testData.get("SearchKey").split("\\|");
		List<String> errorMesg = Arrays.asList("WrongCVN");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.Navigated to 'Belk' Home Page");

			homePage.headers.navigateTo(category[0], category[1]);
			Log.message("2.Navigated to '" + category[0] + " >> " + category[1] + "' page");

			PlpPage plppage = new PlpPage(driver).get();
			PdpPage pdpPage = plppage.navigateToPDP();
			Log.message("3.Selected first product and navigated to PDP page");

			pdpPage.addProductToBag();
			Log.message("4.Color, size are selected and 'Add to shopping bag' button is clicked");

			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5.Minicart icon is clicked and navigated to Shopping bag page");

			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6.Clicked on Checkout in Order summary Page");

			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7.Clicked on Checkout As Guest User in Sign In Page");

			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Express");
			Log.message("8.Entered Shipping Address Details as Guest");

			checkoutpage.clickOnContinueInShipping();
			Log.message("9.Clicked on Continue Shipping Address Details as Guest");

			checkoutpage.ContinueAddressValidationModalWithDefaults();
			checkoutpage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("10.Entered Billing Address Details as Guest");

			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("11.Entered Card Details as Guest");

			checkoutpage.ClickOnEmptyPlaceInBilling();

			checkoutpage.clickOnContinueInBilling();
			Log.message("12.Clicked on Continue Billing Page");

			checkoutpage.clickEditBillingAddress();
			Log.message("13.Clicked on Edit link in Billing Address");

			checkoutpage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("14.Updated with New Address In billing Page");

			checkoutpage.fillingCardDetails("Visa", "card_Visa");
			Log.message("15.Entered Card Details as Guest");

			checkoutpage.fillingCvn("card_WrongValue");
			Log.message("16.Entered CVN Number In Billing Page");

			checkoutpage.ClickOnEmptyPlaceInBilling();

			checkoutpage.clickOnContinueInBilling();
			Log.message("17.Clicked on Continue in Billing Page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result :</b> Warning meaasge should be displayed for Wrong CVN entered as :Invalid Security code");

			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(errorMesg, checkoutpage),
					"Proper Warning meaasge is displayed for entering Wrong CVN",
					"No Proper  Warning message displayed for entering Wrong CVN", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_215

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Order Receipt successfully displays for placed order", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_218(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String successMsg = "Thank you for your order!";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");
			Log.message("4. Selected color : '" + pdpPage.selectColor() + "' from color Swatch in the Pdp Page!");
			Log.message("5. Selected size : '" + pdpPage.selectSize() + "' from size dropdown in the Pdp Page!");
			pdpPage.clickAddToBag();
			Log.message("6. Click on Add to bag button in the Pdp Page");
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("7. Navigated to Shopping bag!");
			String ProductNameinShoppingBag = shoppingBagPage.PrdName();
			float ItemSubTotal = shoppingBagPage.getMerchandiseTotal();
			String ItemTotal = Float.toString(ItemSubTotal);
			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to checkout page!");
			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. Clicked on CheckOutAsGuest Button");

			LinkedHashMap<String, String> shippingDetails = checkoutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Standard");
			Log.message("10. Shipping Address details are entered in the Shipping Address Page!");
			List<String> indexes = new ArrayList<String>(shippingDetails.keySet());
			String ZipCode = indexes.get(5).replace("type_zipcode_", "");
			String city = indexes.get(3).replace("type_city_", "");

			checkoutPage.clickOnContinueInShipping();
			Log.message("11. Clicked on Continue Button in the Shipping page and Navigated to Billing Page !");

			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("13. Billing address details are entered in the Billing Page!");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details are entered in the Billing Page");

			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue Button in the Billing Page and Navigated to Place Order Page!");
			String CardType = checkoutPage.getCreditCardType();
			OrderConfirmationPage orderconfirmationpage = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("16. Clicked on 'Place order' button ");
			LinkedHashMap<String, String> billingdetails = orderconfirmationpage.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> productDetails = orderconfirmationpage
					.getProductDetailsInOrderSummary();

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Thank You for Your Order message should be displayed.");
			Log.assertThat(orderconfirmationpage.GetTextFromSuccessMessage().contains(successMsg),
					"<b> Actual Result 1:</b> '" + orderconfirmationpage.GetTextFromSuccessMessage()
							+ "' is displayed.",
					"<b>Actual Result 1:</b> '" + orderconfirmationpage.GetTextFromSuccessMessage()
							+ "' is not displayed.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Below Order Information header placed order details should be displayed.");
			Log.assertThat(
					orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Order Information")
							&& orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Order Number"),
					"<b>Actual Result 2:</b> Below Order Information header placed order details is displayed",
					"<b>Actual Result 2:</b> Below Order Information header placed order details is not displayed");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> Below Billing Address header Billing address details should be displayed");
			Log.assertThat(orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Billing Address")
					&& billingdetails.get("City").contains(city) && billingdetails.get("Zipcode").contains(ZipCode),
					"<b>Actual Result 3:</b> Below Billing Address header Billing address details " + billingdetails
							+ "  is displayed",
					"<b>Actual Result 3:</b> Below Billing Address header Billing address details " + billingdetails
							+ "  is not displayed");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 4:</b> Below Payment Method header placed order payment method should be displayed.");
			Log.assertThat(
					orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader().contains("Payment Method")
							&& orderconfirmationpage.GetCardType().equals(CardType),
					"<b>Actual Result 4:</b> Below Payment Method header placed order payment method '" + CardType
							+ "' is displayed.",
					"<b>Actual Result 4:</b> Below Payment Method header placed order payment method '" + CardType
							+ "' is not displayed");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 5:</b> Below Payment Details header placed order payment details should be displayed.");
			Log.assertThat(
					orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader().contains("Payment Details")
							&& orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader()
									.contains("Merchandise Total")
					&& orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader().contains("Shipping")
					&& orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader().contains("Sales Tax")
					&& orderconfirmationpage.gettextfrompaymentsummaryIncludingHeader().contains("Order Total"),
					"<b>Actual Result 5:</b> Below Payment Details header placed order payment details is displayed.",
					"<b>Actual Result 5:</b> Below Payment Details header placed order payment details is not displayed.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 6:</b> Below Shipping Address header placed order product details and shipping address details should be displayed..");
			Log.assertThat(
					orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains("Shipping Details")
							&& productDetails.get(0).get("ProductName").contains(ProductNameinShoppingBag)
							&& orderconfirmationpage.GetShippingAddress().contains(city)
							&& orderconfirmationpage.GetShippingAddress().contains(ZipCode),
					"<b>Actual Result 6:</b>  Below Shipping Address header placed order product details "
							+ productDetails + " and shipping address details " + billingdetails + "  is displayed.",
					"<b>Actual Result 6:</b>  Below Shipping Address header placed order product details "
							+ productDetails + " and shipping address details " + billingdetails
							+ "  is not displayed.");
			Log.message("<br>");
			if (Utils.getRunPlatForm() == "desktop") {
				Log.message(
						"<b>Expected Result 7:</b> Below price label Shipping Address price label should be displayed.");
				Log.assertThat(
						orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains("Price")
								&& productDetails.get(0).get("Price").contains(ItemTotal),
						"<b>Actual Result 7:</b> Below price label Shipping Address price label "
								+ productDetails.get(0).get("Price") + " is displayed.",
						"<b>Actual Result 7:</b> Below price label Shipping Address price label "
								+ productDetails.get(0).get("Price") + "is not displayed.");
			} else {
				Log.message(
						"<b>Expected Result 7:</b> Below price label Shipping Address price label should be displayed.");
				Log.assertThat(productDetails.get(0).get("Price").contains(ItemTotal),
						"<b>Actual Result 7:</b> Below price label Shipping Address price label "
								+ productDetails.get(0).get("Price") + " is displayed.",
						"<b>Actual Result 7:</b> Below price label Shipping Address price label "
								+ productDetails.get(0).get("Price") + "is not displayed.");
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_218

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays the category landing page by clicking the brand name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_254(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		List<String> shipToMultipleAddressPagelist = Arrays.asList("btnEditAddress", "btnAddAddress");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked on SignIn button in the header and navigated to 'SignIn' Page!", driver);

			// Entering the credentials in the signIn page

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigated to 'My Account' page",
					driver);

			// Searching a product with product keyword like 'jeans'

			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'" + " and Navigated to PDP");

			pdppage.selectColor();
			Log.message("5. Color swatch Selected");
			// Selecting size by index
			pdppage.selectSizeByIndex(2);
			Log.message("6. Size is Selected from size dropdown");

			// Selecting quantity by index
			pdppage.selectQtyByIndex(2);
			Log.message("7. Quantity drowdown is selected with Quantity 2");

			// Clicking on 'AddToBag'button
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to cart' button");

			// Clicking on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");

			// Clicking on checkout button in 'OrderSummary'
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Clicked on 'Checkout' button in 'OrderSummary'");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'Yes' button on the Checkout Step- 1 Shipping page.");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message("</br>");
			Log.message(
					"<b>Expected Result:</b> Multi Shipping page should be displayed after clicking on 'Yes' button on the Checkout Step- 1 Shipping page.");
			Log.assertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(shipToMultipleAddressPagelist,
							shipToMultipleAddressPage),
					"<b>Actual Result:</b> Multi Shipping page is displayed after clicking on 'Yes' button in the Checkout Step- 1 Shipping page.",
					"<b>Actual Result:</b> Multi Shipping page is not displayed after clicking on 'Yes' button in the Checkout Step- 1 Shipping page.",
					driver);
			shipToMultipleAddressPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_254

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays the category landing page by clicking the brand name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_255(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		List<String> shipToMultipleAddressPagelist = Arrays.asList("itemDetailsdiv");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigating to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			// Entering the credentials in the signIn page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");

			// Searching a product with product keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'" + " and Navigated to PDP");
			pdppage.selectColor();
			Log.message("5. Color swatch Selected");
			// Selecting size
			pdppage.selectSizeByIndex(2);
			Log.message("6. Size is selected from size dropdown");
			// Selecting Quantity
			pdppage.selectQtyByIndex(2);
			Log.message("7. Quantity drowdown is selected with Quantity 2");
			// clicked on 'AddToBag'
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to cart' button");
			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");
			// Clicking on checkout in 'OrderSummary'
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Clicked on 'Checkout' button in 'OrderSummary'");
			// Clicked on 'Yes'
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'Yes' button on the Checkout Step- 1 Shipping page.");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Product information should be displayed in the Multi Shipping Page when User click Yes button on the Checkout Step- 1 Shipping page.");
			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(shipToMultipleAddressPagelist,
							shipToMultipleAddressPage),
					"<b>Actual Result 1:</b>  Product information is displayed in the Multi Shipping Page when User click Yes button on the Checkout Step- 1 Shipping page. ",
					"<b>Actual Result 1:</b> Product information is not displayed in the Multi Shipping Page when User click Yes button on the Checkout Step- 1 Shipping page. ",
					driver);

			Log.message("</br>");
			// Selecting address from the address drop down
			shipToMultipleAddressPage.selectAdd1ByIndex(2);
			Log.message("12. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectAdd2ByIndex(4);
			Log.message("13. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();
			Log.message("<b>Expected Result 2:</b> Shipping method page should be displayed");
			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(shipToMultipleAddressPagelist,
							shipToMultipleAddressPage),
					"<b>Actual Result 2:</b>  Shipping method page is displayed ",
					"<b>Actual Result 2:</b> Shipping method page is not displayed.", driver);
			shippingMethodPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_255

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system does not displays address in the drop down- Guest user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_258(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		List<String> shipToMultipleAddressPagelist = Arrays.asList("drpAdd1");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Searching a product with product keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'" + " and Navigated to PDP");
			pdppage.selectColor();
			Log.message("3. Color swatch Selected");
			pdppage.selectSizeByIndex(2);
			Log.message("4. Size is selected from size dropdown");
			pdppage.selectQtyByIndex(2);
			Log.message("5. Quantity is selected from quantity drowdown");
			// Clicked on 'AddToBag'
			pdppage.clickAddToBag();
			Log.message("6. Clicked on 'Add to cart' button");
			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");
			// Navigating to 'SignIn' page from the shopping bag page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Clicked on 'Checkout' button in 'OrderSummary");
			// Clicked on checkout as 'Guest'
			signIn.clickOnCheckoutAsGuest();
			Log.message("9. Navigated to Checkout page as Guest!");
			CheckoutPage checkoutPage = new CheckoutPage(driver).get();
			// Clicked on 'Yes' button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"10. Navigated to 'ShipToMultipleAddress' page after Clicking 'Yes' button in the checkout shipping address page");
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Drop down should not displayed,if there is no address in the address book.");
			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElementsDoNotExist(shipToMultipleAddressPagelist,
							shipToMultipleAddressPage),
					"<b>Actual Result 1:</b> Drop down is not displayed,if there is no address in the address book.",
					"<b>Actual Result 1:</b> Drop down is displayed even if there is no address in the address book.",
					driver);

			Log.message("</br>");
			// Clicked on 'Add'address link
			shipToMultipleAddressPage.clickaddLink();
			// Filling Shiiping details as 'SignedIn' user
			String expectedshippingDetails = shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes",
					address);
			Log.message("11. Shipping details filled in the shipping address form");
			// Clicked on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("12. Clicked on 'Save' Button on Multi Shipping Address page");
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("13. Selected Use original address as entered Radio button");
			// Clicked on 'Continue' button
			shipToMultipleAddressPage.ClickContinueBtn();
			// Clicked on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14. Clicked on Save Button on Multiple Shipping Address page");

			List<String> expectedAddressList = new ArrayList<String>();
			String[] expectedAddressarr = expectedshippingDetails.split("\\|");
			expectedAddressList.add(expectedAddressarr[0]);

			int AddedAddressCount = shipToMultipleAddressPage.ListOfAddedAddressCount();
			String Actualshippingaddress = shipToMultipleAddressPage.GetAddress(1, AddedAddressCount);
			String[] addressarr = Actualshippingaddress.split("\\,");
			List<String> actualAddressList = new ArrayList<String>();
			actualAddressList.add(addressarr[1].trim());
			Log.message("expectedAddressList:" + expectedAddressList);
			Log.message("actualAddressList:" + actualAddressList);

			Boolean flag = Utils.compareTwoList(expectedAddressList, actualAddressList);
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2:</b> Addresses should be displayed as soon as the customer adds the first address.");
			Log.softAssertThat(flag, "<b>Actual Result 2:</b> Address is displayed after adding the first address.",
					"<b>Actual Result 2:</b> Address is not displayed after adding the first address.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_258

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays a link to add new address.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_259(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		List<String> shipToMultipleAddressPagelist1 = Arrays.asList("btnAddAddress");
		List<String> shipToMultipleAddressPagelist2 = Arrays.asList("drpAdd1");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Searching a product with product keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'" + " and Navigated to PDP");
			pdppage.selectColor();
			Log.message("3. Color swatch Selected");
			pdppage.selectSizeByIndex(2);
			Log.message("4.  Size is selected from size dropdown");
			pdppage.selectQtyByIndex(2);
			Log.message("5. Quantity is selected from quantity drowdown");
			// Clicked on 'AddToBag'
			pdppage.clickAddToBag();
			Log.message("6. Clicked on 'Add to cart' button");
			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");
			// Navigating to 'SignIn' page from the shopping bag page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Clicked on 'Checkout' button in 'OrderSummary");
			// Clicked on checkout as 'Guest'
			signIn.clickOnCheckoutAsGuest();
			Log.message("9. Navigated to Checkout page as Guest!");
			CheckoutPage checkoutPage = new CheckoutPage(driver).get();
			// Clicked on 'Yes' button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"10. Navigated to 'ShipToMultipleAddress' page after Clicking 'Yes' button in the checkout shipping address page");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Add' new address link should be displayed.");
			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(shipToMultipleAddressPagelist1,
							shipToMultipleAddressPage),
					"<b>Actual Result 1:</b> 'Add' new address link is displayed.",
					"<b>Actual Result 1:</b> 'Add' new address link is not displayed.", driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Drop down should not be displayed If there is no address in the address book.");
			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElementsDoNotExist(shipToMultipleAddressPagelist2,
							shipToMultipleAddressPage),
					"<b>Actual Result 2:</b> Drop down is not displayed If there is no address in the address book.",
					"<b>Actual Result 2:</b> Drop down is displayed even if there is no address in the address book.",
					driver);
			Log.message("<br>");

			shipToMultipleAddressPage.ClickHomePage();
			signIn = homePage.headers.navigateToSignIn();
			Log.message("11. Navigated to Sign In Page!");
			signIn.signIn(emailid, password);
			checkoutPage.clickOnYesInShipToMultipleAddress();
			// ShipToMultipleAddressPage shipToMultipleAddressPage1 = new
			// ShipToMultipleAddressPage(driver).get();
			Log.message(
					"12. Navigated to 'ShipToMultipleAddress' page after Clicking 'Yes' button in the checkout shipping address page");
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b>  Added New address should be displayed in the drop down.");
			int AddedAddressCount = shipToMultipleAddressPage.ListOfAddedAddressCount();
			String addressval = shipToMultipleAddressPage.GetAddress(1, AddedAddressCount);
			boolean flag = false;
			if (addressval != null) {
				flag = true;
			}
			Log.softAssertThat(flag, "<b>Actual Result 3:</b>  Added New address is displayed in the drop down.",
					"<b>Actual Result 3:</b>  Added New address is not displayed in the drop down.", driver);
			shipToMultipleAddressPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_259

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays the category landing page by clicking the brand name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_262(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		List<String> addEditAddressPagelist = Arrays.asList("txtfirstname");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to 'HomePage'
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to 'SignIn' page from the header section
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Entering the credentials in the signIn page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// Searching a product with product keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'" + " and Navigated to PDP");
			pdppage.selectColor();
			Log.message("3. Color swatch Selected");
			pdppage.selectSizeByIndex(2);
			Log.message("6. Size is selected from size dropdown");
			pdppage.selectQtyByIndex(2);
			Log.message("7. Quantity drowdown is selected with Quantity 2");
			// Clicked on 'AddToBag'
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to cart' button");
			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");
			// Clicked on checkout in 'OrderSummary'
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Clicked on 'Checkout' button in 'OrderSummary'");
			// Clicked on 'Yes' button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11. Navigated to 'ShipToMultipleAddress' page after Clicking 'Yes' button in the checkout shipping address page");
			// Clicked on 'Add' link
			shipToMultipleAddressPage.clickaddLink();
			Log.message("12. Clicked on 'Add' link on 'Multi Shipping Addess' page");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Add/Edit' address screen should be displayed");

			Log.softAssertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(addEditAddressPagelist,
							shipToMultipleAddressPage),
					"<b>Actual Result 1:</b> 'Add/Edit' address screen is displayed",
					"<b>Actual Result 1:</b> 'Add/Edit' address screen is not displayed", driver);
			// Filling the shipping details in multi address page
			String expectedshippingDetails = shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("yes",
					address);
			Log.message("13. Shipping details filled in the shipping address form");
			// Clicked on 'Save' button
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("14.  Clicked on 'Save' Button on Multi Shipping Address page");
			shipToMultipleAddressPage.SelectRadioButton();
			Log.message("15. Selected Use original address as entered Radio button");
			shipToMultipleAddressPage.ClickContinueBtn();
			Log.message("16. Clicking on Continue Button on Address Validation popup");
			shipToMultipleAddressPage.ClickSaveBtn();
			Log.message("17. Clicking on Save button from Mutliple Shipping Address Page");
			List<String> expectedAddressList = new ArrayList<String>();
			String[] expectedAddressarr = expectedshippingDetails.split("\\|");
			expectedAddressList.add(expectedAddressarr[0]);
			String Actualshippingaddress = shipToMultipleAddressPage.GetAddress(1, 3);
			String[] addressarr = Actualshippingaddress.split("\\,");
			List<String> actualAddressList = new ArrayList<String>();
			actualAddressList.add(addressarr[1].trim());

			Boolean flag = Utils.compareTwoList(expectedAddressList, actualAddressList);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Should be able to add a new address in Multi shipping Page");
			Log.softAssertThat(flag, "<b>Actual Result 2:</b> Able to add a new address in Multi Shipping Page",
					"<b>Actual Result 2:</b> Not able to add a new address in Multi Shipping Page", driver);

			shipToMultipleAddressPage.ClickReturnToShippingAddresslink();

			checkoutPage = new CheckoutPage(driver).get();
			checkoutPage.clickReturnToShoppingBag();

			shoppingBagPage = new ShoppingBagPage(driver).get();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_262

	// TC_BELK_CART_264
	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system displays the category landing page by clicking the brand name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_264(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		List<String> shippingMethodPagelist = Arrays.asList("drpShippingMethod");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to 'Belk' homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to 'SignIn' page from the header section
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Entering the credentials in the signIn page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			// Searching a product with product keyword
			// Searching a product with product keyword
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'" + " and Navigated to PDP");
			pdppage.selectColor();
			Log.message("5. Color swatch Selected");
			pdppage.selectSizeByIndex(2);
			Log.message("6.  Size is Selected from size dropdown");
			pdppage.selectQtyByIndex(2);
			Log.message("7. Quantity drowdown is selected with Quantity 2");
			// Clicked on 'Addtobag'
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to cart' button");
			// Clicked on 'MiniCart' icon
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to 'ShoppingBag' page,after clicking on the minicart icon");
			// Clicked on checkout in 'OrderSummary'
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Clicked on 'Checkout' button in 'OrderSummary'");
			// Clicked on 'Yes' button
			checkoutPage.clickOnYesInShipToMultipleAddress();

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"11.  Navigated to 'ShipToMultipleAddress' page after Clicking 'Yes' button in the checkout shipping address page");
			shipToMultipleAddressPage.selectAdd1ByIndex(2);
			Log.message("12. First Item's address dropdown is selected");
			shipToMultipleAddressPage.selectAdd2ByIndex(4);
			Log.message("13. Second Item's address dropdown is selected");
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();
			Log.message("14. Navigated to Shipping Method Page by Clicking Continue button from Multi Shipping Page");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Clicking on Continue button from the Multi Shipping Page should be navigated to Shipping Method page.");
			Log.assertThat(
					shippingMethodPage.elementLayer.verifyPageElements(shippingMethodPagelist, shippingMethodPage),
					"<b>Actual Result :</b> Clicking on Continue button from the Multi Shipping Page navigated to Shipping Method page.",
					"<b>Actual Result :</b> Clicking on Continue button from the Multi Shipping Page didnot navigate to Shipping Method page.",
					driver);
			shippingMethodPage.ClickReturnToShippingAddresslink();
			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_264

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system navigates the User to Billing Address page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_165(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String address = "inValid_address1";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigated to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigated toPDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product is added to the cart");

			// Navigate to Mini Cart Bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Clicked on Minicart icon is navigated to the bag page");

			// Navigated to signIn page to continue as guest user
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Clicked on the 'Checkout' button!");

			// Navigated to checkout page to checkout the products
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11. Navigated to Checkout Page as Guest!");

			// Filling shipping details
			LinkedHashMap<String, String> shippingDetails = checkOutPage
					.fillingShippingDetailsAsGuest("inValid_address1", "Standard");
			List<String> indexes = new ArrayList<String>(shippingDetails.keySet());
			String value = indexes.get(6).replace("type_zipcode_", "");
			Log.message("12. Shipping Details Filled Successfully!");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on the 'Continue' button from the shipping page'");
			// Filling billingDetails
			checkOutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("14. Billing Details entered Successfully!", driver);
			String value1 = indexes.get(6).replace("type_zipcode_", "");
			Log.message("<br>");
			Log.message("<b>Expected Result-1: </b>Original address entered in Shipping Address should displayed.");
			Log.assertThat(value.equals(value1),
					"<b>Actual Result-1: </b> Original address entered in Shipping Address is displayed.",
					"<b>Actual Result-1: </b> Original address entered in Shipping Address is not displayed.", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result-2: </b>Page should be properly redirected to Billing Address Pane.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkOutPage),
					"<b>Actual Result-2: </b> Page properly redirected to Billing Address Pane!",
					"<b>Actual Result-2: </b> Page <b>not</b> redirected to Billing Address Pane!", driver);
			// Clean Up
			checkOutPage.clickReturnToShipping();
			shoppingBagPage = checkOutPage.clickReturnToShoppingBag();
			if (Integer.parseInt(shoppingBagPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage.removeItemsFromBag();
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_165

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system navigates the User to 'Step 2 Billing page in Cart screen. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_136(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");
		String cardDetails = "card_Visa";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		// List<String> shoppingBagPageElement = Arrays.asList("charCountMsg");

		try {
			// Navigate to HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");

			// Navigate to PDP Page for Search Keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Select Size, Colour and add to Bag
			pdpPage.addProductToBag();
			Log.message("6. Add product to bag from pdpPage !");

			// Navigate to Mini Cart Bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Click on Check out Button
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigate to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", address);
			Log.message("9. Shipping details entered in Checkout Page!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling billingDetails
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("10. Billing details entered in checkout page!", driver);

			// Filling paymentDetails
			checkoutPage.fillingCardDetails("NO", cardDetails);
			Log.message("11. Card Details Filled Successfully!", driver);

			// Click on Continue Buuton
			checkoutPage.clickOnContinueInBilling();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicking on Continue in the Billing Page.");

			// Click on Billing Panel in Header
			checkoutPage.clickOnBillingHeader();
			Log.message("13. Clicked on Billing address tab.");
			String className = checkoutPage.getClassNameOfStepsInCheckoutPage("billing").split(" ")[1];
			Log.message("<br>");
			Log.message(
					"<b> Expected Result: </b>System should navigate the User to Checkout Step 2 Billing page when User click on the Billing Panel.");
			Log.assertThat(className.equals("active"),
					"<b> Actual Result: </b> Billing button is enabled when redirect to Billing page from place order page",
					"<b> Actual Result: <b> Billing button is disabled when redirect to Billing page from place order page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_136

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Edit link for Order Summary in Checkout-Place Order page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_244(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		// List<String> shoppingBagPageElement = Arrays.asList("charCountMsg");

		try {
			// Navigate to Homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");

			// Navigate to PDP Page for Search Keyword
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "");
			Log.message("5. Navigated to PDP");

			// Select Size, Colour and add to Bag
			pdpPage.addProductToBag();
			Log.message("6. Add product to bag from pdpPage !");

			// Navigate to Mini Cart page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			String PresentUrl = driver.getCurrentUrl();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Click on Check Out Button
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");
			Log.message("9. Shipping Details Filled Successfully!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling billingDetails
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message("10. Billinging Details Filled Successfully!", driver);

			// Filling paymentDetails
			checkoutPage.fillingCardDetails("NO", "card_AmericanExpressInvaild");
			Log.message("11. Card Details Filled Successfully!", driver);

			// Click on Continue Button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click on Continue in the Billing Page.");

			// Click on Edit In Order Summary
			checkoutPage.clickOnEditBySectionName("ordersummary");
			String ReturnUrl = driver.getCurrentUrl();
			Log.message("13. Click on Edit Button Panel.");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should be redirected the My Shopping Bag page.");
			Log.assertThat(PresentUrl.equalsIgnoreCase(ReturnUrl),
					"<b> Actual Result: </b> User should be redirected the My Shopping Bag page.",
					"<b> Actual Result: </b> User should not be redirected the My Shopping Bag page.", driver);
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_244

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Edit link for Shipping Address in Checkout-Place Order page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_245(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");

			// Navigate to PDP Page for Search Keyword
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Select a Product on Product Description Page
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Colour and add to Bag
			pdpPage.addProductToBag();
			Log.message("6. Add product to bag from pdpPage !");

			// Navigate to Mini Cart page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();

			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Click on Check Out Button
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address2");
			Log.message("9. Shipping Details Filled Successfully!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling billingDetails
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "NO", "valid_address2");
			Log.message("10. Billing Details Filled Successfully!", driver);

			// Filling paymentDetails
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("11. Card Details Filled Successfully!", driver);

			// Click on Continue Button
			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Click on Continue in the Billing Page.");

			// Click on Edit Button On Place Order page Header
			checkoutPage.clickOnEditBySectionName("shippingaddress");
			Log.message("13. Click on Edit Button Panel.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b>User should be redirected to the Checkout-Shipping page and should be able to edit the shipping address details");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("ShippingInHeaderEnabled"),
							checkoutPage),
					"<b>Actual Result : </b> User should be redirected to the Checkout-Shipping page and should be able to edit the shipping address details.",
					"<b>Actual Result : </b> User should not be redirected to the Checkout-Shipping page and should be able to edit the shipping address details.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b> User should be able to edit the shipping address details");
			checkoutPage.clickOnAddressFieldOnShippingBag();
			Log.message("<b>Actual Result: </b> The Address is editable", driver);

			checkoutPage.clickReturnToShoppingBag();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_245

	@Test(groups = { "desktop",
			"mobile" }, description = "User should be redirected to the Checkout-Billing page and should be able to edit the billing address details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_246(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to Homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Navigate to Login page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered credentials(" + emailid + "/" + password + ") and navigati ng to 'My Account' page");

			// Navigate to PDP Page for Search Keyword
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Select a Product on Product Description Page
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Colour and add to Bag
			pdpPage.addProductToBag();
			Log.message("6. Add product to bag from pdpPage !");

			// Navigate to Mini Cart page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Navigate to Check Out Button
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");
			Log.message("9. Shipping Details Filled Successfully!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling billingDetails
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message("10. Billing Details Filled Successfully!", driver);

			// Filling paymentDetails
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("11. Card Details Filled Successfully!", driver);

			// Click on Continue Button
			checkoutPage.clickOnContinueInBilling();
			// checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Click on Continue in the Billing Page.");

			// Click on Edit Button On Place Order
			checkoutPage.clickOnEditBySectionName("billingaddress");
			Log.message("13. Click on Edit Button Panel.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b>User should be redirected to the Checkout-Shipping page and should be able to edit the shipping address details");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkoutPage),
					"<b>Actual Result : </b> User should be redirected to the Checkout-Shipping page and should be able to edit the shipping address details.",
					"<b>Actual Result : </b> User should not be redirected to the Checkout-Shipping page and should be able to edit the shipping address details.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b> User should be able to edit the shipping address details");
			checkoutPage.clickOnAddressFieldOnBillingBag();
			Log.message("<b>Actual Result:</b> The Address is editable", driver);
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_246

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Line Item details in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_073(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. Selected size : " + pdtDetails.split("\\|")[1]);

			Log.message("5. Selected color : " + pdtDetails.split("\\|")[0]);

			Log.message("6. Selected quantity : " + pdtDetails.split("\\|")[2]);

			Log.message("7. Product added to Minicart!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to Shopping bag!");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> Product Image should be displayed");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("productImg"), shoppingBagPage),
					"<b>Actual Result-1:</b> Product Image is displayed in Shopping bag page",
					"<b>Actual Result-1:</b> Product Image is not displayed in Shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> Product Brand should be displayed");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtProductBrand"), shoppingBagPage),
					"<b>Actual Result-2:</b> Product Brand is displayed in shopping bag page",
					"<b>Actual Result-2:</b> Product Brand is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> Product name should be displayed");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtProductName"), shoppingBagPage),
					"<b>Actual Result-3:</b> Product name is displayed in shopping bag page",
					"<b>Actual Result-3:</b> Product name is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-4:</b> Product color should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageListElements(Arrays.asList("lblColor"), shoppingBagPage),
					"<b>Actual Result-4:</b> Product color is displayed in shopping bag page",
					"<b>Actual Result-4:</b> Product color is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-5:</b> Product size should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageListElements(Arrays.asList("lblSize"), shoppingBagPage),
					"<b>Actual Result-5:</b> Product size is displayed in shopping bag page",
					"<b>Actual Result-5:</b> Product size is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-6:</b> Quantity should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("drpQuantity"), shoppingBagPage),
					"<b>Actual Result-6:</b> Quantity is displayed in shopping bag page",
					"<b>Actual Result-6:</b> Quantity is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-7:</b> Subtotal should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtSubtotalspan"), shoppingBagPage),
					"<b>Actual Result-7:</b> Subtotal is displayed in shopping bag page",
					"<b>Actual Result-7:</b> Subtotal is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-8:</b> 'Find in store' button should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnFindStore"), shoppingBagPage),
					"<b>Actual Result-8:</b> 'Find in store' button  is displayed in shopping bag page",
					"<b>Actual Result-8:</b> 'Find in store' button  is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-9:</b> 'Add to Wishlist' button should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnAddToWishlist"), shoppingBagPage),
					"<b>Actual Result-9:</b> 'Add to Wishlist' button  is displayed in shopping bag page",
					"<b>Actual Result-9:</b> 'Add to Wishlist' button  is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-10:</b> 'Add to Registry' button should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnAddToRegistry"), shoppingBagPage),
					"<b>Actual Result-10:</b> 'Add to Registry' button  is displayed in shopping bag page",
					"<b>Actual Result-10:</b> 'Add to Registry'  is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-11:</b> 'Add to Gift Box' checkbox should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("chkAddGiftBox"), shoppingBagPage),
					"<b>Actual Result-11:</b> 'Add to Gift Box' checkbox  is displayed in shopping bag page",
					"<b>Actual Result-11:</b> 'Add to Gift Box' checkbox  is not displayed in shopping bag page");

			Log.message("<br>");
			Log.message("<b>Expected Result-12:</b> 'Add Free Gift message' checkbox should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtGiftMessage"), shoppingBagPage),
					"<b>Actual Result-12:</b> 'Add Free Gift message' checkbox  is displayed in shopping bag page",
					"<b>Actual Result-12:</b> 'Add Free Gift message' checkbox  is not displayed in shopping bag page");

			Log.message("<br>");
			shoppingBagPage.clickOnProductName();
			Log.message("7. Clicked on Product name in shopping bag page!");
			Log.message("<br>");

			Log.message("<b>Expected Result-13:</b> Edit Modal should be displayed");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("editDetailsPopup"), shoppingBagPage),
					"<b>Actual Result-13:</b> Edit Modal is displayed",
					"<b>Actual Result-13:</b> Edit Modal is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_073

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify resource message displays in Order Receipt screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_221(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Navigating to searchResultPage
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + " and Navigated to PDP");
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			// Add the product to the bag
			Log.message("5. Product is added to the cart");
			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			// Navigating to Minicart page
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("6. Minicart icon is clicked and navigated to Shopping bag page");
			// Cliking on checkout Button and Navigating to signIn page
			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("7. Checkout button in order summary is clicked properly");
			// clicking on clickOnCheckoutAsGuest
			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("8. Checkout as guest button is clicked properly");
			// filling shipping details
			checkoutpage.fillingShippingDetailsAsGuest("valid_address6", "Standard");
			Log.message("9. shipping details filled ");
			// clicking on continue button
			checkoutpage.clickOnContinueInShipping();
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Continue button is clicked in popup modal and navigated to billing page");
			// filling billing details
			checkoutpage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("11. Billing details filled ");
			// filling card details
			checkoutpage.fillingCardDetails("NO", "card_Amex");
			Log.message("12. Card details filled");
			// clicking on continue button
			checkoutpage.clickOnContinueInBilling();
			Log.message("13. Continue In Billing is clicked");
			// clicking placeOrder button
			OrderConfirmationPage orderconfirmationpage = checkoutpage.placeOrder();
			Log.message("14. Place order button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> Verify resource message displays in Order Receipt screen");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("txtResourcemsg"),
							orderconfirmationpage),
					"Thank you for your Order resource message is  displayed in the Order Receipt screen",
					"Thank you for your Order resource message is not  displayed in the Order Receipt screen");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_221

	@Test(groups = { "desktop",
			"Mobile" }, description = "Verify Edit link for Shipping Address in Checkout-Billing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_241(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String addressTwo = testData.get("Address");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);
			ShoppingBagPage shoppingBagPage = myaccount.headers.NavigateToBagPage();
			shoppingBagPage.removeAddedCartItems();
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity

			Log.message("8. Click on Add to bag button");
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Minicart icon is clicked and navigated to Shopping bag page");

			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);

			Log.message("10. Naviagte to checkout page", driver);

			// checkoutpage.clickOnContinueInShipping();
			// checkoutpage.clickOnBillingHeader();
			checkoutpage.enterCVVinExpressCheckout("card_Visa");
			checkoutpage.clickOnApplyButton();
			checkoutpage.clickOnBillingHeader();
			Log.message("12. Shipping header clicked and again navigated to billing ");
			checkoutpage.FillAddress2InShipping(addressTwo);
			Log.message("13. Address 2 details is filled. Filled detail is " + addressTwo);
			String TextFromAddressField = checkoutpage.GetTextFromAddress2TextFiled();
			Log.message("14. Got the text from address 2 text field and the address is: " + TextFromAddressField);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> User should be allowed to edit the shipping address from the Checkout-Billing page.");
			Log.assertThat(TextFromAddressField.equals(addressTwo),
					"<b>Actual Result : </b> User is allowed to edit the shipping address from the Checkout-Billing page.",
					"<b>Actual Result : </b> User is not allowed to edit the shipping address from the Checkout-Billing page.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_241

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the Order Summary details in Cart page(Shopping Bag with Products) for a Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_079(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("5. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagted to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. 'CheckoutAsGuest' button is clicked");

			// CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutasReg();
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("9. Filled the shipping details");

			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("12. Filled the billing details");
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("Clicked on 'Continue' in billing address page");
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message("14. Place order button is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Order Summary section on the cart page should display the below details:Merchandise Total with Gift Card,Total Coupon Savings,Gift Box,Shipping Surcharge,Estimated Shipping,Estimated Order Total");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldShippingCharge"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Shipping Surcharge is present",
					"<b>Actual Result:</b> Shipping Surcharge is not present", driver);
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldMerchandiseTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Merchandise Total is present",
					"<b>Actual Result:</b> Merchandise Total is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldEstimatedTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Estimated total is present",
					"<b>Actual Result:</b> Estimated total is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldSalesTax"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Sales tax is present", "<b>Actual Result:</b> Sales tax is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldOrderTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Order total is present", "<b>Actual Result:</b> Order total is not present",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_079

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the Order Summary details in Cart page(Shopping Bag with Products) for a Signed-In User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_078(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				Log.message("4. Products removed from Cart");

			}
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("8. Product is added to the cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Minicart icon is clicked and navigated to Shopping bag page");

			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("11. Clicked 'Checkout' button and naviagte to checkout page");
			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address2");
			checkoutpage.clickOnContinueInShipping();
			Log.message("12. 'Continue button' in shipping address page is clicked");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Continue button is clicked in popup modal and navigated to billing page");
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details filled");
			checkoutpage.clickOnContinueInBilling();
			Log.message("15. Continue In Billing is clicked");
			OrderConfirmationPage orderconfirmationpage = checkoutpage.placeOrder();
			Log.message("16. Place order button is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Order Summary section on the cart page should display the below details:Merchandise Total with Gift Card,Total Coupon Savings,Gift Box,Shipping Surcharge,Estimated Shipping,Estimated Order Total");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldMerchandiseTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Merchandise Total is present",
					"<b>Actual Result:</b> Merchandise Total is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldEstimatedTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Estimated total is present",
					"<b>Actual Result:</b> Estimated total is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldSalesTax"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Sales tax is present", "<b>Actual Result:</b> Sales tax is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldOrderTotal"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Order total is present", "<b>Actual Result:</b> Order total is not present");
			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageListElements(Arrays.asList("fldShippingCharge"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Shipping Surcharge is present",
					"<b>Actual Result:</b> Shipping Surcharge is not present");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_078

	@Test(enabled = false, groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Edit link for Payment Method in Checkout-Place Order page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_250(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String customerDetails = testData.get("CustomerDetails");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("7. Navigated to 'Mini Cart' !");

			CheckoutPage checkOutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to Checkout as SignedUser!");

			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", customerDetails);
			Log.message("9. Shipping address details are entered!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Clicked on Continue Button in Shipping and Navigated to Billing");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Clicked on Continue Button in Address validation modal");

			Log.message("12. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_AmericanExpress");
			Log.message("13. Entered card details");

			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on Continue Button in Billing  And Navigated To PLace Order ");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result:</b> By Clicking on 'REturn To Billing',  It should be redirected to the Checkout-Billing page.");

			checkOutPage.clickReturnToBilling();
			Log.assertThat(checkOutPage.getTextFromBillingHeader().contains("Return to Shipping Address"),
					"<b>Actual Result:</b> By Clicking on 'REturn To Billing',  It is redirected to the Checkout-Billing page.",
					"<b>Expected Result:</b> By Clicking on 'REturn To Billing',  It should be redirected to the Checkout-Billing page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_250

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Order Receipt details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_251(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String successMsg = "Thank you for your order!";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey + "' and Navigated to Search Result Page!");
			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");
			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");
			// selecting size from Size Drop down in Pdp Page
			Log.message("7. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");
			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");
			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			float ItemSubTotal = shoppingBagPage1.getMerchandiseTotal();
			String ItemTotal = Float.toString(ItemSubTotal);
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");
			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address2");
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			LinkedHashMap<String, String> shippingDetails = checkoutPage.getShippingAddressDetails();
			String City = shippingDetails.get("City");
			String AddressOne = shippingDetails.get("AddressOne");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address2");
			Log.message("14. Filled the Billing details in Billing form!");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("15. Entered card details!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");
			String CardType = checkoutPage.getCreditCardType();
			// Clicking on continue in Place Order Page
			OrderConfirmationPage orderconfirmationpage = checkoutPage.placeOrder();
			Log.message("18.  Navigated to Place Order Page!");
			LinkedHashMap<String, String> billingdetails = orderconfirmationpage.getBillingAddressInOrderSummary();
			LinkedList<LinkedHashMap<String, String>> productDetails = orderconfirmationpage
					.getProductDetailsInOrderSummary();
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Thank You for Your Order message should be displayed.");
			Log.assertThat(orderconfirmationpage.GetTextFromSuccessMessage().contains(successMsg),
					"<b> Actual Result 1:</b> '" + orderconfirmationpage.GetTextFromSuccessMessage()
							+ "' is displayed.",
					"<b>Actual Result 1:</b> '" + orderconfirmationpage.GetTextFromSuccessMessage()
							+ "' is not displayed.");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b>  Order Summary with Order Information, Billing Address, Payment Method and Payment Details should be displayed.");
			Log.assertThat(
					orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Order Information")
							&& orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Order Number")
							&& City.contains(billingdetails.get("City"))
							&& billingdetails.get("Address").contains(AddressOne)
							&& orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Payment Method")
							&& orderconfirmationpage.GetCardType().equals(CardType)
							&& orderconfirmationpage.gettextfromordersummaryIncludingHeader()
									.contains("Payment Details")
					&& orderconfirmationpage.gettextfromordersummaryIncludingHeader().contains("Merchandise Total"),
					"<b>Actual Result 2:</b> Order Summary with Order Information, Billing Address, Payment Method and Payment Details is displayed.",
					"<b>Actual Result 2:</b> Order Summary with Order Information, Billing Address, Payment Method and Payment Details is not displayed.");

			Log.message("<br>");
			if (Utils.getRunPlatForm() == "desktop") {

				Log.message("<br>");
				Log.message("<b>Expected Result 3:</b>  Shipping Details with Price values should be displayed.");
				Log.assertThat(
						orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains("Shipping Details")
								&& orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains("Price")
								&& orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains(City)
								&& productDetails.get(0).get("Price").contains(ItemTotal),
						"<b>Actual Result 3:</b>  Shipping Details with Price values is displayed.",
						"<b>Actual Result 3:</b>  Shipping Details with Price values is not displayed.");

				Log.message("<br>");
				Log.message("<b>Expected Result 4:</b> Return To Shopping button should  be displayed");
				Log.assertThat(
						orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("returnToShoppingBtn"),
								orderconfirmationpage),
						"<b>Actual Result 4:</b>  Return To Shopping button is displayed",
						"<b>Actual Result 4:</b> Return To Shopping button is not displayed");
			} else if (Utils.getRunPlatForm() == "mobile") {

				Log.message("<b>Expected Result 3:</b>  Shipping Details with Price values should be displayed.");
				Log.assertThat(
						orderconfirmationpage.getCurrentLocationOfOrderAndPaymentDetailsByIndex(
								1) == orderconfirmationpage.getCurrentLocationOfOrderAndPaymentDetailsByIndex(2)
						&& orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains("Shipping")
						&& orderconfirmationpage.gettextfromshippingsummaryIncludingHeader().contains(City)
						&& productDetails.get(0).get("Price").contains(ItemTotal),
						"<b>Actual Result 3:</b>  Shipping Details with Price values is displayed.",
						"<b>Actual Result 3:</b>  Shipping Details with Price values is not displayed.");
				((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Log.message("<br>");
				Log.message("<b>Expected Result 4:</b> Return To Shopping button should  be displayed");
				Log.assertThat(
						orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("returnToShoppingBtn"),
								orderconfirmationpage),
						"<b>Actual Result 4:</b>  Return To Shopping button is displayed",
						"<b>Actual Result 4:</b> Return To Shopping button is not displayed");
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_251//

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify page navigates to Shipping Address screen when clicking Edit link in Shipping Address - Order Summary panel from Billing Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_210(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1.Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			pdpPage.addProductToBag();
			Log.message("4.Color, size are selected and 'Add to shopping bag' button is clicked");

			MiniCartPage minicartpage = new MiniCartPage(driver).get();
			ShoppingBagPage shoppingbagpage = minicartpage.navigateToBag();
			Log.message("5.Minicart icon is clicked and navigated to Shopping bag page");

			SignIn signinpage = (SignIn) shoppingbagpage.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("6.Clicked on Checkout in Order summary Page");

			CheckoutPage checkoutpage = signinpage.clickOnCheckoutAsGuest();
			Log.message("7.Clicked on Checkout As Guest User in Sign In Page");

			checkoutpage.fillingShippingDetailsAsGuest("valid_address1", "Express");
			Log.message("8.Entered Shipping Address Details as Guest");

			checkoutpage.clickOnContinueInShipping();
			Log.message("9.Clicked on Continue Shipping Address Details as Guest");

			checkoutpage.ContinueAddressValidationModalWithDefaults();

			LinkedHashMap<String, String> shippingDetail = checkoutpage.getShippingAddress();
			// String
			// shippingFirstname=checkoutpage.getFirstnameOfShippingAddressInBillingAddressPage();
			// String
			// shippingLastname=checkoutpage.getLastnameOfShippingAddressInBillingAddressPage();
			// String
			// shippingPhoneNumber=checkoutpage.getPhoneNoInShippingAddressInBillingAddress();
			// String
			// shippingCity=checkoutpage.getCityInShippingInBillingAddress();
			// String
			// shippingAddress=checkoutpage.getAddressInShippingAddressInBillingAddress();
			//
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_1InBilling"), checkoutpage),
					"10.The page is navigated to Billing Address screen",
					"10.The page is not navigated to Billing Address screen");

			checkoutpage.clickEditShippingAddress();

			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("txtPhoneInShipping"), checkoutpage),
					"11.The page is navigated to Shipping Address screen",
					"11.The page is not navigated to Shipping Address screen");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Added Shipping Address Firstname details should be properly displayed in Shipping Address screen");

			Log.assertThat(((checkoutpage.getFirstnameInShippingAddressPage()).equals(shippingDetail.get("FirstName"))),
					"<b>Actual Result 1: </b> Added shipping Address Firstname details is properly displayed in shipping Address screen",
					"<b>Actual Result 1: </b>  Added shipping Address Firstname details is not displayed properly in shipping Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2:</b> Added shipping Address Lastname details should be properly displayed in shipping Address screen");

			Log.assertThat(((checkoutpage.getLastnameInshippingAddress()).equals(shippingDetail.get("LastName"))),
					"<b>Actual Result 2: </b> Added shipping Address Lastname details is properly displayed in Shipping Address screen",
					"<b>Actual Result 2: </b>  Added shipping Address Lastname details is not displayed properly in Shipping Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 3:</b> Added shipping Address Phone details should be properly displayed in shipping Address screen");

			Log.assertThat(((checkoutpage.getPhoneNoInshippingAddressPage()).equals(shippingDetail.get("Phone"))),
					"<b>Actual Result 3: </b> Added shipping Address Phone details is properly displayed in Shipping Address screen",
					"<b>Actual Result 3: </b>  Added shipping Address Phone details is not displayed properly in Shipping Address screen",
					driver);
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 4:</b> Added Shipping Address City should be properly displayed in Shipping Address screen");
			Log.assertThat(((checkoutpage.getCityInshippingAddress()).equals(shippingDetail.get("City"))),
					"<b>Actual Result 4: </b> Added Shipping Address City details is properly displayed in Shipping Address screen",
					"<b>Actual Result 4: </b>  Added Shipping Address City details is not displayed properly in Shipping Address screen",
					driver);

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 5:</b> Added shipping Address details should be properly displayed in shipping Address screen");
			Log.assertThat(((checkoutpage.getAddressInnshippingAddress()).equals(shippingDetail.get("Address"))),
					"<b>Actual Result 5: </b> Added shipping Address details is properly displayed in shipping Address screen",
					"<b>Actual Result 5: </b>  Added shipping Address details is not displayed properly in shipping Address screen",
					driver);

			checkoutpage.clickOnContinueInShipping();
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("txtAddress_1InBilling"), checkoutpage),
					"The page is again navigated back to Billing Address screen after editing shipping address and clicking on continue",
					"The page is not navigated back to Billing Address screen after editing shipping address and clicking on continue");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_210

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "verify system displays a 'Select a Registry pop up.'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_123(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Entering the credentials in the signIn page
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);
			ShoppingBagPage shoppingBagPage = myAccountPage.headers.NavigateToBagPage();
			shoppingBagPage.removeAddedCartItems();
			// Searching a product with product keyword like 'jeans'
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting the productname from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("10. Product is added to the cart");

			// Navigated to the shoppingBagPage by clicking on minicart icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Clicking on 'Registry' link
			shopingBagPage.clickOnRegistry(productName);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> 'Select a Registry' pop up should be displayed  in the 'ShoppingBag' page ");
			Log.softAssertThat(
					shopingBagPage.elementLayer.verifyPageElements(Arrays.asList("RegistryPopUp"), shopingBagPage),
					"<b>Actual Result :</b> 'Select a Registry' pop up is displayed  in the 'ShoppingBag' page  ",
					"<b>Actual Result :</b> 'Select a Registry' pop up is not displayed  in the 'ShoppingBag' page  ",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_123

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify pages navigates from My Bag screen when clicking 'Edit' link in Order Summary pane from Place Order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_206(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		// String customerDetails = testData.get("CustomerDetails");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("8. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to Shopping bag page !");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Clicked on 'Checkout' button");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("11. Clicked on 'Checkout as Guest' button");

			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("12. Shipping address details are entered!");

			checkOutPage.clickOnContinueInShipping();
			Log.message("13. Clicked on Continue Button in Shipping and Navigated to Billing");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("14. Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
			Log.message("15. Billing address details are entered!");

			checkOutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("16. Entered card details");

			checkOutPage.clickOnContinueInBilling();
			Log.message("17. Clicked on Continue Button in Billing  And Navigated To PLace Order ");

			checkOutPage.clickOnEditBySectionName("ordersummary");
			Log.message("18. Clicked on 'Edit' in orderSummary in the 'PlaceOrder'");

			shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("19. Naviagted to checkout page", driver);

			siginPage.clickOnCheckoutAsGuest();
			Log.message("20. Clicked on Checkout As Guest");
			checkOutPage.clickOnContinueInShipping();
			Log.message("21. Clicked on Continue Button in Shipping and Navigated to Billing");

			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("22. Clicked on Continue Button in Address validation modal");

			checkOutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("23. Filled card details");

			checkOutPage.clickOnContinueInBilling();
			Log.message("24. Clicked on Continue Button in Billing  And Navigated To PLace Order ");

			String creditCardInPaymentSection = checkOutPage.getCreditCardType();
			String paymentDetails = checkOutPage.getPaymentDetails();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The payment details should be updated in 'placeOrder'");
			Log.assertThat(paymentDetails.contains(creditCardInPaymentSection),
					"<b>Actual Result:</b>The payment details is updated in 'placeOrder'",
					"<b>Actual Result:</b>The payment details is not updated in 'placeOrder'", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_206

	@Test(enabled = false, groups = { "desktop",
			"mobile" }, description = "Verify page redirected from My Bag screen when clicking Edit link in Billing Address from Place Order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_Checkout_214(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message("3. Navigated to 'Pdp' Page with randomly selected product");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("6. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("7. Navigated to Shopping bag!");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to checkout page!");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. Clicked on CheckOutAsGuest Button");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("10. Shipping Address details are entered in the Shipping Address Page!");

			checkoutPage.clickOnContinueInShipping();
			Log.message("11. Clicked on Continue Button in the Shipping page and Navigated to Billing Page !");

			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");

			Log.message("13. Billing address details are entered in the Billing Page!");

			checkoutPage.fillingCardDetails("NO", "card_AmericanExpress");
			Log.message("14. Card details are entered in the Billing Page");

			BrowserActions.nap(1);
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue Button in the Billing Page and Navigated to Place Order Page!");
			checkoutPage.clickOnEditBySectionName("billingaddress");
			Log.message("16. Clicked on Edit Link in Place Order Page and Navigated to Billing page !");

			checkoutPage.fillingCardDetails("No", "card_Discover");
			Log.message("17. Edited Payment Details in the Billing Address Page !");

			BrowserActions.nap(2);
			checkoutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on Continue Button in Billing Page and Navigated to Place Order Page !");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to place holder section.");
			String className = checkoutPage.getClassNameOfStepsInCheckoutPage("placeorder").split(" ")[1];
			Log.assertThat(className.equals("active"),
					"<b>Actual Result:</b>The page is redirected to place holder section",
					"<b>Actual Result:</b>The page is not redirected to place hoder section", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CHECKOUT_214

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays shipping address Place Order- Customer choose 'Ship to Home' in Shipping Address panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_208(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			// Selecting the Color
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			// Add the product to the bag
			Log.message("6. Product is added to the cart");
			// Navigating to shoppingBagPage
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to 'shopping Bag Page' !");
			// Navigate to signInPage
			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagted to SignIn page", driver);
			// clicking on checkOutAsGuest
			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("9. clicked on checkOutAsGuest ");

			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("10. Shipping address details are entered!");

			// Click on Continue Button
			checkOutPage.clickOnContinueInShipping();
			Log.message("11. Clicked on Continue Button in Shipping and Navigated to Billing");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("12. Clicked on Continue Button in Address validation modal");

			// filling Billing details
			checkOutPage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
			Log.message("13. Billing address details are entered!");

			// filling Card details
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("14. Entered card details");
			LinkedHashMap<String, String> shippingDetailsInBilling = checkOutPage.getShippingAddress();
			// Click on Continue Button
			checkOutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue Button in Billing  And Navigated To PLace Order ");
			LinkedHashMap<String, String> shipingDetailsInPlaceOrder = checkOutPage.getShippingAddress();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Shipping Address should be displayed in Place order page as per the address entered in shipping address page");
			Log.assertThat(Utils.compareTwoHashMap(shippingDetailsInBilling, shipingDetailsInPlaceOrder),
					"<b>Expected Result:</b> Shipping Address is displayed in Place order page as per the address entered in shipping address page",
					"<b>Expected Result:</b> Shipping Address is not displayed in Place order page as per the address entered in shipping address page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_208

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Create Account panel displays in Order Receipt screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_222(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to searchResultPage
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Product added to Shopping Bag!");
			// clicking on mimicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'shopping Bag Page' !");
			// clicking checkout button
			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Naviagte to sigin page");
			// clicking on CheckoutAsGuest
			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("10. clicked on CheckoutAsGuest");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("11. Shipping address details are entered!");
			// clicking on continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue Button in Shipping and Navigated to Billing");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");
			// filling billing details
			checkOutPage.fillingBillingDetailsAsGuestUser("NO", "valid_address1");
			Log.message("14. Billing address details are entered!");
			// filling card details
			checkOutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("15. Entered card details");
			// clicking on continue button
			checkOutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on Continue Button in Billing  And Navigated To PLace Order ");
			OrderConfirmationPage orderConfirmationPage = checkOutPage.placeOrder();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Create Account panel should be displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address'  ,'Password' , 'Confirm Password', 'Create Account button' in the Order Summary page.");
			Log.softAssertThat(
					orderConfirmationPage.elementLayer
							.verifyPageElements(
									Arrays.asList("txtFirstname", "txtLastname", "txtEmail", "txtConformEmail",
											"txtPassword", "txtConformPassword", "btnCreateAccount"),
									orderConfirmationPage),
					"<b>Actual Resul1t:</b> The Create Account panel is displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address' , 'Create Account button' in the Order Summary page",
					"<b>Actual Result:</b> The Create Account panel is not displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address' ,'Password' , 'Confirm Password', 'Create Account button' in the Order Summary page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_222

	@Test(enabled = false, groups = { "desktop",
			"mobile" }, description = "Verify Edit link for Shipping Address in Checkout-Billing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_240(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("8. Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Minicart icon is clicked and navigated to Shopping bag page");

			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Navigated to Checkout as SignedUser!");

			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address1");
			Log.message("11. Shipping address details are entered and 'Use billing address' check box is clicked!");

			checkoutpage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue Button in Shipping and Navigated to Billing");

			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");
			Log.message("14. Continue button clicked in Shipping page navigated to billing pahe");
			checkoutpage.fillingCardDetails("YES", "card_AmericanExpress");
			String cardOwnerNameInBillingPage = checkoutpage.getTextInNameOnCard();
			String cardTypeInShipping = checkoutpage.getTextFromSelectedCard();
			Log.message("15. Card details entered in billing page");
			checkoutpage.mouseOverOnWhatIsThis();
			Log.message("16. Mouse Over on what is this link");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("toolTips"), checkoutpage),
					"After Mouse over on what is this link popup is present",
					"After Mouse over on what is this link popup is not present");
			checkoutpage.clickOnContinueInBilling();
			OrderConfirmationPage orderconfirmationpage = checkoutpage.placeOrder();
			MyAccountPage myaccountpage = orderconfirmationpage.headers.navigateToMyAccount();
			myaccountpage.navigateToPayMethod();
			String cardTypeinMyaccount = myaccountpage.getTextFromCardType();
			String cardOwnerNameInMyAccount = myaccountpage.getTextFromCardOwner();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The card details in billing page and my accout page should be same.");

			Log.assertThat(
					cardTypeInShipping.equals(cardTypeinMyaccount)
							&& cardOwnerNameInMyAccount.equals(cardOwnerNameInBillingPage),
					"<b>Actual Result:</b>card in my account is same as in the billing page",
					"<b>Actual Result:</b>card in my account is not same as in the billing page", driver);
			myaccountpage.deleteCardFromAccount();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_240

	// TC_BELK_CART_BE_1704
	@Test(enabled = false, groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays warning message for invalid Shipping address on Shipping Address pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_BE_1704(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigated to Search Result Page : " + searchKey);

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = searchResultpage.navigateToPDP();
			Log.message("3. Navigated to PDP from Search results page !");

			// Select Size, Color, Qty
			Log.message("4. Selecting Size, Color, Quantity!");
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag

			Log.message("8. Product is added to the cart");

			// Load shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to 'Mini Cart' !", driver);

			// click on product name
			shoppingBagPage.clickProductName();
			Log.message("10. Navigated to PDP by clicking on Product Name!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_BE_1704

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays the Checkout steps for the MultiShip option. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_139(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching product in my account page with a productId

			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("8. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("9. Product added to Shopping Bag!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Clicking on Mini cart icon is navigated to the bag page");
			shoppingBagPage.selectQuantity("2");
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("11. Navigate to checkout page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Checkout header steps for the Multi ship option should be displayed: 1. Shipping  2.Billing 3. Place Order");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabShipping"), checkoutPage),
					"<b>Actual Result:</b> Shipping tab is displayed in checkOut header",
					"<b>Actual Result:</b> Shipping tab is not displayed in checkOut header", driver);
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabBilling"), checkoutPage),
					"<b>Actual Result:</b> Billing tab is displayed in checkOut header",
					"<b>Actual Result:</b> Billing tab is not displayed in checkOut header", driver);
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabPlaceOrder"), checkoutPage),
					"<b>Actual Result:</b> PlaceOrder tab is displayed in checkOut header",
					"<b>Actual Result:</b> PlaceOrder tab is not displayed in checkOut header", driver);

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("12. Clicked 'Yes' Button for shipping to Multiple Address!");

			if (Utils.getRunPlatForm() == "mobile") {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("window.scrollTo(0, document.body.scrollHeight/2)");
			}
			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2:</b> User should be allowed to next step only when the User completed the current step. ");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnContinueInMultiShipping"),
							checkoutPage),
					"<b>Actual Result:</b> Continue button enabled after filling details",
					"<b>Actual Result:</b> Continue button disabled after filling details", driver);

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();

			Log.message("14. Click continue in multishipping address page!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("15. Click continue in multishipping shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			checkoutPage.fillingCardDetails("YES", "card_Visa");
			Log.message("16. Card details filled successfully!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("17. Click continue in multishipping billing page!");

			// Returning to billing page from place order page after clicking on
			// the link
			checkoutPage.clickReturnToBilling();
			Log.message("18. Returning to billing page!");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 3:</b>System should be allowed to navigate the User to a previously completed step which is enabled.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabledInMultiAddress"),
							checkoutPage),
					"<b>Actual Result :</b> 'User can navigate to the previous page",
					"<b>Actual Result :</b> 'User can not navigate to the previous page");

			Log.testCaseResult();
			shoppingBagPage = checkoutPage.clickOnEditInOrderSummary();
			shoppingBagPage.removeItemsFromBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_139

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system displays the numbers as Checkout steps for the Non-Multi shop option..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_140(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching product in my account page with a productId
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("6. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("7. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");
			Log.message("8. product added to bag from pdpPage !");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Clicking on Mini cart icon is navigated to the bag page");
			// Getting Url of the shopping bag page
			String beforeURL = driver.getCurrentUrl();

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 1:</b> Checkout header steps for the Non-Multi ship option should be displayed: 1. Shipping  2.Billing 3. Place Order");

			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabShipping"), checkoutPage),
					"<b>Actual Result:</b> Shipping tab is displayed in checkOut header",
					"<b>Actual Result:</b> Shipping tab is not displayed in checkOut header", driver);
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabBilling"), checkoutPage),
					"<b>Actual Result:</b> Billing tab is displayed in checkOut header",
					"<b>Actual Result:</b> Billing tab is not displayed in checkOut header", driver);
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tabPlaceOrder"), checkoutPage),
					"<b>Actual Result:</b> placeOrder tab is displayed in checkOut header",
					"<b>Actual Result:</b> placeOrder tab is not displayed in checkOut header", driver);
			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Standard", "valid_address1");
			Log.message("11. Filled shipping details successfully ");

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 2: </b>User should be allowed to next step when the User completed the current step.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnContinueToBill"), checkoutPage),
					"<b>Actual Result : </b>User allowed to next step when the User completed the current step.!",
					"<b>Actual Result : </b>User not allowed to next step when the User completed the current step.",
					driver);

			// returning to shopping bag page by clicking the link 'Return to
			// shopping bag'
			checkoutPage.clickReturnToShoppingBag();
			Log.message("12. Returned to shopping bag page after clicking on the ReturnToShoppingBag link.");
			// getting Url after returning back to the shopping page from the
			// link
			String afterURL = driver.getCurrentUrl();

			Log.message("</br>");
			Log.message(
					"<b>Expected Result 3:</b>System should be allowed to navigate the User to a previously completed step which is enabled.");
			Log.assertThat(beforeURL.equals(afterURL), "<b>Actual Result :</b> 'User can navigate to the previous page",
					"<b>Actual Result :</b> 'User can navigate to the previous page");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_140

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify pages navigates to Shipping Address page when clicking Edit link in Shipping Address from Place Order screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_211(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> element = Arrays.asList("lnkReturnToShoppingBag");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			// Selecting the Color
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			// Add the product to the bag

			Log.message("6. Product is added to the cart");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");
			// Loading the SignInPage
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignInPage");
			// clicking on CheckOutAsGuestButton
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. Cliked On CheckOutAsGuest Button");
			// Filling the shipping details
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("9. Shipping details filled");
			// clicking on continue Button
			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button");
			// Navigating to BillingPage
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Navigated to BillingPage");
			// Filling the Billing details
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Billing details filled");
			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			checkoutPage.clickOnContinueInBilling();
			checkoutPage.clickOnEditBySectionName("shippingaddress");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("User should be redirected to the Shipping Address page.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(element, checkoutPage),
					"By the cliking on edit link of shippingPane in PlaceOrderPage it navigates to the Shipping details Page !",
					"By the cliking on edit link of shippingPane in PlaceOrderPage it's not navigated to the Shipping details Page !",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_211

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays Billing address Place Order- Shipping Address panel .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_212(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String firstNameInPlaceOrderPage = new String();
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			// Add the product to the bag
			Log.message("6. Product is added to the cart");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Navigated to shopping bag page");
			// Loading the SignInPage
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Navigated to SignInPage");
			// clicking on CheckOutAsGuestButton
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("9. Cliked On CheckOutAsGuest Button");
			// Filling the shipping details
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("9. Shipping details filled");
			// clicking on continue Button
			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button");
			// Navigating to BillingPage
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Navigated to BillingPage");
			// Filling the Billing details
			LinkedHashMap<String, String> billingDetails = checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("12. Billing details filled");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message(
					"Billing Address should be displayed in Place order page as per the address entered in address page ");
			Log.message("First Name,Last Name,Address 1,Address 2,City,State,Zip code,Phone Number");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			List<String> indexes = new ArrayList<String>(billingDetails.keySet());
			String firstNameInBillingPage = indexes.get(0).replace("type_firstname_", "");
			String lastNameInBillingPage = indexes.get(1).replace("type_firstname_", "");
			String address1InBillingPage = indexes.get(2).replace("type_address_", "");
			String cityInBillingPage = indexes.get(3).replace("type_city_", "");
			String stateInBillingPage = indexes.get(4).replace("select_state_", "");
			String zipCodeInBillingPage = indexes.get(5).replace("type_zipcode_", "");
			String mobileNoInBillingPage = indexes.get(6).replace("type_phoneno_", "");
			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			checkoutPage.clickOnContinueInBilling();
			ArrayList<String> details = checkoutPage.gettingBillingDetailsInPlaceOrder();
			String Name[] = details.get(0).split(" ");
			firstNameInPlaceOrderPage = Name[0];
			String lastNameInPlaceOrderPage = Name[1];
			String Address1InPlaceOrderPage = details.get(1);
			String CityStateZipCodeInPlaceOrderPage[] = details.get(2).split(",");
			String CityInPlaceOrderPage = CityStateZipCodeInPlaceOrderPage[0];
			String stateZipCodeInPlaceOrderPage[] = details.get(2).split(" ");
			String stateInPlaceOrderPage = stateZipCodeInPlaceOrderPage[1];
			String zipCodeInPlaceOrderPage = stateZipCodeInPlaceOrderPage[2];
			String MobileNoInPlaceOrderPage = details.get(3).replace(" ", "").replace("-", "");
			Log.assertThat(firstNameInBillingPage.equals(firstNameInPlaceOrderPage),
					"FirstName in placeOrderPage is same as entered in BillingPage !",
					"FirstName in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(lastNameInBillingPage.equals(lastNameInPlaceOrderPage),
					"LaststName in placeOrderPage is same as entered in BillingPage !",
					"LaststName in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(address1InBillingPage.equals(Address1InPlaceOrderPage),
					"Address1 in placeOrderPage is same as entered in BillingPage !",
					"Address1 in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(StateUtils.getStateCode(stateInBillingPage).equals(stateInPlaceOrderPage),
					"State in placeOrderPage is same as entered in BillingPage !",
					"State in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(cityInBillingPage.equals(CityInPlaceOrderPage),
					"City in placeOrderPage is same as entered in BillingPage !",
					"City in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(zipCodeInBillingPage.equals(zipCodeInPlaceOrderPage),
					"ZipCode in placeOrderPage is same as entered in BillingPage !",
					"ZipCode in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.assertThat(mobileNoInBillingPage.equals(MobileNoInPlaceOrderPage),
					"MobileNo in placeOrderPage is same as entered in BillingPage !",
					"MobileNo in placeOrderPage is not same as entered in BillingPage !", driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_212

	@Test(enabled = false, groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays the Checkout steps for the MultiShip option. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_145(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = myaccount.headers.searchProductKeyword(searchKey);
			Log.message("4. Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("5. Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.addProductToBag();
			Log.message("6. Product added to Shopping Bag!");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigate to checkout page");

			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("9. Clicked 'Yes' Button in Ship to Multiple Address!");

			// Select only first address from the dropdown
			checkoutPage.selectFirstAddressInAddressDropDown();
			Log.message("10. Address selected from Address dropdown!");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();
			checkoutPage.clickContinueInAddressValidationModal();
			Log.message("11. Click continue in multishipping address page!");

			// clicking on the continue button in the shipping method Page.
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Click continue in multishipping shipping method page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("YES", "card_Visa");
			Log.message("13. Card details filled in the billing form!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("14. Click continue in multishipping address page!");

			Log.message("</br>");
			Log.message("<b>Expected Result :</b>Verify system allows User to do Billing for the Multi-ship option.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("PlaceOrderInHeaderEnabledInMultiShipping"), checkoutPage),
					"<b>Actual Result :</b> 'Enabled to go to the previous page",
					"<b>Actual Result :</b> 'Not Enabled to go to the previous page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_145

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Find in store in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_268(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			FindInStorePage findInStorePage = shoppingBagPage.headers.navigateToFindInStore();
			Log.message("5. Clicked 'FindInStore' link on header");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Clicking on 'FindInStore' link should be navigated to FindInStore page");
			Log.message("<b>Actual Result:</b>");

			Log.assertThat(
					Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "store") && findInStorePage != null,
					"6. Navigate to correct URL: " + driver.getCurrentUrl(),
					"6. Navigated to wrong URL: " + driver.getCurrentUrl(), driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_268

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Find in store in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_228(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homePage.headers.navigateToSignIn();
			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("2. Successfully Login to the Application");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("3. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("5. Navigate to checkout Page");

			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Standard", "valid_address7");
			Log.message("6. Filled Shipping Address in Shipping Page");

			LinkedHashMap<String, String> shippingDetails = checkoutPage.getShippingAddressInShippingPage();
			shippingDetails.remove("ShippingMethod");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("7. Navigate to Billing Page After Filled Shipping Details");

			Log.message(
					"<b>Expected Result:</b> The shipping address given in the Shipping page should reflect in 'Billing Address");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(Utils.compareTwoHashMap(checkoutPage.getBillingAddressInBillingPage(), shippingDetails),
					"The Shipping address: " + shippingDetails
							+ " in the Billing page is reflected in 'Billing Adresss': "
							+ checkoutPage.getBillingAddressInBillingPage() + "' of Billing Address page'",
					"The Shipping address: " + shippingDetails
							+ " in the Billing page is not reflected in 'Billing Adresss': "
							+ checkoutPage.getBillingAddressInBillingPage() + "' of Billing Address page'",
					driver);

			checkoutPage.clickOnBelkLogo();
			Log.message("8. Navigate to Home Page");

			myAccountPage = homePage.headers.navigateToMyAccount();
			Log.message("9. Navigate to MyAccount Page");

			AddressBookPage addressBookPage = myAccountPage.navigateToAddressBook();
			Log.message("10. Navigate to AddressBook Page");

			int i = addressBookPage.getDefaultAddress().size();
			LinkedHashMap<String, String> addressBookDetails = addressBookPage.getSavedAddressByIndex(i);
			addressBookDetails.remove("Title");
			addressBookDetails.remove("Defaultshipping");
			addressBookDetails.remove("Defaultbilling");

			Log.message(
					"<b>Expected Result:</b> The shipping address given in the Shipping page should reflect in 'Address Book");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(Utils.compareTwoHashMap(addressBookDetails, shippingDetails),
					"The Shipping address: " + shippingDetails + " in the Shipping page is reflected "
							+ addressBookDetails + "' in Address Book page'",
					"The Shipping address: " + shippingDetails + " in the Shipping page is not reflected "
							+ addressBookDetails + "' in Address Book page'",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_228

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system updates Order summary when User select different Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_172(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			LinkedHashMap<String, String> costDetailsBefore = checkoutPage.getCostDetailsInOrderSummary();
			Log.message("7. Shipping Charge in Order Summary Before Selecting Shipping Type :: "
					+ costDetailsBefore.get("EstimatedShipping"), driver);
			Log.message("8. Order Total in Order Summary Before Selecting Shipping Type :: "
					+ costDetailsBefore.get("EstimatedOrderTotal"));

			checkoutPage.selectShippingMethod("Express");
			Log.message("9. Selected Shipping Type:: Express");

			LinkedHashMap<String, String> costDetailsAfter = checkoutPage.getCostDetailsInOrderSummary();
			Log.message("10. Shipping Charge in Order Summary after Selecting Shipping Type:: "
					+ costDetailsAfter.get("EstimatedShipping"), driver);
			Log.message("11. Order Total in Order Summary After Selecting Shipping Type :: "
					+ costDetailsAfter.get("EstimatedOrderTotal"));

			// float orderTotalBefore =
			// Float.parseFloat(costDetailsBefore.get("EstimatedOrderTotal"));
			// float orderTotalAfter =
			// Float.parseFloat(costDetailsAfter.get("EstimatedOrderTotal"));

			float shippingChargeBefore = Float.parseFloat(costDetailsBefore.get("EstimatedShipping"));
			float shippingChargeAfter = Float.parseFloat(costDetailsAfter.get("EstimatedShipping"));

			float costDetailbefore = 0;
			List<String> keyString = new ArrayList<String>(costDetailsBefore.keySet());
			for (int q = 0; q < costDetailsBefore.size() - 1; q++)
				costDetailbefore += Float.parseFloat(costDetailsBefore.get(keyString.get(q)));
			float before = costDetailbefore - shippingChargeBefore;

			float costDetailafter = 0;
			for (int q = 0; q < costDetailsAfter.size() - 1; q++)
				costDetailafter += Float.parseFloat(costDetailsAfter.get(keyString.get(q)));
			float after = costDetailafter - shippingChargeAfter;

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>When Selecting different shipping methods should update Order Total and Shipping charge should be updated properly.");
			Log.assertThat(before == after,
					"<b>Actual Result: </b>Selecting different shipping methods updated Order Total and Shipping charge properly",
					"<b>Actual Result: </b>Selecting different shipping methods did not updated Order Total and Shipping charge properly");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_172

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system display the error message for Zero Balance or Insufficient funds Gift card - Belk Gift Card as Payment option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_179(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest(address, "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest(address);
			checkoutPage.fillingBelkGiftCardDetails("GiftCard_Zero_Balance");
			checkoutPage.clickOnApplyGiftCard();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Insufficient Finds or 0 balance Error should come.");
			Log.assertThat(!checkoutPage.getGiftCardErrMsg().equals("No Error Message Shown!"),
					"<b>Actual Result: </b>Error Message displayed:: " + checkoutPage.getGiftCardErrMsg(),
					"<b>Actual Result: </b>Gift Card applied Successfully!");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_179

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify the Belk Gift Card section in Payment Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_188(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest(address, "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Continued to Billing Tab in checkout page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>Giftcard Message, Giftvard Number and PIN fields and Apply Giftcard Button should be displayed.");
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(
					Arrays.asList("lblRewardMsg", "belkGiftCard", "belkGiftCardCvv", "btnGiftCard"), checkoutPage),
					"<b>Actual Result: </b>Giftcard Message, Giftvard Number and PIN fields and Apply Giftcard Button displayed",
					"<b>Actual Result: </b>Giftcard Message, Giftvard Number and PIN fields and Apply Giftcard Button not displayed");

			checkoutPage.fillingBillingDetailsAsGuest(address);
			checkoutPage.fillingBelkGiftCardDetails("GiftCard3");
			checkoutPage.clickOnApplyGiftCard();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Giftcard has been applied' Message should be displayed.");
			Log.softAssertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblGiftcardSuccessMsg"), checkoutPage),
					"<b>Actual Result: </b>Success Message displayed!",
					"<b>Actual Result: </b>Success Message not displayed!");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_188

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify that error message is shown if user tries to add more than 10 Belk Gift Cards.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_190(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity("2");
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest(address, "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest(address);
			for (int i = 1; i <= 10; i++) {
				checkoutPage.fillingBelkGiftCardDetails("GiftCard" + i);
				checkoutPage.clickOnApplyGiftCard();
			}

			checkoutPage.fillingBelkGiftCardDetails("GiftCard11");
			checkoutPage.clickOnApplyGiftCard();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b>User should see an error message specifying that Gift Card limit has exceeded.");
			Log.assertThat(!checkoutPage.getGiftCardErrMsg().equals("No Error Message Shown!"),
					"<b>Actual Result: </b>Error Message displayed:: " + checkoutPage.getGiftCardErrMsg(),
					"<b>Actual Result: </b>Gift Card applied Successfully!");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_190

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify that user is allowed to use a third-party Credit Card using a Gift Card.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_191(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest(address, "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest(address);
			checkoutPage.fillingBelkGiftCardDetails("GiftCard3");

			checkoutPage.clickOnCheckGiftCardBalance();
			float gcBalance = Float.parseFloat(checkoutPage.getGiftCardBalance());
			checkoutPage.clickOnApplyGiftCard();
			Log.message("9. Gift card Applied successfully!", driver);
			float gcUsed = Float.parseFloat(checkoutPage.getGiftCardAmountInSection());

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Maximux balance should be deducted from Gift Card.");
			Log.assertThat(gcBalance == gcUsed, "<b>Actual Result: </b>Maximum Balance deducted from gift card!",
					"<b>Actual Result: </b>Maximum Balance not deducted from Gift card", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>User should be allowed to add valid third-party credit cards.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentMethod"), checkoutPage),
					"<b>Actual Result: </b>User allowed to add valid third-party credit cards.",
					"<b>Actual Result: </b>User not allowed to add valid third-party credit cards.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_191

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify the Applied Gift Card Section.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_193(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String address = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest(address, "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest(address);
			checkoutPage.fillingBelkGiftCardDetails("GiftCard3");

			checkoutPage.clickOnCheckGiftCardBalance();
			float orderTotal = Float.parseFloat(checkoutPage.GetOrderTotalValue().replace("$", "").trim());
			checkoutPage.clickOnApplyGiftCard();
			float gcUsed = Float.parseFloat(checkoutPage.getGiftCardAmountInSection());

			if (orderTotal > gcUsed) {
				Log.message("<br>");
				Log.message(
						"<b>Expected Result: </b>If order total paid fully, Other payments sections should be disabled.");
				Log.assertThat(
						!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentMethod"),
								checkoutPage),
						"<b>Actual Result: </b>Other Payment sections disabled successfully!",
						"<b>Actual Result: </b>Other Payment sections not disabled", driver);
			} else
				Log.message("   --->>> Order total amount is lesser than Giftcard Balance amount!");
			LinkedHashMap<String, String> gcPaymentInfo = checkoutPage.getGiftCardPaymentDetailsInPlaceOrder();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Only 4 characters of Gift card should be displayed.");
			Log.assertThat(gcPaymentInfo.get("GiftCardNumber").replace("*", "").trim().length() == 4,
					"<b>Actual Result: </b>Only 4 characters of Gift card displayed!",
					"<b>Actual Result: </b>More than 4 characters of Gift card is displayed.", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The amount applied toward the order shoud be displayed.");
			Log.assertThat(!gcPaymentInfo.get("Amount").trim().isEmpty(),
					"<b>Actual Result: </b>The amount applied toward the order displayed!",
					"<b>Actual Result: </b>The amount applied toward the order not displayed.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_193

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows customer to add only 10 Gift cards.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_180(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] giftcard = testData.get("Giftcard").split("\\|");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity("2");
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6. 'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			for (int i = 0; i < giftcard.length; i++) {
				checkoutPage.fillingBelkGiftCardDetails(giftcard[i]);
				checkoutPage.clickOnApplyBelkGiftLink();
			}

			Log.message(
					"<br><b>Expected Result:</b> Warning message should shown, when apply more then '10' gift cards");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errMsgGiftCard"), checkoutPage),
					"Warning message is display when apply more then '10' gift cards",
					"Warning message is not display when apply more then '10' gift cards", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_180

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows User to check Belk Gift Card balance from billing Address screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_181(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6. 'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnCheckGiftCardBalance();
			Log.message("11. Clicked on 'check GiftCard Balance'");

			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'checkGiftCardBalance', the Balance amount should be displyed in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.getGiftCardBalanceMessage().contains("Gift Card Balance"),
					"By clicking on 'checkGiftCardBalance', the Balance amount is displyed in the 'Billing' Page",
					"By clicking on 'checkGiftCardBalance', the Balance amount is note displyed in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_181

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Selecting or Adding Shipping Address in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_227(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signIn = homePage.headers.navigateToSignIn();
			CreateAccountPage createAccountPage = signIn.clickCreateAccount();
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage.CreateAccount(createAccountPage);
			Log.message("2. Successfully Login to the Application");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("3. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("5. Navigate to checkout Page");

			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", "Standard", "valid_address7");
			Log.message("6. Filled Shipping Address in Shipping Page");

			LinkedHashMap<String, String> shippingDetails = checkoutPage.getShippingAddressInShippingPage();
			shippingDetails.remove("ShippingMethod");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("7. Navigate to Billing Page After Filled Shipping Details");

			Log.message(
					"<b>Expected Result:</b> The shipping address given in the Shipping page should reflect in 'Billing Address");
			Log.message("<b>Actual Result:</b>");
			Log.softAssertThat(Utils.compareTwoHashMap(checkoutPage.getBillingAddressInBillingPage(), shippingDetails),
					"The Shipping address: " + shippingDetails
							+ " in the Billing page is reflected in 'Billing Adresss': "
							+ checkoutPage.getBillingAddressInBillingPage() + "' of Billing Address page'",
					"The Shipping address: " + shippingDetails
							+ " in the Billing page is not reflected in 'Billing Adresss': "
							+ checkoutPage.getBillingAddressInBillingPage() + "' of Billing Address page'",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_227

	@Test(groups = {
			"mobile" }, description = "Mobile : Verify system displays the Mini Cart page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message((i++) + ". Navigated to " + searchKey + " Search Result Page!");

			PdpPage pdppage = searchresultpage.navigateToPDP();
			Log.message((i++) + ". Navigated to  Pdp Page!");

			pdppage.addProductToBag();
			Log.message((i++) + ". Selected product is added to bag from PdpPage!");

			ShoppingBagPage shoppingbagpage = pdppage.minicart.navigateToBag();
			Log.message((i++) + ". Tapped on the mini cart icon");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b>Mini Cart should be displayed on tapping Cart icon");
			Log.assertThat(
					shoppingbagpage != null
							&& Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "Cart-Show"),
					"<b>Actual Result:</b> Mini Cart is displayed on tapping Cart icon",
					"<b>Actual Result:</b> Mini Cart is not displayed on tapping Cart icon", driver);
			Log.testCaseResult();
		}
		// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_042

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system navigates the User to Shopping bag page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_142(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message((i++) + ". Filled shipping details successfully");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on Continue In Shipping page");
			checkOutPage.chooseOriginalAddressInModal();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". Navigated to Billing page");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message((i++) + ". Filled billing details successfully");
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Filled payment vard details successfully");
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on Continue In Shipping page");
			Log.message((i++) + ". Navigated to Place Order page");
			checkOutPage.clickReturnToBilling();
			Log.message((i++) + ". Clicked 'Return To Billing' link");
			Log.message(
					"<b>Expected Result: </b>User should be navigated to Billing page and Checkout header should have 2. Billing' as active.");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("BillingInHeaderEnabled"), checkOutPage),
					"<b>Actual Result: </b>User is navigated to Billing page and Checkout header have 2. Billing' as active",
					"<b>Actual Result: </b>User is not navigated to Billing page and Checkout header do not have 2. Billing' as active",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_142

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system allows User to add multiple shipping Address in Shipping Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_144(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Logged in with ( " + emailid + " / " + password + " )");
			ShoppingBagPage shoppingBagPage = null;
			PdpPage pdpPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			} else {
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			}

			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag("2");
			Log.message((i++) + ". Selected product added to shopping cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			checkOutPage.clickOnYesInShipToMultipleAddress();
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			shipToMultipleAddressPage.selectdropdownandAddress(1, 2);
			Log.message((i++) + ". First Item's address dropdown is selected !");
			shipToMultipleAddressPage.selectdropdownandAddress(2, 6);
			Log.message((i++) + ". Second Item's address dropdown is selected !");
			// filling shipping details
			ShippingMethodPage shippingMethodPage = shipToMultipleAddressPage.clickContinue();
			Log.message((i++) + ". Clicked 'Continue' button");
			Log.message(
					"<b>Expected Result: </b>MultiShip Checkout 'Step 2 Shipping Method' page should be displayed with Shipping Methods dropdown once User click on the 'Shipping Method'");
			Log.assertThat(shippingMethodPage.verifyNavigatedToShippingMethodPage(),
					"<b>Actual Result: </b>MultiShip Checkout 'Step 2 Shipping Method' page is displayed with Shipping Methods dropdown once User click on the 'Shipping Method'",
					"<b>Actual Result: </b>MultiShip Checkout 'Step 2 Shipping Method' page is not displayed with Shipping Methods dropdown once User click on the 'Shipping Method'",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_144

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system updates Order summary when User select different Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_170(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			LinkedHashMap<String, String> shippingDetails = checkOutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Standard");
			Log.message((i++) + ". Selected 'Standard' shipping method");
			String expectedShippingCost = shippingDetails.get("Shipping_method").split("\\$")[1];
			String actualShippingCost = checkOutPage.shippingCostInPlaceOrder().replace("$", "");
			Log.message(
					"<b>Expected Result: </b>Order Summary pane 'Estimated Order Total' should be properly calculated as per the selected Shipping Method");
			Log.assertThat(expectedShippingCost.equals(actualShippingCost),
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is properly calculated as per the selected Shipping Method",
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is not properly calculated as per the selected Shipping Method",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_142

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify pages navigates from My Bag screen when clicking Edit link in Order Summary pane from Place Order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_205(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey1 = testData.get("SearchKey").split("\\|")[0];
		String searchKey2 = testData.get("SearchKey").split("\\|")[1];
		String coupon = testData.get("CouponCode");
		String lblCouponMessage = "Coupon " + coupon + " has been applied.";
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey1);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey1);

			// add product to cart
			String pdtName = pdpPage.getProductName();
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message((i++) + ". Filled shipping details successfully");
			// clicking continue button
			checkOutPage.clickOnContinueInShipping();
			Log.message((i++) + ". Clicked on Continue In Shipping page");
			checkOutPage.chooseOriginalAddressInModal();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". Navigated to Billing page");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message((i++) + ". Filled billing details successfully");
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Filled payment vard details successfully");
			checkOutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked on Continue In Shipping page");
			Log.message((i++) + ". Navigated to Place Order page");
			shoppingBagPage = checkOutPage.clickOnEditInOrderSummary();
			String qtyValueBeforeEditing = shoppingBagPage.getQuantityValue(0);
			shoppingBagPage.selectQuantity("2");
			String qtyValueAfterEditing = shoppingBagPage.getQuantityValue(0);
			Log.message(
					"<br><b>Expected Result: </b>User should be allowed to edit the added item from My Bag screen.");
			Log.softAssertThat(!qtyValueBeforeEditing.equals(qtyValueAfterEditing),
					"<b>Actual Result: </b>User is allowed to edit the added item from My Bag screen.",
					"<b>Actual Result: </b>User is not allowed to edit the added item from My Bag screen.", driver);

			Log.message("<br><b>Expected Result: </b>User should be allowed add Wish list from My Bag screen.");
			shoppingBagPage.clickOnAddToWishList(pdtName);
			Log.softAssertThat(Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "wishlist"),
					"<b>Actual Result: </b>User is allowed to add Wish list from My Bag screen.",
					"<b>Actual Result: </b>User is not allowed to add Wish list from My Bag screen.", driver);

			driver.navigate().back();
			Log.message("<br><b>Expected Result: </b>User should be allowed add Registry from My Bag screen.");
			shoppingBagPage.clickOnRegistry(pdtName);
			Log.softAssertThat(Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "giftregistry"),
					"<b>Actual Result: </b>User is allowed to add Registry from My Bag screen.",
					"<b>Actual Result: </b>User is not allowed to add Registry from My Bag screen.", driver);

			driver.navigate().back();

			shoppingBagPage.applyCouponInShoppingBag(coupon);
			Log.message("<br>");
			Log.message((i++) + ". Applied coupon : '" + coupon + "'");
			Log.message("<br><b>Expected Result: </b>User should be allowed apply coupon code from My Bag screen.");

			Log.softAssertThat(shoppingBagPage.getCouponSuccessMsg().contains(lblCouponMessage),
					"<b>Actual Result: </b>User is allowed to apply coupon code : '" + coupon + "' from My Bag screen.",
					"<b>Actual Result: </b>User is not allowed to apply coupon code : '" + coupon
							+ "' from My Bag screen.");

			Log.message(
					"<br><b>Expected Result: </b>User should be allowed add new items by clicking on 'Continue Shopping' button.");
			int pdtCountBeforeAddingNewProduct = shoppingBagPage.getProductCount();
			shoppingBagPage.clickContinueShopping();
			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey2);
			pdpPage.addProductToBag();
			shoppingBagPage = pdpPage.navigateToBagPage();
			int pdtCountAfterAddingNewProduct = shoppingBagPage.getProductCount();
			Log.softAssertThat(pdtCountAfterAddingNewProduct > pdtCountBeforeAddingNewProduct,
					"<b>Actual Result: </b>User is allowed to add new items by clicking on 'Continue Shopping' button.",
					"<b>Actual Result: </b>User is not allowed to add new items by clicking on 'Continue Shopping' button.",
					driver);

			signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("<br>");
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			Log.message(
					"<br><b>Expected Result: </b>User should be allowed redirect to Shipping Address screen by clicking 'Checkout' button from My Bag screen.");
			Log.softAssertThat(Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "COShipping-Start"),
					"<b>Actual Result: </b>User is redirected to Shipping Address screen by clicking 'Checkout' button from My Bag screen",
					"<b>Actual Result: </b>User is not redirected to Shipping Address screen by clicking 'Checkout' button from My Bag screen",
					driver);

			driver.navigate().back();
			driver.navigate().back();
			int pdtCountBeforeRemoving = shoppingBagPage.getProductCount();
			Log.message(
					"<br><b>Expected Result: </b>User should be allowed to remove the added item from My Bag screen.");
			shoppingBagPage.removeItemsFromBag();
			int pdtCountAfterRemoving = shoppingBagPage.getProductCount();
			Log.softAssertThat(pdtCountBeforeRemoving > pdtCountAfterRemoving,
					"<b>Actual Result: </b>User is allowed to remove the added item from My Bag screen.",
					"<b>Actual Result: </b>User is not allowed to remove the added item from My Bag screen.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_205

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Return to Shipping link in Checkout-Billing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_242(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Logged in with ( " + emailid + " / " + password + " )");
			ShoppingBagPage shoppingBagPage = null;
			PdpPage pdpPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			} else {
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			}

			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++) + ". Navigated to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address2");
			Log.message((i++) + ". Shipping details are entered in Checkout Page!");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". Clicked on 'Continue' button in Shipping page");
			checkoutPage.clickReturnToShipping();
			Log.message("<b>Expected Result: </b>User should be redirected to the Checkout-Shipping page.");
			Log.assertThat(checkoutPage.getTextFromBillingHeader().equals("Return to Shopping Bag"),
					"<b>Actual Result: </b>User is redirected to the Checkout-Shipping page.",
					"<b>Actual Result: </b>User is not redirected to the Checkout-Shipping page.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_242

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Place Order details in Checkout page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_243(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		int i = 1;
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Logged in with ( " + emailid + " / " + password + " )");
			ShoppingBagPage shoppingBagPage = null;
			PdpPage pdpPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			} else {
				pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			}

			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");
			CheckoutPage checkoutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++) + ". Navigated to checkout page");

			// Filling shippingDetails
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address2");
			Log.message((i++) + ". Shipping details are entered in Checkout Page");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message((i++) + ". Clicked 'Continue' button in Shipping page");
			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message((i++) + ". Filled billing details");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Filled the payment card details");
			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Clicked 'Continue' button in Billing page");
			Log.message(
					"<b>Expected Result: </b>The following section should be available in the Place Order page:<br> 1. Return to Billing link below the Belk logo <br> 2. Product with Price details<br> 3. Cancel button <br> 4. Place Order button <br> 5. Order Summary with Edit link <br> 6. Shipping Address with Edit link <br> 7. Billing Address with Edit link <br> 8. Payment Method with Edit link <br> 9. Shop With Confidence options above the footer");
			Log.message("<b>Actual Result: </b>");
			int j = 1;
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("ReturnToshipPage"), checkoutPage),
					(j++) + ". Return to Billing link below the Belk logo is available",
					"Return to Billing link below the Belk logo is not available", driver);
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSubtotal"), checkoutPage),
					(j++) + ". Product with Price details is available", "Product with Price details is not available",
					driver);
			if (runPltfrm == "desktop") {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnCancelInPlaceOrderDesktop"),
								checkoutPage),
						(j++) + ". Cancel button is available", "Cancel button is not available", driver);
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrderdesktop"),
								checkoutPage),
						(j++) + ". Place Order button is available", "Place Order button is not available");
			} else {
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnCancelInPlaceOrderMobile"),
								checkoutPage),
						(j++) + ". Cancel button is available", "Cancel button is not available", driver);
				Log.assertThat(
						checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrdermobile"),
								checkoutPage),
						(j++) + ". Place Order button is available", "Place Order button is not available");
			}
			Log.assertThat(checkoutPage.verifyEditLinkBySectionName("ordersummary"),
					(j++) + ". Order Summary with Edit link is available",
					"Order Summary with Edit link is not available", driver);
			Log.assertThat(checkoutPage.verifyEditLinkBySectionName("shippingaddress"),
					(j++) + ". Shipping Address with Edit link is available",
					"Shipping Address with Edit link is not available", driver);
			Log.assertThat(checkoutPage.verifyEditLinkBySectionName("billingaddress"),
					(j++) + ". Billing Address with Edit link is available",
					"Billing Address with Edit link is not available", driver);
			Log.assertThat(checkoutPage.verifyEditLinkBySectionName("paymentmethod"),
					(j++) + ". Payment Method with Edit link is available",
					"Payment Method with Edit link is not available", driver);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblShopWithConfidence"), checkoutPage),
					(j++) + ". Shop With Confidence options above the footer is available",
					"Shop With Confidence options above the footer is not available", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_243

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system allows customer to add Address by using Add Address link- Guest user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_257(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag("2");
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			checkOutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on Yes in ship to multiple address");
			Log.message(
					"<b>Expected Result: </b>Add Address link is included in the drop down list for all line items as the customer continues");
			checkOutPage.clickOnAddInShipToMulipleAddressByRow(1);
			Log.message("<br>");
			Log.message((i++) + ". Clicked on the address link for 1st product");
			Log.message("<br>");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("dialogMultiShippingAddress"),
							checkOutPage),
					"<b>Actual Result1: </b>Add Address link is included in the drop down list for product 1 as the customer continues",
					"<b>Actual Result1: </b>Add Address link is not included in the drop down list for product 1 as the customer continues",
					driver);
			checkOutPage.clickOnCloseShippingModelPopup();
			checkOutPage.clickOnAddInShipToMulipleAddressByRow(2);
			Log.message("<br>");
			Log.message((i++) + ". Clicked on the address link for 1st product");
			Log.message("<br>");
			Log.assertThat(
					checkOutPage.elementLayer.verifyPageElements(Arrays.asList("dialogMultiShippingAddress"),
							checkOutPage),
					"<b>Actual Result1: </b>Add Address link is included in the drop down list for product 2 as the customer continues",
					"<b>Actual Result1: </b>Add Address link is not included in the drop down list for product 2 as the customer continues",
					driver);
			checkOutPage.clickOnCloseShippingModelPopup();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_257

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system navigates to Home page from Order Receipt Screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_219(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String color;
		String size;
		List<String> element = Arrays.asList("headerNav");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("3. Navigated to PDP Page !");
			// select the color
			color = pdpPage.selectColor();
			Log.message("4. selected the color : " + color);
			// Select the size
			size = pdpPage.selectSize();
			Log.message("5. selected the size : " + size);
			pdpPage.addProductToBag();
			Log.message("6. Product is added to the Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Navigated to shopping bag page");
			// Navigating to signIn page
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Navigated to SignIn page !");
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. clicked on checkOutAsGuest Button !");
			checkOutPage.fillingShippingDetailsAsGuest("valid_address6", "Standard");
			Log.message("9. filled shipping details !");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. clicked on continue Button !");

			checkOutPage.fillingBillingDetailsAsGuest("valid_address6");
			Log.message("11. filled billing details !");
			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("12. filled card details !");
			checkOutPage.clickOnContinueInBilling();
			Log.message("13. clicked on continue Button in Billing Page !");

			OrderConfirmationPage orderConfirmationPage = checkOutPage.ClickOnPlaceOrderButton();
			Log.message("14. clicked on placeOrder Button !");

			orderConfirmationPage.ClickOnReturnToShoppingBtn();
			Log.message("15. clicked on returnToShopping Button !");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b>");
			Log.message(
					"Home page should be displayed when User click on the 'Return to Shopping' button from Place Order screen.");
			Log.message("<br>");
			Log.message("<b>Actual Result 1:</b>");

			String URL = driver.getCurrentUrl();
			Log.assertThat(URL.contains("home"), "After clicking on returnToShopping page navigated to HomePage !",
					"After clicking on returnToShopping page not navigated to HomePage !", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b>");
			Log.message("Category header and product details should be properly shown");
			Log.message("<br>");
			Log.message("<b>Actual Result 2:</b>");

			Log.assertThat(homePage.headers.elementLayer.verifyPageElements(element, homePage.headers),
					"Category header and product details properly shown !",
					"Category header and product details not properly shown !", driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_219

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Save to Address Book and Don’t Miss out options in Checkout-Shipping page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_238(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String EmailId = testData.get("EmailAddress");
		String Password = testData.get("Password");

		String color;
		String size;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Navigating to signIn page
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAcc = signIn.signInToMyAccount(EmailId, Password);
			Log.message("2. Entered email id as '" + EmailId + "' and password '" + Password
					+ "' and clicked signIn button !");

			ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultpage = homePage.searchProductKeyword(searchKey);
			Log.message("3. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			// Load the PDP Page with search keyword
			PdpPage pdpPage = searchResultpage.selectProduct();
			Log.message("4. Navigated to PDP Page !");
			// select the color
			color = pdpPage.selectColor();
			Log.message("5. selected the color : " + color);
			// Select the size
			size = pdpPage.selectSize();
			Log.message("6. selected the size : " + size);
			pdpPage.addProductToBag();
			Log.message("7. Product is added to the Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Clicked on Minicart and Navigated to shopping bag page !");
			CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			checkOutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address4");
			Log.message("9. filled shipping details !");
			checkOutPage.clickOnContinueInShipping();
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. clicked on continue Button !");

			checkOutPage.fillingBillingDetailsAsSignedInUser("YES", "YES", "valid_address7");
			Log.message("11. filled billing details !");
			LinkedHashMap<String, String> BillingAddress = checkOutPage.getBillingAddressInBillingPage();

			String Address = BillingAddress.get("Address").trim();
			String city = BillingAddress.get("City").trim();

			checkOutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("12. filled card details !");
			checkOutPage.clickOnContinueInBilling();
			Log.message("13. clicked on continue Button in Billing Page !");

			checkOutPage.ClickOnPlaceOrderButton();
			Log.message("14. clicked on placeOrder Button and Navigated to order Confirmation Page !");
			homePage.headers.navigateToMyAccount();

			AddressBookPage addressBookPage = myAcc.navigateToAddressBook();
			LinkedHashMap<String, String> SavedAddress = addressBookPage.getSavedAddressByIndex(0);

			String AddressInAddressBook = SavedAddress.get("Address1").trim();
			String cityInAddressBook = SavedAddress.get("City").trim();
			boolean Status = false;
			if (Address.equals(AddressInAddressBook) && city.equals(cityInAddressBook)) {
				Status = true;
			} else {
				Status = false;
			}
			Log.message("<br>");
			Log.message("<b>Expected Result :</b>");
			Log.message(
					"The new billing address should be saved to the Address Book and the 'Save to Address Book' should be unchecked by default.");
			Log.message("<br>");
			Log.message("<b>Actual Result :</b>");

			Log.assertThat(Status,
					"The new billing address saved to the Address Book and the 'Save to Address Book' unchecked by default.",
					"The new billing address not saved to the Address Book and the 'Save to Address Book' not unchecked by default.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_238

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system updates Order summary when User select different Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_179(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			LinkedHashMap<String, String> shippingDetails = checkOutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Standard");
			Log.message((i++) + ". Selected 'TestShipment' shipping method");
			String expectedShippingCost = shippingDetails.get("Shipping_method").split("\\$")[1];
			String actualShippingCost = checkOutPage.shippingCostInPlaceOrder().replace("$", "");
			Log.message(
					"<b>Expected Result: </b>Order Summary pane 'Estimated Order Total' should be properly calculated as per the selected Shipping Method");
			Log.assertThat(expectedShippingCost.equals(actualShippingCost),
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is properly calculated as per the selected Shipping Method",
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is not properly calculated as per the selected Shipping Method",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_179

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system updates Order summary when User select different Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_180(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			LinkedHashMap<String, String> shippingDetails = checkOutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Express");
			Log.message((i++) + ". Selected 'Express' shipping method");
			String expectedShippingCost = shippingDetails.get("Shipping_method").split("\\$")[1];
			String actualShippingCost = checkOutPage.shippingCostInPlaceOrder().replace("$", "");
			Log.message(
					"<b>Expected Result: </b>Order Summary pane 'Estimated Order Total' should be properly calculated as per the selected Shipping Method");
			Log.assertThat(expectedShippingCost.equals(actualShippingCost),
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is properly calculated as per the selected Shipping Method",
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is not properly calculated as per the selected Shipping Method",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_180

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify system updates Order summary when User select different Shipping Method", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_182(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");
			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". Clicked on Checkout As Guest Button");
			// filling shipping details
			LinkedHashMap<String, String> shippingDetails = checkOutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Overnight");
			Log.message((i++) + ". Selected 'Overnight' shipping method");
			String expectedShippingCost = shippingDetails.get("Shipping_method").split("\\$")[1];
			String actualShippingCost = checkOutPage.shippingCostInPlaceOrder().replace("$", "");
			Log.message(
					"<b>Expected Result: </b>Order Summary pane 'Estimated Order Total' should be properly calculated as per the selected Shipping Method");
			Log.assertThat(expectedShippingCost.equals(actualShippingCost),
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is properly calculated as per the selected Shipping Method",
					"<b>Actual Result: </b>Order Summary pane 'Estimated Order Total' is not properly calculated as per the selected Shipping Method",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_182

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify user is taken to Shipping Address page after entering valid account details.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_071(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			// clean up
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
			ShoppingBagPage bagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.navigateToMyAccount();
				myAccountPage.clickOnLogout();

			}

			else {
				myAccountPage.clickOnLogout();
			}

			PdpPage pdpPage = signIn.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart

			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");

			// clicking Checkout As Guest
			CheckoutPage checkOutPage = signIn.clickOnCheckoutAsUser(username, passwd);
			Log.message((i++) + ". Clicked on Checkout Button");
			Log.message("<b>Expected Result: </b>User should be navigated to Step 1 Shipping Address page.");

			Log.assertThat(driver.getCurrentUrl().contains("COShipping-Start") && checkOutPage != null,
					"<b>Actual Result: </b>User navigated to Step 1 Shipping Address page.",
					"<b>Actual Result: </b>User is not navigated to Step 1 Shipping Address page.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_071

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify user is shown an error message on entering invalid account details.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_072(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		int i = 1;
		String msg = "Sorry, this does not match our records. Check your spelling and try again.";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Navigated to PDP page with search key : " + searchKey);

			// add product to cart

			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping cart page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page");

			// clicking Checkout As Guest
			signIn.signInToAccount(username, passwd);
			Log.message((i++) + ". Clicked on Checkout Button");
			Log.message("<b>Expected Result: </b>User should be shown the error message");

			Log.assertThat(
					signIn.elementLayer.verifyPageElements(Arrays.asList("formError"), signIn)
							&& signIn.getSignInFormErrorMsg().contains(msg),
					"<b>Actual Result: </b>User see the error message '" + signIn.getSignInFormErrorMsg() + "'",
					"<b>Actual Result: </b>User can't see the error message ", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_072

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify that the items in a signed user cart are retained.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_082(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			// clean up
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
			ShoppingBagPage bagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.navigateToMyAccount();
				myAccountPage.clickOnLogout();

			}

			else {
				myAccountPage.clickOnLogout();
			}

			myAccountPage = signIn.signInToMyAccount(username, passwd);
			Log.message((i++) + ". Entered credentials(" + username + "/" + passwd
					+ ") and navigating to 'My Account' page");

			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with search key : " + searchKey);
			PdpPage pdpPage = searchResultPage.navigateToPDP();
			Log.message((i++) + ". Navigated to PDP page");
			pdpPage.selectColor();
			pdpPage.selectSize();
			String pdtName = pdpPage.getProductName();
			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");
			myAccountPage = pdpPage.headers.navigateToMyAccount();
			Log.message((i++) + ". Navigated to My Account page");
			signIn = myAccountPage.clickOnLogout();
			Log.message((i++) + ". Clicked Logout button");
			myAccountPage = signIn.signInToMyAccount(username, passwd);
			Log.message((i++) + ". Navigated to My Account page again by giving the same credentials");
			ShoppingBagPage shoppingBagPage = myAccountPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to Bag page");

			Log.message("<b>Expected Result: </b>User should be on Bag Page with previously added products.");

			Log.assertThat(driver.getCurrentUrl().contains("Cart-Show") && shoppingBagPage.PrdName().contains(pdtName),
					"<b>Actual Result: </b>User is on Bag Page with previously added product '" + pdtName + "'",
					"<b>Actual Result: </b>User is not on Bag Page with previously added product", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_082

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify that Bag Merge i.e., products from bag are clubbed when an user signs in after having products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_083(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey1 = testData.get("SearchKey").split("\\|")[0];
		String searchKey2 = testData.get("SearchKey").split("\\|")[1];
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		int i = 1;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");

			// clean up
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
			ShoppingBagPage bagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.navigateToMyAccount();
				myAccountPage.clickOnLogout();

			}

			else {
				myAccountPage.clickOnLogout();
			}
			// Pre - requiste
			myAccountPage = signIn.signInToMyAccount(username, passwd);
			SearchResultPage searchResultPage = myAccountPage.headers.searchProductKeyword(searchKey1);
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.addProductToBag();
			myAccountPage = pdpPage.headers.navigateToMyAccount();
			signIn = myAccountPage.clickOnLogout();

			searchResultPage = signIn.headers.searchProductKeyword(searchKey2);
			Log.message((i++) + ". Searched with search key : " + searchKey2);
			pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message((i++) + ". Navigated to PDP page");
			pdpPage.selectColor();
			pdpPage.selectSize();

			pdpPage.addProductToBag();
			Log.message((i++) + ". Selected product added to shopping cart");
			signIn = pdpPage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to Signin page");
			ShoppingBagPage shoppingBagPage = signIn.signInFromShoppingBag(username, passwd);
			Log.message((i++) + ". Entered credentials(" + username + "/" + passwd
					+ ") and navigating to Shopping bag page");

			Log.message("<b>Expected Result: </b>signed in user should be available in Bag after user signs in.");

			Log.assertThat(
					driver.getCurrentUrl().contains("Cart-Show?basketUpdateList=&basketGlobalUpdate=merge")
							&& shoppingBagPage.getBagmergeGloballevelalert(),
					"<b>Actual Result: </b>signed in user are available in Bag with Bag merge message'"
							+ shoppingBagPage.getGlobalMessage() + "'",
					"<b>Actual Result: </b>signed in user are not available in Bag after user signs in.", driver);

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_083

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify  system allows User to add multiple shipping Address in Shipping Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_153(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey").split("S_")[1];
		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");
		int i = 1;
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signIn.signInToMyAccount(username, passwd);
			ShoppingBagPage bagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.navigateToMyAccount();
				myAccountPage.clickOnLogout();

			}

			else {
				myAccountPage.clickOnLogout();
			}

			myAccountPage = signIn.signInToMyAccount(username, passwd);
			Log.message((i++) + ". Entered credentials(" + username + "/" + passwd
					+ ") and navigating to 'My Account' page");

			PdpPage pdpPage = myAccountPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey + "");
			Log.message((i++) + ". Navigated to PDP");
			pdpPage.selectQuantity("2");
			String pdtDetails = pdpPage.addProductToBag();
			Log.message((i++) + ". selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message((i++) + ". selected color: " + pdtDetails.split("\\|")[0]);

			Log.message((i++) + ". Product is added to the cart");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Minicart icon is clicked and navigated to Shopping bag page");

			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to Checkout as SignedUser!");
			checkoutpage.clickOnYesInShipToMultipleAddress();
			Log.message((i++) + ". Clicked on Yes in ship to Multi Address");

			Log.message("<b>Expected Result: </b> MultiShip Checkout Step 1 Shipping Address page should be displayed");

			Log.assertThat(driver.getCurrentUrl().contains("COShipping-SingleShipping"),
					"<b>Actual Result: </b>MultiShip Checkout Step 1 Shipping Address page is displayed",
					"<b>Actual Result: </b>MultiShip Checkout Step 1 Shipping Address page is not displayed", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_153

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Shopping Bag icon on the global header", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_027(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result-1:</b> The 'Shopping bag' has  no items , then the mouse over state should be disabled.");

			Log.assertThat(
					((homePage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnViewBag"), homePage))),
					"<b>Actual Result-1:</b> 'The 'Shopping bag' has  no items' , then the mouse over state is disabled.",
					"<b>Actual Result-1:</b> 'The 'Shopping bag' has  no items' , then the mouse over state is not disabled.");
			Log.message("<br>");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword " + searchKey);

			Log.message("3. Navigated to pdpPage with productId");

			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("4. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("5. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("6. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("7. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("8. Color, size and quantity are selected and 'Add to shopping bag' button is clicked");

			Log.message("9. Product is added to the cart");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result-2:</b> After adding the item , mouse hover the shopping bag icon in the header, the mini?cart should open ");

			Log.assertThat(homePage.checkViewBag(),
					"<b>Actual Result-2:</b> After adding the item, mouse hover the shopping bag icon in the header, the minicart is opened",
					"<b>Actual Result-2:</b> After adding the item, mouse hover the shopping bag icon in the header, the minicart is not opened",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_CART_027

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system updates 'Order Summary' when Gift Card is applied.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_185(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' button");

			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'ApplyGiftCard', the GiftCard should be applied and details should be updated in OrderSummary in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					((checkoutPage.getGiftCardAppliedMessage().contains("Gift card has been applied"))
							&& (checkoutPage.getPaymentDetails().contains("Gift Card"))),
					"By clicking on 'ApplyGiftCard', the GiftCard is applied and details are updated in OrderSummary in the 'Billing' Page",
					"By clicking on 'ApplyGiftCard', the GiftCard is not applied and details are not updated in OrderSummary in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_185

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays the last four digit of Gift card and amount applied to the Order.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_186(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' button");

			Log.message(checkoutPage.getGiftcardAmountInBillingPage());
			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'ApplyGiftCard',  Last four Digit of the Gift card and Amount applied towards the Order should be displayed in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					(checkoutPage.getPaymentDetails().contains("***************")
							&& (checkoutPage.getGiftcardAmountInBillingPage().contains("$"))),
					"By clicking on 'ApplyGiftCard',  Last four Digit of the Gift card and Amount applied towards the Order is displayed in the 'Billing' Page",
					"By clicking on 'ApplyGiftCard',  Last four Digit of the Gift card and Amount applied towards the Order is not displayed in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_186

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system disables the Credit card and additional Payment section", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_187(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard100");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' button");

			Log.message(
					"<br><b>Expected Result:</b> When the order total can be paid entirely with the Gift Card amount, then the Credit Card  Payment section should be disabled in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					(checkoutPage.getMsgPaymentNotNeed().contains(
							"Your order has no balance, so no payment method is necessary to complete this order")),
					"When the order total can be paid entirely with the Gift Card amount, then the Credit Card  Payment section is disabled in the 'Billing' Page",
					"When the order total can be paid entirely with the Gift Card amount, then the Credit Card  Payment section is not disabled in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_187

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Saved Card dropdown in Checkout-Billing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_239(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("6. Product added to Shopping Bag!");

			pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8. Naviagted to checkout page");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("9. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("11. Filled Billing Address in Billing Page");

			String cardDetails = checkoutPage.selectSavedPaymentMethodByIndex(0).split("\\)")[0].replace("(", "");
			Log.message("12. Entered Card Details");

			Log.message(
					"<br><b>Expected Result:</b> The saved card in Payment Method  should be able to select from the drop down list, which should fill the corresponding form fields with the User’s saved credit card details in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(cardDetails.trim().equalsIgnoreCase(checkoutPage.getPaymentDetailsInBillingPage().trim()),
					"The saved card in Payment Method  should be able to select from the drop down list, which is filled the corresponding form fields with the User’s saved credit card details in the 'Billing' Page",
					"The saved card in Payment Method  should be able to select from the drop down list, which is not filled the corresponding form fields with the User’s saved credit card details in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally
	}// TC_BELK_CART_239

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays a error message for the Gift Card Services.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_199(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("GiftCardBlocked");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' button");

			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					(checkoutPage.getGiftCardErrMsg().trim().contains("Something Went Wrong, please try again later")),
					"By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page",
					"By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_199

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify order summary pane below selected product details Order payment amount is properly calculated.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_204(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		double shipValue;

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			String price = shoppingBagPage.getTextNowPrice().split("\\$")[1];
			double productPrice = Double.parseDouble(price);
			String qty = shoppingBagPage.getQuantityValue(0);
			double quantity = Double.parseDouble(qty);
			double merchandiseTotal = (productPrice) * (quantity);

			shoppingBagPage.applyCouponInShoppingBag("963258741");
			Log.message("5. Applied Coupon to shopping Bag Page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("6. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("7.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("8. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("9. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("10. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("11. Filled Card Details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Clicked on continue button in the billing page");

			OrderConfirmationPage orderconfirmationpage = checkoutPage.placeOrder();
			Log.message("13 Clicked On PlaceOrder");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result1:</b> Merchandise Total should be calculated Properly in the OrderConformtion Page");
			Log.softAssertThat(orderconfirmationpage.getMerchandiseTotal() == merchandiseTotal,
					"<b>Actual Result1:</b> Merchandise Total is calculated Properly in the OrderConformtion Page",
					"<b>Actual Result1:</b> Merchandise Total is not calculated Properly in the OrderConformtion Page",
					driver);

			String shipment = orderconfirmationpage.getTextFromShipping();
			shipment = shipment.replaceAll("\\$", "");
			if (shipment.contains("FREE")) {
				shipValue = 0.0;
			} else {
				shipValue = Double.parseDouble(shipment);
			}
			Log.message("14. Got the text from shipping Value and the value is:" + shipValue);

			String salesTax = orderconfirmationpage.getTextFromSalesTax();
			salesTax = salesTax.replaceAll("\\$", "");
			double salesTaxValue = Double.parseDouble(salesTax);
			Log.message("15. Got the text from sales tax and the value is:" + salesTaxValue);

			String shipSurCharge = orderconfirmationpage.getTextFromShipSurCharge();
			shipSurCharge = shipSurCharge.replaceAll("\\$", "");
			double shipSurChargeValue = Double.parseDouble(shipSurCharge);
			Log.message("16. Got the text from shipSurCharge and the value is:" + shipSurChargeValue);

			String coupanSavings = orderconfirmationpage.getTextFromCoupanSavings();
			coupanSavings = coupanSavings.replaceAll("\\-", "").replaceAll("\\$", "");
			double coupanSavingsValue = Double.parseDouble(coupanSavings);
			Log.message("17. Got the text from coupanSavings and the value is:" + coupanSavingsValue);

			String orderTotal = orderconfirmationpage.getTextFromOrderTotal().replaceAll("\\$", "");
			double orderTotalValue = Double.parseDouble(orderTotal);
			Log.message("18. Got the text from orderTotal and the value is:" + orderTotalValue);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result2:</b> Order Total should be calculated Properly in the OrderConformtion Page");
			Log.assertThat(
					orderTotalValue == orderconfirmationpage.getMerchandiseTotal() + salesTaxValue + shipValue
							+ shipSurChargeValue - coupanSavingsValue,
					"<b>Actual Result2:</b> Order Total is calculated Properly in the OrderConformtion Page",
					"<b>Actual Result2:</b> Order Total is not calculated Properly in the OrderConformtion Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_204

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify pages navigates to My Bag screen when clicking 'Edit' link in Shipping Address from Place Order screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_226(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("10. Filled Card Details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Clicked on continue button in the billing page");

			checkoutPage.clickEditShippingAddress();
			Log.message("12. Clicked on 'Edit' link in the 'placeOrder' Page and Navigated to 'Shopping Bag' Page");

			checkoutPage.clickOnContinueInShipping();
			Log.message("15. Navigate to Billing Page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The filled cvv card details should be removed in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(checkoutPage.verifyCvvfieldIsEmpty(),
					"The filled cvv card details is removed in the 'Billing' Page",
					"The filled cvv card details is not removed in the 'Billing' Page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_226

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the behavior of 'Find in Store' for products with low inventory.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_105(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Navigating to PDP page
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");
			// selecting color from color swatch in Pdp Page
			Log.message("4. Selected color:'" + pdppage.selectColor() + "' from Color swatch in the Pdp!");
			// selecting size from Size Drop down in Pdp Page
			Log.message("5. Selected size:'" + pdppage.selectSize() + "' from Size dropdown in the Pdp!");
			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("6. Clicked on 'Add to bag' button");
			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("7. Clicked on 'Mini cart icon' and Navigated to the ShoppingBag Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> User should not see the Find In Store link.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnFindStore"),
							shoppingBagPage),
					"<b>Actual Result :</b> 'Find In Store' link is not displayed for the products with Low Inventory",
					"<b>Actual Result :</b> 'Find In Store' link is displayed for the products with Low Inventory");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4b_105

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows the User to Edit Shipping Address in Place Order page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_215(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String ValidCoupon = testData.get("CouponCode");
		String cardDetails = "card_BelkRewardsCreditCard1";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			// Navigating to PDP page
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("6. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("7. Navigated to Shopping bag!");

			shoppingBagPage.applyCouponInShoppingBag(ValidCoupon);
			Log.message("8. Coupon is Applied to the Product");
			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout page!");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on CheckOutAsGuest Button");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Shipping Address details are entered in the Shipping Address Page!");

			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue Button in the Shipping page and Navigated to Billing Page !");

			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("14. Billing address details are entered in the Billing Page!");
			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");
			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar4");
			Log.message("16. Applied Belk Reward Dollar in the Billing Page");
			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message("17.Clicked on Apply Belk reward dollars");
			checkoutPage.clickOnContinueInBilling();
			Log.message("18. Clicked on Continue Button in the Billing Page and Navigated to Place Order Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> The page should redirected to place holder section.");
			String className = checkoutPage.getClassNameOfStepsInCheckoutPage("placeorder").split(" ")[1];
			Log.assertThat(className.equals("active"),
					"<b>Actual Result 1:</b>The page is redirected to place holder section",
					"<b>Actual Result 1:</b>The page is not redirected to place hoder section", driver);

			checkoutPage.clickOnEditBySectionName("shippingaddress");
			Log.message("19. Clicked on Edit Link in Place Order Page and Navigated to Shipping page!");
			String className2 = checkoutPage.getClassNameOfStepsInCheckoutPage("shipping").split(" ")[1];
			LinkedHashMap<String, String> OrderDetails = checkoutPage.getCostDetailsInOrderSummary();
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Order Summary should be Displayed with the Following details: Merchandise Total, Total Coupon Savings(if applicable), Shipping Surcharge(if applicable), Estimated Shipping(if applicable), Estimated Sales Tax(if applicable), Estimated Order Total");
			Log.assertThat(
					className2.equals("active")
							&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtMerchandiseTotalCost"),
									checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtTotalCouponSavings"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtShippingCost"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtEstimatedSalesTaxCost"),
							checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtTotalOrderValure"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSurchargeValue"), checkoutPage)
					&& checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtBelkRewardDollarDiscountPrice"),
							checkoutPage),
					"<b>Actual Result 2:</b> Order Summary is Displayed with the Following details: Merchandise Total: "
							+ OrderDetails.get("MerchandiseTotal") + ", Total Coupon Savings(if applicable): "
							+ OrderDetails.get("CouponSavings") + ", Belk Reward Dollars(if applicable)"
							+ checkoutPage.getRewardDollarPrice() + ", Shipping Surcharge(if applicable): "
							+ OrderDetails.get("Surcharge") + ", Estimated Shipping(if applicable)"
							+ OrderDetails.get("EstimatedShipping") + ", Estimated Sales Tax(if applicable): "
							+ OrderDetails.get("SalesTax") + ", Estimated Order Total: "
							+ OrderDetails.get("EstimatedOrderTotal") + "",
					"<b>Actual Result 2:</b> Order Summary is not Displayed with the Following details: Merchandise Total: "
							+ OrderDetails.get("MerchandiseTotal") + ", Total Coupon Savings(if applicable): "
							+ OrderDetails.get("CouponSavings") + ", Belk Reward Dollars(if applicable)"
							+ checkoutPage.getRewardDollarPrice() + ", Shipping Surcharge(if applicable): "
							+ OrderDetails.get("Surcharge") + ", Estimated Shipping(if applicable)"
							+ OrderDetails.get("EstimatedShipping") + ", Estimated Sales Tax(if applicable): "
							+ OrderDetails.get("SalesTax") + ", Estimated Order Total: "
							+ OrderDetails.get("EstimatedOrderTotal") + "",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4b_215

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify user can select a store to pickup product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_100(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey + "' and  Navigated to PDP Page  !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++) + ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			shoppingBagPage.clickOnFindStorelnk();
			Log.message((i++) + ". clicked on FindStore link !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> User should see the 'Find in Store' Zipcode search modal when clicked on 'Find in Store' link");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("findInStorePopUp"), shoppingBagPage),
					"<b>Actual Result : </b> After clicking findstore link the Zipcode search modal displayed.",
					"<b>Actual Result : </b> After clicking findstore link the Zipcode search modal not displayed.",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_100

	// /////////

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify the Order Summary details in Cart page(Shopping Bag with Products) for a Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_223(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String password = testData.get("Password");
		List<String> myaccountelement = Arrays.asList("divMyAccount");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("5. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagted to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. 'CheckoutAsGuest' button is clicked");

			// CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutasReg();
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("9. Filled the shipping details");
			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("12. Filled the billing details");
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' in billing address page");
			OrderConfirmationPage orderconfirmationpage = checkOutPage.placeOrder();
			Log.message("15. Place order button is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> The Create Account panel should be displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address'  ,'Password' , 'Confirm Password', 'Create Account button' in the Order Summary page.");
			Log.assertThat(
					orderconfirmationpage.elementLayer
							.verifyPageElements(
									Arrays.asList("txtFirstname", "txtLastname", "txtEmail", "txtConformEmail",
											"txtPassword", "txtConformPassword", "btnCreateAccount"),
									orderconfirmationpage),
					"<b>Actual Resul1t:</b> The Create Account panel is displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address' , 'Create Account button' in the Order Summary page",
					"<b>Actual Result:</b> The Create Account panel is not displayed the fields 'First Name', 'Last Name', 'Email Address' ,' Confirm Email Address' ,'Password' , 'Confirm Password', 'Create Account button' in the Order Summary page",
					driver);
			orderconfirmationpage.enterPasswordInCreateAccount(password);
			Log.message("16. Entered password in create account");
			orderconfirmationpage.clickOnSubmitInCreateAccount();
			Log.message("17. Submit button is clicked in create account");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> Warning message is displayed in Create Account panel for invalid User account");

			Log.assertThat(
					orderconfirmationpage.elementLayer.verifyPageElements(Arrays.asList("txtErrorInEmail"),
							orderconfirmationpage),
					"<b>Actual Result:</b> Warning message is displayed in Create Account panel for invalid User account",
					"<b>Actual Result:</b> Warning message is not displayed in Create Account panel for invalid User account");
			Log.message("<br>");
			orderconfirmationpage.enterRandomEmailIdInCreateAccount();
			Log.message("18. Entered random email id in create account");
			orderconfirmationpage.enterPasswordInCreateAccount(password);
			Log.message("19. Entered password in create account");
			orderconfirmationpage.clickOnSubmitInCreateAccount();
			Log.message("20. Submit button is clicked after enter valid details in create account section");
			MyAccountPage myaccountPage = new MyAccountPage(driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> User should be allowed to login Belk site for valid User account and should be navigated to the My Account Landing page.");
			Log.assertThat(myaccountPage.elementLayer.verifyPageElements(myaccountelement, myaccountPage),
					"<b>Actual Result:</b> User is allowed to login Belk site for valid User account and should be navigated to the My Account Landing page.",
					"<b>Actual Result:</b> User is not allowed to login Belk site for valid User account and should be navigated to the My Account Landing page.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_223

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system enables the Credit card and additional Payment section when Gift Card applied is removed.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B__204(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest("valid_florida_address", "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest("valid_florida_address");
			Log.message("9. Billing details filled");
			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard100");
			Log.message("10. Gift card details filled");

			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Gift card is applied");
			checkoutPage.clickonRemoveGiftcard();
			Log.message("13. Remove icon is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> Credit Card and Payment section should be enabled, if an already used Gift Card aaplied is removed");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentMethod"), checkoutPage),
					"<b>Actual Result: </b> Credit Card and Payment section should be enabled, if an already used Gift Card aaplied is removed",
					"<b>Actual Result: </b> Credit Card and Payment section should be enabled, if an already used Gift Card aplied is removed");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B__204

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify page redirected from My Bag screen when clicking Edit link in Order Summary panel in Place Order page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_229(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String txtEditBySection = "ordersummary";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to search result Page");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("5. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("6. Naviagted to Shopping bag");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Naviagte to checkout page");

			CheckoutPage checkOutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("8. 'CheckoutAsGuest' button is clicked");

			// CheckoutPage checkOutPage = shoppingBagPage.clickCheckoutasReg();
			checkOutPage.fillingShippingDetailsAsGuest("valid_address1", "Standard");
			Log.message("9. Filled the shipping details");
			checkOutPage.clickOnContinueInShipping();
			Log.message("10. Clicked on the 'Continue' in shipping address page");
			checkOutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. 'Continue' button is clicked in suggestion popup modal");
			checkOutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("12. Filled the billing details");
			checkOutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("13. Filled the card details");
			checkOutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on 'Continue' in billing address page");
			checkOutPage.clickOnEditBySectionName(txtEditBySection);
			Log.message("15. Edit link is clicked in order summary section");
			Log.message(
					"<b>Expected Result: </b> when clicked on Edit link in Order Summary panel, use should be redirected to Shopping Bag page.");
			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("divCart"), shoppingBagPage),
					"<b>Actual Result: </b> user is redirected to Shopping Bag page",
					"<b>Actual Result: </b> user is not redirected to Shopping Bag page");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_229

	@Test(groups = { "desktop",
			"mobile" }, description = "Customer should not be allowed for Free Shipping when customer choose the Belk Rewards Credit card as payment option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_187(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				Log.message("4. Products removed from Cart");

			}
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);

			Log.message("8. Product is added to the cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Minicart icon is clicked and navigated to Shopping bag page");

			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("11. Clicked 'Checkout' button and naviagte to checkout page");
			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address2");
			checkoutpage.clickOnContinueInShipping();
			Log.message("12. 'Continue button' in shipping address page is clicked");
			checkoutpage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Continue button is clicked in popup modal and navigated to billing page");
			checkoutpage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("14. Card details filled");
			checkoutpage.clickOnContinueInBilling();
			Log.message("15. Continue In Billing is clicked");
			Log.message(
					"<b>Expected Result: </b> Customer should not be allowed for Free Shipping when customer choose the Belk Rewards Credit card as payment option");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("msgShipmentResource"),
							checkoutpage),
					"<b>Actual Result: </b> Customer is not allowed for Free Shipping when customer choose the Belk Rewards Credit card as payment option",
					"<b>Actual Result: </b> Customer is allowed for Free Shipping when customer choose the Belk Rewards Credit card as payment option");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_187

	@Test(groups = { "desktop",
			"mobile" }, description = "verify system displays the Coupon message in the line item.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_124(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String couponCode = testData.get("CouponCode");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				Log.message("4. Products removed from Cart");

			}
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("8. Product is added to the cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Minicart icon is clicked and navigated to Shopping bag page");
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("11. Coupon code is applied");
			Log.message(
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item, if the User applied any coupon to the product.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtAppliedCoupon"), shoppingBagPage),
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item",
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_124

	@Test(groups = { "desktop",
			"mobile" }, description = "verify system displays the Coupon message in the line item.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_057(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String couponCode = testData.get("CouponCode");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
				Log.message("4. Products removed from Cart");

			}
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("8. Product is added to the cart");

			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Minicart icon is clicked and navigated to Shopping bag page");
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message("11. Coupon code is applied");
			Log.message(
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item, if the User applied any coupon to the product.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtAppliedCoupon"), shoppingBagPage),
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item",
					"<b>Actual Result: </b>  Coupon message should be displayed on the line item");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_057

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Apply Coupon button in Coupons section of cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_054(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey + "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++) + ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Clicked on minicart and navigated to ShoppingBag page !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> Apply Coupon button should be displayed below the Coupon Code (Optional) field and next to the View All Coupons link.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnApplyCouponNxtToViewAll"),
							shoppingBagPage),
					"<b>Actual Result : </b> Apply Coupon button displayed below the Coupon Code (Optional) field and next to the View All Coupons link.",
					"<b>Actual Result : </b> Apply Coupon button not displayed below the Coupon Code (Optional) field and next to the View All Coupons link.",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_054

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Remove link for Applied Coupons in cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_063(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String couponCode = "11223344";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey + "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++) + ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Clicked on minicart and navigated to ShoppingBag page !");
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message((i++) + ". Entered the coupon code as :" + couponCode);
			shoppingBagPage.clickOnRemoveCouponLnk();
			Log.message((i++) + ". Clicked on 'Remove' link and Applied coupon removed !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> Apply Coupon button should be displayed below the Coupon Code (Optional) field and next to the View All Coupons link.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnRemoveCoupon"),
							shoppingBagPage),
					"<b>Actual Result : </b> Apply Coupon button displayed below the Coupon Code (Optional) field and next to the View All Coupons link.",
					"<b>Actual Result : </b> Apply Coupon button not displayed below the Coupon Code (Optional) field and next to the View All Coupons link.",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_063

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays a success message and adds product to 'Registry' after user logs in using valid credentials - Multiple Registries.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_130(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'SignIn' page from the header section
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!", driver.getCurrentUrl());

			// Entering the credentials in the signIn page
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page",
					driver);

			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");

			// Searching a product with product keyword like 'jeans'
			PdpPage pdpPage = signinPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			// Getting the productname from the breadcrumb
			String productName = pdpPage.getProductNameFromBreadCrumb();
			Log.message("6. Selected ProductName:'" + productName + "' from the 'PDP' page");

			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("7. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("8. selected color: " + pdtDetails.split("\\|")[0]);
			// select the quantity
			Log.message("9. selected qty1: " + pdtDetails.split("\\|")[2]);
			// Add the product to the bag
			Log.message("10. Product is added to the cart");

			// Navigated to the shoppingBagPage by clicking on minicart icon
			ShoppingBagPage shopingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			// Clicking on Registry Link in shopping bag page
			RegistrySignedUserPage registrySignedUserPage = shopingBagPage.clickOnRegistry(productName);
			Log.message("12. By Clicking on 'Registry', It Navigated to'Registry' Page");

			registrySignedUserPage.clickOnApplyInEditRegistry();
			Log.message("13. Clicked on 'Apply' button in the Registry page ");

			Log.message(registrySignedUserPage.gettxtitemAddedRegistry());
			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> After Clicking on 'Apply' ,the message 'Item Added to Registry' should be displayed");
			Log.softAssertThat(registrySignedUserPage.gettxtitemAddedRegistry().contains("Item Added to Registry"),
					"<b>Actual Result:</b> After Clicking on 'Apply' ,the message 'Item Added to Registry' is displayed ",
					"<b>Actual Result:</b> After Clicking on 'Apply' ,the message 'Item Added to Registry' is not displayed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_130

	@Test(groups = { "desktop",
			"tablet" }, description = "Verify system navigates the User to 'Multi-Ship step 2 Shipping method' page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_279(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "'");
			Log.message("6. Navigated to PDP");

			// Selecting the random colour from the colour swatch
			String selectedColor = pdpPage.selectColor();
			Log.message("7. Selected Color:'" + selectedColor + "' from the color swatches in the PDP Page");

			// Selecting the random size from the select size dropdown
			String selectedSize = pdpPage.selectSize();
			Log.message("8. Selected size:'" + selectedSize + "' from the size drop down  in the PDP Page");

			// Selecting the random quantity from the quantity dropdown
			pdpPage.selectQtyByIndex(2);
			Log.message("9. Selected Quantity: '2' from the quantity drop down  in the PDP Page");

			// Clicking on 'AddToBag' link
			pdpPage.clickAddToBag();
			Log.message("10. Product added to Shopping Bag!'");

			// Navigating to shopping bag page from pdp page after clicking on
			// minicart icon
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("11. Clicking on Mini cart icon, It navigated to the ShoppingBag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("13. Clicking on 'Yes' button for multi shipping in the 'ShippingDetails' Page");

			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();

			shipToMultipleAddressPage.clickaddLink();
			Log.message("14. Clicked on 'Add' Link to add the New Address In the 'ShipToMultipleAddress' Page");

			shipToMultipleAddressPage.fillingShippingDetailsAsSignedInUser("NO", "valid_address1");
			Log.message("15. Entered Address Details");

			shipToMultipleAddressPage.ClickSaveBtn();
			shipToMultipleAddressPage.ContinueAddressValidationModalWithDefaults();
			Log.message("16. Added Address is Saved");

			// Select only first address from the dropdown
			shipToMultipleAddressPage.selectFirstAddressInAddressDropDown();
			Log.message("17. Selected the saved address from the dropdown");

			// After filling address,clicking on the continue button in the
			// shipping Page.
			checkoutPage.clickOnContinueInMultiAddress();

			Log.message("<br>");
			Log.message(
					"<b>Expected Result:</b> By Clicking on 'Continue' in the MultipleShippingAddress Page, It should Navigated to Shipping method page");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("drpDownShipppingMethods"),
							checkoutPage),
					"<b>Actual Result:</b> By Clicking on 'Continue' in the MultipleShippingAddress Page, It is Navigated to Shipping method page",
					"<b>Actual Result:</b> By Clicking on 'Continue' in the MultipleShippingAddress Page, It is not Navigated to Shipping method page");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4B_279

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays the error message for the entered Gift card.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_198(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("GiftCardBlocked");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' button");

			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					(checkoutPage.getGiftCardErrMsg().trim().contains("Something Went Wrong, please try again later")),
					"By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page",
					"By clicking on 'ApplyGiftCard' with wrong details, then error should be displayed in the 'Billing' Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_198

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows User to check Belk Gift Card balance from billing Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_194(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Navigate to Login Page!");

			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("6. Navigate to Checkout Page as Guest!");

			checkoutPage.fillingShippingDetailsAsGuest("valid_florida_address", "Standard");
			Log.message("7. Shipping Details entered successfully!");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("8. Continued to Billing Tab in checkout page!");

			checkoutPage.fillingBillingDetailsAsGuest("valid_florida_address");
			Log.message("9. Billing details filled");
			checkoutPage.fillingBelkGiftCardDetails("GiftCard1");
			Log.message("10. Gift card details filled");
			checkoutPage.clickOnCheckGiftCardBalance();
			Log.message("11. Check Gift card balance button ic clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> Balance details should be updated in Belk Gift Card Pane");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtbalanceGiftCard"), checkoutPage),
					"<b>Actual Result: </b> Balance details is updated in Belk Gift Card Pane",
					"<b>Actual Result: </b> Balance details is not updated in Belk Gift Card Pane");
			Log.message("<br>");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("12. Gift card is applied");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> User should be allowed to order only for the balance amount available.");
			Log.assertThat(
					checkoutPage.elementLayer
							.verifyPageElements(Arrays.asList("txtNoBalanceAvailableforAppliedGiftCard"), checkoutPage),
					"<b>Actual Result: </b> User is allowed to order only for the balance amount available.",
					"<b>Actual Result: </b> User is not allowed to order only for the balance amount available.");
			Log.message("<br>");
			checkoutPage.clickonRemoveGiftcard();
			Log.message("13. Remove icon is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> Applied Gift card is removed when clicking remove button from Belk Gift Card Pane");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("fldAppliedGiftCard"),
							checkoutPage),
					"<b>Actual Result: </b> Applied Gift card is removed when clicking remove button from Belk Gift Card Pane",
					"<b>Actual Result: </b> Applied Gift card is not removed when clicking remove button from Belk Gift Card Pane");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_194

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify by confirm order with payment method 'Amex' and by adding different multiple address example Georgia, Florida", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CHECKOUT_233(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to Pdp Page using the search key " + searchKey);
			pdppage.selectQtyByIndex(2);
			Log.message("3.  Size is Selected from size dropdown !");
			pdppage.selectQuantity();
			Log.message("4. Quantity drowdown is selected with Quantity '2' !");
			pdppage.clickAddToBag();
			Log.message("5. product added to MyBag !");
			ShoppingBagPage shoppingBagPage = pdppage.clickOnMiniCart();
			Log.message("6. Minicart icon is clicked");
			SignIn signIn = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("7. Navigated to Shopping bag page!");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("8. clicked on checkoutAsGuest Button !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> Do you want to ship to multiple addresses text should be displayed with Yes button above the Select Shipping Address section.");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("fldShiptoMultiple"), checkoutPage),
					"<b>Actual Result: </b> Do you want to ship to multiple addresses text is displayed with Yes button above the Select Shipping Address section.",
					"<b>Actual Result: </b> Do you want to ship to multiple addresses text is not displayed with Yes button above the Select Shipping Address section.");
			Log.message("<br>");
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("9. clicked on yes Button for MultiShipping !");
			ShipToMultipleAddressPage shipToMultipleAddressPage = new ShipToMultipleAddressPage(driver).get();
			Log.message(
					"10. Navigated from Checkout Page to ShipToMultipleAddress Page by Clicking Yes button for ship to multiple addresses");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result: </b> Ship to multiple address page is opened User can able to ship the product to multiple addresses.");
			Log.assertThat(
					shipToMultipleAddressPage.elementLayer.verifyPageElements(Arrays.asList("btnAddAddress"),
							shipToMultipleAddressPage),
					"<b>Actual Result: </b> Ship to multiple address page is opened User can able to ship the product to multiple addresses.",
					"<b>Actual Result: </b> Ship to multiple address page is not opened. So,User cannot able to ship the product to multiple addresses.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CHECKOUT_233

	@Test(groups = { "desktop",
			"mobile" }, description = "Verify Billing details in Checkout page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_236(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPlatform = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid, password);
			Log.message(
					"3. Entered the credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myaccount.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myaccount.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			Log.message("4. Products removed from Cart");
			PdpPage pdpPage = myaccount.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Searched with '" + searchKey + "' and navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the colour
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("8. Product is added to the cart");
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("10. Minicart icon is clicked and navigated to Shopping bag page");
			CheckoutPage checkoutpage = (CheckoutPage) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("11. Clicked 'Checkout' button and naviagte to checkout page");
			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address2");
			checkoutpage.clickOnContinueInShipping();
			Log.message("12. 'Continue button' in shipping address page is clicked");
			checkoutpage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message("13. Continue button is clicked in popup modal and navigated to billing page");
			if (runPlatform == "desktop") {
				Log.message(
						"<b>Expected Result: </b> The following section should be displayed in the Billing page:1. Return to Shipping link below the Belk logo <br> 2. Select or Add Billing Address3. Select or Add Payment Method4. Belk Gift Card 5. Belk Reward Dollars6. Order Summary with Edit link7. Shipping Address with Edit link8.Continue button9. Shop With Confidence options above the footer");
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lnkReturnToShoppingBag"),
								checkoutpage),
						"<b>Actual Result: </b> Return to Shipping link below the Belk logo is displayed properly",
						"<b>Actual Result: </b> Return to Shipping link below the Belk logo is not displayed properly");
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("fldAddBillingMethod"),
								checkoutpage),
						"<b>Actual Result: </b> Select or Add Billing Address is displayed properly",
						"<b>Actual Result: </b> Select or Add Billing Address is not displayed properly");
				Log.assertThat(
						checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblPaymentMethod"), checkoutpage),
						"<b>Actual Result: </b> Select or Add Payment Methodis displayed properly ",
						"<b>Actual Result: </b> Select or Add Payment Method is displayed properly");
			} else if (runPlatform == "mobile") {
				Log.message(
						"<b>Expected Result: </b> The following section should be displayed in the Billing page: 1. Belk Gift Card 2. Belk Reward Dollars 3. Order Summary with Edit link 4. Shipping Address with Edit link8.Continue button 5. Shop With Confidence options above the footer");
			}
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblBelkGiftCard"), checkoutpage),
					"<b>Actual Result: </b> Belk Gift Card is displayed properly",
					"<b>Actual Result: </b> Belk Gift Card is not displayed properly");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblBelkRewardDollar"), checkoutpage),
					"<b>Actual Result: </b> Belk Reward Dollars is displayed properly",
					"<b>Actual Result: </b> Belk Reward Dollars is not displayed properly");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lnkEditOrderSummary"), checkoutpage),
					"<b>Actual Result: </b> Order Summary with Edit link is displayed properly",
					"<b>Actual Result: </b> Order Summary with Edit link is not displayed properly");
			Log.assertThat(checkoutpage.elementLayer.verifyPageElements(Arrays.asList("shippingModule"), checkoutpage),
					"<b>Actual Result: </b> Shipping Address with Edit link is displayed properly",
					"<b>Actual Result: </b> Shipping Address with Edit link is not displayed properly");
			Log.assertThat(
					checkoutpage.elementLayer.verifyPageElements(Arrays.asList("lblShopWithConfidence"), checkoutpage),
					"<b>Actual Result: </b> Shop With Confidence options is displayed properly",
					"<b>Actual Result: </b> Shop With Confidence options is not displayed properly");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_236

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Find in Store link in PDP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_007(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");

			Log.message(
					"<b>Expected Result 1:</b> 'Find in Store' link should be displayed below the Qty field on PDP. ");
			Log.assertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("lnkFindInStore"), pdppage),
					"<b>Actual Result 1:</b> 'Find in Store' link is displayed below the Qty field on PDP.",
					"<b>Actual Result 1:</b> 'Find in Store' link is not displayed below the Qty field on PDP.",
					driver);
			pdppage.clickFindStore();
			Log.message("6. Clicked on 'Find In store' Link");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After Clicking 'Find In Store' Link,the zip code store modal should open.");
			Log.assertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("FindInstorePopUp"), pdppage),
					"<b>Actual Result 2:</b> After Clicking on 'Find In Store' Link,the zip code store modal is opened.",
					"<b>Actual Result 2:</b> After Clicking on 'Find In Store' Link,the zip code store modal is not opened.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_007

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Coupons applied details in Coupons section of cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_062(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		System.out.println(CouponCode);
		String NotApplicableText = "N/A";
		String RemoveLink = "Remove";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");
			PdpPage pdppage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");

			Log.message("4. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("6. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("7. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("8. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("9. Coupon is Applied to the Product");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Applying Coupons in the cart, 'Coupon Code' should be displayed in the Coupon Section.");
			Log.assertThat(
					shoppingbagpage.getCouponDetailsFromCouponSection()
							.contains(shoppingbagpage.getAppliedCouponCode()),
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Coupon Code' is displayed in the Coupon Section.",
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Coupon Code' is not displayed in the Coupon Section.",
					driver);

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> After Applying Coupons in the cart, 'Applied or Not Applicable' should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromCouponSection().contains(NotApplicableText),
					"<b>Actual Result 2:</b> After Applying Coupons in the cart, 'Applied or Not Applicable' is displayed in the Coupon Section.",
					"<b>Actual Result 2:</b> After Applying Coupons in the cart, 'Applied or Not Applicable' is not displayed in the Coupon Section.",
					driver);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> After Applying Coupons in the cart, 'Remove' Link  should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromCouponSection().contains(RemoveLink),
					"<b>Actual Result 3:</b> After Applying Coupons in the cart, 'Remove' Link is displayed in the Coupon Section.",
					"<b>Actual Result 3:</b> After Applying Coupons in the cart, 'Remove' Link is not displayed in the Coupon Section.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4b_062

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system does not allow Free Gift box for Belk Rewards Credit Card as Payment option - Registered User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_191(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String address = testData.get("Address");
		String cardDetails = "card_BelkRewardsCreditCard";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Navigating to PDP page
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage1 = pdpPage.minicart.navigateToBag();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			shoppingBagPage1.checkAddGiftBox();
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", address);
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", address);
			Log.message("14. Filled the Billing details in Billing form!");

			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");
			LinkedHashMap<String, String> CostDetails = checkoutPage.getCostDetailsInOrderSummary();

			System.out.println((CostDetails.get("Giftbox")));

			float GiftBoxPrice = Float.parseFloat(CostDetails.get("Giftbox"));
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Customer should not be allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.");
			Log.assertThat(!(GiftBoxPrice == 0.00),
					"<b>Actual Result :</b> Customer is not allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.",
					"<b>Actual Result :</b> Customer is allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CART_191//

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system does not allow Free Gift box for Belk Rewards Credit Card as Payment option.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_193(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");
			PdpPage pdpPage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");
			pdpPage.selectSize();
			pdpPage.selectColor();
			String pdtDetails = pdpPage.addProductToBag();
			Log.message("4. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("5. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("6. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("7. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			shoppingBagPage.checkAddGiftBox();
			Log.message("8. Selected 'Add a Gift Box'");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout page!");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on CheckOutAsGuest Button");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address2", "Standard");
			Log.message("11. Shipping Address details are entered in the Shipping Address Page!");

			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue Button in the Shipping page and Navigated to Billing Page !");

			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("14. Billing address details are entered in the Billing Page!");

			checkoutPage.fillingBelkRewardCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("15. Filled the Belk Reward Card details in Billing Page!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			LinkedHashMap<String, String> CostDetails = checkoutPage.getCostDetailsInOrderSummary();
			float GiftBoxPrice = Float.parseFloat(CostDetails.get("Giftbox"));
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Customer should not be allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.");
			Log.assertThat(!(GiftBoxPrice == 0.00),
					"<b>Actual Result :</b> Customer is not allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.",
					"<b>Actual Result :</b> Customer is allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {

			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_4b_193

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Coupon Resource Message in Coupons section of cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4b_055(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		String CouponResourceMsg = "Some coupons require using your Belk Rewards Card as a form of payment.";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchresultpage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey + "' and navigated to Search Result Page !");
			PdpPage pdppage = searchresultpage.navigateToPDPByRow(1);
			Log.message("3. Navigated to PDP Page!");

			Log.message("4. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("5. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float ProductpriceofOneProduct = Float.parseFloat(pdppage.getProductPrice());
			Log.message("6. Price Of a Selected Product is " + ProductpriceofOneProduct);
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("7. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("8. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("9. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("10. Coupon is Applied to the Product");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The Coupon Resource Message should be displayed with a 'View Details' link below the Coupons header.");
			Log.assertThat(shoppingbagpage.getTextFromCouponResourceMsg().contains(CouponResourceMsg),
					"<b>Actual Result :</b> The Coupon Resource Message is displayed with a 'View Details' link below the Coupons header.",
					"<b>Actual Result :</b> The Coupon Resource Message is not displayed with a 'View Details' link below the Coupons header.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_055

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system does not 'Free Gift box' for 'Belk Rewards Credit Card' as Payment option - Guest User", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_189(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.checkAddGiftBox();
			Log.message("5. Checked  'Gift Box' in the 'ShoppingBag' Page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("6. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("7.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("8. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("9. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("10. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_NonEliteBelkRewardsCreditCard");
			Log.message("11. Filled Card Details in Billing details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("12. Clicked on continue button in Billing page");

			Log.message(
					"<br><b>Expected Result:</b> The Free Gift Box should not be allowed , when customer choose Non elite belk Reward Credit Card as Payment");
			Log.assertThat((checkoutPage.getGiftBoxValue().equals(0.00)),
					"<b>Actual Result:</b> The Free Gift Box is not allowed , when customer choose Non elite belk Reward Credit Card as Payment",
					"<b>Actual Result:</b> The Free shipping is allowed , when customer choose belk Non elite belk Reward Card as Payment",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_189

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Coupon Code Entry Field in Coupons section of cart.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_053(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String InvalidcouponCode = "96325874";
		String validcode = "11223344";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey + "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);
			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++) + ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Clicked on minicart and navigated to ShoppingBag page !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> Coupon Code (Optional) text field should be displayed below the Coupon Resource Message and the field should accept valid codes and all the codes entered will be converted to upper case letters and any spec entered will be trimmed off.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("txtCouponCode"), shoppingBagPage),
					"<b>Actual Result 1: </b> Coupon Code (Optional) text field displayed below the Coupon Resource Message.",
					"<b>Actual Result 1: </b> Coupon Code (Optional) text field not displayed below the Coupon Resource Message.",
					driver);
			Log.message("<br>");
			shoppingBagPage.enterCoupon("abcdef");
			String couponTxt = shoppingBagPage.getCouponTxt();
			Log.assertThat(couponTxt.contains("uppercase"),
					"<b>Actual Result 2: </b> All letters displayed as uppercase.",
					"<b>Actual Result 2: </b> All letters not displayed as uppercase.", driver);
			Log.message("<br>");
			shoppingBagPage.applyCouponInShoppingBag(InvalidcouponCode);
			Log.message((i++) + ". Entered the invalid coupon code as :" + InvalidcouponCode);
			String ERROR_MSG = shoppingBagPage.getCouponErrMsg();
			String Expected_ERROR_MSG = "That coupon code is invalid.";
			Log.assertThat(ERROR_MSG.equals(Expected_ERROR_MSG),
					"<b>Actual Result 3: </b> After entering the invalid coupon code it displayed error message.",
					"<b>Actual Result 3: </b> After entering the invalid coupon code it has not displayed any error message.",
					driver);
			Log.message("<br>");
			shoppingBagPage.applyCouponInShoppingBag(validcode);
			Log.message((i++) + ". Entered the coupon code as :" + validcode);
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnRemoveCoupon"), shoppingBagPage),
					"<b>Actual Result 4: </b> Coupon applied successfully.",
					"<b>Actual Result 4: </b> Coupon not applied successfully.", driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_053

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Store addresses details in the store search results window.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_011(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String ZipCode = "28211";
		List<String> StoreDetails = Arrays.asList("storeName", "storeAddress", "storeNumber", "storeDistance",
				"storeWorkingHour", "storeProductInStock");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey + "' and Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++) + ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			shoppingBagPage.clickOnFindStorelnk();
			Log.message((i++) + ". clicked on FindStore link !");
			shoppingBagPage.applyZipCodeInShoppingBag(ZipCode);
			Log.message((i++) + ". Entered ZipCode and clicked continue button !");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result : </b> The window should show up to six store addresses with the following information:Store Name​Address, Phone Number​ ,Distance​ HoursAvailability​Limited ,Quantity Message​.");

			Log.assertThat(shoppingBagPage.elementLayer.verifyPageElements(StoreDetails, shoppingBagPage),
					"<b>Actual Result 2: </b> The store addresses displayed with the following information:Store Name​Address, Phone Number​ ,Distance​ HoursAvailability​Limited ,Quantity Message​.",
					"<b>Actual Result 2: </b> The store addresses not displayed with the following information:Store Name​Address, Phone Number​ ,Distance​ HoursAvailability​Limited ,Quantity Message​.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_CART_011

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system enables the Credit card and additional Payment section when Gift Card previously used is applied.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_205(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6. 'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("10. Entered GiftCard Details");
			checkoutPage.clickOnApplyGiftCard();
			Log.message("11. Clicked on 'ApplyGiftCard' in the Billing Page");

			Log.message(
					"<br><b>Expected Result:</b> By clicking on 'ApplyGiftCard', Credit Card Payment section should be enabled, if the Gift Card value is less than the Order Total");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentMethod"), checkoutPage),
					"By clicking on 'ApplyGiftCard', Credit Card Payment section is enabled, as the Gift Card value is less than the Order Total",
					"By clicking on 'ApplyGiftCard', Credit Card Payment section is not enabled, as the Gift Card value is more than the Order Total",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_205

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system displays shipping address  Place Order- Customer choose 'Ship to Home' in Shipping Address panel.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_225(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6. 'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			LinkedHashMap<String, String> shippingDetails = checkoutPage.getShippingAddressInShippingPage();
			shippingDetails.remove("Address2");
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("10. Filled Card Details in Billing Page");

			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Clicked on 'Continue' button in the Billing Page");
			LinkedHashMap<String, String> shippingdetails = checkoutPage.getShippingAddress();

			Log.message(
					"<br><b>Expected Result:</b> The Entered Shipping details should be displayed in the Place Order screen");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(Utils.compareTwoHashMap(shippingDetails, shippingdetails),
					"The Entered Shipping details is displayed in the Place Order screen",
					"The Entered Shipping details is not displayed in the Place Order screen", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_225

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows 'Free Shipping' for 'Belk Rewards Credit Card' as Payment option for Elite Customers - Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_186(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("10. Filled Card Details in Billing details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Clicked on continue button in Billing page");

			Log.message(
					"<br><b>Expected Result:</b> The Free shipping should be allowed , when customer choose belk Rewaed Credit Card as Payment");
			Log.assertThat(checkoutPage.shippingCostInPlaceOrder().replace("$", "").contains("0.00"),
					"The Free shipping is allowed , when customer choose belk Rewaed Credit Card as Payment",
					"The Free shipping is not allowed , when customer choose belk Rewaed Credit Card as Payment",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_186

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system does not include 'Free Shipping' for 'Belk Rewards Credit Card' as Payment option for non-Elite Customers - Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_185(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingCardDetails("NO", "card_NonEliteBelkRewardsCreditCard");
			Log.message("10. Filled Card Details in Billing details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("11. Clicked on continue button in Billing page");

			Log.message(
					"<br><b>Expected Result:</b> The Free shipping should not be allowed , when customer choose Non elite belk Reward Credit Card as Payment");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("shippingDiscountInOrdersummary"), checkoutPage),
					"The Free shipping is not allowed , when customer choose Non elite belk Reward Credit Card as Payment",
					"The Free shipping is allowed , when customer choose Non elite belk Reward Credit Card as Payment",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_185

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system display the Product Thumbnail in the Item detail.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_107(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);
			String size = pdpPage.selectSize();
			Log.message("3. Selected size is" + size);
			String color = pdpPage.selectColor();
			Log.message("4. Selected color is " + color);
			String quantity = pdpPage.selectQuantity();
			Log.message("5. Selected quantity is" + quantity);
			pdpPage.addProductToBag();
			Log.message("6. Product added to Shopping Bag!");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("productImg"), shoppingBagPage),
					"Product Thumbnail is displayed in the Item detail",
					"Product Thumbnail is not displayed in the Item detail");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_107

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify system allows Free Gift box for Belk Rewards Credit Card as Payment option - Registered User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_192(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String cardDetails = "card_BelkRewardsCreditCard";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Launching homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}

			// Navigating to PDP page
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Searched with '" + searchKey + "'");
			Log.message("5. Navigated to PDP");

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("6. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("7. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("8. Product is added to the cart");

			ShoppingBagPage shoppingBagPage1 = pdpPage.minicart.navigateToBag();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			shoppingBagPage1.checkAddGiftBox();
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("10. Navigated to checkout page");
			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address1");
			Log.message("11. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address1");
			Log.message("14. Filled the Billing details in Billing form!");
			checkoutPage.fillingBelkRewardCardDetails("NO", cardDetails);
			Log.message("15. Filled the Belk Reward Card details in Billing form!");
			// Click continue in the Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked on 'Continue' Button in Billing Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("17. Clicked on 'Continue' Button in Validation Modal");
			LinkedHashMap<String, String> CostDetails = checkoutPage.getCostDetailsInOrderSummary();
			Log.message("18. Gist box amount is");
			Log.message(CostDetails.get("Giftbox"));
			float GiftBoxPrice = Float.parseFloat(CostDetails.get("Giftbox"));
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Customer should not be allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option.");
			Log.assertThat((GiftBoxPrice == 0.00),
					"<b>Actual Result :</b> Customer is allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option",
					"<b>Actual Result :</b> Customer is not allowed for Free Gift box when customer choose the Belk Rewards Credit card as payment option",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_192

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Find in Store link in PDP Page behavior for non Drop Ship product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_010(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String zipcode = testData.get("Address");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);
			String size = pdpPage.selectSize();
			Log.message("3. Selected size is" + size);
			String color = pdpPage.selectColor();
			Log.message("4. Selected color is " + color);
			String quantity = pdpPage.selectQuantity();
			Log.message("5. Selected quantity is" + quantity);
			pdpPage.clickOnFindStore();
			Log.message("6. Find in store link is clicked");
			pdpPage.EnterZipCode(zipcode);
			Log.message("7. Zip code is entered");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> Change your location link should be displayed next to the store results message");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkChangeYourLocation"), pdpPage),
					"<b>Actual Result :</b> Change your location is available",
					"<b>Actual Result :</b> Change your location is not available");
			Log.message("<br>");
			pdpPage.clickOnChangeYourLocation();
			Log.message("8. Change your location link is clicked");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> zip code store modal should open and the User should be able to perform a new search using a different zip code.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("InputtxtFieldEnterZipCode"), pdpPage),
					"<b>Actual Result :</b> Enter zip code dialog box is opened",
					"<b>Actual Result :</b> Enter zip code dialog box is not opened");
			pdpPage.EnterZipCode(zipcode);
			Log.message("9. Zip code is entered");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkStoreHeading"), pdpPage),
					"<b>Actual Result :</b> User is able to perform a new search using a different zip code.",
					"<b>Actual Result :</b> User is not able to perform a new search using a different zip code.");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_010

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Free Shipping Message in shopping bag with products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_072(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey[] = testData.get("SearchKey").split("\\|");
		String msgFreeShip = null;
		String msgFreeShipProper = "Congratulations! You qualify for Free Shipping";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[0]);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			String pdtDetails = pdpPage.addProductToBag();
			Log.message("3. selected size: " + pdtDetails.split("\\|")[1]);
			// Select the color
			Log.message("4. selected color: " + pdtDetails.split("\\|")[0]);
			Log.message("5. Product is added to the cart");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("6. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			if (Utils.getRunPlatForm() == "desktop") {
				msgFreeShip = shoppingBagPage.getTextOfFreeShippingMessage();
			} else if (Utils.getRunPlatForm() == "mobile") {
				msgFreeShip = shoppingBagPage.getTextOfFreeShippingMessageMob();
			}
			Log.message("7. Got the shipping message and the message is");
			Log.message(msgFreeShip);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The Free Shipping Message should be displayed below the Cart Messaging and above the product details in cart page.");
			Log.message("<b>Actual Result :</b> ");
			Log.assertThat(msgFreeShip.contains(msgFreeShipProper), "Free shipping message is displayed properly",
					"Free shipping message is not displayed properly");
			shoppingBagPage.removeItemsFromBag();
			Log.message("<br>");
			Log.message("8. Product is removed from bag");
			shoppingBagPage.searchAndNavigateToPDP(searchKey[1]);
			Log.message("9. Searched free shipping not available product and navigate to pdp page");
			Log.message("<br>");
			String pdtDetails1 = pdpPage.addProductToBag();
			Log.message("10. selected size: " + pdtDetails1.split("\\|")[1]);
			// Select the color
			Log.message("11. selected color: " + pdtDetails1.split("\\|")[0]);
			Log.message("12. Product is added to the cart");
			pdpPage.minicart.navigateToBag();
			Log.message("13. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The product is not qualified for Free shipping, proximity message should be displayed based on total needed to qualify for Free Shipping current total");
			if (Utils.getRunPlatForm() == "desktop") {
				msgFreeShip = shoppingBagPage.getTextFromNeededAmountForFreeShip();
			} else if (Utils.getRunPlatForm() == "mobile") {
				msgFreeShip = shoppingBagPage.getTextOfFreeShippingMessage();
			}

			Log.assertThat(msgFreeShip.contains("Spend") && msgFreeShip.contains(" more for"),
					"<b>Actual Result :</b> The product is not qualified for Free shipping, proximity message should be displayed based on total needed to qualify for Free Shipping current total ",
					"<b>Actual Result :</b> The product is not qualified for Free shipping, proximity message is not displayed based on total needed to qualify for Free Shipping current total");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_072

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify 'Find in Store' link in PDP Page behavior for non 'Drop Ship' product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_010(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");

			SearchResultPage searchResultPage = homePage.searchProductKeyword(searchKey);
			Log.message("2. Navigate to searchResultPage Page with Product: '" + searchKey);

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message("3. Selected the product and navigated to Pdp page !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should see the 'Find in Store' link in PDP Page.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkFindInStore"), pdpPage),
					"<b>Actual Result:</b> User is able to see the 'Find in Store' link in PDP Page.",
					"<b>Actual Result:</b> User is not able to see the 'Find in Store' link in PDP Page.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_010

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify 'Find in Store' link in PDP Page behavior for 'Drop Ship' product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_011(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page !");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to Pdp page  with Product: '" + searchKey);

			pdpPage.selectColor();
			Log.message("3. Selected the color from color swatch !");
			pdpPage.selectSize();
			Log.message("4. Selected the size from size dropdown !");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> User should not see the 'Find in Store' link in PDP Page.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkFindInStore"), pdpPage),
					"<b>Actual Result:</b> User is not able to see the 'Find in Store' link in PDP Page.",
					"<b>Actual Result:</b> User is able to see the 'Find in Store' link in PDP Page.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_011

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify applying coupons to cart with no items.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_065(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		String NotApplicable = "N/A";
		String Applied = "Applied";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PDP with Product: '" + searchKey);
			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("8. Coupon is Applied to the Product");
			shoppingbagpage.removeAddedCartItems();
			Log.message("9. Removed the Items From the Cart");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Removing an Applicable Product in the cart, 'N/A' text should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromEmptyCart().contains(NotApplicable),
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Applied' text  is displayed in the Coupon Section.",
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Applied' text is not displayed in the Coupon Section.",
					driver);
			homePage.headers.searchAndNavigateToPDP(searchKey);
			pdppage.selectSize();
			pdppage.selectColor();
			pdppage.selectQuantity();
			pdppage.addProductToBag();
			Log.message("10. Again an Applicable Product is added to Shopping Bag!");
			pdppage.clickOnMiniCart();
			Log.message("11. Clicking on Mini cart icon and navigated to the bag page");

			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> This message(N/A) should be changed to 'Applied' After an applicable product is added to the cart.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromCouponSection().contains(Applied),
					"<b>Actual Result 2:</b> This message(N/A) is changed to 'Applied' After an applicable product is added to the cart.",
					"<b>Actual Result 2:</b> This message(N/A) is not changed to 'Applied' After an applicable product is added to the cart.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_065

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify applying a valid coupon code in a cart with items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_058(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		String CouponAppliedSuccesMsg = "Coupon " + CouponCode + " has been applied.";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PDP with Product: '" + searchKey);

			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("8. Coupon is Applied to the Product");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Applying Coupons in the cart, A message “Coupon [coupon code] has been applied” should be displayed below the View All Coupons link.");
			Log.assertThat(shoppingbagpage.getCouponSuccessfullyAppliedMsg().contains(CouponAppliedSuccesMsg),
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, A message “Coupon [coupon code] has been applied” is displayed below the View All Coupons link.",
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, A message “Coupon [coupon code] has been applied” is not displayed below the View All Coupons link.",
					driver);

			LinkedHashMap<String, String> OrderSummaryDetails = shoppingbagpage.getOrderSummaryDetails();
			String CouponSplit[] = OrderSummaryDetails.get("CouponSavings").split("\\-");
			float couponPromotion = Float.parseFloat(CouponSplit[1]);
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 2:</b> Promotions should automatically applied to the order and line item amount in cart upon the entry of the valid coupon code.");
			Log.assertThat(shoppingbagpage.getAppliedCouponPrice() == couponPromotion,
					"<b>Actual Result 2:</b> Promotions automatically applied to the order and line item amount in cart upon the entry of the valid coupon code.",
					"<b>Actual Result 2:</b> Promotions automatically not applied to the order and line item amount in cart upon the entry of the valid coupon code.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4b_058

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Sale Product Discount Savings message in mini-cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_041(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String SaveProductDiscountSavingsMsg = "Checkout now and save $19.02 with merchandise discounts.";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PDP with Product: '" + searchKey);

			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			pdppage.mouseOverMiniCart();
			Log.message("7. Mouse Hovered on MiniCart Icon.");
			MiniCartPage minicartPage = new MiniCartPage(driver);
			System.out.println(minicartPage.getSaleProductDiscountSavingsMsg());
			Log.message("<br>");
			Log.message(
					"<b>Expected Result :</b> The sale product discount savings message Checkout now and save $00.00 with merchandise discounts should be displayed below the free shipping promotions message in the mini-cart.");
			Log.assertThat(minicartPage.getSaleProductDiscountSavingsMsg().contains(SaveProductDiscountSavingsMsg),
					"<b>Actual Result :</b> The sale product discount savings message Checkout now and save $00.00 with merchandise discounts is displayed below the free shipping promotions message in the mini-cart.",
					"<b>Actual Result :</b> The sale product discount savings message Checkout now and save $00.00 with merchandise discounts is not displayed below the free shipping promotions message in the mini-cart.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4b_041

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Enter Zip Code field in the zip code store modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_009(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String ValidZipCode = "28262";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickFindStore();
			Log.message("6. Clicked on 'Find In store' Link");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Clicking 'Find In Store' Link,'Enter Zip Code' text field should be displayed in the zip code store modal");
			Log.assertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("txtEnterZipCode"), pdppage),
					"<b>Actual Result 1:</b> After Clicking on 'Find In Store' Link, 'Enter Zip Code' text field is displayed in the zip code store modal.",
					"<b>Actual Result 1:</b> After Clicking on 'Find In Store' Link, 'Enter Zip Code' text field is not displayed in the zip code store modal.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> User should be able to enter a zip code to search for stores.");
			Log.assertThat(pdppage.EnterZipCode(ValidZipCode),
					"<b>Actual Result 2:</b> User is able to enter a zip code to search for stores.",
					"<b>Actual Result 2:</b> User is not able to enter a zip code to search for stores.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_009

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify Applied Coupons for the items in cart", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_063(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		String AppliedText = "Applied";
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);
			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("8. Coupon is Applied to the Product");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Applying Coupons in the cart, 'Applied' text should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromCouponSection().contains(AppliedText),
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Applied' text  is displayed in the Coupon Section.",
					"<b>Actual Result 1:</b> After Applying Coupons in the cart, 'Applied' text is not displayed in the Coupon Section.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_063

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify applying a valid coupon code in a cart with No items", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_061(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		String WarningMsg = "Please add an item to your bag in order to manually enter additional coupon codes.";

		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigated to PDP with Product: '" + searchKey);
			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("8. Coupon is Applied to the Product");
			shoppingbagpage.removeAddedCartItems();
			Log.message("9. Removed the Items From the Cart");
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 1:</b> After Applying Valid Coupons without Items in the Cart, A warning Message '"
							+ WarningMsg + "' should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getTextFromCouponWarningMsgInEmptyBag().contains(WarningMsg),
					"<b>Actual Result 1:</b> After Applying Coupons without Items in the Cart, in the cart, A warning Message '"
							+ WarningMsg + "' is displayed in the Coupon Section.",
					"<b>Actual Result 1:</b> After Applying Coupons without Items in the Cart, in the cart, A warning Message '"
							+ WarningMsg + "' is not displayed in the Coupon Section.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_061

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows 'Free Shipping' for Belk Rewards Credit Card as Payment option for Elite Customers - Registered User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_188(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");
			// Signing with a valid user id
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid, password);
			Log.message("3. Entered credentials(" + emailid + "/" + password + ") and navigating to 'My Account' page");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag().replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("4. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("6. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage1 = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");
			// Clicking checkout button in order summary module
			CheckoutPage checkoutPage = shoppingBagPage1.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Navigated to checkout page");

			// Filling the details in the shipping page
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "Standard", "valid_address3");
			Log.message("9. Filled the Shipping details in Shipping form!");
			checkoutPage.clickOnContinueInShipping();
			Log.message("10. Clicked on 'Continue' Button in Shipping Page");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("11. Clicked on 'Continue' Button in Validation Modal");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES", "valid_address3");
			Log.message("12. Filled the Billing details in Billing form!");

			checkoutPage.fillingCardDetails("NO", "card_BelkRewardsCreditCard");
			Log.message("13. Filled Card Details in Billing Page");

			checkoutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on continue button in Billing page");

			Log.message(
					"<br><b>Expected Result:</b> The Free shipping should be allowed , when customer choose belk Rewaed Credit Card as Payment");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingDiscountInOrdersummary"),
							checkoutPage),
					"The Free shipping is allowed , when customer choose belk Rewaed Credit Card as Payment",
					"The Free shipping is not allowed , when customer choose belk Rewaed Credit Card as Payment",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4B_188

	@Test(groups = { "desktop", "mobile",
			"tablet" }, description = "Verify the discount details shown for the items in cart after applying the coupons", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_066(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		String CouponCode = testData.get("CouponCode");
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			PdpPage pdppage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			Log.message("3. Selected color: '" + pdppage.selectColor() + "' from color swatch in the Pdp Page!");
			Log.message("4. Selected size: '" + pdppage.selectSize() + "' from Size dropdown in the Pdp Page!");
			float quantity = Float.parseFloat(pdppage.selectQuantity("2"));
			Log.message("5. Selected quantity: '" + quantity + "' from quantity DropDown in the Pdp Page!");
			pdppage.clickAddToBag();
			Log.message("6. Clicked on Add to bag button");
			ShoppingBagPage shoppingbagpage = pdppage.clickOnMiniCart();
			Log.message("7. Navigated to Shoppingbagpage!");
			shoppingbagpage.applyCouponInShoppingBag(CouponCode);
			Log.message("8. Coupon is Applied to the Product");

			String CouponDiscount = Float.toString(shoppingbagpage.getAppliedCouponPrice());
			Log.message("<br>");
			Log.message(
					"<b>Expected Result 3:</b> After Applying Coupons in the cart, 'Discount' should be displayed in the Coupon Section.");
			Log.assertThat(shoppingbagpage.getCouponDetailsFromCouponSection().contains("-$" + CouponDiscount),
					"<b>Actual Result 3:</b> After Applying Coupons in the cart, 'Discount': -$" + CouponDiscount
							+ " is displayed in the Coupon Section.",
					"<b>Actual Result 3:</b> After Applying Coupons in the cart, 'Discount': -$" + CouponDiscount
							+ " is not displayed in the Coupon Section.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_4b_066

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system enables the 'Wish list' link for eligible products.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_118(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			Log.message(
					"<br><b>Expected Result:</b> The 'Wish list' link for the product should be displayed in the ShoppingBag Page");

			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("btnAddToWishlist"), shoppingBagPage),
					"The 'Wish list' link for the product is displayed in the ShoppingBag Page",
					"The 'Wish list' link for the product is not displayed in the ShoppingBag Page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_118

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify 'Online only' text displays for the product.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_127(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			SearchResultPage searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword : '" + searchKey);

			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message("3. Navigated to PDP Page");

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			Log.message(
					"<br><b>Expected Result:</b> 'Online only' text should be displayed for the product which can be purchased only via online in the ShoppingBag Page");

			Log.message("<b>Actual Result:</b>");
			Log.assertThat(shoppingBagPage.getTxtOnlineOnly().contains("Online Only"),
					"'Online only' text is displayed for the product which can be purchased only via online in the ShoppingBag Page",
					"'Online only' text is not displayed for the product which can be purchased only via online in the ShoppingBag Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_127

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify system allows User to add valid Belk Certificate Number", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_196(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();

			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("5. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("6.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("7. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("8. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("9. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar_Valid");
			Log.message("10. Filled Bellk reward Dollars details");

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message("11. Clicked on 'Apply belk reward dollar' in the Billing Page");

			Log.message(
					"<br><b>Expected Result1:</b> The 'Belk Reward Dollars Applied' successfully message should be displayed in the Billing Page");

			Log.message("<b>Actual Result1:</b>");
			Log.assertThat(
					checkoutPage.getMsgBelkRewardDollarsApplied()
							.contains("Your Belk Reward Dollars have been applied"),
					"The 'Belk Reward Dollars Applied' successfully message is displayed in the Billing Page",
					"The 'Belk Reward Dollars Applied' successfully message is not displayed in the Billing Page",
					driver);

			Log.message(
					"<br><b>Expected Result2:</b> Belk Reward Dollars Amount should be updated in the Order Summary in Billing page");

			Log.message("<b>Actual Result2:</b>");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtBelkRewardDollarAmount"),
							checkoutPage),
					"Belk Reward Dollars Amount is updated in the Order Summary in Billing page",
					"Belk Reward Dollars Amount is not updated in the Order Summary in Billing page", driver);

			String BRDNumber = checkoutPage.getBRDNumberFromSection(0);

			Log.message(
					"<br><b>Expected Result3:</b> The Last four digits of the Belk  Reward Dollars number should be displayed in Billing page");

			Log.message("<b>Actual Result3:</b>");
			Log.assertThat(BRDNumber.length() == 4,
					"The Last four digits of the Belk  Reward Dollars number is displayed in Billing page",
					"The Last four digits of the Belk  Reward Dollars number is not displayed in Billing page", driver);

			Log.message(
					"<br><b>Expected Result4:</b> 'Remove' link should be displayed for applied Belk Reward dollar in Billing page");
			Log.message("<b>Actual Result4:</b>");
			Log.assertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnRemoveInBRD"), checkoutPage),
					"'Remove' link is displayed for applied Belk Reward dollar in Billing page",
					"'Remove' link is not displayed for applied Belk Reward dollar in Billing page", driver);

			checkoutPage.clickonRemoveBelkRewardDollar();
			Log.message("12. Clicked on 'Remove' link ");

			Log.message(
					"<br><b>Expected Result5:</b> By clicking the Belk Reward Dollars 'Remove' Link ,It should be removed in the Billing Page");
			Log.message("<b>Actual Result5:</b>");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtBelkRewardDollarAmount"),
							checkoutPage),
					"By clicking the Belk Reward Dollars 'Remove' Link ,It is removed in the Billing Page",
					"By clicking the Belk Reward Dollars 'Remove' Link ,It is not removed in the Billing Page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_196

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify order summary details updates as per the Coupon code applied for the added item", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_4B_220(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag("14549496");
			Log.message("5. Coopon Code Applied");

			shoppingBagPage.checkAddGiftBox();
			Log.message("6. Selected 'AddGiftBox' in the ShoppinBag Page");

			homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("7. Navigate to PDP Page with another Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			pdpPage.addProductToBag();
			Log.message("8. Another Product added to Shopping Bag!");

			pdpPage.clickOnMiniCart();
			Log.message("9. Clicking on Mini cart icon is navigated to the bag page");

			SignIn siginPage = (SignIn) shoppingBagPage.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Naviagted to checkout page");

			CheckoutPage checkoutPage = siginPage.clickOnCheckoutAsGuest();
			Log.message("11.'CheckoutAsGuest' button is clicked");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address7", "Standard");
			Log.message("12. Filled Shipping Address in Shipping Page");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.chooseOriginalAddressInModal();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Navigate to Billing Page After Filled Shipping Details");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address7");
			Log.message("14. Filled Billing Address in Billing Page");

			checkoutPage.fillingBelkGiftCardDetails("card_GiftCard");
			Log.message("15. Filled Gift card details");

			checkoutPage.clickOnApplyGiftCard();
			Log.message("16. Clicked on 'ApplyGiftCard'");

			checkoutPage.fillingBelkRewardDollars("belk_Reward_Dollar_Valid");
			Log.message("17. Filled Belk reward Dollars Details");

			checkoutPage.clickOnApplyBelkRewardDollars();
			Log.message("18. Clicked on 'Apply Belk Reward Dollars'");

			checkoutPage.fillingCardDetails("NO", "card_Valid_Discover");
			Log.message("19. Filled Card Details in Billing details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("20. Clicked on continue button in Billing page");

			Log.message(
					"<br><b>Expected Result:</b> Order Summary should show the details 'Merchandise Total' , 'Total Coupon Savings' , 'Belk Reward Dollars' , 'Shipping Surcharge' , 'Estimated Shipping' , 'Estimated Sales Tax' , 'Belk Gift Card' , 'Estimated Order Total' in the place order screen");
			Log.message("<b>Actual Result5:</b>");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("txtMerchandiseTotalCost", "txtTotalCouponSavings", "txtGiftBoxValue",
									"txtSurchargeValue", "txtShippingCost", "lblAmount", "txtBelkRewardDollarAmount",
									"txtEstimatedSalesTaxCost", "txtEstimatedSalesTaxCost"),
							checkoutPage),
					"Order Summary shown the details 'Merchandise Total' , 'Total Coupon Savings' , 'Belk Reward Dollars' , 'Shipping Surcharge' , 'Estimated Shipping' , 'Estimated Sales Tax' , 'Belk Gift Card' , 'Estimated Order Total' in the place order screen",
					"Order Summary did'nt shown the details 'Merchandise Total' , 'Total Coupon Savings' , 'Belk Reward Dollars' , 'Shipping Surcharge' , 'Estimated Shipping' , 'Estimated Sales Tax' , 'Belk Gift Card' , 'Estimated Order Total' in the place order screen",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally
	}// TC_BELK_CART_4B_220

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify the Shipping Surcharge displayed on Cart Line Item when 'Surcharge_Flag' is set to 'Y'.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_108(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			Log.message(
					"<br><b>Expected Result:</b> The Shipping Surcharge message should be displayed on the Cart Line Item in the ShoppingBag Page");

			Log.message("<b>Actual Result:</b>");
			Log.assertThat(shoppingBagPage.getMsgItemIncldeSurchargealert(),
					"The Shipping Surcharge message is displayed on the Cart Line Item in the ShoppingBag Page",
					"The Shipping Surcharge message is not displayed on the Cart Line Item in the ShoppingBag Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_108

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify the Shipping Surcharge displayed on Cart Line Item when 'Surcharge_Flag' is set to 'N'..", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_109(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page");

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();
			pdpPage.addProductToBag();
			Log.message("3. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("4. Clicking on Mini cart icon is navigated to the bag page");

			Log.message(
					"<br><b>Expected Result:</b> The Shipping Surcharge message should not be displayed on the Cart Line Item in the ShoppingBag Page");

			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblLineitemalertmessage"),
							shoppingBagPage),
					"The Shipping Surcharge message is not displayed on the Cart Line Item in the ShoppingBag Page",
					"The Shipping Surcharge message is displayed on the Cart Line Item in the ShoppingBag Page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_CART_109

	@Test(groups = { "desktop", "tablet",
			"mobile" }, description = "Verify Regular Checkout for Logged In Users from cart page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_CART_067(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myAcc = signIn.signInToMyAccount(emailid, password);
			ShoppingBagPage bagPage = myAcc.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}

			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("5. Navigate to PDP Page with Product: '" + searchKey);

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQtyByIndex(2);
			pdpPage.addProductToBag();
			Log.message("6. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicking on Mini cart icon is navigated to the bag page");

			shoppingBagPage.applyCouponInShoppingBag("14549496");
			Log.message("8. Coupon Code Applied");

			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message("9. Clicked on checkout in the ShoppingBag Page");

			Log.message(
					"<br><b>Expected Result:</b> After Clicking Checkout in shopping bag page , it should be redirected to Checkout: Step 1 ­ Shipping page.");
			Log.message("<b>Actual Result5:</b>");
			Log.assertThat(checkoutPage.getTextFromBillingHeader().contains("Return to Shopping Bag"),
					"After Clicking Checkout in shopping bag page , it is redirected to Checkout: Step 1 ­ Shipping page.",
					"After Clicking Checkout in shopping bag page , it is not redirected to Checkout: Step 1 ­ Shipping page.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally
	}// TC_BELK_CART_067

}// checkout
