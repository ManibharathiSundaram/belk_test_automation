package com.belk.testscripts.myaccount;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.syntax.jedit.InputHandler.home;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CheckoutPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.pages.HomePage;
import com.belk.pages.OrderConfirmationPage;
import com.belk.pages.PdpPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.AddressBookPage;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.OrderHistoryPage;
import com.belk.pages.account.PaymentMethodsPage;
import com.belk.pages.account.ProfilePage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.ApoFpoPage;
import com.belk.pages.footers.WhyIsRequiredPage;
import com.belk.pages.registry.RegistryInformationPage;
import com.belk.reusablecomponents.StateUtils;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;
import com.gargoylesoftware.htmlunit.util.StringUtils;

@Listeners(EmailReport.class)
public class MyAccount {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "MyAccount";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop" }, description = "Verify Left Navigation in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_003(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase 'Left Nav' is not applicable for mobile view");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			int i = 1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The Left navigation should enabled in the signIn page");

			Log.assertThat(
					signinPage.elementLayer.verifyPageElements(
							Arrays.asList("secondaryNavigation"), signinPage),
					"<b>Actual Result-1:</b> The Left navigation is enabled in the signIn page",
					"<b>Actual Result-1:</b> The left navigation is disabled in the signIn page",
					driver);
			Log.assertThat(
					signinPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstContentAssetTitle"), signinPage),
					"<b>Actual Result-1:</b> The content asset titles 'Account Setting' and 'Shopping Confidently' is displayed",
					"<b>Actual Result-1:</b> The content titles are not displayed");

			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The 'Create Account', 'privacyPolicy' and 'Secure shopping' should displayed in the signIn page");

			String hrefCreateAccount = signinPage.getLeftNavAttributeByTitle(
					"createAccount", "href");
			Log.assertThat(
					(signinPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkCreateAccount"), signinPage))
							&& (hrefCreateAccount.contains("https")),
					"<b>Actual Result-2:</b> The 'Create Account' link is displayed with linkable",
					"<b>Actual Result-2:</b> The 'Create Account' link is not displayed");
			String hrefPrivacyPolicy = signinPage.getLeftNavAttributeByTitle(
					"privacyPolicy", "href");
			Log.assertThat(
					(signinPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkPrivacyPolicy"), signinPage))
							&& (hrefPrivacyPolicy.contains("https")),
					"<b>Actual Result-2:</b> The 'privacyPolicy' link is displayed with linkable",
					"<b>Actual Result-2:</b> The 'privacy Policy' link is not displayed");
			String hrefSecureShopping = signinPage.getLeftNavAttributeByTitle(
					"secureShopping", "href");
			Log.assertThat(
					(signinPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkSecureShopping"), signinPage))
							&& (hrefSecureShopping.contains("https")),
					"<b>Actual Result-2:</b> The 'Secure shopping' link is displayed with linkable",
					"<b>Actual Result-2:</b> The 'Secure shopping' link is not displayed",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_003

	@Test(groups = { "desktop", "mobile" }, description = "Verify 'Need Help?' content displayed in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The Need Help pane should displayed");

			if (Utils.getRunPlatForm() == "mobile") {
				Log.assertThat(
						signinPage.elementLayer
								.verifyPageElements(
										Arrays.asList("needHelpPaneMobile"),
										signinPage),
						"<b>Actual Result-1:</b> The 'Need help?' pane is displayed",
						"<b>Actual Result-1:</b> The 'Need help?' pane is not displayed",
						driver);
			} else {
				Log.assertThat(
						signinPage.elementLayer.verifyPageElements(
								Arrays.asList("needHelpPane"), signinPage),
						"<b>Actual Result-1:</b> The 'Need help?' pane is displayed",
						"<b>Actual Result-1:</b> The 'Need help?' pane is not displayed",
						driver);

			}

			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The following content should displayed in the 'Need help' pane (Email, Phone number, Reward card customer care phone number)");

			String needHelpText = signinPage.getNeedHelpText();
			Log.softAssertThat(
					needHelpText.contains("Belk_Customer_Care@belk.com"),
					"<b>Actual Result-2:</b> The mail id 'Belk_Customer_Care@belk.com' is displayed in the customer service content",
					"<b>Actual Result-2:</b> The mail id 'Belk_Customer_Care@belk.com' is not displayed in the customer service content");
			Log.softAssertThat(
					needHelpText.contains("1-866-235-5443"),
					"<b>Actual Result-2:</b> The phone number '1-866-235-5443' is displayed in the customer service content",
					"<b>Actual Result-2:</b> The phone number '1-866-235-5443' is not displayed in the customer service content");
			Log.softAssertThat(
					needHelpText.contains("1-800-669-6550"),
					"<b>Actual Result-2:</b> The reward card customer service phone number '1-800-669-6550' is displayed in the customer service content",
					"<b>Actual Result-2:</b> The reward card customer service phone number '1-800-669-6550' is not displayed in the customer service content",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_004

	@Test(groups = { "desktop" }, description = "Verify Additional address under the Additional Addresses pane", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_079(String browser) throws Exception {
		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase 'Address 3 Rows' is not applicable for mobile view");
		}
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ " / '" + password);

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			int totalAddress = addressbookpage.getTotalAddressCount();

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> System should limit the number of address to display to 20");
			Log.assertThat((totalAddress == 20), "<b>Actual Result-1:</b> '"
					+ totalAddress + "' Addresses is displayed",
					"<b>Actual Result-1:</b> '" + totalAddress
							+ "' Addresses is displayed", driver);
			String classNameOfAddNewAddressButton = addressbookpage
					.getClassNameOfAddNewAddress();
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The Add Address button should disabled if the saved address reached 20");
			Log.assertThat(
					(totalAddress == 20)
							&& (classNameOfAddNewAddressButton
									.contains("disabled")),
					"<b>Actual Result-2:</b> The total address is '"
							+ totalAddress
							+ "' so the 'Add New Address' button is disabled'",
					"<b>Actual Result-2:</b> The total address is '"
							+ totalAddress
							+ "' so the 'Add New Address' button is not disabled'",
					driver);

			int fourthAddressLocation = addressbookpage.getLocation(4);
			int fifthAddressLocation = addressbookpage.getLocation(5);
			int sixthAddressLocation = addressbookpage.getLocation(6);

			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> Three addresses should displayed per row");
			Log.softAssertThat(
					(fourthAddressLocation == fifthAddressLocation)
							&& (fifthAddressLocation != sixthAddressLocation),
					"<b>Actual Result-3:</b> Three addresses are displayed per row",
					"<b>Actual Result-3:</b> Three addresses are not displayed per row",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_079

	@Test(groups = { "desktop", "mobile" }, description = "Verify Additional Addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_070(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ " / '" + password);

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			boolean status = addressbookpage
					.verifySetDefaultBillingShippingIsChecked();
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> Addresses that should not set as default billing or shipping should display under the Additional Addresses header.");
			Log.assertThat(
					status,
					"<b>Actual Result-1:</b> Addresses that are not set as default billing or shipping should display under the Additional Addresses header.",
					"<b>Actual Result-1:</b> Addresses that are set as default billing or shipping should display under the Additional Addresses header.",
					driver);

			int totalAddress = addressbookpage.getTotalAddressCount();
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b>system should limit the number of address to display 20.");
			Log.assertThat((totalAddress == 20), "<b>Actual Result-2:</b> '"
					+ totalAddress + "' Addresses is displayed",
					"<b>Actual Result-2:</b> '" + totalAddress
							+ "' Addresses is displayed", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_070

	@Test(groups = { "desktop", "mobile" }, description = "Verify Additional Addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_080(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String lastName = testData.get("LastName");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ " / '" + password);

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			addressbookpage.clickOnEditAddressByIndex(5);
			Log.message((i++)
					+ ". Clicked on the Edit link from additional address");
			addressbookpage.typeOnLastName(lastName);
			Log.message((i++) + ". Edited the Last Name:'" + lastName + "'",
					driver);
			addressbookpage.ClickOnSave();
			Log.message((i++) + ". Clicked on the 'Save' button");
			String nameOnAddress = addressbookpage.getNameFromAddressByIndex(5);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The modified address details should get updated.");
			Log.assertThat(nameOnAddress.contains(lastName),
					"<b>Actual Result:</b> The address get updated.",
					"<b>Actual Result:</b> The address is not updated.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_080

	@Test(groups = { "desktop", "mobile" }, description = "Verify unchecking 'Express checkout' checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_036(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(Arrays.asList(
							"defaultPaymentMethodPane",
							"defaultShippingAddressPane",
							"defaultBillingAddressPane"), profilepage),
					"5. The default Shipping, Billing and Payment method is displayed.",
					"5. The default Shipping, Billing and/or Payment method is not displayed.",
					driver);

			if (!profilepage.isExpressCheckoutCheckboxEnabled()) {
				profilepage.checkExpressCheckout("UnCheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.event((i++) + ". Unchecked the Express checkout check box");
			} else {
				Log.event("The express checkout is unchecked by default");
			}

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Navigated to '" + searchKey
					+ "' search result page");
			PdpPage pdpPage = searchresultpage.navigateToPDP();
			Log.message((i++) + ". Navigated to PDP page with random product");
			String color = pdpPage.selectColor();
			Log.message((i++) + ". Selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message((i++) + ". Selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Clicked on the 'Add to bag' button");
			homePage.headers.clickSignOut();
			Log.message((i++) + ". Clicked on the SignOut link");
			signinPage = homePage.headers.navigateToSignIn();
			myaccountpage = signinPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Again login to the application with '"
					+ emailid + " / '" + password);

			ShoppingBagPage shoppingbagPage = homePage.headers
					.NavigateToBagPage();
			Log.message((i++) + ". Navigated to the shopping bag page");

			CheckoutPage checkoutpage = shoppingbagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++)
					+ ". Clicked on the Checkout button from the shopping bag page");

			String className = checkoutpage.getClassNameOfStepsInCheckoutPage(
					"shipping").split(" ")[1];
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The user should not be navigated to the Place Order page directly but re-directed to the checkout page for entering the shipping address.");
			Log.assertThat(
					className.equals("active"),
					"<b>Actual Result:</b> The page is redirected to the Shipping address page",
					"<b>Actual Result:</b> The page is not redirected to Shipping address page.",
					driver);
			checkoutpage.clickReturnToShoppingBag();
			if (Integer.parseInt(myaccountpage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingbagPage.removeItemsFromBag();
				Log.event("Cleanedup the shopping bag");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_036

	@Test(groups = { "desktop", "mobile" }, description = "Verify adding new payment in Set Default Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_047(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			if (profilepage.verifyDefaultPaneAvailabilityByName("payment")) {
				if (Utils.getRunPlatForm() == "mobile") {
					myaccountpage = homePage.headers.navigateToMyAccount();
				}
				PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) myaccountpage
						.navigateToSection("paymentmethods");
				Log.message((i++) + ".Navigated to Payment method page");
				paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++) + ".Deleted the saved card details");
				if (Utils.getRunPlatForm() == "mobile") {
					myaccountpage = homePage.headers.navigateToMyAccount();
				}
				profilepage = (ProfilePage) myaccountpage
						.navigateToSection("profile");
				Log.message((i++) + ".Navigated to profile page");
			}
			profilepage.clickOnChangeDefaultInPayment();
			Log.message((i++) + ". Clicked on change default in payment");
			profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetDefaultInPayment();
			Log.message((i++) + ". Clicked on the Set default button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The set default payment should add successfully.");
			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(
							Arrays.asList("defaultPaymentMethodPane"),
							profilepage),
					"<b>Actual Result:</b> The payment method successfully added",
					"<b>Actual Result:</b> The payment method is not added",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_047

	@Test(groups = { "desktop", "mobile" }, description = "Verify that the user is able to select an address from the 'select possible matches' option in the 'Address not Recognized by USPC' modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_097(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");
			int addresscount=addressbookpage.getTotalAddressCount();
			for(int j=1;j<=addresscount;j++) {
				addressbookpage.deleteAddressByIndex(0);
				Log.event(".Deleted the already saved address");
			}
			addressbookpage.clickAddNewAddress();
			Log.message((i++) + ". 'Add New Address' is clicked");
			addressbookpage.fillingAddNewAddressDetails("usps_address", "YES",
					"YES");
			Log.message((i++) + ". Address is filled");
			addressbookpage.ClickOnSave();
			Log.message((i++) + ". Clicked save button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The The address dropdown should displayed in the 'Address not Recognized by USPC' modal");
					Log.assertThat(addressbookpage.elementLayer.verifyPageElements(Arrays.asList("drpPossibleAddress"), addressbookpage),
							"<b>Actual Result:</b> The address dropdown is displayed",
							"<b>Actual Result:</b> The address dropdown is not displayed");
			addressbookpage.clickAddressDropown();
			Log.message((i++) + ". Clicked on the address dropdown");
			addressbookpage.selectAddressByIndex(2);
			Log.message((i++) + ". Select address from address dropdown");
			String selectedAddress=addressbookpage.getSelectedAddressFromUSPC();
			int number = 0;
			if(selectedAddress.contains("even")){
				String partialAddress=selectedAddress.split("\\...")[0].trim();
				String[] splittedValue=partialAddress.split(" ");
				number=Integer.parseInt(splittedValue[splittedValue.length-1]);
				number=number+2;
				addressbookpage.typeApartmentNumber(Integer.toString(number));
				addressbookpage.clickContinueAddressSuggestion();
				Log.message((i++) + ". Entered the even number in the Apartment number text box and clicked the continue button");
			}
			
			
			addressbookpage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Again clicked the continue button for suggested address");
			LinkedHashMap<String, String> savedAddress=addressbookpage.getSavedAddressByIndex(0);
			
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The selected address should added");
			Log.assertThat(addressbookpage.getTotalAddressCount()>0, "<b>Actual Result:</b> The selected address is added", "<b>Actual Result:</b> The selected address is added",driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_097

	@Test(groups = { "desktop", "mobile" }, description = "Verify adding additional payment type in Add Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_129(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) myaccountpage
					.navigateToSection("paymentmethods");
			Log.message((i++) + ". Navigated to payment method page");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();
			if (totalPayment > 3) {
				paymentmethodpage.deletePaymentByIndex(3);
				Log.message((i++) + ".Deleted the already saved card details");
			}
			totalPayment = paymentmethodpage.getTotalPaymentCount();
			paymentmethodpage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked on the 'Add Payment Method' button");
			paymentmethodpage.fillingCardDetails("NO", "card_MasterCard");
			Log.message((i++) + ". Filled card details");
			paymentmethodpage.clickOnSave();
			Log.message((i++) + ". Clicked on the 'Save' button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The payment popup should closed after clicked the save button.");
			Log.assertThat(
					paymentmethodpage.verifyPopupAvailability(),
					"<b>Actual Result:</b> The payment popup is closed after clicked the save button",
					"<b>Actual Result:</b> The card is not saved, may be data issue",
					driver);
			if (Utils.getRunPlatForm() == "desktop") {
				int secondPaymentLocation = paymentmethodpage
						.getLocationofSavedPayment(2);
				int ThirdpaymentLocation = paymentmethodpage
						.getLocationofSavedPayment(3);
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The payment method should displayed 3 per row");
				Log.assertThat(
						secondPaymentLocation == ThirdpaymentLocation,
						"<b>Actual Result:</b> The payment method is displayed 3 per row",
						"<b>Actual Result:</b> The payment method is not displayed 3 per row");
			}
			int size = paymentmethodpage
					.verifyPaymentBelowAdditionalAndReturnSize();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The payment method should added below the additional payment");
			Log.assertThat(
					size > totalPayment,
					"<b>Actual Result:</b> The payment method is added below the additional payment",
					"<b>Actual Result:</b> The payment method is not added");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Save Changes' button is displayed below the additional addresses");
			Log.assertThat(
					paymentmethodpage.verifySaveChangesBelowAddress(),
					"<b>Actual Result:</b> The 'Save Changes' button is displayed below the additional addresses",
					"<b>Actual Result:</b> The 'Save Changes' button is not displayed below the additional addresses",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_129

	@Test(groups = { "desktop", "mobile" }, description = "Verify existing card details in default Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_045(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			if (profilepage.paymentMethodVisiblity()) {
				if (Utils.getRunPlatForm() == "mobile") {
					myaccountpage = homePage.headers.navigateToMyAccount();
				}
				PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) myaccountpage
						.navigateToSection("paymentmethods");
				Log.event("Navigated to Payment method page");
				paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++) + ".Deleted the already saved card details");
				if (Utils.getRunPlatForm() == "mobile") {
					myaccountpage = homePage.headers.navigateToMyAccount();
				}
				profilepage = (ProfilePage) myaccountpage
						.navigateToSection("profile");
				Log.message((i++) + ".Navigated to profile page");
			}
			profilepage.clickOnChangeDefaultInPayment();
			Log.message((i++) + ". Clicked on 'Set Default Payment' button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Saved Card dropdown box should not display");
			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(
							Arrays.asList("lblSavedCardFromDialog"),
							profilepage),
					"<b>Actual Result:</b> The 'Saved card' is not displayed as expected",
					"<b>Actual Result:</b> The 'Saved card' is displayed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_045

	@Test(groups = { "desktop", "mobile" }, description = "Verify adding new shipping address in Set as Default address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_117(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The default billing, shipping and payment method should be displayed");
			Log.softAssertThat(
					profilepage.elementLayer.verifyPageElements(Arrays.asList(
							"defaultPaymentMethodPane",
							"defaultShippingAddressPane",
							"defaultBillingAddressPane"), profilepage),
					"<b>Actual Result-1:</b> The default Shipping, Billing and Payment method is displayed.",
					"<b>Actual Result-1:</b> The default Shipping, Billing and/or Payment method is not displayed.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The Express checkout check box should checked");
			Log.assertThat(
					profilepage.isExpressCheckoutCheckboxChecked(),
					"<b>Actual Result-2:</b> The Express checkout check box is checked",
					"<b>Actual Result-2:</b> The Express checkout check box is not checked");
			if (Utils.getRunPlatForm() == "mobile") {
				myaccountpage = homePage.headers.navigateToMyAccount();
				Log.message((i++) + ". Navigated to My Account page");
			}
			Log.message("<br>");
			AddressBookPage addressbookpage = (AddressBookPage) profilepage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");
			int totalAddress = addressbookpage.getTotalAddressCount();
			if (totalAddress > 2) {
				addressbookpage.deleteAddressByIndex(2);
				Log.message((i++) + ".Deleted the already saved card details");
			}
			totalAddress = addressbookpage.getTotalAddressCount();
			addressbookpage.clickOnEditAddressByIndex(2);
			Log.message((i++) + ". Clicked on Edit link");
			addressbookpage.clickOnCancelInAddNewAddressModal();
			Log.message((i++) + ". Clicked on the 'Cancel' button");
			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> The Edit dialog container should be closed when clicked on the Cancel button");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lblAddressFormDialogContainer"),
							addressbookpage),
					"<b>Actual Result-3:</b> The Edit Address dialog container is closed",
					"<b>Actual Result-3:</b> The Edit address dialog container is not closed",
					driver);
			BrowserActions.nap(3);
			addressbookpage.clickOnEditAddressByIndex(2);
			Log.message("<br>");
			Log.message((i++) + ". Again clicked on Edit link");
			addressbookpage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Unchecked the default checkbox");
			addressbookpage.fillingAddNewAddressDetails("valid_address6", "NO",
					"NO");
			Log.message((i++) + ". Filled the address details");
			addressbookpage.clickOnSetAsDefaultInAddNewAddressModal();
			addressbookpage.selectUseOriginalAddress();
			addressbookpage.ClickOnContinueInValidationPopUp();
			BrowserActions.nap(5);
			Log.message((i++)
					+ ". Seleted 'Use original address' and clicked on 'Continue' button");
			Log.message("<br>");
			Log.message("<b>Expected Result-4:</b> The new address should be added");
			Log.assertThat(
					addressbookpage.getTotalAddressCount() > totalAddress,
					"<b>Actual Result-4:</b> The New address is added",
					"<b>Actual Result-4:</b> The New address is not added",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_117

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section of the site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.enterEmailID(emailid);
			signinPage.enterPassword(password);
			signinPage.clickBtnSignIn();
			Log.message((i++) + ". Entered Invalid credentials(" + emailid
					+ "/" + password + ") in 'Sign In' page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Error Message should be shown to block user from Login.");
			Log.assertThat(
					signinPage
							.getSignInFormErrorMsg()
							.equals("Sorry, this does not match our records. Check your spelling and try again."),
					"<b>Actual Result:</b> Invalid login error message '"
							+ signinPage.getSignInFormErrorMsg()
							+ "' is shown blocking user from login.",
					"<b>Actual Result:</b> No Login error message is shown to block user from login.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_006

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify creating an account from My Account screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_148(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String phoneNumber = testData.get("PhoneNumber");
		String emailID = testData.get("EmailAddress");
		String confirmEmailID = testData.get("ConfirmEmailAddress");
		String password = testData.get("Password");
		String confirmPassword = testData.get("ConfirmPassword");

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
				.format(new Date());
		String emailIDList[] = emailID.split("@");
		String confirmEmailIDList[] = confirmEmailID.split("@");

		firstName = firstName + timeStamp;
		emailID = emailIDList[0] + timeStamp + "@" + emailIDList[1];
		confirmEmailID = confirmEmailIDList[0] + timeStamp + "@"
				+ confirmEmailIDList[1];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message((i++)
					+ ". Clicked on Create Account button in SignIn page");

			// Creating a new account
			createAccountPage.enterFirstName(firstName);
			createAccountPage.enterLastName(lastName);
			createAccountPage.enterPhoneNumber(phoneNumber);
			createAccountPage.enterEmailID(emailID);
			createAccountPage.enterConfirmationEmailID(confirmEmailID);
			createAccountPage.enterPassword(password);
			createAccountPage.enterConfirmPassword(confirmPassword);
			Log.message((i++)
					+ ". Filled the Information for creating new account");

			// Click Create Account Button
			createAccountPage.clickCreateCreateAccountBtn();
			Log.message((i++) + ". Clicked the Create Account Button");

			WishListPage wishListPage = new WishListPage(driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The User should be navigated to Wish List Page after Creating Account");

			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Wishlist-Show")
							&& wishListPage != null,
					"<b>Actual Result:</b>  Creating a 'New Account' from WishList is navigated to WishList page",
					"<b>Actual Result:</b>  Creating a 'New Account' from WishList is Not navigated to My WishList page with expected url is: "
							+ driver.getCurrentUrl(), driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_148

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify entering a password and a Confirm password that do not match.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_150(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String phoneNumber = testData.get("PhoneNumber");
		String emailID = testData.get("EmailAddress");
		String confirmEmailID = testData.get("ConfirmEmailAddress");
		String password = testData.get("Password");
		String confirmPassword = testData.get("ConfirmPassword");

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
				.format(new Date());
		String emailIDList[] = emailID.split("@");
		String confirmEmailIDList[] = confirmEmailID.split("@");

		firstName = firstName + timeStamp;
		emailID = emailIDList[0] + timeStamp + "@" + emailIDList[1];
		confirmEmailID = confirmEmailIDList[0] + timeStamp + "@"
				+ confirmEmailIDList[1];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message((i++)
					+ ". Clicked on Create Account button in SignIn page");

			// Creating a new account
			Log.message((i++) + ". Fill In Information");
			createAccountPage.enterFirstName(firstName);
			createAccountPage.enterLastName(lastName);
			createAccountPage.enterPhoneNumber(phoneNumber);
			createAccountPage.enterEmailID(emailID);
			createAccountPage.enterConfirmationEmailID(confirmEmailID);
			createAccountPage.enterPassword(password);
			createAccountPage.enterConfirmPassword(confirmPassword);
			Log.message((i++)
					+ ".Filled the information for creating new account");
			// Click Create Account Button
			createAccountPage.clickCreateCreateAccountBtn();
			Log.message((i++) + ". Click Create Account Button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Error Message should be shown to block user from Creating new User Account.");

			Log.assertThat(
					createAccountPage.getCreateAccountFormErrorMsg().equals(
							"please enter valid password"),
					"<b>Actual Result:</b> Invalid Password error message '"
							+ createAccountPage.getCreateAccountFormErrorMsg()
							+ "' is shown blocking user from Creating new User Account..",
					"<b>Actual Result:</b> No Login error message is shown to block user from Creating new User Account..",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_150

	@Test(groups = { "desktop", "tablet" }, description = "Verify Breadcrumb in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_001(String browser) throws Exception {

		if (Utils.getRunPlatForm() == "mobile") {
			throw new SkipException(
					"This testcase 'Breadcrumb' is not applicable for Mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SignInPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to SignIn Page");

			List<String> txtInBreadCrumb = SignInPage.getTextInBreadcrumb();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The breadcrumb should displayed as 'Home > Sign In/Register'");
			Log.assertThat(
					txtInBreadCrumb.get(0).equals("Home")
							&& txtInBreadCrumb.get(1)
									.equals("Sign In/Register"),
					"<b>Actual Result:</b>  The breadcrumb is displayed as expected - 'Home > Sign In/Register'",
					"<b>Actual Result:</b>  The breadcrumb is not displayed as expected - 'Home > Sign In/Register' and the actual is : '"
							+ txtInBreadCrumb.get(0)
							+ " > "
							+ txtInBreadCrumb.get(1) + "'", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_001

	@Test(groups = { "mobile" }, description = "Verify Breadcrumb in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SignInPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to signIn page");

			List<String> txtInBreadCrumb = SignInPage.getTextInBreadcrumb();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The breadcrumb should displayed as 'Back to Home'");

			Log.assertThat(
					txtInBreadCrumb.get(0).equals("Back to Home"),
					"<b>Actual Result:</b>  The breadcrumb is displayed as expected - '"
							+ txtInBreadCrumb.get(0) + "'",
					"<b>Actual Result:</b>  The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
							+ txtInBreadCrumb.get(0) + "'", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_002

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section of the site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_025(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();
		String errorMessage = "We're sorry, but we are unable to create an account using that email address because it's already in use. Please use a different email address to create your account or try logging in.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			// update email, email confirmation and password field
			editProfilePage.typeOnEmailField("testuser1@automation.com");
			editProfilePage.typeOnConfirmEmailField("testuser1@automation.com");
			editProfilePage.typeOnPasswordField("password6");
			Log.message((i++)
					+ ". Updated values of email, confirm email and password fields on Edit Profile Page.");

			// click on 'ProfileApplyButon'
			editProfilePage.clickOnProfileApplyChanges();
			Log.message((i++) + ". Clicked on Profile Apply Button");

			// get the error message 'Email Already in use' and verify the text
			String message = editProfilePage.getEmailAlreadyInUseErrorMessage();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Error message, Email Already in use should displayed in UI.");

			Log.assertThat(
					errorMessage.equals(message),
					"<b>Actual Result:</b> Error message, Email Already in use is displayed in UI.",
					"<b>Actual Result:</b> Error message, Email Already in use is not displayed in UI.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_025

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Order Detail with invalid data from My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String orderemail = testData.get("InvalidOrderEmail");
		String ordernumber = testData.get("InvalidOrderNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.signInToViewOrderDetail(orderemail, ordernumber);
			Log.message((i++) + ". Entered Invalid details(" + orderemail + "/"
					+ ordernumber + ") in 'Sign In' page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Error Message should be shown to block user from Login.");

			Log.assertThat(
					signinPage
							.getInvalidCheckOrderErrorMsg()
							.equals("We're sorry. The order number and email combination you entered could not be found. Please double-check the number and email address and enter them again."),
					"<b>Actual Result:</b> We're sorry. The order number and email combination you entered could not be found. Please double-check the number and email address and enter them again.",
					"Actual Result is not matched with the expected results",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_010

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Breadcrumb in My Account Landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_014(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> breadcrumbText = new ArrayList<String>();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			breadcrumbText = myAccountPage.getTextInBreadcrumb();

			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed 'Home > My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Home")
								&& breadcrumbText.get(1).equals("My Account"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Home > My Account'",
						"<b>Actual Result:</b> The breadcrumb is not displayed as expected - 'Home > My Account' and the actual is : '"
								+ breadcrumbText.get(0)
								+ " > "
								+ breadcrumbText.get(1) + "'", driver);
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed 'Back to Home'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Back to Home"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Back to Home'",
						"<b>Actual Result:</b>  The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ breadcrumbText.get(0) + "'", driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_014

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Breadcrumbs displayed Profile details page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_019(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> breadcrumbText = new ArrayList<String>();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Login into and navigated to 'MyAccountPage' Page!");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++)
					+ ". Clicked Profile link and navigated to 'ProfilePage' Page!");
			breadcrumbText = profilePage.getTextInBreadcrumb();
			String accountHeader = profilePage.getTextFromAccountHeader();

			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed 'Home > My Account > Edit Profile'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Home")
								&& breadcrumbText.get(1).equals("My Account")
								&& accountHeader.equals("Edit Profile"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Home > My Account > Edit Profile'",
						"<b>Actual Result:</b> The breadcrumb is not displayed as expected - 'Home > My Account > Edit Profile' and the actual is : '"
								+ breadcrumbText.get(0)
								+ " > "
								+ breadcrumbText.get(1) + "'");
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed 'Back to My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Back to My Account"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Back to My Account'",
						"<b>Actual Result:</b> The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ breadcrumbText.get(0) + "'");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_019

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify resource message while entering invalid email address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_024(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String invalidemailid = testData.get("InvalidEmailId");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Login into and navigated to 'MyAccountPage' Page!");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++)
					+ ". Clicked Profile link and navigated to 'ProfilePage' Page!");

			profilePage.typeOnEmailField(invalidemailid);
			Log.message((i++)
					+ ". Entered invalid Email address in Profile page");

			profilePage.clickOnProfileApplyChanges();
			Log.message((i++)
					+ ". Clicked on the Profile Apply changes button in Profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'We're sorry, but this email address is not in the proper format: (EXAMPLE: email_user@email.com). Please re-enter your email address.' error message should displayed");

			Log.assertThat(
					profilePage
							.getInvalidEmailAddressErrorMessage()
							.equals("We're sorry, but this email address is not in the proper format (EXAMPLE: email_user@email.com). Please re-enter your email address."),
					"<b>Actual Result:</b> Error message 'We're sorry, but this email address is not in the proper format: (EXAMPLE: email_user@email.com). Please re-enter your email address.' is displayed on updating invalid email address ",
					"<b>Actual Result:</b> Required Error message 'We're sorry, but this email address is not in the proper format: (EXAMPLE: email_user@email.com). Please re-enter your email address.' is not displayed on updated invalid email address",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_024

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Add New Address button in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_061(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> NewAddress = null;
		NewAddress = Arrays.asList("txtAddressName", "txtFirstName",
				"txtLastName", "txtAddress1", "txtAddress2", "txtCity",
				"txtSelectedState", "txtZipcode", "txtPhone");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address Button!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The respective fields should displayed in the 'Add new address' form");

			Log.assertThat(
					addressbook.elementLayer.verifyPageElements(NewAddress,
							addressbook),
					"<b>Actual Result:</b> Fields in the Add New Address form is displayed ",
					"<b>Actual Result:</b> Fields in the Add New Address form not displayed as expected, plz check the event log",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Default Shipping and Billing checkboxes should displayed");
			Log.assertThat(
					addressbook
							.verifyDefaultShippingAndBillingAddressInAddNewAddress(),
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are displayed",
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are not displayed as expected, plz check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_061

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Address Details in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_071(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String addresstext = testData.get("Address");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The address should displaed correctly");

			Log.assertThat(
					addressbook.getTextFromAdditionalAddresses(0).contains(
							addresstext),
					"<b>Actual Result:</b> The Address is displayed correctly",
					"<b>Actual Result:</b> The Address is not display as expected",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Check boxes in the Address label should displayed");
			Log.assertThat(
					addressbook.elementLayer
							.verifyPageListElements(Arrays.asList(
									"lnkEditAddress", "lnkDeleteAddress"),
									addressbook),
					"<b>Actual Result:</b> Check boxes in the Address label is displayed ",
					"<b>Actual Result:</b> Check boxes in the Address label not displayed as expected, plz check the event log",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Default Shipping and Billing check boxes should displayed");
			Log.assertThat(
					addressbook
							.verifyDefaultShippingAndBillingAddressInSavedAddress(0),
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are displayed",
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are not displayed as expected, plz check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_071

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Set as Default Radio Buttons in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_064(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Default Shipping and Billing check boxes should displayed for first address in AddressBook");

			Log.assertThat(
					addressbook
							.verifyDefaultShippingAndBillingAddressInSavedAddress(0),
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are displayed for first address in AddressBook",
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are not displayed for first address in AddressBook as expected, plz check the event log",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Default Shipping and Billing check boxes should displayed for second address in AddressBook");
			Log.assertThat(
					addressbook
							.verifyDefaultShippingAndBillingAddressInSavedAddress(1),
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are displayed for second address in AddressBook",
					"<b>Actual Result:</b> The Default Shipping and Billing check boxes are not displayed for second address in AddressBook as expected, plz check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_064

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Left Navigation links in My Account Landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_015(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> navLinks = null;
		if (runPltfrm == "desktop") {
			navLinks = Arrays.asList("lnkProfile", "lnkOrderHistory",
					"lnkAddressBook", "lnkPaymentMethods", "lnkRegistryLeftNav",
					"lnkWishListLeftNav", "lnkBelkRewardsCreditCard",
					"lnkEmailPreferences");
		} else if (runPltfrm == "mobile") {
			navLinks = Arrays.asList("lnkProfileMobile",
					"lnkOrderHistoryMobile", "lnkAddressBookMobile",
					"lnkPaymentMethodsMobile", "lnkRegistryMobile",
					"lnkWishListMobile");
		}

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			Log.message("<br><b>Expected Result:</b> Navigation Links should be displayed after Log In in My Account Page.");
			
			Log.assertThat(
					myAccountPage.elementLayer.verifyPageElements(navLinks,
							myAccountPage),
					"<b>Actual Result:</b> Navigation Links displayed after Log In in My Account Page.",
					"<b>Actual Result:</b> Navigation Links are not displayed after Log In in My Account Page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_015

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section of the site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_030(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			String isAddToEmailListChecked = editProfilePage
					.isAddToEmailListChecked();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'AddToEmailList' checkbox should checked.");

			Log.assertThat(
					isAddToEmailListChecked.equals("Yes"),
					"<b>Actual Result:</b> 'AddToEmailList' checkbox is checked.",
					"AddToEmailList checkbox is not checked.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_030

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Header section of the site", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_033(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i = 1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			// edit edit profile fields
			editProfilePage.typeOnFirstNameField("UpdatedFirstName");
			editProfilePage.typeOnLastNameField("UpdatedLastName");
			editProfilePage.typeOnPhoneField("2589631470");
			editProfilePage.typeOnEmailField("UpdatedEmail@yopmail.com");
			editProfilePage.typeOnConfirmEmailField("UpdatedEmail@yopmail.com");
			editProfilePage.typeOnPasswordField("UpdatedPassword");
			Log.message((i++) + ". Updated values of edit profile fields.",
					driver);

			// click on profile cancel button
			editProfilePage.clickOnCancel();
			Log.message((i++) + ". Clicked on cancel profile button.");

			// navigate to profile page
			editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++)
					+ ". Navigated to Edit Profile page after updating edit profile fields");

			// verify edit profile fields are not updated
			String firstNameField = editProfilePage.getFirstNameFieldValue();
			String lastNameField = editProfilePage.getLastNameFieldValue();
			String emailField = editProfilePage.getEmailFieldValue();
			String phoneField = editProfilePage.getPhoneFieldValue();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The fields values should not be changed");
			Log.softAssertThat(
					(!(firstNameField.equals("UpdatedFirstName"))),
					"<b>Actual Result:</b> First name field value is not changed.",
					"<b>Actual Result:</b> First name field value is changed.");
			Log.softAssertThat(
					(!(lastNameField.equals("UpdatedFirstName"))),
					"<b>Actual Result:</b> Last name field value is not changed.",
					"<b>Actual Result:</b> Last name field value is changed.");
			Log.softAssertThat(
					(!(emailField.equals("UpdatedEmail@yopmail.com"))),
					"<b>Actual Result:</b> Email field value is not changed.",
					"<b>Actual Result:</b> Email field value is changed.");
			Log.softAssertThat((!(phoneField.equals("2589631470"))),
					"<b>Actual Result:</b> Phone field value is not changed.",
					"<b>Actual Result:</b> Phone field value is changed.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_033

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			List<String> txtInBreadCrumb = myaccount.getTextInBreadcrumb();

			if (Utils.getRunPlatForm() == "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed like 'Back to Home'");
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Back to Home'",
						"<b>Actual Result:</b> The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'", driver);
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The breadcrumb should displayed like 'Home > My Account'");
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals("My Account"),
						"<b>Actual Result:</b> The breadcrumb is displayed as expected - 'Home > My Account'",
						"<b>Actual Result:</b> The breadcrumb is not displayed as expected - 'Home > My Account' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_005

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Breadcrumb in My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> element = Arrays.asList("passwordRecoveryPage");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SignInPage = homePage.headers.navigateToSignIn();

			SignInPage.clikForgotPwd();

			Log.message((i++)
					+ ". Clicked on Forgot password link in SignIn page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'Password Recovery' page should be displayed, when User Click on the 'Forgot password' link.");
			Log.assertThat(
					SignInPage.elementLayer.verifyPageElements(element,
							SignInPage),
					"<b>Actual Result:</b> 'Password Recovery' page  displayed, when User Click on the 'Forgot password' link.",
					"<b>Actual Result:</b> 'Password Recovery' page not displayed, when User Click on the 'Forgot password' link.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_007

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the content in My Account Landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_018(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Update and manage your profile.' content should be displayed below 'Profile' section");

			Log.assertThat(
					myAccountPage.getContentInAccountLanding("Profile").equals(
							"Profile\nUpdate and manage your profile."),
					"<b>Actual Result:</b> The 'Update and manage your profile.' content is displayed below 'Profile' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Profile' section",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Track your orders and view your order history.' content should displayed below 'Order History' section");
			Log.assertThat(
					myAccountPage
							.getContentInAccountLanding("Order History")
							.equals("Order History\nTrack your orders and view your order history."),
					"<b>Actual Result:</b> The 'Track your orders and view your order history.' content is displayed below 'Order History' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Order History' section",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Add, update or manage your addresses.' content should be displayed below 'Address Book' section");
			Log.assertThat(
					myAccountPage
							.getContentInAccountLanding("Address Book")
							.equals("Address Book\nAdd, update or manage your addresses."),
					"<b>Actual Result:</b> The 'Add, update or manage your addresses.' content is displayed below 'Address Book' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Address Book' section",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Add, delete, or manage your payment methods.' content should be displayed below 'Payment Methods' section");
			Log.assertThat(
					myAccountPage
							.getContentInAccountLanding("Payment Methods")
							.equals("Payment Methods\nAdd, delete, or manage your payment methods."),
					"<b>Actual Result:</b> The 'Add, delete, or manage your payment methods.' content is displayed below 'Payment Methods' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Payment Methods' section",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Locate a registry or view and manage your own.' content should be displayed below 'Registry' section");
			Log.assertThat(
					myAccountPage
							.getContentInAccountLanding("Registry")
							.equals("Registry\nLocate a registry or view and manage your own."),
					"<b>Actual Result:</b> The 'Locate a registry or view and manage your own.' content is displayed below 'Registry' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Registry' section",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Locate a wish list or view and manage your own.' content should be displayed below 'Wish List' section");
			Log.assertThat(
					myAccountPage
							.getContentInAccountLanding("Wish List")
							.equals("Wish List\nLocate a wish list or view and manage your own."),
					"<b>Actual Result:</b> The 'Locate a wish list or view and manage your own.' content is displayed below 'Wish List' section",
					"<b>Actual Result:</b> Expected content is not dispalyed for 'Wish List' section",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_018

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Express Checkout option when Default addresses and payment method are available", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_035(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressLine1 = testData.get("AddressLine1");
		String shippingAddressLine1 = testData.get("ShippingAddressLine1");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipcode = testData.get("ZipCode");
		String phoneNumber = testData.get("PhoneNumber");
		String cardType = testData.get("CardType");
		String cardOwner = testData.get("CardOwner");
		String cardNumber = testData.get("CardNumber");
		cardNumber = cardNumber.replace("411111111111", "************");
		String cardExpireDate = testData.get("CardExpireDate");

		LinkedHashMap<String, String> defaultBillingAddress = null;
		LinkedHashMap<String, String> defaultShippingAddress = null;
		LinkedHashMap<String, String> defaultPaymentOptions = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigate to the Profile page", driver);

			defaultBillingAddress = profilePage.getDefaultBillingAddress();
			defaultShippingAddress = profilePage.getDefaultShippingAddress();
			defaultPaymentOptions = profilePage.getDefaultPaymentOption();

			// Verify the default billing address
			Log.message("<b>Expected Result: </b>Default Billing Address title should be 'Billing'");
			Log.softAssertThat(
					defaultBillingAddress.get("title").equals("Billing"),
					"<b>Actual Result: </b>Default Billing Address title is displayed as 'Billing'",
					"<b>Actual Result: </b>Default Billing Address title mismatched",
					driver);
			Log.message("<b>Expected Result: </b>Default Billing Address should be same as given");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(
					(defaultBillingAddress.get("firstname") + " " + defaultBillingAddress
							.get("lastname"))
							.equals(firstName + " " + lastName),
					"1. Default Billing Address name is displayed as "
							+ firstName + " " + lastName,
					"1. Default Billing Address name mismatched", driver);
			Log.softAssertThat(
					defaultBillingAddress.get("address1").equals(addressLine1),
					"2. Default Billing Address line 1 is displayed as "
							+ addressLine1,
					"2. Default Billing Address Line1 Mismatched", driver);
			Log.softAssertThat(defaultBillingAddress.get("city").equals(city),
					"3. Default Billing Address city is displayed as " + city,
					"3. Default Billing Address city Mismatched", driver);
			Log.softAssertThat(
					defaultBillingAddress.get("state").equals(
							StateUtils.getStateName(state)),
					"4. Default Billing Address state is displayed as " + state,
					"4. Default Billing Address state Mismatched", driver);
			Log.softAssertThat(
					defaultBillingAddress.get("zipcode").equals(zipcode),
					"5. Default Billing Address zipcode is displayed as "
							+ zipcode,
					"5. Default Billing Address zipcode Mismatched", driver);
			Log.softAssertThat(
					defaultBillingAddress.get("phonenumber")
							.equals(phoneNumber),
					"6. Default Billing Address Phone number is displayed as "
							+ phoneNumber,
					"6. Default Billing Address phonenumber Mismatched", driver);

			// Verify the default shipping address
			Log.message("<b>Expected Result: </b>Default Shipping Address title should be 'Shipping'");
			Log.softAssertThat(
					defaultShippingAddress.get("title").equals("Shipping"),
					"<b>Actual Result: </b>Default Shipping Address title is displayed as 'Shipping'",
					"<b>Actual Result: </b>Default Shipping Address Title Mismatched",
					driver);
			Log.message("<b>Expected Result: </b>Default Shipping Address should be same as given");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(
					defaultShippingAddress.get("name").equals(
							firstName + " " + lastName),
					"1. Default Shipping Address title is displayed as "
							+ firstName + " " + lastName,
					"1. Default Shipping Address Name Mismatched", driver);
			Log.softAssertThat(
					defaultShippingAddress.get("address").equals(
							shippingAddressLine1),
					"2. Default Shipping Address Line1 is displayed as "
							+ shippingAddressLine1,
					"2. Default Shipping Address Line1 Mismatched", driver);
			Log.softAssertThat(defaultShippingAddress.get("city").equals(city),
					"3. Default Shipping Address City is displayed as " + city,
					"3. Default Shipping Address City Mismatched", driver);
			Log.softAssertThat(defaultShippingAddress.get("state")
					.equals(state),
					"4. Default Shipping Address State is displayed as "
							+ state,
					"4. Default Shipping Address State Mismatched", driver);
			Log.softAssertThat(
					defaultShippingAddress.get("zipcode").equals(zipcode),
					"5. Default Shipping Address Zipcode is displayed as "
							+ zipcode,
					"5. Default Shipping Address Zipcode Mismatched", driver);
			Log.softAssertThat(defaultShippingAddress.get("phonenumber")
					.equals(phoneNumber),
					"6. Default Shipping Address phone number is displayed as "
							+ phoneNumber,
					"6. Default Shipping Address PhoneNumber Mismatched",
					driver);

			// Verify the default payment options details
			Log.message("<b>Expected Result: </b>Default Payment option Card Type should be same as given");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(
					defaultPaymentOptions.get("cardtype").equals(cardType),
					"1. Default Payment option Card Type is displayed as "
							+ cardType,
					"1. Default Payment option Card Type Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardowner").equals(cardOwner),
					"2. Default Payment option Card Owner is displayed as "
							+ cardOwner,
					"2. Default Payment option Card Owner Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardnumber").equals(cardNumber),
					"3. Default Payment option Card Number is displayed as "
							+ cardNumber,
					"3. Default Payment option Card Number Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardexpire").equals(
							cardExpireDate),
					"4. Default Payment option Card ExpireDate is displayed as "
							+ cardExpireDate,
					"4. Default Payment option Card ExpireDate Mismatched",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_035

	@Test(groups = { "desktop" }, description = "Verify Left Navigation in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_074(String browser) throws Exception {

		if(Utils.getRunPlatForm()=="mobile"){
			throw new SkipException("This testcase (Left Navigation verification) is not applicable for Mobile");
		}
		
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> NavList = null;
		NavList = Arrays.asList("lnkMyAccount", "lnkProfile",
				"lnkOrderHistory", "lnkAddressBook", "lnkPaymentMethods",
				"lnkRegistry", "lnkFAQ", "lnkWishList",
				"lnkBelkRewardsCreditCard", "lnkEmailPreferences");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Left Navigation links should displayed under My Account section");
			Log.assertThat(
					addressbook.elementLayer.verifyPageElements(NavList,
							addressbook),
					"<b>Actual Result: </b>The Left Navigation links are displayed under My Account section",
					"<b>Actual Result: </b>The Left Navigation links are not displayed under My Account section as expected",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_074

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Save Changes in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_072(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.setDefaultShippingAddressByIndex(0);
			Log.message((i++) + ". Checked Default Shipping Address!");

			addressbook.setDefaultBillingAddressByIndex(0);
			Log.message((i++) + ". Checked Default Billing Address!");

			addressbook.clickSaveChanges();
			Log.message((i++) + ". Clicked on Save Changes!");

			Log.message("<b>Expected Result: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressbook.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Shipping Address is checked",
					"<b>Actual Result: </b>The Default Shipping Address is not checked as expected, plz check the event log");
			Log.message("<b>Expected Result: </b>The Default Billing Address should be checked");
			Log.assertThat(
					addressbook.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Billing Address is checked",
					"<b>Actual Result: </b>The Default Billing Address is not checked as expected, plz check the event log");

			addressbook.clickOnEditAddressByIndex(1);

			addressbook.unCheckDefaultShippingAddressInAddNewAddress();

			addressbook.unCheckDefaultBillingAddressInAddNewAddress();

			addressbook.clickOnSaveInAddNewAddressModalAfterEdit();
			Log.message((i++) + ". CleanedUp.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_072

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Set Default Billing Address option", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_041(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int j = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((j++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(j++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((j++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((j++) + ". Navigated to Edit Profile page", driver);

			if (editProfilePage.verifyDefaultPaneAvailabilityByName("billing")) {
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
				}
				AddressBookPage addressbookpage = (AddressBookPage) editProfilePage
						.navigateToSection("addressbook");
				Log.message((j++) + ".Address book page is displayed");
				int addressCount = addressbookpage.getTotalAddressCount();
				for (int i = 1; i <= addressCount; i++)
					addressbookpage.deleteAddressByIndex(0);

				addressbookpage.clickAddNewAddress();
				addressbookpage.fillingAddNewAddressDetails("valid_address6",
						"NO", "NO");
				addressbookpage.ClickOnSave();
				addressbookpage.selectUseOriginalAddress();
				addressbookpage.ClickOnContinueInValidationPopUp();
				Log.message((j++) + ".Added new address");
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
				}
				editProfilePage = (ProfilePage) addressbookpage
						.navigateToSection("profile");
				Log.message((j++) + ".Navigated to Edit profile page");
			} else {
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
				}
				AddressBookPage addressbookpage = (AddressBookPage) editProfilePage
						.navigateToSection("addressbook");
				int addressCount = addressbookpage.getTotalAddressCount();
				for (int i = 1; i <= addressCount; i++)
					addressbookpage.deleteAddressByIndex(0);
				Log.message((j++) + ".Address book page is displayed");
				addressbookpage.clickAddNewAddress();
				addressbookpage.fillingAddNewAddressDetails("valid_address6",
						"NO", "NO");
				addressbookpage.ClickOnSave();
				addressbookpage.selectUseOriginalAddress();
				addressbookpage.ClickOnContinueInValidationPopUp();
				Log.message((j++) + ". Added new address");
				BrowserActions.nap(3);
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
				}
				editProfilePage = (ProfilePage) addressbookpage
						.navigateToSection("profile");
				Log.message((j++) + ".Navigated to Edit profile page");
			}

			// click on changeDefaultInBilling link
			editProfilePage.clickOnSetAsDefaultBilling();
			Utils.waitForPageLoad(driver, 4);
			Log.message((j++) + ". Clicked on Set As Default Billing.");

			// select the saved address
			editProfilePage.selectSavedAddressByIndex(1);
			Log.message((j++)
					+ ". Selected existing address from Saved Address dropdown.");

			// Saved Address for Set Default Billing Dialog
			LinkedHashMap<String, String> defaultBillingValues = new LinkedHashMap<String, String>();
			defaultBillingValues.put("address1", "77 Green Acres Rd");
			defaultBillingValues.put("address2", "a");
			defaultBillingValues.put("city", "Valley Stream");
			defaultBillingValues.put("state", "New York");
			defaultBillingValues.put("zipcode", "11581");
			defaultBillingValues.put("phonenumber", "9293972740");
			Log.message((j++)
					+ ". Existing address for Set Default Billing is set.");

			// get the set default billing dialog container values
			LinkedHashMap<String, String> setDefaultBillingValues = editProfilePage
					.getSetDefaultBillingAddressDialogContainer();
			Log.message((j++) + ". Get Default Billing Address Values.");

			// compare two LinkedHashmaps Values(compare existing address with
			// selected address)
			Log.message("<b>Expected Result: </b>The Default Billing Address should be displayed as existing one");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(
					setDefaultBillingValues.get("address1").contains(
							defaultBillingValues.get("address1")),
					"1. address1(Set Default Billing Address) Field is same as Existing address1.",
					"1. address1(Set Default Billing Address) Field is not same as Existing address1.");

			Log.softAssertThat(
					setDefaultBillingValues.get("city").contains(
							defaultBillingValues.get("city")),
					"2. city(Set Default Billing Address) Field is same as Existing city.",
					"2. city(Set Default Billing Address) Field is not same as Existing city.");

			Log.softAssertThat(
					setDefaultBillingValues.get("state").contains(
							defaultBillingValues.get("state")),
					"3. state(Set Default Billing Address) Field is same as Existing state.",
					"3. state(Set Default Billing Address) Field is not same as Existing state.");

			Log.softAssertThat(
					setDefaultBillingValues.get("zipcode").contains(
							defaultBillingValues.get("zipcode")),
					"4. zipcode(Set Default Billing Address) Field is same as Existing zipcode.",
					"4. zipcode(Set Default Billing Address) Field is not same as Existing zipcode.");

			Log.softAssertThat(
					setDefaultBillingValues.get("phonenumber").contains(
							defaultBillingValues.get("phonenumber")),
					"5. phonenumber(Set Default Billing Address) Field is same as Existing phonenumber.",
					"5. phonenumber(Set Default Billing Address) Field is not same as Existing phonenumber.");

			// click 'Set as Default' button
			editProfilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((j++) + ". Clicked on 'Set as Default' button");
			editProfilePage.selectUseOriginalAddress();
			editProfilePage.clickContinueAddressSuggestionInAddressValidation();
			Log.message("<b>Expected Result: </b>After clicking 'Set as Default' button in Default billing dialog, the Default Billing Address should be saved");
			Log.softAssertThat(
					editProfilePage.elementLayer.verifyPageElements(
							Arrays.asList("defaultBillingAddressPane"),
							editProfilePage),
					"<b>Actual Result: </b>After clicking 'Set as Default' button in Default billing dialog, the default billing address is saved.",
					"<b>Actual Result: </b>After clicking 'Set as Default' button in Default billing dialog, the default billing address is not saved.",
					driver);

			// verify selected address is set as deault address
			LinkedHashMap<String, String> displyedDefaultBillingValues = editProfilePage
					.getDefaultBillingAddress();
			Log.message("<b>Expected Result: </b>The Default Billing Address should be displayed as given");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(
					displyedDefaultBillingValues.get("address1").contains(
							defaultBillingValues.get("address1")),
					"1. address(Set Default Billing Address) Field is same as Default address.",
					"1. address(Set Default Billing Address) Field is not same as Default address.");
			Log.softAssertThat(
					displyedDefaultBillingValues.get("city").equals(
							defaultBillingValues.get("city")),
					"2. city(Set Default Billing Address) Field is same as Default city.",
					"2. city(Set Default Billing Address) Field is not same as Default city.");

			Log.softAssertThat(
					defaultBillingValues
							.get("state")
							.toLowerCase()
							.contains(
									displyedDefaultBillingValues.get("state")
											.toLowerCase()),
					"3. state(Set Default Billing Address) Field is same as Default state.",
					"3. state(Set Default Billing Address) Field is not same as Default state.");

			Log.softAssertThat(
					displyedDefaultBillingValues.get("zipcode").contains(
							defaultBillingValues.get("zipcode")),
					"4. zipcode(Set Default Billing Address) Field is same as Default zipcode.",
					"4. zipcode(Set Default Billing Address) Field is not same as Default zipcode.");

			Log.softAssertThat(
					displyedDefaultBillingValues.get("phonenumber").contains(
							defaultBillingValues.get("phonenumber")),
					"5. phonenumber(Set Default Billing Address) Field is same as Default phonenumber.",
					"5. phonenumber(Set Default Billing Address) Field is not same as Default phonenumber.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_041

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Breadcrumb in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_060(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			List<String> txtInBreadCrumb = myaccount.getTextInBreadcrumb();
			if (runPltfrm == "desktop") {
				Log.message("<b>Expected Result: </b>The breadcrumb should be displayed as - 'Home > My Account > Address Book'");
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals("My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Home > My Account > Address Book'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Home > My Account > Address Book' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1)
								+ " > "
								+ txtInBreadCrumb.get(2) + "'");
			} else if (runPltfrm == "mobile") {
				Log.message("<b>Expected Result: </b>The breadcrumb should be displayed as - 'Back to My Account'");
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Back to My Account'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_060

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Default Addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_067(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			// user has existing addresses and verify in default address 'edit'
			// and 'delete'links are displayed
			Log.message("<b>Expected Result: </b>Edit & Delete link should be displayed for address - index(0)");
			Log.assertThat(
					addressbook.isEditAddressDisplayedByIndex(0),
					"<b>Actual Result: </b>Edit link is displayed for address - index(0)",
					"<b>Actual Result: </b>Edit link is not displayed for iaddress - ndex(0)");
			Log.assertThat(
					addressbook.isDeleteAddressDisplayedByIndex(0),
					"<b>Actual Result: </b>Delete link is displayed for address - index(0)",
					"<b>Actual Result: </b>Delete link is not displayed for address - index(0)");
			Log.message("<b>Expected Result: </b>Edit & Delete link should be displayed for address - index(0)");
			Log.assertThat(
					addressbook.isEditAddressDisplayedByIndex(1),
					"<b>Actual Result: </b>Edit link is displayed for address - index(1)",
					"<b>Actual Result: </b>Edit link is not displayed for address - index(1)");
			Log.assertThat(
					addressbook.isDeleteAddressDisplayedByIndex(1),
					"<b>Actual Result: </b>Delete link is displayed for address - index(1)",
					"<b>Actual Result: </b>Delete link is not displayed for address - index(1)");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_067

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting the Employee checkbox in Edit Profile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_050(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> EmployeeDetails = null;
		if (runPltfrm == "desktop") {
			EmployeeDetails = Arrays.asList("txtEmployeeID", "txtStoreID");
		} else if (runPltfrm == "mobile") {
			EmployeeDetails = Arrays.asList("txtEmployeeID", "txtStoreID");

		}

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile Page", driver);

			profilePage.checkIAmBelkEmployee();
			Log.message((i++) + ". Selected Check Box 'I am Belk Employee'",
					driver);
			Log.message("<b>Expected Result: </b>Employee Details should be displayed after Selecting 'I am Belk Employee' in Profile Page.");
			Log.assertThat(
					profilePage.elementLayer.verifyPageElements(
							EmployeeDetails, profilePage),
					"<b>Actual Result: </b>Employee Details are displayed after Selecting 'I am Belk Employee' in Profile Page.",
					"<b>Actual Result: </b>Employee Details are not displayed after Selecting 'I am Belk Employee' in Profile Page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_050

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Edit Address window in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_082(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked Edit Address Link", driver);

			LinkedHashMap<String, String> expectedAddress = new LinkedHashMap<String, String>();

			expectedAddress.put("addressName", addressName);
			expectedAddress.put("firstname", firstName);
			expectedAddress.put("lastname", lastName);
			expectedAddress.put("address1", addressLine1);
			expectedAddress.put("address2", addressLine2);
			expectedAddress.put("city", city);
			expectedAddress.put("state", state);
			expectedAddress.put("zipcode", zipCode);
			expectedAddress.put("phone", phone);
			expectedAddress.put("formTitle", "Edit Address");

			LinkedHashMap<String, String> actualAddress = addressBookPage
					.getAddressDetailsFromAddAddressSection();

			actualAddress.put("formTitle", addressBookPage.getFormTitle()
					.trim());
			Log.message("<b>Expected Result: </b>The value in Edit Address Form should be displayed as given");
			Log.assertThat(
					expectedAddress.equals(actualAddress),
					"<b>Actual Result: </b>The value in Edit Address Form is same as expected",
					"<b>Actual Result: </b>The value in Edit Address Form is not same as expected",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_082

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding duplicate address with unique nick name", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_083(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save Address", driver);
			Log.message("<b>Expected Result: </b>Invalid error message: 'This field is required.' should be shown when fields are empty.");

			Log.assertThat(
					addressBookPage.getAddEditAddressFormErrorMsg().contains(
							"This field is required."),
					"<b>Actual Result:</b> Invalid error message: 'This field is required.' is shown when fields are empty.",
					"<b>Actual Result:</b> No Invalid error message is shown when fields are empty.",
					driver);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled New Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message(
					(i++) + ". Clicked Save button to close all Error Msgs",
					driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Selected Use Original Address", driver);

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++)
					+ ". Clicked 'Continue' button on Address Suggestion",
					driver);

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(1);

			List<String> index = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", index.get(0));
			newAddress.put("FirstName", index.get(1));
			newAddress.put("LastName", index.get(2));
			newAddress.put("Address1", index.get(3));
			newAddress.put("Address2", index.get(4));
			newAddress.put("City", index.get(5));
			newAddress.put("State", index.get(6));
			newAddress.put("Zipcode", index.get(7));
			newAddress.put("PhoneNo", index.get(8));
			newAddress.put("Defaultshipping", index.get(9));
			newAddress.put("Defaultbilling", index.get(10));

			Log.message("<b>Expected Result: </b>Address saved should be same as given");
			Log.assertThat(
					savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.",
					driver);

			addressBookPage.deleteAddressByIndex(1);
			Log.message((i++)
					+ ". Test Data Cleanup - Deleted the Added Address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_083

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Save and Cancel button in the Edit and New Address modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_087(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled in Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Selected Use Original Address", driver);

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++)
					+ ". Clicked Continue button on Address Suggestion", driver);

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			List<String> index = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", index.get(0));
			newAddress.put("FirstName", index.get(1));
			newAddress.put("LastName", index.get(2));
			newAddress.put("Address1", index.get(3));
			newAddress.put("Address2", index.get(4));
			newAddress.put("City", index.get(5));
			newAddress.put("State", index.get(6));
			newAddress.put("Zipcode", index.get(7));
			newAddress.put("PhoneNo", index.get(8));
			newAddress.put("Defaultshipping", index.get(9));
			newAddress.put("Defaultbilling", index.get(10));
			Log.message("<b>Expected Result: </b>Address saved should be saved to address book same as given");
			Log.assertThat(
					savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.",
					driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			addressBookPage.clickCancelAddressAddEditForm();
			Log.message(
					(i++)
							+ ". Clicked 'Cancel' button to close the Add Address Form",
					driver);
			Log.message("<b>Expected Result: </b>Address saved should not be saved to address book");
			Log.assertThat(
					addressBookPage.getSavedAddress().size() == 1,
					"<b>Actual Result: </b>The Address is not saved to the Address book on Clicking Cancel.",
					"<b>Actual Result: </b>The Address is saved to the Address book on Clicking Cancel.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++)
					+ ". Test Data Celanup - Deleted the Added Address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_087

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify welcome message and logout links", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_017(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String FirstName = testData.get("FirstName");
		String LastName = testData.get("LastName");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SignInPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page!");
			MyAccountPage myaccount = SignInPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Signed In to MyAccount with username/password("
					+ emailid + "/" + password + ")");
			String txtInBreadCrumb = myaccount.getLogoutext();
			String txtInUsername = myaccount.getUsername();
			String Username = FirstName + " " + LastName;
			Log.message("<b>Expected Result: </b>The Welcome message with the customer First and Last name("
					+ Username
					+ ") and the 'Logout' links should be displayed in the my account landing page.");
			Log.assertThat(
					txtInBreadCrumb.equals("Logout")
							&& txtInUsername.equals(Username),
					"<b>Actual Result: </b>The Welcome message with the customer First and Last name("
							+ Username
							+ ") and the 'Logout' links is displayed in the my account landing page.",
					"<b>Actual Result: </b>The Welcome message with the customer First and Last name("
							+ Username
							+ ") and the 'Logout' links is not displayed in the my account landing page.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_017

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Employee checkbox in Edit Profile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_049(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Profile Page", driver);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("window.scrollTo(0, document.body.scrollHeight/4)");
			Log.message("<b>Expected Result: </b>'I'm Belk Employee' checkbox should be displayed");
			Log.assertThat(
					profilePage.elementLayer.verifyPageElements(
							Arrays.asList("chkIAmBelkEmployeeSpan"),
							profilePage),
					"<b>Actual Result: </b>I'm Belk Employee checkbox is displayed",
					"<b>Actual Result: </b>I'm Belk employee check box is not displayed",
					driver);
			Log.message("<b>Expected Result: </b>'Please enter your Belk associate ID number to get your associate discount when placing orders on belk.com.' Message should be displayed");
			Log.assertThat(
					profilePage
							.getImBelkEmployeeMessage()
							.contains(
									"Please enter your Belk associate ID number to get your associate discount when placing orders on belk.com."),
					"<b>Actual Result: </b>'Please enter your Belk associate ID number to get your associate discount when placing orders on belk.com.' Message should be displayed",
					"<b>Actual Result: </b>Required Message is not displayed under the I'm Belk employee check box");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_049

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Continue button after selecting the original address option in the Address Verification modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_093(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled New Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Selected Use Original Address", driver);

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++)
					+ ". Clicked Continue button on Address Suggestion", driver);

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			List<String> index = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", index.get(0));
			newAddress.put("FirstName", index.get(1));
			newAddress.put("LastName", index.get(2));
			newAddress.put("Address1", index.get(3));
			newAddress.put("Address2", index.get(4));
			newAddress.put("City", index.get(5));
			newAddress.put("State", index.get(6));
			newAddress.put("Zipcode", index.get(7));
			newAddress.put("PhoneNo", index.get(8));
			newAddress.put("Defaultshipping", index.get(9));
			newAddress.put("Defaultbilling", index.get(10));
			Log.message("<b>Expected Result: </b>Original Address should be saved to the Address book.");
			Log.assertThat(
					savedAddress.equals(newAddress),
					"<b>Actual Result: </b>Original Address is saved to the Address book.",
					"<b>Actual Result: </b>Original Address is not saved to the Address book.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message("11. Test Data Celanup - Deleted the Added Address",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_093

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify ​selecting same default shipping and billing addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_065(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.setDefaultShippingAddressByIndex(0);
			Log.message((i++) + ". Set Address as Default Shipping Address");

			addressBookPage.setDefaultBillingAddressByIndex(1);
			Log.message((i++) + ". Set Address as Default Billing Address");

			addressBookPage.clickSaveChanges();
			Log.message((i++) + ". Clicked on Save Changes!");

			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should be changed to Default Address");
			Log.assertThat(
					addressBookPage.getTotalAddressCount() == 2,
					"<b>Actual Result: </b>The Address is changed to Default Address",
					"<b>Actual Result: </b>The Address is not changed to Default Address",driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Shipping Address is checked",
					"<b>Actual Result: </b>The Default Shipping Address is not checked as expected, plz check the event log",driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Billing Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(1),
					"<b>Actual Result: </b>The Default Billing Address is checked",
					"<b>Actual Result: </b>The Default Billing Address is not checked as expected, plz check the event log",driver);

			Log.event("Test Data - Cleanedup");

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.event((i++) + ". Edited the Default Address");

			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();
			Log.event((i++)
					+ ". UnSelected Address as Default Shipping Address");

			addressBookPage.clickSaveAddressEditForm();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.event((i++) + ". Clicked Save button to Save the Address");

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.event((i++) + ". Edited the Default Address");

			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.event((i++)
					+ ". UnSelected Address as Default Billing Address");

			addressBookPage.clickSaveAddressEditForm();
			Log.event((i++) + ". Clicked Save button to Save the Address");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_065

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify ​selecting same default shipping and billing addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_066(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.setDefaultShippingAddressByIndex(0);
			Log.message((i++) + ". Set Address as Default Shipping Address",
					driver);

			addressBookPage.setDefaultBillingAddressByIndex(0);
			Log.message((i++) + ". Set Address as Default Billing Address",
					driver);

			addressBookPage.clickSaveChanges();
			Log.message((i++) + ". Clicked on Save Changes!");

			addressBookPage = new AddressBookPage(driver);
			Log.message("<b>Expected Result: </b>The Address should be changed to Default Address");
			Log.assertThat(
					addressBookPage.getTotalAddressCount() == 1
							&& (addressBookPage
									.isDefaultShippingAddressByIndexChecked(0) || addressBookPage
									.isDefaultBillingAddressByIndexChecked(0)),
					"8. The Address is changed to Default Address",
					"8. The Address is not changed to Default Address", driver);
			Log.message("<b>Expected Result: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultShippingAddressByIndexChecked(0),
					"9. The Default Shipping Address is checked",
					"9. The Default Shipping Address is not checked as expected, plz check the event log");
			Log.message("<b>Expected Result: </b>The Default Billing Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0),
					"10. The Default Billing Address is checked",
					"10. The Default Billing Address is not checked as expected, plz check the event log");

			Log.message("Test Data - Cleanup", driver);

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Edit the Default Address", driver);

			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();
			Log.message((i++)
					+ ". UnSelect Address as Default Shipping Address", driver);

			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message(
					(i++) + ". UnSelect Address as Default Billing Address",
					driver);

			addressBookPage.clickSaveAddressEditForm();
			Log.message((i++) + ". Click Save button to Save the Address",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_066

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify deleting the address from Address Book screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_111(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> deleteButtons = null;
		deleteButtons = Arrays.asList("btnConfirmDeleteAddress",
				"btnCancelDeleteAddress");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			if (addressBookPage.getTotalAddressCount() <= 0) {
				addressBookPage.clickAddNewAddress();
				addressBookPage.fillingAddNewAddressDetails("valid_address6",
						"No", "No");
				addressBookPage.ClickOnSave();
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.clickContinueAddressSuggestion();
				Log.message((i++) + ". Added New Address", driver);
			}

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked Delete Address", driver);
			Log.message("<b>Expected Result: </b>Delete and Cancel Button should be displayed in the Form");
			Log.assertThat(
					addressBookPage.elementLayer.verifyPageElements(
							deleteButtons, addressBookPage),
					"<b>Actual Result: </b>Delete and Cancel Button are displayed in the Form ",
					"<b>Actual Result: </b>Delete and Cancel Button are not displayed in the Form",
					driver);

			addressBookPage.clickCancelDeleteAddress();
			Log.message((i++) + ". Clicked Cancel Delete Address", driver);
			Log.message("<b>Expected Result: </b>The Address should not be Deleted/Removed");
			Log.assertThat(addressBookPage.getSavedAddress().size() == 1,
					"<b>Actual Result: </b>The Address is not Deleted/Removed",
					"<b>Actual Result: </b>The Address is Deleted/Removed");

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked Delete Address", driver);
			Log.message("<b>Expected Result: </b>Delete Address popup should be displayed");
			Log.assertThat(
					addressBookPage
							.getFormContent()
							.contains(
									"You're about to delete this address. Are you sure you want to proceed?"),
					"<b>Actual Result: </b>Delete Address popup is displayed",
					"<b>Actual Result: </b>Delete Address popup is not displayed");

			addressBookPage.clickConfirmDeleteAddress();
			Log.message((i++) + ". Clicked Confirm Delete Address", driver);

			addressBookPage = new AddressBookPage(driver);
			Log.message("<b>Expected Result: </b>The Address should be Deleted/Removed");
			Log.assertThat(addressBookPage.getSavedAddress().isEmpty(),
					"<b>Actual Result: </b>The Address is Deleted/Removed",
					"<b>Actual Result: </b>The Address is not Deleted/Removed");

			Log.message("Test Data - Setup", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_111

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Set Default Address screen while editing a billing address from Address Book", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_112(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i = 1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();

			int addressCount = addressBookPage.getTotalAddressCount();
			for (int j = 1; j <= addressCount; j++) {
				addressBookPage.deleteAddressByIndex(0);
			}

			Log.message((i++) + ". Navigated to Address Book Page");

			if (addressBookPage.getTotalAddressCount() <= 0) {
				addressBookPage.clickAddNewAddress();
				addressBookPage.fillingAddNewAddressDetails("valid_address2",
						"YES", "YES");
				addressBookPage.ClickOnSave();
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.clickContinueAddressSuggestion();
				addressBookPage.clickAddNewAddress();
				addressBookPage.fillingAddNewAddressDetails("valid_address5",
						"NO", "NO");
				addressBookPage.ClickOnSave();
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.clickContinueAddressSuggestion();
			}

			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0)
							&& addressBookPage
									.isDefaultShippingAddressByIndexChecked(0),
					(i++)
							+ ". Added addresses are properly displayed in tile format",
					(i++)
							+ ". Added addresses are not properly displayed in tile format",driver);

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked Delete Address");

			Log.message("<b>Expected Result: </b>'You're about to delete this address. Are you sure you want to proceed?' warning message should be displayed with Yes and Cancel button.");
			String warningMessage = addressBookPage.getFormContent();
			Log.assertThat(
					warningMessage
							.equals("You're about to delete this address. Are you sure you want to proceed?")
							&& addressBookPage.elementLayer.verifyPageElements(
									Arrays.asList("btnCancelDeleteAddress",
											"btnConfirmDeleteAddress"),
									addressBookPage),
					"<b>Actual Result: </b>Delete Address pop up is displayed with warning message: '"
							+ warningMessage + "' and 'Yes' / 'Cancel' button",
					"<b>Actual Result: </b>Delete Address pop up is not displayed with warning message: '"
							+ warningMessage + "' and 'Yes' / 'Cancel' button",driver);

			addressBookPage.clickCancelDeleteAddress();
			Log.message((i++) + ". Click Cancel Delete Address");

			Log.message("<b>Expected Result: </b>While clicking on Cancel button in Delete modal, the selected address should not get deleted from Address Book screen.");

			Log.assertThat(
					addressBookPage.getDefaultAddress().size() == 1,
					"<b>Actual Result: </b>While clicking on Cancel button in Delete modal, the selected address is not deleted from Address Book screen.",
					"<b>Actual Result: </b>While clicking on Cancel button in Delete modal, the selected address is deleted from Address Book screen.",driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++) + ". Delete Address");

			Log.message("<b>Expected Result: </b>While Clicking on 'Yes' button in Delete modal, the selected address should be properly deleted from Address Book screen");
			addressBookPage = new AddressBookPage(driver);

			Log.assertThat(
					addressBookPage.getTotalAddressCount() == 1,
					"<b>Actual Result: </b>While Clicking on 'Yes' button in Delete modal, the selected address is deleted from Address Book screen",
					"<b>Actual Result: </b>While Clicking on 'Yes' button in Delete modal, the selected address is not deleted from Address Book screen",driver);

			Log.message("<b>Expected Result: </b>The next shipping address present will become the default address.");
			Log.softAssertThat(
					addressBookPage.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The next shipping address present become the default address.",
					"<b>Actual Result: </b>The next shipping address present did not become the default address.",driver);

			Log.message("<b>Expected Result: </b>The next billing address present will become the default address.");
			Log.softAssertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The next billing address present become the default address.",
					"<b>Actual Result: </b>The next billing address present did not become the default address.",driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_112

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify to set the default address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_077(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String title = testData.get("Title");
		String firstname = testData.get("FirstName");
		String lastname = testData.get("LastName");
		String address1 = testData.get("AddressLine1");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipcode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultshipping = testData.get("DefaultShipping");
		String defaultbilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		LinkedHashMap<String, String> defaultAddress = new LinkedHashMap<String, String>();

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.setDefaultShippingAddressByIndex(1);
			Log.message((i++) + ". Checked Default Shipping Address!");

			addressbook.setDefaultBillingAddressByIndex(1);
			Log.message((i++) + ". Checked Default Billing Address!");

			addressbook.clickSaveChanges();
			Log.message((i++) + ". Clicked on Save Changes!");
			// *****************************************************************************
			defaultAddress = addressbook.getDefaultAddressByIndex(0);
			Log.message("<b>Expected Result: </b>The address should be saved as given");
			Log.message("<b>Actual Result: </b>");

			Log.assertThat(defaultAddress.get("firstname").equals(firstname),
					"2. The firstname is saved properly",
					"2. The firstname is not saved properly", driver);

			Log.assertThat(defaultAddress.get("lastname").equals(lastname),
					"3. The lastname is saved properly",
					"3. The lastname is not saved properly", driver);

			Log.assertThat(defaultAddress.get("address1").equals(address1),
					"4. The address1 is saved properly",
					"4. The address1 is not saved properly", driver);

			Log.assertThat(defaultAddress.get("city").equals(city),
					"5. The city is saved properly",
					"5. The city is not saved properly", driver);

			Log.assertThat(defaultAddress.get("state").equals(state),
					"6. The state is saved properly",
					"6. The state is not saved properly", driver);

			Log.assertThat(defaultAddress.get("zipcode").equals(zipcode),
					"7. The zipcode is saved properly",
					"7. The zipcode is not saved properly", driver);

			Log.assertThat(defaultAddress.get("phone").equals(phone),
					"8. The phone is saved properly",
					"8. The phone is not saved properly", driver);

			Log.assertThat(
					defaultAddress.get("defaultshipping").equals(
							defaultshipping),
					"9. The defaultshipping is saved properly",
					"9. The defaultshipping is not saved properly", driver);

			Log.assertThat(
					defaultAddress.get("defaultbilling").equals(defaultbilling),
					"10. The defaultbilling is saved properly",
					"10. The defaultbilling is not saved properly", driver);

			// ***********************************************************************

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");

			addressbook.unCheckDefaultShippingAddressInAddNewAddress();
			Log.message((i++) + ". UnChecked Default Shipping Address!");

			addressbook.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". UnChecked Default Billing Address!");

			addressbook.clickOnSaveInAddNewAddressModalAfterEdit();
			Log.message((i++)
					+ ". Clicked on Save in New Address Modal After Edit!");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_077

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on New and Edit Address link", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_084(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String title = testData.get("Title");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.clickAddNewAddress();
			Log.message((i++) + ". Clicked on Add New Address button!");
			Log.message("<b>Expected Result: </b>'Add New Address' title should be displayed");
			Log.assertThat(
					addressbook.getFormTitle().equals("Add New Address"),
					"<b>Actual Result: </b>The Add New Address title is displayed correctly",
					"<b>Actual Result: </b>The Add New Address title is not displayed as expected",
					driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();
			newAddress.put("addressName", "");
			newAddress.put("firstname", "");
			newAddress.put("lastname", "");
			newAddress.put("address1", "");
			newAddress.put("address2", "");
			newAddress.put("city", "");
			newAddress.put("state", "Select");
			newAddress.put("zipcode", "");
			newAddress.put("phone", "");
			Log.message("<b>Expected Result: </b>Empty fields should be displayed in Add new Address modal");
			Log.assertThat(
					addressbook.getAddressDetailsFromAddAddressSection()
							.equals(newAddress),
					"<b>Actual Result: </b>Empty fields are displayed in Add new Address modal",
					"<b>Actual Result: </b>The fields are not empty in the Add New Address modal",
					driver);

			addressbook.clickOnCancelInAddNewAddressModal();
			Log.message((i++) + ". Clicked on Cancel in Add New Address modal!");

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");
			Log.message("<b>Expected Result: </b>The Edit Address title: 'Edit Address' should be displayed");
			Log.assertThat(
					addressbook.getFormTitle().equals("Edit Address"),
					"<b>Actual Result: </b>The Edit Address title: 'Edit Address' is displayed correctly",
					"<b>Actual Result: </b>The Edit Address title is not displayed as expected",
					driver);

			LinkedHashMap<String, String> editAddress = new LinkedHashMap<String, String>();

			editAddress = addressbook.getAddressDetailsFromAddAddressSection();
			Log.message("<b>Expected Result: </b>The address details should be saved properly");
			Log.message("<b>Actual Result: </b>");
			Log.assertThat(editAddress.get("addressName").equals(title),
					"1. The Title is saved properly",
					"1. The Title is not saved properly", driver);

			Log.assertThat(editAddress.get("firstname").equals(firstName),
					"2. The firstname is saved properly",
					"2. The firstname is not saved properly", driver);

			Log.assertThat(editAddress.get("lastname").equals(lastName),
					"3. The lastname is saved properly",
					"3. The lastname is not saved properly", driver);

			Log.assertThat(editAddress.get("address1").equals(addressLine1),
					"4. The address1 is saved properly",
					"4. The address1 is not saved properly", driver);

			Log.assertThat(editAddress.get("address2").equals(addressLine2),
					"5. The address2 is saved properly",
					"5. The address2 is not saved properly", driver);

			Log.assertThat(editAddress.get("city").equals(city),
					"6. The city is saved properly",
					"6. The city is not saved properly", driver);

			Log.assertThat(editAddress.get("state").equals(state),
					"7. The state is saved properly",
					"7. The state is not saved properly", driver);

			Log.assertThat(editAddress.get("zipcode").equals(zipCode),
					"8. The zipcode is saved properly",
					"8. The zipcode is not saved properly", driver);

			Log.assertThat(editAddress.get("phone").equals(phone),
					"9. The phone is saved properly",
					"9. The phone is not saved properly", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_084

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Create Account button and Create Account Text is displayed", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_011(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> linksToBeVerified = null;
		linksToBeVerified = Arrays.asList("btnViewOrderDetails",
				"btnCreateAccount");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page from home page");

			// The TC specifies to verify if New Customers section is wrapped
			// below the Check Order Section.
			// Since the position of the buttons cannot be verified in script,
			// only checking if both the buttons are displayed as per Vijay's
			// comments.
			Log.message("<b>Expected Result: </b>The View Order Details and the Create Account buttons should be present in Signin page");
			if (runPltfrm == "mobile") {
				Log.assertThat(
						signinPage.elementLayer.verifyPageElements(
								linksToBeVerified, signinPage),
						"<b>Actual Result: </b>The View Order Details and the Create Account buttons are present in Signin page",
						"<b>Actual Result: </b>The View Order Details and the Create Account buttons are not present in Signin page as expected",
						driver);
			}

			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message((i++)
					+ ". Clicked on Create Account button in SignIn page");
			Log.message("<b>Expected Result: </b>The title: 'Create an Account' should be displayed correctly");
			Log.assertThat(
					createAccountPage.getTitle().equals("Create an Account"),
					"<b>Actual Result: </b>The title: 'Create an Account' is displayed correctly",
					"<b>Actual Result: </b>The title: 'Create an Account' is not displayed as expected",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the Express Checkout check box is disabled when there are no default shipping address, default billing address and default payment", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_034(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> elementsToBeVerified = null;
		elementsToBeVerified = Arrays.asList("lblExpressCheckoutDescription",
				"btnSetDefaultAddress", "btnSetDefaultBilling",
				"btnSetAsDefaultPayment");
		String description = "You must have a Default Shipping Address, Default Billing Address and Default Payment Method to select Express Checkout";
		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			ProfilePage profilepage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Address Book Page!");
			Log.message("<b>Expected Result: </b>The express checkout button should be disabled");
			Log.assertThat(
					!profilepage.isExpressCheckoutCheckboxEnabled(),
					"<b>Actual Result: </b>The express checkout button is disabled",
					"<b>Actual Result: </b>The express checkout button is not disabled. Check log",
					driver);
			Log.message("<b>Expected Result: </b>The buttons and the Checkout description should be displayed under Express Checkout section");
			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(
							elementsToBeVerified, profilepage),
					"<b>Actual Result: </b>The buttons and the Checkout description are displayed under Express Checkout section",
					"<b>Actual Result: </b>The buttons and the Checkout description are not displayed under Express Checkout section",
					driver);
			Log.message("<b>Expected Result: </b>The Checkout description: '"
					+ description
					+ "' should be displayed under Express Checkout section");
			Log.assertThat(
					profilepage.getExpressCheckOutDescription().equals(
							description),
					"<b>Actual Result: </b>The Checkout description: '"
							+ description
							+ "' is displayed under Express Checkout section",
					"<b>Actual Result: </b>The Checkout description is not displayed under Express Checkout section",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_034

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Employee form fields in Edit Profile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_051(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String employeeID = testData.get("Employee ID");
		String storeID = testData.get("Store ID");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> elementsToBeVerified = null;
		elementsToBeVerified = Arrays.asList("txtEmployeeID", "txtStoreID");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			ProfilePage profilepage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Address Book Page!");

			profilepage.checkIAmBelkEmployee();
			Log.message((i++) + ". Clicked I am Belk Employee!");
			Log.message("<b>Expected Result: </b>The Employee ID and Store ID should be displayed");
			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(
							elementsToBeVerified, profilepage),
					"<b>Actual Result: </b>The Employee ID and Store ID are displayed",
					"<b>Actual Result: </b>The Employee ID and Store ID are not displayed",
					driver);

			profilepage.typeOnEmployeeIdField(employeeID);
			Log.message((i++) + ". Typed value in to Employee Id field!");

			profilepage.typeOnStoreIdField(storeID);
			Log.message((i++) + ". Typed value in to Store Id field!");
			Log.message("<b>Expected Result: </b>The Employee ID should be displayed");
			Log.assertThat(
					profilepage.getEmployeeId().equals(employeeID),
					"<b>Actual Result: </b>The Employee ID is displayed correctly",
					"<b>Actual Result: </b>The Employee ID is not displayed correctly",
					driver);
			Log.message("<b>Expected Result: </b>The Store ID should be displayed");
			Log.assertThat(
					profilepage.getStoreId().equals(storeID),
					"<b>Actual Result: </b>The Store ID is displayed correctly",
					"<b>Actual Result: </b>The Store ID is not displayed correctly",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_051

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify warning message when customer username and password combination is invalid", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_159(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// SignIn SigninPage = homePage.headers.navigateToSignIn();
			// Log.message("2. Clicked SignIn button on header and navigated to 'SignIn' Page!",driver);
			signinPage.enterEmailID(emailid);
			signinPage.enterPassword(password);
			signinPage.clickBtnSignIn();
			Log.message((i++) + ". Entered Invalid credentials(" + emailid
					+ "/" + password + ") in 'Sign In' page", driver);

			Log.message("<b>Expected Result:</b> Error Message should be shown to block user from Login.");

			Log.assertThat(
					signinPage
							.getSignInFormErrorMsg()
							.equals("Sorry, this does not match our records. Check your spelling and try again."),
					"<b>Actual Result:</b> Invalid login error message is shown blocking user from login.",
					"<b>Actual Result:</b> No Login error message is shown to block user from login.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_159

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting “I am a Belk Employee” checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_029(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		List<String> element = Arrays.asList("txtEmployeeID", "txtStoreID");

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SignInPage = homePage.headers.navigateToSignIn();

			MyAccountPage myaccount = SignInPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Navigated to MyAccount page with credentials:"
					+ emailid + "/" + password, driver);
			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page", driver);

			editProfilePage.clickOnIamaBelkemployee();
			Log.message("Clicked on 'I am a Belk Employee' link");
			Log.message("<b>Expected Result: </b>'Employee ID' should be displayed, when User Clicked on the 'Im a Belk Employee' link.");
			Log.assertThat(
					editProfilePage.elementLayer.verifyPageElements(element,
							editProfilePage),
					"<b>Expected Result:</b>'Employee ID' is  displayed, when User Click on the 'Im a Belk Employee' link.",
					"<b>Expected Result:</b>'Employee ID' page not displayed, when User Click on the 'Im a Belk Employee' link.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_029

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Forgot Password link in login section on Wishlist or Gift Registry or Cart header pages", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_153(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> element = Arrays.asList("passwordRecoveryPage");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn SignInPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// SignIn SignInPage = homePage.headers.navigateToSignIn();

			SignInPage.clikForgotPwd();

			Log.message((i++)
					+ ". Clicked on Forgot password link in SignIn page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b>");
			Log.message("'Password Recovery' page should be displayed, when User Click on the 'Forgot password' link.");
			Log.message("<br>");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					SignInPage.elementLayer.verifyPageElements(element,
							SignInPage),
					"'Password Recovery' page  displayed, when User Click on the 'Forgot password' link.",
					"'Password Recovery' page not displayed, when User Click on the 'Forgot password' link.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_153

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Default Addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_062(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String title = testData.get("Title");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String phoneNumber = testData.get("PhoneNumber");
		String addressLine1 = testData.get("AddressLine1");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			// user has existing default addresses and verify the default
			// address is displayed same as test data(from excel)
			LinkedHashMap<String, String> defaultAddress = addressbook
					.getDefaultAddressByIndex(0);
			Log.message("<b>Expected Result: </b>The address details should be same as given by default");
			Log.message("<b>Actual Result: </b>");
			Log.softAssertThat(title.contains(defaultAddress.get("title")),
					"1. Title is same as given Title.",
					"1. Title is not same as given Title.");

			Log.softAssertThat(
					firstName.contains(defaultAddress.get("firstname")),
					"2. firstname is same as given firstname.",
					"2. firstname is not same as given firstname.");

			Log.softAssertThat(
					lastName.contains(defaultAddress.get("lastname")),
					"3. lastname is same as given lastname.",
					"3. lastname is not same as given lastname.");

			Log.softAssertThat(
					addressLine1.contains(defaultAddress.get("address1")),
					"4. address1 is same as given address1.",
					"4. address1 is not same as given address1.");

			Log.softAssertThat(city.contains(defaultAddress.get("city")),
					"5. city is same as given city.",
					"5. city is not same as given city.");

			Log.softAssertThat(
					StateUtils.getStateCode(state).equals(
							defaultAddress.get("state")),
					"6. state is same as given state.",
					"6. state is not same as given state.");

			Log.softAssertThat(zipCode.contains(defaultAddress.get("zipcode")),
					"7. zipcode is same as given zipcode.",
					"7. zipcode is not same as given zipcode.");

			Log.softAssertThat(
					phoneNumber.contains(defaultAddress.get("phone")),
					"8. phoneNumber is same as given phoneNumber.",
					"8. phoneNumber is not same as given phoneNumber.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_062

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the Tooltip \"APO/FPO and Phone\" in Add and Edit Address modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_088(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String apoFpoTooltipText = "Demandware is pleased to support billing and shipping of your order to an Armed Forces APO or FPO address. To take advantage of this service, please enter either APO or FPO in the \"City\" field. All APO and FPO orders will be shipped via the U.S. Postal Service. \"Express\" and \"Next-Day\" service is not available.";
		String shippingPhoneTooltipText = "This phone number is required in the event the shipping partner needs to arrange a delivery time with you.";
		String billingPhoneTooltipText = "The billing phone number must match the telephone number on your credit card account, otherwise the card will not be authorized.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			// user has existing addresses and edit the default address
			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on 'Edit' link of address 1");
			Log.message("<b>Expected Result: </b>Apo/Fpo Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getApoFpoTooltipText().contains(
							apoFpoTooltipText),
					"<b>Actual Result: </b>Apo/Fpo Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Apo/Fpo Tooltip text is not properly displayed(mismatch in Apo/Fpo Tooltip text) in UI.");
			Log.message("<b>Expected Result: </b>Shipping Phone Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getShippingPhoneTooltipText().contains(
							shippingPhoneTooltipText),
					"<b>Actual Result: </b>Shipping Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Shipping Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.");
			Log.message("<b>Expected Result: </b>Billing Phone Tooltip tex should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getBillingPhoneTooltipText().contains(
							billingPhoneTooltipText),
					"<b>Actual Result: </b>Billing Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Billing Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_088

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the Tooltip \"APO/FPO and Phone\" in Add and Edit Address modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_089(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String apoFpoTooltipText = "Demandware is pleased to support billing and shipping of your order to an Armed Forces APO or FPO address. To take advantage of this service, please enter either APO or FPO in the \"City\" field. All APO and FPO orders will be shipped via the U.S. Postal Service. \"Express\" and \"Next-Day\" service is not available.";
		String shippingPhoneTooltipText = "This phone number is required in the event the shipping partner needs to arrange a delivery time with you.";
		String billingPhoneTooltipText = "The billing phone number must match the telephone number on your credit card account, otherwise the card will not be authorized.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			// user has existing addresses and edit the default address
			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on 'Edit' link for address 2");
			Log.message("<b>Expected Result: </b>Apo/Fpo Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getApoFpoTooltipText().contains(
							apoFpoTooltipText),
					"<b>Actual Result: </b>Apo/Fpo Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Apo/Fpo Tooltip text is not properly displayed(mismatch in Apo/Fpo Tooltip text) in UI.");
			Log.message("<b>Expected Result: </b>Shipping Phone Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getShippingPhoneTooltipText().contains(
							shippingPhoneTooltipText),
					"<b>Actual Result: </b>Shipping Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Shipping Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.");
			Log.message("<b>Expected Result: </b>Billing Phone Tooltip textshould be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getBillingPhoneTooltipText().contains(
							billingPhoneTooltipText),
					"<b>Actual Result: </b>Billing Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result: </b>Billing Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.");

			// click on apo/fpo link and ApoFpo page is displayed.
			ApoFpoPage apofpoPage = addressbook
					.clickOnApoFpoTooltipLinkInAddNewAddressModal();
			// click 'My Account' in headers
			myaccount = apofpoPage.headers.navigateToMyAccount();
			// click on 'Address Book'
			addressbook = myaccount.navigateToAddressBook();
			// user has existing addresses and edit the default address
			addressbook.clickOnEditAddressByIndex(1);
			// click on phone tooltip and WhyIsRequiredPage is displayed.
			WhyIsRequiredPage whyIsRequiredPage = addressbook
					.clickOnPhoneTooltipLinkInAddNewAddressModal();
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_089

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Editing an address in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_068(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			// click edit address
			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on 'Edit' link for address 1");

			// update address field
			String[] addresses = { "home", "office", "shop", "complex" };
			Random address = new Random();
			int index = address.nextInt(addresses.length);
			Log.message((i++) + ". Updated Address field value is: "
					+ addresses[index]);
			addressbook.setAddressTitle(addresses[index]);

			// apply changes
			addressbook.clickSaveAddressEditForm();
			Log.message((i++)
					+ ". Clicked on 'Save' button in Edit Address form");
			// provide some wait
			Thread.sleep(8000);

			// get the updated address field values
			LinkedHashMap<String, String> updatedAddressValues = addressbook
					.getDefaultAddressByIndex(0);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Updated title field should be displayed in UI.");
			Log.softAssertThat(
					addresses[index-1]
							.contains(updatedAddressValues.get("title")),
					"<b>Actual Result: </b>Updated title field is displayed in UI.",
					"<b>Actual Result: </b>Updated title field is not displayed in UI.");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Default Shipping Checkbox should be selected.");
			Log.softAssertThat(
					updatedAddressValues.get("defaultshipping").toLowerCase()
							.contains("yes"),
					"<b>Actual Result: </b>Default Shipping Checkbox is selected.",
					"<b>Actual Result: </b>Default Shipping Checkbox is not selected.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_068

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify \"Address Verification\" modal, when  the entered address is incorrect and a single address is returned as a recommendation.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_090(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");
		String titleInNoAddressDialog = "No Address Found";
		String messageInNoAddressDialog = "Your shipping address could not be confirmed, making your order potentially undeliverable. Please verify the shipping address was entered correctly before continuing.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled new Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);
			Log.message("<b>Expected Result: </b>Title should be same as Title in NoAddressDialog.");
			Log.softAssertThat(
					titleInNoAddressDialog.contains(addressBookPage
							.getTitleInNoAddressDetailsDialog()),
					"<b>Actual Result: </b>Title is same as Title in NoAddressDialog.",
					"<b>Actual Result: </b>Title mismatch in NoAddressDialog.");
			Log.message("<b>Expected Result: </b>Message should be same as Message in NoAddressDialog.");
			Log.softAssertThat(
					messageInNoAddressDialog.contains(addressBookPage
							.getMessageContentInNoAddressDetailsDialog()),
					"<b>Actual Result: </b>Message is same as Message in NoAddressDialog.",
					"<b>Actual Result: </b>Message mismatch in NoAddressDialog.",
					driver);

			addressBookPage.clickCountinueInNoAddressDetailsDialog();
			Log.message((i++) + ". Clicked Continue button in NoAddressDialog",
					driver);

			addressBookPage = new AddressBookPage(driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++)
					+ ". Test Data Cleanup - Deleted the Added Address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_090

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting Suggested address or Use Original address in Address Verification modal. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_091(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			int totalAddress = addressBookPage.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++) {
					addressBookPage.deleteAddressByIndex(0);
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled in Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked 'Save' button to Save the Address",
					driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Selected Use Original Address", driver);
			boolean useOriginal = true;
			addressBookPage.selectUseSuggestedAddress();
			Log.message((i++) + ". Selected Use Suggested Address", driver);
			boolean useSuggestion = true;
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++)
					+ ". Clicked Continue button on Address Suggestion", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Use original address' and 'Use suggestion address' should displayed and user able to check on it");
			Log.assertThat(
					useSuggestion && useOriginal,
					"<b>Actual Result: </b>User can able to check the 'Use original address' and 'Use suggestion address' options",
					"<b>Actual Result: </b>User unable to check the 'Use original address' and 'Use suggestion address' options",
					driver);
			addressBookPage = new AddressBookPage(driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++)
					+ ". Test Data Celanup - Deleted the Added Address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_091

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Cancel and Continue button in the Address Verification modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_092(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);
			String suggesstedAddress = "23415 Three Notch Rd Ste 2057";

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address");

			addressBookPage.selectUseSuggestedAddress();
			Log.message((i++) + ". Select Use Suggessted Address");

			addressBookPage.clickContinueAddressSuggestion();
			Log.message(
					(i++) + ". Click Continue button on Address Suggestion");

			addressBookPage = new AddressBookPage(driver);

			String savedAddress = addressBookPage
					.getAddressOneFromSavedAddress(0);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Suggessted Address should be saved to the Address book.");
			Log.assertThat(
					suggesstedAddress.contains(savedAddress),
					"<b>Actual Result: </b>Suggessted Address is saved to the Address book.",
					"<b>Actual Result: </b>Suggessted Address is not saved to the Address book.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message("<br>");
			Log.message((i++)
					+ ". Test Data Celanup - Delete the Added Address");

			addressBookPage = new AddressBookPage(driver);
			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Again clicked the add new address button");
			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled the address");
			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked on save button");
			addressBookPage.clickCancelAddressSuggestion();
			Log.message((i++) + ". Clicked on cancel button from the address suggestion popup");
			addressBookPage.clickCancelAddressAddEditForm();
			Log.message((i++) + ". Clicked on cancel button from the add new address popup");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address suggestion should be closed.");
			String popupTitle=addressBookPage.getTextFromDialogContainerHeader().trim();
			Log.assertThat(popupTitle.equals("Add New Address"),
					"<b>Actual Result: </b>The Address suggestion is closed.",
					"<b>Actual Result: </b>The Address suggestion is not closed.",
					driver);
			
			addressBookPage = new AddressBookPage(driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_092

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Cancel and Continue button in the Address Verification modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_103(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message("6. Fill in Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message("7. Click Save button to Save the Address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_092

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Forgot Password link in login section on Wishlist or Gift Registry or Cart header pages", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_151(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn SignInPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// SignIn SignInPage = homePage.headers.navigateToSignIn();

			CreateAccountPage createAccountPage = SignInPage
					.clickCreateAccount();

			// Creating a new account
			WishListPage wishlistpage = (WishListPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message((i++) + ". Created a new user account");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_151

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'Need Help' content in the footer of the Address Verification modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_094(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled New Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);
			Log.message("<b>Expected Result: </b>Footer: 'Need Help?' should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationFooter().equals(
							"Need Help?"),
					"8. Expected Footer: 'Need Help?' is displayed in the Address Validation Form",
					"8. Expected Footer: 'Need Help?' is not displayed in the Address Validation Form.",
					driver);

			addressBookPage.clickCancelAddressSuggestion();

			addressBookPage.clickCancelAddressAddEditForm();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_094

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'Need Help' content in the footer of the Address Verification modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_095(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled New Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);
			Log.message("<b>Expected Result: </b>Expected Header: 'Address not Recognized by USPS' should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address not Recognized by USPS"),
					"<b>Actual Result: </b>Expected Header: 'Address not Recognized by USPS' is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);
			String message = "We're sorry. The U.S Postal Service does not recognize this address. Please choose from the following options:";
			Log.message("<b>Expected Result: </b>Expected Header Msg: '"
					+ message
					+ "' should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeaderMsg().equals(
							message),
					"<b>Actual Result: </b>Expected Header Msg: '" + message
							+ "' is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Header Msg is not displayed in the Address Validation Form.",
					driver);
			Log.message("<b>Expected Result: </b>Expected Footer: 'Need Help?' should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationFooter().equals(
							"Need Help?"),
					"<b>Actual Result: </b>Expected Footer is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Footer is not displayed in the Address Validation Form.",
					driver);

			addressBookPage.clickCancelAddressSuggestion();

			addressBookPage.clickCancelAddressAddEditForm();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_095

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the cancel button and Help section in 'Address not Recognized by USPC' modal, when street no entered is not correct.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_102(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled new Address Details", driver);

			addressBookPage.clickCancelAddressAddEditForm();
			Log.message(
					(i++)
							+ ". Clicked 'Cancel' button to close the Add Address Form",
					driver);

			addressBookPage = new AddressBookPage(driver);
			Log.message("<b>Expected Result: </b>The Address should not be empty");
			Log.assertThat(addressBookPage.getSavedAddress().isEmpty(),
					"<b>Actual Result: </b>The Address is not Added",
					"<b>Actual Result: </b>The Address is Added");
			Log.message("<b>Expected Result: </b>Fields 'Contact Customer Service' in the Need Help Section should be displayed ");
			Log.assertThat(
					addressBookPage.needHelpSection.getNeedHelpSectionText()
							.contains("Contact Customer Service"),
					"<b>Actual Result: </b>Fields 'Contact Customer Service' in the Need Help Section is displayed ",
					"<b>Actual Result: </b>Fields in the Need Help Section is not displayed as expected, plz check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_102

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Cancel Button and the Help Section in 'Address Verification' modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_106(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
							+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled New Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage.clickCancelAddressSuggestion();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage.clickCancelAddressAddEditForm();
			Log.message((i++) + ". Clicked Save button to Save the Address",
					driver);

			addressBookPage = new AddressBookPage(driver);

			Log.assertThat(addressBookPage.getTotalAddressCount() > 0, (i++)
					+ ". The Address is not Added", "10. The Address is Added");
			Log.message("<b>Expected Result: </b>Fields 'Contact Customer Service' in the Need Help Section should be displayed");
			Log.assertThat(
					addressBookPage.needHelpSection.getNeedHelpSectionText()
							.contains("Contact Customer Service"),
					"<b>Actual Result: </b>Fields 'Contact Customer Service' in the Need Help Section is displayed ",
					"<b>Actual Result: </b>Fields in the Need Help Section is not displayed as expected, plz check the event log",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_106

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the address displayed in the 'No Address Found' modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_108(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address");
			BrowserActions.nap(3);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The Address Entered by the User should listed in Original Address Section");
			Log.assertThat(
					addressBookPage.getOriginalAddressInAddressValidation()
							.trim().contains(addressLine1),
					"<b>Actual Result: </b>The Address Entered by the User is listed in Original Address Section",
					"<b>Actual Result: </b>The Address Entered by the User is not listed in Original Address Section",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_108

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Forgot Password link in login section on Wishlist or Gift Registry or Cart header pages", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_154(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn SignInPage = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++) + ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// SignIn SignInPage = homePage.headers.navigateToSignIn();

			SignInPage.clikForgotPwd();
			Log.message((i++) + ". Clicked on Forgot password link in SignIn page");

			SignInPage.enterResetPasswordEmail(emailid);
			Log.message((i++) + ". Entered Reset Password Email ID and clicked Send button");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Reset Password Email Sent' message should displayed");
			
			Log.assertThat(
					SignInPage.getResetPasswordHeaderContent().contains(
							"New Password Email Sent"),
					"<b>Actual Result: </b>'Password Recovery Sucess' page Header content displayed, when User enter Reset Password Mail ID and click sent.",
					"<b>Actual Result: </b>'Password Recovery Sucess' page Header content not displayed, when User enter Reset Password Mail ID and click sent.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Thanks message should displayed");
			Log.assertThat(
					SignInPage
							.getResetPasswordMsg()
							.contains(
									"Please check your inbox. If <email_user@email.com> is associated with a belk.com account, you will receive your new password shortly."),
					"<b>Actual Result: </b>'Password Recovery Sucess' Message displayed, when User enter Reset Password Mail ID and click sent.",
					"<b>Actual Result: </b>'Password Recovery Sucess' Message not displayed, when User enter Reset Password Mail ID and click sent.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_154

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Write a review link in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_190(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			OrderHistoryPage OrderHistoryPage = myaccount
					.navigateToOrderHistoryPage();
			Log.message("Navigated to Order history page");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The order details page should displayed while click on View details");
			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message("<b>Actual Result: </b>. Order Details page is displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_190

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting the set default checkboxes in add and edit address modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_085(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");

			addressbook.checkDefaultShippingAddressInAddNewAddress();
			Log.message((i++) + ". Checked Default Shipping Address!");

			addressbook.clickOnSaveInAddNewAddressModalAfterEdit();
			addressbook.clickOnContinueInNoAddressFoundPopup();
			Log.message((i++) + ". Clicked on Save in New Address Modal After Edit!");
			BrowserActions.nap(5);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address checkbox should checked.");
			Log.assertThat(
					addressbook.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Shipping Address checkbox is checked.",
					"<b>Actual Result: </b>The Default Shipping Address checkbox is not checked as expected.",
					driver);

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address checkbox should checked in the Add New Address modal");
			Log.assertThat(
					addressbook
							.isDefaultShippingAddressInAddNewAddressChecked(),
					"<b>Actual Result: </b>The Default Shipping Address checkbox is checked in the Add New Address modal.",
					"<b>Actual Result: </b>The Default Shipping Address checkbox is not checked in the Add New Address modal as expected",
					driver);

			addressbook.CheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Checked Default Billing Address!");

			addressbook.clickOnSaveInAddNewAddressModalAfterEdit();
			addressbook.clickOnContinueInNoAddressFoundPopup();
			Log.message((i++) + ". Clicked on Save in New Address Modal After Edit!");
			BrowserActions.nap(3);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address checkbox should checked.");
			Log.assertThat(
					addressbook.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Shipping Address checkbox is checked.",
					"<b>Actual Result: </b>The Default Shipping Address checkbox is not checked as expected.");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Billing Address checkbox should checked.");
			Log.assertThat(
					addressbook.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result: </b>The Default Billing Address checkbox is checked.",
					"<b>Actual Result: </b>The Default Billing Address checkbox is not checked as expected.",
					driver);

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address checkbox should checked in the Add New Address modal");
			Log.assertThat(
					addressbook
							.isDefaultShippingAddressInAddNewAddressChecked(),
					"<b>Actual Result: </b>The Default Shipping Address checkbox is checked in the Add New Address modal.",
					"<b>Actual Result: </b>The Default Shipping Address checkbox is not checked in the Add New Address modal as expected");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Billing Address checkbox should checked in the Add New Address modal");
			Log.assertThat(
					addressbook.isDefaultBillingAddressInAddNewAddressChecked(),
					"<b>Actual Result: </b>The Default Billing Address checkbox is checked in the Add New Address modal.",
					"<b>Actual Result: </b>The Default Billing Address checkbox is not checked in the Add New Address modal as expected",
					driver);

			addressbook.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Unchecked Default Billing Address!");

			addressbook.unCheckDefaultShippingAddressInAddNewAddress();
			Log.message((i++) + ". Unchecked Default Shipping Address!");

			addressbook.clickOnSaveInAddNewAddressModalAfterEdit();
			addressbook.clickOnContinueInNoAddressFoundPopup();
			Log.message((i++) + ". Clicked on Save in New Address Modal After Edit!");
			BrowserActions.nap(3);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Shipping Address checkbox should not checked.");
			Log.assertThat(
					!(addressbook.isDefaultShippingAddressByIndexChecked(0)),
					"<b>Actual Result: </b>The Default Shipping Address checkbox is not checked.",
					"<b>Actual Result: </b>The Default Shipping Address checkbox is checked, but should be unchecked.");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Default Billing Address checkbox should not checked");
			Log.assertThat(
					!(addressbook.isDefaultBillingAddressByIndexChecked(0)),
					"<b>Actual Result: </b>The Default Billing Address checkbox is not checked.",
					"<b>Actual Result: </b>The Default Billing Address checkbox is checked as expected, but should be unchecked.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_085

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that when the user tries to delete a default billing/shipping address in the Express checkout then user is navigated to the Delete Default Address modal window.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_113(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> elementsToBeVerified = null;
		elementsToBeVerified = Arrays.asList("txtSelectedSavedAddress",
				"txtAddressName", "txtFirstName", "txtLastName", "txtAddress1",
				"txtAddress2", "txtCity", "txtSelectedState", "txtZipcode",
				"txtPhone", "btnCancelInAddNewAddressModal", "btnSetAsDefault");

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");

			addressbook.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". UnChecked Default Billing Address!");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The respective fields (select saved address, addressname, first name, last name, address1, address2, city, state, zipcode, phone number) should displayed");
			
			Log.assertThat(addressbook.elementLayer.verifyPageElements(
					elementsToBeVerified, addressbook),
					"<b>Actual Result: </b>The respective fields are displayed",
					"<b>Actual Result: </b>The respective fields are not displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_113

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify unchecking the default address and payment when Express Checkout option is selected", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_086(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> elementsToBeVerified = null;
		elementsToBeVerified = Arrays.asList("txtSelectedSavedAddress",
				"txtAddressName", "txtFirstName", "txtLastName", "txtAddress1",
				"txtAddress2", "txtCity", "txtSelectedState", "txtZipcode",
				"txtPhone", "btnCancelInAddNewAddressModal", "btnSetAsDefault");

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit in Address Book Page!");

			addressbook.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". UnChecked Default Billing Address!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The 'Set Default Address' popup should displayed");
			Log.assertThat(
					addressbook.getFormTitle().equals("Set Default Address"),
					"<b>Actual Result: </b>The popup is displayed and the title is displayed correctly",
					"<b>Actual Result: </b>The popup is not displayed and/or the title is not displayed as expected", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The popup title should displayed as 'Select from existing address or add a new default address.'");
			Log.assertThat(
					addressbook
							.getFormDescription()
							.equals("Select from existing address or add a new default address."),
					"<b>Actual Result: </b>The popup title is displayed correctly",
					"<b>Actual Result: </b>The popup title is not displayed as expected", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The respective fields (select saved address, addressname, first name, last name, address1, address2, city, state, zipcode, phone number) should displayed");
			Log.assertThat(addressbook.elementLayer.verifyPageElements(
					elementsToBeVerified, addressbook),
					"<b>Actual Result: </b>The respective fields are displayed",
					"<b>Actual Result: </b>The respective fields are not displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_086

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Payment Method link from My Account screen when there are no payments already added for that user.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_127(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> breadcrumbText = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to payment methods page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' message should displayed");
			Log.assertThat(
					paymentMethodsPage
							.getNoPaymentMethodsAddedMessage()
							.equals("You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future."),
					"<b>Actual Result: </b>Message 'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' is displayed when there is no Payment Methods already added for the account",
					"<b>Actual Result: </b>Required Error message 'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' is not displayed when there is no Payment Methods already added for the account",driver);
			List<String> displayedElements = Arrays
					.asList("btnAddPaymentMethod");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Add payment method' button should displayed");
			Log.assertThat(paymentMethodsPage.elementLayer.verifyPageElements(
					displayedElements, paymentMethodsPage),
					"<b>Actual Result: </b>Add Payment Method buton is displayed.",
					"<b>Actual Result: </b>Add Payment Method button is not displayed.", driver);
			breadcrumbText = paymentMethodsPage.getTextInBreadcrumb();
			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>The breadcrumb should displayed as 'Home > My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Home")
								&& breadcrumbText.get(1).equals("My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Home > My Account'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Home > My Account' and the actual is : '"
								+ String.join(">", breadcrumbText) + "'");
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>The breadcrumb should displayed as 'Back to My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Back to My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Back to My Account'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ String.join(">", breadcrumbText) + "'");
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_127

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Payment Method screen, when no payment methods are added", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_142(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> breadcrumbText = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to 'payment method' Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' message should displayed if the account doesn't have any payment methods");
			Log.assertThat(
					paymentMethodsPage
							.getNoPaymentMethodsAddedMessage()
							.equals("You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future."),
					"<b>Actual Result: </b>Message 'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' is displayed when there is no Payment Methods already added for the account",
					"<b>Actual Result: </b>Required Error message 'You have not added any methods of payment to your profile. Adding a payment method will save you time and make ordering quicker in the future.' is not displayed when there is no Payment Methods already added for the account");
			List<String> displayedElements = Arrays
					.asList("btnAddPaymentMethod");
			
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Add Payment Method buton should displayed");
			Log.assertThat(paymentMethodsPage.elementLayer.verifyPageElements(
					displayedElements, paymentMethodsPage),
					"<b>Actual Result: </b>Add Payment Method buton is displayed.",
					"<b>Actual Result: </b>Add Payment Method button is not displayed.", driver);

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked on the add payment method button");
			displayedElements = Arrays.asList("frmAddPaymentMethodForm");
			Log.assertThat(paymentMethodsPage.elementLayer.verifyPageElements(
					displayedElements, paymentMethodsPage),
					"<b>Actual Result: </b>Add Payment Method form is displayed.",
					"<b>Actual Result: </b>Add Payment Method form is not displayed.", driver);

			paymentMethodsPage.clickOnCancel();
			Log.message((i++) + ". Cancel button is clicked");
			breadcrumbText = paymentMethodsPage.getTextInBreadcrumb();
			if (runPltfrm != "mobile") {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>The breadcrumb should displayed as 'Home > My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Home")
								&& breadcrumbText.get(1).equals("My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Home > My Account'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Home > My Account' and the actual is : '"
								+ String.join(">", breadcrumbText) + "'");
			} else {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>The breadcrumb should displayed as 'Back to My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Back to My Account"),
						"<b>Actual Result: </b>The breadcrumb is displayed as expected - 'Back to My Account'",
						"<b>Actual Result: </b>The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ String.join(">", breadcrumbText) + "'");
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_142

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify validating Employee ID and Store ID for a Belk employee in Edit Profile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_052(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String confirmEmail = testData.get("ConfirmEmailAddress");
		String confirmPassword = testData.get("ConfirmPassword");
		String empId = testData.get("Employee ID");
		String storeId = testData.get("Store ID");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage profilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Profile Page!");

			profilePage.typeOnConfirmEmailField(confirmEmail);
			profilePage.typeOnPasswordField(confirmPassword);

			profilePage.checkIAmBelkEmployee();
			Log.message((i++) + ". Checked I Am Belk employee check box in Profile Page!");

			profilePage.typeOnEmployeeIdField(empId);
			profilePage.typeOnStoreIdField(storeId);
			profilePage.clickOnProfileApplyChanges();
			Log.message((i++) + ". Enter invalid details in Emplyoee section in Profile Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Proper error message should displayed");
			Log.assertThat(
					profilePage
							.getInvalidEmployeeIdMessage()
							.equals("We're sorry. We don't recognize the Belk associate ID number you entered. Please double-check the number and re-enter it."),
					"<b>Actual Result: </b>We're sorry. We don't recognize the Belk associate ID number you entered. Please double-check the number and re-enter it.' error message should be displayed for Employee Id",
					"<b>Actual Result: </b>Invalid Employee Id error message is not displayed as expected");

			Log.assertThat(
					profilePage
							.getInvalidStoreIdMessage()
							.equals("We're sorry. We don't recognize the Belk associate ID number you entered. Please double-check the number and re-enter it."),
					"<b>Actual Result: </b>We're sorry. We don't recognize the Belk associate ID number you entered. Please double-check the number and re-enter it.' error message should be displayed for Store Id",
					"<b>Actual Result: </b>Invalid Store Id error message is not displayed as expected");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_052

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Address link from My Account landing page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_058(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressBookPage = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");
			int addressCount = addressBookPage.getTotalAddressCount();
			if (addressCount > 0) {
				for (int j = 1; j <= addressCount; j++) {
					addressBookPage.deleteAddressByIndex(0);
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + "Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);
			List<String> indexes = new ArrayList<String>(newAddress.values());

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Fill in Address Details", driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ".  Click Save Address");

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address");

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			newAddress.clear();
			newAddress.put("Title", indexes.get(0));
			newAddress.put("FirstName", indexes.get(1));
			newAddress.put("LastName", indexes.get(2));
			newAddress.put("Address1", indexes.get(3));
			newAddress.put("Address2", indexes.get(4));
			newAddress.put("City", indexes.get(5));
			newAddress.put("State", indexes.get(6));
			newAddress.put("Zipcode", indexes.get(7));
			newAddress.put("PhoneNo", indexes.get(8).replace("-", ""));
			newAddress.put("Defaultshipping", indexes.get(9));
			newAddress.put("Defaultbilling", indexes.get(10));
		
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should saved to the Address book.");


			Log.assertThat(savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);

			addressBookPage.deleteAddressByIndex(0);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_058

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Deleting an address in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_069(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ".  Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressBookPage = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");

			int addressCount = addressBookPage.getTotalAddressCount();
			if (addressCount > 0) {
				for (int j = 1; j <= addressCount; j++) {
					addressBookPage.deleteAddressByIndex(0);
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> filledAddressDetails = addressBookPage
					.fillingAddNewAddressDetails("valid_address6", "NO", "NO");
			Log.message((i++) + ". Fill in Address Details");

			List<String> indexes = new ArrayList<String>(
					filledAddressDetails.keySet());
			String title = indexes.get(0).replace("type_addressname_", "");
			String firstname = indexes.get(1).replace("type_firstname_", "");
			String lastname = indexes.get(2).replace("type_lastname_", "");
			String address = indexes.get(3).replace("type_address_", "");
			String address1 = indexes.get(4).replace("type_address_", "");
			String city = indexes.get(5).replace("type_city_", "");
			String state = indexes.get(6).replace("select_state_", "");
			String zipcode = indexes.get(7).replace("type_zipcode_", "");
			String phone = indexes.get(8).replace("type_phoneno_", "");
			filledAddressDetails.clear();
			filledAddressDetails.put("title", title);
			filledAddressDetails.put("firstname", firstname);
			filledAddressDetails.put("lastname", lastname);
			filledAddressDetails.put("address1", address);
			filledAddressDetails.put("address2", address1);
			filledAddressDetails.put("city", city);
			filledAddressDetails.put("state", state);
			filledAddressDetails.put("zipcode", zipcode);
			filledAddressDetails.put("phone", phone);
			filledAddressDetails.put("defaultshipping", "No");
			filledAddressDetails.put("defaultbilling", "No");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save Address");

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address");

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			List<String> index = new ArrayList<String>(
					filledAddressDetails.values());

			filledAddressDetails.clear();
			filledAddressDetails.put("Title",
					index.get(0).replace("type_addressname_", ""));
			filledAddressDetails.put("FirstName",
					index.get(1).replace("type_firstname_", ""));
			filledAddressDetails.put("LastName",
					index.get(2).replace("type_lastname_", ""));
			filledAddressDetails.put("Address1",
					index.get(3).replace("type_address_", ""));
			filledAddressDetails.put("Address2",
					index.get(4).replace("type_address_", ""));
			filledAddressDetails.put("City",
					index.get(5).replace("type_city_", ""));
			filledAddressDetails.put("State",
					index.get(6).replace("select_state_", ""));
			filledAddressDetails.put("Zipcode",
					index.get(7).replace("type_zipcode_", ""));
			filledAddressDetails.put("PhoneNo",
					index.get(8).replace("type_phoneno_", ""));
			filledAddressDetails.put("Defaultshipping", index.get(9));
			filledAddressDetails.put("Defaultbilling", index.get(10));
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should saved to the Address book.");
			Log.assertThat(savedAddress.equals(filledAddressDetails),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked on the 'Delete' link");
			addressBookPage.clickCancelDeleteAddress();
			Log.message((i++) + ". Click Cancel Delete Address");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>he Address should not Deleted/Removed");
			Log.assertThat(addressBookPage.getSavedAddress().size() == 1,
					"<b>Actual Result: </b>The Address is not Deleted/Removed",
					"<b>Actual Result: </b>The Address is Deleted/Removed");

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++) + ". Click Confirm Delete Address");

			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should Deleted/Removed");
			Log.assertThat(addressBookPage.getSavedAddress().isEmpty(),
					"<b>Actual Result: </b>The Address is Deleted/Removed",
					"<b>Actual Result: </b>The Address is not Deleted/Removed",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_069

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify editing and deleting the addresses", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_078(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		Date dateobj = new Date();
		addressName = addressName + df.format(dateobj).toString();
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressBookPage = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");
			int addressCount = addressBookPage.getTotalAddressCount();

			if (addressCount <= 0) {
				addressBookPage.clickAddNewAddress();
				addressBookPage.fillingAddNewAddressDetails("valid_address6",
						"No", "No");
				addressBookPage.ClickOnSave();
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.clickContinueAddressSuggestion();
			} else if (addressBookPage.getTotalAddressCount() > 1) {
				for (int j = 1; j <= addressCount; j++) {
					addressBookPage.deleteAddressByIndex(0);
				}
			}

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Click on the Edit link for first address in Address Book Page!");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Edit Address Modal should be displayed");
			Log.assertThat(
					addressBookPage.getFormTitle().trim()
							.equals("Edit Address"),
					"<b>Actual Result: </b>Edit Address Modal is displayed",
					"<b>Actual Result: </b>Edit Address modal is not displayed",driver);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Filled in Address Details");

			addressBookPage.ClickOnSave();
			if (addressBookPage.verifyDialogContainerDisplayed()) {
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.clickContinueAddressSuggestion();
			}
			Log.message((i++) + ". Click Save Address");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			List<String> index = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", index.get(0));
			newAddress.put("FirstName", index.get(1));
			newAddress.put("LastName", index.get(2));
			newAddress.put("Address1", index.get(3));
			newAddress.put("Address2", index.get(4));
			newAddress.put("City", index.get(5));
			newAddress.put("State", index.get(6));
			newAddress.put("Zipcode", index.get(7));
			newAddress.put("PhoneNo", index.get(8));
			newAddress.put("Defaultshipping", index.get(9));
			newAddress.put("Defaultbilling", index.get(10));
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should saved to the Address book.");
			Log.assertThat(savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++) + ". Click Confirm Delete Address", driver);

			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Address should Deleted/Removed");
			Log.assertThat(addressBookPage.getSavedAddress().isEmpty(),
					"<b>Actual Result: </b>The Address is Deleted/Removed",
					"<b>Actual Result: </b>The Address is not Deleted/Removed");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_078

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting shipping address from Saved Address dropdown in Delete Default Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_116(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			int totalAddress = addressbook.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++)
					addressbook.deleteAddressByIndex(0);
				Log.message((i++) + ".Navigated to address book page and removed previously added address!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int k = 1; k <= totalPayment; k++)
					paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++) + ".Navigated to payment page and removed previously added payment!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			ProfilePage profilepage = (ProfilePage) addressbook
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to Profile page");
			profilepage.clickOnSetDefaultAddress();
			Log.message((i++) + ". Clicked on set default address button");
			profilepage
					.fillingAddNewAddressDetail("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++) + ". Clicked on save button and default shipping address is added");

			profilepage.clickOnSetDefaultBilling();
			Log.message((i++) + ". Clicked on set default billing button");
			profilepage
					.fillingAddNewAddressDetail("valid_address2", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();

			Log.message((i++) + ". Clicked on save button and default billing address is added");

			profilepage.clickOnSetAsDefaultPayment();
			Log.message((i++) + ". Clicked on set default payment button");
			profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			Log.message((i++) + ". Clicked on save button and default payment is added");

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Express checkout checkbox is checked");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			addressbook = (AddressBookPage) addressbook
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");
			addressbook.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked on the delete button from the 'default shipping address'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Delete default address' popup should displayed");
			Log.assertThat(
					addressbook.getTextFromDialogContainerHeader().equals(
							"Delete Default Address"),
					"<b>Actual Result:</b> The 'Delete default address' popup is displayed",
					"<b>Actual Result:</b> The 'Delete default address' popup is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_116

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding new billing address in Delete Default Address screen address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_115(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			
			ProfilePage profilepage=(ProfilePage) myaccount.navigateToSection("profile");
			
			if (!profilepage.isExpressCheckoutCheckboxEnabled()) {
				profilepage.checkExpressCheckout("UnCheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.event("UnChecked the Express checkout check box");
			} else {
				Log.event("The express checkout is unchecked by default");
			}
			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			int totalAddress = addressbook.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++)
					addressbook.deleteAddressByIndex(0);
				Log.message((i++) + ".Navigated to address book page and removed previously added address!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int k = 1; k <= totalPayment; k++)
					paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++) + ".Navigated to payment page and removed previously added payment!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			profilepage = (ProfilePage) addressbook
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to Profile page");
			
			profilepage.clickOnSetDefaultBilling();
			Log.message((i++) + ". Clicked on set default billing button");
			profilepage
					.fillingAddNewAddressDetail("valid_address2", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();

			Log.message((i++) + ". Clicked on save button and default billing address is added");
			
			profilepage.clickOnSetDefaultAddress();
			Log.message((i++) + ". Clicked on set default address button");
			profilepage
					.fillingAddNewAddressDetail("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++) + ". Clicked on save button and default shipping address is added");
			profilepage.clickOnSetAsDefaultPayment();
			Log.message((i++) + ". Clicked on set default payment button");
			profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			Log.message((i++) + ". Clicked on save button and default payment is added");

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Express checkout checkbox is checked");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			addressbook = (AddressBookPage) addressbook
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");
			addressbook.clickOnDeleteAddressByIndex(1);
			Log.message((i++) + ". Clicked on the delete button from the 'default billing address'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Delete default address' popup should displayed");
			Log.assertThat(
					addressbook.getTextFromDialogContainerHeader().equals(
							"Delete Default Address"),
					"<b>Actual Result:</b> The 'Delete default address' popup is displayed",
					"<b>Actual Result:</b> The 'Delete default address' popup is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_115

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding new payment method from Add Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_128(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		String cardType = testData.get("CardType");
		String cardOwner = testData.get("CardOwner");
		String cardNumber = testData.get("CardNumber");
		cardNumber = cardNumber.replace("411111111111", "************");
		String cardExpireDate = testData.get("CardExpireDate");
		String defaultPayment = testData.get("DefaultPayment");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ".  Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged into and navigated to 'MyAccountPage' Page!",
					driver);

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment Page");

			int paymentCount = paymentMethodsPage.getTotalPaymentCount();
			if (paymentCount > 0) {
				for (int j = 1; j <= paymentCount; j++) {
					paymentMethodsPage.deletePaymentByIndex(0);
				}
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked on 'Add Payment' button");

			LinkedHashMap<String, String> newPaymentDetails = new LinkedHashMap<String, String>();

			newPaymentDetails.put("cardtype", cardType);
			newPaymentDetails.put("cardowner", cardOwner);
			newPaymentDetails.put("cardno", cardNumber);
			newPaymentDetails.put("cardexpirydate", cardExpireDate);
			newPaymentDetails.put("defaultpayment", defaultPayment);

			paymentMethodsPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Filled in Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Saved Payment Details");

			LinkedHashMap<String, String> savedPaymentDetails = paymentMethodsPage
					.getSavedPaymentByIndex(0);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Payment should saved to the Payment details");
			Log.assertThat(savedPaymentDetails.equals(newPaymentDetails),
					"<b>Actual Result: </b>The Payment is saved to the Payment details.",
					"<b>Actual Result: </b>The Payment is not saved to the Payment details.",
					driver);

			paymentMethodsPage.setDefaultPaymentByIndex(0);
			Log.message((i++) + ". Set Current Payment as Default");

			paymentMethodsPage.clickSaveChanges();
			Log.message((i++) + ". Clicked on Save Changes!");

			newPaymentDetails.put("defaultpayment", "Yes");

			savedPaymentDetails = paymentMethodsPage.getSavedPaymentByIndex(0);
			Log.message("<b>Expected Result: </b>The Payment details should be changed to default in Payment Details");
			Log.assertThat(
					savedPaymentDetails.equals(newPaymentDetails),
					"<b>Actual Result: </b>The Payment is Changed to default in Payment Details.",
					"<b>Actual Result: </b>The Payment is not Changed to default in Payment Details.",
					driver);

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked 'Add Payment' button");

			paymentMethodsPage.fillingCardDetails("NO", "card_Amex");
			Log.message((i++) + ". Filled New Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Saved Payment Details", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Payment given should be saved to the Payment details.");
			Log.assertThat(savedPaymentDetails.equals(newPaymentDetails),
					"<b>Actual Result: </b>The Payment is saved to the Payment details.",
					"<b>Actual Result: </b>The Payment is not saved to the Payment details.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_128

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify deleting the cards from Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_133(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment Page");

			int totalPayment = paymentMethodsPage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int j = 1; j <= totalPayment; j++) {
					paymentMethodsPage.deletePaymentByIndex(0);
				}
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			paymentMethodsPage.fillingCardDetails("NO", "card_Visa");
			paymentMethodsPage.clickOnSave();

			Log.message((i++) + ". Saved the card 1 details");

			paymentMethodsPage.clickOnAddPaymentMethod();
			paymentMethodsPage.fillingCardDetails("NO", "card_Amex");
			paymentMethodsPage.clickOnSave();

			Log.message((i++) + ". Saved the card 2 details");

			LinkedHashMap<String, String> savedPaymentDetails_1 = paymentMethodsPage
					.getSavedPaymentByIndex(0);

			LinkedHashMap<String, String> savedPaymentDetails_2 = paymentMethodsPage
					.getSavedPaymentByIndex(1);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The card types should be different");

			Log.assertThat(
					!(savedPaymentDetails_1.get("cardtype")
							.equals(savedPaymentDetails_2.get("cardtype"))),
					"<b>Actual Result: </b>Card Types are different as expected.",
					"<b>Actual Result: </b>Card Types are same.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The 'Delete this address' should be displayed");
			Log.assertThat(
					paymentMethodsPage.getDeleteTooltipText().equals(
							"Delete this address"),
					"<b>Actual Result: </b>The Tool Tip is Same as Expected: 'Delete this address'.",
					"<b>Actual Result: </b>The Tool Tip is not same as Expected.",
					driver);
			Log.message("<br>");
			paymentMethodsPage.deletePaymentByIndex(1);
			paymentMethodsPage.deletePaymentByIndex(0);

			Log.message((i++) + ". Removed the all card details.");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The payment should not be displayed after deleted all the details");
			Log.assertThat(paymentMethodsPage.getSavedPayment().isEmpty(),
					"<b>Actual Result: </b>Payments are deleted / Removed",
					"<b>Actual Result: </b>Payments are not deleted / Removed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_133

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the order of display for the addresses in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_063(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPlatform = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			myaccount.clickOnTabFromAccountLanding("Address Book");
			AddressBookPage addressbook = new AddressBookPage(driver);

			Log.message((i++) + ". Navigated to Address Book Page!");

			if (runPlatform.equals("desktop")) {
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Three Address per row should be displayed for Desktop");
				Log.assertThat(
						addressbook.getLocation(1) == addressbook
								.getLocation(2)
								&& addressbook.getLocation(2) == addressbook
										.getLocation(3),
						"<b>Actual Result: </b>Three Address per row is displayed for Desktop",
						"<b>Actual Result: </b>Three Address per row is not displayed for Desktop");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> Fourth Address should be displayed in next row for Desktop");
				Log.assertThat(
						addressbook.getLocation(1) != addressbook
								.getLocation(4),
						"<b>Actual Result: </b>Fourth Address is displayed in next row for Desktop",
						"<b>Actual Result: </b>Fourth Address is not displayed in next row for Desktop");

			} else if (runPlatform.equals("mobile")) {
				Log.message("<br>");
				Log.message("<b>Expected Result: </b>Only one Address per row should be displayed for Mobile");
				Log.assertThat(
						addressbook.getLocation(1) != addressbook
								.getLocation(2),
						"<b>Actual Result: </b>Only one Address per row is displayed for Mobile",
						"<b>Actual Result: </b>Only one Address per row is not displayed for Mobile");
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_063

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify un-selecting \"Don't miss out\" checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_032(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String confirmEmail = testData.get("ConfirmEmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			String isAddToEmailListChecked = editProfilePage
					.isAddToEmailListChecked();
			
			if(isAddToEmailListChecked.equals("No")){
				editProfilePage.checkOnEmailSubscriptionCheckbox();
				editProfilePage.typeOnConfirmEmailField(confirmEmail);
				editProfilePage.typeOnPasswordField(password);

				// click on 'Apply Changes'
				editProfilePage.clickOnProfileApplyChanges();
				editProfilePage = myaccount.navigateToEditProfilePage();
			}
			isAddToEmailListChecked = editProfilePage
					.isAddToEmailListChecked();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> 'AddToEmailList' checkbox should checked.");
			Log.assertThat(
					isAddToEmailListChecked.equals("Yes"),
					"<b>Actual Result:</b> AddToEmailList checkbox is checked.",
					"<b>Actual Result:</b> AddToEmailList checkbox is not checked.");
			Log.message("<br>");

			if (isAddToEmailListChecked.equals("Yes"))
				editProfilePage.uncheckOnEmailSubscriptionCheckbox();
			Log.message((i++) + ". Unchecked the Email Subscription checkbox.");

			// enter confirm email and password values
			editProfilePage.typeOnConfirmEmailField(confirmEmail);
			editProfilePage.typeOnPasswordField(password);
			BrowserActions.nap(3);
			// click on 'Apply Changes'
			editProfilePage.clickOnProfileApplyChanges();
			editProfilePage.clickOnProfileApplyChanges();
			Log.message((i++) + ". Apply changes button is clicked");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Unchecked the Email subscription and the page should navigated to Unsubscribed page");

			Log.softAssertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Unsubscribe"),
					"<b>Actual Result:</b> Unchecked the Email subscription and the page is navigated to Unsubscribed page with expected url: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b> Unchecked the Email subscription and the page is not navigated to Unsubscribed page with expected url is: "
							+ driver.getCurrentUrl(), driver);
			Log.message("<b>Expected Result:</b> The confirmation message should be displayed that the user's email address has been unsubscribed.");
			Log.assertThat(
					editProfilePage.verifyUnsubscribeConfirmationSuccessMsg(),
					"<b>Actual Result:</b> The confirmation message is displayed that the user's email address has been unsubscribed.",
					"<b>Actual Result:</b> The confirmation message is not displayed that the user's email address has been unsubscribed.");
			Log.message("<br>");

			myaccount = editProfilePage.headers.navigateToMyAccount();
			editProfilePage = myaccount.navigateToEditProfilePage();

			isAddToEmailListChecked = editProfilePage.isAddToEmailListChecked();

			if (isAddToEmailListChecked.equals("No"))
				editProfilePage.checkOnEmailSubscriptionCheckbox();

			// enter confirm email and password values
			editProfilePage.typeOnConfirmEmailField(confirmEmail);
			editProfilePage.typeOnPasswordField(password);

			// click on 'Apply Changes'
			editProfilePage.clickOnProfileApplyChanges();
			Log.message("Cleaned Up");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_032

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the login information of the new customer", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_149(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String phoneNumber = testData.get("PhoneNumber");
		String emailID = testData.get("EmailAddress");
		String confirmEmailID = testData.get("ConfirmEmailAddress");
		String password = testData.get("Password");
		String confirmPassword = testData.get("ConfirmPassword");

		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
				.format(new Date());
		String emailIDList[] = emailID.split("@");
		String confirmEmailIDList[] = confirmEmailID.split("@");

		firstName = firstName + timeStamp;
		emailID = emailIDList[0] + timeStamp + "@" + emailIDList[1];
		confirmEmailID = confirmEmailIDList[0] + timeStamp + "@"
				+ confirmEmailIDList[1];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i=1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message((i++) + ". Clicked on Create Account button in SignIn page");

			// verify fields are blank
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The following fields should blank (FirstName, LastName, PhoneNumber, Email, ConfirmEmail, Password, ConfirmPassword)");
			Log.softAssertThat(createAccountPage.getFirstName().equals(""),
					"<b>Actual Result: </b>FirstName field is blank", "<b>Actual Result: </b>FirstName field is not blank");
			Log.softAssertThat(createAccountPage.getLastName().equals(""),
					"<b>Actual Result: </b>LastName field is blank", "<b>Actual Result: </b>LastName field is not blank");
			Log.softAssertThat(createAccountPage.getPhoneNumber().equals(""),
					"<b>Actual Result: </b>PhoneNumber field is blank",
					"<b>Actual Result: </b>PhoneNumber field is not blank");
			Log.softAssertThat(createAccountPage.getEmailID().equals(""),
					"<b>Actual Result: </b>Email field is blank", "<b>Actual Result: </b>Email field is not blank");
			Log.softAssertThat(createAccountPage.getConfirmationEmailID()
					.equals(""), "<b>Actual Result: </b>ConfirmEmail field is blank",
					"<b>Actual Result: </b>ConfirmEmail field is not blank");
			Log.softAssertThat(createAccountPage.getPassword().equals(""),
					"<b>Actual Result: </b>Password field is blank", "<b>Actual Result: </b>Password field is not blank");
			Log.softAssertThat(createAccountPage.getConfirmPassword()
					.equals(""), "<b>Actual Result: </b>ConfirmPassword field is blank",
					"<b>Actual Result: </b>ConfirmPassword field is not blank",driver);

			// Creating a new account
			createAccountPage.enterFirstName(firstName);
			createAccountPage.enterLastName(lastName);
			createAccountPage.enterPhoneNumber(phoneNumber);
			createAccountPage.enterEmailID(emailID);
			createAccountPage.enterConfirmationEmailID(confirmEmailID);
			createAccountPage.enterPassword(password);
			createAccountPage.enterConfirmPassword(confirmPassword);
			Log.message((i++) + ". Fill the Information");
			// Click Create Account Button
			createAccountPage.clickCreateCreateAccountBtn();
			Log.message((i++) + ". Clicked Create Account Button");

			MyAccountPage myAccountPage = new MyAccountPage(driver);
			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to profile page");

			// verify fields values
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The following fields shoud have the filled values (FirstName, LastName, PhoneNumber, Email");
			Log.softAssertThat(
					profilePage.getFirstNameFieldValue().equals(firstName),
					"<b>Actual Result: </b>FirstName field is same as filled '"+firstName+"'", "<b>Actual Result: </b>FirstName field is not same");
			Log.softAssertThat(
					profilePage.getLastNameFieldValue().equals(lastName),
					"<b>Actual Result: </b>LastName field is same as filled '"+lastName+"'", "<b>Actual Result: </b>LastName field is not same");
			Log.softAssertThat(
					profilePage.getPhoneFieldValue().equals(phoneNumber),
					"<b>Actual Result: </b>PhoneNumber field is same as filled '"+phoneNumber+"'",
					"<b>Actual Result: </b>PhoneNumber field is not same");
			Log.softAssertThat(
					profilePage.getEmailFieldValue().equals(emailID),
					"<b>Actual Result: </b>Email field is same as filled '"+emailID+"'", "<b>Actual Result: </b>Email field is not same",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_149

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Help Content in Address Book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_075(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String helpContentByEmail = "Belk_Customer_Care@belk.com";
		String helpContentByPhone = "1-866-235-5443";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Need Help?' should displayed");
			Log.assertThat(
					addressbook.getHelpContentHeaderTitle().trim()
							.toLowerCase().equals("need help?"),
					"<b>Actual Result: </b>Need Help is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Need Help is not displayed in the Address Validation Form.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Help Content(By Email) should displayed in UI");
			Log.softAssertThat(addressbook.getHelpContentMessage().trim()
					.contains(helpContentByEmail),
					"<b>Actual Result: </b>Help Content(By Email) is displayed in UI.",
					"<b>Actual Result: </b>Help Content(By Email) is not displayed in UI.");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Help Content(By phone) should displayed in UI");
			Log.softAssertThat(addressbook.getHelpContentMessage().trim()
					.contains(helpContentByPhone),
					"<b>Actual Result: </b>Help Content(By Phone) is displayed in UI.",
					"<b>Actual Result: </b>Help Content(By Phone) is not displayed in UI.",driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_075

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Order Detail with valid data from My Account Login page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String Ordernumber = testData.get("InvalidOrderNumber");
		String Orderemail = testData.get("InvalidOrderEmail");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn SigninPage = homePage.headers.navigateToSignIn();
			 Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			SigninPage.signInToViewOrderDetail(Orderemail, Ordernumber);
			Log.message((i++) + ". Entered Order Details in 'Sign In' page", driver);

			OrderHistoryPage orderhistorypage=new OrderHistoryPage(driver).get();
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The Order details page should displayed");
			Log.assertThat((orderhistorypage!=null) && (orderhistorypage.getPageTitle().trim().equals("Order Details")),"<b>Actual Result: </b>The order details page is displayed","<b>Actual Result: </b>The order details page is not displayed",driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_009

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the View details link in Order History page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_162(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i=1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			OrderHistoryPage OrderHistoryPage = myaccount
					.navigateToOrderHistoryPage();

			OrderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Navigated to Order History page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_162

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the user is able to enter the flat number value in the Apartment Number text box of Address not Recognized by USPC modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_096(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i=1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			int totalAddress=addressBookPage.getTotalAddressCount();
			
			for(int j=1;j<=totalAddress;j++){
				addressBookPage.deleteAddressByIndex(0);
			}
			
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled new Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked 'Save' button to Save the Address");
			Log.message("<b>Expected Result: </b>Address should be validated in Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address not Recognized by USPS"),
					"<b>Actual Result: </b>Expected Header is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);

			Log.message("<b>Expected Result: </b>'Additional address' should be pre-selected by default ");
			Log.assertThat(
					addressBookPage
							.isEnterAdditionalAddressRadioButtonSelected(),
					"<b>Actual Result: </b>Enter Additional Address Radio Button is selected by default",
					"<b>Actual Result: </b>Enter Additional Address Radio Button is not selected by default");
			String streetNo = RandomStringUtils.randomNumeric(2);
			addressBookPage.typeStreetNumber(streetNo);
			Log.message((i++) + ". Entered Street Number '" + streetNo
					+ "' in text box", driver);
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Clicked Continue button on USPS popup and the street number is updated");
			addressBookPage.selectUseOriginalAddress();

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Clicked Continue button on USPS popup");
//
//			LinkedHashMap<String, String>  getSavedAddress = addressBookPage.getSavedAddressByIndex(0);
//			Log.message(
//					(i++) + ". Get the address details",
//					driver);

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Clicked Continue button on Address Suggestion",
					driver);

			Log.message("<b>Expected Result: </b>When clicked on the continue button the apartment number is updated in the customer's address book");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			Log.assertThat(
					savedAddress.get("Address1").contains(streetNo),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.event("Clean-up");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_096

	@Test(groups = { "desktop", "mobile" }, description = "Verify clicking on Delete link for the address which is not set with default billing and shipping address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_119(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged in to the application with '" + emailid
					+ "' / '" + password + "'");

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			if (addressbookpage.VerifyAddressAvailability()) {
				Log.message((i++) + ". The saved address is displayed.", driver);

			} else {
				addressbookpage.clickAddNewAddress();
				addressbookpage.fillingAddNewAddressDetails("valid_address7",
						"YES", "YES");
				addressbookpage.ClickOnSave();
				addressbookpage.selectUseOriginalAddress();
				addressbookpage.ClickOnContinueInValidationPopUp();
				Log.message((i++) + ". New address is saved");
			}

			addressbookpage.clickOnEditAddressByIndex(1);

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The delete confirmation popup should displayed");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElements(
							Arrays.asList("lblAddressFormDialogContainer"),
							addressbookpage),
					"<b>Actual Result:</b> The Delete confirmation popup is displayed",
					"<b>Actual Result:</b> The Delete confirmation popup is not displayed",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The delete default address popup should displayed");
			Log.assertThat(
					!(addressbookpage.getTextFromDialogContainerHeader().trim())
							.equals("Delete Default Address"),
					"<b>Actual Result:</b> The delete default address popup is not displayed",
					"<b>Actual Result:</b> The delete default address popup is displayed",
					driver);
			addressbookpage.clickOnCancelInAddNewAddressModal();
			addressbookpage.clickOnDeleteAddressByIndex(0);
			addressbookpage.clickConfirmDeleteAddress();
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_119

	@Test(groups = { "desktop", "mobile" }, description = "Verify Change Address link and Continue button in the 'No Address Found' modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_109(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			if (addressbookpage.VerifyAddressAvailability()) {
				Log.event("The saved address is displayed.");
				addressbookpage.clickOnDeleteAddressByIndex(0);
				addressbookpage.clickConfirmDeleteAddress();
				Log.event((i++) + ".Deleted if any saved address is displayed");
			}
			addressbookpage.clickAddNewAddress();
			addressbookpage.fillingAddNewAddressDetails("no_address_found",
					"YES", "YES");
			addressbookpage.ClickOnSave();
			BrowserActions.nap(3);
			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The 'No Address Found' popup should displayed");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElements(
							Arrays.asList("btnCancelAddressSuggestion"),
							addressbookpage),
					"<b>Actual Result-1:</b> The 'No Address Found' popup is displayed",
					"<b>Actual Result-1:</b> The 'No Address Found' popup is not displayed",
					driver);
			addressbookpage.clickCancelAddressSuggestion();
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> The 'No Address Found' popup should closed");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnCancelAddressSuggestion"),
							addressbookpage),
					"<b>Actual Result-2:</b> The 'No Address Found' popup is closed",
					"<b>Actual Result-2:</b> The 'No Address Found' popup is not closed",
					driver);
			addressbookpage.ClickOnSave();
			addressbookpage.clickCountinueInNoAddressDetailsDialog();
			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> The 'No Address Found' modal should closed and returned to the Address Book page.");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lblAddressFormDialogContainer"),
							addressbookpage),
					"<b>Actual Result-3:</b> The 'No Address Found' modal is closed and returned to the Address Book page.",
					"<b>Actual Result-3:</b> The 'No Address Found' modal is not closed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_109

	@Test(groups = { "desktop", "mobile" }, description = "Verify the address value format that is displayed for the Premises Partial, Street Partial and Multiple scenarios.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_110(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			AddressBookPage addressbookpage = (AddressBookPage) myaccountpage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to Address book page");

			
			int totalAddress=addressbookpage.getTotalAddressCount();
			if(totalAddress>0){
				for(int j=0;j<totalAddress;j++){
					addressbookpage.deleteAddressByIndex(0);
				}
			}
			
			addressbookpage.clickAddNewAddress();
			Log.message((i++) + ".Clicked the add new address button");
			LinkedHashMap<String, String> filledAddress = addressbookpage
					.fillingAddNewAddressDetails("usps_address", "YES", "YES");
			Log.message((i++) + ". Filled the address details");
			addressbookpage.ClickOnSave();
			Log.message((i++) + ". Clicked on the save button");
			List<String> indexes = new ArrayList<String>(filledAddress.keySet());
			String firstAddress = addressbookpage
					.getAddressFromPossibleMatchPopupByIndex(1).trim();
			Log.message((i++) + ". Selected the address from the QAS modal");
			String state = StateUtils.getStateCode(indexes.get(6).replace(
					"select_state_", ""));
			String address1 = indexes.get(3).replace("type_address_", "")
					.replace("Street", "St");
			String city = indexes.get(5).replace("type_city_", "");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The addresses displayed in the dropdown should be displayed as below example: 100 Bechter Loop, Charlotte NC 28117-9352");
			Log.assertThat(
					firstAddress.contains(address1 + ", " + city + " " + state),
					"<b>Actual Result:</b> The displayed address : '"
							+ firstAddress + "' as expected",
					"<b>Actual Result:</b> The displayed address is not as expected",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_110

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify No Address found modal, when system couldn’t recognize the address which is entered by the customer.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_107(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i=1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked Add New Address button", driver);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Filled all the details in the Add new Address modal",
					driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Clicked 'Save' button to Save the Address", driver);
			Log.message("<b>Expected Result: </b>Address should be validated in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address Validation"),
					"<b>Actual Result: </b>Expected Header is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);
			Log.message("<b>Expected Result: </b> U.S. Postal Service Header Message should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage
							.getAddressValidationHeaderMsg()
							.equals("We're sorry. The U.S. Postal Service does not recognize this address. Please choose from the following options:"),
					"<b>Actual Result: </b>Expected Header Msg is displayed in the Address Validation Form",
					"<b>Actual Result: </b>Expected Header Msg is not displayed in the Address Validation Form.",
					driver);

			addressBookPage.clickCancelAddressSuggestion();
			Log.message((i++) + ". Clicked 'Cancel' button on Address Suggestion", driver);

			addressBookPage.clickCancelAddressAddEditForm();
			Log.message((i++) + ". Clicked 'Cancel' button on Add Address Form", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_107

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify set new address as default billing and shipping address from Set As Default screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_120(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		Date dateobj = new Date();
		addressName = addressName + df.format(dateobj).toString();
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ".  Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);
			if (addressBookPage.getTotalAddressCount() <= 0) {
				addressBookPage.clickAddNewAddress();
				addressBookPage.fillingAddNewAddressDetails("valid_address7",
						"YES", "YES");
				addressBookPage.ClickOnSave();
				addressBookPage.selectUseOriginalAddress();
				addressBookPage.ClickOnContinueInValidationPopUp();
				Log.message((i++) + ". New address is saved");
			}
			Log.message("<br>");
			Log.message("<b><u>Verification for billing Address</u></b>");
			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message(
					(i++) + ". Clicked on Edit link in the first additional addressess in Address Book Page");

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Edited additional addressess in Address Book Page",
					driver);

			addressBookPage.clickSaveAddressEditForm();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Saved editted addressess in Address Book Page");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);
			List<String> indexes = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", indexes.get(0));
			newAddress.put("FirstName", indexes.get(1));
			newAddress.put("LastName", indexes.get(2));
			newAddress.put("Address1", indexes.get(3));
			newAddress.put("Address2", indexes.get(4));
			newAddress.put("City", indexes.get(5));
			newAddress.put("State", indexes.get(6));
			newAddress.put("Zipcode", indexes.get(7));
			newAddress.put("PhoneNo", indexes.get(8).replace("-", ""));
			newAddress.put("Defaultshipping", indexes.get(9));
			newAddress.put("Defaultbilling", indexes.get(10));

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The address should be saved in the address book");
			Log.softAssertThat(savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The 'default address' should displayed as the address section header");
			Log.assertThat(
					addressBookPage.getAddressSectionHeader().equals(
							"Default Addresses"),
					"<b>Actual Result: </b>'Default Addressess' is displayed as the Address section header",
					"<b>Actual Result: </b>Incorrect Address section Header name is displayed");

			addressBookPage.verifyDefaultAddressColor(1);
			Log.message((i++) + ". Verified the default address tile color in Address Book Page");

			// Set the data to default value by unchecking default billing
			// address check box
			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on the Edit link from the address");
			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();
			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Unchecked the billing and shipping address");
			addressBookPage.clickSaveAddressEditForm();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			addressBookPage.clickSaveChanges();
			Log.message((i++) + ". Clicked on the save button");
			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Additional Addresses' should be displayed as the Address section header");
			
			Log.assertThat(
					addressBookPage.getAddressSectionHeader().equals(
							"Additional Addresses"),
					"<b>Actual Result: </b>'Additional Addresses' is displayed as the Address section header",
					"<b>Actual Result: </b>Incorrect Address section Header name is displayed",driver);
			
			Log.message("<br>");
			Log.message("<b><u>Verification for Shipping Address</u></b>");
			// Verification for Shipping Address
			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on the Edit link from the address");
			defaultShipping = "Yes";
			defaultBilling = "No";
			newAddress.replace("defaultshipping", defaultShipping);
			newAddress.replace("defaultbilling", defaultBilling);
			addressBookPage.fillingAddNewAddressDetail("valid_address2",
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Edit additional addressess in Address Book Page");

			addressBookPage.clickSaveAddressEditForm();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Saved editted Shipping addressess in Address Book Page");

			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The address should be saved in the address book");
			Log.assertThat(addressBookPage.getTotalAddressCount() > 0,
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Default Addressess' should be displayed as the Address section header");
			Log.assertThat(
					addressBookPage.getAddressSectionHeader().equals(
							"Default Addresses"),
					"<b>Actual Result: </b>'Default Addressess' is displayed as the Address section header",
					"<b>Actual Result: </b>Incorrect Address section Header name is displayed");

			addressBookPage.verifyDefaultAddressColor(1);
			Log.message((i++) + ". Verified the default address tile color in Address Book Page");

			// Set the data to default value
			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on the Edit link from the address");
			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();
			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Unchecked the billing and shipping address");
			addressBookPage.clickSaveAddressEditForm();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Clicked on the save button");
			addressBookPage = new AddressBookPage(driver);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>'Additional Addresses' should be displayed as the Address section header");
			Log.assertThat(
					addressBookPage.getAddressSectionHeader().equals(
							"Additional Addresses"),
					"'Additional Addresses' should be displayed as the Address section header",
					"Incorrect Address section Header name is displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_120

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Address Book link from My Account screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_124(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		List<String> addressBookPageElements = Arrays
				.asList("btnAddNewAddress");
		List<String> addNewAddress = null;
		addNewAddress = Arrays.asList("txtAddressName", "txtFirstName",
				"txtLastName", "txtAddress1", "txtAddress2", "txtCity",
				"txtSelectedState", "txtZipcode", "txtPhone");
		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);
			Log.message("<b>Expected Result: </b>Fields 'AddressName, FirstName, LastName, Address1, Address2, City, State, Zipcode, Phone' in the Address Book Page should be displayed");
			Log.assertThat(
					addressBookPage.elementLayer.verifyPageElements(
							addressBookPageElements, addressBookPage),
					"<b>Actual Result: </b>Fields 'AddressName, FirstName, LastName, Address1, Address2, City, State, Zipcode, Phone' in the Address Book Page is displayed ",
					"<b>Actual Result: </b>Fields in the Address Book Page is not displayed as expected, plz check the event log",
					driver);

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked on AddNewAddress button", driver);
			Log.message("<b>Expected Result: </b>Fields 'AddressName, FirstName, LastName, Address1, Address2, City, State, Zipcode, Phone' in the Add New Address form should be displayed");
			Log.assertThat(
					addressBookPage.elementLayer.verifyPageElements(
							addNewAddress, addressBookPage),
					"<b>Actual Result: </b>Fields 'AddressName, FirstName, LastName, Address1, Address2, City, State, Zipcode, Phone' in the Add New Address form is displayed ",
					"<b>Actual Result: </b>Fields in the Add New Address form not displayed as expected, plz check the event log",
					driver);

			Log.assertThat(
					addressBookPage
							.verifyDefaultShippingAndBillingAddressInAddNewAddress(),
					"8. The Default Shipping and Billing check boxes are displayed",
					"The Default Shipping and Billing check boxes are not displayed as expected, plz check the event log");

			addressBookPage.clickCancelAddressAddEditForm();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_124

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding new address from Address Book screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_125(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page", driver);

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			// Verification for Billing Address
			addressBookPage.clickAddNewAddress();
			Log.message(
					(i++) + ". Click on the Add New Address button in Address Book Page",
					driver);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message(
					(i++) + ". Enter all the values in Add New Address section in Address Book Page",
					driver);

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address", driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address", driver);

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click Continue button on Address Suggestion",
					driver);

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			List<String> index = new ArrayList<String>(newAddress.values());

			newAddress.clear();
			newAddress.put("Title", index.get(0));
			newAddress.put("FirstName", index.get(1));
			newAddress.put("LastName", index.get(2));
			newAddress.put("Address1", index.get(3));
			newAddress.put("Address2", index.get(4));
			newAddress.put("City", index.get(5));
			newAddress.put("State", index.get(6));
			newAddress.put("Zipcode", index.get(7));
			newAddress.put("PhoneNo", index.get(8));
			newAddress.put("Defaultshipping", index.get(9));
			newAddress.put("Defaultbilling", index.get(10));
			Log.message("<b>Expected Result: </b>The Address should be saved to the Address book");
			Log.assertThat(savedAddress.equals(newAddress),
					"<b>Actual Result: </b>The Address is saved to the Address book.",
					"<b>Actual Result: </b>The Address is not saved to the Address book.", driver);
			Log.message("<b>Expected Result: </b>The Address should be displayed as Address Section header");
			Log.assertThat(
					addressBookPage.getAddressSectionHeader().equals(
							"Additional Addresses"),
					"<b>Actual Result: </b>'Additional Addresses' is displayed as the Address section header",
					"<b>Actual Result: </b>Incorrect Address section Header name is displayed");
			Log.message("<b>Expected Result: </b>The back ground color of the default address should be displayed in gray");
			Log.assertThat(addressBookPage.verifyAdditionalAddressColor(1),"<b>Actual Result: </b>The back ground color of the Default address should be gray"
				,"<b>Actual Result: </b>The back ground color of the default address is not displayed in gray",
					driver);

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Deleted the added address", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_125

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Make default payment Method option in Add|Edit Payment Method Modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_137(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment Page");
			int paymentCount = paymentMethodsPage.getTotalPaymentCount();
			if (paymentCount > 0) {
				for (int j = 1; j <= paymentCount - 1; j++) {
					paymentMethodsPage.deletePaymentByIndex(0);
				}
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked Add Payment", driver);

			LinkedHashMap<String, String> cardDetail = new LinkedHashMap<String, String>();
			cardDetail.put("cardType", "Select");
			cardDetail.put("cardOwner", "");
			cardDetail.put("cardNo", "");
			cardDetail.put("cardExpireMonth", "Select Month");
			cardDetail.put("cardExpireYear", "Select Year");
			cardDetail.put("cardDefaultPayment", "No");

			LinkedHashMap<String, String> cardDetailAddForm = paymentMethodsPage
					.getCardDetailsAddForm();
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Card Details should be Blank");
			Log.assertThat(cardDetailAddForm.equals(cardDetail),
					"<b>Actual Result: </b>Card Details are Blank as expected.",
					"<b>Actual Result: </b>Card Default are not Blank as expected.",driver);

			paymentMethodsPage.fillingCardDetails("YES", "card_Visa");
			Log.message((i++) + ".Filled new Payment Details", driver);

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Saved Payment Details");

			LinkedHashMap<String, String> savedPaymentDetails = paymentMethodsPage
					.getSavedPaymentByIndex(0);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Default Payment Option should be same");
			Log.assertThat(
					savedPaymentDetails.get("defaultpayment").equals("Yes"),
					"<b>Actual Result: </b>Default Payment Option is same as expected.",
					"<b>Actual Result: </b>Default Payment Option is not same as expected.",
					driver);

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked Add Payment");

			paymentMethodsPage.fillingCardDetails("NO", "card_Amex");
			Log.message((i++) + ". Filled in Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Saved Payment Details", driver);

			savedPaymentDetails = paymentMethodsPage.getSavedPaymentByIndex(1);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>Default Payment Option should be same.");
			Log.assertThat(
					savedPaymentDetails.get("defaultpayment").equals("No"),
					"<b>Actual Result: </b>Default Payment Option is same as expected.",
					"<b>Actual Result: </b>Default Payment Option is not same as expected.",
					driver);

			Log.event("Test Data Cleanup");

			paymentMethodsPage.deletePaymentByIndex(1);
			Log.event((i++) + ". Deleted Pyment -1");

			paymentMethodsPage.deletePaymentByIndex(0);
			Log.event((i++) + ". Deleted Pyment -2");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_137

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'Change Default' link in Express checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_037(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");



		List<String> BillingShippingAddressForm = null;		
		BillingShippingAddressForm = Arrays.asList("lblAddressNameInSetDefaultBilling", "lblFirstNameInSetDefaultBilling",
				"lblAddressOneInSetDefaultBilling", "lblAddressTwoInSetDefaultBilling", "lblCityNameInSetDefaultBilling", "drpStateInSetDefaultBilling",
				"lblZipcodeInSetDefaultBilling", "lblPhoneInSetDefaultBilling");


		List<String> PaymentForm = null;		
		PaymentForm = Arrays.asList("txtCardOwnerInSetDefaultPayment");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"	+ password + ") in 'Sign In' page");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigate to the Profile page");

			profilePage.clickOnChangeDefaultInBilling();			
			Log.message((i++) + ". Click on Change Default Billing Address");

			// Verify the default billing address
			Log.message("<b>Expected Result 1: </b>Default Billing Address Fields should be available");
			Log.assertThat(profilePage.elementLayer.verifyPageElements(BillingShippingAddressForm, profilePage), 
					"<b>Actual Result 1:"
							+ "</b> Default Billing Address Fields are available", 
							"<b>Actual Result 1:"
									+ "</b> Default Billing Address Fields are not available",driver);

			profilePage.clickOnCancelSetDefaultShippingBillingAddress();
			Log.message((i++) + ". Click on Cancel to close Change Default Billing Address Form");

			profilePage.clickOnChangeDefaultInShipping();			
			Log.message((i++) + ". Click on Change Default Shipping Address");

			// Verify the default billing address
			Log.message("<b>Expected Result 2: </b>Default Shipping Address Fields should be available");
			Log.assertThat(profilePage.elementLayer.verifyPageElements(BillingShippingAddressForm, profilePage), 
					"<b>Actual Result 2:"
							+ "</b> Default Shipping Address Fields are available", 
							"<b>Actual Result 2:"
									+ "</b> Default Shipping Address Fields are not available",driver);

			profilePage.clickOnCancelSetDefaultShippingBillingAddress();
			Log.message((i++) + ". Click on Cancel to close Change Default Shipping Address Form");

			profilePage.clickOnChangeDefaultInPayment();			
			Log.message((i++) + ". Click on Change Default Payment");

			// Verify the default billing address
			Log.message("<b>Expected Result 3: </b>Default Payment Fields should be available");
			Log.assertThat(profilePage.elementLayer.verifyPageElements(PaymentForm, profilePage), 
					"<b>Actual Result 3:"
							+ "</b> Default Payment Fields are available", 
							"<b>Actual Result 3:"
									+ "</b> Default Payment Fields are not available",driver);

			profilePage.clickOnCancelSetDefaultPayment();
			

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_037

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Shipping Address details in Set Default Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_039(String browser) throws Exception 
	{

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		LinkedHashMap<String, String> defaultBillingAddress = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"	+ password + ") in 'Sign In' page");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigate to the Profile page");
			Log.message("<b>Expected Result 1: </b>The express checkout button should be disabled");
			Log.assertThat(!profilePage.isExpressCheckoutCheckboxEnabled(),
					"<b>Actual Result 1:</b>The express checkout button is disabled",
					"<b>Actual Result 1:</b>The express checkout button is not disabled. Check log",
					driver);
			if(profilePage.verifyDefaultPaneAvailabilityByName("shipping")){
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
				}
				AddressBookPage addressbookpage=(AddressBookPage) profilePage.navigateToSection("addressbook");
				addressbookpage.deleteAddressByIndex(0);
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
				}
				profilePage=(ProfilePage) addressbookpage.navigateToSection("profile");
			}
			profilePage.clickOnSetDefaultAddress();		
			Log.message((i++) + ". Click on Set Default Address");
			BrowserActions.nap(5);
			Log.message("<b>Expected Result 2: </b>Resource message should be shown");
			Log.assertThat(profilePage.getFormHelpContent().equals("Select from existing address or add a new default address."),
					"<b>Actual Result 2:</b> Resource message is shown",
					"<b>Actual Result 2:</b> Resource message is not shown",
					driver);

			profilePage.clickOnCancelSetDefaultShippingBillingAddress();
			Log.message((i++) + ". Click on Cancel to close Change Default Shipping Address Form");

			profilePage.clickOnSetDefaultAddress();		
			Log.message((i++) + ". Click on Set Default Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();
			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("addressName", addressName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");
			Log.message("<b>Expected Result 3: </b>Invalid error message should be shown when fields are empty.");
			Log.assertThat(profilePage.getsetDefaultAddressFormErrorMsg().contains("This field is required."),
					"<b>Actual Result 3:</b> Invalid error message is shown when fields are empty.",
					"<b>Actual Result 3:</b> No Invalid error message is shown when fields are empty.",
					driver);

			profilePage.fillingSetDefaultAddressDetails(newAddress);
			Log.message((i++) + ". Filled set default Address");
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");

			profilePage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address");

			profilePage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			defaultBillingAddress = profilePage.getDefaultShippingAddress();

			LinkedHashMap<String, String> defaultAddress=new LinkedHashMap<String, String>();;
			defaultAddress.put("title", defaultBillingAddress.get("title"));
			defaultAddress.put("address1", defaultBillingAddress.get("address"));

			// Verify the default billing address
			Log.message("<b>Expected Result 4: </b> Default Billing Address Fields should be available");
			Log.assertThat(defaultAddress.get("title").equals(newAddress.get("title")), 
					"<b>Actual Result 4a:"
							+ "</b> Title Fields are available", 
							"<b>Actual Result 4a:"
									+ "</b> Title Fields are not available",driver);
			
			Log.assertThat(defaultAddress.get("address1").equals(newAddress.get("address1")), 
					"<b>Actual Result 4b:"
							+ "</b> Address Fields are available", 
							"<b>Actual Result 4b:"
									+ "</b> Address Fields are not available",driver);
			if(Utils.getRunPlatForm()=="mobile"){
				homePage.headers.navigateToMyAccount();
			}
			profilePage.navigateToSection("addressbook");

			AddressBookPage addressBookPage = new AddressBookPage(driver);

			addressBookPage.deleteAddressByIndex(0);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_039
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Payment details in Set Default Payment screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_040(String browser) throws Exception 
	{

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		String cardType = testData.get("CardType");
		String cardOwner = testData.get("CardOwner");
		String cardNumber = testData.get("CardNumber");
		cardNumber = cardNumber.replace("411111111111", "************");
		String cardExpireDate = testData.get("CardExpireDate");

		LinkedHashMap<String, String> defaultPaymentOptions = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"	+ password + ") in 'Sign In' page");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigate to the Profile page");
			Log.message("<b>Expected Result 1: </b>The express checkout button should be disabled");
			Log.assertThat(!profilePage.isExpressCheckoutCheckboxEnabled(),
					"<b>Actual Result 1: </b>The express checkout button is disabled",
					"<b>Actual Result 1: </b>The express checkout button is not disabled. Check log");

			profilePage.clickOnSetAsDefaultPayment();		
			Log.message((i++) + ". Click on set as default payment");

			profilePage.clickOnCancelSetDefaultPayment();
			Log.message((i++) + ". Click on Cancel in set as default payment");

			profilePage.clickOnSetAsDefaultPayment();			
			Log.message((i++) + ". Click on set as default payment");

			profilePage.clickOnSetAsDefaultInSetDefaultPaymentDialog();
			Log.message((i++) + ". Click on set as default payment in set as default payment dialog");
			Log.message("<b>Expected Result 2: </b>Invalid error message should be shown when fields are empty.");
			Log.assertThat(profilePage.getsetDefaultPaymentFormErrorMsg().contains("Please enter your Name as it appears on the card"),
					"<b>Actual Result 2:</b> Invalid error message is shown when fields are empty.",
					"<b>Actual Result 2:</b> No Invalid error message is shown when fields are empty.",
					driver);

			profilePage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled Card Details");
			profilePage.clickOnSetAsDefaultInSetDefaultPaymentDialog();
			Log.message((i++) + ". Click on set as default payment in set as default payment dialog");


			defaultPaymentOptions = profilePage.getDefaultPaymentOption();
			Log.message("<b>Expected Result 3: </b>Default Payment option Details should be displayed");
			// Verify the default payment options details
			Log.softAssertThat(
					defaultPaymentOptions.get("cardtype").equals(cardType),
					"<b>Actual Result 3a: </b>Default Payment option Card Type should be " + cardType,
					"<b>Actual Result 3a: </b>Default Payment option Card Type Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardowner").equals(cardOwner),
					"<b>Actual Result 3b: </b>Default Payment option Card Owner should be " + cardOwner,
					"<b>Actual Result 3b: </b>Default Payment option Card Owner Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardnumber").equals(cardNumber),
					"<b>Actual Result 3c: </b>Default Payment option Card Number should be "
							+ cardNumber,
							"<b>Actual Result 3c: </b>Default Payment option Card Number Mismatched", driver);
			Log.softAssertThat(
					defaultPaymentOptions.get("cardexpire").equals(
							cardExpireDate),
							"<b>Actual Result 3d: </b>Default Payment option Card ExpireDate should be "
									+ cardExpireDate,
									"<b>Actual Result 3d: </b>Default Payment option Card ExpireDate Mismatched", driver);
			if(Utils.getRunPlatForm()=="mobile"){
				myAccountPage=homePage.headers.navigateToMyAccount();
			}
			profilePage.navigateToSection("paymentmethods");
			PaymentMethodsPage paymentMethodPage = new PaymentMethodsPage(driver);
			int paymentCount=paymentMethodPage.getTotalPaymentCount();
			for(int j=1;j<=paymentCount;j++)
				paymentMethodPage.deletePaymentByIndex(0);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_040

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on Cancel button in Set Default Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_048(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page", driver);

			// click on changeDefaultInPayment link
			editProfilePage.clickOnSetAsDefaultPayment();
			Utils.waitForPageLoad(driver, 4);
			Log.message((i++) + ". Clicked on Set As Default Payment.");

			// click 'Cancel' on Set Default Payment Dialog
			editProfilePage.clickOnCancelSetDefaultPayment();
			Log.message((i++) + ". Clicked on Cancel in Set Default Payment Dialog.");

			Log.message("<b>Expected Result: </b>Page should be properly redirected to Home-My Account-Edit Profile screen when clicking on Cancel button.");
			Log.assertThat(
					editProfilePage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnSetAsDefaultInDialogContainer"), editProfilePage) &&
					driver.getCurrentUrl().contains("Account-EditProfile"),
							"<b>Actual Result: </b>Page is properly redirected to Home-My Account-Edit Profile screen when clicking on Cancel button. ",
							"<b>Actual Result: </b>Page is not properly redirected to Home-My Account-Edit Profile screen when clicking on Cancel button.",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_048

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking Send and Cancel button in Forgot Password window", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_155(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		

		String emailid = testData.get("EmailAddress");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signIn = homePage.headers.navigateToWishListAsGuest();
			Log.message((i++) + ". Navigated to 'SignIn' Page from home page By Clicking WishList Link");

			signIn.clikForgotPwd();
			Log.message((i++) + ". Clicked on Forgot password link in SignIn page");

			signIn.enterResetPasswordEmail(emailid);
			Log.message((i++) + ". Entered Reset Password Email ID and clicked Send");
			Log.message("<b>Expected Result 1: </b>A message as Email has successfully sent should be shown when customer clicks on Send button.");
			Log.assertThat(
					signIn.getResetPasswordHeaderContent().contains(
							"New Password Email Sent"),
							"<b>Actual Result 1a: </b>'Reset Password Email Sent' page Header content displayed, when User enter Reset Password Mail ID and click sent.",
							"<b>Actual Result 1a: </b>'Reset Password Email Sent' page Header content not displayed, when User enter Reset Password Mail ID and click sent.",
							driver);

			Log.assertThat(
					signIn.getResetPasswordMsg()
					.contains(
							"Please check your inbox. If <email_user@email.com> is associated with a belk.com account, you will receive your new password shortly."),
							"<b>Actual Result 1b: </b>'Password Recovery Sucess' Message displayed, when User enter Reset Password Mail ID and click sent.",
							"<b>Actual Result 1b: </b>'Password Recovery Sucess' Message not displayed, when User enter Reset Password Mail ID and click sent.",
							driver);

			// click 'close' button in recovery password dialog
			signIn.clickCloseOnResetPasswordRecoveryDialog();

			Log.message((i++) + ". Closed the recovery password dialog.");

			signIn.clikForgotPwd();
			Log.message((i++) + ". Clicked on Forgot password link in SignIn page Again.");

			// click cancel button in recovery password dialog
			signIn.clickCancelOnResetPasswordRecoveryDialog();
			Log.message((i++) + ". Clicked Cancel button on recovery password dialog.");
			Log.message("<b>Expected Result 2: </b>The Forgot Password modal should get closed, when customer clicks on Cancel button.");
			Log.assertThat(
					signIn.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnResetPwdCancel"), signIn),
							"<b>Actual Result 2: </b>The Forgot Password modal closed, when customer clicks on Cancel button.",
							"<b>Actual Result 2: </b>The Forgot Password modal not closed, even the customer clicks on Cancel button.",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_155

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Pagination in Order History page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_163(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");		

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> paginationInOrderHistory = null;		
		paginationInOrderHistory = Arrays.asList("lblPagination");	

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();		
			Log.message((i++) + ". Navigated to Order History page");
			
			Log.message("<b>Expected Result 1: </b>The Maximum no of orders per page should be 5");
			Log.assertThat(OrderHistoryPage.getOrderHistoryDetails().size() == 5
					,"<b>Actual Result 1: </b>The 5 orders is displayed per page"
					,"<b>Actual Result 1: </b>The 5 orders is not displayed per page",driver);

			// Verify the default billing address
			Log.message("<b>Expected Result 2: </b>Default Payment Fields should be available");
			Log.assertThat(OrderHistoryPage.elementLayer.verifyPageElements(paginationInOrderHistory, OrderHistoryPage), 
					"<b>Actual Result 2: </b>Default Payment Fields are available", 
							"<b>Actual Result 2: </b>Default Payment Fields are not available",driver);

			OrderHistoryPage.clickOnPaginationByPageNumber(1);
			OrderHistoryPage.clickOnPaginationByPageNumber(2);			

			OrderHistoryPage.clickOnNextPageSymbol();
			OrderHistoryPage.clickOnPreviousPageSymbol();
			OrderHistoryPage.clickOnFirstPageSymbol();
			OrderHistoryPage.clickOnLastPageSymbol();		

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_163

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Make default payment Method option in Add|Edit Payment Method Modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_139(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);		

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigate to Payment Page");

			int totalPayment=paymentMethodsPage.getTotalPaymentCount();

			if(totalPayment>0){
				for (int j = 1; j <= totalPayment; j++) {
					paymentMethodsPage.deletePaymentByIndex(0);
				}
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Click Add Payment");

			LinkedHashMap<String, String> cardDetail = new LinkedHashMap<String, String>();
			cardDetail.put("cardType", "Select");
			cardDetail.put("cardOwner", "");
			cardDetail.put("cardNo", "");
			cardDetail.put("cardExpireMonth", "Select Month");
			cardDetail.put("cardExpireYear", "Select Year");
			cardDetail.put("cardDefaultPayment", "No");			

			LinkedHashMap<String, String> cardDetailAddForm = paymentMethodsPage.getCardDetailsAddForm();
			Log.message("<b>Expected Result 1: </b>Card Details should be blank");
			Log.assertThat(cardDetailAddForm.equals(cardDetail), 
					"<b>Actual Result 1: </b>Card Details are Blank as expected.", 
					"<b>Actual Result 1: </b>Card Default are not Blank as expected.",driver);

			paymentMethodsPage.fillingCardDetails("YES","card_Visa");
			Log.message((i++) + ". Fill in Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Save Payment Details");

			LinkedHashMap<String, String> savedPaymentDetails = paymentMethodsPage.getSavedPaymentByIndex(0);
			Log.message("<b>Expected Result 2: </b>Default Payment Option should be same as expected.");
			Log.assertThat(
					savedPaymentDetails.get("defaultpayment").equals("Yes"),
					"<b>Actual Result 2: </b>Default Payment Option is same as expected.",
					"<b>Actual Result 2: </b>Default Payment Option is not same as expected.",
					driver);

			paymentMethodsPage.clickOnDeletePaymentByIndex(0);
			Log.message((i++) + ". Click Delete in Saved Payment Details");
			Log.message("<b>Expected Result 3: </b>Message should be displayed as expected.");
			Log.assertThat(
					paymentMethodsPage.getMessageInDeletePaymentModal().equals("You're about to delete this payment method. Are you sure you want to proceed?")
					,"<b>Actual Result 3: </b>Message is displayed as expected."
					,"<b>Actual Result 3: </b>Message is not displayed as expected.",
					driver);

			paymentMethodsPage.clickCancelInDeletePaymentModal();
			Log.message((i++) + ". Click Cancel in Delete Payment Details Modal");

			paymentMethodsPage = new PaymentMethodsPage(driver);

			paymentMethodsPage.deletePaymentByIndex(0);
			Log.message((i++) + ". Delete Payment details");

			paymentMethodsPage = new PaymentMethodsPage(driver);
			Log.message("<b>Expected Result 4: </b>No address should be displayed after clicking on delete button");
			Log.assertThat(paymentMethodsPage.getSavedPayment().isEmpty()
					,"<b>Actual Result 4: </b>No address displayed after clicking on delete button"
					,"<b>Actual Result 4: </b>Address is displayed even after clicking on delete button",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_139
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that Select from list of possible matches section is displayed \"Address Verification\" modal, when system finds more possible address to reach customer.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_104(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");
			
			int totalAddress=addressBookPage.getTotalAddressCount();
			if(totalAddress>0){
				for(int j=0;j<totalAddress;j++){
					addressBookPage.deleteAddressByIndex(0);
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
			Log.message((i++) + ". Fill in Address Details");
			addressBookPage.ClickOnSave();
			Log.message((i++) + ". Clicked on save");
			// Verify Header
			Log.message("<b>Expected Result 1: </b>Expected Header should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address not Recognized by USPS"),
							"<b>Actual Result 1: </b>Expected Header is displayed in the Address Validation Form",
							"<b>Actual Result 1: </b>Expected Header is not displayed in the Address Validation Form.",
							driver);
			Log.message("<b>Expected Result 2: </b>Address Verification modal should display Select from list of possible matches with a drop down list of available addresses");
			Log.assertThat(addressBookPage.verifyAddressDropdownInDialog() &&
					addressBookPage.getTextFromDropdown().contains("list of possible matches"),
							"<b>Actual Result 2: </b>Select from list of possible matches with a drop down list is displayed in Address Verification modal",
							"<b>Actual Result 2: </b>Select from list of possible matches with a drop down list is not displayed in Address Verification modal",
							driver);
			addressBookPage.clickAddressDropown();
			Log.message((i++) + ". Clicked Address Dropdown");
			addressBookPage.selectAddressByIndex(1);
			String selectedAddress=addressBookPage.getSelectedAddressFromUSPC();
			int number = 0;
			if(selectedAddress.contains("even")){
				number=Integer.parseInt(selectedAddress.split("\\.\\.\\.")[0].trim());
				number=number+2;
				addressBookPage.typeStreetNumber(Integer.toString(number));
			}
			Log.message((i++) + ". Select Address from dropdown by index");
			addressBookPage.clickContinueAddressSuggestion();
			addressBookPage.selectUseSuggestedAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Clicked Continue In Popup");
			
			LinkedHashMap<String, String>savedAddress=addressBookPage.getSavedAddressByIndex(0);
			String selectedZipcode=selectedAddress.split("\\[even\\]")[1];
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The selected address should saved properly");
			Log.assertThat(savedAddress.get("Address1").contains(Integer.toString(number)) && selectedZipcode.equals(savedAddress.get("Zipcode")), "<b>Actual Result: </b>The selected address is saved properly", "<b>Actual Result: </b>The selected address is not saved",driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_104
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the user is able to select \"Use Original address\" option in \"Address Verification\" modal.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_105(String browser) throws Exception {

			// ** Loading the test data from excel using the test case id */
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String emailid = testData.get("EmailAddress");
			String password = testData.get("Password");
			String firstName = testData.get("FirstName");
			String lastName = testData.get("LastName");
			String addressName = testData.get("Address");
			String addressLine1 = testData.get("AddressLine1");
			String addressLine2 = testData.get("AddressLine2");
			String city = testData.get("City");
			String state = testData.get("State");
			String zipCode = testData.get("ZipCode");
			String phone = testData.get("PhoneNumber");
			String defaultShipping = testData.get("DefaultShipping");
			String defaultBilling = testData.get("DefaultBilling");

			// Get the web driver instance
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				int i = 1;
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message((i++) + ". Navigated to 'Belk' Home Page!");

				SignIn signinPage = homePage.headers.navigateToSignIn();
				Log.message(
						(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

				MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
						password);
				Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
						+ password + ") in 'Sign In' page");

				AddressBookPage addressBookPage = myAccountPage
						.navigateToAddressBook();
				Log.message((i++) + ". Navigated to Address Book Page");

				int totalAddress=addressBookPage.getTotalAddressCount();
				if(totalAddress>0){
					for(int j=0;j<totalAddress;j++){
						addressBookPage.deleteAddressByIndex(0);
					}
				}

				addressBookPage.clickAddNewAddress();
				Log.message((i++) + ". Click Add New Address");

				LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

				newAddress.put("title", addressName);
				newAddress.put("firstname", firstName);
				newAddress.put("lastname", lastName);
				newAddress.put("address1", addressLine1);
				newAddress.put("address2", addressLine2);
				newAddress.put("city", city);
				newAddress.put("state", state);
				newAddress.put("zipcode", zipCode);
				newAddress.put("phone", phone);
				newAddress.put("defaultshipping", defaultShipping);
				newAddress.put("defaultbilling", defaultBilling);

				addressBookPage.fillingAddNewAddressDetails(newAddress, "No", "No");
				Log.message((i++) + ". Fill in Address Details");
				addressBookPage.ClickOnSave();
				Log.message((i++) + ". Clicked on save");
				// Verify Header
				Log.message("<b>Expected Result 1: </b>Expected Header should be displayed in the Address Validation Form");
				Log.assertThat(
						addressBookPage.getAddressValidationHeader().equals(
								"Address not Recognized by USPS"),
								"<b>Actual Result 1: </b>Expected Header is displayed in the Address Validation Form",
								"<b>Actual Result 1: </b>Expected Header is not displayed in the Address Validation Form.",
								driver);
				Log.message("<b>Expected Result 2: </b>Original Address Section should be shown with the original address entered");
				Log.assertThat(
						addressBookPage.elementLayer.verifyPageElements(Arrays.asList("rbUseOriginalAddress","addressInAddNewAddressPopup"), addressBookPage),
								"<b>Actual Result 2: </b>Original Address Section is shown with the original address entered",
								"<b>Actual Result 2: </b>Original Address Section is not shown with the original address entered",
								driver);
				
				
				addressBookPage.selectUseOriginalAddress();
				Log.message((i++) + ". Choose Original Address In Popup");
				addressBookPage.clickContinueAddressSuggestion();
				Log.message((i++) + ". Clicked Continue In Popup");
				Log.message("<b>Expected Result 3: </b>The address should be saved in the address book.");
				Log.assertThat(
						addressBookPage.elementLayer.verifyPageListElements(Arrays.asList("lstAddressSpan"), addressBookPage),
								"<b>Actual Result 3: </b>The address is saved in the address book.",
								"<b>Actual Result 3: </b>The address is not saved in the address book.",
								driver);
				Log.testCaseResult();

			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally

		}// TC_BELK_MYACCOUNT_105

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Billing Address in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_175(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String addressLine1 = testData.get("AddressLine1");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount
					.navigateToOrderHistoryPage();

			Log.message((i++) + ". Navigated to Order History page");
			orderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Clicked on view details");

			LinkedHashMap<String, String> billingAddressDetails = orderHistoryPage
					.getBillingAddressInOrderDetails();
			Log.message("<b>Expected Result 1: </b>Billing Address details should be displayed in order history details page");
			Log.softAssertThat(
					billingAddressDetails.get("name").contains(firstName),
					"<b>Actual Result 1a: </b>firstName Field is same as Ordered User firstName.",
					"<b>Actual Result 1a: </b>firstName Field is not same as Ordered User firstName.");

			Log.softAssertThat(
					billingAddressDetails.get("address").contains(addressLine1),
					"<b>Actual Result 1b: </b>address1 Field is same as Ordered User address1.",
					"<b>Actual Result 1b: </b>address1 Field is not same as Ordered User address1.");

			Log.softAssertThat(
					billingAddressDetails.get("city").contains(city),
					"<b>Actual Result 1c: </b>city Field is same as Ordered User city.",
					"<b>Actual Result 1c: </b>city Field is not same as Ordered User city.");

			Log.softAssertThat(
					billingAddressDetails.get("state").contains(state),
					"<b>Actual Result 1d: </b>state Field is same as Ordered User state.",
					"<b>Actual Result 1d: </b>state Field is not same as Ordered User state.");

			Log.softAssertThat(
					billingAddressDetails.get("zipcode").contains(zipCode),
					"<b>Actual Result 1e: </b>zipcode Field is same as Ordered User zipcode.",
					"<b>Actual Result 1e: </b>zipcode Field is not same as Ordered User zipcode.");

			Log.softAssertThat(billingAddressDetails.get("phonenumber")
					.contains(phone),
					"<b>Actual Result 1f: </b>phonenumber Field is same as Ordered User phonenumber.",
					"<b>Actual Result 1f: </b>phonenumber Field is not same as Ordered User phonenumber.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_175

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Billing Address in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_177(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount
					.navigateToOrderHistoryPage();

			Log.message((i++) + ". Navigated to Order History page");
			orderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Clicked on view details");

			LinkedHashMap<String, String> billingAddressDetails = orderHistoryPage
					.getPaymentMethodInOrderDetails();
			Log.message("<b>Expected Result 1: </b>payment method details should be displayed in Order History Details page");
			Log.softAssertThat(
					billingAddressDetails.get("cardtype").equals("Belk Reward Credit Card"),
					"<b>Actual Result 1a: </b>cardtype Field is same as Ordered User cardtype.",
					"<b>Actual Result 1a: </b>cardtype Field is not same as Ordered User cardtype.");

			Log.softAssertThat(billingAddressDetails.get("nameoncard")
					.contains("karthik"),
					"<b>Actual Result 1b: </b>nameoncard Field is same as Ordered User nameoncard.",
					"<b>Actual Result 1b: </b>nameoncard Field is not same as Ordered User nameoncard.");

			Log.softAssertThat(billingAddressDetails.get("cardNumber")
					.contains("5072"),
					"<b>Actual Result 1c: </b>cardNumber Field is same as Ordered User cardNumber.",
					"<b>Actual Result 1c: </b>cardNumber Field is not same as Ordered User cardNumber.");

			Log.softAssertThat(
					billingAddressDetails.get("purchaseAmount").contains(
							"$66.65"),
							"<b>Actual Result 1d: </b>purchaseAmount Field is same as Ordered User purchaseAmount.",
					"<b>Actual Result 1d: </b>purchaseAmount Field is not same as Ordered User purchaseAmount.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_177

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Order Total in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_179(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount
					.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to Order History page");
			orderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Clicked on view details");

			LinkedHashMap<String, String> billingAddressDetails = orderHistoryPage
					.getPaymentDetailsInOrderHistory();
			
			Log.message("<b>Expected Result 1: </b>System should display Order Total which is charged to the customer.");
			Log.softAssertThat(
					billingAddressDetails.get("merchandiseTotal").contains(
							"52.00"),
							"<b>Actual Result 1a: </b>merchandiseTotal Field is same as Ordered User merchandiseTotal.",
					"<b>Actual Result 1a: </b>merchandiseTotal Field is not same as Ordered User merchandiseTotal.");

			Log.softAssertThat(
					billingAddressDetails.get("shipping").contains("9.50"),
					"<b>Actual Result 1b: </b>shipping Field is same as Ordered User shipping.",
					"<b>Actual Result 1b: </b>shipping Field is not same as Ordered User shipping.");

			Log.softAssertThat(
					billingAddressDetails.get("salesTax").contains("$5.15"),
					"<b>Actual Result 1c: </b>salesTax Field is same as Ordered User salesTax.",
					"<b>Actual Result 1c: </b>salesTax Field is not same as Ordered User salesTax.");

			Log.softAssertThat(billingAddressDetails.get("orderTotal")
					.contains("$66.65"),
					"<b>Actual Result 1d: </b>orderTotal Field is same as Ordered User orderTotal.",
					"<b>Actual Result 1d: </b>orderTotal Field is not same as Ordered User orderTotal.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_179

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Shipment method heading for Ship To Home in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_183(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount
					.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to Order History page");
			orderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Clicked on View Details");
			
			Log.message("<b>Expected Result 1: </b>Shipment Header should be displayed in UI.");
			Log.assertThat(
					orderHistoryPage.getShipmentNumberHeader().contains(
							"Shipment #"),
							"<b>Actual Result 1: </b>Shipment Header is displayed in UI.",
					"<b>Actual Result 1: </b>Shipment Header is not displayed in UI.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_183

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Line item subtotals in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_191(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount
					.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to Order History page");
			orderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Clicked on View Details");
			Log.message("<b>Expected Result 1: </b>Each Line Item should display quantity, price and subtotal of the ordered item");
			Log.assertThat(orderHistoryPage.elementLayer.verifyPageListElements(Arrays.asList("lstOrderItemQuantity","lstOrderItemPrice","lstOrderItemSubTotal"), orderHistoryPage),
					"<b>Actual Result 1:</b>quantity, price and subtotal of the ordered item is displayed",
					"<b>Actual Result 1:</b>quantity, price and subtotal of the ordered item is not displayed",
					driver);
			int qty = Integer.parseInt(orderHistoryPage.getItemQuantity(1));
			double price = Double.parseDouble(orderHistoryPage.getItemPrice(1).replace("$", ""));
			double qtyXprice = qty * price;
			double subtotal = Double.parseDouble(orderHistoryPage.getItemSubTotal(1).split("$")[1]);
			Log.message("<b>Expected Result 2: </b>Subtotal should be properly calculated");
			Log.assertThat(Double.compare(qtyXprice, subtotal)==0,
					"<b>Actual Result 2:</b>Subtotal properly calculated",
					"<b>Actual Result 2:</b>Subtotal is not properly calculated",
					driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_191

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting existing shipping address from Saved Address dropdown", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_043(String browser) throws Exception 
	{

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String addressLine1 = testData.get("AddressLine1");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"	+ password + ") in 'Sign In' page");

			myAccountPage.navigateToSection("profile");

			ProfilePage profilePage = new ProfilePage(driver);
			Log.message((i++) + ". Navigate to the Profile page");


			if(profilePage.verifyDefaultPaneAvailabilityByName("shipping")){
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to the My Account page");
				}
				AddressBookPage addressbookpage= (AddressBookPage) profilePage.navigateToSection("addressbook");
				Log.event("Address book page is displayed");
				addressbookpage.deleteAddressByIndex(0);
				if(addressbookpage.getTotalAddressCount()<=0){
					addressbookpage.clickAddNewAddress();
					
					addressbookpage.fillingAddNewAddressDetails("valid_address6", "NO", "NO");
					addressbookpage.ClickOnSave();
					addressbookpage.selectUseOriginalAddress();
					addressbookpage.ClickOnContinueInValidationPopUp();
					Log.event("Address added");
				}
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to the My Account page");
				}
				profilePage=(ProfilePage) addressbookpage.navigateToSection("profile");
				Log.event("Navigated to Edit profile page");
			}else{
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to the My Account page");
				}
				AddressBookPage addressbookpage= (AddressBookPage) profilePage.navigateToSection("addressbook");
				Log.event("Address book page is displayed");
				if(addressbookpage.getTotalAddressCount()<=0){
					addressbookpage.clickAddNewAddress();
					addressbookpage.fillingAddNewAddressDetails("valid_address6", "NO", "NO");
					addressbookpage.ClickOnSave();
					addressbookpage.selectUseOriginalAddress();
					addressbookpage.ClickOnContinueInValidationPopUp();
					Log.event("Address added");
					BrowserActions.nap(3);
				}
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to the My Account page");
				}
				profilePage=(ProfilePage) addressbookpage.navigateToSection("profile");
				Log.event("Navigated to Edit profile page");	
			}

			profilePage.clickOnSetAsDefaultShipping();		
			Log.message((i++) + ". Click on Change Default Shipping Address");

			profilePage.selectSavedAddressByIndex(1);		
			Log.message((i++) + ". Click on Change Default Shipping Address");

			String selectedAddress = profilePage.getselectedAddressFromSavedAddressDropdown();
			Log.message("<b>Expected Result 1: </b>Address Suggestion should have the Details of Saved Address.");
			Log.assertThat(selectedAddress.contains(addressLine1) && selectedAddress.contains(zipCode),
					"<b>Actual Result 1:</b> Address Suggestion has Details of Saved Address.",
					"<b>Actual Result 1:</b> Address Suggestion doesn't has Details of Saved Address.",
					driver);

			LinkedHashMap<String, String> defaultBillingDetails = profilePage.getSetDefaultBillingAddressDialogContainer();
			Log.message("<b>Expected Result 2: </b>Default Billing Address Container should have the same address as that chosen from Address Suggestion.");
			Log.assertThat(defaultBillingDetails.get("address1").equals(addressLine1) && 
					defaultBillingDetails.get("city").equals(city) &&
					defaultBillingDetails.get("state").equals(state) &&
					defaultBillingDetails.get("zipcode").equals(zipCode) && 
					defaultBillingDetails.get("phonenumber").equals(phone),

					"<b>Actual Result 2:</b> Default Billing Address Container has same address as that chosen from Address Suggestion.",
					"<b>Actual Result 2:</b> Default Billing Address Container doen't has same address as that chosen from Address Suggestion.",
					driver);			

			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");

			profilePage.selectUseOriginalAddress();

			profilePage.clickContinueAddressSuggestionInAddressValidation();

			defaultBillingDetails = profilePage.getDefaultShippingAddress();
			Log.message("<b>Expected Result 3: </b>Default Shipping Address should be Shown Correctly.");
			Log.assertThat(defaultBillingDetails.get("address").equals(addressLine1) && 
					defaultBillingDetails.get("city").equals(city) &&
					defaultBillingDetails.get("state").equals(StateUtils.getStateCode(state)) &&
					defaultBillingDetails.get("zipcode").equals(zipCode) && 
					defaultBillingDetails.get("phonenumber").equals(phone),

					"<b>Actual Result 3:</b> Default Shipping Address is Shown Correctly.",
					"<b>Actual Result 3:</b> Default Shipping Address is not Shown Correctly.",
					driver);	
			if(Utils.getRunPlatForm()=="mobile"){
				myAccountPage=homePage.headers.navigateToMyAccount();
			}
			profilePage.navigateToSection("addressbook");

			AddressBookPage addressBookPage = new AddressBookPage(driver);

			addressBookPage.clickOnEditAddressByIndex(1);
			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();

			addressBookPage.clickOnSaveInAddNewAddressModalAfterEdit();

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_043

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the content in Address book page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_059(String browser) throws Exception 
	{

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,	password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"	+ password + ") in 'Sign In' page");

			myAccountPage.navigateToSection("profile");

			ProfilePage profilePage = new ProfilePage(driver);

			Log.message((i++) + ". Navigate to the Profile page");

			if(Utils.getRunPlatForm()=="mobile"){
				myAccountPage=homePage.headers.navigateToMyAccount();
				Log.message((i++) + ". Navigate to the My Account page");
			}

			AddressBookPage addressBookPage = (AddressBookPage) profilePage.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b>The saved address should be displayed");
			Log.assertThat(addressBookPage.elementLayer.verifyPageListElements(Arrays.asList("lstSavedAddress"), addressBookPage),
					"<b>Actual Result 1: </b>The saved address is displayed", 
					"<b>Actual Result 1: </b>The saved address is not displayed");
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b>The 'Set as default shipping' radio button should be displayed");
			Log.assertThat(addressBookPage.elementLayer.verifyPageElements(Arrays.asList("rdoMakeDefaultShippingForFirstAddress"), addressBookPage), 
					"<b>Actual Result 2: </b>The 'Set as default shipping' radio button is displayed", 
					"<b>Actual Result 2: </b>The 'Set as default shipping' radio button is not displayed",driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b>The 'Set as default billing' radio button should be displayed");
			Log.assertThat(addressBookPage.elementLayer.verifyPageElements(Arrays.asList("rdoMakeDefaultBillingForFirstAddress"), addressBookPage), 
					"<b>Actual Result 3: </b>The 'Set as default billing' radio button is displayed", 
					"<b>Actual Result 3: </b>The 'Set as default billing' radio button is not displayed",driver);			
			Log.message("<br>");
			addressBookPage.clickOnEditAddressByIndex(1);		
			Log.message((i++) + ". Click on Edit Address");

			addressBookPage.CheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Choose Default billing Address radio button");
			addressBookPage.clickOnSaveInAddNewAddressModalAfterEdit();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click on save in Add New Address");
			BrowserActions.nap(3);
			Log.message("<br>");
			Log.message("<b>Expected Result 4: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result 4: </b>The Default Shipping Address is checked",
					"<b>Actual Result 4: </b>The Default Shipping Address is not checked as expected, plz check the event log",driver);


			Log.message("<br>");
			addressBookPage.clickOnEditAddressByIndex(1);		
			Log.message((i++) + ". Click on Edit Address");

			addressBookPage.checkDefaultShippingAddressInAddNewAddress();
			Log.message((i++) + ". Choose Default Shipping Address radio button");
			addressBookPage.clickOnSaveInAddNewAddressModalAfterEdit();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click on save in Add New Address");
			BrowserActions.nap(5);
			Log.message("<br>");
			Log.message("<b>Expected Result 5: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultShippingAddressByIndexChecked(0),
					"<b>Actual Result 5: </b>The Default Shipping Address is checked",
					"<b>Actual Result 5: </b>The Default Shipping Address is not checked as expected, plz check the event log");


			addressBookPage.clickOnEditAddressByIndex(1);
			addressBookPage.unCheckDefaultShippingAddressInAddNewAddress();
			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();

			addressBookPage.clickOnSaveInAddNewAddressModalAfterEdit();
			addressBookPage.selectUseOriginalAddress();
			addressBookPage.clickContinueAddressSuggestion();
			addressBookPage.clickSaveChanges();
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_059

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the content in 'Address not Recognized by USPC' modal, when street no entered is not correct.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_101(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);
			newAddress.put("defaultshipping", defaultShipping);
			newAddress.put("defaultbilling", defaultBilling);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address");
			Log.message("<b>Expected Result 1: </b>Expected Header should be displayed in the Address Validation Form");
			Log.assertThat(	addressBookPage.getAddressValidationHeader().equals(
					"Address not Recognized by USPS"),
					"<b>Actual Result 1: </b>Expected Header is displayed in the Address Validation Form",
					"<b>Actual Result 1: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);

			addressBookPage.typeStreetNumber("77");

			Log.message((i++) + ". Customer is able to enter the 'Street number' in the text box of 'Enter additional address details'");
			Log.message("<b>Expected Result 2: </b>The 'Select from list of possible matches' should be displayed with list of available address which customer can select.");
			Log.assertThat(addressBookPage.verifyAddressDropdownInDialog(),
					"<b>Actual Result 2: </b>The 'Select from list of possible matches' is displayed with list of available address which customer can select",
					"<b>Actual Result 2: </b>The 'Select from list of possible matches' is not displayed with list of available address which customer can select",driver);
			Log.message("<b>Expected Result 3: </b>'Use original address as entered' should be displayed with the original address which is entered by the customer.");
			String originalAddress = addressBookPage
					.getOriginalAddressInAddressValidation();
			Log.assertThat(
					addressBookPage.elementLayer.verifyPageElements(
							Arrays.asList("rbUseOriginalAddress"),
							addressBookPage)
							&& originalAddress.contains(addressLine1),
							"<b>Actual Result 3: </b>'Use original address as entered' is displayed with the original address '"
									+ originalAddress + "' entered by the customer.",
									"<b>Actual Result 3: </b>'Use original address as entered' is not displayed with the original address '"
											+ originalAddress + "' entered by the customer",driver);
			Log.message("<b>Expected Result 4: </b>Customer should be able to select one radio button at a time");
			Log.assertThat(addressBookPage.isEnterAdditionalAddressRadioButtonSelected()
					, "<b>Actual Result 4: </b>Customer is able to select one radio button at a time"
					, "<b>Actual Result 4: </b>Customer is not able to select one radio button at a time",driver);	

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Selected 'Use Original Address' Radio Button");

			addressBookPage.clickContinueAddressSuggestion();			
			Log.message((i++) + ". Clicked Continue button on Address not found popup");
			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage.getSavedAddressByIndex(0);		
			Log.message("<b>Expected Result 5: </b>When the customer clicks on the continue button then the corresponding address gets saved in the customer's address book");
			Log.assertThat(savedAddress.get("Address1").equals(addressLine1),
					"<b>Actual Result 5: </b>Clicking on continue button then the corresponding address gets saved in the customer's address book",
					"<b>Actual Result 5: </b>Clicking on continue button then the corresponding address is not saved in the customer's address book", driver);

			addressBookPage.deleteAddressByIndex(0);
			

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_101

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting billing address from Saved Address dropdown in Delete Default Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_114(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");	
			Log.message("<b>Expected Result 1: </b>The Default Billing Address Should be checked");
			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result 1: </b>The Default Billing Address is checked",
					"<b>Actual Result 1: </b>The Default Billing Address is not checked as expected, plz check the event log");
			Log.message("<b>Expected Result 2: </b>The Default Shipping Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultShippingAddressByIndexChecked(1),
					"<b>Actual Result 2: </b>The Default Shipping Address is checked",
					"<b>Actual Result 2: </b>The Default Shipping Address is not checked as expected, plz check the event log");

			addressBookPage.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Click Edit Address of First Address");	

			addressBookPage.unCheckDefaultBillingAddressInAddNewAddress();
			Log.message((i++) + ". Uncheck Default Billing Address CheckBox in Edit Address Form");	

			addressBookPage.selectSuggestedAddressByIndex(1);
			Log.message((i++) + ". Chosse 1 st Address from Address Suggestion Drop Down");	

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("addressName", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			LinkedHashMap<String, String> CheckAddress = addressBookPage.getAddressDetailsFromAddAddressSection();
			Log.message("<b>Expected Result 3: </b>Address Details should Match with the Address Chose from Address Suggestion Drop Down");
			Log.assertThat(CheckAddress.equals(newAddress), 
					"<b>Actual Result 3: </b>Address Details Match with the Address Chose from Address Suggestion Drop Down", 
					"<b>Actual Result 3: </b>Address Details doen't Match with the Address Chose from Address Suggestion Drop Down");

			addressBookPage.clickOnSetAsDefaultInAddNewAddressModal();
			Log.message((i++) + ". Save Address Details By Clicking Set Default Button");

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". Chosse Use Original Address Radio Button");

			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click Continue Address Suggestion Button");

			addressBookPage = new AddressBookPage(driver);
			Log.message("<b>Expected Result 4: </b>Default Address should Remains Unchanged");
			Log.assertThat(addressBookPage.getTotalAddressCount() == 2,
					"<b>Actual Result 4: </b>Default Address Remains Unchanged",
					"<b>Actual Result 4: </b>Default Address is changed");
			Log.message("<b>Expected Result 5: </b>The Default Billing Address should be checked");
			Log.assertThat(
					addressBookPage.isDefaultBillingAddressByIndexChecked(0),
					"<b>Actual Result 5: </b>The Default Billing Address is checked",
					"<b>Actual Result 5: </b>The Default Billing Address is not checked as expected, plz check the event log");

			addressBookPage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Click Delete Address", driver);

			addressBookPage.clickOnCancelInAddNewAddressModal();
			Log.message((i++) + ". Click Cancel Delete Address");
			Log.message("<b>Expected Result 6: </b>Default Address should Remains Unchanged");
			Log.assertThat(addressBookPage.getTotalAddressCount()== 2,
					"<b>Actual Result 6: </b>Default Address Remains Unchanged",
					"<b>Actual Result 6: </b>Default Address is changed");


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_114

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the confirmation message if the customer successfully cancels the order", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_193(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			//signinPage.enterEmailID(emailid);
			//signinPage.enterPassword(password);
			//signinPage.clickBtnSignIn();

			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();	

			Log.message((i++) + ". Navigated to Order History page");	

			OrderHistoryPage.clickOnViewDetailsByIndex(1);								
			Log.message((i++) + ". Clicked on View Details");
			OrderHistoryPage.clickOnViewDetailsByIndex(1);								
			Log.message((i++) + ". Clicked on Cancel Order By Index");
			OrderHistoryPage.clickOnYesInOrderCancelPopUp();
			Log.message((i++) + ". Clicked on yes in order cancel popup");
			
			//NEED TO EXECUTE
			Log.message("<b>Expected Result 1: </b>Confirmation message should be displayed if the customer successfully cancels the order.");
			Log.assertThat(false, 
					"<b>Actual Result 1: </b>Confirmation message displayed", 
					"<b>Actual Result 1: </b>Confirmation message not displayed",driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_193

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Shipment to a Registry Address in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_187(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			//signinPage.enterEmailID(emailid);
			//signinPage.enterPassword(password);
			//signinPage.clickBtnSignIn();

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();	

			Log.message((i++) + ". Navigated to Order History page");						
			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Clicked On View Details");
			
			//need to execute
			Log.message("<b>Expected Result 1: </b>The Shippment Address should be masked and should display the Registry icon.");
			Log.assertThat(false, 
					"<b>Actual Result 1: </b>The Shippment Address is masked and Registry icon is displayed.", 
					"<b>Actual Result 1: </b>The Shippment Address is not masked and Registry icon is not displayed.",driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_187

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Breadcrumb in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_173(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			//signinPage.enterEmailID(emailid);
			//signinPage.enterPassword(password);
			//signinPage.clickBtnSignIn();

			

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to Order History page", driver);
			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Clicked On View Details");
						

			List<String> txtInBreadCrumb = signinPage.getTextInBreadcrumb();

			String OrderNumber = OrderHistoryPage.getTextFromOrderNumber();
			Log.message("<b>Expected Result 1: </b>The breadcrumb should displayed as expected - 'Order Number'");
			Log.assertThat(
					txtInBreadCrumb.get(2).equals(OrderNumber),
					"<b>Actual Result 1: </b>The breadcrumb is displayed as expected - 'Order Number'",
					"<b>Actual Result 1: </b>The breadcrumb is not displayed as expected - 'Order Number' and the actual is : '"
							+ txtInBreadCrumb.get(2) + "'");	

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_173
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the Back to Shopping and Back to Order History links in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_167(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");                

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			
			SearchResultPage searchresultpage=homePage.searchProductKeyword(searchKey);
			
			PdpPage pdpPage=searchresultpage.navigateToPDP();
			
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickAddToBag();
			
			ShoppingBagPage shoppingBagPage=pdpPage.clickOnMiniCart();
			
			CheckoutPage checkoutpage=shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			
			checkoutpage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Standard", "valid_address6");
			checkoutpage.clickOnContinueInShipping();
			checkoutpage.clickContinueInAddressValidationModal();
			checkoutpage.clickOnContinueInShipping();
//			checkoutpage.ContinueAddressValidationModalInMultipleShipping();
			
			checkoutpage.fillingCardDetails("NO", "card_Visa");
			checkoutpage.clickOnContinueInBilling();
			
			OrderConfirmationPage orderConfirmationPage=checkoutpage.ClickOnPlaceOrderButton();
			
			myaccount=homePage.headers.navigateToMyAccount();
			
			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();

			Log.message((i++) + ". Navigated to Order History page");

			OrderHistoryPage.clickOnViewDetailsByIndex(1);

			Log.message((i++) + ". Navigated to View Details Page");   
			
			OrderHistoryPage.clickOnBackToOrder();
			Log.message((i++) + ". Clicked on Back to order");
			
			
			Log.message("<b>Expected Result 1: </b>Customer should be navigated to Order History page if the customer clicks on the Back to Order History link.");
			Log.assertThat(
					driver.getCurrentUrl().contains("Order-History"),
					"<b>Actual Result 1: </b>Customer navigated to Order History page ",
					"<b>Actual Result 1: </b>Customer not navigated to Order History page ", driver);    

			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Navigated to View Details Page"); 
			OrderHistoryPage.clickOnReturnToShopping();
			Log.message((i++) + ". Clicked on Return to Shopping");
			Log.message("<b>Expected Result 2: </b>Customer should be navigated to Home page if the customer clicks on the Return to Shopping link.");
			Log.assertThat(
					driver.getCurrentUrl().contains("home"),
					"<b>Actual Result 2: </b>Customer navigated to Home page ",
					"<b>Actual Result 2: </b>Customer not navigated to Home page ", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_167

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify selecting Express Checkout option when Default addresses and payment method are not available", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_028(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();
		String errorMessage = "You must have a Default Shipping Address, Default Billing Address and Default Payment Method to select Express Checkout";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount
					.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			// get the warning message and verify the text
			String message = editProfilePage.getExpressCheckoutWarningMsg();
			
			Log.message("<b>Expected Result 1: </b>Warning message for Express checkout should be displayed in UI.");
			Log.assertThat(errorMessage.equals(message), 
					"<b>Actual Result 1: </b>Warning message for Express checkout is displayed in UI.", 
					"<b>Actual Result 1: </b>Warning message for Express checkout is not displayed in UI.", driver);
			

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_028

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on close('X') icon on size chart window", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_208(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");


			// Navigating to product with productID
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Search UPC '"+searchKey+"' and navigated to PDP!");

			pdpPage.clickSizeChartLink();
			Log.message((i++) + ". Clicked the size chart.");
			
			pdpPage.closeSizeChartByCloseButton();
			BrowserActions.nap(3);
			Log.message((i++) + ". Clicked Close button on the size chart.");
			Log.message("<b>Expected Result 1: </b>Size chart should be closed when user clicks on X icon ");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("iconCloseSizeChart"), pdpPage),
							"<b>Actual Result 1: </b>Size chart is closed when user clicks on X icon ",
							"<b>Actual Result 1: </b>Size chart is not closed when user clicks on X icon ",
							driver);
			pdpPage.clickSizeChartLink();
			Log.message((i++) + ". Clicked the size chart.");
			pdpPage.closeSizechartByClickingOtherElement(homePage);
			
			Log.message((i++) + ". Clicked Other Element.");
			Log.message("<b>Expected Result 2: </b>Size chart should be closed when user clicks outside the modal");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("iconCloseSizeChart"), pdpPage),
							"<b>Actual Result 2: </b>Size chart is closed when user clicks outside the modal",
							"<b>Actual Result 2: </b>Size chart is not closed when user clicks outside the modal",
							driver);
			pdpPage.clickSizeChartLink();
			Log.message((i++) + ". Clicked the size chart.");
			pdpPage.closeSizechartByEsckey();
			Log.message((i++) + ". Pressed ESC key");
			Log.message("<b>Expected Result 3: </b>Size chart should be closed when user presses ESC key");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("windowSizeChartClosed"), pdpPage),
							"<b>Actual Result 3: </b>Size chart is closed when user presses ESC key",
							"<b>Actual Result 3: </b>Size chart is not closed when user presses ESC key",
							driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_208

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify to set new shipping address as Default Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_044(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		LinkedHashMap<String, String> defaultShippingAddress = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage profilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			if(profilePage.verifyDefaultPaneAvailabilityByName("billing")){
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigated to My Account page");
				}
				AddressBookPage addressbookpage= (AddressBookPage) profilePage.navigateToSection("addressbook");
				Log.event("Address book page is displayed");
				addressbookpage.deleteAddressByIndex(0);
				Log.event("Address deleted");
				if(Utils.getRunPlatForm()=="mobile"){
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigated to My Account page");
				}
				profilePage=(ProfilePage) addressbookpage.navigateToSection("profile");
				Log.event("Navigated to Edit profile page");
			}

			profilePage.clickOnSetAsDefaultBilling();
			Log.message((i++) + ". Click on Set Default Shipping Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();
			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("addressName", addressName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", "zipcode");
			newAddress.put("phone", "phonenumber");

			profilePage.fillingSetDefaultAddressDetails(newAddress);
			Log.message((i++) + ". Filled Set Default Shipping Address");
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click on Set Default Shipping Address");
			Log.message("<b>Expected Result 1: </b>Invalid error message should be shown when Zipcode is incorrect.");
			Log.assertThat(
					profilePage.getsetDefaultAddressFormErrorMsg().contains(
							"Please enter a valid ZIP Code"),
							"<b>Actual Result 1: </b>Invalid error message is shown when Zipcode is incorrect.",
							"<b>Actual Result 1: </b>No Invalid error message is shown when Zipcode is incorrect.",
							driver);

			// modify zipcode and phone value
			LinkedHashMap<String, String> newAddress1 = new LinkedHashMap<String, String>();
			newAddress1.put("title", addressName);
			newAddress1.put("firstname", firstName);
			newAddress1.put("lastname", lastName);
			newAddress1.put("addressName", addressName);
			newAddress1.put("address1", addressLine1);
			newAddress1.put("address2", addressLine2);
			newAddress1.put("city", city);
			newAddress1.put("state", state);
			newAddress1.put("zipcode", zipCode);
			newAddress1.put("phone", phone);


			profilePage.fillingSetDefaultAddressDetails(newAddress1);
			Log.message((i++) + ". Filled Address Details");

			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click on Set Default Shipping Address");

			profilePage.clickContinueAddressSuggestion();
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			defaultShippingAddress = profilePage.getDefaultBillingAddress();
			
			// Verify the default billing address
			Log.message("<b>Expected Result 2: </b>Title and address field in the existing address should be same as in the saved address");
			Log.assertThat(
					defaultShippingAddress.get("title").equals(
							newAddress.get("title")),
							"<b>Actual Result 2a: </b>Title is same as given title.",
							"<b>Actual Result 2a: </b>Title is not same as given title.", driver);

			Log.assertThat(
					defaultShippingAddress.get("address1").equals(
							newAddress.get("address1")),
							"<b>Actual Result 2b: </b>Address1 feild is same as given Address1",
							"<b>Actual Result 2b: </b>Address1 feild is not same as given Address1", driver);

			//Delete the created address
			if(Utils.getRunPlatForm()=="mobile"){
				homePage.headers.navigateToMyAccount();
				
			}
			profilePage.navigateToSection("addressbook");
			
			AddressBookPage addressBookPage = new AddressBookPage(driver);
			addressBookPage.deleteAddressByIndex(0);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_044

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify to set new billing address as Default Address", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_042(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		List<String> elementInProfile = new ArrayList<String>();
		elementInProfile.add("lnkChangeDefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage editProfilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile page");

			if(editProfilePage.verifyDefaultPaneAvailabilityByName("billing")){
				AddressBookPage addressbook=(AddressBookPage) editProfilePage.navigateToSection("addressbook");
				Log.message((i++) + ". Navigated to Address Book");
				int addressCount=addressbook.getTotalAddressCount();
				
				for(int j=1;j<=addressCount;j++){
					addressbook.deleteAddressByIndex(0);
					
				}
				editProfilePage=(ProfilePage) addressbook.navigateToSection("profile");
				Log.message((i++) + ". Navigated to Profile page");
			}

			// click on changeDefaultInBilling link
			editProfilePage.clickOnSetAsDefaultBilling();
			Utils.waitForPageLoad(driver, 4);
			Log.message((i++) + ". Clicked on Set As Default Billing.");

			// Saved Address for Set Default Billing Dialog
			LinkedHashMap<String, String> defaultBillingValues = new LinkedHashMap<String, String>();			
			defaultBillingValues.put("title", addressName);
			defaultBillingValues.put("firstname", firstName);
			defaultBillingValues.put("lastname", lastName);
			defaultBillingValues.put("address1",addressLine1);
			defaultBillingValues.put("address2", addressLine2);
			defaultBillingValues.put("city", city);
			defaultBillingValues.put("state", state);
			defaultBillingValues.put("zipcode", zipCode);
			defaultBillingValues.put("phone", phone);

			editProfilePage.fillingSetDefaultAddressDetails(defaultBillingValues);
			Log.message((i++) + ". Add address ");

			editProfilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");

			editProfilePage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address");

			editProfilePage.clickContinueAddressSuggestionInAddressValidation();;
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			editProfilePage = new ProfilePage(driver);
			LinkedHashMap<String, String> savedDefaultBillingAddress = editProfilePage.getDefaultBillingAddress();
			
			// compare two LinkedHashmaps Values(compare existing address with
			// selected address)			
			Log.message("<b>Expected Result 1: </b>All the information in the existing address should be same as the saved address");
			Log.softAssertThat(
					savedDefaultBillingAddress.get("title").contains(
							defaultBillingValues.get("title")),
							"<b>Actual Result 1a: </b>title(Set Default Billing Address) Field is same as Existing title.",
					"<b>Actual Result 1a: </b>title(Set Default Billing Address) Field is not same as Existing title.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("firstname").contains(
							defaultBillingValues.get("firstname")),
							"<b>Actual Result 1b: </b>firstName(Set Default Billing Address) Field is same as Existing firstName.",
					"<b>Actual Result 1b: </b>firstName(Set Default Billing Address) Field is not same as Existing firstName.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("lastname").contains(
							defaultBillingValues.get("lastname")),
							"<b>Actual Result 1c: </b>lastName(Set Default Billing Address) Field is same as Existing lastName.",
					"<b>Actual Result 1c: </b>lastName(Set Default Billing Address) Field is not same as Existing lastName.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("address1").contains(
							defaultBillingValues.get("address1")),
							"<b>Actual Result 1d: </b>address1(Set Default Billing Address) Field is same as Existing address1.",
					"<b>Actual Result 1d: </b>address1(Set Default Billing Address) Field is not same as Existing address1.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("city").contains(
							defaultBillingValues.get("city")),
							"<b>Actual Result 1e: </b>city(Set Default Billing Address) Field is same as Existing city.",
					"<b>Actual Result 1e: </b>city(Set Default Billing Address) Field is not same as Existing city.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("state").contains(defaultBillingValues.get("state")),
					"<b>Actual Result 1f: </b>state(Set Default Billing Address) Field is same as Existing state.",
					"<b>Actual Result 1f: </b>state(Set Default Billing Address) Field is not same as Existing state.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("zipcode").contains(
							defaultBillingValues.get("zipcode")),
							"<b>Actual Result 1g: </b>zipcode(Set Default Billing Address) Field is same as Existing zipcode.",
					"<b>Actual Result 1g: </b>zipcode(Set Default Billing Address) Field is not same as Existing zipcode.",driver);

			Log.softAssertThat(
					savedDefaultBillingAddress.get("phonenumber").contains(
							defaultBillingValues.get("phone")),
							"<b>Actual Result 1h: </b>phonenumber(Set Default Billing Address) Field is same as Existing phonenumber.",
					"<b>Actual Result 1h: </b>phonenumber(Set Default Billing Address) Field is not same as Existing phonenumber.",driver);
			Log.message("<b>Expected Result 2: </b>Change Default link should be displayed for Saved default billing Address");
			Log.assertThat(editProfilePage.elementLayer.verifyPageElements(
					Arrays.asList("lnkChangeDefaultBilling"),editProfilePage)
					, "<b>Actual Result 2: </b>Change Default link is displayed for Saved default billing Address"
					, "<b>Actual Result 2: </b>Change Default link is not displayed for Saved default billing Address",driver);

			//Navigate to Address book page to delete the added address
			if(Utils.getRunPlatForm()=="mobile"){
				homePage.headers.navigateToMyAccount();
				
			}
			AddressBookPage addressBookPage = (AddressBookPage) editProfilePage.navigateToSection("addressbook");
			
			addressBookPage.deleteAddressByIndex(0);
			

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_042

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Order status as 'Cancelled'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_194(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		//String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");
			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();	

			Log.message((i++) + ". Navigated to Order History page");	

			OrderHistoryPage.clickOnViewDetailsByIndex(1);								
			Log.message((i++) + ". Clicked on View Details by Index");
			
			OrderHistoryPage.clickOnViewDetailsByIndex(1);								
			Log.message((i++) + ". Clicked on Cancel Order By Index");
			OrderHistoryPage.clickOnYesInOrderCancelPopUp();
			Log.message((i++) + ". Clicked on yes in order cancel popup");
			OrderHistoryPage.clickOnBackToOrder();
			Log.message((i++) + ". Clicked on Back to Oder History");
			Log.message("<b>Expected Result 1: </b> The Order status should be displayed as Cancelled once the customer clicks the Cancel button.");
			Log.assertThat(false, 
					"<b>Actual Result 1a: </b>The Order status is displayed as Cancelled", 
					"<b>Actual Result 1a: </b>The Order status is not displayed as Cancelled",driver);
			Log.testCaseResult();
			
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_194

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Line Items details in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_188(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to 'Order History' Page!");

			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Clicked on View Details by Index");
			Log.message("<b>Expected Result 1: </b>Item Name, Item UPC, Item Color and Item Size should be Present");
			Log.assertThat(OrderHistoryPage.getItemName(1) != "", 
					"<b>Actual Result 1a: </b>ItemName is Present", 
					"<b>Actual Result 1a: </b>ItemName is Blank",driver);
			Log.assertThat(OrderHistoryPage.getItemSize(1) != "", 
					"<b>Actual Result 1b: </b>ItemUPC is Present", 
					"<b>Actual Result 1b: </b>ItemUPC is Blank",driver);
			Log.assertThat(OrderHistoryPage.getItemColor(1) != "", 
					"<b>Actual Result 1c: </b>ItemColor is Present", 
					"<b>Actual Result 1c: </b>ItemColor is Blank",driver);
			Log.assertThat(OrderHistoryPage.getItemSize(1) != "", 
					"<b>Actual Result 1d: </b>ItemSize is Present", 
					"<b>Actual Result 1d: </b>ItemSize is Blank",driver);


			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_188

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Order information in Order History Details page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_174(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password+ ") and navigated to 'My Account' page");

			OrderHistoryPage OrderHistoryPage = myaccount.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to 'Order History' Page!");

			OrderHistoryPage.clickOnViewDetailsByIndex(1);
			Log.message((i++) + ". Clicked view details");

			LinkedHashMap<String, String> OrderDetails = OrderHistoryPage.getOrderInformationFromOrderDetails();
			
			
			Log.message("<b>Expected Result 1: </b>Order Date, Order Time and Order Number should be in Expected Format and Order Status should be Present");
			Log.assertThat(OrderDetails.get("orderDate").matches("\\d{2}/\\d{2}/\\d{4}"),
					"<b>Actual Result 1a: </b>Order Date is in Expected Format",
					"<b>Actual Result 1a: </b>Order Date is not in Expected Format",driver);
			String Time[] = OrderDetails.get("orderTime").split(" ");
			Log.assertThat(Time[0].matches("\\d:\\d{2}:\\d{2}") || Time[0].matches("\\d{2}:\\d{2}:\\d{2}"),
					"<b>Actual Result 1b: </b>Order Time is in Expected Format",
					"<b>Actual Result 1b: </b>Order Time is not in Expected Format",driver);
			Log.assertThat(OrderDetails.get("orderNumber").matches("\\d{8}"),
					"<b>Actual Result 1c: </b>Order Number is in Expected Format",
					"<b>Actual Result 1c: </b>Order Number is not in Expected Format",driver);
			Log.assertThat(OrderDetails.get("orderStatus") != "",
					"<b>Actual Result 1d: </b>Order Status is Present",
					"<b>Actual Result 1d: </b>Order Status is not Present",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_174

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify creating payment method with invalid details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_132(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigate to Payment Page");

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Click Add Payment");

			paymentMethodsPage.fillingCardDetails("NO","card_AmericanExpressInvaild");
			Log.message((i++) + ". Fill in Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Save Payment Details");
			Log.message("<b>Expected Result 1: </b>Invalid Credit Card Number error message should be displayed.");
			Log.assertThat(
					paymentMethodsPage.getErrorMesssageOfInvalidCardNumber()
					.equals("Invalid Credit Card Number"),
					"<b>Actual Result 1: </b>Invalid Credit Card Number error message is displayed.",
					"<b>Actual Result 1: </b>Invalid Credit Card Number error message is not displayed.",driver);
			Log.message((i++) + ". Invalid Error Message Error Message is verified.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_132

	//Card details is not getting saved.
	//Couldn't verify Belk Reward Credit Card tile, the card icon should be shown.
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding Belk Reward card then its icon is dislaying in Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_131(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		LinkedHashMap<String, String> expectedCardDetails = new LinkedHashMap<>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigate to Payment method Page");
			int paymentCount = paymentMethodsPage.getTotalPaymentCount();
			
			if (paymentCount <= 0) {
				for (int j = 1; j <= paymentCount; j++) {
					paymentMethodsPage.deletePaymentByIndex(0);
					
				}
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Click Add Payment", driver);

			LinkedHashMap<String, String> cardDetails = paymentMethodsPage
					.fillingCardDetails("YES", "card_BelkRewardsCreditCard");
			
			List<String> indexes = new ArrayList<String>(cardDetails.keySet());
			expectedCardDetails.put("cardtype",
					indexes.get(0).replace("select_cardtype_", ""));
			expectedCardDetails.put("cardowner",
					indexes.get(1).replace("type_cardname_", ""));
			expectedCardDetails.put("cardno",
					indexes.get(2).replace("type_cardno_", ""));

			Log.message((i++) + ". Fill in Payment Details");

			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Save Payment Details");
			LinkedHashMap<String, String> actualCardDetails = paymentMethodsPage
					.getSavedPayment().get(0);
			
			String cardEndNo = actualCardDetails.get("cardno").substring(12);
			Log.message("<b>Expected Result 1: </b>Newly added card type Belk Reward Credit Card should be properly displayed in Payment Method screen.");
			Log.assertThat(expectedCardDetails.get("cardowner").equals(
					actualCardDetails.get("cardowner"))
					&& expectedCardDetails.get("cardno").endsWith(
							cardEndNo),
							"<b>Actual Result 1: </b>Newly added card type Belk Reward Credit Card is properly displayed in Payment Method screen.",
					"<b>Actual Result 1: </b>Newly added card type Belk Reward Credit Card is not properly displayed in Payment Method screen.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_131

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify that the user is able to select \"Use Original address\" option in the \"Address not Recognized by USPC\" modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_098(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address");
			Log.message("<b>Expected Result 1: </b> Expected Header should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address not Recognized by USPS"),
					"<b>Actual Result 1: </b>Expected Header is displayed in the Address Validation Form",
					"<b>Actual Result 1: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);

			addressBookPage.selectUseOriginalAddress();
			Log.message((i++) + ". chosen the original Address");
			addressBookPage.clickContinueAddressSuggestion();
			Log.message((i++)
					+ ". Click Continue button on Address not found popup");

			addressBookPage = new AddressBookPage(driver);

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);
			Log.message((i++) + ". Got saved Address by index");
			Log.message("<b>Expected Result 2: </b> Address should be displayed in the customer's address book.");
			Log.assertThat(
					savedAddress.get("Address1").equals(addressLine1),
					"<b>Actual Result 2: </b>The Address is displayed in the customer's address book.",
					"<b>Actual Result 2: </b>The Address is not displayed in the customer's address book.",
					driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++) + ". Delete the address for data setup clean up");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_098

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the Cancel button in the \"Address not Recognized by USPC\" modal", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_099(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickOnCancelInAddNewAddressModal();
			Log.message((i++)
					+ ". Clicked 'Cancel button' on Add New Address Detail Dialog.");

			addressBookPage = new AddressBookPage(driver);
			Log.message((i++) + ". Address Book Page is displayed.");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_099

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify \"Address not Recognized by USPS\" modal, when street no entered is not recognized by USPS.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_100(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");
		String defaultShipping = testData.get("DefaultShipping");
		String defaultBilling = testData.get("DefaultBilling");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();

			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			addressBookPage.fillingAddNewAddressDetails(newAddress,
					defaultShipping, defaultBilling);
			Log.message((i++) + ". Fill in Address Details");

			addressBookPage.clickSaveAddressNewForm();
			Log.message((i++) + ". Click Save button to Save the Address");
			Log.message("<b>Expected Result 1: </b> Expected Header should be  displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage.getAddressValidationHeader().equals(
							"Address not Recognized by USPS"),
					"<b>Actual Result 1: </b>Expected Header is displayed in the Address Validation Form",
					"<b>Actual Result 1: </b>Expected Header is not displayed in the Address Validation Form.",
					driver);
			Log.message("<b>Expected Result 2: </b>Expected Header message should be displayed in the Address Validation Form");
			Log.assertThat(
					addressBookPage
							.getAddressValidationHeaderMsg()
							.contains(
									"We're sorry. The U.S Postal Service does not recognize this address. Please choose from the following options:"),
					"<b>Actual Result 2: </b>Expected Header Message is displayed in the Address Validation Form",
					"<b>Actual Result 2: </b>Expected Header Message is not displayed in the Address Validation Form.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_100

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify left navigation panel in Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_135(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment method Page!");
			if (runPltfrm != "mobile") {
				List<String> expectedElements = Arrays.asList("lnkMyAccount",
						"lnkProfile", "lnkOrderHistory", "lnkAddressBook",
						"lnkPaymentMethods", "lnkRegistry", "lnkFAQ",
						"lnkWishList", "lnkBelkRewardsCreditCard",
						"lnkEmailPreferences");
				Log.message("<b>Expected Result 1: </b> All Fields should be visible in Side panel.");
				Log.assertThat(
						paymentMethodsPage.elementLayer
								.verifyPageElements(expectedElements,
										paymentMethodsPage.sidePannel),
						"<b>Actual Result 1: </b>Fields are visible in Side panel.",
						"<b>Actual Result 1: </b>Not all fields are visible in side panel",
						driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_135

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify 'Help' panel in left navigation of the Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_136(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Login into and navigated to 'MyAccountPage' Page!");

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment method Page!");
			List<String> expectedElements = null;
			if (Utils.getRunPlatForm() == "mobile") {
				expectedElements = Arrays.asList("needHelpSectionMobile","customerServiceContentPaneMobile");
			} else {
				expectedElements = Arrays.asList("needHelpSectionDesktop","customerServiceContentPaneDesktop");
			}
			Log.message("<b>Expected Result 1: </b> All Fields should be visible in Help panel.");
			Log.assertThat(
					paymentMethodsPage.elementLayer.verifyPageElements(
							expectedElements,
							paymentMethodsPage.needHelpSection),
					"<b>Actual Result 1: </b>Fields are visible in Help panel.",
					"<b>Actual Result 1: </b>Not all fields are visible in Help panel",
					driver);
			String helpSectionText = paymentMethodsPage.needHelpSection
					.getNeedHelpSectionText();
			Log.message((i++) + ". Get text from Need help section");
			Log.message("<b>Expected Result 2: </b>Fields in the Need Help Section should be displayed as expected");
			Log.assertThat(
					helpSectionText.contains("Contact Customer Service:")
							&& helpSectionText
									.contains("By Email: Belk_Customer_Care@belk.com")
							&& helpSectionText
									.contains("By Phone: 1-866-235-5443")
							&& helpSectionText
									.contains("Mon-Fri 7:00am - 10:00pm (EST) Sat-Sun 9:00am - 10:00pm (EST)")
							&& helpSectionText
									.contains("Contact Belk Rewards Card Customer Service:")
							&& helpSectionText
									.contains("1-800-669-6550 Mon-Sat 8:30am - 7:00pm (EST)"),
					"<b>Actual Result 2: </b>Fields in the Need Help Section is displayed ",
					"<b>Actual Result 2: </b>Fields in the Need Help Section is not displayed as expected, plz check the event log",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_136

	@Test(groups = { "mobile" }, description = "Verify the breadcrumb displayed for mobile from Address Book screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_126(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		//
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String runPltfrm = Utils.getRunPlatForm();
		List<String> breadcrumbText = new ArrayList<String>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		//		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page");

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Click Add New Address");

			breadcrumbText = addressBookPage.getTextInBreadcrumb();
			
			if (runPltfrm == "mobile") {
				Log.message("<b>Expected Result 1: </b>The breadcrumb should be displayed as expected - 'Back to My Account'");
				Log.assertThat(
						breadcrumbText.get(0).equals("Back to My Account"),
						"<b>Actual Result 1: </b>The breadcrumb is displayed as expected - 'Back to My Account'",
						"<b>Actual Result 1: </b>The breadcrumb is not displayed as expected - 'Back to My Account' and the actual is : '"
								+ String.join(">", breadcrumbText)+"'",
								driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_126

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify entering invalid Employee ID and Store ID for a Belk employee in Edit Profile", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_054(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String empId = testData.get("Employee ID");
		String storeId = testData.get("Store ID");
		String employeeIDErrorMessage = "We're sorry. We don't recognize the Belk associate ID number you entered. Please double-check the number and re-enter it.";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			ProfilePage profilePage = myaccount.navigateToEditProfilePage();			
			Log.message((i++) + ". Navigated to Profile Page!");

			profilePage.checkIAmBelkEmployee();
			Log.message((i++) + ". Checked I Am Belk employee check box in Profile Page!");

			profilePage.typeOnEmployeeIdField(empId);
			profilePage.typeOnStoreIdField(storeId);
			Log.message((i++) + ". Entered invalid Employee ID '"+empId+"' and Store ID '"+storeId+"'");
			profilePage.typeOnConfirmEmailField(emailid);
			profilePage.typeOnPasswordField(password);
			Log.message((i++) + ". Entered email ID '"+emailid+"' and Password '"+password+"'");
			profilePage.clickOnProfileApplyChanges();
			Log.message((i++) + ". Clicked on 'Apply Changes' button");
			String actualEmployeeIDErrorMessage = profilePage.getInvalidEmployeeIdMessage();
			
			Log.message("<b>Expected Result 1: </b>If an employee enters an invalid value into both Employee ID and/or Store ID fields, an appropriate error message should be displayed below each field");
			
			Log.softAssertThat(actualEmployeeIDErrorMessage.equals(employeeIDErrorMessage)
					,"<b>Actual Result 1a: </b>Error message '"+actualEmployeeIDErrorMessage+"' is displayed under Employee ID field"
					,"<b>Actual Result 1a: </b>Error message '"+actualEmployeeIDErrorMessage+"' is not displayed under Employee ID field",driver);

			Log.softAssertThat(profilePage.getInvalidStoreIdMessage().equals(employeeIDErrorMessage)
					,"<b>Actual Result 1b: </b>Error message '"+actualEmployeeIDErrorMessage+"' is displayed under Store ID field"
					,"<b>Actual Result 1b: </b>Error message '"+actualEmployeeIDErrorMessage+"' is not displayed under Store ID field", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_054

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify My Account global banner across all the pages", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_016(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			ProfilePage profilePage = myaccount.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Profile Page!");

			//Script need to be modified
			Log.failsoft("Account slot banner is not available. It is based on BM configuration.");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_016

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Create Account button in the cart header, Wishlist and Gift Registry pages.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_146(String browser) throws Exception {
		//
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			//			
			SignIn signInPage =   homePage.headers.navigateToWishListAsGuest();
			Log.message((i++) + ". Navigated to WishList page as guest");
			List<String> expectedElements = Arrays.asList("btnCreateAccount");
			
			Log.message("<b>Expected Result 1: </b>Create Account button should be displayed in WishListPage");
			Log.assertThat(signInPage.elementLayer.verifyPageElements(expectedElements , signInPage), 
					"<b>Actual Result 1: </b>Create Account button is displayed in WishListPage", 
					"<b>Actual Result 1: </b>Create Account button is not displayed in WishListPage",driver);
			CreateAccountPage createAccountPage =  signInPage.clickCreateAccount();
			Log.message((i++) + ". Navigated to Create Account Page!");
			signInPage =  homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message((i++) + ". Navigated to registry Page as guest");
			
			Log.message("<b>Expected Result 2: </b>Create Account button should be displayed in Registry page");
			Log.assertThat(signInPage.elementLayer.verifyPageElements(expectedElements , signInPage), 
					"<b>Actual Result 2: </b>Create Account button is displayed in Registry page", 
					"<b>Actual Result 2: </b>Create Account button is not displayed in Registry page",driver);
			createAccountPage =  signInPage.clickCreateAccount();
			Log.message((i++) + ". Navigated to Create account Page!");
			// Searching product in my account page with a productId
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message((i++) + ". Search " + searchKey + " in the home page and  Navigated to 'Search Result'  Page");

			// Navigating to product with productID
			PdpPage pdpPage = searchResultPage.navigateToPDPWithProductId();
			Log.message((i++) + ". Select product from search result page!");

			// Select Size, Color, Qty
			pdpPage.addProductToBag();
			Log.message((i++) + ". " + pdpPage.getProductName() + "Product is added into Shopping bag");

			// Navigate to SignIn Page

			ShoppingBagPage shoppingPage=    pdpPage.headers.NavigateToBagPage();
			Log.message((i++) + ". Navigated to Bag Page!");
			signInPage = (SignIn)   shoppingPage.clickOnCheckoutInOrderSummary(shoppingPage);
			Log.message((i++) + ". Clicked on Checkout in Order Summary");
			expectedElements = Arrays.asList("btnCheckoutAsGuest");
			Log.message("<b>Expected Result 3: </b>Continue as guest button should be displayed in Checkout page");
			Log.assertThat(signInPage.elementLayer.verifyPageElements(expectedElements , signInPage), 
					"<b>Actual Result 3: </b>Continue as guest button is displayed in Checkout page", 
					"<b>Actual Result 3: </b>Continue as guest button is not displayed in not Checkout page",driver);
			Log.message("<b>Expected Result 4: </b>'You will have the opportunity to create an account and track your order once you have completed checkout.' - message should be displayed when clicking checkout when not logged in");
			Log.assertThat(signInPage.getResourceMessageBelowCheckoutGuest()
					.contains("You will have the opportunity to create an account and track your order once you have completed checkout."),
					"<b>Actual Result 4: </b>'You will have the opportunity to create an account and track your order once you have completed checkout.' - message is displayed when clicking checkout when not logged in",
					"<b>Expected Result 4: </b>'You will have the opportunity to create an account and track your order once you have completed checkout.' - message is not displayed when clicking checkout when not logged in",driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_146

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify PDP sizechart in the PDP page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_207(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to product with productID
			PdpPage pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Search UPC '"+searchKey+"' and navigated to PDP!");

			pdpPage.clickSizeChartLink();
			Log.message((i++) + ". Clicked the size chart.");
			Log.message("<b>Expected Result 1: </b>Customer should be navigated to a window that shows a size chart.");
			Log.assertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("windowSizeChartOpened"), pdpPage), 
					"<b>Actual Result 1: </b>Customer navigated to a window that shows a size chart.", 
					"<b>Actual Result 1: </b>Customer not navigated to a window that shows a size chart.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_207

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the Tooltip \"APO/FPO and Why this is Required\" in Delete Default Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_118(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String apoFpoTooltipText = "Demandware is pleased to support billing and shipping of your order to an Armed Forces APO or FPO address. To take advantage of this service, please enter either APO or FPO in the \"City\" field. All APO and FPO orders will be shipped via the U.S. Postal Service. \"Express\" and \"Next-Day\" service is not available.";
		String shippingPhoneTooltipText = "This phone number is required in the event the shipping partner needs to arrange a delivery time with you.";
		String billingPhoneTooltipText = "The billing phone number must match the telephone number on your credit card account, otherwise the card will not be authorized.";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			AddressBookPage addressbook = myaccount.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book Page!");
			int addressCount=addressbook.getTotalAddressCount();
			
			if(addressCount<=0){
				addressbook.clickAddNewAddress();
				Log.message((i++) + ". Clicked Add new Address");
				addressbook.fillingAddNewAddressDetails("valid_address6", "No", "No");
				Log.message((i++) + ". Filled Address Details");
				addressbook.ClickOnSave();
				Log.message((i++) + ". Clicked on save button");
				addressbook.selectUseOriginalAddress();
				Log.message((i++) + ". Selected the use original Adress");
				addressbook.clickContinueAddressSuggestion();
				Log.message((i++) + ". Clicked on Continue Address Suggestion");
			}

			// user has existing addresses and edit the default address
			addressbook.clickOnEditAddressByIndex(1);
			Log.message((i++) + ". Clicked on Edit Address By Index");
			Log.message("<b>Expected Result 1: </b>Apo/Fpo Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getApoFpoTooltipText().contains(
							apoFpoTooltipText),
					"<b>Actual Result 1: </b>Apo/Fpo Tooltip text is properly displayed in UI.",
					"<b>Actual Result 1: </b>Apo/Fpo Tooltip text is not properly displayed(mismatch in Apo/Fpo Tooltip text) in UI.");


			
			Log.message("<b>Expected Result 2: </b>Shipping Phone Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getShippingPhoneTooltipText().contains(
							shippingPhoneTooltipText),
							"<b>Actual Result 2: </b>Shipping Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result 2: </b>Shipping Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.",driver);
			
			Log.message("<b>Expected Result 3: </b>Billing Phone Tooltip text should be properly displayed in UI.");
			Log.softAssertThat(
					addressbook.getBillingPhoneTooltipText().contains(
							billingPhoneTooltipText),
							"<b>Actual Result 3: </b>Billing Phone Tooltip text is properly displayed in UI.",
					"<b>Actual Result 3: </b>Billing Phone Tooltip text is not properly displayed(mismatch in phone Tooltip text) in UI.");
			

			



			// click on apo/fpo link and ApoFpo page is displayed.
			ApoFpoPage apofpoPage = addressbook
					.clickOnApoFpoTooltipLinkInAddNewAddressModal();
			
			// click 'My Account' in headers
			myaccount = apofpoPage.headers.navigateToMyAccount();
			
			// click on 'Address Book'
			addressbook = myaccount.navigateToAddressBook();
			

			// user has existing addresses and edit the default address
			addressbook.clickOnEditAddressByIndex(1);
			
			// click on phone tooltip and WhyIsRequiredPage is displayed.

			WhyIsRequiredPage whyIsRequiredPage = addressbook
					.clickOnPhoneTooltipLinkInAddNewAddressModal();
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_118

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify the details of each order in Order History page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_161(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++) + ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage = myaccount.navigateToOrderHistoryPage();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			LinkedList<LinkedHashMap<String, String>>  allOrdersDetails =  orderHistoryPage.getOrderHistoryDetails();

			int j = 1;
			for(LinkedHashMap<String, String> orderDetails : allOrdersDetails){
				int index=allOrdersDetails.indexOf(orderDetails)+1;
				
				//Verifying Order Number format
				Log.message("<br><b>Expected Result" + j + ": </b> Order Number should be in expected number format");
				Log.assertThat(orderDetails.get("orderNumber").matches("\\d+"),
						"<br><b>Actual Result" + (j++) + ":Order Number for item:"+index +" is in expected number format",
						"<br><b>Actual Result" + (j++) + ":Order Number for item:"+index +" is not in expected number format");

				//Verifying date format
				SimpleDateFormat format =  new SimpleDateFormat("mm/dd/yy");
				format.setLenient(false);
				
				try {
					format.parse(orderDetails.get("dateOrdered"));
					Log.message((i++) + ". Date is in expected format for item:");
				} catch (Exception e) {
					Log.fail((i++) + ".Date is not in expected format for item:" +  index);
				}

				//Verifying Order Total format
				Log.message("<br><b>Expected Result" + j + ": </b> Order Total should be in expected currency format");
				Log.assertThat(orderDetails.get("orderTotal").matches("[$,£]\\d+\\.\\d\\d"),
						"<br><b>Actual Result" + (j++) + ":Order Total for item:"+index +" is in expected currency format",
						"<br><b>Actual Result" + (j++) + ":Order Total for item:"+index +" is not in expected currency format",driver);

				//Verifying Order Status 
				List<String> statuses= Arrays.asList("Cancelled","In-Process","Completed");

				Log.message("<br><b>Expected Result" + j + ": </b> Order Status should be as expected");

				Log.assertThat(statuses.indexOf(orderDetails.get("orderStatus"))>=0 ,
						"<br><b>Actual Result" + (j++) + ":Order Status for item:"+index +" is as expected",
						"<br><b>Actual Result" + (j++) + ":Order Status for item:"+index +" is not as expected",driver);

			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_161
		// Something Went Wrong, please try again later. error message is

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify adding existing card details in default Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_046(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i = 1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Login into and navigated to 'MyAccountPage' Page!");
			myAccountPage.navigateToSection("profile");
			Log.message((i++) + ". navigate to Profile Page");
			ProfilePage profilePage = new ProfilePage(driver).get();
			profilePage.clickOnChangeDefaultInPayment();
			Log.message((i++) + ". Clicked on Change Default In Payment");
			Log.message("<b>Expected Result 1: </b>Saved Card dropdown box should display all the values that are already saved in Set default Payment Method screen.");
			Log.assertThat(
					profilePage.elementLayer.verifyPageElements(
							Arrays.asList("lblSavedCardFromDialog"),
							profilePage),
					"<b>Actual Result 1:</b> The 'Saved card' dropdown is displayed",
					"<b>Actual Result 1:</b> The 'Saved card' dropdown is not displayed",
					driver);
			profilePage.clickOnSetAsDefaultInSetDefaultPaymentDialog();
			Log.message((i++)
					+ ". Clicked on 'Set As Default' button without selecting any values from the dropdown in Set Default Payment Dialog");
			List<String> errorMsgs = profilePage
					.getsetDefaultPaymentFormErrorMsg();
			Log.message((i++) + ". Get Set Default Payment form error message");
			Log.message("<br><b>Expected Result 2: </b>When the user does not select any value from the dropdown and clicks Set as Default Button then the user is prompted with a warning message");

			Log.softAssertThat(
					errorMsgs.get(0).equals(
							"Please enter your Name as it appears on the card"),
					"<b>Actual Result 2a: </b>Error message: '"
							+ errorMsgs.get(0)
							+ "' under Card Type is displayed",
					"<b>Actual Result 2a: </b>Error message under Card Type is not displayed",
					driver);
			Log.softAssertThat(
					errorMsgs
							.get(1)
							.equals("Please enter the Number as it appears on your card"),
					"<b>Actual Result 2b: </b>Error message: '"
							+ errorMsgs.get(1)
							+ "' under Name On Card is displayed",
					"<b>Actual Result 2b: </b>Error message under Name On Card is not displayed",
					driver);
			Log.softAssertThat(
					errorMsgs.get(2).equals("Please select a Expiration Month"),
					"<b>Actual Result 2c: </b>Error message: '"
							+ errorMsgs.get(2)
							+ "' under Card Number is displayed",
					"<b>Actual Result 2c: </b>Error message under Card Number is not displayed",
					driver);
			Log.softAssertThat(
					errorMsgs.get(3).equals("Please select a Expiration Year"),
					"<b>Actual Result 2d: </b>Error message: '"
							+ errorMsgs.get(3)
							+ "' under Expiry Month & Expiry Year is displayed",
					"<b>Actual Result 2d: </b>Error message under Expiry Month & Expiry Year is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_46

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify editing Profile details does not change the names associated in Addresses, Payments and Registry Details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_022(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String phoneNo = testData.get("PhoneNumber");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			int i = 1;
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++)
					+ ". Logged in and navigated to 'MyAccountPage' Page!");

			ProfilePage profilepage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Profile message");
			int randomNum = new Random().nextInt(30000);
			String newFirstName = "Test022 " + randomNum;
			String newLastName = "Test022LasName " + randomNum;
			profilepage.typeOnFirstName(newFirstName);

			profilepage.typeOnLastName(newLastName);
			profilepage.typeOnPhoneNumber(phoneNo);
			Log.message((i++) + ". Edited the firstName, LastName and phone");
			profilepage.typeOnConfirmEmailField(emailid);
			profilepage.typeOnPasswordField(password);
			Log.message((i++) + ". Entered the Confirm Email and password");
			profilepage.clickOnProfileApplyChanges();
			Log.message((i++) + ". Clicked on 'Apply' button");
			String usernameInMyAccountPage = myAccountPage.getUsername();
			Log.message((i++) + ". Get the User Name");
			Log.message("<b>Expected Result 1: </b>The updated profile information should be saved accordingly ");

			Log.assertThat(
					usernameInMyAccountPage.equals(newFirstName + " "
							+ newLastName),
					"<b>Actual Result 1: </b>The username '"
							+ usernameInMyAccountPage
							+ "'  is saved as expected firstname '"
							+ newFirstName + "' and last name '" + newLastName
							+ "'", "<b>Actual Result 1: </b>The username '"
							+ usernameInMyAccountPage
							+ "'  is not saved as expected firstname '"
							+ newFirstName + "' and last name '" + newLastName
							+ "'", driver);
			AddressBookPage addressBookPage = myAccountPage
					.navigateToAddressBook();
			Log.message((i++) + ". Navigated to Address Book page");
			LinkedHashMap<String, String> addressDetails = addressBookPage
					.getSavedAddressByIndex(0);
			Log.message((i++) + ". Get the Saved Address By Index");
			Log.message("<b>Expected Result 2: </b>The updated Name should not be saved in the Address book section");
			Log.assertThat(
					addressDetails.get("FirstName").equals(firstName),
					"<b>Actual Result 2a: </b>Address book- First name '"
							+ firstName
							+ "' is not altered when profile details are updated.",
					"<b>Actual Result 2a: </b>Address- First name is altered when profile details are updated.",
					driver);
			Log.assertThat(
					addressDetails.get("LastName").equals(lastName),
					"<b>Actual Result 2b: </b>Address book- Last name '"
							+ lastName
							+ "' is not altered when profile details are updated.",
					"<b>Actual Result 2b: </b>Address- Last name is altered when profile details are updated.",
					driver);

			GiftRegistryPage giftRegistryPage = myAccountPage.headers
					.navigateToGiftRegistry();
			Log.message((i++) + ". Navigated to Registry page");
			giftRegistryPage.clickOnViewRegistrylnk();
			Log.message((i++) + ". Clicked on View Link");

			RegistryInformationPage registryInfoPage = new RegistryInformationPage(
					driver).get();
			registryInfoPage.clickOnEventInfoTab();
			Log.message((i++) + ". Clicked on Event Info tab<br>");
			Log.message("<b>Expected Result 3: </b>The updated Name should not be saved in the Registry section");
			Log.assertThat(
					registryInfoPage.getRegistrantFirstName().equals(firstName),
					"<b>Actual Result 3a: </b>Registry- First name '"
							+ firstName
							+ "' is not altered when profile details are updated.",
					"<b>Actual Result 3a: </b>Registry- First name is altered when profile details are updated.",
					driver);
			Log.assertThat(
					registryInfoPage.getRegistrantLastName().equals(lastName),
					"<b>Actual Result 3b: </b>Registry- Last name '"
							+ firstName
							+ "' is not altered when profile details are updated.",
					"<b>Actual Result 3b: </b>Registry- First name is altered when profile details are updated.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_22

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify Billing Address details in Set Default Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_038(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String addressName = testData.get("Address");
		String addressLine1 = testData.get("AddressLine1");
		String addressLine2 = testData.get("AddressLine2");
		String city = testData.get("City");
		String state = testData.get("State");
		String zipCode = testData.get("ZipCode");
		String phone = testData.get("PhoneNumber");

		LinkedHashMap<String, String> defaultBillingAddress = null;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page");

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigate to the Profile page");
			Log.message("</br>");
			Log.message("<b>Expected Result 1 :</b>The express checkout button should be disabled");
			Log.assertThat(
					!profilePage.isExpressCheckoutCheckboxEnabled(),
					"<b>Actual Result 1 :</b>The express checkout button is disabled",
					"<b>Actual Result 1 :</b>The express checkout button is not disabled. Check log",
					driver);

			if (profilePage.verifyDefaultPaneAvailabilityByName("billing")) {
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to My Account Page");
				}
				AddressBookPage addressbookpage = (AddressBookPage) profilePage
						.navigateToSection("addressbook");
				Log.message((i++) + ". Navigate to address book section");
				addressbookpage.deleteAddressByIndex(0);
				Log.message((i++) + ". Deleted the address by index");
				if (Utils.getRunPlatForm() == "mobile") {
					homePage.headers.navigateToMyAccount();
					Log.message((i++) + ". Navigate to My Account Page");
				}
				profilePage = (ProfilePage) addressbookpage
						.navigateToSection("profile");
				Log.message((i++) + ". Navigate to profile section");
			}

			profilePage.clickOnSetAsDefaultBilling();
			Log.message((i++) + ". Click on Change Default Billing Address");
			BrowserActions.nap(5);
			Log.message("</br>");
			Log.message("<b>Expected Result 2 :</b>Invalid error message should be shown when fields are empty.");

			Log.assertThat(
					profilePage
							.getFormHelpContent()
							.equals("Select from existing address or add a new default address."),
					"<b>Actual Result 2:</b> Invalid error message is shown when fields are empty.",
					"<b>Actual Result 2:</b> No Invalid error message is shown when fields are empty.",
					driver);
			//
			profilePage.clickOnCancelSetDefaultShippingBillingAddress();
			Log.message((i++)
					+ ". Click on Cancel to close Change Default Shipping Address Form");

			profilePage.clickOnSetAsDefaultBilling();
			Log.message((i++) + ". Click on Change Default Billing Address");

			LinkedHashMap<String, String> newAddress = new LinkedHashMap<String, String>();
			newAddress.put("title", addressName);
			newAddress.put("firstname", firstName);
			newAddress.put("lastname", lastName);
			newAddress.put("addressName", addressName);
			newAddress.put("address1", addressLine1);
			newAddress.put("address2", addressLine2);
			newAddress.put("city", city);
			newAddress.put("state", state);
			newAddress.put("zipcode", zipCode);
			newAddress.put("phone", phone);

			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");
			Log.message("</br>");
			Log.message("<b>Expected Result 3 :</b>Invalid error message should be shown when fields are empty.");

			Log.assertThat(
					profilePage.getsetDefaultAddressFormErrorMsg().contains(
							"This field is required."),
					"<b>Actual Result 3:</b> Invalid error message is shown when fields are empty.",
					"<b>Actual Result 3:</b> No Invalid error message is shown when fields are empty.",
					driver);

			profilePage.fillingSetDefaultAddressDetails(newAddress);
			Log.message((i++) + ". Filled Default Address");
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");
			profilePage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			Log.message((i++) + ". Click Save Address");

			profilePage.selectUseOriginalAddress();
			Log.message((i++) + ". Select Use Original Address");

			profilePage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++) + ". Click Continue button on Address Suggestion");

			defaultBillingAddress = profilePage.getDefaultBillingAddress();
			Log.message((i++) + ". Get the Default billing Address");
			// Verify the default billing address

			Log.message("</br>");
			Log.message("<b>Expected Result 4 :</b> Default Billing Address Fields should be available");

			Log.assertThat(
					defaultBillingAddress.get("title").equals(
							newAddress.get("title")),
					"<b>Actual Result 4:"
							+ "</b> Default Billing Address Fields are available",
					"<b>Actual Result 4:"
							+ "</b> Default Billing Address Fields are not available",
					driver);
			Log.message("</br>");
			Log.message("<b>Expected Result 5 :</b> Default Billing Address Fields should be available");

			Log.assertThat(
					defaultBillingAddress.get("address1").equals(
							newAddress.get("address1")),
					"<b>Actual Result 5:"
							+ "</b> Default Billing Address Fields are available",
					"<b>Actual Result 5:"
							+ "</b> Default Billing Address Fields are not available",
					driver);

			if (Utils.getRunPlatForm() == "mobile") {
				myAccountPage = homePage.headers.navigateToMyAccount();
				Log.message((i++) + ". Navigate to My Account Page");
			}

			profilePage.navigateToSection("addressbook");
			Log.message((i++) + ". Navigate to Address Book Section");
			AddressBookPage addressBookPage = new AddressBookPage(driver);

			addressBookPage.deleteAddressByIndex(0);
			Log.message((i++) + ". Deleted the address by index");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_038

	@Test(groups = { "desktop", "mobile","Tablet" }, description = "Verify editing Profile details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_SPRINT6_MYACCOUNT_121(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);	
		String shippingaddress = "valid_address6";
		RegistrySignedUserPage registrySignedUserPage = null;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		String addressOne_first=null;
		String addressOne_Second=null;
		try {
			
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					"2. Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver.getCurrentUrl());

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message("3. Clicked on Create Account button in SignIn page");

			// Creating a new account
			MyAccountPage myAccountPage = (MyAccountPage) createAccountPage
					.CreateAccount(createAccountPage);
			Log.message("4. Created a new user account");
			

			int registriesToBeCreated = 2;
			for (int i = 1; i <= registriesToBeCreated; i++) {
				registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
						.navigateToSection("registry");
				Log.message("5. Navigated to Edit profile page to add belk employee details");

				// Click on Create New Registry button
				registrySignedUserPage.clickOnCreateNewRegistry();
				Log.message("4. Click on Create New Registry button");

				// Enter the valid Event details to register
				registrySignedUserPage
						.fillingRegistryDetails("weddingRegistry");
				Log.message("5. Filling the valid event details");

				// Click on Continue button in Step 1
				registrySignedUserPage.clickContinueInCreateRegistry();
				Log.message("6. Click on Continue button in Event Registry page");

				// Update the PreEvent Address and Fill Post Event address
				if(i==1)
				{
				registrySignedUserPage
						.fillingEventShippingdetails(shippingaddress);
				 addressOne_first=registrySignedUserPage.getTextFromAddressOne();
				}
				else
				{
					registrySignedUserPage
					.fillingEventShippingdetails("valid_address2");
				Log.message("7. Update the Pre Event address and Fill Post Event address");
				addressOne_Second=registrySignedUserPage.getTextFromAddressOne();
				}
	
				// Click on Continue button in Shipping address section
				registrySignedUserPage.clickOnContinueButtonInEventShipping();
				Log.message("8. Click on Continue button in shipping address");
	
				// Click on Submit button
				registrySignedUserPage.clickOnSubmitButton();
				Log.message("9. Click on Subtmit button in Post Event address");
			}			
			
			registrySignedUserPage.navigateToSection("addressbook");
			Log.message("10. Navigated to Address book page");
			
			int AddressAtRowToBeDeleted=2;
			registrySignedUserPage.clickDeleteRegistryByIndex(AddressAtRowToBeDeleted);
			Log.message("11. Deleting the address from a registry");
			
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> The following fields and buttons should displayed (AddressName,FirstName, LastName, AddressOne,AddressTwo,City,ZipCode,State,Phone)");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("txtAddressNameInDelAddress","txtFirstNameInDelAddress","txtAddressOneInDelAddress","txtAddressTwoInDelAddress","txtCityInDelAddress","txtZipCodeInDelAddress","drpStateInDelAddress","txtPhoneInDelAddress","btnSaveInDelAddress","txtCancelInDelAddress"), registrySignedUserPage), 
					"<b>Actual Result 1: </b>The following fields and buttons is displayed (AddressName,FirstName, LastName, AddressOne,AddressTwo,City,ZipCode,State,Phone,Cancel button and save button)",
					"<b>Actual Result 1: </b>Some of the address fields are missing in Delete address window",driver);
			
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b> Resource message should be displayed to instruct the customer to select an existing address or add a new default address to their registry");
			Log.assertThat(registrySignedUserPage.elementLayer.verifyPageElements(Arrays.asList("lblResourceMessage"), registrySignedUserPage), 
					"<b>Actual Result 2: </b>Resource message displayed to instruct the customer to select an existing address or add a new default address to their registry",
					"<b>Actual Result 2: </b>Resource message misssing which instructs the customer to select an existing address",driver);
			
			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b> Existing address should be displayed in Saved Address drop down box");
			String address_actual=null;
			String address_expected=registrySignedUserPage.getDropDownAddress();
			if(AddressAtRowToBeDeleted==2)
				address_actual= addressOne_first;		
			else
				address_actual=addressOne_Second;			
			Log.assertThat(address_expected.contains(address_actual), "<b>Actual Result 3: </b> Existing address displayed in Saved Address drop down box",
					"<b>Actual Result 3: </b> Existing address not displayed in Saved Address drop down box",driver);		
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_SPRINT6_MYACCOUNT_121
	
	@Test(groups = { "desktop", "mobile" }, description = "Verify editing Profile details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_021(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged in to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage= (ProfilePage) myaccountpage.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The following fields and buttons should displayed (FirstName, LastName, Phone, Email, ConfirmEmail, Password, ApplyChanges, CurrentPassword, NewPassword, ConfirmPassword)");
			Log.assertThat(profilepage.elementLayer.verifyPageElements(Arrays.asList("txtFirstName","txtLastName","txtPhoneNumber","txtEmail","txtEmailConfirmation","txtPassword","txtCurrentPassword","txtNewPassword","txtConfirmPassword","btnProfileApplyChanges","btnPasswordApplyChanges"), profilepage), "<b>Actual Result: </b>The following fields and buttons is displayed (FirstName, LastName, Phone, Email, ConfirmEmail, Password, ApplyChanges, CurrentPassword, NewPassword, ConfirmPassword)", "<b>Actual Result: </b>Some fields are not displayed",driver);

			Log.message("<br>");

			String firstName=profilepage.getFirstNameFieldValue();
			String lastName=profilepage.getLastNameFieldValue();
			String phoneNumber=profilepage.getPhoneFieldValue();
			String email=profilepage.getEmailFieldValue();

			if(firstName.equals("")){
				firstName=null;
			}
			if(lastName.equals("")){
				lastName=null;
			}
			if(phoneNumber.equals("")){
				phoneNumber=null;
			}
			if(email.equals("")){
				email=null;
			}

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The FirstName, LastName, PhoneNumber and Email id fields should prefilled");
			Log.assertThat(!(firstName.equals("null") || lastName.equals("null") || phoneNumber.equals("null") || email.equals("null")),"<b>Actual Result: </b>The FirstName '"+firstName+"', LastName '"+lastName+"', PhoneNumber '"+phoneNumber+"' and Email id '"+email+"' fields should prefilled","<b>Actual Result: </b> some fields are not prefilled",driver);

			Log.message("<br>");

			String newPassword=profilepage.getNewPasswordFieldValue();
			String confirmPassword=profilepage.getConfirmPasswordFieldValue();
			if(newPassword.equals("")){
				newPassword="nullValue";
			}
			if(confirmPassword.equals("")){
				confirmPassword="nullValue";
			}
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The New Password and Confirm password fields should not be prefilled");
			Log.assertThat((newPassword.equals("nullValue") && confirmPassword.equals("nullValue")),"<b>Actual Result: </b>The New Password and Confirm Password fields is not prefilled","<b>Actual Result: </b> The New password and/or Confirm password fields are prefilled",driver);
			Log.message("<br>");


			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The divider lines should displayed by section wise");
			Log.assertThat(profilepage.getBorderCount()==5,"<b>Actual Result: </b>The divider lines are displayed by section wise","<b>Actual Result: </b> The divider lines are not displayed",driver);
			Log.message("<br>");

			String randomFirstName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
			profilepage.typeOnFirstName(randomFirstName);
			Log.message((i++) + ". Updated the profile details");

			profilepage.typeOnConfirmEmailField(emailid);
			profilepage.typeOnPasswordField(password);
			profilepage.clickOnProfileApplyChanges();

			Log.message((i++) + ". Clicked on the 'ApplyChanges' button");

			profilepage=(ProfilePage) myaccountpage.navigateToSection("profile");

			Log.message((i++) + ". Navigated to Profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The changes should updated.");
			Log.assertThat(profilepage.getFirstNameFieldValue().equals(randomFirstName),"<b>Actual Result: </b>The changes (First Name: '"+randomFirstName+"') is updated","<b>Actual Result: </b> The changes is not updated",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_021

	@Test(groups = { "desktop", "mobile" }, description = "Verify that password field validations", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_027(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged in to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage= (ProfilePage) myaccountpage.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			String randomPassword1=RandomStringUtils.randomAlphanumeric(6).toLowerCase();
			String randomPassword2=RandomStringUtils.randomAlphanumeric(6).toLowerCase();
			String invalidPassword=RandomStringUtils.randomAlphanumeric(3).toLowerCase();

			profilepage.typeOnCurrentPassword(password);
			Log.message((i++) + ". Entered the current password");
			profilepage.typeOnNewPassword(invalidPassword);
			Log.message((i++) + ". Entered the new password with less than 6 digits");
			profilepage.typeOnConfirmNewPassword(invalidPassword);
			Log.message((i++) + ". Entered the confirm new password with less than 6 digits");
			profilepage.typeOnCurrentPassword(password);
			List<String> errorMessage=profilepage.getTextFromPasswordError();
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The error message should displayed like 'Please enter at least 6 characters.' when entered less than 6 characters");
			Log.assertThat(errorMessage.get(0).equals("Please enter at least 6 characters.") && errorMessage.get(1).equals("Please enter at least 6 characters."), "<b>Actual Result: </b> The error message '"+errorMessage.get(0)+"' is displayed", "<b>Actual Result: </b> The error message is not displayed", driver);
			Log.message("<br>");
			profilepage.typeOnCurrentPassword(password);
			Log.message((i++) + ". Again entered the current password");
			profilepage.typeOnNewPassword(randomPassword1);
			Log.message((i++) + ". Again entered the new password");
			profilepage.typeOnConfirmNewPassword(randomPassword2);
			Log.message((i++) + ". Again entered the confirm new password (which is not equal as entered in new password field)");
			profilepage.typeOnCurrentPassword(password);

			errorMessage.clear();
			errorMessage=profilepage.getTextFromPasswordError();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The error message should displayed like 'please enter valid password' when entered password mismatch between new and confirm password");
			Log.assertThat(errorMessage.get(1).equals("please enter valid password"), "<b>Actual Result: </b> The error message '"+errorMessage.get(1)+"' is displayed when entered password mismatch between new and confirm password", "<b>Actual Result: </b> The error message is not displayed when entered password mismatch between new and confirm password", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_027

	@Test(groups = { "desktop", "mobile" }, description = "Verify adding same type of cards in Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_138(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) myaccountpage
					.navigateToSection("paymentmethods");
			Log.message((i++) + ".Navigated to Payment method page");

			int totalPayment=paymentmethodpage.getTotalPaymentCount();

			if(totalPayment>1){
				for(int j=1;j<totalPayment;j++){
					paymentmethodpage.deletePaymentByIndex(1);
					Log.event("Removed the saved card details if more than one");
				}
				Log.message((i++) + ".Removed the saved card details");
			}else if(totalPayment==0){
				paymentmethodpage.clickOnAddPaymentMethod();
				Log.message((i++) + ".Clicked on Add Payment Method button for first payment");
				paymentmethodpage.fillingCardDetails("NO", "card_Visa");
				Log.message((i++) + ".Filled the card details");
				paymentmethodpage.clickOnSave();
				Log.message((i++) + ".Clicked on the save button");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The card details should be saved");
				Log.assertThat(paymentmethodpage.getTotalPaymentCount()==1, "The new card details is added", "The new card details are not added",driver);
			}
			Log.message("<br>");
			paymentmethodpage.clickOnAddPaymentMethod();
			Log.message((i++) + ".Again clicked on the add payment method button");
			paymentmethodpage.fillingCardDetails("NO", "card_Visa1");
			Log.message((i++) + ".Filled the new card details");
			paymentmethodpage.clickOnCancel();
			Log.message((i++) + ".Clicked on the cancel button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The add payment dialog box should closed");
			Log.assertThat(paymentmethodpage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnCancel"), paymentmethodpage), "The add payment dialog box is closed", "The add payment dialog box is not closed",driver);
			Log.message("<br>");

			totalPayment=paymentmethodpage.getTotalPaymentCount();

			paymentmethodpage.clickOnAddPaymentMethod();
			Log.message((i++) + ".Again clicked on the add payment method button");
			paymentmethodpage.fillingCardDetails("NO", "card_Visa1");
			Log.message((i++) + ".Filled the new card details");
			paymentmethodpage.clickOnSave();
			Log.message((i++) + ".Clicked on the save button");

			int currentPaymentCount=paymentmethodpage.getTotalPaymentCount();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The added payment details should be saved");
			Log.assertThat(currentPaymentCount>totalPayment, "The added payment details is saved", "The added payment details is not saved",driver);			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_138

	@Test(groups = { "desktop", "mobile" }, description = "Verify deleting the default payment type from Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_140(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) myaccountpage
					.navigateToSection("paymentmethods");
			Log.message((i++) + ".Navigated to Payment method page");

			int totalPayment=paymentmethodpage.getTotalPaymentCount();

			if(totalPayment>1){
				for(int j=1;j<totalPayment;j++){
					paymentmethodpage.deletePaymentByIndex(1);
					Log.event("Removed the saved card details if more than one");
				}
				Log.message((i++) + ".Removed the saved card details");
			}else if(totalPayment==0){
				paymentmethodpage.clickOnAddPaymentMethod();
				Log.message((i++) + ".Clicked on Add Payment Method button for first payment");
				paymentmethodpage.fillingCardDetails("NO", "card_Visa");
				Log.message((i++) + ".Filled the card details");
				paymentmethodpage.clickOnSave();
				Log.message((i++) + ".Clicked on the save button");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> The card details should be saved");
				Log.assertThat(paymentmethodpage.getTotalPaymentCount()==1, "The new card details is added", "The new card details are not added",driver);
			}
			Log.message("<br>");

			paymentmethodpage.clickOnDeletePaymentByIndex(0);
			Log.message((i++) + ".Clicked on the delete link");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The delete payment dialog should displayed");
			Log.assertThat(paymentmethodpage.elementLayer.verifyPageElements(Arrays.asList("PaymentPopUp"), paymentmethodpage), "The delete payment dialog is displayed", "The delete payment dialog is displayed",driver);
			Log.message("<br>");

			paymentmethodpage.clickOnNoInDeleteConfirmation();
			Log.message((i++) + ".Clicked on the NO button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Delete confirmation dialog should closed");
			Log.assertThat(paymentmethodpage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("PaymentPopUp"), paymentmethodpage), "The delete confirmation dialog is closed", "The delete confirmation dialog is not closed",driver);
			Log.message("<br>");

			int paymentCount=paymentmethodpage.getTotalPaymentCount();

			paymentmethodpage.clickOnDeletePaymentByIndex(0);
			Log.message((i++) + ". Again clicked on delete link");

			paymentmethodpage.clickOnYesInDeleteConfirmation();
			Log.message((i++) + ".Clicked on the YES button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The address should deleted");
			Log.assertThat(paymentmethodpage.getTotalPaymentCount()<paymentCount, "The address is deleted", "The address is not deleted",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_140

	@Test(groups = { "mobile" }, description = "Verify the mobile breadcrumb for the Create Account page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_152(String browser) throws Exception {

		if(Utils.getRunPlatForm()!="mobile"){
			throw new SkipException("This testcase only applicable for Mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			WishListPage wishlistpage = homePage.clickOnWishList();
			Log.message((i++) + ". Navigated to 'WishList' Page");

			String breadcrumb=wishlistpage.getTextFromBreadcrumb();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The breadcrumb should be displayed as 'Back to My Account'");
			Log.assertThat(breadcrumb.equals("Back to My Account"),"<b>Actual Result: </b> The breadcrumb is displayed as '"+breadcrumb+"'","<b>Actual Result: </b> The breadcrumb is not displayed as expected",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_152

	@Test(groups = { "mobile" }, description = "Verify the Breadcrumbs in Mobile for the Order History Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_165(String browser) throws Exception {

		if(Utils.getRunPlatForm()!="mobile"){
			throw new SkipException("This testcase only applicable for Mobile");
		}

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");
			OrderHistoryPage orderHistoryPage = (OrderHistoryPage) myaccountpage
					.navigateToSection("orderhistory");
			Log.message((i++) + ". Navigated to Order History page");

			String breadcrumb=orderHistoryPage.getTextFromBreadcrumb();


			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The breadcrumb should be displayed as 'Back to My Account'");
			Log.assertThat(breadcrumb.equals("Back to My Account"),"<b>Actual Result: </b> The breadcrumb is displayed as '"+breadcrumb+"'","<b>Actual Result: </b> The breadcrumb is not displayed as expected",driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_165

	@Test(groups = { "desktop", "mobile" }, description = "Verify selecting saved address from Delete default Registry Address screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_122(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Click on Registry Link in Headers menu
			SignIn sign = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigate to Signin Page");

			// Login with valid user credientials
			RegistrySignedUserPage registrySignIn = sign.signInFromRegistryPage(emailid, password);
			Log.message((i++) + ". Sign In with valid user Credientials Email ID:" + emailid + "/Password:" + password);

			homePage.headers.navigateToGiftRegistry();

			// Click on Create New Registry button
			registrySignIn.clickOnCreateNewRegistry();
			Log.message((i++) + ". Click on Create New Registry button");

			// Enter the valid Event details to register
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			Log.message((i++) + ". Filling the valid event details");

			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message((i++) + ". Click on Continue button in Event Registry page");

			// Select Saved Address For Pre Event
//			registrySignIn.selectSavedPrePostEventAddressByIndex(1, "PreEvent");
			Log.message((i++) + ". Select the saved address for Pre Event");

			// Update the PreEvent Address and Fill Post Event address
//			registrySignIn.fillingEventShippingdetails("valid_shipping_address6");
			registrySignIn.fillingEventShippingdetails("Shipping_PreEvent_details");
			Log.message((i++) + ". Update the Pre Event address and Fill Post Event address");

			// Click on Continue button in Shipping address section
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message((i++) + ". Click on Continue button in shipping address");

			// Click on Submit button
			registrySignIn.clickOnSubmitButton();
			Log.message((i++) + ". Click on Subtmit button in Post Event address");

			Log.message("<br>");
			AddressBookPage addressbookpage = (AddressBookPage) registrySignIn
					.navigateToSection("addressbook");
			addressbookpage.clickAddNewAddress();
			addressbookpage.fillingAddNewAddressDetails("valid_address2", "No", "No");
			addressbookpage.ClickOnSave();
			addressbookpage.selectUseOriginalAddress();
			addressbookpage.ClickOnContinueInValidationPopUp();
			Log.message((i++) + ". Navigated to Address book page");
			addressbookpage.clickOnDeleteAddressByIndex(0);
			Log.message((i++) + ". Clicked on Delete link for address 1");
			addressbookpage.clickOnCancelInAddNewAddressModal();
			Log.message((i++) + ". Clicked on the 'Cancel' button in Delete Default Address popup");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The Delete Default Address popup should be closed when clicked on the Cancel button");
			Log.assertThat(
					addressbookpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("lblAddressFormDialogContainer"),
							addressbookpage),
					"<b>Actual Result:</b> The Delete Default Address popup is closed",
					"<b>Actual Result:</b> The Delete Default Address popup is not closed",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> While clicking on 'Cancel' button in Delete Default Address popup, the details should not be updated in Address book screen.");
			Log.softAssertThat(
					addressbookpage.getTotalAddressCount()==2,
					"<b>Actual Result:</b> While clicking on 'Cancel' button in Delete Default Address popup, the details should not be updated in Address book screen.",
					"<b>Actual Result:</b> While clicking on 'Cancel' button in Delete Default Address popup, the details should is updated in Address book screen.");
			
			addressbookpage.clickOnDeleteAddressByIndex(0);
			Log.message("<br>");
			Log.message((i++) + ". Clicked on Delete link for address 1");
			addressbookpage.selectSuggestedAddressByIndex(1);
			Log.message((i++) + ". Selected saved address from suggested address drop down");
			
			String selectedAddress = addressbookpage.getselectedAddressFromSavedAddressDropdown();
			String expectedAddressName = selectedAddress.split(" ", 0)[0].replace("(", "").replace(")", "");
			int count = selectedAddress.length() - selectedAddress.replace(" ", "").length();
			String expectedState = selectedAddress.split(" ", 0)[count-1];
			String expectedZipcode = selectedAddress.split(" ", 0)[count];
			LinkedHashMap<String, String> addressDetails = addressbookpage.getAddressDetailsFromAddAddressSection();
			String actualAddressName = addressDetails.get("addressName");
			String actualState = StateUtils.getStateCode(addressDetails.get("state"));
			String actualZipcode = addressDetails.get("zipcode");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Selected saved address should be properly filled in all fields in 'Delete default Registry Address screen'");
			Log.softAssertThat(expectedState.equals(actualState)
					&& expectedAddressName.equals(actualAddressName)
					&& expectedZipcode.equals(actualZipcode), "<b>Actual Result:</b> Selected saved address is properly filled in all fields in 'Delete default Registry Address screen'", "<b>Actual Result:</b> Selected saved address is not properly filled in all fields in 'Delete default Registry Address screen'");
			addressbookpage.clickOnSetAsDefaultInAddNewAddressModal();
			addressbookpage.selectUseOriginalAddress();
			addressbookpage.ClickOnContinueInValidationPopUp();
			Log.message("<br>");
			Log.message((i++) + ". Clicked on 'Set As Deafult' button");
			Log.message("<br><b>Expected Result:</b> While clicking on 'Set as Default' button, the selected address should be set as 'Default Shipping/Billing Address' in Address Book screen.");
			Log.softAssertThat(
					addressbookpage.getTotalAddressCount()==1,
					"<b>Actual Result:</b> While clicking on 'Set as Default' button, the selected address is set as 'Default Shipping/Billing Address' in Address Book screen.",
					"<b>Actual Result:</b> While clicking on 'Set as Default' button, the selected address does not set as 'Default Shipping/Billing Address' in Address Book screen.");
			registrySignIn = (RegistrySignedUserPage) addressbookpage
					.navigateToSection("registry");
			registrySignIn.deleteRegistryByIndex(1);
			addressbookpage = (AddressBookPage) registrySignIn
					.navigateToSection("addressbook");
			addressbookpage.deleteAddressByIndex(0);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_MYACCOUNT_122

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify clicking on the links in the left navigation of Create Account page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_147(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			WishListPage wishlistpage = homePage.clickOnWishList();

			Log.message("2. Login to the Application");
			SignIn signIn = new SignIn(driver).get();
			wishlistpage = signIn.signInToWishList(emailid, password);

			Log.message("Verifying as Guest User");
			Log.message("3. Navigate to Privacy Page ");
			wishlistpage.ClickonPrivacypolicy();
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("lblprivacypolicy"), wishlistpage),
					"<b>Actual Result:</b> Create Account Page is displayed",
					"<b>Actual Result:</b> Create Account Page is not displayed",
					driver);

			Log.message("4. Navigate to Secure shopping page");
			Log.message("Navigate back to WishlistPage Account Page ");
			BrowserActions.navigateToBack(driver);
			wishlistpage.ClickonSecureShopping();
			// Need to include the txtPrivacy element
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("Secureshopping"), wishlistpage),
					"<b>Actual Result:</b> Secure shopping Page is displayed",
					"<b>Actual Result:</b> Secure shopping Page is not displayed",
					driver);

			Log.message("Signout from WishList");
			wishlistpage.headers.clickSignOut();

			Log.message("5. Navigate to WishList as Guest User");
			homePage.clickOnWishList();
			Log.message("Verifying as Guest User");
			homePage.clickOnWishList();
			Log.message("6. Navigate back to Privacy Page ");
			wishlistpage.ClickonPrivacypolicy();
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("lblprivacypolicy"), wishlistpage),
					"<b>Actual Result:</b> Create Account Page is displayed",
					"<b>Actual Result:</b> Create Account Page is not displayed",
					driver);

			Log.message("7. Navigate back to WishlistPage Account Page ");
			BrowserActions.navigateToBack(driver);
			wishlistpage.ClickonSecureShopping();
			// Need to include the txtPrivacy element
			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("Secureshopping"), wishlistpage),
					"<b>Actual Result:</b> Secure shopping Page is displayed",
					"<b>Actual Result:</b> Secure shopping Page is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify login to the account 6 number of times unsuccessfully", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_157(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);
		String emailid;
		String password = "password";

		try {
			Log.message("1. Navigate to Home page of Belk site");
			HomePage homepage = new HomePage(driver,webSite).get();
			Log.message("2. Navigate to Sign In page");
			SignIn signIn = homepage.headers.navigateToSignIn();

			CreateAccountPage createAccount = signIn.clickCreateAccount();
			createAccount.CreateAccount(createAccount);
			emailid = createAccount.getAccountDetails().get("email");
			homepage.headers.clickSignOut();

			signIn = homepage.headers.navigateToSignIn();
			int i = 0;

			while (i == 6) {
				Log.message("Entering the user credentials " + i
						+ "th time: Email" + emailid + "Password:" + password);
				signIn.signInToAccount(emailid, password);
			}
			Log.assertThat(
					signIn.getSignInFormErrorMsg()
							.contentEquals(
									"This account is currently locked.  Please try again later."),
					"<b>Actual Result</b>: Valid Error message is displayed",
					"<b>Actual Result</b>: Valid Error message is not displayed and Error message is:"
							+ signIn.getSignInFormErrorMsg(), driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify deselecting I am a Belk Employee (optional) check box", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_057(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message(
					(i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!",
					driver);

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			ProfilePage profilePage = myAccountPage.navigateToEditProfilePage();
			Log.message((i++) + ". Navigated to Edit Profile Page", driver);

			profilePage.checkIAmBelkEmployee("uncheck");
			Log.message((i++) + ". Unchecked Check Box 'I am Belk Employee'",
					driver);
			Log.message("<b>Expected Result: </b>Employee Details should not be displayed after unchecking 'I am Belk Employee' in Profile Page.");
			Log.assertThat(
					profilePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("txtEmployeeInfo"), profilePage),
							"<b>Actual Result: </b>Employee Details are not displayed after unchecking 'I am Belk Employee' in Profile Page.",
							"<b>Actual Result: </b>Employee Details are displayed after unchecking 'I am Belk Employee' in Profile Page.",
							driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_057

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify setting the default payment type in the Payment Method screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_130(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ".  Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged into and navigated to 'MyAccountPage' Page!",
					driver);

			PaymentMethodsPage paymentMethodsPage = myAccountPage
					.navigateToPaymentMethodsPage();
			Log.message((i++) + ". Navigated to Payment Page");

			int paymentCount = paymentMethodsPage.getTotalPaymentCount();
			if (paymentCount > 0) {
				paymentMethodsPage.deletePaymentByIndex(0);
			}

			paymentMethodsPage.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked on 'Add Payment' button");

			paymentMethodsPage.fillingCardDetails("YES", "card_Valid_Discover");
			Log.message((i++) + ". Filled in Payment Details");
			paymentMethodsPage.clickOnSave();
			Log.message((i++) + ". Clicked on the 'Save' button");
			LinkedHashMap<String, String> savedCard = paymentMethodsPage.getSavedPaymentByIndex(0);
			Log.message("<b>Expected Result: </b>Added card should be saved as default payment");
			Log.assertThat(savedCard.get("defaultpayment").equals("Yes"), "Added card is saved as default payment", "Added card is not saved as default payment");
			Log.message("<b>Expected Result: </b>The payment method should be set as default payment method and will be displayed separately in the Default Payments section below the Add Payment method button");
			Log.assertThat(
					paymentMethodsPage
					.verifyPaymentMethodTitle("Default Payment Method"),
					"<b>Actual Result: </b>The payment method is set as default payment method and is displayed separately in the Default Payments section below the Add Payment method button",
					"<b>Actual Result: </b>The payment method is not set as default payment method and is not displayed separately in the Default Payments section below the Add Payment method button");
			Log.message("<b>Expected Result: </b>The other payment methods should be displayed under the Additional Payments section");
			Log.softAssertThat(
					paymentMethodsPage
					.verifyPaymentMethodTitle("Additional Payment Methods"),
					"<b>Actual Result: </b>The other payment methods is displayed under the Additional Payments section",
					"<b>Actual Result: </b>The other payment methods is not displayed under the Additional Payments section", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_130

	@Test(groups = { "desktop"}, description = "Verify the links present in the My Account Left Navigation for the Guest User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_144(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String link1 = "createAccount";
		String link2 = "privacyPolicy";
		String link3 = "secureShopping";
		List<String> txtInBreadcrumb = new ArrayList<>();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ".  Clicked SignIn button on header and navigated to 'SignIn' Page!");

			signinPage.clickLeftNavLinks(link1);
			Log.message((i++) + ".  Clicked Left Nav link: '"+link1+"'");
			txtInBreadcrumb = signinPage.getTextInBreadcrumb();
			Log.message("<b>Expected Result: </b>Clicking on link: '"+link1+"' should navigate to '"+link1+" page'");
			Log.assertThat(txtInBreadcrumb.contains("Create an Account") && Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "Account-StartRegister"), "<b>Actual Result: </b>Clicking on link: '"+link1+"' is navigated to '"+link1+" page'", "Clicking on link: '"+link1+"' is not navigated to '"+link1+" page'", driver);
			driver.navigate().back();
			signinPage.clickLeftNavLinks(link2);
			Log.message((i++) + ".  Clicked Left Nav link: '"+link2+"'");
			txtInBreadcrumb = signinPage.getTextInBreadcrumb();
			Log.message("<b>Expected Result: </b>Clicking on link: '"+link2+"' should navigate to '"+link2+" page'");
			Log.assertThat(txtInBreadcrumb.contains("Privacy Policy") && Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "privacy-policy"), "<b>Actual Result: </b>Clicking on link: '"+link1+"' is navigated to '"+link1+" page'", "Clicking on link: '"+link1+"' is not navigated to '"+link1+" page'", driver);
			driver.navigate().back();
			signinPage.clickLeftNavLinks(link3);
			Log.message((i++) + ".  Clicked Left Nav link: '"+link3+"'");
			Log.message("<b>Expected Result: </b>Clicking on link: '"+link3+"' should navigate to '"+link3+" page'");
			Log.assertThat(Utils.verifyPageURLContains(driver, webSite.split("/s/")[0], "security-policy"), "<b>Actual Result: </b>Clicking on link: '"+link1+"' is navigated to '"+link1+" page'", "Clicking on link: '"+link1+"' is not navigated to '"+link1+" page'", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_144

	@Test(groups = { "desktop", "mobile"}, description = "Verify the account navigation text present below the My Account Left Navigation for the Authenticated User.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_145(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		List<String> elementToVerify = null;
		String runPltfrm = Utils.getRunPlatForm();
		if(runPltfrm=="desktop") {
			elementToVerify = Arrays.asList("txtAccountNavigationInDesktop");
		} else if(runPltfrm=="mobile") {
			elementToVerify = Arrays.asList("txtAccountNavigationInMobile");
		}
		Log.testCaseInfo(testData);

		try {
			int i=1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ".  Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged into and navigated to 'MyAccountPage' Page!",
					driver);
			Log.message("<b>Expected Result: </b>The account navigation text should be displayed below the Left Navigation based on the accountnavtext content asset.");
			Log.assertThat(
					myAccountPage.elementLayer.verifyPageElements(
							elementToVerify, myAccountPage),
					"<b>Actual Result: </b>The account navigation text is displayed below the Left Navigation based on the accountnavtext content asset.",
					"<b>Actual Result: </b>The account navigation text is not displayed below the Left Navigation based on the accountnavtext content asset.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUNT_145

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify updating the card details from Delete Default Payment Method screen.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_141(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");
			ProfilePage profilepage = (ProfilePage) myaccount
					.navigateToSection("profile");

			if (profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Uncheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.event("Express checkout checkbox is un checked");
			}
			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			int totalAddress = addressbook.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++)
					addressbook.deleteAddressByIndex(0);
				Log.message((i++)
						+ ".Navigated to address book page and removed previously added address!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int k = 1; k <= totalPayment; k++)
					paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++)
						+ ".Navigated to payment page and removed previously added payment!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			profilepage = (ProfilePage) addressbook
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to Profile page");
			profilepage.clickOnSetDefaultAddress();
			Log.message((i++) + ". Clicked on set default address button");
			profilepage
					.fillingAddNewAddressDetail("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++)
					+ ". Clicked on save button and default shipping address is added");

			profilepage.clickOnSetAsDefaultPayment();
			Log.message((i++) + ". Clicked on set default payment button");
			LinkedHashMap<String, String> firstCard=profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			Log.assertThat(
					profilepage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnSetAsDefaultInDialogContainer"),
							profilepage),
					(i++)
							+ ". Clicked on save button and default payment is added",
					(i++)
							+ ". Clicked on save button and default payment is not added",
					driver);

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Express checkout checkbox is checked");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			Log.message((i++) + ". Navigated to payment methods page");
			paymentmethodpage.clickOnDeletePaymentByIndex(0);
			Log.message((i++)
					+ ". Clicked on the delete button from the 'default default payment'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Delete Default Payment Method' popup should displayed");
			Log.assertThat(
					paymentmethodpage.getTextFromDialogContainerHeader().equals(
							"Delete Default Payment Method"),
					"<b>Actual Result:</b> The 'Delete default payment' popup is displayed",
					"<b>Actual Result:</b> The 'Delete default payment' popup is not displayed",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Customer instruction' is displayed in the popup");
			Log.assertThat(
					paymentmethodpage.getMessageInDeletePaymentModal().equals(
							"You are deleting a default payment method. Select another payment method or add new payment method copy."),
					"<b>Actual Result:</b> The 'Customer instruction' is displayed in the popup as: "+paymentmethodpage.getMessageInDeletePaymentModal(),
					"<b>Actual Result:</b> The 'Customer instruction' is not displayed in the popup",
					driver);
			Log.message("<br>");
			paymentmethodpage.clickOnCancel();
			Log.message((i++) + ". Clicked on the cancel button");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The delete default address popup should closed");
			Log.assertThat(
					paymentmethodpage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("PaymentPopUp"),
							paymentmethodpage),
					(i++)
							+ ". The delete default address popup is closed",
					(i++)
							+ ". The delete default address popup is not closed",
					driver);
			Log.message("<br>");
			paymentmethodpage.clickOnDeletePaymentByIndex(0);
			Log.message((i++)
					+ ". Clicked on the delete button from the 'default default payment'");
			LinkedHashMap<String, String> secondCard=paymentmethodpage.fillingCardDetails("NO", "card_Visa1");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			List<String> indexes1 = new ArrayList<String>(firstCard.keySet());
			List<String> indexes2 = new ArrayList<String>(secondCard.keySet());
			
			String NameOnCardFirst=indexes1.get(1).replace("type_cardname_", "");
			String NameOnCardSecond=indexes2.get(2).replace("type_cardname_", "");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The new card should saved in the payment methods");
			Log.assertThat(!(NameOnCardFirst.equals(NameOnCardSecond)) && (paymentmethodpage.verifyPaymentMethodTitle("Default Payment Method")), "<b>Actual Result:</b> New card is saved in the payment methods", "<b>Actual Result:</b> New card is not saved in the payment methods",driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUT_141
	
	@Test(groups = {  }, description = "Place order continuously", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void PlaceOrder(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey=testData.get("SearchKey");

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");
			
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			for(int j=1;j<=3;j++){

			PdpPage pdpPage=homePage.searchProduct(searchKey);
			Log.message((i++)
					+ ". Navigated a product to PDP page using UPC");
			
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked on the 'Add to bag' button");
			
			ShoppingBagPage shoppingBagPage=homePage.headers.NavigateToBagPage();
			Log.message((i++)
					+ ". Navigated to Shopping bag page");
			
			CheckoutPage checkoutpage=shoppingBagPage.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++)
					+ ". Clicked on the checkout button");
			
			checkoutpage.enterCVVinExpressCheckout("card_Visa");
			checkoutpage.clickOnApplyButton();
			Log.message((i++)
					+ ". Entered the CVV and clicked on Apply button");
			OrderConfirmationPage orderconfirmationpage=checkoutpage.ClickOnPlaceOrderButton();
			Log.message((i++)
					+ ". Clicked on the 'Place order' button and navigated to Order confirmation page");
			
			orderconfirmationpage.ClickOnReturnToShoppingBtn();
			Log.message((i++)
					+ ". Clicked on the Return to shopping bag");
			}			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// PlaceOrder
	
	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify list of orders in Order History page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_MYACCOUNT_160(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");
			
			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");

			OrderHistoryPage orderHistoryPage=(OrderHistoryPage) myaccount.navigateToSection("orderhistory");
			Log.message((i++) + ". Navigated to order history page");
			
			String firstOrderNumber=orderHistoryPage.getOrderNumberByIndex(1);
			String secondOrderNumber=orderHistoryPage.getOrderNumberByIndex(2);
			String thirdOrderNumber=orderHistoryPage.getOrderNumberByIndex(3);
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The order list should be in ascending based on date or newest - Oldest");
			Log.assertThat((Integer.parseInt(firstOrderNumber)>Integer.parseInt(secondOrderNumber)) && (Integer.parseInt(secondOrderNumber)>Integer.parseInt(thirdOrderNumber)), "<b>Actual Result: </b> The order history list is in ascending order (Newest - Oldest)", "<b>Actual Result: </b> The order history list is not in ascending order (Newest - Oldest)");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_MYACCOUT_160

	
	
}
