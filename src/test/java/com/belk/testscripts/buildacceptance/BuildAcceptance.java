package com.belk.testscripts.buildacceptance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.belk.pages.CheckoutPage;
import com.belk.pages.CreateAccountPage;
import com.belk.pages.registry.GiftRegistryPage;
import com.belk.pages.registry.RegistryInformationPage;
import com.belk.pages.registry.RegistrySignedUserPage;
import com.belk.pages.CategoryLandingPage;
import com.belk.pages.HomePage;
import com.belk.pages.OrderConfirmationPage;
import com.belk.pages.PdpPage;
import com.belk.pages.PlpPage;
import com.belk.pages.QuickViewPage;
import com.belk.pages.SearchResultPage;
import com.belk.pages.ShoppingBagPage;
import com.belk.pages.SignIn;
import com.belk.pages.account.AddressBookPage;
import com.belk.pages.account.MyAccountPage;
import com.belk.pages.account.OrderHistoryPage;
import com.belk.pages.account.PaymentMethodsPage;
import com.belk.pages.account.ProfilePage;
import com.belk.pages.account.WishListPage;
import com.belk.pages.footers.CustomerServicePage;
import com.belk.pages.footers.FaqPage;
import com.belk.pages.footers.SiteMapPage;
import com.belk.pages.footers.StoreLocationPage;
import com.belk.pages.footers.TermsAndConditionsPage;
import com.belk.reusablecomponents.e2eUtils;
import com.belk.support.BrowserActions;
import com.belk.support.DataProviderUtils;
import com.belk.support.EmailReport;
import com.belk.support.EnvironmentPropertiesReader;
import com.belk.support.Log;
import com.belk.support.TestDataExtractor;
import com.belk.support.Utils;
import com.belk.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class BuildAcceptance {

	EnvironmentPropertiesReader environmentPropertiesReader;
	String webSite;
	private String workbookName = "testdata\\data\\Regression.xls";
	private String sheetName = "BAT";

	@BeforeTest(alwaysRun = true)
	public void init(ITestContext context) {
		webSite = (System.getProperty("webSite") != null ? System
				.getProperty("webSite") : context.getCurrentXmlTest()
				.getParameter("webSite"));
	}

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify creating an account from SignIn screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String firstName = "FirstName_" + RandomStringUtils.randomAlphabetic(3);
		String lastName = "LastName_" + RandomStringUtils.randomAlphabetic(3);
		String phoneNumber = RandomStringUtils.randomNumeric(10);
		String emailID = RandomStringUtils.randomAlphabetic(5) + "@testing.com";
		String password = RandomStringUtils.randomAlphanumeric(6);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Navigated to 'SignIn' Page from home page By Clicking Registery Link");

			// Clicking on Create Account in SignIn page
			CreateAccountPage createAccountPage = signinPage
					.clickCreateAccount();
			Log.message((i++)
					+ ". Clicked on Create Account button in SignIn page");

			// Creating a new account
			createAccountPage.enterFirstName(firstName);
			createAccountPage.enterLastName(lastName);
			createAccountPage.enterPhoneNumber(phoneNumber);
			createAccountPage.enterEmailID(emailID);
			createAccountPage.enterConfirmationEmailID(emailID);
			createAccountPage.enterPassword(password);
			createAccountPage.enterConfirmPassword(password);
			Log.message((i++)
					+ ". Filled the Information for creating new account");

			// Click Create Account Button
			createAccountPage.clickCreateCreateAccountBtn();
			Log.message((i++) + ". Clicked the Create Account Button");

			MyAccountPage myaccountPage = new MyAccountPage(driver).get();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The User should be navigated to My account Page after Creating Account");

			Log.assertThat(
					driver.getCurrentUrl().contains("Account-Show")
							&& myaccountPage != null,
					"<b>Actual Result:</b>  Created a 'New Account' from SignIn page is navigated to My account page",
					"<b>Actual Result:</b>  'New Account' is not created or not navigated to My Account page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT6_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verification for navigate to all pages in my account", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			Log.message((i++) + ". Navigating to Profile page");
			ProfilePage profilePage = (ProfilePage) myAccountPage
					.navigateToSection("profile");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Profile page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						profilePage.getTextInBreadcrumb().get(0)
								.equals("Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to Profile page",
						"<b>Actual Result: </b>The page is not redirected to Profile page",
						driver);
			} else {
				Log.softAssertThat(
						profilePage.getTextInBreadcrumb().contains(
								"Edit Profile"),
						"<b>Actual Result:</b> The page is redirected to Profile page",
						"<b>Actual Result: </b>The page is not redirected to Profile page",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.navigateToMyAccount();
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to Order history page");
			OrderHistoryPage orderHistoryPage = (OrderHistoryPage) myAccountPage
					.navigateToSection("orderhistory");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Order History page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						orderHistoryPage.getTextFromBreadcrumb().equals(
								"Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to Order History page",
						"<b>Actual Result: </b>The page is not redirected to Order History page",
						driver);
			} else {
				Log.softAssertThat(
						orderHistoryPage.getTextFromBreadcrumb().contains(
								"Order History"),
						"<b>Actual Result:</b> The page is redirected to Order History page",
						"<b>Actual Result: </b>The page is not redirected to Order History page",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.navigateToMyAccount();
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to Address Book page");
			AddressBookPage addressbookpage = (AddressBookPage) myAccountPage
					.navigateToSection("addressbook");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Address Book page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						addressbookpage.getTextInBreadcrumb().get(0)
								.equals("Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to Address Book page",
						"<b>Actual Result: </b>The page is not redirected to Address Book page",
						driver);
			} else {
				Log.softAssertThat(
						addressbookpage.getTextInBreadcrumb().contains(
								"Address Book"),
						"<b>Actual Result:</b> The page is redirected to Address Book page",
						"<b>Actual Result: </b>The page is not redirected to Address Book page",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.navigateToMyAccount();
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to Payment Methods page");
			PaymentMethodsPage paymentMethodsPage = (PaymentMethodsPage) myAccountPage
					.navigateToSection("paymentmethods");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Payment Methods page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						paymentMethodsPage.getTextInBreadcrumb().get(0)
								.equals("Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to Payment Methods page",
						"<b>Actual Result: </b>The page is not redirected to Payment Methods page",
						driver);
			} else {
				Log.softAssertThat(
						paymentMethodsPage.getTextInBreadcrumb().contains(
								"Payment Methods"),
						"<b>Actual Result:</b> The page is redirected to Payment Methods page",
						"<b>Actual Result: </b>The page is not redirected to Payment Methods page",
						driver);
			}

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.navigateToMyAccount();
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to Registry page");
			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Registry page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						registrySignedUserPage.getTextFromBreadCrumb().equals(
								"Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to Registry page",
						"<b>Actual Result: </b>The page is not redirected to Registry page",
						driver);
			} else {
				Log.softAssertThat(
						registrySignedUserPage.getTextFromBreadCrumb()
								.contains("Your Registries"),
						"<b>Actual Result:</b> The page is redirected to Registry page",
						"<b>Actual Result: </b>The page is not redirected to Registry page",
						driver);
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to FAQs page");
			Log.message("<br>");
			Log.failsoft("The FAQs page is not fully completed by the development team");
			// FAQPage faqPage=(FAQPage) myAccountPage.navigateToSection("faq");
			// Log.message("<br>");
			// Log.message("<b>Expected Result:</b> The page should redirected to FAQ page");
			// Log.softAssertThat(faqPage.getTextFromBreadCrumb().contains("FAQ"),
			// "<b>Actual Result:</b> The page is redirected to FAQ page","<b>Actual Result: </b>The page is not redirected to FAQ page",driver);

			if (Utils.getRunPlatForm() == "mobile") {
				homePage.headers.navigateToMyAccount();
			}

			Log.message("<br>");
			Log.message((i++) + ". Navigating to WishList page");
			WishListPage wishListPage = (WishListPage) myAccountPage
					.navigateToSection("wishlist");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to WishList page");
			if (Utils.getRunPlatForm() == "mobile") {
				Log.softAssertThat(
						wishListPage.getTextFromBreadcrumb().equals(
								"Back to My Account"),
						"<b>Actual Result:</b> The page is redirected to WishList page",
						"<b>Actual Result: </b>The page is not redirected to WishList page",
						driver);
			} else {
				Log.softAssertThat(
						wishListPage.getTextFromBreadcrumb().contains(
								"Wish List"),
						"<b>Actual Result:</b> The page is redirected to WishList page",
						"<b>Actual Result: </b>The page is not redirected to WishList page",
						driver);
			}

			Log.message("<br>");
			Log.message((i++) + ". Verifying Belk Rewards Credit Card page");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Belk Rewards Credit Card page");
			Log.softAssertThat(myAccountPage.getTextFromAttributeOfBRCC()
					.contains("www.belkcredit.com"),
					"<b>Actual Result:</b> The page is hyper link and the URL is: '"
							+ myAccountPage.getTextFromAttributeOfBRCC() + "'",
					"<b>Actual Result:</b> The page is not a hyper link and the URL is: '"
							+ myAccountPage.getTextFromAttributeOfBRCC() + "'");
			Log.message("<br>");
			Log.message((i++) + ". Navigating to Email Preferences page");
			Log.message("<br>");
			Log.failsoft("The Email Preferences page is not fully completed by the development team");
			// EmailPreferencesPage emailPreferencesPage=(EmailPreferencesPage)
			// myAccountPage.navigateToSection("emailpreferences");
			// Log.message("<br>");
			// Log.message("<b>Expected Result:</b> The page should redirected to Email Preferences page");
			// Log.softAssertThat(emailPreferencesPage.getTextFromBreadCrumb().contains("Email Preferences"),
			// "<b>Actual Result:</b> The page is redirected to Email Preferences page","<b>Actual Result: </b>The page is not redirected to Email Preferences page",driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Add new address and address details verification", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = (AddressBookPage) myAccountPage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");

			int addressCount = addressBookPage.getTotalAddressCount();

			if (addressCount > 0) {
				for (int j = 1; j <= addressCount; j++) {
					addressBookPage.deleteAddressByIndex(0);
					Log.event("The saved address are removed from the address book");
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked on the 'Add New Address' button");

			LinkedHashMap<String, String> filledAddress = addressBookPage
					.fillingAddNewAddressDetails("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address details");

			addressBookPage.ClickOnSave();
			Log.message((i++) + ". Clicked on the SAVE button");

			addressBookPage.selectUseOriginalAddress();
			addressBookPage.ClickOnContinueInValidationPopUp();
			Log.message((i++)
					+ ". Selected the 'Use Original address' and clicked on 'Continue' button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The address should be saved");
			Log.assertThat(addressBookPage.getTotalAddressCount() > 0,
					"The address is saved in the address book",
					"The address is not displayed in the address book", driver);
			List<String> indexes = new ArrayList<String>(filledAddress.keySet());
			LinkedHashMap<String, String> filledAddressCopy = new LinkedHashMap<String, String>();
			filledAddressCopy.put("Title",
					indexes.get(0).replace("type_addressname_", ""));
			filledAddressCopy.put("FirstName",
					indexes.get(1).replace("type_firstname_", ""));
			filledAddressCopy.put("LastName",
					indexes.get(2).replace("type_lastname_", ""));
			filledAddressCopy.put("Address1",
					indexes.get(3).replace("type_address_", ""));
			filledAddressCopy.put("Address2",
					indexes.get(4).replace("type_address_", ""));
			filledAddressCopy.put("City",
					indexes.get(5).replace("type_city_", ""));
			filledAddressCopy.put("State",
					indexes.get(6).replace("select_state_", ""));
			filledAddressCopy.put("Zipcode",
					indexes.get(7).replace("type_zipcode_", ""));
			filledAddressCopy.put("PhoneNo",
					indexes.get(8).replace("type_phoneno_", ""));
			filledAddressCopy.put("Defaultshipping", "No");
			filledAddressCopy.put("Defaultbilling", "No");

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The saved address details should be same as filled address details");
			Log.assertThat(
					filledAddressCopy.equals(savedAddress),
					"<b>Actual Result:</b>  The saved address details is same as filled address details",
					"<b>Actual Result:</b>  The saved address details is not same as filled address details",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Add new payment and payment details verification", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			PaymentMethodsPage paymentMethods = (PaymentMethodsPage) myAccountPage
					.navigateToSection("paymentmethods");
			Log.message((i++) + ". Navigated to payment methods page");

			int paymentCount = paymentMethods.getTotalPaymentCount();

			if (paymentCount > 0) {
				for (int j = 1; j <= paymentCount; j++) {
					paymentMethods.deletePaymentByIndex(0);
				}
				Log.event("The saved payments are removed from the payment methods");
			}

			paymentMethods.clickOnAddPaymentMethod();
			Log.message((i++) + ". Clicked on the ADD PAYMENT METHODS button");

			LinkedHashMap<String, String> filledPayment = paymentMethods
					.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Filled the card details");

			paymentMethods.clickOnSave();
			Log.message((i++) + ". Clicked on the save button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The filled payment should be saved");
			Log.assertThat(
					paymentMethods.getTotalPaymentCount() > 0,
					"<b>Actual Result:</b>  The payment is saved in the payment methods",
					"<b>Actual Result:</b>  The payment is not displayed in the payment methods",
					driver);

			LinkedHashMap<String, String> savedPayment = paymentMethods
					.getSavedPaymentByIndex(0);
			List<String> indexes = new ArrayList<String>(filledPayment.keySet());
			LinkedHashMap<String, String> filledCardCopy = new LinkedHashMap<String, String>();
			String month = indexes.get(3).replace("select_expmonth_", "")
					.substring(0, 3).toUpperCase();
			int monthNumber = 0;
			try {
				Date date = new SimpleDateFormat("MMM").parse(month);// put your
				// month
				// name
				// here
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				monthNumber = cal.get(Calendar.MONTH) + 1;
			} catch (Exception e) {
				e.printStackTrace();
			}

			filledCardCopy.put("cardtype",
					indexes.get(0).replace("select_cardtype_", ""));
			filledCardCopy.put("cardowner",
					indexes.get(1).replace("type_cardname_", ""));
			filledCardCopy.put("cardno",
					indexes.get(2).replace("type_cardno_", ""));
			String cardexpiry = monthNumber
					+ "/"
					+ indexes.get(4).replace("select_expyr_", "")
							.substring(2, 4);
			filledCardCopy.put("cardexpirydate", cardexpiry);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The saved payment details should be same as filled payment details");
			Log.assertThat(
					filledCardCopy.get("cardtype").equals(
							savedPayment.get("cardtype"))
							&& filledCardCopy.get("cardowner").equals(
									savedPayment.get("cardowner"))
							&& filledCardCopy.get("cardno")
									.contains(
											savedPayment.get("cardno").replace(
													"*", ""))
							&& filledCardCopy.get("cardexpirydate").equals(
									savedPayment.get("cardexpirydate")),
					"<b>Actual Result:</b>  The saved payment details is same as filled payment details",
					"<b>Actual Result:</b>  The saved payment details is not same as filled payment details",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_004

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify checking 'Express checkout' checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(Arrays.asList(
							"defaultPaymentMethodPane",
							"defaultShippingAddressPane",
							"defaultBillingAddressPane"), profilepage),
					(i++)
							+ ". The default Shipping, Billing and Payment method is displayed.",
					(i++)
							+ ". The default Shipping, Billing and/or Payment method is not displayed.",
					driver);

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Checked the Express checkout check box");
			} else {
				Log.message("The express checkout is checked by default");
			}

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Navigated to '" + searchKey
					+ "' search result page");
			PdpPage pdpPage = searchresultpage.navigateToPDP();
			Log.message((i++) + ". Navigated to PDP page with random product");
			String color = pdpPage.selectColor();
			Log.message((i++) + ". Selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message((i++) + ". Selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Clicked on the 'Add to bag' button");
			// homePage.headers.clickSignOut();
			myaccountpage = homePage.headers.navigateToMyAccount();
			myaccountpage.clickOnLogout();
			Log.message((i++) + ". Clicked on the SignOut link");
			signinPage = homePage.headers.navigateToSignIn();
			myaccountpage = signinPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Again login to the application with '"
					+ emailid + " / '" + password);

			ShoppingBagPage shoppingbagPage = homePage.headers
					.NavigateToBagPage();
			Log.message((i++) + ". Navigated to the shopping bag page");

			CheckoutPage checkoutpage = shoppingbagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++)
					+ ". Clicked on the Checkout button from the shopping bag page");

			String className = checkoutpage.getClassNameOfStepsInCheckoutPage(
					"placeorder").split(" ")[1];
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The user should be navigated to the Place Order page directly");
			Log.assertThat(
					className.equals("active"),
					"<b>Actual Result:</b> The page is redirected to the Place Order page",
					"<b>Actual Result:</b> The page is not redirected to Place Order page.",
					driver);
			checkoutpage.clickReturnToBilling();
			checkoutpage.clickReturnToShipping();
			checkoutpage.clickReturnToShoppingBag();
			if (Integer.parseInt(myaccountpage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingbagPage = myaccountpage.clickOnMiniCart();
				shoppingbagPage.removeItemsFromBag();
				Log.event("Cleanedup the shopping bag");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT6_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify unchecking 'Express checkout' checkbox", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Login to the application with '" + emailid
					+ "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(Arrays.asList(
							"defaultPaymentMethodPane",
							"defaultShippingAddressPane",
							"defaultBillingAddressPane"), profilepage),
					(i++)
							+ ". The default Shipping, Billing and Payment method is displayed.",
					(i++)
							+ ". The default Shipping, Billing and/or Payment method is not displayed.",
					driver);

			if (profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("UnCheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++)
						+ ". Unchecked the Express checkout check box");
			} else {
				Log.message("The express checkout is unchecked by default");
			}

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Navigated to '" + searchKey
					+ "' search result page");
			PdpPage pdpPage = searchresultpage.navigateToPDP();
			Log.message((i++) + ". Navigated to PDP page with random product");
			String color = pdpPage.selectColor();
			Log.message((i++) + ". Selected color :" + color);
			String size = pdpPage.selectSize();
			Log.message((i++) + ". Selected size :" + size);
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Clicked on the 'Add to bag' button");
			// homePage.headers.clickSignOut();
			myaccountpage = homePage.headers.navigateToMyAccount();
			myaccountpage.clickOnLogout();
			Log.message((i++) + ". Clicked on the SignOut link");
			signinPage = homePage.headers.navigateToSignIn();
			myaccountpage = signinPage.signInToMyAccount(emailid, password);
			Log.message((i++) + ". Again login to the application with '"
					+ emailid + " / '" + password);

			ShoppingBagPage shoppingbagPage = homePage.headers
					.NavigateToBagPage();
			Log.message((i++) + ". Navigated to the shopping bag page");

			CheckoutPage checkoutpage = shoppingbagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message((i++)
					+ ". Clicked on the Checkout button from the shopping bag page");

			String className = checkoutpage.getClassNameOfStepsInCheckoutPage(
					"shipping").split(" ")[1];
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The user should not be navigated to the Place Order page directly but re-directed to the checkout page for entering the shipping address.");
			Log.assertThat(
					className.equals("active"),
					"<b>Actual Result:</b> The page is redirected to the Shipping address page",
					"<b>Actual Result:</b> The page is not redirected to Shipping address page.",
					driver);
			checkoutpage.clickReturnToShoppingBag();
			if (Integer.parseInt(myaccountpage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingbagPage = myaccountpage.clickOnMiniCart();
				shoppingbagPage.removeItemsFromBag();
				Log.event("Cleanedup the shopping bag");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT6_006

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Delete default address on Express checkout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");
			ProfilePage profilepage = (ProfilePage) myaccount
					.navigateToSection("profile");

			if (profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Uncheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.event("Express checkout checkbox is un checked");
			}
			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			AddressBookPage addressbook = (AddressBookPage) profilepage
					.navigateToSection("addressbook");

			int totalAddress = addressbook.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++)
					addressbook.deleteAddressByIndex(0);
				Log.event((i++)
						+ ".Navigated to address book page and removed previously added address!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int k = 1; k <= totalPayment; k++)
					paymentmethodpage.deletePaymentByIndex(0);
				Log.event((i++)
						+ ".Navigated to payment page and removed previously added payment!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			profilepage = (ProfilePage) addressbook
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to Profile page");
			profilepage.clickOnSetDefaultAddress();
			Log.message((i++) + ". Clicked on set default address button");
			profilepage
					.fillingAddNewAddressDetail("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++)
					+ ". Clicked on save button and default shipping address is added");

			profilepage.clickOnSetAsDefaultPayment();
			Log.message((i++) + ". Clicked on set default payment button");
			profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			Log.assertThat(
					profilepage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnSetAsDefaultInDialogContainer"),
							profilepage),
					(i++)
							+ ". Clicked on save button and default payment is added",
					(i++)
							+ ". Clicked on save button and default payment is not added",
					driver);

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Express checkout checkbox is checked");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			addressbook = (AddressBookPage) addressbook
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");
			addressbook.clickOnDeleteAddressByIndex(0);
			Log.message((i++)
					+ ". Clicked on the delete button from the 'default shipping address'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Delete default address' popup should displayed");
			Log.assertThat(
					addressbook.getTextFromDialogContainerHeader().equals(
							"Delete Default Address"),
					"<b>Actual Result:</b> The 'Delete default address' popup is displayed",
					"<b>Actual Result:</b> The 'Delete default address' popup is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_007

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Delete Default payment screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_008(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ".Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigated to 'My Account' page");
			ProfilePage profilepage = (ProfilePage) myaccount
					.navigateToSection("profile");

			if (profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Uncheck");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.event("Express checkout checkbox is un checked");
			}
			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			AddressBookPage addressbook = myaccount.navigateToAddressBook();

			int totalAddress = addressbook.getTotalAddressCount();
			if (totalAddress > 0) {
				for (int j = 1; j <= totalAddress; j++)
					addressbook.deleteAddressByIndex(0);
				Log.message((i++)
						+ ".Navigated to address book page and removed previously added address!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}
			PaymentMethodsPage paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			int totalPayment = paymentmethodpage.getTotalPaymentCount();

			if (totalPayment > 0) {
				for (int k = 1; k <= totalPayment; k++)
					paymentmethodpage.deletePaymentByIndex(0);
				Log.message((i++)
						+ ".Navigated to payment page and removed previously added payment!");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			profilepage = (ProfilePage) addressbook
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to Profile page");
			profilepage.clickOnSetDefaultAddress();
			Log.message((i++) + ". Clicked on set default address button");
			profilepage
					.fillingAddNewAddressDetail("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address");
			profilepage.clickOnSetAsDefaultInSetDefaultBillingDialog();
			profilepage.selectUseOriginalAddress();
			profilepage.clickContinueAddressSuggestionInAddressValidation();
			Log.message((i++)
					+ ". Clicked on save button and default shipping address is added");

			profilepage.clickOnSetAsDefaultPayment();
			Log.message((i++) + ". Clicked on set default payment button");
			profilepage.fillingCardDetails("card_Visa");
			Log.message((i++) + ". Filled the card details");
			profilepage.clickOnSetAsDefaultInSetDefaultPaymentDialog();

			Log.assertThat(
					profilepage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnSetAsDefaultInDialogContainer"),
							profilepage),
					(i++)
							+ ". Clicked on save button and default payment is added",
					(i++)
							+ ". Clicked on save button and default payment is not added",
					driver);

			if (!profilepage.isExpressCheckoutCheckboxChecked()) {
				profilepage.checkExpressCheckout("Check");
				profilepage.typeOnConfirmEmailField(emailid);
				profilepage.typeOnPasswordField(password);
				profilepage.clickOnProfileApplyChanges();
				Log.message((i++) + ". Express checkout checkbox is checked");
			}

			if (Utils.getRunPlatForm() == "mobile") {
				myaccount = homePage.headers.navigateToMyAccount();
			}

			paymentmethodpage = (PaymentMethodsPage) addressbook
					.navigateToSection("paymentmethods");
			Log.message((i++) + ". Navigated to payment methods page");
			paymentmethodpage.clickOnDeletePaymentByIndex(0);
			Log.message((i++)
					+ ". Clicked on the delete button from the 'default default payment'");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Delete Default Payment Method' popup should displayed");
			Log.assertThat(
					addressbook.getTextFromDialogContainerHeader().equals(
							"Delete Default Payment Method"),
					"<b>Actual Result:</b> The 'Delete default payment' popup is displayed",
					"<b>Actual Result:</b> The 'Delete default payment' popup is not displayed",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_008

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Edit and updated details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++)
					+ ". Clicked SignIn button on header and navigated to 'SignIn' Page!");

			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Entered Valid credentials(" + emailid + "/"
					+ password + ") in 'Sign In' page", driver);

			AddressBookPage addressBookPage = (AddressBookPage) myAccountPage
					.navigateToSection("addressbook");
			Log.message((i++) + ". Navigated to address book page");

			int addressCount = addressBookPage.getTotalAddressCount();

			if (addressCount > 0) {
				for (int j = 1; j <= addressCount; j++) {
					addressBookPage.deleteAddressByIndex(0);
					Log.event("The saved address are removed from the address book");
				}
			}

			addressBookPage.clickAddNewAddress();
			Log.message((i++) + ". Clicked on the 'Add New Address' button");

			LinkedHashMap<String, String> filledAddress = addressBookPage
					.fillingAddNewAddressDetails("valid_address6", "NO", "NO");
			Log.message((i++) + ". Filled the address details");

			addressBookPage.ClickOnSave();
			Log.message((i++) + ". Clicked on the SAVE button");

			addressBookPage.selectUseOriginalAddress();
			addressBookPage.ClickOnContinueInValidationPopUp();
			Log.message((i++)
					+ ". Selected the 'Use Original address' and clicked on 'Continue' button");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The address should be saved");
			Log.assertThat(
					addressBookPage.getTotalAddressCount() > 0,
					"<b>Actual Result:</b> The address is saved in the address book",
					"<b>Actual Result:</b> The address is not displayed in the address book",
					driver);
			List<String> indexes = new ArrayList<String>(filledAddress.keySet());
			LinkedHashMap<String, String> filledAddressCopy = new LinkedHashMap<String, String>();
			filledAddressCopy.put("Title",
					indexes.get(0).replace("type_addressname_", ""));
			filledAddressCopy.put("FirstName",
					indexes.get(1).replace("type_firstname_", ""));
			filledAddressCopy.put("LastName",
					indexes.get(2).replace("type_lastname_", ""));
			filledAddressCopy.put("Address1",
					indexes.get(3).replace("type_address_", ""));
			filledAddressCopy.put("Address2",
					indexes.get(4).replace("type_address_", ""));
			filledAddressCopy.put("City",
					indexes.get(5).replace("type_city_", ""));
			filledAddressCopy.put("State",
					indexes.get(6).replace("select_state_", ""));
			filledAddressCopy.put("Zipcode",
					indexes.get(7).replace("type_zipcode_", ""));
			filledAddressCopy.put("PhoneNo",
					indexes.get(8).replace("type_phoneno_", ""));
			filledAddressCopy.put("Defaultshipping", "No");
			filledAddressCopy.put("Defaultbilling", "No");

			LinkedHashMap<String, String> savedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The saved address details should be same as filled address details");
			Log.assertThat(
					filledAddressCopy.equals(savedAddress),
					"<b>Actual Result:</b> The saved address details is same as filled address details",
					"<b>Actual Result:</b> The saved address details is not same as filled address details",
					driver);
			Log.message("<br>");

			addressBookPage.clickOnEditAddress();
			Log.message((i++) + ". Clicked on the 'Edit' link");

			LinkedHashMap<String, String> updatedAddress = addressBookPage
					.fillingAddNewAddressDetails("valid_address7", "NO", "NO");
			Log.message((i++) + ". Updated new address details");

			addressBookPage.ClickOnSave();
			Log.message((i++) + ". Clicked on the SAVE button");

			addressBookPage.selectUseOriginalAddress();
			addressBookPage.ClickOnContinueInValidationPopUp();
			Log.message((i++)
					+ ". Selected the 'Use Original address' and clicked on 'Continue' button");

			List<String> indexes1 = new ArrayList<String>(
					updatedAddress.keySet());
			LinkedHashMap<String, String> filledUpdatedAddressCopy = new LinkedHashMap<String, String>();
			filledUpdatedAddressCopy.put("Title",
					indexes1.get(0).replace("type_addressname_", ""));
			filledUpdatedAddressCopy.put("FirstName",
					indexes1.get(1).replace("type_firstname_", ""));
			filledUpdatedAddressCopy.put("LastName",
					indexes1.get(2).replace("type_lastname_", ""));
			filledUpdatedAddressCopy.put("Address1",
					indexes1.get(3).replace("type_address_", ""));
			filledUpdatedAddressCopy.put("Address2",
					indexes1.get(4).replace("type_address_", ""));
			filledUpdatedAddressCopy.put("City",
					indexes1.get(5).replace("type_city_", ""));
			filledUpdatedAddressCopy.put("State",
					indexes1.get(6).replace("select_state_", ""));
			filledUpdatedAddressCopy.put("Zipcode",
					indexes1.get(7).replace("type_zipcode_", ""));
			filledUpdatedAddressCopy.put("PhoneNo",
					indexes1.get(8).replace("type_phoneno_", ""));
			filledUpdatedAddressCopy.put("Defaultshipping", "No");
			filledUpdatedAddressCopy.put("Defaultbilling", "No");

			LinkedHashMap<String, String> EditedAddress = addressBookPage
					.getSavedAddressByIndex(0);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The updated address details should be same as edited address details");
			Log.assertThat(
					filledUpdatedAddressCopy.equals(EditedAddress),
					"<b>Actual Result:</b> The updated address details is same as edited address details",
					"<b>Actual Result:</b> The updated address details is not same as edited address details",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT6_009

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify editing Profile details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT6_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message((i++) + ". Navigated to 'SignIn' Page");

			MyAccountPage myaccountpage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message((i++) + ". Logged in to the application with '"
					+ emailid + "' / '" + password + "'");

			ProfilePage profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");
			Log.message((i++) + ". Navigated to profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The following fields and buttons should displayed (FirstName, LastName, Phone, Email, ConfirmEmail, Password, ApplyChanges, CurrentPassword, NewPassword, ConfirmPassword)");
			Log.assertThat(
					profilepage.elementLayer.verifyPageElements(Arrays.asList(
							"txtFirstName", "txtLastName", "txtPhoneNumber",
							"txtEmail", "txtEmailConfirmation", "txtPassword",
							"txtCurrentPassword", "txtNewPassword",
							"txtConfirmPassword", "btnProfileApplyChanges",
							"btnPasswordApplyChanges"), profilepage),
					"<b>Actual Result: </b>The following fields and buttons is displayed (FirstName, LastName, Phone, Email, ConfirmEmail, Password, ApplyChanges, CurrentPassword, NewPassword, ConfirmPassword)",
					"<b>Actual Result: </b>Some fields are not displayed",
					driver);

			Log.message("<br>");

			String firstName = profilepage.getFirstNameFieldValue();
			String lastName = profilepage.getLastNameFieldValue();
			String phoneNumber = profilepage.getPhoneFieldValue();
			String email = profilepage.getEmailFieldValue();

			if (firstName.equals("")) {
				firstName = null;
			}
			if (lastName.equals("")) {
				lastName = null;
			}
			if (phoneNumber.equals("")) {
				phoneNumber = null;
			}
			if (email.equals("")) {
				email = null;
			}

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The FirstName, LastName, PhoneNumber and Email id fields should prefilled");
			Log.assertThat(
					!(firstName.equals("null") || lastName.equals("null")
							|| phoneNumber.equals("null") || email
							.equals("null")),
					"<b>Actual Result: </b>The FirstName '" + firstName
							+ "', LastName '" + lastName + "', PhoneNumber '"
							+ phoneNumber + "' and Email id '" + email
							+ "' fields should prefilled",
					"<b>Actual Result: </b> some fields are not prefilled",
					driver);

			Log.message("<br>");

			String newPassword = profilepage.getNewPasswordFieldValue();
			String confirmPassword = profilepage.getConfirmPasswordFieldValue();
			if (newPassword.equals("")) {
				newPassword = "nullValue";
			}
			if (confirmPassword.equals("")) {
				confirmPassword = "nullValue";
			}
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The New Password and Confirm password fields should not be prefilled");
			Log.assertThat(
					(newPassword.equals("nullValue") && confirmPassword
							.equals("nullValue")),
					"<b>Actual Result: </b>The New Password and Confirm Password fields is not prefilled",
					"<b>Actual Result: </b> The New password and/or Confirm password fields are prefilled",
					driver);
			Log.message("<br>");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The divider lines should displayed by section wise");
			Log.assertThat(
					profilepage.getBorderCount() == 5,
					"<b>Actual Result: </b>The divider lines are displayed by section wise",
					"<b>Actual Result: </b> The divider lines are not displayed",
					driver);
			Log.message("<br>");

			String randomFirstName = RandomStringUtils.randomAlphanumeric(5)
					.toLowerCase();
			profilepage.typeOnFirstName(randomFirstName);
			Log.message((i++) + ". Updated the profile details");

			profilepage.typeOnConfirmEmailField(emailid);
			profilepage.typeOnPasswordField(password);
			profilepage.clickOnProfileApplyChanges();

			Log.message((i++) + ". Clicked on the 'ApplyChanges' button");

			profilepage = (ProfilePage) myaccountpage
					.navigateToSection("profile");

			Log.message((i++) + ". Navigated to Profile page");

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The changes should updated.");
			Log.assertThat(
					profilepage.getFirstNameFieldValue()
							.equals(randomFirstName),
					"<b>Actual Result: </b>The changes (First Name: '"
							+ randomFirstName + "') is updated",
					"<b>Actual Result: </b> The changes is not updated", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT6_010

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify 'Add to Wishlist' in PDP of the product as a  logged in user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_007(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchkey = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			signIn.signInToMyAccount(emailid, password);

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchkey[0]);
			Log.message("3. Searched with '" + searchkey
					+ "' in the search text box");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			String BrandName = pdpPage.getProductBrandName();
			String productName = pdpPage.getProductName();

			productName = BrandName.concat(" " + productName);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Add to Wishlist' link should be displayed in PDP of the product");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddToWishList"), pdpPage),
					"<b>Actual Result 1:</b> 'Add to Wishlist' link is displayed in PDP of the product",
					"<b>Actual Result 1:</b> 'Add to Wishlist' link is not displayed in PDP of the product",
					driver);

			pdpPage.clickAddToWishListLink();
			Log.message("5. Clicked on 'Add to Wishlist' link");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> 'Item added to Wishlist' message should be displayed when clicking on 'Add to Wishlist' link");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddToWishList"), pdpPage),
					"<b>Actual Result 2:</b> 'Item added to Wishlist' message is displayed when clicking on 'Add to Wishlist' link",
					"<b>Actual Result 2:</b> 'Item added to Wishlist' message is not displayed when clicking on 'Add to Wishlist' link",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Item should be added to the Wishlist");

			WishListPage WishListPage = pdpPage.headers.navigateToWishList();

			Log.assertThat(
					(WishListPage.verifyProductNameInWishlist(productName
							.trim())),
					"<b>Actual Result 3:</b> Item is added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					"<b>Actual Result 3:</b> Item is not added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					driver);

			WishListPage.ClickAddItemsToWishList();
			Log.message("6. Clicked on 'Add to Items to Wishlist' button");

			homePage.headers.searchAndNavigateToPDP(searchkey[1]);
			Log.message("7. Searched with '" + searchkey
					+ "' in the search text box");

			Log.message("8. Selecting Size, Color, Quantity!");
			String size2 = pdpPage.selectSize();
			String color2 = pdpPage.selectColor();
			String qty2 = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size2);
			Log.message("   Selected Color : " + color2);
			Log.message("   Selected Quantity : " + qty2);

			String brandName2 = pdpPage.getProductBrandName();
			String productName2 = pdpPage.getProductName();

			productName2 = brandName2.concat(" " + productName2);

			pdpPage.clickAddToWishListLink();
			Log.message("9. Clicked on 'Add to Wishlist' link");

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> Second Item should be added to the Wishlist");

			pdpPage.headers.navigateToWishList();

			Log.assertThat(
					(WishListPage.verifyProductNameInWishlist(productName2
							.trim())),
					"<b>Actual Result 4:</b> Second item is added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					"<b>Actual Result 4:</b> Second item is not added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_083

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify 'Add to Shopping bag' from wishlist page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_009(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchkey = testData.get("SearchKey").split("\\|");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			signIn.signInToMyAccount(emailid, password);

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchkey[0]);
			Log.message("3. Searched with '" + searchkey
					+ "' in the search text box");

			Log.message("4. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			String qty = pdpPage.selectQuantity();
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity : " + qty);

			String BrandName = pdpPage.getProductBrandName();
			String productName = pdpPage.getProductName();

			productName = BrandName.concat(" " + productName);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> 'Add to Wishlist' link should be displayed in PDP of the product");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddToWishList"), pdpPage),
					"<b>Actual Result 1:</b> 'Add to Wishlist' link is displayed in PDP of the product",
					"<b>Actual Result 1:</b> 'Add to Wishlist' link is not displayed in PDP of the product",
					driver);

			pdpPage.clickAddToWishListLink();
			Log.message("5. Clicked on 'Add to Wishlist' link");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> 'Item added to Wishlist' message should be displayed when clicking on 'Add to Wishlist' link");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddToWishList"), pdpPage),
					"<b>Actual Result 2:</b> 'Item added to Wishlist' message is displayed when clicking on 'Add to Wishlist' link",
					"<b>Actual Result 2:</b> 'Item added to Wishlist' message is not displayed when clicking on 'Add to Wishlist' link",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Item should be added to the Wishlist");

			WishListPage WishListPage = pdpPage.headers.navigateToWishList();

			Log.assertThat(
					(WishListPage.verifyProductNameInWishlist(productName
							.trim())),
					"<b>Actual Result 3:</b> Item is added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					"<b>Actual Result 3:</b> Item is not added to the wishlist when clicking on 'Add to Wishlist' link in PDP",
					driver);

			String productNameInWishlist = WishListPage.getProductName();

			WishListPage.addProductToBag(productName.trim());
			Log.message("6. Clicked on 'Add to Shopping Bag' button");

			ShoppingBagPage shoppingBagPage = WishListPage.clickOnMiniCart();
			Log.message("7. Clicked on Mini cart ");

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> Page should be navigated to shopping bag page");

			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "mobile") {

				Log.assertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								Arrays.asList("btnContinueShoppingMobile"),
								shoppingBagPage),
						"<b>Actual Result 4:</b> Page is navigated to shopping bag page",
						"<b>Actual Result 4:</b> Page is not navigated to shopping bag page",
						driver);
			} else {
				Log.assertThat(
						shoppingBagPage.elementLayer.verifyPageElements(
								Arrays.asList("btnContinueShopping"),
								shoppingBagPage),
						"<b>Actual Result 4:</b> Page is navigated to shopping bag page",
						"<b>Actual Result 4:</b> Page is not navigated to shopping bag page",
						driver);

			}

			Log.message("<br>");
			Log.message("<b>Expected Result 5:</b> Product should be added to the cart");

			Log.assertThat(
					(shoppingBagPage.getProductName(productNameInWishlist
							.trim())),
					"<b>Actual Result 5:</b> Product should be added to the cart",
					"<b>Actual Result 5:</b> Product should be added to the cart",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_REGISTRY_083

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify clicking on Find Wish List button with valid details", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			// Navigate to Signin page
			WishListPage wishlistpage = homePage.headers.navigateToWishList();
			Log.message("2. Navigate to WishList Page");

			// Search the wishlist based on Email Address
			wishlistpage.wishListSearch(emailid);
			Log.message("3. Search the wishlist based on Email Address:"
					+ emailid);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Wishlist Result table should be displayed");

			Log.assertThat(
					wishlistpage.elementLayer.verifyPageElements(
							Arrays.asList("wishListTable"), wishlistpage),
					"<b>Actual Result 1:</b> Wishlist Result table is displayed",
					"<b>Actual Result 1:</b> Wishlist Result table is not displayed",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify user can search the Added Registry from Find Registry screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_008(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String registryresults = "registryresults";
		String Firstname = "registrybelk";
		String Lastname = "registrybelk";
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Navigate to 'Belk' homePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to 'registry' page from the header section
			GiftRegistryPage registryPage = homePage.headers
					.navigateToGiftRegistry();
			Log.message("2. Clicked on 'Registry' button on header and Navigated to 'Registry' Page!");

			RegistrySignedUserPage registrysignedUserPage = new RegistrySignedUserPage(
					driver);
			registrysignedUserPage.findRegistry(Firstname, Lastname,
					"Housewarming");
			String Eventname1 = registrysignedUserPage
					.gettextFromSelectedEventinDropDown();
			Log.message("3. Entered FirstName: '" + Firstname
					+ "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type1 "
					+ Eventname1);

			registrysignedUserPage.findRegistry(Firstname, Lastname, "Dorm");
			String Eventname2 = registrysignedUserPage
					.gettextFromSelectedEventinDropDown();
			Log.message("4. Entered FirstName: '" + Firstname
					+ "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type2  "
					+ Eventname2);

			registrysignedUserPage.findRegistry(Firstname, Lastname,
					"Wedding Registry");
			String Eventname3 = registrysignedUserPage
					.gettextFromSelectedEventinDropDown();
			Log.message("5. Entered FirstName: '" + Firstname
					+ "', LastName: '" + Lastname
					+ "' and Found the'Registry' with an event Type3 "
					+ Eventname3);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should be allow to search with different Event type example: Wedding Registry, House warming etc.");
			Log.assertThat(
					Eventname1.contains("Housewarming")
							&& Eventname2.contains("Dorm")
							&& Eventname3.contains("Wedding Registry"),
					"<b>Actual Result 1:</b> User is allowed to search with different Event type example: Wedding Registry, House warming etc. ",
					"<b>Actual Result 1:</b> User is not allowed to search with different Event type example: Wedding Registry, House warming etc.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Registry details should be displayed in Registry screen.");
			Log.assertThat(
					registryPage.elementLayer.verifyPageElements(
							Arrays.asList(registryresults), registryPage),

					"<b>Actual Result 3:</b> Registry details is displayed in Registry screen.",
					"<b>Actual Result 3:</b> Registry details is not displayed in Registry screen.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	} // TC_BELK_REGISTRY_013

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify random L1 navigation with bread crumbs", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] categoryName = homePage.headers
					.navigateToRandomCategory("level-1");
			CategoryLandingPage categoryLandingPage = new CategoryLandingPage(
					driver).get();
			ArrayList<String> breadcrumbList = categoryLandingPage
					.getTextInBreadcrumb();
			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ categoryName[0]);
			Log.message("<br><b>Actual Result:</b>");
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equalsIgnoreCase("Home"))
								&& (breadcrumbList.get(1)
										.equalsIgnoreCase(categoryName[0])),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ categoryName[0] + "'",
						"2. Breadcrumb is not displayed as expected- Home > "
								+ categoryName[0] + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT1_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify random L2 navigation with bread crumbs", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] categoryName = homePage.headers
					.navigateToRandomCategory("level-2");
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ categoryName[1]);
			Log.message("<br><b>Actual Result:</b>");
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equalsIgnoreCase("Home"))
								&& (breadcrumbList.get(1)
										.equalsIgnoreCase(categoryName[0]))
								&& (breadcrumbList.get(2)
										.equalsIgnoreCase(categoryName[1])),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ categoryName[0] + "> " + categoryName[1]
								+ "'",
						"2. Breadcrumb is not displayed as expected- Home > "
								+ categoryName[0] + " and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT1_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify random L3 navigation with bread crumbs", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String runPltfrm = Utils.getRunPlatForm();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			String[] categoryName = homePage.headers
					.navigateToRandomCategory("level-3");
			PlpPage plpPage = new PlpPage(driver).get();
			ArrayList<String> breadcrumbList = plpPage.getTextInBreadcrumb();
			Log.message("<b>Expected Result:</b> User should be navigate to - Home > "
					+ categoryName[0]
					+ " > "
					+ categoryName[1]
					+ " > "
					+ categoryName[2]);
			Log.message("<br><b>Actual Result:</b>");
			if (runPltfrm == "desktop") {
				Log.assertThat(
						(breadcrumbList.get(0).equalsIgnoreCase("Home"))
								&& (breadcrumbList.get(1).replace("\n", " ")
										.equalsIgnoreCase(categoryName[0]))
								&& (breadcrumbList.get(2)
										.equalsIgnoreCase(categoryName[1]))
								&& (breadcrumbList.get(3)
										.equalsIgnoreCase(categoryName[2])),
						"2. Breadcrumb is displayed as expected- 'Home > "
								+ categoryName[0] + "> " + categoryName[1]
								+ "> " + categoryName[2] + "'",
						"2. Breadcrumb is not displayed as expected- 'Home > "
								+ categoryName[0] + "> " + categoryName[1]
								+ "> " + categoryName[2]
								+ "' and the actual is: "
								+ breadcrumbList.get(0) + " > "
								+ breadcrumbList.get(1) + " > "
								+ breadcrumbList.get(2) + " > "
								+ breadcrumbList.get(3), driver);
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						(breadcrumbList.get(0).equals("Back to Home")),
						"2. Breadcrumb is displayed as expected - 'Back to Home'",
						"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
								+ breadcrumbList.get(0) + "'", driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT1_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify product search & auto suggestion", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey1 = testData.get("SearchKey").split("\\|")[0];
		String searchKey2 = testData.get("SearchKey").split("\\|")[1];
		String invalidSearchKey = testData.get("SearchKey").split("\\|")[2];
		String searchTextBox = "Your search results for \"" + searchKey1 + "\"";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey1);
			Log.message("2. Navigated to " + searchKey1
					+ " search result Page!");

			Log.message("<b>Expected Result:</b> Products related to "
					+ searchTextBox
					+ " should be displayed in search result page ");
			Log.assertThat(
					searchResultPage.getTextFromSearchResult().contains(
							searchTextBox),
					"<b>Actual Result:</b> Products related to "
							+ searchTextBox
							+ " is displayed in search result page ",
					"<b>Actual Result:</b> Products related to "
							+ searchTextBox
							+ " is Not displayed in search result page ",
					driver);

			Log.message("<b>Expected Result:</b> Auto Suggestion pop-up should not be displayed for Invalid Charater Search key");
			homePage.enterSearchTextBox(invalidSearchKey);
			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("autoSuggestPopup"), homePage.get()),
					"<b>Actual Result:</b> Auto Suggestion pop-up is not displayed for Invalid Charater Search key : "
							+ invalidSearchKey,
					"<b>Actual Result:</b> Auto Suggestion pop-up is displayed for Invalid Charater Search key : "
							+ invalidSearchKey, driver);

			searchResultPage = homePage
					.searchProductWithAutoSuggestion(searchKey2);
			Log.message("3. Searched with valid keyword: " + searchKey2);
			Log.message("<b>Expected Result:</b> Clicking on a link in Auto suggestion list should navigate to Search results");

			Log.assertThat(
					searchResultPage != null,
					"<b>Actual Result:</b> Clicking on a link in Auto suggestion list is navigate to Search results",
					"<b>Actual Result:</b> Clicking on a link in Auto suggestion list is not navigated to Search results",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_21

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Login & Logout", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);
			Log.message("<b>Expected Result:</b> Clicking on 'Sign In' in header should navigate to Sign In page");
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"<b>Actual Result:</b>Clicking on 'Sign In' in header is navigated to Sign In page with the expected url: "
							+ driver.getCurrentUrl(),
					"<b>Actual Result:</b>Clicking on 'Sign In' in header is not navigated to Sign In page with the expected url: "
							+ driver.getCurrentUrl(), driver);

			MyAccountPage myaccount = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("<br>");
			Log.message("3. Entered credentials(" + emailid + "/" + password
					+ ") and navigated to 'My Account' page", driver);

			myaccount.headers.clickSignOut();
			Log.message("4. Clicked on SignOut link");
			Log.message("<b>Expected Result:</b> Clicking on 'Sign Out' link as Member should be navigated to My Account Login page");
			Log.message("<b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], "Login-Show")
							&& signinPage != null,
					"1. Clicking on 'Sign out' link as Member is navigated to My Account Login page with the expected url: "
							+ driver.getCurrentUrl(),
					"1. Clicking on 'Sign out' link as Member is Not navigated to My Account Login page and the actual url is: "
							+ driver.getCurrentUrl(), driver);
			List<String> txtInBreadCrumb = signinPage.getTextInBreadcrumb();
			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "desktop") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Home")
								&& txtInBreadCrumb.get(1).equals(
										"Sign In/Register"),
						"2. The breadcrumb is displayed as expected - 'Home > Sign In/Register'",
						"2. The breadcrumb is not displayed as expected - 'Home > Sign In/Register' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'");
			} else if (runPltfrm == "mobile") {
				Log.assertThat(
						txtInBreadCrumb.get(0).equals("Back to Home"),
						"2. The breadcrumb is displayed as expected - 'Back to Home'",
						"2. The breadcrumb is not displayed as expected - 'Back to Home' and the actual is : '"
								+ txtInBreadCrumb.get(0) + "'");
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT1_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Customer service page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		List<String> customerServices = Arrays
				.asList("custmerServicesPrimaryContent");
		List<String> footerTrackOrder = Arrays
				.asList("lnkTrackYourOrderOriginal");
		List<String> footerShippingInFormation = Arrays
				.asList("lnkShippingInFormationOriginal");
		List<String> termsAndConditionContent = Arrays.asList("primaryContent");
		String lblTFaqs = FaqPage.FAQS_CONTENT_NAME;
		List<String> FAQContent = Arrays.asList("fqaPrimaryContent");
		List<String> footerContact = Arrays.asList("lnkContactOriginal");
		String lblStore = StoreLocationPage.STORES_CONTENT_STRING;

		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");
			if (!(runPltfrm == "mobile")) {
				CustomerServicePage customerSerivcePage = homePage.footers
						.navigateToCustomerService();
				Log.message((i++) + ". Navigated to 'Belk' Customer Service!",
						driver.getCurrentUrl());
				Log.message("<b>Expected Result:</b> Clicking on 'Customer service' link should navigate to Customer serviice page");
				Log.softAssertThat(
						customerSerivcePage.elementLayer.verifyPageElements(
								customerServices, customerSerivcePage),
						"<b>Actual Result:</b> Clicking on 'Customer service' link is navigated to Customer serviice page",
						"<b>Actual Result:</b> Clicking on 'Customer service' link is Not navigated to Customer serviice page",
						driver);
				homePage.footers.scrollToFooter();
				Log.message((i++) + ". Scroll to footer in the homepage");
			}

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Track Your Order' link should navigate to Order Tracking page");
			Log.softAssertThat(
					homePage.footers.elementLayer.verifyPageElements(
							footerTrackOrder, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Track Your Order' link is navigated to Order Tracking page",
					"<b>Actual Result:</b> Clicking on 'Track Your Order' link is Not navigated to Order Tracking page",
					driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			Log.message("<b>Expected Result:</b> Clicking on 'Shipping information' link should navigate to Shipping information page");
			Log.softAssertThat(
					homePage.footers.elementLayer.verifyPageElements(
							footerShippingInFormation, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Shipping information' link is navigated to Shipping information page",
					"<b>Actual Result:</b> Clicking on 'Shipping information' link is Not navigated to Shipping information page",
					driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			TermsAndConditionsPage termsandconditions = homePage.footers
					.navigateToPolicyGuideLine();
			Log.message((i++) + ". Navigated to Policy Guide Line Page!",
					driver.getCurrentUrl());

			Log.message("<b>Expected Result:</b> Clicking on 'Policy & Guidelines' link should navigate to Policy & Guidelines page");
			Log.softAssertThat(
					termsandconditions.elementLayer.verifyPageElements(
							termsAndConditionContent, termsandconditions)
							&& driver.getCurrentUrl().contains(
									"policy-guidelines"),
					"<b>Actual Result:</b> Clicking on 'Policy & Guidelines' link is navigated to Policy & Guidelines page",
					"<b>Actual Result:</b> Clicking on 'Policy & Guidelines' link is Not navigated to Policy & Guidelines page",
					driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}

			FaqPage faqPage = homePage.footers.navigateToFAQ();
			Log.message((i++) + ". Navigated to FAQ Page!",
					driver.getCurrentUrl());

			Log.softAssertThat(
					faqPage.getTextFromFAQSPage().contains(lblTFaqs), (i++)
							+ ". " + lblTFaqs
							+ " should be displayed in the FAQS page", lblTFaqs
							+ " is not displayed in the FAQS page");

			Log.message("<b>Expected Result:</b> Clicking on 'FAQS' link should navigate to FAQS page");
			Log.softAssertThat(
					faqPage.elementLayer
							.verifyPageElements(FAQContent, faqPage),
					"<b>Actual Result:</b> Clicking on 'FAQS' link is navigated to FAQS page",
					"<b>Actual Result:</b> Clicking on 'FAQS' link is Not navigated to FAQS page",
					driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");

			if ((runPltfrm == "mobile")) {
				homePage.footers.clickCustomerServiceLink();
			}
			homePage.footers.clickContactUsLink();
			Log.message((i++) + ". Clicked on 'Contact us' link ");
			Log.message("<b>Expected Result:</b> Clicking on 'Contact us' link should navigate to Contact Us page");
			Log.softAssertThat(
					homePage.footers.elementLayer.verifyPageElements(
							footerContact, homePage.footers),
					"<b>Actual Result:</b> Clicking on 'Contact us' link is navigated to Contact Us page",
					"<b>Actual Result:</b> Clicking on 'Contact us' link is Not navigated to Contact Us page",
					driver);

			homePage.footers.scrollToFooter();
			Log.message((i++) + ". Scroll to footer in the homepage");

			StoreLocationPage storeLocationPage = homePage.footers
					.navigateToStore();
			Log.message((i++) + ". Navigated to store Page!");

			Log.softAssertThat(storeLocationPage.verifyBreadcrumb(), "4. "
					+ lblStore + " should be displayed in the FAQS page",
					lblStore + " is not display in the FAQS page");

			Log.message("<b>Expected Result:</b> Clicking on 'Store Locations' link should navigate to Store Locations page");
			Log.softAssertThat(
					storeLocationPage != null,
					"<b>Actual Result:</b> Clicking on 'Store Locations' link is navigated to Store Locations page",
					"<b>Actual Result:</b> Clicking on 'Store Locations' link is Not navigated to Store Locations page",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT1_006

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Sitemap", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String runPltfrm = Utils.getRunPlatForm();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> headers = null;
		if (runPltfrm == "desktop") {
			headers = Arrays.asList("headerBannerBottomPromotion");
		} else if (runPltfrm == "mobile") {
			headers = Arrays.asList("headerBannerBottomPromotionMobile");
		}
		List<String> headers1 = Arrays.asList("headerBannerSiteWideCopy");
		List<String> siteMapContent = Arrays.asList("siteContent");
		List<String> leftNavigation = Arrays.asList("btnCustomerService",
				"btnNeedHelp");
		List<String> headerNavL2;
		List<String> siteMapL2;
		String level1 = testData.get("SearchKey");

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!", driver);
			headerNavL2 = homePage.headers.getL2Category(level1);
			Log.message("<b>Expected Result:</b> Header Banner 'Bottom Promotion' should be displayed in Home page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers,
							homePage.headers),
					"<b>Actual Result:</b> Header Banner 'Bottom Promotion' is displayed in Home page ",
					"<b>Actual Result:</b> Header Banner 'Bottom Promotion' is Not displayed in Home page",
					driver);
			Log.message("<b>Expected Result:</b> Header Banner 'Site Wide Copy' should be displayed in Home page");
			Log.assertThat(
					homePage.elementLayer.verifyPageElements(headers1,
							homePage.headers),
					"<b>Actual Result:</b> Header Banner 'Site Wide Copy' is displayed in Home page ",
					"<b>Actual Result:</b> Header Banner 'Site Wide Copy' is Not displayed in Home page",
					driver);

			SiteMapPage siteMapPage = homePage.footers.navigateToSiteMap();
			Log.message((i++)
					+ ". Clicked 'SiteMap on footer and navigated to SiteMap page");

			Log.assertThat(
					siteMapPage.elementLayer.verifyPageElements(siteMapContent,
							siteMapPage),
					(i++) + ". Site Map content is displayed as expected",
					"Site Map content is not displayed as expected, plz check the event log",
					driver);
			if (runPltfrm == "desktop") {
				Log.message("<b>Expected Result:</b> Site Map page should display Left navigation list");
				Log.assertThat(
						siteMapPage.elementLayer.verifyPageElements(
								leftNavigation, siteMapPage),
						"<b>Actual Result:</b> Site Map page displays all the Left navigation list",
						"<b>Actual Result:</b> Site Map page doesn't display all the Left navigation list",
						driver);
			}
			siteMapL2 = siteMapPage.getL2Category(level1);
			Log.message((i++) + ". Site map's " + level1
					+ "sub-categories are :: " + siteMapL2);

			Log.message("<b>Expected Result:</b> Site Map page should display all L1 & L2 Category list");
			Log.assertThat(
					Utils.compareTwoList(headerNavL2, siteMapL2),
					"Header "
							+ level1
							+ " <b>Actual Result:</b> sub-categories(L2) are displayed as same in sitemap page's sub-category(L2):: "
							+ siteMapL2,
					"<b>Actual Result:</b> Site map's "
							+ level1
							+ " sub-categories(L2) are not displayed as header's "
							+ level1 + " sub-categories(L2):: " + siteMapL2
							+ " , but expected is: " + headerNavL2, driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_018

	@Test(groups = { "mobile", "mobile_bat" }, description = "Verify Mobile – Bread Crumbs verifications for Plp", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT1_008(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		String runPltfrm = Utils.getRunPlatForm();
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		if (runPltfrm == "desktop") {
			throw new SkipException("Applicable only for mobile");
		}
		String l1_Category = testData.get("SearchKey").split("\\|")[0];
		String l2_Category = testData.get("SearchKey").split("\\|")[1];
		String l3_Category = testData.get("SearchKey").split("\\|")[2];
		String search_key = testData.get("SearchKey").split("\\|")[3];
		String stringMatches = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			Log.message("<b>Expected Result:</b> User should be navigate to - "
					+ l1_Category + " >> " + l2_Category + " >> " + l3_Category);
			homePage.headers.navigateTo(l1_Category, l2_Category, l3_Category);
			PlpPage plpPage = new PlpPage(driver).get();

			String url = driver.getCurrentUrl();
			Log.message("<br><b>Actual Result:</b>");
			Log.assertThat(
					Utils.verifyPageURLContains(driver,
							webSite.split("/s/")[0], stringMatches)
							&& (plpPage != null),
					"1. User is navigated to Product Landing Page and actual URL is: "
							+ url,
					"1. User is not navigated to Product Landing Page and actual URL is: "
							+ url);
			SearchResultPage searchResultPage = plpPage.headers
					.searchProductKeyword(search_key);
			List<String> breadcrumbList = searchResultPage
					.getTextInBreadcrumb();
			Log.assertThat(
					(breadcrumbList.get(0).equals("Back to " + l3_Category)),
					"2. Breadcrumb is displayed as expected - 'Back to "
							+ l3_Category + "'",
					"2. Breadcrumb is not displayed as expected- 'Back to Home' and the actual is: '"
							+ breadcrumbList.get(0) + "'", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_GLOBALNAV_105

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the user is able to Create Registry and the tabs after registry is created ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String username = testData.get("EmailAddress");
		String passwd = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			signIn.signInToMyAccount(username, passwd);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage registryPage = homePage.headers
					.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");

			RegistrySignedUserPage regSignedUser = registryPage
					.clickOnCreateRegistrybutton();
			Log.message("5. Clicked on Create New Registry");

			regSignedUser.fillingEventDetails("weddingRegistry");
			Log.message("6. Entered Registery Details");

			regSignedUser.clickContinueInCreateRegistry();
			Log.message("7. Clicked on Continue in step1");

			regSignedUser
					.fillingPreEventShippingdetails("valid_shipping_address6");
			regSignedUser
					.fillingPostEventShippingdetails("valid_shipping_address6");
			Log.message("8. Entered Post Event and Pre event shipping details");

			regSignedUser.clickOnContinueButtonInEventShipping();
			Log.message("9. Clicked on Continue in step 2");

			regSignedUser.clickOnCheckboxInStep3();
			regSignedUser.clickOnSubmitButton();
			Log.message("10. Entered all details and clicked on submit button");

			String runPltfrm = Utils.getRunPlatForm();
			if (runPltfrm == "mobile") {
				Log.assertThat(regSignedUser.elementLayer.verifyPageElements(
						Arrays.asList("btnAddToRegistry"), regSignedUser),
						"11.Registry Id is generated",
						"11.Registry Id is not generated");

			} else {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(
								Arrays.asList("btnMakeThisListPrivate"),
								regSignedUser),
						"11. My Registry Tab is be enabled",
						"11. My Registry Tab is not enabled");
			}
			String RegisterId = regSignedUser.getRegisterId();
			Log.message("The Register Id is :" + RegisterId);

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> The Registry should be created succesfully with 9 digit registry ID");
			Log.assertThat(
					regSignedUser.verifyRegisterIdFormat(RegisterId),
					"<b>Actual Result 1:</b> The Registry is created succesfully with 9 digit registry ID",
					"<b>Actual Result 1:</b> The Registry is not created succesfully with 9 digit registry ID");
			Log.message("<br>");

			if (Utils.getRunPlatForm() == "desktop") {
				regSignedUser.clickOnMyRegistry();
				Log.message("<br>");
				Log.message("<b>Expected Result 2:</b> My Registry Tab should be enabled");
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(
								Arrays.asList("lblRegistryConfirmation"),
								regSignedUser),
						"<b>Actual Result 2:</b> My Registry Tab is enabled",
						"<b>Actual Result 2:</b> My Registry Tab is not enabled");
			} else {
				Log.assertThat(
						regSignedUser.elementLayer.verifyPageElements(
								Arrays.asList("lblRegistryConfirmation"),
								regSignedUser),
						"<b>Actual Result 2:</b> My Registry Tab is enabled",
						"<b>Actual Result 2:</b> My Registry Tab is not enabled");
			}
			Log.message("<br>");

			regSignedUser.clickOnEventInfo();

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Event info Tab should be enabled");
			Log.assertThat(regSignedUser.elementLayer.verifyPageElements(
					Arrays.asList("lblRegistrationInfoInEventInfo"),
					regSignedUser),
					"<b>Actual Result 3:</b> Event info Tab is  enabled",
					"<b>Actual Result 3:</b> Event info Tab is not enabled");
			Log.message("<br>");

			regSignedUser.clickOnShippingInfo();

			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> Shipping info Tab should be enabled");
			Log.assertThat(regSignedUser.elementLayer.verifyPageElements(
					Arrays.asList("lblPreEventShipping"), regSignedUser),
					"<b>Actual Result 4:</b> Shipping info Tab is enabled",
					"<b>Actual Result 4:</b> Shipping info Tab is not enabled");
			Log.message("<br>");

			regSignedUser.clickOnPurchases();

			Log.message("<br>");
			Log.message("<b>Expected Result 5:</b> Purchases Tab should be enabled");
			Log.assertThat(regSignedUser.elementLayer.verifyPageElements(
					Arrays.asList("warningMsgInPurchase"), regSignedUser),
					"<b>Actual Result 5:</b> Purchases Tab is enabled",
					"<b>Actual Result 5:</b> Purchases Tab is not enabled");
			Log.message("<br>");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		}

	}// TC_BELK_BAT_SPRINT7_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify user add multiple items to their Registry and verify the items are displayed in registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_002(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] searchKey = testData.get("SearchKey").split("\\|");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");

			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");

			RegistrySignedUserPage registrySignedUserPage = (RegistrySignedUserPage) myAccountPage
					.navigateToSection("registry");
			Log.message("4. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("5. Clicked on View link");

			registrySignedUserPage.clickOnAddItemsToRegistry();
			Log.message("6. Clicked on add items to registry");

			Log.assertThat(
					driver.getCurrentUrl().contains("/home/"),
					"7. Navigate to home Page when Clicking on 'Add Items to Registry' button",
					"7. Not navigate to home Page when Clicking on 'Add Items to Registry' button");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey[0]);
			Log.message("8. Searched with '" + searchKey[0]
					+ "' in the search text box");
			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();

			String BrandName1 = pdpPage.getProductBrandName();
			String productName1 = pdpPage.getProductName();
			;
			productName1 = BrandName1.concat(" " + productName1);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Clicked on 'Add To Registry' link in PDP");

			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey[1]);
			Log.message("11. Searched with '" + searchKey[1]
					+ "' in the search text box");

			Log.message("12. Navigated to 'Pdp' Page with randomly selected product");

			pdpPage.selectSize();
			pdpPage.selectColor();
			pdpPage.selectQuantity();

			String BrandName2 = pdpPage.getProductBrandName();
			String productName2 = pdpPage.getProductName();
			productName2 = BrandName2.concat(" " + productName2);

			pdpPage.navigateToRegistryAsSignedUser();
			Log.message("13. Clicked on 'Add To Registry' link in PDP");

			pdpPage.headers.navigateToMyAccount();
			Log.message("14. Clicked on 'My Account' link from PDP");

			myAccountPage.navigateToSection("registry");
			Log.message("15. Clicked on Registry link");
			registrySignedUserPage.clickOnViewRegistrylnk();
			Log.message("16. Clicked on View link");

			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> "
					+ productName1
					+ " should be added in PDP and displayed in registry should be same");

			Log.assertThat(
					registrySignedUserPage
							.verifyProductAddedInRegistry(productName1),
					"<b>Actual Result 1:</b> '"
							+ productName1
							+ "' is added in PDP and displayed in registry are same",
					"<b>Actual Result 1:</b> '"
							+ productName1
							+ "' is not added in PDP and not displayed in registry");

			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> "
					+ productName2
					+ " should be added in PDP and displayed in registry should be same");

			Log.assertThat(
					registrySignedUserPage
							.verifyProductAddedInRegistry(productName2),
					"<b>Actual Result 2:</b> '"
							+ productName2
							+ "' is added in PDP and displayed in registry are same",
					"<b>Actual Result 2:</b> '"
							+ productName2
							+ "' is not added in PDP and not displayed in registry");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT7_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify update and Remove links are displayed in Registry With Products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_003(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message("7. Searched with keyword '" + searchKey
					+ "' and navigated to PDP Page");
			pdpPage.selectColor();
			Log.message("8. Color swatch Selected");
			pdpPage.selectSize();
			Log.message("9.  Size is Selected from size dropdown");
			pdpPage.selectQtyByIndex(2);
			Log.message("10. Quantity drowdown is selected with Quantity 2");
			String productName = pdpPage.getProductName();
			pdpPage.clickAddToBag();
			Log.message("11.Product is added to shopping bag");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("12.Navigated to Shopping bag Page");
			shoppingBagPage.clickOnRegistry(productName);
			Log.message("13. Navigated to Registry Page");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> Update and remove links should be available.");
			Log.assertThat(
					registryInformationPage.elementLayer.verifyPageElements(
							Arrays.asList("linkUpdate", "linkRemove"),
							registryInformationPage),
					"<b>Actual Result 1:</b> Update and remove links is available.",
					"<b>Actual Result 1:</b>  Update and remove links is not available.",
					driver);
			Log.message("<br>");
			registryInformationPage.clickupdate();
			Log.message("15. Clicked Update from the Active Registry");
			String Count = registryInformationPage.getQunatityCountOfProduct();
			registryInformationPage.selectSortByDropDownByIndex(5);
			Log.message("16.Changed the Quanity");
			String updatedCount = registryInformationPage
					.getQunatityCountOfProduct();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Quanity Count needs to be updated for the product in registry");
			Log.assertThat(
					!Count.equals(updatedCount),
					"<b>Actual Result 2:</b> Quanity Count is updated for the product in registry",
					"<b>Actual Result 2:</b> Quanity Count is not updated for the product in registry",
					driver);
			Log.message("<br>");

			registryInformationPage.removeItemsFromRegigistry();
			Log.message("17.Clicked On remove items from registry");

			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Product Item should be removed from registry");
			Log.assertThat(
					registryInformationPage.elementLayer
							.verifyPageElementsDoNotExist(
									Arrays.asList("linkRemove"),
									registryInformationPage),
					"<b>Actual Result 3:</b>Product Item is removed from registry",
					"<b>Actual Result 3:</b>Product Item is not removed from registry",
					driver);
			Log.message("<br>");

			Log.testCaseResult();
		}

		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT7_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Still Needs value displays in the Registry page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_005(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("4. Navigated to giftregistry page on Clicking Registry");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			registrySignedUserPage.clickAddItemsToRegistry();
			Log.message("6. Clicked on AddItemsToRegistry button");
			SearchResultPage searchResultPagehomePage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("7. Searched with keyword '" + searchKey
					+ "' and navigated to search result Page");
			PdpPage pdpPage = searchResultPagehomePage.selectProductByIndex(2);
			Log.message("8. Navigated to PDP Page from Search Results Page");
			pdpPage.selectSizeByIndex(1);
			Log.message("9.  Size is Selected from size dropdown");
			registrySignedUserPage = pdpPage.navigateToRegistryAsSignedUser();
			Log.message("10. Add to registry button is Clicked");
			pdpPage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("12. Clicked View from the Active Registry");
			RegistryInformationPage registryInformationPage = new RegistryInformationPage(
					driver).get();
			Log.message("<b>Expected Result:</b> Still Needs value should be a calculated value which should be the difference of the Would Love and Purchased values.");
			registrySignedUserPage.selectWouldLoveIndex(2);
			registryInformationPage.clickupdate();
			int wouldcount = Integer.parseInt(registrySignedUserPage
					.getWouldLoveSelectedvalue());
			int stillneed = Integer.parseInt(registryInformationPage
					.stillNeedsTxt());
			int qtypurshased = Integer.parseInt(registrySignedUserPage
					.getQtySelectedvalue());
			registryInformationPage.clickOnAddtoBag();
			Log.message("13. Purchase [" + qtypurshased + "]"
					+ "of the product");
			ShoppingBagPage shoppingBagPage = registryInformationPage
					.clickOnMiniCart();
			Log.message("14. Navigated to shopping Cart Page");
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			checkoutPage.clickBtnShippingPage();
			Log.message("15. Clicked Continue button");
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("16. Payment details Entered");
			checkoutPage.clickBtnShippingPage();
			Log.message("17. Clicked Continue button");
			Log.message("18. Order Placed with [" + qtypurshased + "]");
			OrderConfirmationPage orderConfirmationPage = checkoutPage
					.placeOrder();
			GiftRegistryPage giftRegistryPage = orderConfirmationPage.headers
					.navigateToGiftRegistry();
			registrySignedUserPage = new RegistrySignedUserPage(driver).get();

			registrySignedUserPage = new RegistrySignedUserPage(driver).get();
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("19. Clicked View from the Active Registry");
			registryInformationPage = new RegistryInformationPage(driver).get();
			int stillneeds = Integer.parseInt(registryInformationPage
					.stillNeedsTxt());

			Log.softAssertThat(
					stillneeds == wouldcount - qtypurshased,
					"<b>Actual Result1:</b> Still Needs value is the difference of the Would Love and Purchase values",
					"<b>Actual Result1:</b> Still Needs value is not the difference of the Would Love and Purchase values",
					driver);
			registryInformationPage.removeItemsFromRegigistry();
			Log.testCaseResult();

		} catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// // TC_BELK_REGISTRY_099

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify page redirected to Registry Page", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signIn = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to Sign In Page!");
			// Navigate to My Account Page
			MyAccountPage myAccountPage = signIn.signInToMyAccount(emailid,
					password);
			Log.message("3. Navigated to My Account Page!");
			myAccountPage.headers.navigateToGiftRegistry();
			Log.message("4. Clicked on Registry Link from Header");
			RegistrySignedUserPage registrySignedUserPage = new RegistrySignedUserPage(
					driver).get();
			Log.message("<b>Expected Result:</b> Clicking on Registry link from Header page should be redirected to registry page");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage),
					"<b>Actual Result:</b> Page is redirected to  registry page",
					"<b>Actual Result:</b> Page is not redirected to registry page",
					driver);
			myAccountPage.headers.navigateToMyAccount();
			Log.message("5. Click on Myaccounts link from header");

			Log.message("6. Cliked on Registry Link from MyAccounts");
			myAccountPage.ClickOnRegistryicon();
			Log.message("<b>Expected Result:</b> Clicking on Registry link from My Accounts should be redirected to registry page");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage),
					"<b>Actual Result:</b> Page is redirected to  registry page",
					"<b>Actual Result:</b> Page is not redirected to registry page",
					driver);
			myAccountPage.headers.navigateToMyAccount();
			Log.message("7. Click on Myaccounts link from header");
			myAccountPage.navigateToSection("registry");
			Log.message("8. Click On Registry Link From My Accounts Left Navigation");
			Log.message("<b>Expected Result:</b> Clicking on Registry link from My Accounts Left Navigation should be redirected to registry page");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("btnCreateNewRegistry"),
							registrySignedUserPage),
					"<b>Actual Result:</b> Page is redirected to  registry page",
					"<b>Actual Result:</b> Page is not redirected to registry page",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT7_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify View Rregistry and Delete Registry", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT7_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String registryInfo = testData.get("RegistryInfo");
		String shippingInfo = testData.get("ShippingInfo");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signIn = homePage.headers.navigateToGiftRegistryAsGuest();
			Log.message("2. Navigated to Sign In Page!");

			// / Navigate to My Account Page
			RegistrySignedUserPage registrySignedUserPage = signIn
					.signInToMyRegistryAccount(emailid, password);
			Log.message("3. Navigated to My Account Page!");
			GiftRegistryPage giftregistry = homePage.headers
					.navigateToGiftRegistry();
			Log.message("4. Navigated to Registry Page");
			if (Utils.getRunPlatForm() != "mobile") {
				registrySignedUserPage.viewRegistryByIndex(1);
			} else {
				registrySignedUserPage.viewRegistryByIndexMobile(1);
			}
			Log.message("5. Clicked View from the Active Registry");

			Log.message("<b>Expected Result :</b> The following options should be available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!");
			Log.assertThat(
					registrySignedUserPage.elementLayer.verifyPageElements(
							Arrays.asList("prioritylbl", "Qtylbl", "btnadd",
									"btnEdit", "btnRemove",
									"btnMakeListPrivate"),
							registrySignedUserPage),
					"<b>Actual Result :</b> All options are available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!",
					"<b>Actual Result :</b> All options are not available available to the user: Add,Edit,Remove Items,Change Qty,Priority,Make registry public/private,Make items public/private!");
			homePage.headers.navigateToGiftRegistry();
			Log.message("6. Navigated to Registry Page");
			RegistrySignedUserPage registrySignIn = giftregistry
					.clickOnCreateRegistrybutton();
			Log.message("7. Clicked on Create New Registry button");

			// Enter the valid Registry details
			registrySignIn.fillingRegistryDetails("weddingRegistry");
			// Click on Continue button in Step 1
			registrySignIn.clickContinueInCreateRegistry();
			Log.message("8. Filled the valid Registry details and Clicked on Continue button in Create Registry page");

			// Update the PreEvent Address and Fill Post Event address in Step 2
			registrySignIn
					.fillingEventShippingdetails("valid_shipping_address6");
			registrySignIn.clickOnContinueButtonInEventShipping();
			Log.message("9. Filled the valid Event details and Clicked on Continue button in Event Registry page");

			registrySignIn.clickOnSubmitButton();
			Log.message("10. Clicked on submit button in the Registry Page");

			homePage.headers.navigateToGiftRegistry();
			Log.message("11. Navigated to Registry Page");
			String firstregidbeforedelete = registrySignIn
					.getRegistryidByindex();
			Log.message("regid:" + firstregidbeforedelete);
			registrySignedUserPage.clickOnDelete();
			Log.message("12. Clicked on delete link!");
			Log.message("<b>Expected Result 1:</b> Added Registry Modal should be displayed in My registry screen with Yes and Cancel button.");
			registrySignedUserPage.clickOnYesInPopUp();
			Log.message("7. Clicked on Yes Button in Pop Up!");
			String firstregidafterdelete = registrySignIn
					.getRegistryidByindex();
			Log.message("regid:" + firstregidafterdelete);
			Log.message("<b>Expected Result 1:</b> Yes ? successfully deletes the registry from the Customer’s account.");
			Log.assertThat(
					!firstregidbeforedelete.equals(firstregidafterdelete),
					"<b>Actual Result :</b> Yes ? successfully deletes the registry from the Customer’s account, closes the modal and returns the customer to the My Registries page.!",
					"<b>Actual Result :</b> Yes ? unsuccessfully not deletes the registry from the Customer’s account, closes the modal and not returns the customer to the My Registries page.!",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			// driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT7_007

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the breadcrumb should contain the product name and the brandname should be linkable, It should redirected to PLP when click on it", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		ArrayList<String> breadcrumblist;
		String searchKey = testData.get("SearchKey");
		String[] category = searchKey.split("\\|");
		String l1category = category[0];
		String l2category = category[1];
		String l3category = category[2];
		String breadcrumblastvalue;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(l1category, l2category, l3category);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ l1category
					+ ") -> L2-Category("
					+ l2category
					+ ") -> L3-Category(" + l3category + ")");
			PlpPage plpPage = new PlpPage(driver).get();
			PdpPage pdppage = plpPage.navigateToPDP();
			Log.message("3. Navigated from PLP Page to PDP Page by selecting the first Product");

			breadcrumblist = pdppage.getTextInBreadcrumb();
			Log.message("4. Getting the Product's bread crumb");
			int index = 0;
			if (Utils.getRunPlatForm() != "mobile") {
				index = breadcrumblist.size() - 2;
			} else {
				index = breadcrumblist.size() - 1;
			}
			plpPage = pdppage.clickCategoryByNameInBreadcrumb(breadcrumblist
					.get(index));
			Log.message("5. Clicking on the Product's Breadcrumb '"
					+ breadcrumblist.get(index) + "' from PDP Page");
			breadcrumblist = plpPage.getTextInBreadcrumb();
			index = breadcrumblist.size() - 1;
			breadcrumblastvalue = breadcrumblist.get(index);
			Log.message("<b>Expected Result:</b>Clicking on the Product's Last BreadCrumb from the PDP Page should be navigated to PLP Page.");
			if (Utils.getRunPlatForm() != "mobile") {
				Log.assertThat(
						breadcrumblastvalue.equalsIgnoreCase(l3category),
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page successfully navigated to PLP page.",
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page failed to navigated to  PLP page.",
						driver);
			} else {
				Log.assertThat(
						breadcrumblastvalue.equalsIgnoreCase("Back to Home"),
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page successfully navigated to PLP Page.",
						"<b>Actual Result:</b>Clicking on Product's Last BreadCrumb from the PDP Page failed to navigated to PLP Page.",
						driver);
			}
			pdppage = plpPage.selectProductByIndex(1);
			Log.message("2. Selected 1st product and navigated to PDP page");
			String brandName = pdppage.getTextFromBrand();
			Log.message("<br>");
			Log.message("<b>Expected Result</b> The product's brand name with hyperlink should be displayed on product details page");
			String urlBeforeClicking = driver.getCurrentUrl();
			SearchResultPage searchResultPage = pdppage.clickBrandname();
			Log.assertThat(!(urlBeforeClicking.equals(driver.getCurrentUrl())),
					"<b>Actual Result:</b> The product's brand name '"
							+ brandName
							+ "' is displayed as a hyperlink in PDP",
					"<b>Actual Result:</b> The product's brand name '"
							+ brandName
							+ "' is not displayed as a hyperlink in PDP",
					driver);
			Log.message("<b>Expected Result</b> Clicking on brand link, it should navigate to Search Result Page");
			Log.assertThat(searchResultPage != null,
					"<b>Actual Result:</b> Clicking on brand link '"
							+ brandName
							+ "', it is navigated to Search Result Page",
					"<b>Actual Result:</b> Clicking on brand link '"
							+ brandName
							+ "', it is navigated to Search Result Page",
					driver);
			Log.testCaseResult();
		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally
	}// TC_BELK_PDP_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the HOVER TO ZOOM and CLICK TO ENLARGE functionality and Verify the Product left, right nav", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_002(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		List<String> elements = Arrays.asList("btnPreviousImage",
				"btnNextImage");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		List<String> clickToEnlarge = Arrays.asList("lnkClickToEnlarge");
		List<String> zoomLinks = Arrays.asList("txtHoverToZoom");

		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message((i++)
					+ ". Navigated to Search Result Page with Keyword : "
					+ searchKey);

			// Load QuickView Page
			String color = pdpPage.selectColor();
			Log.message((i++)
					+ ". Navigated to PDP of the product selected randomly");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> 'Click to enlarge' link should be displayed in PDP ");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(clickToEnlarge,
							pdpPage),
					"<b>Actual Result1:</b> 'Click to enlarge' link is displayed in PDP",
					"<b>Actual Result1:</b> 'Click to enlarge' link is not displayed in PDP",
					driver);

			if (runPltfrm == "desktop") {

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> 'Hover to zoom' link should be displayed in PDP ");

				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(zoomLinks,
								pdpPage),
						"<b>Actual Result2:</b> 'Hover to zoom' link is displayed in PDP",
						"<b>Actual Result2:</b> 'Hover to zoom' link is not displayed in PDP",
						driver);
			}

			Log.message("<br>");
			Log.message("<b>Expected Result-1:</b> The product should be having more than 3 thumbnail images");
			Log.assertThat(
					pdpPage.getCountOfThumbnailImages() >= 4,
					"<b>Actual Result-1:</b> This product have more than 3 thumbnail images!",
					"<b>Actual Result-1:</b> This product is not have more than 3 thumbnail images");

			pdpPage.clickImageNavArrowButton("Next");
			Log.message("<br>");
			Log.message((i++) + ". Clicked on next navigation arrow");
			Log.message("<br>");
			Log.message("<b>Expected Result-2:</b> On clicking the next navigation arrow, the system should slide 1 position to the right (>)");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-2:</b> The system slides 1 position to the right (>)",
					"<b>Actual Result-2:</b> The system does not slide 1 position to the right (>)",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result-3:</b> Scroll arrows '<' and '>' should be displayed when 4 or more alternate images or videos exist");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(elements, pdpPage),
					"<b>Actual Result-3:</b> Scroll arrows '<' and '>' are displayed when 4 or more alternate images or videos exist",
					"<b>Actual Result-3:</b> Scroll arrows '<' and '>' are not displayed when 4 or more alternate images or videos exist");

			pdpPage.clickImageNavArrowButton("Previous");
			Log.message("<br>");
			Log.message((i++) + ". Clicked on previous navigation arrow");
			Log.message("<br>");
			Log.message("<b>Expected Result-4:</b> On clicking the previous navigation arrow, the system should slide 1 position to the left (<)");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("btnPreviousImage"), pdpPage),
					"<b>Actual Result-4:</b> The system slides 1 position to the left (<)",
					"<b>Actual Result-4:</b> The system does not slide 1 position to the left (<)",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> 'Click to enlarge' link should be displayed in PDP ");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(clickToEnlarge,
							pdpPage),
					"<b>Actual Result1:</b> 'Click to enlarge' link is displayed in PDP",
					"<b>Actual Result1:</b> 'Click to enlarge' link is not displayed in PDP",
					driver);

			if (runPltfrm == "desktop") {

				Log.message("<br>");
				Log.message("<b>Expected Result2:</b> 'Hover to zoom' link should be displayed in PDP ");

				Log.assertThat(
						pdpPage.elementLayer.verifyPageElements(zoomLinks,
								pdpPage),
						"<b>Actual Result2:</b> 'Hover to zoom' link is displayed in PDP",
						"<b>Actual Result2:</b> 'Hover to zoom' link is not displayed in PDP",
						driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT3_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify all the elements availability (color, qty, find a store, add to shopping bag, brand name, productname, prices, images and its nav arrow, social sharing, Add to registry, Add to wishlist and Description)", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_004(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message((i++)
					+ ". Navigated to Search Result Page with Keyword : "
					+ searchKey);

			// Load QuickView Page
			Log.message((i++)
					+ ". Navigated to PDP of the product selected randomly");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> Verify all the elements availability (color, qty, find a store, add to shopping bag, brand name, productname, prices, images and its nav arrow, social sharing, Add to registry, Add to wishlist and Description)");
			Log.message("<b>Actual Result1:</b>");
			int j = 1;
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("imgInPdp"), pdpPage),
					(j++)
							+ ". Product primary image is displayed is displayed in PDP",
					(j++) + ". Product primary image is not displayed in PDP");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkBrandName"), pdpPage), (j++)
							+ ". Brand name link is displayed in PDP", (j++)
							+ ". Brand name link is not displayed in PDP");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lblProductName"), pdpPage), (j++)
							+ ". Product name is displayed in PDP", (j++)
							+ ". Product name is not displayed in PDP");
			if (runPltfrm == "desktop") {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(
						Arrays.asList("txtDescriptionPdpdesktop"), pdpPage),
						(j++) + ". Product Description is displayed in PDP",
						(j++) + ". Product Description is not displayed in PDP");
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(
						Arrays.asList("txtDescriptionPdpmobile"), pdpPage),
						(j++) + ". Product Description is displayed in PDP",
						(j++) + ". Product Description is not displayed in PDP");
			}
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(Arrays.asList(
							"lnkFacebook", "lnkTwitter", "lnkGooglePlus",
							"lnkPintrest"), pdpPage),
					(j++)
							+ ". Social Media Icons (Facebook, Twitter, GooglePlus, Pinterest) is displayed in PDP",
					(j++)
							+ ". Social Media Icons (Facebook, Twitter, GooglePlus, Pinterest) is not displayed in PDP");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageListElements(
							Arrays.asList("lstListOfColor"), pdpPage), (j++)
							+ ". Color is displayed in PDP", (j++)
							+ ". Color is not displayed in PDP", driver);
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lblQuantity"), pdpPage), (j++)
							+ ". Quantity is displayed in PDP", (j++)
							+ ". Quantity is not displayed in PDP");
			// ((JavascriptExecutor) driver)
			// .executeScript("window.scrollTo(0, document.body.scrollHeight)/2");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPrice"), pdpPage), (j++)
							+ ". Product price is displayed in PDP", (j++)
							+ ". Product price is not displayed in PDP");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElementsDisabled(
							Arrays.asList("btnAddtoBag"), pdpPage),
					(j++) + ". Add To Shopping Bag button is displayed in PDP",
					(j++)
							+ ". Add To Shopping Bag button is not displayed in PDP");
			pdpPage.addProductToBag();
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("addItemToRegistry"), pdpPage), (j++)
							+ ". Add To Registry button is displayed in PDP",
					(j++) + ". Add To Registry button is not displayed in PDP");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkAddToWishList"), pdpPage), (j++)
							+ ". Add To Wishlist button is displayed in PDP",
					(j++) + ". Add To Wishlist button is not displayed in PDP");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT3_004

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Add to bag functionality", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_005(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			// Load the HomePage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message((i++)
					+ ". Navigated to Search Result Page with Keyword : "
					+ searchKey);

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> Add To bag button should be disabled before selecting color and size");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElementsDisabled(
							Arrays.asList("btnAddtoBag"), pdpPage),
					"<b>Actual Result1:</b> Add To bag button is disabled before selecting color and size",
					"<b>Actual Result1:</b> Add To bag button is not disabled before selecting color and size");

			String color = pdpPage.selectColor();
			Log.message((i++) + ". Selected color: '" + color + "'");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> Add To bag button should be disabled before selecting size");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElementsDisabled(
							Arrays.asList("btnAddtoBag"), pdpPage),
					"<b>Actual Result1:</b> Add To bag button is disabled before selecting size",
					"<b>Actual Result1:</b> Add To bag button is disabled before selecting size");

			String size = pdpPage.selectSize();
			Log.message((i++) + ". Selected size: '" + size + "'");

			Log.message("<br>");
			Log.message("<b>Expected Result1:</b> Add To bag button should be enabled after selecting color and size");
			Log.softAssertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("btnAddtoBag"), pdpPage),
					"<b>Actual Result1:</b> Add To bag button is enabled after selecting color and size",
					"<b>Actual Result1:</b> Add To bag button is enabled after selecting color and size");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT3_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Surcharge message is displayed on the Shopping Bag page and Verify  the billing details .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_008(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {

			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

			} else {
				myAccountPage.headers.clickSignOut();

			}
			pdpPage = homePage.headers.searchAndNavigateToPDP(searchKey);
			Log.message("2. Searched with '" + searchKey + "'");
			Log.message("3. Navigated to PDP");

			String selectedSize = pdpPage.selectSize();
			Log.message("4. Selected size: '" + selectedSize
					+ "' from color swatch in the PDP page");

			String colorName = pdpPage.selectColor();
			Log.message("5. Selected color: '" + colorName
					+ "' from color swatch in the PDP page");

			String qty = pdpPage.selectQuantity();
			Log.message("6. Select the Quantity: " + qty);

			pdpPage.clickAddToBag();
			Log.message("7. Add the Product to the bag");

			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message("8. Naviagte to Shopping bag");
			String surcharhehgettxt = shoppingBagPage.surchargeTxtDisplay();
			Log.message("9. The Shipping Surcharge is: " + surcharhehgettxt);

			Log.message("<br>");

			Log.message("<b> Expected Result 1: </b> This item will incur a shipping surcharge.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("txtSurchargeOnCartPage"),
							shoppingBagPage),
					"<b> Actual Result 1: </b> \"Shipping Surcharge\" is displayed in the Shopping Bag page and the text is: "
							+ surcharhehgettxt,
					"<b> Actual Result 1: </b> \"Shipping Surcharge\" is not displayed in the Shopping Bag page.",
					driver);

			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10. Clicked on Checkout in Order summary Page");

			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage
					.clickOnCheckoutAsUser(emailid, password);
			Log.message("11. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Standard", "valid_address1");
			Log.message("12. Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue in shipping");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES",
					"valid_address2");
			Log.message("14. filled billing address");

			checkoutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("15. filled card details");
			LinkedHashMap<String, String> getTheBillingDetials = checkoutPage
					.getBillingAddressDetails();
			Log.message("<b>Expected Result 2: </b> Verify the billing details on the billing page!");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
							"txtAddress_1InBilling", "txtZipcodeInBilling",
							"txtCityInBilling"), checkoutPage),
					"<b>Actual Result 2: </b> The Billing Details is Displayed and the Details are ::"
							+ getTheBillingDetials,
					"<b>Actual Result 2: </b> The Billing Details is not Displayed!",
					driver);
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. clicked on continue button");

			Log.message("<br>");

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT5_008

	@Test(groups = { "desktop_bat" }, description = "Verify badges in Quick View screen", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_007(String browser) throws Exception {
		String runPltfrm = Utils.getRunPlatForm();
		if (runPltfrm == "mobile")
			throw new SkipException(
					"This testcase is only applicable for desktop");
		else {
			HashMap<String, String> testData = TestDataExtractor.initTestData(
					workbookName, sheetName);

			String searchKey = testData.get("SearchKey");
			final WebDriver driver = WebDriverFactory.get(browser);
			Log.testCaseInfo(testData);

			try {
				HomePage homePage = new HomePage(driver, webSite).get();
				Log.message("1. Navigated to 'Belk' Home Page!");

				SearchResultPage searchresultPage = homePage
						.searchProductKeyword(searchKey);

				String productNameInSRP = searchresultPage.getProductName(1);
				Log.message("2. Searched with the  '"
						+ searchKey
						+ "' in search text box and navigated to 'Search Result'  Page!");
				QuickViewPage quickview = searchresultPage
						.navigateToQuickViewByIndex(1);
				Log.message("3. Clicked on Quick View button");
				String productNameInQuickView = quickview.getBrandName();
				quickview.selectSize();
				LinkedHashMap<String, String> productInQuickView = quickview
						.getProductDetails();
				Log.message("<br><b>Expected Result: </b>Product name and in QuickView Page should be same as in SRP");
				Log.message("<b>Actual Result: </b>");

				Log.softAssertThat(
						productNameInSRP.contains(productNameInQuickView),
						"Brand name in QuickView Page : '"
								+ productNameInQuickView
								+ "' is same as in SRP : '" + productNameInSRP
								+ "'", "2. Brand name in QuickView Page : '"
								+ productNameInQuickView
								+ "' is not same as in SRP : '"
								+ productNameInSRP + "'");
				String color = quickview.selectColor();
				Log.message("<br>");
				Log.message("4. Selected color: '" + color + "'");
				String size = quickview.selectSize();
				Log.message("5. Selected size: '" + size + "'");
				quickview.clickAddToBag();
				Log.message("6. Clicked 'Add To Bag' button");

				ShoppingBagPage shoppingBagPage = searchresultPage.headers
						.NavigateToBagPage();

				Log.message("7. Navigated to shopping bag page");
				LinkedList<LinkedHashMap<String, String>> pdtDetails = shoppingBagPage
						.getProductDetails();
				Log.message("<br><b>Expected Result: </b>The Product name should same as in the Quick View page");
				Log.assertThat(
						pdtDetails.get(0).get("ProductName")
								.equals(productInQuickView.get("ProductName")),
						"<b>Actual Result: </b>The product name is same as in the Quick View ("
								+ pdtDetails.get(0).get("ProductName") + "=="
								+ (productInQuickView.get("ProductName")) + ")",
						"<b>Actual Result: </b>The product name is not same as in the Quick View ("
								+ pdtDetails.get(0).get("ProductName") + "!="
								+ (productInQuickView.get("ProductName")) + ")",
						driver);

				Log.testCaseResult();
			} // try
			catch (Exception e) {
				Log.exception(e, driver);
			} // catch
			finally {
				Log.endTestCase();
				driver.quit();
			} // finally
		}
	} // TC_BELK_BAT_SPRINT3_007

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Free  Shipping Message on Cart Page and Shipping Billing details on Place order page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey[] = testData.get("SearchKey").replace("S_", "")
				.split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Navigate to Belk homepage
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = null;
			// To search 2 different products like jeans and pants
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {
				pdpPage = homePage.headers
						.searchAndNavigateToPDP(searchKey[searchIndex]);
				Log.message("2. Searched with '" + searchKey + "'");
				Log.message("3. Navigated to PDP");

				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message("4. Selected size: '" + selectedSize
						+ "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message("5. Selected color: '" + colorName
						+ "' from color swatch in the PDP page");

				pdpPage.selectQtyByIndex(2);
				Log.message("6. Select the Quantity: ");

				pdpPage.clickAddToBag();
				Log.message("7. Add the Product to the bag");

			}
			// Navigated to shoppingbag page after clicking on minicart icon in
			// pdp page
			ShoppingBagPage shoppingbagpage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to 'Shopping bag' page. ");

			Log.message("</br>");
			Log.message("<b>Expected Result 1:</b> Verify Free Shipping Message is displayed On the cart.");
			if (Utils.getRunPlatForm() == "desktop") {

				String freeShipText = shoppingbagpage
						.getTextOfFreeShippingMessage();
				Log.assertThat(
						shoppingbagpage.elementLayer.verifyPageElements(
								Arrays.asList("promoFreeShipDsk"),
								shoppingbagpage),
						"<b>Actual Result 1:</b> 'Free Shipping Message is displayed on the Shopping Page as:"
								+ freeShipText,
						"<b>Actual Result 1:</b> 'Free Shipping Message is not displayed on the Shopping Page");
			}
			if (Utils.getRunPlatForm() == "mobile") {

				String promomsg = shoppingbagpage
						.getTextOfFreeShippingMessageMob();
				Log.assertThat(
						shoppingbagpage.elementLayer.verifyPageElements(
								Arrays.asList("promoFreeShipMob"),
								shoppingbagpage),
						"<b>Actual Result 1:</b> 'Free Shipping Message is displayed on the Shopping Page as:"
								+ promomsg,
						"<b>Actual Result 1:</b> 'Free Shipping Message is not displayed on the Shopping Page",
						driver);
			}
			Log.message("</br>");
			// Navigated to signIn page after clicking on checkout in order
			// summary
			SignIn signinpage = (SignIn) shoppingbagpage
					.clickOnCheckoutInOrderSummary(shoppingbagpage);
			Log.message("9. Clicked on Checkout in Order summary Page");

			// Clicking on checkout as Guest user in sign in page
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("10. Clicked on Checkout As Guest User in Sign In Page");

			// Filling the shipping details as a GuestUser
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Standard");
			Log.message("11. Entered Shipping Address Details as Guest");

			// clicking on continue in shipping page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.clickContinueInAddressValidationModal();
			checkoutPage.clickOnContinueInShipping();
			Log.message("12. Clicked on Continue in Shipping Address Details as Guest");

			// Filling billing address details as a guest user
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("13. Billing address details are filled in billing page!");

			// Filling the CardDetails in the billing page
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("14. Card details are fillied with 'Visa' in the billing page!");

			// After filling all the details in the billing page, click on the
			// continue button
			checkoutPage.clickOnContinueInBilling();
			Log.message("15. Clicked on Continue in Billing page! ");

			String shippingAddr = checkoutPage.getShippingDetailsInPlaceOrder();

			String billingAddr = checkoutPage.getBillingDetailsInPlaceOrder();

			Log.message("</br>");
			Log.message("<b>Expected Result 2:</b> Verify the Shipping And Billing details on the Place Order Page.");

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("shippingModule"), checkoutPage),
					"<b>Actual Result 2a:</b> 'Shipping Module is displayed on the Place Order Page with Shipping Details :"
							+ shippingAddr,
					"<b>Actual Result 2a:</b> 'Shipping Module is not displayed on the Place Order Page with Shipping Details ",
					driver);

			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(
							Arrays.asList("billingModule"), checkoutPage),
					"<b>Actual Result 2b:</b> 'Billing Module is displayed on the Place Order Page with Billing Details :"
							+ billingAddr,
					"<b>Actual Result 2b:</b> 'Billing Module is not displayed on the Place Order Page with Billing Details ",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT5_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify  clicking on the cancel button navigate to the cart page .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = testData.get("SearchKey").split("\\S_")[1];
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Load Sign In Page
			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			ShoppingBagPage bagPage = null;
			SearchResultPage srch = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

				srch = signInPage.headers.searchProductKeyword(searchKey);

			} else {
				myAccountPage.headers.clickSignOut();

				srch = signInPage.headers.searchProductKeyword(searchKey);
			}

			Log.message("2. Searched with '" + searchKey + "'");
			PdpPage pdpPage = srch.selectProductByIndex(1);

			Log.message("3. Navigated to PDP!");
			String getPromoCallOutMessage = pdpPage
					.getPromotionalCalloutMessage();
			Log.message("The CallOutMessage is :" + getPromoCallOutMessage);
			Log.message("<b>Expected Result 1:</b> call out message should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("calloutMessage"), pdpPage),
					"<b>Actual Result 1:</b> call out message displayed '"
							+ getPromoCallOutMessage,
					"<b>Actual Result 1:</b> call out message is not displayed");
			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity("2");
			Log.message("6. selected the '" + quantity + "' quantity !");
			// Adding item to the shopping bag
			String pdtName = pdpPage.getProductName();
			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			Log.message("<b>Expected Result 2:</b> Deal Based Product should added to the bag");
			Log.assertThat(
					shoppingBagPage.getProductName(pdtName),
					"<b>Actual Result 2 :</b> Deal Based Product added to the bag",
					"<b>Actual Result 2:</b> Deal Based Product not added to the bag");
			// clicking on checkout button in order summary
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage
					.clickOnCheckoutAsUser(emailid, password);
			Log.message("10. Entered credentials(" + emailid + "/" + password
					+ ") and navigating to 'My Account' page", driver);
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("12. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails1 =
			checkoutPage
					.fillingAddressDetailsInMultipleShipping("valid_address7");
			// checkoutPage.clickOnSaveInAddEditAddressModal();
			boolean addressvalidation = checkoutPage
					.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("13. Filled shipping Address for 1st Product and clicked save button");

			checkoutPage.clickOnAddInShipToMulipleAddress();
			Log.message("14. Clicked Add button on multiship step1 page");
			// LinkedHashMap<String, String> shipDetails2 =
			checkoutPage
					.fillingAddressDetailsInMultipleShipping("valid_address6");
			// checkoutPage.clickOnSaveInAddEditAddressModal();
			addressvalidation = checkoutPage.isAddressValidationModalShowing();

			if (addressvalidation == true) {
				checkoutPage.chooseOriginalAddressInModal();
				checkoutPage.ContinueAddressValidationModalInMultipleShipping();
			}

			Log.message("15. filled address for 2nd product and clicked save button");
			checkoutPage.selectFirstAddressInAddressDropDown();
			// checkoutPage.selectdropdownandAddress(1, 2);
			Log.message("16. chosen the address in dropdown for 1st product");

			// checkoutPage.selectdropdownandAddress(2, 1);
			Log.message("17. chosen the address in dropdown for 2nd product");
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("18. Clicked on continue button in multiaddress ");

			checkoutPage.clickOnContinueInShipping();
			Log.message("19. Clicked on continue button in multiShipping page");
			// filling billing address
			checkoutPage.fillingBillingDetailsAsSignedInUser("No", "No",
					"valid_address2");
			Log.message("20. filled billing address");
			checkoutPage.fillingCardDetails("No", "card_BelkRewardsCreditCard");
			Log.message("21. filled card Details");
			// clicking continue in billing page

			checkoutPage.clickOnContinueInBilling();
			Log.message("22. Clicked continue button in billing page");
			// click on place order button
			checkoutPage.clickOnCancelButtonInPlaceOrder();
			Log.message("23. Clicked Place Order button");

			// / Navigate to My Account Page

			Log.message("<b>Expected Result 3: </b> When clicking on the cancel button it should navigate to the cart page");
			Log.assertThat(
					driver.getCurrentUrl().contains("Cart-Show"),
					"<b>Actual Result 3: </b> clicking on the cancel button navigate to the cart page",
					"<b>Actual Result 3: </b> clicking on the cancel button not navigate to the cart page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT5_003

	@Test(groups = { "desktop", "mobile", "tablet" }, description = "Verify an error message for invalid or Empty CVV in the Express Checkout process.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String CvvErrorMsg = "Please enter CVV number.";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			// Load the HomePage

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			// Navigating to SignIn Page from Home page
			SignIn signinPage = homePage.headers.navigateToSignIn();
			Log.message("2. Navigated to 'Sign In' Page from Home Page!");

			// Log In with valid Credentials
			MyAccountPage myAccountPage = signinPage.signInToMyAccount(emailid,
					password);
			Log.message("3. Login with Valid Credentials( " + emailid + " / "
					+ password + " )");

			// deleting the item from cart
			ShoppingBagPage shoppingBagPage = null;
			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {
				shoppingBagPage = myAccountPage.clickOnMiniCart();
				shoppingBagPage.removeItemsFromBag();
			}
			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = myAccountPage.headers
					.searchProductKeyword(searchKey);
			Log.message("4. Searched with keyword '" + searchKey
					+ "' and Navigated to 'Search Result' Page!");

			// Navigating to Pdp Page
			PdpPage pdppage = searchresultPage.navigateToPDP();
			Log.message("5. Navigated to PDP!");

			// selecting color from color swatch in Pdp Page
			Log.message("6. Selected color:'" + pdppage.selectColor()
					+ "' from color swatch in the Pdp.");

			// selecting size from Size Dropdown in Pdp Page

			Log.message("7. Selected size:'" + pdppage.selectSize()
					+ "' from Size dropdown in the Pdp.");

			// Adding product to bag
			pdppage.clickAddToBag();
			Log.message("8. Clicked on 'Add to bag' button");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage1 = pdppage.clickOnMiniCart();
			Log.message("9. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Clicking Checkout Button in Shopping Bag Page
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPage1
					.clickOnCheckoutInOrderSummary(shoppingBagPage1);
			Log.message("10. Clicked on 'Check out' Button In Shopping Bag Page!");

			checkoutPage.clickOnApplyButton();
			Log.message("11. Clicked on 'Apply' Button In Place Order Page!");
			checkoutPage.getTextfromCvvErrorMsg();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> Error message should be displayed when customer click on Apply button with invalid or Empty CVV field.");
			Log.assertThat(
					(checkoutPage.getTextfromCvvErrorMsg()
							.contains(CvvErrorMsg)),
					"<b>Actual Result:</b>  Error message is displayed when customer click on Apply button with invalid or Empty CVV field.",
					"<b>Actual Result1:</b> Error message is not displayed when customer click on Apply button with invalid or Empty CVV field.",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();

			driver.quit();
		} // finally
	}

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the search suggestion functionality for 2, 3 characters", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_001(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchKey = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey[0]);
			Log.message((i++) + ". Entered 2 characters '" + searchKey[0]
					+ "' in the search box");
			Log.message("<br>");
			BrowserActions.nap(3);
			Log.message("<b>Expected Result-1:</b> Search with 2 characters, should not show the auto search suggestion popup");

			Log.assertThat(
					homePage.elementLayer.verifyPageElementsDoNotExist(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result-1:</b> Auto search suggestion popup is not displayed for 2 character",
					"<b>Actual Result-1:</b> Auto search suggestion popup getting display for 2 character",
					driver);
			Log.message("<br>");
			homePage.enterSearchTextBox(searchKey[1]);
			Log.message((i++) + ". Entered 3 characters '" + searchKey[1]
					+ "' in the search box");
			Log.message("<br>");
			BrowserActions.nap(3);
			Log.message("<b>Expected Result-2:</b> Search with 3 characters, should show the auto search suggestion popup");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result-2:</b> Auto search suggestion popup is displayed for 3 characters",
					"<b>Actual Result-2:</b> Auto search suggestion popup is not getting display for 3 characters",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the search result page and breadcrumb when search a product", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_002(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String breadcrumb = null;
		if (Utils.getRunPlatForm() == "desktop") {
			breadcrumb = "Your search results for \"" + searchKey + "\"";
		} else if (Utils.getRunPlatForm() == "mobile") {
			breadcrumb = "Back to Home ";
		}
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result'  Page!");

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Search Result page when search a product");
			String searchWord = driver.getCurrentUrl().split("q=")[1];
			Log.assertThat(
					(searchresultPage != null)
							&& (searchWord.split("\\&")[0].equals(searchKey)),
					"<b>Actual Result:</b> Navigated to Search Result Page",
					"<b>Actual Result:</b> Not navigated to Search result Page",
					driver);

			List<String> ActualBreadcrumb = searchresultPage
					.getTextFromEntrieBreadCrumb();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The breadcrumb should displayed like, 'Your search results for 'Keyword''");
			int arraySize = ActualBreadcrumb.size();
			Log.assertThat(
					breadcrumb.contains(ActualBreadcrumb.get(arraySize - 1)),
					"<b>Actual Result:</b> The breadcrumb is displayed as expected: "
							+ breadcrumb,
					"<b>Actual Result:</b> The breadcrumb is not displayed as expected. Actual: "
							+ ActualBreadcrumb, driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Product count per page, Pagination and Viewing count text", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_003(String browser) throws Exception {

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		int currentPageNo;
		int nextPageNo;
		int PreviousPageNo;
		int lastPageNo;
		List<String> lastPageElement = Arrays.asList("iconLastPage");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.message("<br>");
			Log.message("<b><u>Product Count per Page Verification</u></b>");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The product count shoud between 1 to 30 per page");

			Log.assertThat(
					searchResultPage.getProductCount() <= 30,
					"<b>Actual Result:</b> The product count is '"
							+ searchResultPage.getProductCount() + "'.",
					"<b>Actual Result:</b> The product count is not between 1 to 30 per page i.e.,(Product count="
							+ searchResultPage.getProductCount() + ").", driver);
			Log.message("<br>");
			Log.message("<b><u>Viewing count Verification</u></b>");
			String itemCountText = searchResultPage.getResultHitsText();
			int count;
			count = searchResultPage.getProductCount();
			String[] splitTotalItemCountText = itemCountText.split("of ");
			String totalItemCount = splitTotalItemCountText[1];
			searchResultPage.navigateToPagination("2");
			count += searchResultPage.getProductCount();
			if (count < 30)
				throw new Exception(
						"Product count is not greater than 30, Hence unable to test this case");
			else {
				searchResultPage.navigateToPagination("1");
				Log.message("<br>");
				Log.message(totalItemCount
						+ "' products are displayed in the Search result page!");
				Log.message("<br>");
				Log.message("<b>Expected Result:</b> System should display 'Viewing 1 - 30 of "
						+ totalItemCount + " results' for more than 30 items.");
				Log.assertThat(
						itemCountText.equals("Viewing 1-30 of "
								+ totalItemCount),
						"<b>Actual Result:</b> 4. '" + itemCountText
								+ "' is displayed! in the search result page",
						"<b>Actual Result:</b> 4. '"
								+ itemCountText
								+ "' is not displayed! in the search result page",
						driver);
			}
			searchResultPage = homePage.searchProductWithKeyBoard(searchKey);
			currentPageNo = searchResultPage.getCurrentPageNo();
			Log.message("<br>");
			Log.message("3. Current page no before cliking is :"
					+ currentPageNo);
			Log.message("<br>");
			Log.message("<b><u>Pagination Verification</u></b>");
			Log.message("<br>");
			Log.assertThat(currentPageNo == 1,
					"Curent page no is 1 before cliking on next button !",
					"Curent page no is not 1 before cliking on next button !",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b><br>   i. Previous number or the back arrow icon System should navigates the customer to the previous page.<br>   ii. Next number or the forward, System should navigates the customer to next page.<br>   iii. System should navigates the customer to first page of the results.<br> iv. System should returns customer to the last page of results.<br>");
			Log.message("<b>Actual Result:</b>");
			searchResultPage.navigateToPagination("Next");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			nextPageNo = searchResultPage.getCurrentPageNo();
			Log.assertThat(
					nextPageNo == currentPageNo + 1,
					" After cliking on next button it navigated to next page  !",
					" After cliking on next button it's not navigated to next page  !",
					driver);
			searchResultPage.navigateToPagination("Previous");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			PreviousPageNo = searchResultPage.getCurrentPageNo();
			Log.assertThat(
					PreviousPageNo == currentPageNo,
					" After cliking on previous button it navigated to previous page !",
					" After cliking on previous button it's not navigated to previous page !",
					driver);
			searchResultPage.navigateToPagination("Last");
			searchResultPage.waitUntilSearchSpinnerDisappear();
			lastPageNo = searchResultPage.getCurrentPageNo();
			Log.softAssertThat(
					searchResultPage.elementLayer.verifyPageElementsDoNotExist(
							lastPageElement, searchResultPage),
					" After cliking on last button it navigated to last page !",
					" After cliking on last button it's not navigated to last page !",
					driver);
			searchResultPage.navigateToPagination("First");
			Log.assertThat(
					currentPageNo == lastPageNo - (lastPageNo - 1),
					" After cliking on first button it navigated to first page !",
					" After cliking on first button it's not navigated to first page !",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the 'Products Found' count and 'Viewing Count'", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_004(String browser) throws Exception {

		// Loading the test data from excel using the test case id
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductWithKeyBoard(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search result page");
			Log.message("<br>");

			String productFoundText = searchResultPage.getProductsFound()
					.replace("Products Found (", "");
			productFoundText = productFoundText.replace(")", "");
			productFoundText = productFoundText.replace(",", "").trim();
			Log.message("The 'Product Found' count is: " + productFoundText);

			String ViewingCount = searchResultPage.getResultHitsText();
			ViewingCount = ViewingCount.split("of")[1].trim();
			ViewingCount = ViewingCount.replace(",", "");
			Log.message("The 'Viewing Count' is: " + ViewingCount);

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The 'Product Found' count shoud equals to 'Viewing Product Count'");

			Log.assertThat(productFoundText.equals(ViewingCount),
					"<b>Actual Result:</b> The viewing count is '"
							+ ViewingCount
							+ "' is equal to the product found count '"
							+ productFoundText + "'.",
					"<b>Actual Result:</b> The viewing count is '"
							+ ViewingCount
							+ "' is not equal to the product found count '"
							+ productFoundText + "'.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_004

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Page should redirected to PDP when using UPC for search", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.searchProduct(searchKey);

			Log.message("2. Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Product details' Page!");
			String upcCode = pdpPage.getUPCValue();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to Product Details Page with the searched product");
			Log.assertThat(
					(pdpPage != null) && (searchKey.contains(upcCode)),
					"<b>Actual Result:</b> The page is redirected to PDP with the searched product",
					"<b>Actual Result:</b> The page is not redirected to PDP or not searched the exact product",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the page should redirected to Search Result page when search a UPC, which is associated with the Collection products", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);

			Log.message("2. Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result' Page!");
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to search result page when search UPC, which is associated with Collection product");
			Log.assertThat(
					(searchresultpage.getTextFromProductBadge().trim()
							.equals("Collection"))
							&& (searchresultpage.getProductCount() > 1),
					"<b>Actual Result:</b> The page is redirected to Search result page and the product count is: "
							+ searchresultpage.getProductCount(),
					"<b>Actual Result:</b> The page is not redirected to Search Result page or the product count is eqaul to 1 or less than that.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_006

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the SORT BY and ITEM PER PAGE values and functionalities", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_007(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		ArrayList<String> expected_ItemsPerPageoptions = new ArrayList<String>();
		int productCount;

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SearchResultPage searchresultpage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result' Page!");
			Log.message("<br>");
			Log.message("<b><u>'SORT BY' dropdown Verification</u></b>");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The top and bottom 'SORT BY' dropdown should displayed");
			Log.assertThat(
					searchresultpage.elementLayer.verifyPageElements(
							Arrays.asList("drpSortBy", "drpSortbyBottom"),
							searchresultpage),
					"<b>Actual Result: </b>The top and bottom 'SORT BY' dropdown is displayed",
					"<b>Actual Result: </b>The top and bottom 'SORT BY' dropdown is not displayed",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The selected 'SORT BY' dropdown value should displayed with updated page");

			String sortValue = searchresultpage
					.selectSortByDropDownByText("Best Sellers");
			String currentUrl = driver.getCurrentUrl().split("srule=")[1];
			String sortedValueFromUrl = currentUrl.split("&pmin")[0].replace(
					"-", " ");
			Log.softAssertThat(sortedValueFromUrl.equals(sortValue),
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: '"
							+ sortValue + "' and page getting updated",
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: "
							+ sortValue + " and page is not getting updated",
					driver);

			sortValue = searchresultpage.selectSortByDropDownByText("Featured");
			currentUrl = driver.getCurrentUrl().split("srule=")[1];
			sortedValueFromUrl = currentUrl.split("&pmin")[0].replace("-", " ");
			Log.softAssertThat(sortedValueFromUrl.equals(sortValue),
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: '"
							+ sortValue + "' and page getting updated",
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: "
							+ sortValue + " and page is not getting updated",
					driver);

			sortValue = searchresultpage
					.selectSortByDropDownByText("Most Recent");
			currentUrl = driver.getCurrentUrl().split("srule=")[1];
			sortedValueFromUrl = currentUrl.split("&pmin")[0].replace("-", " ");
			Log.softAssertThat(sortedValueFromUrl.equals(sortValue),
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: '"
							+ sortValue + "' and page getting updated",
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: "
							+ sortValue + " and page is not getting updated",
					driver);

			sortValue = searchresultpage
					.selectSortByDropDownByText("Top Rated");
			currentUrl = driver.getCurrentUrl().split("srule=")[1];
			sortedValueFromUrl = currentUrl.split("&pmin")[0].replace("-", " ");
			Log.softAssertThat(sortedValueFromUrl.equals(sortValue),
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: '"
							+ sortValue + "' and page getting updated",
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: "
							+ sortValue + " and page is not getting updated",
					driver);

			sortValue = searchresultpage
					.selectSortByDropDownByText("Best Matches");
			currentUrl = driver.getCurrentUrl().split("srule=")[1];
			sortedValueFromUrl = currentUrl.split("&pmin")[0].replace("-", " ");
			Log.softAssertThat(sortedValueFromUrl.equals(sortValue),
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: '"
							+ sortValue + "' and page getting updated",
					"<b>Actual Result: </b>The selected value in SORT BY dropdown is: "
							+ sortValue + " and page is not getting updated",
					driver);

			Log.message("<br>");
			Log.message("<b><u>'ITEM PER PAGE' dropdown Verification</u></b>");
			Log.message("<br>");
			Log.message("<b>Expected Result: </b>The top and bottom 'ITEM PER PAGE' dropdown should displayed");
			Log.assertThat(
					searchresultpage.elementLayer.verifyPageElements(
							Arrays.asList("drpItemsPerPage",
									"drpItemsPerPageBottom"), searchresultpage),
					"<b>Actual Result: </b>The top and bottom 'ITEM PER PAGE' dropdown is displayed",
					"<b>Actual Result: </b>The top and bottom 'ITEM PER PAGE' dropdown is not displayed",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> View 30 should be the default value for Items Per Page Dropdown.");
			String selectedoption = searchresultpage
					.getSelectedOptionFromItemsPerPageDropDown();
			ArrayList<String> mylist = searchresultpage
					.getItemsPerPageOptionList();
			Log.assertThat(
					selectedoption.equals("View 30"),
					"<b>Actual Result:</b> View 30 is default value for Items Per Page Dorpdown.",
					"<b>Actual Result:</b> View 30 is not the default value for Items Per Page Dropdown.",
					driver);
			expected_ItemsPerPageoptions.add("View 30");
			expected_ItemsPerPageoptions.add("View 60");
			expected_ItemsPerPageoptions.add("View 90");
			for (String itemsperpagevalue : mylist) {

				Log.softAssertThat(expected_ItemsPerPageoptions
						.contains(itemsperpagevalue),

				"<b>Actual Result:</b> ItemsPerPage drop down have '"
						+ itemsperpagevalue + "'", "<b>Actual Result:</b>"
						+ itemsperpagevalue
						+ " is not displayed in the 'ItemsPerPage' drop down");
			}
			searchresultpage.clickOnItemsPerPageDropDown();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> By Selecting 'View' drop down value, the product count should be updated");
			searchresultpage.selectItemsPerPageDropDownByText("60");
			productCount = searchresultpage.getProductCount();
			Log.assertThat(
					productCount == 60,
					"<b>Actual Result:</b> 'View 60' is clicked and 60 products are displayed.",
					"'View 60' is clicked but 60 products are not displayed.",
					driver);
			searchresultpage.selectItemsPerPageDropDownByText("30");
			productCount = searchresultpage.getProductCount();
			Log.assertThat(
					productCount == 30,
					"<b>Actual Result:</b> 'View 30' is clicked and 30 products are displayed.",
					"'View 30' is clicked but 30 products are not displayed.",
					driver);
			searchresultpage.selectItemsPerPageDropDownByText("90");
			productCount = searchresultpage.getProductCount();
			Log.assertThat(
					productCount == 90,
					"<b>Actual Result:</b> 'View 90' is clicked and 90 products are displayed.",
					"'View 90' is clicked but 90 products are not displayed.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_007

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Selected prices in Price refinement under product refinements panel", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_008(String browser) throws Exception {

		String runPltfrm = Utils.getRunPlatForm();
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String[] category = testData.get("CategoryLevel").split("\\|");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			homePage.headers.navigateTo(category[0], category[1], category[2]);
			Log.message("2. Navigated from Home Page to PLP Page with L1-Category("
					+ category[0]
					+ ") -> L2-Category("
					+ category[1]
					+ ") -> L3-Category(" + category[2] + ")");

			PlpPage plpPage = new PlpPage(driver).get();

			if (runPltfrm == "mobile") {
				plpPage.clickFilterByInMobile();
				plpPage.clickCollapseExpandToggle(" ");

			}

			String priceinPlp = plpPage.selectUnselectFilterOptions(" ");
			priceinPlp = priceinPlp.split("\\(")[0].trim();
			Log.message("3. Selected price: '" + priceinPlp
					+ "' in the refinement under price section in plp page");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The price refinement option should displays an orange checkbox next to the item indicating that the price range has been selected in plp page.");
			if (runPltfrm == "mobile") {
				plpPage.clickFilterByInMobile();
				plpPage.clickCollapseExpandToggle(" ");

			}
			Log.assertThat(
					plpPage.elementLayer.verifyPageListElements(
							Arrays.asList("chkRefinMentActive"), plpPage),
					"<b> Actual Result : </b> price refinement option displays an orange checkbox next to the item indicating that the price range has been selected in plp page.",
					"<b> Actual Result : </b>price refinement option not displays an orange checkbox next to the item indicating that the price range has been selected in plp page.",
					driver);
			Log.message("<br>");
			ArrayList<String> textInBreadCrumbValue = plpPage
					.getBreadCrumbLastValues();
			Log.message("4. Got the text from the refinement breadcrumb and the text is"
					+ textInBreadCrumbValue);
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> After selecting price only those products having one of those price range should displays in the product grid");
			Log.assertThat(
					textInBreadCrumbValue.get(0).equals(priceinPlp),
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in plp page",
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in plp page",
					driver);
			Log.message("<br>");
			if (runPltfrm == "mobile") {
				plpPage.clickOnCloseIcon();
			}

			// Navigate to the SearchResult Page of the Search Key.
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("5. Navigated to " + searchKey + " search result Page!");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(" ");

			}

			String price = searchresultPage.selectUnselectFilterOptions(" ");
			price = price.split("\\(")[0].trim();
			Log.message("6. Selected price: '" + price
					+ "' in the refinement under price section in clp page");
			Log.message("<br>");
			Log.message("<b> Expected Result : </b> The price refinement option should displays an orange checkbox next to the item indicating that the price range has been selected in slp page.");
			if (runPltfrm == "mobile") {
				searchresultPage.clickFilterByInMobile();
				searchresultPage.clickCollapseExpandToggle(" ");

			}
			Log.assertThat(
					searchresultPage.elementLayer.verifyPageListElements(
							Arrays.asList("chkRefinMentActive"),
							searchresultPage),
					"<b> Actual Result : </b> price refinement option displays an orange checkbox next to the item indicating that the price range has been selected in slp page.",
					"<b> Actual Result : </b>price refinement option not displays an orange checkbox next to the item indicating that the price range has been selected in slp page.",
					driver);
			Log.message("<br>");
			ArrayList<String> textInBreadCrumbValueinslp = searchresultPage
					.getBreadCrumbLastValue();
			Log.message("7. Got the text from the refinement breadcrumb and the text is"
					+ textInBreadCrumbValueinslp);

			Log.message("<br>");
			Log.message("<b> Expected Result : </b> After selecting price only those products having one of those price range should displays in the product grid in slp page");
			Log.assertThat(
					textInBreadCrumbValueinslp.get(0).equals(price),
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in slp page",
					"<b> Actual Result : </b> After selecting prices only those products having one of those price range should displays in the product grid in slp page",
					driver);
			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		}// catch
		finally {
			Log.endTestCase();
			driver.quit();
		}// finally

	}// TC_BELK_BAT_SPRINT2_008

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the LIST & GRID icon functionality", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_009(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String searchKey = testData.get("SearchKey");
		List<String> listView = Arrays.asList("listSearchResult");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("2. Search with keyword '" + searchKey
					+ "' and navigated to search result page");

			int productCount = searchResultPage.getProductCount();
			if (productCount == 0)
				throw new Exception(
						"The product count is 0. so unable to execute this testcase.");
			else {
				String currentViewType = searchResultPage.getCurrentViewType();
				if (currentViewType != "wide") {
					searchResultPage.clickGridListToggle("List");
				}
				Log.message("3. The 'List' view is clicked");
				Log.message("<br>");
				Log.message("<b>Expected Result-1:</b> The products should displayed in List view.");

				int xLocationOfFirstProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(1);
				int xLocationOfSecondProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(2);
				Log.softAssertThat(
						xLocationOfFirstProduct == xLocationOfSecondProduct,
						"<b>Actual Result-1:</b> The products are displayed in List view.",
						"<b>Actual Result-1:</b> The products are not displayed in List view.",
						driver);
				searchResultPage.clickGridListToggle("Grid");
				Log.message("<br>");
				Log.message("4. The 'Grid' view is clicked");
				xLocationOfFirstProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(1);
				xLocationOfSecondProduct = searchResultPage
						.getCurrentLocationOfPdtByIndex(2);
				Log.message("<br>");
				Log.message("<b>Expected Result-2:</b> The products should displayed in Grid view.");
				Log.softAssertThat(
						xLocationOfFirstProduct != xLocationOfSecondProduct,
						"<b>Actual Result-2:</b> The products are displayed in Grid view.",
						"<b>Actual Result-2:</b> The products are not displayed in Grid view.",
						driver);
				Log.message("<br>");
				searchResultPage.clickGridListToggle("List");
				Log.message("5. Clicked 'List' view toggle in the search result page");

				Log.message("<br>");
				Log.message("<b>Expected Result:</b> By clicking 'List' toggle, products are should be display in 'List' view");

				Log.assertThat(
						searchResultPage.elementLayer.verifyPageElements(
								listView, searchResultPage),
						"<b>Actual Result:</b> By clicking 'List' toggle, products are displayed in 'List' view",
						"By clicking 'List' toggle, products are not displayed in 'List' view",
						driver);
			}

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT2_009

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the brand name and product name links from the Search suggestion", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT2_010(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			homePage.enterSearchTextBox(searchKey);
			Log.message((i++) + ". Entered the keyword '" + searchKey
					+ "' in the search box");
			Log.message("<br>");
			BrowserActions.nap(3);
			Log.message("<b>Expected Result:</b> Search with the keyword, should show the auto search suggestion popup");

			Log.assertThat(
					homePage.elementLayer.verifyPageElements(
							Arrays.asList("autoSuggestPopup"), homePage),
					"<b>Actual Result:</b> Auto search suggestion popup is displayed for 3 characters",
					"<b>Actual Result:</b> Auto search suggestion popup is not getting display for 3 characters",
					driver);

			String productName = homePage
					.getProductNameFromProductSuggestion(1);
			PdpPage pdpPage = homePage.clickOnProductFromSearchSuggestion(1);
			String productNameInPDP = pdpPage.getProductNameFromBreadCrumb();

			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to the PDP with the selected product");
			Log.assertThat(
					productName.equals(productNameInPDP),
					"<b>Actual Result:</b> The page is navigated to PDP with the selected product: "
							+ productNameInPDP,
					"<b>Actual Result:</b> The page is not navigated to PDP page or the product is not same as selected one",
					driver);
			homePage.enterSearchTextBox(searchKey);
			Log.message((i++) + ". Again entered the keyword '" + searchKey
					+ "' in the search box");
			SearchResultPage searchResultPage = homePage
					.clickOnBrandNameInSearchSuggestionByIndex(1);
			List<String> breadcrumb = searchResultPage.getTextInBreadcrumb();
			Log.message("<br>");
			Log.message("<b>Expected Result:</b> The page should redirected to the Search result page with the selected product");
			Log.assertThat(
					breadcrumb.get(1).contains("Your search results for"),
					"<b>Actual Result:</b> The page is navigated to Search result page with the selected product: "
							+ productNameInPDP,
					"<b>Actual Result:</b> The page is not navigated to Search Result page or the product is not same as selected one",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT2_010

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the product details .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_001(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");
			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);
			// select the quantity
			String pdtDetails = pdpPage.addProductToBag();
			String Size = pdtDetails.split("\\|")[1].trim();
			Log.message((i++) + ". selected size: " + Size);
			// Select the color
			String Color = pdtDetails.split("\\|")[0].trim().trim();
			Log.message((i++) + ". selected color: " + Color);
			String Qty = pdtDetails.split("\\|")[2].trim();
			Log.message((i++) + ". selected Qty: " + Qty);

			String ProductPrice = pdpPage.getProductPrice();
			Log.message((i++) + ". Product Price is :" + ProductPrice);
			String UPC = pdpPage.getUPCValue().trim();
			Log.message((i++) + ". UPC displayed in PDP after variation : "
					+ UPC);

			Log.message((i++) + ". Product added to ShoppingBag !");
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++)
					+ ". clicked on Minicart and Navigated to shopping bag page !");

			LinkedList<LinkedHashMap<String, String>> ProductDetails = shoppingBagPage
					.getProductDetails();
			String ProductNameInBag = ProductDetails.get(0).get("ProductName")
					.trim();
			String UPCvalue = ProductDetails.get(0).get("Upc").trim();
			String ColorInBag = ProductDetails.get(0).get("Color").trim();
			String SizeInBag = ProductDetails.get(0).get("Size").trim();
			String ProductPriceInBag = ProductDetails.get(0).get("Price")
					.trim();
			String QtyInBag = ProductDetails.get(0).get("Quantity").trim();

			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The 'product Name','UPC value','Color','Size','QTY','Price' should display same in PDP and Shopping Bag Page .");

			Log.assertThat(
					ProductName.equals(ProductNameInBag),
					"<b>Actual Result 1: </b> Product Name displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 1: </b> Product Name not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);

			Log.assertThat(
					UPC.equals(UPCvalue),
					"<b>Actual Result 2: </b> UPC value displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 2: </b> UPC value not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);

			Log.assertThat(
					ProductPrice.equals(ProductPriceInBag),
					"<b>Actual Result 3: </b> Product price displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 3: </b> Product price not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);

			Log.assertThat(
					Color.equals(ColorInBag),
					"<b>Actual Result 4: </b> Color Name displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 4: </b> Color Name not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);

			Log.assertThat(
					Size.equals(SizeInBag),
					"<b>Actual Result 5: </b> Size value displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 5: </b> Size value not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);

			Log.assertThat(
					Qty.equals(QtyInBag),
					"<b>Actual Result 6: </b> Quantity value displayed same in ShoppingBag as displayed in PDP Page.",
					"<b>Actual Result 6: </b> Quantity value not displayed same in ShoppingBag as displayed in PDP Page.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_001

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Success Message and 'Remove','continueToShopping' link functionality in ShoppingBagPage .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_002(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			String SUCCESS_MSG = pdpPage.getAddToBagMessage();
			String Expected_MSG = "Added to Shopping Bag";
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> After clicking AddToBag button system should display the success message in minicart popup.");
			Log.assertThat(
					SUCCESS_MSG.equals(Expected_MSG),
					"<b>Actual Result 1: After clicking AddToBag Button Success message 'Added to Shopping Bag' dispalyed in minicart PopUp.",
					"<b>Actual Result 1: After clicking AddToBag Button Success message 'Added to Shopping Bag' not dispalyed in minicart PopUp.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 2.a: </b> In shoppingBagPage 'Remove' link should display and after clicking 'Remove' link empty bag with 'ContinueToShopping' link should display.");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++)
					+ ". clicked on Minicart icon and Navigated to ShoppingBagPage !");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageListElements(
							Arrays.asList("removeProductByIndex"),
							shoppingBagPage),
					"<b>Actual Result 2.a: </b> After adding product from PDP , 'Remove' link displayed in ShoppingBagPage.",
					"<b>Actual Result 2.a: </b> After adding product from PDP , 'Remove' link not displayed in ShoppingBagPage.",
					driver);
			Log.message("<br>");
			shoppingBagPage.removeItemsFromBag();
			Log.message((i++)
					+ ". Added product successfully removed from ShoppingBag !");

			String EMPTY_BAG_MSG = shoppingBagPage.getTextFromEmptyBag();
			String Expected_EMPTY_MSG = "Your shopping bag is empty.";

			Log.assertThat(
					EMPTY_BAG_MSG.equals(Expected_EMPTY_MSG),
					"<b>Actual Result 2.b: </b>  Empty bag message 'Your shopping bag is empty.' displayed in ShoppingBagPage.",
					"<b>Actual Result 2.b: </b>  Empty bag message 'Your shopping bag is empty.' not displayed in ShoppingBagPage.",
					driver);
			Log.assertThat(
					shoppingBagPage.elementLayer
							.verifyPageElements(
									Arrays.asList("btnContinueShoppingWithoutProducts"),
									shoppingBagPage),
					"<b>Actual Result 2.c: </b>  'ContinueToShopping' button displayed in ShoppingBagPage.",
					"<b>Actual Result 2.c: </b>  'ContinueToShopping' button not displayed in ShoppingBagPage.",
					driver);

			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b> After clicking on 'ContinueToShopping' button system should naviagte to HomePage.");
			shoppingBagPage.clickContinueShoppingWithoutProducts();
			Log.message((i++)
					+ ". clicked on 'ContinueToShopping' button and Navigated to HomePage !");

			Log.assertThat(
					driver.getCurrentUrl().contains("home"),
					"<b>Actual Result 3: </b>  After clicking on 'ContinueToShopping' button system naviagted to HomePage.",
					"<b>Actual Result 3: </b>  After clicking on 'ContinueToShopping' button system not naviagted to HomePage.",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify 'Edit' link functionality in ShoppingBagPage .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_003(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			shoppingBagPage.clickEditDetails(ProductName);

			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> After clicking on 'EDIT' link QuickView PopUp should be display.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("QuickViewDialog"), shoppingBagPage),
					"<b>Actual Result 1: </b>  After clicking on 'EDIT' link QuickView PopUp displayed.",
					"<b>Actual Result 1: </b>  After clicking on 'EDIT' link QuickView PopUp not displayed.",
					driver);

			LinkedList<LinkedHashMap<String, String>> ProductDetails = shoppingBagPage
					.getProductDetails();
			String ProductNameInBag = ProductDetails.get(0).get("ProductName")
					.trim();
			String UPCvalue = ProductDetails.get(0).get("Upc").trim();
			String ColorInBag = ProductDetails.get(0).get("Color").trim();
			String SizeInBag = ProductDetails.get(0).get("Size").trim();
			String QtyInBag = ProductDetails.get(0).get("Quantity").trim();
			QuickViewPage QuickViewPage = new QuickViewPage(driver).get();
			LinkedHashMap<String, String> ProductDetailsInQuickView = QuickViewPage
					.getProductDetails();
			String ProductNameInQV = ProductDetailsInQuickView.get(
					"ProductName").trim();
			String UPCvalueInQV = ProductDetailsInQuickView.get("Upc").trim();
			String ColorInQV = ProductDetailsInQuickView.get("Color").trim();
			String SizeInQV = ProductDetailsInQuickView.get("Size").trim();
			String QtyInQV = ProductDetailsInQuickView.get("Quantity").trim();

			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b> The 'product Name','UPC value','Color','Size','QTY' should display same in Shopping Bag Page and after clicking EDIT link in QuickView Tab .");

			Log.assertThat(
					ProductNameInBag.equals(ProductNameInQV),
					"<b>Actual Result 2.a: </b> Product Name displayed same in QuickView tab as displayed in ShoppingBag Page.",
					"<b>Actual Result 2.a: </b> Product Name not displayed same in QuickView tab as displayed in ShoppingBag Page.",
					driver);

			Log.assertThat(
					UPCvalue.equals(UPCvalueInQV),
					"<b>Actual Result 2.b: </b> UPC value displayed same in QuickView tab as displayed in ShoppingBag Page.",
					"<b>Actual Result 2.b: </b> UPC value not displayed same in QuickView tab as displayed in ShoppingBag Page.",
					driver);

			Log.assertThat(
					ColorInBag.equals(ColorInQV),
					"<b>Actual Result 2.c: </b> Color Name displayed same in QuickView tab as displayed in ShoppingBag Page.",
					"<b>Actual Result 2.c: </b> Color Name not displayed same in QuickView tab as displayed in ShoppingBag Page.",
					driver);

			Log.assertThat(
					SizeInBag.equals(SizeInQV),
					"<b>Actual Result 2.d: </b> Size value displayed same in QuickView tab as displayed in ShoppingBag Page.",
					"<b>Actual Result 2.d: </b> Size value not displayed same in QuickView tab as displayed in ShoppingBag Page.",
					driver);

			Log.assertThat(
					QtyInBag.equals(QtyInQV),
					"<b>Actual Result 2.e: </b> Quantity value displayed same in QuickView tab as displayed in ShoppingBag Page.",
					"<b>Actual Result 2.e: </b> Quantity value not displayed same in QuickView tab as displayed in ShoppingBag Page.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3: </b> After clicking on 'EDIT' link QuickView PopUp should be display.");

			QuickViewPage.selectSizeByIndex(1);
			Log.message((i++) + ". selected other size from sizedropdown !");
			QuickViewPage.clickOnUpdateShoppingCartlnk();
			Log.message((i++) + ". clicked on UpdateShoppingCart link !");
			LinkedList<LinkedHashMap<String, String>> ProductDetailsAfterUpdate = shoppingBagPage
					.getProductDetails();
			Log.assertThat(
					!(ProductDetailsAfterUpdate.equals(ProductDetails)),
					"<b>Actual Result 3: </b> After saving the value from QuickView , updated value displayed in shoppingBag Page. ",
					"<b>Actual Result 3: </b> After saving the value from QuickView , updated value not displayed in shoppingBag Page. ",
					driver);

			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify 'coupon' link functionality in ShoppingBagPage .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String couponCode = "96325874";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			shoppingBagPage.applyCouponInShoppingBag(couponCode);
			Log.message((i++) + ". Entered the coupon code as :" + couponCode);
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> If user will enter invalid coupon code then system should display an Error message.");
			String ERROR_MSG = shoppingBagPage.getCouponErrMsg();
			String Expected_ERROR_MSG = "That coupon code is invalid.";
			Log.assertThat(
					ERROR_MSG.equals(Expected_ERROR_MSG),
					"<b>Actual Result 1: </b> After entering the invalid coupon code it displayed error message.",
					"<b>Actual Result 1: </b> After entering the invalid coupon code it has not displayed any error message.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b> If user will click viewAllCoupon system should navigate to Coupon page.");
			shoppingBagPage.clickViewAllcoupon();
			Log.message((i++)
					+ ". Clicked on ViewAllCoupon link and navigated to couponPage !");
			Log.assertThat(
					driver.getCurrentUrl().contains("coupons"),
					"<b>Actual Result 2: </b> After clicking vewAllCoupon link page navigated to CouponPage.",
					"<b>Actual Result 2: </b> After clicking vewAllCoupon link page not navigated to CouponPage.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_004

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify 'FindStore' link functionality in ShoppingBagPage .", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String ZipCode = "96325";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load the SearchResult Page with search keyword
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			shoppingBagPage.clickOnFindStorelnk();
			Log.message((i++) + ". clicked on FindStore link !");
			Log.message("<br>");
			Log.message("<b>Expected Result 1: </b> After clicking findstore link the Store popup should display.");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("findInStorePopUp"), shoppingBagPage),
					"<b>Actual Result 1: </b> After clicking findstore link the Store popup displayed.",
					"<b>Actual Result 1: </b> After clicking findstore link the Store popup not displayed.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2: </b> After Entering invalid ZipCode system should display an Error Message.");
			shoppingBagPage.applyZipCodeInShoppingBag(ZipCode);
			Log.message((i++)
					+ ". Entered ZipCode and clicked continue button !");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("errorMessage"), shoppingBagPage),
					"<b>Actual Result 2: </b> After entering invalid ZipCode it diplayed the Error message.",
					"<b>Actual Result 2: </b> After entering invalid ZipCode it's not diplayed any Error message.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the functionality of line message and Global message.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_006(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String color;
		int QtyInBagIn2ndTry;
		int QtyInBagIn1stTry;
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page");
			SignIn signIn = homePage.headers.navigateToSignIn();
			MyAccountPage myaccount = signIn.signInToMyAccount(emailid,
					password);
			ShoppingBagPage bagPage = myaccount.miniCartPage.navigateToBag();
			if (!bagPage.isShoppingBagEmpty()) {
				bagPage.removeItemsFromBag();
			}
			// Load the SearchResult Page with search keyword
			PdpPage pdpPage = myaccount.headers
					.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey + "'");
			Log.message((i++) + ". Navigated to PDP");
			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message((i++) + ". selected the '" + color + "' Color !");
			// Selecting the Size
			pdpPage.selectSizeByIndex(1);
			Log.message((i++) + ". selected the size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			QtyInBagIn1stTry = Integer.parseInt(shoppingBagPage
					.getNoOfQtyInShoppingBagPage());
			shoppingBagPage.headers.clickSignOut();
			pdpPage = shoppingBagPage.headers.searchAndNavigateToPDP(searchKey);
			Log.message((i++) + ". Searched with '" + searchKey + "'");

			Log.message((i++) + ". Navigated to PDP");

			// Selecting the Color
			color = pdpPage.selectColor();
			Log.message((i++) + ". selected the '" + color + "' Color !");
			// Selecting the Size
			pdpPage.selectSizeByIndex(1);
			Log.message((i++) + ". selected the size !");
			// Adding item to the shopping bag
			pdpPage.clickAddToBag();
			Log.message((i++) + ". Product added to Bag !");
			// Navigating to shopping bag
			shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to shopping bag page");
			// Navigating to signIn page
			signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			signIn.enterEmailID(emailid);
			signIn.enterPassword(password);
			signIn.clickBtnSignIn();
			BrowserActions.nap(5);
			QtyInBagIn2ndTry = Integer.parseInt(shoppingBagPage
					.getNoOfQtyInShoppingBagPage());
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> User should see that only the Quantity of product should be increased in the Cart.");
			Log.assertThat(
					(QtyInBagIn1stTry + 1) == QtyInBagIn2ndTry,
					"<b>Actual Result 1:</b> No of Quantity is increased by 1 in shoppingBagPage .",
					"<b>Actual Result 1:</b> No of Quantity is not increased in shoppingBagPage .",
					driver);
			String GlobalMessage = shoppingBagPage.getGlobalMessage();
			String LineMessage = shoppingBagPage.getLineMessage();
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> Global Message should be: Items from your other shopping bag have been added to your current shopping bag.");
			Log.assertThat(
					GlobalMessage.equals(shoppingBagPage.GLOBAL_MESSAGE),
					"<b>Actual Result 2:</b> Global Message :'Items from your other shopping bag have been added to your current shopping bag' displayed.",
					"<b>Actual Result 2:</b> Global Message :'Items from your other shopping bag have been added to your current shopping bag' not displayed.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> Line Item message should be:This item in your bag has been changed.");
			Log.assertThat(
					LineMessage.equals(shoppingBagPage.LINE_MESSAGE),
					"<b>Actual Result 3:</b> Line Message :'This item in your bag has been changed.' displayed.",
					"<b>Actual Result 3:</b> Line Message :'This item in your bag has been changed.' not displayed.",
					driver);
			Log.testCaseResult();

		} // try

		catch (Exception e) {
			Log.exception(e, driver);

		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT4_006

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the billing and shipping address should same when checked 'Use this Address for Billing' in Checkout page. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_007(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			// Search for a product
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to ShoppingBag page !");
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message((i++) + ". clicked on checkoutAsGuest Button !");
			checkoutPage.fillingShippingDetailsAsGuest("valid_address3",
					"Standard");
			LinkedHashMap<String, String> ShippingAddressInShippingPage = checkoutPage
					.getShippingAddressInShippingPage();
			checkoutPage.clickCheckBoxInShipping();
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithOriginalAddress();
			Log.message((i++)
					+ ". Clicked on continue Button in shipping page !");
			LinkedHashMap<String, String> BillingAddressInBilligPage = checkoutPage
					.getBillingAddressInBillingPage();
			Log.message("<br>");
			Log.message("<b>Expected Result :</b> When user will click checkbox 'Use this Address for Billing' the shipping address should update in Billing address field. ex:'FirstName','LastName','zipcode'.");
			Log.assertThat(
					ShippingAddressInShippingPage.get("FirstName").equals(
							BillingAddressInBilligPage.get("FirstName")),
					"<b>Actual Result 1:</b> After clicking on checkbox 'Use this Address for Billing' the same FirstName updated in Billing Page.",
					"<b>Actual Result 1:</b> After clicking on checkbox 'Use this Address for Billing' the same FirstName not updated in Billing Page.",
					driver);
			Log.assertThat(
					ShippingAddressInShippingPage.get("LastName").equals(
							BillingAddressInBilligPage.get("LastName")),
					"<b>Actual Result 2:</b> After clicking on checkbox 'Use this Address for Billing' the same LastName updated in Billing Page.",
					"<b>Actual Result 2:</b> After clicking on checkbox 'Use this Address for Billing' the same LastName not updated in Billing Page.",
					driver);
			Log.assertThat(
					ShippingAddressInShippingPage.get("Address").equals(
							BillingAddressInBilligPage.get("Address")),
					"<b>Actual Result 3:</b> After clicking on checkbox 'Use this Address for Billing' the same Address updated in Billing Page.",
					"<b>Actual Result 3:</b> After clicking on checkbox 'Use this Address for Billing' the same Address not updated in Billing Page.",
					driver);
			Log.assertThat(
					ShippingAddressInShippingPage.get("City").equals(
							BillingAddressInBilligPage.get("City")),
					"<b>Actual Result 4:</b> After clicking on checkbox 'Use this Address for Billing' the same City updated in Billing Page.",
					"<b>Actual Result 4:</b> After clicking on checkbox 'Use this Address for Billing' the same City not updated in Billing Page.",
					driver);
			Log.assertThat(
					ShippingAddressInShippingPage.get("Zipcode").equals(
							BillingAddressInBilligPage.get("Zipcode")),
					"<b>Actual Result 5:</b> After clicking on checkbox 'Use this Address for Billing' the same Zipcode updated in Billing Page.",
					"<b>Actual Result 5:</b> After clicking on checkbox 'Use this Address for Billing' the same Zipcode not updated in Billing Page.",
					driver);
			Log.assertThat(
					ShippingAddressInShippingPage.get("Phone").equals(
							BillingAddressInBilligPage.get("Phone")),
					"<b>Actual Result 6:</b> After clicking on checkbox 'Use this Address for Billing' the same PhoneNo updated in Billing Page.",
					"<b>Actual Result 6:</b> After clicking on checkbox 'Use this Address for Billing' the same PhoneNo not updated in Billing Page.",
					driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT4_007

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Add Gift box checkbox functionality and After Click on Brand name System Should redirected to PLP and clicking on product name it Should displayed Update QV popup. ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT4_008(String browser) throws Exception {
		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");
			// Search for a product
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++) + ". Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = searchResultPage.selectProduct();
			Log.message((i++) + ". Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message((i++) + ". Product Name is :" + ProductName);

			String Color = pdpPage.selectColor();
			Log.message((i++) + ". selected color: " + Color);

			String Size = pdpPage.selectSize();
			Log.message((i++) + ". selected size: " + Size);
			String Qty = pdpPage.selectQuantity();
			Log.message((i++) + ". selected Qty: " + Qty);
			pdpPage.clickAddToBagButton();
			Log.message((i++)
					+ ". Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message((i++) + ". Navigated to ShoppingBag page !");
			shoppingBagPage.checkAddGiftBox();
			Log.message((i++) + ". Clicked Add gift box link !");
			LinkedHashMap<String, String> costDetailsInCart = shoppingBagPage
					.getOrderSummaryDetails();
			Log.message("<br>");
			Log.message("<b>Expected Result 1:</b> When user will click 'AddGiftBox' link total order should calculate properly.");
			Log.softAssertThat(
					e2eUtils.verifyOrderSummaryTotal(costDetailsInCart),
					"<b>Actual Result 1:</b> After clicking on AddGiftBox link total order calculated properly.",
					"<b>Actual Result 1:</b> After clicking on AddGiftBox link total order not calculated properly.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 2:</b> When user will click 'AddFreeGiftMessage' link then system should display message text popup.");
			shoppingBagPage.checkAddFreeGiftMessage(0);
			Log.message((i++) + ". Clicked Add Free gift message link !");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("txtGiftMessageTextArea"),
							shoppingBagPage),
					"<b>Actual Result 2:</b> After clicking on AddFreeGiftMessage link message text popup displayed.",
					"<b>Actual Result 2:</b> After clicking on AddFreeGiftMessage link message text popup not displayed.",
					driver);
			Log.message("<br>");
			Log.message("<b>Expected Result 3:</b> When user will click on 'productName' link QuickView popup should displayed.");
			shoppingBagPage.clcikOnProductNameInShoppingBag();
			Log.message((i++)
					+ ". Clicked on product name link and Navigated to update page !");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("QuickViewDialog"), shoppingBagPage),
					"<b>Actual Result 3:</b> After clicking on productName link QuickView popup displayed.",
					"<b>Actual Result 3:</b> After clicking on productName link QuickView popup not displayed.",
					driver);
			shoppingBagPage.clcikOncancelLnkUpdatePopup();
			Log.message("<br>");
			Log.message("<b>Expected Result 4:</b> When user will click on 'BrandName' link System should naviagte to SearchResultPage.");
			shoppingBagPage.clcikOnBrandNameInShoppingBag();
			Log.message((i++)
					+ ". Clicked on Brand name link and Navigated to update page !");
			Log.assertThat(
					driver.getCurrentUrl().contains("search"),
					"<b>Actual Result 4:</b> After clicking on BrandName link it navigated to SearchResultPage.",
					"<b>Actual Result 4:</b> After clicking on BrandName link it's not navigated to SearchResultPage.",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT4_008

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Deal Based  Promotional Product is Displayed on the Order summary page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_002(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {

			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");
			// Search for a product
			SearchResultPage srchresultpage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with keyword '" + searchKey
					+ "' and navigated to search Result Page !");

			PdpPage pdpPage = srchresultpage.selectProductByIndex(1);
			Log.message("3. Navigated to PDP Page !");

			String ProductName = pdpPage.getProductName();
			Log.message("4. Product Name is :" + ProductName);

			String Size = pdpPage.selectSize();
			Log.message("5. selected size: " + Size);
			String Color = pdpPage.selectColor();
			Log.message("6. selected color: " + Color);
			pdpPage.selectQtyByIndex(2);
			Log.message("7. selected Qty ");
			String txtMessageOfBuyOneProduct = pdpPage.getBogoMessage();
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> Verify Deal Based call out messag is displayed on the PDP page");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtBogoMessage"), pdpPage),
					"<b> Actual Result 1: </b> Navigate to the PDP page and Deal based call out message is displayed :"
							+ txtMessageOfBuyOneProduct,
					"<b> Actual Result 1: </b> Navigate to the PDPpage and Deal Based call out message is not displayed",
					driver);
			Log.message("<br>");
			pdpPage.clickAddToBagButton();
			Log.message("8. Clicked AddToBag Button and product Added to Bag !");
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("9. Navigated to ShoppingBag page !");
			LinkedList<LinkedHashMap<String, String>> detailsOfAddProduct = shoppingBagPage
					.getProductDetails();
			Log.message("<br>");
			Log.message("<b> Expected Result 2: </b> Verify the deal based product added to the cart");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("txtPromoMessage"), shoppingBagPage),
					"<b> Actual Result 2: </b> Navigate to the Shopping bag page and Deal based  Product is added to the bag and the details is: "
							+ detailsOfAddProduct,
					"<b> Actual Result 2: </b> Navigate to the Shopping bag page and Deal Based Product is not add to the bag",
					driver);
			Log.message("<br>");
			SignIn signIn = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("10 Navigated to SignIn page !");
			CheckoutPage checkoutPage = signIn.clickOnCheckoutAsGuest();
			Log.message("11 clicked on checkoutAsGuest Button !");

			// Filling the shipping details as a GuestUser
			checkoutPage.fillingShippingDetailsAsGuest("valid_address1",
					"Standard");
			Log.message("12. Entered Shipping Address Details as Guest");

			// clicking on continue in shipping page
			checkoutPage.clickOnContinueInShipping();
			checkoutPage.clickContinueInAddressValidationModal();
			checkoutPage.clickOnContinueInShipping();
			Log.message("13. Clicked on Continue in Shipping Address Details as Guest");

			// Filling billing address details as a guest user
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("14. Billing address details are filled in billing page!");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message("15. filled card Details");
			checkoutPage.clickOnContinueInBilling();
			Log.message("16. Clicked continue button in billing page");

			OrderConfirmationPage oder = checkoutPage.ClickOnPlaceOrderButton();
			Log.message("17. Clicked Place Order button");
			Log.message("<br>");
			Log.message("<b> Expected Result 3: </b> Verify the  Product Promotion  is displayed Order summary page");
			Log.assertThat(
					oder.elementLayer.verifyPageElements(
							Arrays.asList("txtPromoMessageOnOrderSummary"),
							oder),
					"<b> Actual Result 3: </b> Navigate to the Order Summary Page and Deal based Promotional Product  is displayed ",
					"<b> Actual Result 3: </b> Navigate to the Order summary page page and Deal Based Promotional Product message is not dispalyed",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT5_002

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Add a GWP product ,verify free gift added to that product with master & gift cardand Verify shopping bag link is available on the order reciept page ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_006(String browser) throws Exception {
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.headers
					.searchProductKeyword(searchKey);
			Log.message("2. Searched with '" + searchKey
					+ "' Navigate to PDP Page");

			PdpPage pdpPage = searchResultPage.navigateToPDPByRow(4);

			Log.message("3. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(3);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Select Quantity");
			Log.message("<b>Expected Result-1: </b> Verify Deal Based call out message is displayed on the PDP page");

			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("txtstandardPrice"), pdpPage),
					"<b>Actual Result-1: </b>  Deal Based call out message is  displayed on the PDP page",
					"<b>Actual Result-1: </b>  Deal Based call out message is not displayed on the PDP page",
					driver);
			pdpPage.clickOnViewDetails();
			Log.message("4.Clicked on View Details Link !");
			pdpPage.clickOnOfferDetails();
			Log.message("5. Clicked on promotional message Link !");
			Log.message("<b>Expected Result-2: </b>Verify Gwp selection modal is displayed and click on close[x] icon GWP modal get closed ");

			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("titleGElModel", "linkCloseIcon"),
							pdpPage),
					"<b>Actual Result-2: </b> Gwp selection modal is displayed and After click on close[x] icon GWP modal get closed !",
					"<b>Actual Result-2: </b> Gwp selection modal is not displayed and After click on close[x] icon GWP modal not get closed !",
					driver);
			pdpPage.clickOnCloseIcon();
			Log.message("5. Click on close icon of  'Gwp selection modal'  !");
			pdpPage.clickAddToBag();
			Log.message("6. Product added to Shopping Bag!");

			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("<b>Expected Result-3: </b>Verify the free gift added to the shopping bag page with promotional message");

			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("lblFreeGiftMessage"),
							shoppingBagPage),
					"<b>Actual Result-3: </b> The free gift added to the shopping bag page with promotional message !",
					"<b>Actual Result-3: </b> The free gift not added to the shopping bag page with promotional message !",
					driver);
			Log.message("7. Navigated to 'Mini Cart' !");

			SignIn signinpage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("8.Clicked on Checkout in Order summary Page");

			// Load checkout page to checkout the products
			CheckoutPage checkoutPage = signinpage.clickOnCheckoutAsGuest();
			Log.message("9. Navigated to Checkout Page as Guest User!");

			checkoutPage.fillingShippingDetailsAsGuest("valid_address2",
					"Standard");
			Log.message("10. Entered Shipping address as guest user");
			checkoutPage.clickOnContinueInShipping();
			Log.message("11. clicked on continue Button in Shipping address Page");
			checkoutPage.chooseOriginalAddressInModal();
			Log.message("12. Selected Original address in the modal");
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("13. Clicked on Continue Button in Address validation modal");

			checkoutPage.fillingBillingDetailsAsGuest("valid_address1");
			Log.message("14. Billing address details are entered!");

			checkoutPage.fillingCardDetails("NO", "card_Discover");

			Log.message("15. Entered card details");
			checkoutPage.clickOnContinueInBilling();

			Log.message("16. Clicked on continue button  in Billing address Page");

			Log.message("</br>");
			Log.message("<b>Expected Result-4: </b> Verify navigate to the place order page  it is having -Order summary ,Shipping address,Billing address,Payment method(headings are displayed)");
			Log.assertThat(
					checkoutPage.elementLayer.verifyPageElements(Arrays.asList(
							"lblMiniOrderSummaryInPlaceOrder",
							"lblMiniShippingAddressInPlaceOrder",
							"lblMiniBillingAddressInPlaceOrder"), checkoutPage),
					"<b>Actual Result-4: </b> Navigated to place order page and the mentioned headings are displayed on the place order page",
					"<b>Actual Result-4: </b> Navigated to place order page and the mentioned headings are not displayed on the place order page ",
					driver);

			OrderConfirmationPage orderConfirmationPage = checkoutPage
					.placeOrder();

			Log.message("17. Clicked on 'Place order' button in Place order page");

			Log.message("</br>");
			Log.message("<b>Expected Result-5: </b> Verify Retun To shopping bag link is available on the Order reciept page.");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(
							Arrays.asList("returnToShoppingBtn"),
							orderConfirmationPage),
					"<b>Actual Result 5: </b> Return to shopping bag link available on the order receipt page",
					"<b>Actual Result 5: </b> Return to shopping bag link not available on the order receipt page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT5_006

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the price details in PDP and compare the price with SRP", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_003(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++)
					+ ".Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result'  Page!");

			HashMap<String, String> productDetailsSRP = searchresultPage
					.getPriceDetailsByIndex(1);
			Log.message((i++) + ". Get the price details of a product!", driver);
			PdpPage pdpPage = searchresultPage.navigateToPDPByRow(1);
			Log.message((i++) + ". Navigated to PDP page with that product");
			Log.message("<br>");
			String originalPriceInSRP = null;
			int size = productDetailsSRP.size();
			if (size == 1) {
				String productPricePDP = pdpPage.getProductPrice();
				originalPriceInSRP = productDetailsSRP.get("NowPrice")
						.replace("Now $", "").trim();
				originalPriceInSRP = originalPriceInSRP.replace("$", "");
				Log.message("<b>Expected Result: </b> The product price in PDP should same as in the Search Result Page");
				Log.assertThat(originalPriceInSRP.equals(productPricePDP),
						"<b>Actual Result: </b> The Product Now price in Search result is: '"
								+ originalPriceInSRP
								+ "' is equal to the price in PDP: '"
								+ productPricePDP + "'.",
						"<b>Actual Result: </b> The Product Now price in Search result is: '"
								+ originalPriceInSRP
								+ "' is not equal to the price in PDP: '"
								+ productPricePDP + "'", driver);

			} else {
				String productNowPrice = pdpPage.getProductPrice();
				String productOriginalPrice = pdpPage.getStandardPrice();
				originalPriceInSRP = productDetailsSRP.get("OriginalPrice")
						.replace("Orig. $", "").trim();
				String nowPriceInSRP = productDetailsSRP.get("NowPrice")
						.replace("Now $", "").trim();
				Log.message("<b>Expected Result: </b> The product price in PDP should same as in the Search Result Page");
				Log.assertThat(
						(nowPriceInSRP.equals(productNowPrice))
								&& (originalPriceInSRP
										.equals(productOriginalPrice)),
						"<b>Actual Result: </b> The Product prices in Search result is NOW: '"
								+ nowPriceInSRP + "' and ORIG: '"
								+ originalPriceInSRP
								+ "' is equal to the price in PDP is NOW: '"
								+ productNowPrice + "' and ORIG: '"
								+ productOriginalPrice + "'",
						"<b>Actual Result: </b> The Product prices in Search result is NOW: '"
								+ nowPriceInSRP
								+ "' and ORIG: '"
								+ originalPriceInSRP
								+ "' is not equal to the price in PDP is NOW: '"
								+ productNowPrice + "' and ORIG: '"
								+ productOriginalPrice + "'", driver);
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT3_003

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify the Qty, Color, Size, add to bag, registry, wishlist links, image Nav button, Social sharing icons in QuickView Page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT3_008(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			// Load PDP Page for Search Keyword
			SearchResultPage searchresultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message((i++)
					+ ".Searched with the keyword '"
					+ searchKey
					+ "' in search text box and navigated to 'Search Result'  Page!");

			QuickViewPage quickViewPage = searchresultPage
					.navigateToQuickViewByIndex(1);
			Log.message((i++)
					+ ".Clicked the Quick View button and the QuickView popup is displayed");

			quickViewPage.selectColor();
			quickViewPage.selectSize();
			Log.message("<br>");
			Log.message("<b>Expected Result: </b> The following element should present in the Quick View (Qty, Color, Size, add to bag, registry, wishlist links, image Nav button, Social sharing).");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkBrandName"), quickViewPage),
					"<b>Actual Result: </b> The 'Brand Name' is displayed",
					"<b>Actual Result: </b> The 'Brand Name' is not displayed",
					driver);
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("productName"), quickViewPage),
					"<b>Actual Result: </b> The 'Product Name' is displayed",
					"<b>Actual Result: </b> The 'Product Name' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lblProductPrice"), quickViewPage),
					"<b>Actual Result: </b> The Product price is displayed",
					"<b>Actual Result: </b> The Product price is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("productcolor"), quickViewPage),
					"<b>Actual Result: </b> The 'Product Color' is displayed",
					"<b>Actual Result: </b> The 'Product Color' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("productSize"), quickViewPage),
					"<b>Actual Result: </b> The 'Product Size' is displayed",
					"<b>Actual Result: </b> The 'Product Size' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("productQty"), quickViewPage),
					"<b>Actual Result: </b> The 'Product Quantity' is displayed",
					"<b>Actual Result: </b> The 'Product Quantity' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("btnUpdateShoppingCart"),
							quickViewPage),
					"<b>Actual Result: </b> The 'Add to Shopping bag' button is displayed",
					"<b>Actual Result: </b> The 'Add to Shopping bag' button is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("btnRegistryAsGuest"), quickViewPage),
					"<b>Actual Result: </b> The 'Registry' link is displayed",
					"<b>Actual Result: </b> The 'Registry' link is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("btnAddToWishList"), quickViewPage),
					"<b>Actual Result: </b> The 'Wish List' link is displayed",
					"<b>Actual Result: </b> The 'Wish List' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkFacebook"), quickViewPage),
					"<b>Actual Result: </b> The 'Facebook' icon is displayed",
					"<b>Actual Result: </b> The '' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkTwitter"), quickViewPage),
					"<b>Actual Result: </b> The 'Twitter' icon is displayed",
					"<b>Actual Result: </b> The 'Twitter' icon is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkGooglePlus"), quickViewPage),
					"<b>Actual Result: </b> The 'Google Plus' icon is displayed",
					"<b>Actual Result: </b> The '' is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkEmail"), quickViewPage),
					"<b>Actual Result: </b> The 'Email' icon is displayed",
					"<b>Actual Result: </b> The 'Email' icon is not displayed");
			Log.softAssertThat(
					quickViewPage.elementLayer.verifyPageElements(
							Arrays.asList("lnkPintrest"), quickViewPage),
					"<b>Actual Result: </b> The 'Pintrest' icon is displayed",
					"<b>Actual Result: </b> The 'Pintrest' icon is not displayed",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT3_008

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify user able to place the order with Default billing address and Select existing saved Shipping address as a authenticated chekout!", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_009(String browser) throws Exception {

		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String searchKey = testData.get("SearchKey");
		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			Log.message("2. Navigated to Sign In Page!");

			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();

			}
			SearchResultPage searchResultPage = homePage
					.searchProductKeyword(searchKey);
			Log.message("3. Searched with '" + searchKey
					+ "' in the search text box!");

			pdpPage = searchResultPage.navigateToPDP();
			Log.message("4. Navigated to 'Pdp' Page with randomly selected product!");

			Log.message("5. Selecting Size, Color, Quantity!");
			String size = pdpPage.selectSize();
			String color = pdpPage.selectColor();
			pdpPage.selectQtyByIndex(5);
			Log.message("   Selected Size : " + size);
			Log.message("   Selected Color : " + color);
			Log.message("   Selected Quantity");

			// Adding product to bag
			pdpPage.clickAddToBag();
			Log.message("6. Clicked on 'Add to bag' button!");

			// Navigating to Shopping Bag Page
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("7. Clicked on 'Mini cart icon' and Navigated to the Bag Page!");

			// Click On Checkout Button
			CheckoutPage checkoutPage = shoppingBagPage
					.clickCheckoutInOrderSummaryForRegUser();
			Log.message("8. Clicked Checkout button and navigated to Shipping page as authorized user!");

			// Filling Shipping Details
			checkoutPage.selectShipingAddressByValue(0);
			Log.message("9. Successfully Selected Address In Shipping Page!");

			// Click On Continue Shipping Page
			checkoutPage.clickOnContinueInShipping();
			Log.message("10. clicked on continue Button in Shipping address Page!");

			checkoutPage.chooseOriginalAddressInModal();
			Log.message("11. Selected Original address in the modal!");
			checkoutPage.ContinueAddressValidationModalWithDefaults();

			// Filling Billing Details
			checkoutPage.fillingBillingDetailsAsSignedInUser("No", "No",
					"valid_address1");
			Log.message("12.  Successfully Filled Billing Details!");

			// Filling Card Details
			checkoutPage.fillingCardDetails("NO", "card_Discover");
			Log.message("13.  Successfully Filled Card Details!");

			// Click On Continue in Billing Page
			checkoutPage.clickOnContinueInBilling();
			Log.message("14. Clicked on continue button in Billing address Page");

			// Click On Place Order Button
			checkoutPage.placeOrder();
			Log.message("15. Clicked on 'Place order' button in Place order page");
			Log.message("<b>Expected Result:</b> Order should placed successfully with Default billing address and Select existing saved Shipping address as a authenticated chekout");
			Log.assertThat(driver.getCurrentUrl().contains("COSummary-Submit"),
					"<b>Actual Result:</b>  Order placed successfully",
					"<b>Actual Result1:</b> Order not placed successfully",
					driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT5_009

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = " Verify user able to place the order with multi shipping and Standard and Express shipping method type as a Autherizied user", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_010(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);
		String[] searchKey = testData.get("SearchKey").replace("S_", "")
				.split("\\|");
		String username = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] address = testData.get("Address").split("\\|");
		int i = 1;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo(testData);

		try {
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(
					username, password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

			} else {
				myAccountPage.headers.clickSignOut();

			}

			// Searching 2 different products like jean & T-shirts.
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				pdpPage = signInPage.headers
						.searchAndNavigateToPDP(searchKey[searchIndex]);

				Log.message((i++) + ". Searched with keyword '"
						+ searchKey[searchIndex] + "' ");

				Log.message((i++) + ". Navigated to PDP");
				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize
						+ "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName
						+ "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}
			// Navigate to the Shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message((i++)
					+ ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage
					.clickOnCheckoutAsUser(username, password);
			Log.message((i++) + ". Entered credentials(" + username + "/"
					+ password + ") and navigating to 'My Account' page",
					driver);
			// Clicking on 'Yes' button for multishipping
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message((i++)
					+ ". Clicked on 'YES' Button in Ship to Multiple Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage
						.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message((i++) + ". Click on 'Continue' in ShippingAddress Page");
			checkoutPage.selectShippingMethodInMultiShippingByRow(1, "Express");
			Log.message((i++) + ". Select the shipping type for product 1");
			checkoutPage
					.selectShippingMethodInMultiShippingByRow(2, "Standard");
			Log.message((i++) + ". Select the shipping type for product 2");
			checkoutPage.clickOnContinueInShipping();
			Log.message((i++)
					+ ". Click On the continue button the on Select Shipping type ");
			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES",
					"valid_address1");
			// filling card details
			Log.message((i++) + ". Fill the billing details");

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message((i++) + ". Fill the Card details in Billing form!");

			checkoutPage.clickOnContinueInBilling();
			Log.message((i++) + ". Click continue in Billing Page.");

			// Navigated to 'Order Summary Page'
			OrderConfirmationPage orderconfirmationpage = (OrderConfirmationPage) checkoutPage
					.placeOrder();
			Log.message((i++) + ". Place order button is clicked");

			String orderText = orderconfirmationpage.getTextFromOrderTotal();
			Log.message((i++) + ". The Order total ammount is :" + orderText);

			String totalShippingAmmount = orderconfirmationpage
					.getTextFromShipping();
			Log.message((i++) + ". The Total Shipping ammount is: "
					+ totalShippingAmmount);
			Log.message("<br>");
			Log.message("<b> Expected Result 1: </b> User is able to place the order. ");
			Log.message("<b> Actual Result 1: </b> The Total Shipping ammount is: "
					+ totalShippingAmmount);
			Log.message("<br>");

			// Getting 'Order Number' from order summary page
			String ordernumb = orderconfirmationpage.GetOrderNumber();
			Log.message((i++) + ". The Order Number is : " + ordernumb);
			Log.message("<br>");
			Log.message("<b> Expected Result 2: </b> Order should be properly placed from Place order screen.");
			Log.assertThat(orderconfirmationpage.elementLayer
					.verifyPageElements(Arrays.asList("orderNumber"),
							orderconfirmationpage),
					"<b> Actual Result 2: </b> The Order is placed and order id is :"
							+ ordernumb,
					"<b> Actual Result 2: </b> The order is not placed", driver);

			// Filling shipping address

			Log.message("<br>");
			Log.testCaseResult();
			shoppingBagPage.removeAddedCartItemsInShoppingBag();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT5_010

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify order Details Displayed in Order Confirmation page.", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_005(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		String emailid = testData.get("EmailAddress");
		String password = testData.get("Password");
		String[] searchKey = testData.get("SearchKey").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo(testData);
		try {
			int i = 1;
			// Load the Home Page
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message((i++) + ". Navigated to 'Belk' Home Page!");

			SignIn signInPage = homePage.headers.navigateToSignIn();
			MyAccountPage myAccountPage = signInPage.signInToMyAccount(emailid,
					password);
			ShoppingBagPage bagPage = null;
			PdpPage pdpPage = null;

			if (Integer.parseInt(myAccountPage.headers.getQuantityFromBag()
					.replace(" item(s)", "")) > 0) {

				bagPage = myAccountPage.clickOnMiniCart();
				bagPage.removeItemsFromBag();
				bagPage.headers.clickSignOut();

			} else {
				myAccountPage.headers.clickSignOut();

			}

			// Searching 2 different products like jean & T-shirts.
			for (int searchIndex = 0; searchIndex < searchKey.length; searchIndex++) {

				pdpPage = signInPage.headers
						.searchAndNavigateToPDP(searchKey[searchIndex]);

				Log.message((i++) + ". Searched with keyword '"
						+ searchKey[searchIndex] + "' ");

				Log.message((i++) + ". Navigated to PDP");
				// Select the size,color & qty.

				String selectedSize = pdpPage.selectSize();
				Log.message((i++) + ". Selected size: '" + selectedSize
						+ "' from color swatch in the PDP page");

				String colorName = pdpPage.selectColor();
				Log.message((i++) + ". Selected color: '" + colorName
						+ "' from color swatch in the PDP page");

				String qty = pdpPage.selectQuantity();
				Log.message((i++) + ". Select the Quantity: " + qty);

				pdpPage.clickAddToBag();
				Log.message((i++) + ". Add the Product to the bag");
			}
			// Navigate to the Shopping bag page
			ShoppingBagPage shoppingBagPage = pdpPage.minicart.navigateToBag();
			Log.message((i++)
					+ ". By Clicking on 'MiniCart' Icon, It Navigated to 'Shopping Bag' Page");

			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message((i++) + ". Clicked on Checkout in Order summary Page");

			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage
					.clickOnCheckoutAsUser(emailid, password);
			Log.message((i++) + ". Entered credentials(" + emailid + "/"
					+ password + ") and navigating to 'My Account' page",
					driver);

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO",
					"Standard", "valid_address1");
			Log.message("9. Filled shipping details and shipment method");

			checkoutPage.clickOnContinueInShipping();
			checkoutPage.ContinueAddressValidationModalWithDefaults();
			Log.message("10. Clicked on Continue in shipping");

			checkoutPage.fillingBillingDetailsAsSignedInUser("NO", "YES",
					"valid_address2");
			Log.message("11. filled billing address");

			checkoutPage.fillingCardDetails("NO", "card_Amex");
			Log.message("12. filled card details");

			checkoutPage.clickOnContinueInBilling();
			Log.message("13. clicked on continue button");

			OrderConfirmationPage orderConfirmationPage = checkoutPage
					.placeOrder();
			Log.message("14. clicked on Place order button");
			LinkedHashMap<String, String> Address = orderConfirmationPage
					.getBillingAddressInOrderSummary();

			Log.message("<b>Expected Result : </b> Verify Order Summary Details!");
			Log.assertThat(
					orderConfirmationPage.elementLayer.verifyPageElements(
							Arrays.asList("returnToShoppingBtn",
									"confirmationMessage"), orderConfirmationPage),
					"<b>Actual Result : </b> Order is SuccessFully Placed and Details are :"
							+ Address,
					"<b>Actual Result : </b> Order is not SuccessFully Placed!",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		}
		// catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_BELK_BAT_SPRINT5_005

	@Test(groups = { "desktop_bat", "mobile_bat", "tablet_bat" }, description = "Verify Deal Based call out messag is displayed on the PDP page and Verify that clicking on the Edit  button on the shipping address panel ,navigate to the new shipping  address page .   ", dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_BELK_BAT_SPRINT5_004(String browser) throws Exception {

		// ** Loading the test data from excel using the test case id */
		HashMap<String, String> testData = TestDataExtractor.initTestData(
				workbookName, sheetName);

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey[] = testData.get("SearchKey").replace("S_", "")
				.split("\\|");
		String address[] = testData.get("Address").split("\\|");
		Log.testCaseInfo(testData);

		try {
			HomePage homePage = new HomePage(driver, webSite).get();
			Log.message("1. Navigated to 'Belk' Home Page!");

			PdpPage pdpPage = homePage.headers
					.searchAndNavigateToPDP(searchKey[0]);
			Log.message("2. Searched with '" + searchKey[0] + "'");

			Log.message("3. Navigated to PDP!");
			Log.message("<b>Expected Result :</b> Rebate message should be displayed");
			Log.assertThat(
					pdpPage.elementLayer.verifyPageElements(
							Arrays.asList("msgPromotion"), pdpPage),
					"<b>Actual Result :</b> Rebate message displayed '"
							+ pdpPage.getTextforPromotionMessage() + "'",
					"<b>Actual Result :</b> Rebate message is not displayed");
			// Selecting the Color
			String color = pdpPage.selectColor();
			Log.message("4. selected the '" + color + "' Color !");
			// Selecting the Size
			String size = pdpPage.selectSize();
			Log.message("5. selected the '" + size + "' size !");
			// Selecting the quantity as two for the same product
			String quantity = pdpPage.selectQuantity();
			Log.message("6. selected the '" + quantity + "' quantity !");
			// Adding item to the shopping bag

			pdpPage.clickAddToBag();
			Log.message("7. Product added to Bag !");

			pdpPage.headers.searchAndNavigateToPDP(searchKey[1]);
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickAddToBag();
			// Navigating to shopping bag
			ShoppingBagPage shoppingBagPage = pdpPage.clickOnMiniCart();
			Log.message("8. Navigated to shopping bag page");
			Log.message("<b>Expected Result :</b> surcharge message should be dispalyed to the shopping cart page");
			Log.assertThat(
					shoppingBagPage.elementLayer.verifyPageElements(
							Arrays.asList("lblLineitemalertmessage"),
							shoppingBagPage),
					"<b>Actual Result :</b> surcharge message is dispalyed in the shopping cart page",
					"<b>Actual Result :</b> surcharge message is not dispalyed in the shopping cart page");
			// clicking on checkout button in order summary
			SignIn checkoutSignInPage = (SignIn) shoppingBagPage
					.clickOnCheckoutInOrderSummary(shoppingBagPage);
			Log.message("9. Navigated to checkout Login page");
			// Navigating to checkout page as user
			CheckoutPage checkoutPage = checkoutSignInPage
					.clickOnCheckoutAsGuest();
			Log.message("10. navigating to 'Checkout' page");
			// Click on Multiple Shipping address Button
			checkoutPage.clickOnYesInShipToMultipleAddress();
			Log.message("11. Clicked on 'YES' Button in Ship to Multiple Address Page");

			for (int g = 0; g < address.length; g++) {
				checkoutPage.clickOnAddInShipToMulipleAddress();
				checkoutPage
						.fillingAddressDetailsInMultipleShipping(address[g]);
				checkoutPage.chooseOriginalAddressInModal();
				if (checkoutPage.clickContinueInAddressValidationModal())
					checkoutPage.clickOnSaveInAddEditAddressModal();
			}
			checkoutPage.selectAddrInAddressDropDownInMultiship();
			checkoutPage.clickOnContinueInMultiAddress();
			Log.message("18. Clicked on continue button in multiaddress ");
			checkoutPage
					.selectShippingMethodInMultiShippingByRow(1, "Standard");
			Log.message("17. Selected 'Express Shipping' for the 1st product.");

			checkoutPage.clickOnContinueInShipping();
			Log.message("19. Clicked on continue button in multiShipping page");
			// filling billing address
			checkoutPage.fillingBillingDetailsAsGuest("valid_address2");
			Log.message("20. filled billing address");
			checkoutPage.fillingBelkGiftCardDetails("GiftCard5");
			checkoutPage.clickOnApplyGiftCard();
			checkoutPage.fillingCardDetails("No", "card_Amex");
			Log.message("21. filled card Details");
			// clicking continue in billing page

			checkoutPage.clickOnContinueInBilling();
			Log.message("22. Clicked continue button in billing page");
			// click on place order button
			checkoutPage.clickOnEditBySectionName("shippingaddress");

			// / Navigate to My Account Page

			Log.message("<b>Expected Result :</b> When clicking on the  Edit  button on the shipping address panel , it should navigate to the shipping  address page");
			Log.assertThat(
					driver.getCurrentUrl().contains("COShippingMultiple-Start"),
					"<b>Actual Result :</b> clicking on the  Edit  button on the shipping address panel ,navigate to the shipping  address page",
					"<b>Actual Result :</b> clicking on the  Edit  button on the shipping address panel , not navigate to the new shipping  address page",
					driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_BELK_BAT_SPRINT5_004

}
