package utils.belk.testlink;

public class OverridedAPIClient {

	public void resultUpdate(String tcID, String devKey, String url, String projectName, String testPlanName, String testSuiteID, String execNotes, String testResultStatus, String testBuildName) {

		TestLinkAPIClient api = new TestLinkAPIClient(devKey, url);
		try {
			api.reportTestCaseResult(projectName, testPlanName, testSuiteID, tcID, execNotes, testResultStatus, testBuildName);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
}
